<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

	public function render($view, $data = null, $return = false)
	{
		return parent::render($view, $data, $return);
	}

	public function renderPartial($view, $data = null, $return = false, $processOutput = false)
	{
		$returnType = Yii::app()->params['requestType'];
		if ($returnType == 'mobile' || $view == 'json') {
			$jsonData = json_encode($data);
			if ($return === false) {
				$statusCode = Rest::REST_OK;
			} else {
				$statusCode = $return;
			}
			if ($view != 'json') {  // must be mobile
				return $jsonData;
			} else { // assume json
				Rest::sendResponse($statusCode, $jsonData, 'application/json');
			}
		} else {
			return parent::renderPartial($view, $data, $return, $processOutput);
		}
	}

	public function renderMessage($params, $partial = false)
	{
		if (!is_array($params)) {
			$params = array('messageId' => $params);
		}
		if ($partial) {
			$this->renderPartial('//site/vMessage', $params);
		} else {
			$this->render('//site/vMessage', $params);
		}
		Yii::app()->end();
	}

	public function paramObscureId($required=true,$idName='id')
	{
		$id = getGet($idName,false);
		if ($id === false) $id = getPost($idName);
		if (Obscure::isObscure($id) !== false) {
			$id = Obscure::decode($id);
		} else if ($required || !empty($id)) {
			badurl($idName . ' is not obscure and invalid.');
		}
		return $id;
	}

}