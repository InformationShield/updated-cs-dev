<div id="divMessage" class="x_panel">
    <div class="x_title">
        <h2>Recent Messages</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li>
                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" style="display: block;">
        <div class="dashboard-widget-content">
            <ul class="list-unstyled timeline widget">
                <? foreach ($messages as $mes) { ?>
                    <li>
                        <div class="block">
                            <div class="block_content">
                                <h2 class="title">
                                    <a><?= $mes->title; ?></a>
                                </h2>
                                <div class="byline">
                                    <span>
                                        <?= DateTimeTools::TimeElapsedString($mes->created_on,false); ?>

                                    </span>
                                        by
                                    <a><?= $mes->sender->username; ?></a>
                                </div>
                                <p class="excerpt">
                                    <?= substr($mes->message, 0, 190); ?>...
                                    <a href="<?= Yii::app()->createAbsoluteUrl("/message/list"); ?>" style="display: block;">
                                        Read&nbsp;More
                                    </a>
                                </p>
                            </div>
                        </div>
                    </li>

                <? } ?>
            </ul>
        </div>
    </div>
</div>
