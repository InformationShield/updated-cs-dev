<?php
/**
 *  GridViewController is the customized Controller class for
 *  supporting similar grid view pages to perform List, View, Search, Delete, Copy, and Download actions.
 *  This base class helps insure common license and role checking and handling in general.
 */
class GridViewController extends Controller
{
    // override this method to set skipEncode and return view values
    protected function getViewValues($id,&$skipEncode){
        throw new Exception('getViewValues override method missing.');
    }
    public function renderView($menuLevel=Cons::LIC_TRIAL, $role=Cons::ROLE_AUDITOR)
    {
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		if (isset($id) && $id > 0) {
			$skipEncode = null;
			$values = $this->getViewValues($id,$skipEncode);
			$gmAjaxListView = new gmAjaxListView($values, $skipEncode);
			AjaxListViewGadget::show($gmAjaxListView); // renderPartial a view
		}
    }

    public function getSearchParams()
    {
        $cmd = getPost('cmd');
        $pageSize = getPost('limit');
        $offsetBy = getPost('offset');
        $search = getPost('search');
        $searchLogic = getPost('searchLogic');
        $sort = getPost('sort');
        return array('$cmd'=>$cmd,'pageSize'=>$pageSize,'offsetBy'=>$offsetBy,'search'=>$search,'searchLogic'=>$searchLogic,'sort'=>$sort);
    }

    // can override to do post delete actions
    protected function postDelete($id,$model,$markStatusOnly=false) {
        return true; // return true or false or message to log, false logs a general message
    }
    // can override to do preDeleteCheck to potential stop delete action if return is not true
    protected function preDeleteCheck($id,$model,$markStatusOnly=false) {
        return true; // return true or results array with status and message etc
    }
    // called by doDelete() which is a an override method
    // generic function to delete model and returns params for json staus and message, check $status for 'success' or 'error'
    // can be overridden for other use cases
    protected function doModelDelete($id,$classInstance,$attrs,$markStatusOnly=false)
    {
        $results = array('status'=>'error','message'=>'Failed to delete record.');
        try {
            $model = $classInstance->findByAttributes($attrs);
            if (isset($model)) {
                $results = $this->preDeleteCheck($id, $model, $markStatusOnly);
                if ($results === true) {
                    $results = array('status' => 'error', 'message' => 'Failed to delete record.');
					if ($markStatusOnly) {
						$model->status = 0;
						$delStatus = $model->save();
					} else {
						$delStatus = $model->delete();
					}
                    if ($delStatus) {
                        $results['status'] = 'success';
                        $results['message'] = 'Deletion successful.';
                        $postDel = $this->postDelete($id, $model, $markStatusOnly);
                        if ($postDel !== true) {
                            if ($postDel === false) {
                                Yii::log('Post delete action failed for ' . $model->tableName() . ' record ' . $id . '.', 'error', 'GridViewController.doModelDelete.postDelete');
                            } else {
                                Yii::log($postDel, 'error', 'GridViewController.doModelDelete.postDelete');
                            }
                        }
                    } else {
                        $results['message'] = 'Failed to delete record, due to errors.';
                        Yii::log(print_r($model->getErrors(), true), 'error', 'GridViewController.doModelDelete');
                    }
                }
            } else {
                $results['message'] = 'Record could not be found.';
            }
        } catch (CDBException $e) {
            $log = false;
            if (isset($e->errorInfo[1]) && $e->errorInfo[1] == 1451) {
                if (stripos($e->getMessage(),'user_profile_policy') !== false) {
                    $results['message'] = 'Can not delete record, because it is referenced by a User Profile -> Policy List. Remove the reference to delete.';
                } else if (stripos($e->getMessage(),'user_profile_training') !== false) {
                    $results['message'] = 'Can not delete record, because it is referenced by a User Profile -> Training List. Remove the reference to delete.';
                } else if (stripos($e->getMessage(),'video_tracking') !== false) {
                    $results['message'] = 'Can not delete record, because it is referenced by a Video Tracking.';
                } else if (stripos($e->getMessage(),'user_profile_policy') !== false) {
                    $results['message'] = 'Can not delete record, because it is referenced by a Policy. Remove the reference to delete.';
                } else if (stripos($e->getMessage(),'video_training') !== false) {
                    $results['message'] = 'Can not delete record, because it is has Videos defined. Remove the Video Series entries first to delete.';
                } else {
                    $log = true;
                    $results['message'] = 'Can not delete record, likely because it is referenced by another object. You will need to remove all references to delete.';
                }
            } else {
                $log = true;
                $results['message'] = 'Failed to delete record, due to an exception.';
            }
            if ($log) {
                Yii::log($e->getMessage(), 'error', 'GridViewController.doModelDelete.CDBException');
            }
        } catch (Exception $e) {
            $results['message'] = 'Failed to delete record, due to an exception.';
            Yii::log($e->getMessage(), 'error', 'GridViewController.doModelDelete.Exception');
        }
        return json_encode($results);
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        throw new Exception('doDelete override method missing.');
        // example
        // return $this->doModelDelete($id,ControlBaseline::model(),array('control_baseline_id' => $id, 'company_id' => Utils::contextCompanyId()),$markStatusOnly);
    }

    // override method called by ajaxCopy()
    // function is to copy and returns params for json staus and message
    // can be overridden for other use cases
    protected function doCopy($id)
    {
        throw new Exception('doCopy override method missing.');
        return array('status'=>'error','message'=>'doCopy override method missing.');
    }
    public function ajaxCopy($targetLicense=Cons::LIC_TRIAL,$targetRole=Cons::ROLE_AUTHOR)
    {
        $results = array('status'=>'error','message'=>'Failed to copy record, due to an issue.');
		if (mPermission::checkLicense(null, $targetLicense, 'silent')) {
			if (mPermission::checkRole(null, $targetRole, 'silent')) {
                $id = $this->paramObscureId(); // will exit if bad param for security reasons
                $results = $this->doCopy($id);
            } else {
                $results['status'] = 'error';
                $results['message'] = 'Your role is not authorized to perform this action.';
            }
        } else {
            $results['status'] = 'error';
            $results['message'] = 'Your license level is not authorized to perform this action, please upgrade your license by contacting us.';
        }
        $json = json_encode($results);
        return $json;
    }

    public function downloadFile($downloadType, $targetLicense=Cons::LIC_TRIAL, $targetRole=Cons::ROLE_ADMIN)
    {
        if (mPermission::checkLicense(null, $targetLicense, 'silent')) {
            if (mPermission::checkRole(null, $targetRole, 'silent')) {
                $id = $this->paramObscureId(); // will exit if bad param for security reasons
                // will send headers and exit if successful, otherwise returns message of failure
                $message = mDownloadFile::send($id, $downloadType, Utils::contextCompanyId());
            } else {
                $message = 'Your role is not authorized to download the file.';
            }
        } else {
            $message = 'Your license level is not authorized to download the file, please upgrade your license by contacting us.';
        }
        $this->renderMessage(array('messageId'=>0,'title'=>'<i class="fa fa-warning"></i> Download Failed!','message'=>$message));
    }
}