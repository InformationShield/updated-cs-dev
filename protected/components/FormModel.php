<?php
/**
 * FormModel is the customized base CFormModel class.
 * All FormModel classes for this application should extend from this base class.
 */
class FormModel extends CFormModel
{
    public $i=0;
	public $RestCode = null;
	public $Results = null;

    public function saveInput($dbTableName, $data, $findBy, $specialFields=array()) {
        $retVal = null;

        $dbTableClass = new $dbTableName();
        if (is_array($findBy)) {
            $dbTable = $dbTableClass::model()->findByAttributes($findBy);
        } else {
            $dbTable = $dbTableClass::model()->findByPk($findBy);
        }

        if ($dbTable) {
            $dbTable->isNewRecord = false;
            // only try to mass assign db fields that actually exists in the class
            foreach ($data as $key=>$value) {
                if (!array_key_exists($key, $dbTable->attributes)) {
                    unset($data[$key]);
                }
            }
            $this->unsetPrimaryKey($dbTableName, $data); // primary key is an unsafe attribute, so don't assign it or it will fill the log with warnings.
            $dbTable->attributes = $data;
            foreach ($specialFields as $key=>$value) {
                $dbTable->{$key} = $value;
            }
            // set and format default fields properly
            foreach ($dbTable->attributes as $key=>$value) {
                if ($key == 'updated_on') {
                    $dbTable->updated_on = new CDbExpression('CURRENT_TIMESTAMP');
                } else if ($key == 'updated_user_id') {
                    $dbTable->updated_user_id = userId();
                } else if (substr($key,-3) == '_on' || substr($key,-4) == 'date' ) {
                    if ($value == 'DateNow') {
                        $dbTable->{$key} = new CDbExpression('CURRENT_TIMESTAMP');
                    } else {
                        $dbTable->{$key} = DateHelper::FormatDateTimeString($value, 'Y-m-d H:i:s');
                    }
                }
            }
            if ($dbTable->save()) {
                $retVal = $dbTable;
            } else {
                $errors = $dbTable->getErrors();
                if ($errors) {
                    $error = reset($errors);    // reset gets the first element in an array
                    $this->addError(get_class($this), $error[0]);
					Yii::log($dbTableName.'::'.$error[0],'error','FormModel.saveInput');
                }
            }
        }
        return $retVal;
    }

    public function createFromInput($dbTableName, $data, $requiredFields=array()) {
        $retVal = null;

        $dbTable = new $dbTableName();
        if ($dbTable) {
            $dbTable->isNewRecord = true;
            // only try to mass assign db fields that actually exists in the class
            foreach ($data as $key=>$value) {
                if (!array_key_exists($key, $dbTable->attributes)) {
                    unset($data[$key]);
                }
            }
            $this->unsetPrimaryKey($dbTableName, $data); // primary key is an unsafe attribute, so don't assign it or it will fill the log with warnings.
            $dbTable->attributes = $data;
            foreach ($requiredFields as $key=>$value) {
                $dbTable->{$key} = $value;
            }
            // set and format default fields properly
            foreach ($dbTable->attributes as $key=>$value) {
				if ($key == 'updated_on') {
					$dbTable->updated_on = new CDbExpression('CURRENT_TIMESTAMP');
				} else if ($key == 'updated_user_id') {
					$dbTable->updated_user_id = userId();
				} else if ($key == 'created_on') {
                    $dbTable->created_on = new CDbExpression('CURRENT_TIMESTAMP');
                } else if (substr($key,-3) == '_on' || substr($key,-4) == 'date' ) {
                    if ($value == 'DateNow') {
						$dbTable->{$key} = new CDbExpression('CURRENT_TIMESTAMP');
					} else if ($value && substr($value, 0, 4) == 'CDB ') {
						$cdbExpression = substr($value, 4, strlen($value)-4);
						$dbTable->{$key} = new CDbExpression($cdbExpression);
                    } else {
                        $dbTable->{$key} = DateHelper::FormatDateTimeString($value, 'Y-m-d H:i:s');
                    }
                }
            }
            if ($dbTable->save()) {
                $retVal = $dbTable;
            } else {
                $errors = $dbTable->getErrors();
                if ($errors) {
                    $error = reset($errors);    // reset gets the first element in an array
                    $this->addError(get_class($this), $error[0]);
					Yii::log($dbTableName.'::'.$error[0],'error','FormModel.createFromInput');
                }
            }
        }
        return $retVal;
    }

    // recursive function to scrub data
    public function standardScrub(&$data) {
        $this->i++;
        if (is_array($data)) {
            foreach ($data as $key=>$input) {
                if (is_array($input)) {
                    if ($this->i > 10) {    // protection for potential infinite loop
                        break;
                    } else {
                        $this->standardScrub($data[$key]);
                    }
                } else {
                    $data[$key] = blank2null($data[$key]);
                    if (strlen($data[$key]) > 0 && (stripos($key, 'phone') !== false  || stripos($key, 'fax') !== false)) {
                        $this->formatPhoneNumber($data[$key]);
                    }
                }
            }
        }
        return true;
    }

    public function formatPhoneNumber(&$phoneNumber) {
        $phn = preg_replace("/[^0-9]/", "", $phoneNumber);
        if (strlen($phn) == 11 && substr($phn, 0, 1) == 1) {
            $phoneNumber = substr($phn, 0, 1).'-'.substr($phn, 1, 3).'-'.substr($phn, 4, 3).'-'.substr($phn, 7, 4);
        } else if (strlen($phn) == 10) {
            $phoneNumber = substr($phn, 0, 3).'-'.substr($phn, 3, 3).'-'.substr($phn, 6, 4);
        }
    }

    private function unsetPrimaryKey($dbTableName, &$data) {
        switch (strtolower($dbTableName)) {
            case 'inspectanswer':
                $primaryKey = 'InspectAnswerId';
                break;
            case 'inspectbilling':
                $primaryKey = 'InspectBillingId';
                break;
            case 'inspecthistory':
                $primaryKey = 'InspectHistoryId';
                break;
            case 'servicedoc':
                $primaryKey = 'ServiceDocId';
                break;
            default:
                $primaryKey = $dbTableName.'Id';
        }

        unset($data[$primaryKey]);
    }

    public function addError($attribute, $error) {
        if ($error) {
            $bang = substr($error, 0, 1) == '!' ? '' : '!'; // Add bang(!) if error text already doesn't start with one
            $this->setFlash($attribute, $bang.$error);
            return parent::addError($attribute, $error);
        }
        return false;
    }


    public function setFlash($attribute, $error) {
		if (Yii::app()->params['appType'] == 'Console') {
			echo "$attribute: $error\n";
		} else {
			Yii::app()->getUser()->setFlash($attribute, $error);
		}
    }

    public function intersectScrub($target, $source) {
        foreach($source as $key=>$value) {
            if (!array_key_exists($key, $target)) {
                unset($source[$key]);
            }
        }
        return $source;
    }

}