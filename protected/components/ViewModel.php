<?php
/**
 * FormModel is the customized base CFormModel class.
 * All FormModel classes for this application should extend from this base class.
 */
class ViewModel extends CFormModel
{
	public $i = 0;

    public static function saveInput($callingObj, $dbClassName, $dbTableName, $data, $findBy, $specialFields=array()) {
        $retVal = null;

        $dbTableClass = new $dbClassName();
        if (is_array($findBy)) {
            $dbTable = $dbTableClass::model()->findByAttributes($findBy);
        } else {
            $dbTable = $dbTableClass::model()->findByPk($findBy);
        }

        if ($dbTable) {
            $dbTable->isNewRecord = false;
            // only try to mass assign db fields that actually exists in the class
            foreach ($data as $key=>$value) {
                if (!array_key_exists($key, $dbTable->attributes)) {
                    unset($data[$key]);
                }
            }
            self::unsetPrimaryKey($dbTableName, $data); // primary key is an unsafe attribute, so don't assign it or it will fill the log with warnings.
			unset($data['created_on']); // database sets this field
            $dbTable->attributes = $data;
            foreach ($specialFields as $key=>$value) {
                $dbTable->{$key} = $value;
            }
            // set and format default fields properly
//            foreach ($dbTable->attributes as $key=>$value) {
//                if ($key == 'UpdatedOn') {
//                    $dbTable->UpdatedOn = new CDbExpression('CURRENT_TIMESTAMP');
//                } else if ($key == 'UpdatedBy') {
//                    $dbTable->UpdatedBy = userId();
//                } else if (substr($key,-2) == 'On' || substr($key,-4) == 'Date' ) {
//                    if ($value == 'DateNow') {
//                        $dbTable->{$key} = new CDbExpression('CURRENT_TIMESTAMP');
//                    } else {
//                        $dbTable->{$key} = DateHelper::FormatDateTimeString($value, 'Y-m-d H:i:s');
//                    }
//                }
//            }
            if ($dbTable->save()) {
                $retVal = $dbTable;
            } else {
                $errors = $dbTable->getErrors();
                if ($errors) {
                    $error = reset($errors);    // reset gets the first element in an array
					$callingObj->addError(get_class($callingObj), $error[0]);
                }
            }
        }
        return $retVal;
    }

    public static function createFromInput($callingObj, $dbClassName, $dbTableName, $data, $requiredFields=array()) {
        $retVal = null;

        $dbTable = new $dbClassName();
        if ($dbTable) {
            $dbTable->isNewRecord = true;
            // only try to mass assign db fields that actually exists in the class
            foreach ($data as $key=>$value) {
                if (!array_key_exists($key, $dbTable->attributes)) {
                    unset($data[$key]);
                }
            }
            self::unsetPrimaryKey($dbTableName, $data); // primary key is an unsafe attribute, so don't assign it or it will fill the log with warnings.
			unset($data['created_on']); // database sets this field
            $dbTable->attributes = $data;
            foreach ($requiredFields as $key=>$value) {
                $dbTable->{$key} = $value;
            }
            // set and format default fields properly
//            foreach ($dbTable->attributes as $key=>$value) {
//                if ($key == 'created_on') {
//                    $dbTable->CreatedOn = new CDbExpression('CURRENT_TIMESTAMP');
//                } else if ($key == 'created_by') {
//                    $dbTable->CreatedBy = userId();
//                } else if (substr($key,-2) == 'On' || substr($key,-4) == 'Date' ) {
//                    if ($value == 'DateNow') {
//                        $dbTable->{$key} = new CDbExpression('CURRENT_TIMESTAMP');
//                    } else {
//                        $dbTable->{$key} = DateHelper::FormatDateTimeString($value, 'Y-m-d H:i:s');
//                    }
//                }
//            }
            if ($dbTable->save()) {
                $retVal = $dbTable;
            } else {
                $errors = $dbTable->getErrors();
                if ($errors) {
                    $error = reset($errors);    // reset gets the first element in an array
					$callingObj->addError(get_class($callingObj), $error[0]);
                }
            }
        }
        return $retVal;
    }

    private static function unsetPrimaryKey($dbTableName, &$data) {
        switch (strtolower($dbTableName)) {
            default:
                $primaryKey = $dbTableName.'_id';
        }

        unset($data[$primaryKey]);
    }

}
