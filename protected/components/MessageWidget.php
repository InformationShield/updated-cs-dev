<?php

class MessageWidget extends CWidget
{
    /**
     * @var message
     */
    public $messages;

    public function run()
    {
        if (! $this->messages ) {
                throw new RuntimeException('No message available.');
            }
        $this->render('messageWidget', array('messages'=>$this->messages));
    }
}