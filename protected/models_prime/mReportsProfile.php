<?php

class mReportsProfile
{
    public $header;

    public function ComplianceReportDetail($company_id, $user_profile_id, $withHeader=false)
    {
		$header = array(
			'firstname'=>'First Name',
			'lastname'=>'Last Name',
			'department_name'=>'Department',
			'lastvisit_at'=>'Last Visit',

			'policy_count'=>'Policy Count',
			'policy_views'=>'Policy Viewed',
			'policy_views_per'=>'%',
			'policy_acks'=>'Policy Acknowledged',
			'policy_acks_per'=>'%',
			'policy_unopened'=>'Policy Unopened',
			'policy_unopened_per'=>'%',

			'training_count'=>'Training Count',
			'training_views'=>'Training Viewed',
			'training_views_per'=>'%',
			'training_completed'=>'Training Completed',
			'training_completed_per'=>'%',
			'training_unopened'=>'Training Unopened',
			'training_unopened_per'=>'%',

			'quiz_count'=>'Quiz Count',
			'quiz_viewed'=>'Quiz Viewed',
			'quiz_viewed_per'=>'%',
			'quiz_completed'=>'Quiz Completed',
			'quiz_completed_per'=>'%',
			'quiz_passed'=>'Quiz Passed',
			'quiz_passed_per'=>'%',
			'quiz_unopened'=>'Quiz Unopened',
			'quiz_unopened_per'=>'%',
		);

        $sql = "
			SELECT employee_id,
				firstname, lastname, department_name, id, 
				
				CASE
					WHEN MAX(lastvisit_at) IS NULL then
						'unregistered'
					WHEN MAX(lastvisit_at) =  '0000-00-00 00:00:00' then
						'never signed-in'
					ELSE
						DATE_FORMAT(MAX(lastvisit_at),'%m/%d/%Y')
				END lastvisit_at,
				
				SUM(policy_count) policy_count, 
				SUM(policy_unopened) policy_unopened, 
				SUM(policy_views) policy_views, 
				SUM(policy_acks) policy_acks, 
				
				SUM(training_count) training_count, 
				SUM(training_unopened) training_unopened, 
				SUM(training_views) training_views, 
				SUM(training_completed) training_completed, 

				SUM(quiz_count) quiz_count, 
				SUM(quiz_unopened) quiz_unopened, 
				SUM(quiz_viewed) quiz_viewed, 
				SUM(quiz_completed) quiz_completed, 
				SUM(quiz_passed) quiz_passed, 

				IF (SUM(policy_count) > 0, ROUND(SUM(policy_views) / SUM(policy_count) * 100, 0), 0) policy_views_per,
				IF (SUM(policy_count) > 0, ROUND(SUM(policy_acks) / SUM(policy_count) * 100, 0), 0) policy_acks_per,
				IF (SUM(policy_count) > 0, 
					100 - ROUND((SUM(policy_views) + SUM(policy_acks)) / SUM(policy_count) * 100, 0)
				, 0) policy_unopened_per, 

				IF (SUM(training_count) > 0, ROUND(SUM(training_views) / SUM(training_count) * 100, 0), 0) training_views_per,
				IF (SUM(training_count) > 0, ROUND(SUM(training_completed) / SUM(training_count) * 100, 0), 0) training_completed_per,
				IF (SUM(training_count) > 0, 
					100 - ROUND((SUM(training_views) + SUM(training_completed)) / SUM(training_count) * 100, 0)
				, 0) training_unopened_per, 

				IF (SUM(quiz_count) > 0, ROUND(SUM(quiz_viewed) / SUM(quiz_count) * 100, 0), 0) quiz_viewed_per,
				IF (SUM(quiz_count) > 0, ROUND(SUM(quiz_completed) / SUM(quiz_count) * 100, 0), 0) quiz_completed_per,
				IF (SUM(quiz_count) > 0, ROUND(SUM(quiz_passed) / SUM(quiz_count) * 100, 0), 0) quiz_passed_per,
				IF (SUM(quiz_count) > 0, 
					100 - ROUND((SUM(quiz_viewed) + SUM(quiz_completed)) / SUM(quiz_count) * 100, 0)
				, 0) quiz_unopened_per

			FROM (
			
				-- Policy Stats
				SELECT cu.employee_id,
					cu.firstname, cu.lastname, cu.department_name, u.id, MAX(u.lastvisit_at) lastvisit_at,
					COUNT(p.policy_id) policy_count, COUNT(p.policy_id) - IFNULL(SUM(upt.view), 0) policy_unopened, IFNULL(SUM(upt.view = 1 AND upt.ack != 1), 0) policy_views, IFNULL(SUM(upt.ack = 1), 0) policy_acks,
					0 training_count, 0 training_unopened, 0 training_views, 0 training_completed,
					0 quiz_count, 0 quiz_unopened, 0 quiz_viewed, 0 quiz_completed, 0 quiz_passed
				FROM company cp
					INNER JOIN employee cu ON cu.company_id = cp.company_id
					INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
					INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
					INNER JOIN policy p ON p.company_id = cp.company_id AND p.policy_status_id = 3 AND p.status = 1
					INNER JOIN user_profile_policy upp ON upp.policy_id = p.policy_id AND upp.user_profile_id = up.user_profile_id
					LEFT JOIN tbl_users u ON u.email = cu.email
					LEFT JOIN user_policy_tracking upt ON upt.policy_id = p.policy_id AND upt.user_id = u.id
				WHERE cp.company_id = :company_id
					AND up.user_profile_id = :user_profile_id
				GROUP BY cu.employee_id
				
				UNION ALL
				
				-- Training Stats
				SELECT cu.employee_id,
					cu.firstname, cu.lastname, cu.department_name, u.id, MAX(u.lastvisit_at) lastvisit_at,
					0 policy_count, 0 policy_unopened, 0 policy_views, 0 policy_acks,
					COUNT(t.training_id) training_count, COUNT(t.training_id) - IFNULL(SUM(utt.view), 0) training_unopened, IFNULL(SUM(utt.view = 1 AND utt.ack != 1), 0) training_views, IFNULL(SUM(utt.ack = 1), 0) training_completed,
					0 quiz_count, 0 quiz_unopened, 0 quiz_viewed, 0 quiz_completed, 0 quiz_passed
				FROM company cp
					INNER JOIN employee cu ON cu.company_id = cp.company_id
					INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
					INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
					INNER JOIN training t ON t.company_id = cp.company_id AND t.training_status_id = 3 AND t.status = 1
					INNER JOIN user_profile_training upt ON upt.training_id = t.training_id AND upt.user_profile_id = up.user_profile_id
					LEFT JOIN tbl_users u ON u.email = cu.email
					LEFT JOIN user_training_tracking utt ON utt.training_id = t.training_id AND utt.user_id = u.id
				WHERE cp.company_id = :company_id
					AND up.user_profile_id = :user_profile_id
				GROUP BY cu.employee_id
				
				UNION ALL
				
				-- Quiz Stats
				SELECT cu.employee_id,
					cu.firstname, cu.lastname, cu.department_name, u.id, MAX(u.lastvisit_at) lastvisit_at,
					0 policy_count, 0 policy_unopened, 0 policy_views, 0 policy_acks,
					0 training_count, 0 training_unopened, 0 training_views, 0 training_completed,
					COUNT(q.quiz_id) quiz_count, 
						COUNT(q.quiz_id) - IFNULL(SUM(qt.viewed), 0) quiz_unopened, 
						IFNULL(SUM(qt.viewed = 1 AND qt.passed != 1), 0) quiz_viewed, 
						IFNULL(SUM(qt.completed = 1), 0) quiz_completed, 
						IFNULL(SUM(qt.passed = 1), 0) quiz_passed
				FROM company cp
					INNER JOIN employee cu ON cu.company_id = cp.company_id
					INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
					INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
					INNER JOIN quiz q ON q.company_id = cp.company_id AND q.quiz_status_id = 3 AND q.status = 1
					INNER JOIN user_profile_quiz upq ON upq.quiz_id = q.quiz_id AND upq.user_profile_id = up.user_profile_id
					LEFT JOIN tbl_users u ON u.email = cu.email
					LEFT JOIN quiz_tracking qt ON qt.quiz_id = q.quiz_id AND qt.user_id = u.id
				WHERE cp.company_id = :company_id
					AND up.user_profile_id = :user_profile_id
				GROUP BY cu.employee_id

			) AS PolicyStats
			GROUP BY employee_id
			ORDER BY firstname, lastname
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $command->bindParam(":user_profile_id", $user_profile_id, PDO::PARAM_INT);
        $data = $command->query()->readAll();
        if (!isset($data)) $data = array();
		if ($withHeader) $data = mGrid::addHeader($header, $data);
        return $data;
    }

	public function ComplianceReport($company_id, $withHeader=false)
	{
		$header = array(
			'profile_title'=>'Profile Name',

			'policy_count'=>'Policy Count',
			'policy_views'=>'Policy Viewed',
			'policy_views_per'=>'%',
			'policy_acks'=>'Policy Acknowledged',
			'policy_acks_per'=>'%',
			'policy_unopened'=>'Policy Unopened',
			'policy_unopened_per'=>'%',

			'training_count'=>'Training Count',
			'training_views'=>'Training Viewed',
			'training_views_per'=>'%',
			'training_completed'=>'Training Completed',
			'training_completed_per'=>'%',
			'training_unopened'=>'Training Unopened',
			'training_unopened_per'=>'%',

			'quiz_count'=>'Quiz Count',
			'quiz_viewed'=>'Quiz Viewed',
			'quiz_viewed_per'=>'%',
			'quiz_completed'=>'Quiz Completed',
			'quiz_completed_per'=>'%',
			'quiz_passed'=>'Quiz Passed',
			'quiz_passed_per'=>'%',
			'quiz_unopened'=>'Quiz Unopened',
			'quiz_unopened_per'=>'%',
		);

		$sql = "
			SELECT user_profile_id, profile_title, --  employee_id,

				SUM(policy_count) policy_count, 
				SUM(policy_unopened) policy_unopened, 
				SUM(policy_views) policy_views, 
				SUM(policy_acks) policy_acks, 
				
				SUM(training_count) training_count, 
				SUM(training_unopened) training_unopened, 
				SUM(training_views) training_views, 
				SUM(training_completed) training_completed, 

				SUM(quiz_count) quiz_count, 
				SUM(quiz_unopened) quiz_unopened, 
				SUM(quiz_viewed) quiz_viewed, 
				SUM(quiz_completed) quiz_completed, 
				SUM(quiz_passed) quiz_passed, 

				IF (SUM(policy_count) > 0, ROUND(SUM(policy_views) / SUM(policy_count) * 100, 0), 0) policy_views_per,
				IF (SUM(policy_count) > 0, ROUND(SUM(policy_acks) / SUM(policy_count) * 100, 0), 0) policy_acks_per,
				IF (SUM(policy_count) > 0, 
					100 - ROUND((SUM(policy_views) + SUM(policy_acks)) / SUM(policy_count) * 100, 0)
				, 0) policy_unopened_per, 

				IF (SUM(training_count) > 0, ROUND(SUM(training_views) / SUM(training_count) * 100, 0), 0) training_views_per,
				IF (SUM(training_count) > 0, ROUND(SUM(training_completed) / SUM(training_count) * 100, 0), 0) training_completed_per,
				IF (SUM(training_count) > 0, 
					100 - ROUND((SUM(training_views) + SUM(training_completed)) / SUM(training_count) * 100, 0)
				, 0) training_unopened_per, 

				IF (SUM(quiz_count) > 0, ROUND(SUM(quiz_viewed) / SUM(quiz_count) * 100, 0), 0) quiz_viewed_per,
				IF (SUM(quiz_count) > 0, ROUND(SUM(quiz_completed) / SUM(quiz_count) * 100, 0), 0) quiz_completed_per,
				IF (SUM(quiz_count) > 0, ROUND(SUM(quiz_passed) / SUM(quiz_count) * 100, 0), 0) quiz_passed_per,
				IF (SUM(quiz_count) > 0, 
					100 - ROUND((SUM(quiz_viewed) + SUM(quiz_completed)) / SUM(quiz_count) * 100, 0)
				, 0) quiz_unopened_per

			FROM (
			
				-- Policy Stats
				SELECT up.user_profile_id, up.profile_title, cu.employee_id,
					COUNT(p.policy_id) policy_count, COUNT(p.policy_id) - IFNULL(SUM(upt.view), 0) policy_unopened, IFNULL(SUM(upt.view = 1 AND upt.ack != 1), 0) policy_views, IFNULL(SUM(upt.ack = 1), 0) policy_acks,
					0 training_count, 0 training_unopened, 0 training_views, 0 training_completed,
					0 quiz_count, 0 quiz_unopened, 0 quiz_viewed, 0 quiz_completed, 0 quiz_passed
				FROM company cp
					INNER JOIN employee cu ON cu.company_id = cp.company_id
					INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
					INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
					INNER JOIN policy p ON p.company_id = cp.company_id AND p.policy_status_id = 3 AND p.status = 1
					INNER JOIN user_profile_policy upp ON upp.policy_id = p.policy_id AND upp.user_profile_id = up.user_profile_id
					LEFT JOIN tbl_users u ON u.email = cu.email
					LEFT JOIN user_policy_tracking upt ON upt.policy_id = p.policy_id AND upt.user_id = u.id
				WHERE cp.company_id = :company_id
				GROUP BY up.user_profile_id
				
				UNION ALL
				
				-- Training Stats
				SELECT up.user_profile_id, up.profile_title, cu.employee_id,
					0 policy_count, 0 policy_unopened, 0 policy_views, 0 policy_acks,
					COUNT(t.training_id) training_count, COUNT(t.training_id) - IFNULL(SUM(utt.view), 0) training_unopened, IFNULL(SUM(utt.view = 1 AND utt.ack != 1), 0) training_views, IFNULL(SUM(utt.ack = 1), 0) training_completed,
					0 quiz_count, 0 quiz_unopened, 0 quiz_viewed, 0 quiz_completed, 0 quiz_passed
				FROM company cp
					INNER JOIN employee cu ON cu.company_id = cp.company_id
					INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
					INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
					INNER JOIN training t ON t.company_id = cp.company_id AND t.training_status_id = 3 AND t.status = 1
					INNER JOIN user_profile_training upt ON upt.training_id = t.training_id AND upt.user_profile_id = up.user_profile_id
					LEFT JOIN tbl_users u ON u.email = cu.email
					LEFT JOIN user_training_tracking utt ON utt.training_id = t.training_id AND utt.user_id = u.id
				WHERE cp.company_id = :company_id
				GROUP BY up.user_profile_id
				
				UNION ALL
				
				-- Quiz Stats
				SELECT up.user_profile_id, up.profile_title, cu.employee_id,
					0 policy_count, 0 policy_unopened, 0 policy_views, 0 policy_acks,
					0 training_count, 0 training_unopened, 0 training_views, 0 training_completed,
					COUNT(q.quiz_id) quiz_count, COUNT(q.quiz_id) - IFNULL(SUM(qt.viewed), 0) quiz_unopened, 
						IFNULL(SUM(qt.viewed = 1 AND qt.passed != 1), 0) quiz_viewed, 
						IFNULL(SUM(qt.completed = 1), 0) quiz_completed, 
						IFNULL(SUM(qt.passed = 1), 0) quiz_passed
				FROM company cp
					INNER JOIN employee cu ON cu.company_id = cp.company_id
					INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
					INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
					INNER JOIN quiz q ON q.company_id = cp.company_id AND q.quiz_status_id = 3 AND q.status = 1
					INNER JOIN user_profile_quiz upq ON upq.quiz_id = q.quiz_id AND upq.user_profile_id = up.user_profile_id
					LEFT JOIN tbl_users u ON u.email = cu.email
					LEFT JOIN quiz_tracking qt ON qt.quiz_id = q.quiz_id AND qt.user_id = u.id
				WHERE cp.company_id = :company_id
				GROUP BY up.user_profile_id
				
			) AS PolicyStats
			GROUP BY user_profile_id
			ORDER BY profile_title
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
		$data = $command->query()->readAll();
		if (!isset($data)) $data = array();
		foreach($data as $key=>$row) {
			$data[$key]['user_profile_id'] = Obscure::encode($data[$key]['user_profile_id']);
		}
		if ($withHeader) $data = mGrid::addHeader($header, $data);
		return $data;
	}

}