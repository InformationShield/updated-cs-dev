<?php
class mGrid
{
	public static function toJsonGrid($data)
	{
		$array = array("status" => "success", "total" => count($data), "records" => $data);
		$data = json_encode($array);
		return $data;
	}

	public static function toGridMessage($status="success", $message=null)
	{
		$array = array("status" => $status, "message" => $message);
		return json_encode($array);
	}

	public static function addHeader($header, $data) {
		$dataWithHeader[] = $header;
		foreach($data as $key=>$row) {
			$sortedRow = array();
			foreach($header as $headerKey=>$column) {
				$sortedRow[$headerKey] = $row[$headerKey];
			}
			$dataWithHeader[] = $sortedRow;
		}
		return  $dataWithHeader;
	}

	public static function calcHeight($rowCount) {
		if ($rowCount > 0) {
			$rowCount++;
			$gridHeight = ($rowCount > 10) ? 24*11 : 24*($rowCount+1);
		} else {
			$gridHeight = 24*2;
		}
		return $gridHeight;
	}
}