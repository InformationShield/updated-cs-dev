<?php

class mReportsTraining
{
    public $header;

    public function ComplianceReportDetail($company_id,$training_id,$asJSON=true,$withHeader=false,$trainingInfo=null)
    {
        $header = array(
            'firstname'=>'First Name',
            'lastname'=>'Last Name',
            'department_name'=>'Department',
            'status'=>'Status',
            'status_name'=>'Status',
            'ack_date'=>'Completed On',
        );
		$sql = "
			SELECT 
				cu.firstname, cu.lastname, cu.department_name,
				CASE
					WHEN utt.ack > 0 then
						2
					WHEN utt.view > 0 then
						1
					ELSE
						0
				END status,
				CASE
					WHEN utt.ack > 0 then
						'Completed'
					WHEN utt.view > 0 then
						'Viewed'
					ELSE
						'' -- 'Unopened'
				END status_name,
				DATE_FORMAT(IF(utt.ack = 0 OR utt.ack_date = '0000-00-00', null, utt.ack_date),'%m/%d/%Y %h:%i %p') ack_date,
				DATE_FORMAT(IF(utt.view = 0 OR utt.view_date = '0000-00-00', null, utt.view_date),'%m/%d/%Y %h:%i %p') view_date
			FROM company cp
				INNER JOIN employee cu ON cu.company_id = cp.company_id
				INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
				INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
				INNER JOIN training t ON t.company_id = cp.company_id AND t.training_status_id = 3 AND t.status = 1
				INNER JOIN user_profile_training upt ON upt.training_id = t.training_id AND upt.user_profile_id = up.user_profile_id
				LEFT JOIN tbl_users u ON u.email = cu.email
				LEFT JOIN user_training_tracking utt ON utt.training_id = t.training_id AND utt.user_id = u.id
			WHERE cp.company_id = :company_id
				AND t.training_id = :training_id
			GROUP BY cu.employee_id
			ORDER BY cu.lastname, cu.firstname
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $command->bindParam(":training_id", $training_id, PDO::PARAM_INT);
        $data = $command->query()->readAll();
        if (!isset($data)) $data = array();
		if ($withHeader) $data = mGrid::addHeader($header, $data);
        if (!empty($trainingInfo)) {
            $details = array();
            $details[] = array('title' => 'Training Compliance Detail Report');
            $details[] = array('training' => $trainingInfo['doc_title']);
            //$details[] = array('description' => strip_tags($trainingInfo['description']));
            $details[] = array('blankline' => '   ');
            $data = array_merge($details,$data);
        }
        if ($asJSON) {
            $array = array("status" => "success", "total" => count($data), "records" => $data);
            $data = json_encode($array);
            //Yii::log(print_r($array,true)."\n\n".$data,'error','mReportsTraining.ComplianceReport');
        }
        return $data;
    }

    public function ComplianceReport($company_id,$asJSON=true,$withHeader=false)
    {
        $header = array(
            'doc_title'=>'Title',
            'description'=>'Description',
            'publish_date'=>'Published',

			'training_count'=>'Training Count',
            'training_completed'=>'Completed',
            'training_completed_per'=>'%',
            'training_unopened'=>'Unopened',
            'training_unopened_per'=>'%',
        );

        $sql = "
			SELECT *,
			IF (training_count > 0, ROUND(training_completed / training_count * 100, 0), 0) training_completed_per,
			IF (training_count > 0, 100 - ROUND(training_completed / training_count * 100, 0), 0) training_unopened_per
			FROM (

				SELECT t.training_id, t.doc_title, t.description, t.publish_date,
				COUNT(t.training_id) training_count, 
				COUNT(t.training_id) - IFNULL(SUM(utt.ack), 0) training_unopened, 
				IFNULL(SUM(utt.ack = 1), 0) training_completed
				FROM company cp
				INNER JOIN employee cu ON cu.company_id = cp.company_id
				INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
				INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
				INNER JOIN training t ON t.company_id = cp.company_id AND t.training_status_id = 3 AND t.status = 1
				INNER JOIN user_profile_training upt ON upt.training_id = t.training_id AND upt.user_profile_id = up.user_profile_id
				LEFT JOIN tbl_users u ON u.email = cu.email
				LEFT JOIN user_training_tracking utt ON utt.training_id = t.training_id AND utt.user_id = u.id
				WHERE cp.company_id = :company_id
				GROUP BY t.training_id
			) AS TrainingStats
			ORDER BY doc_title
		";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $data = $command->query()->readAll();
        if (!isset($data)) $data = array();
        foreach($data as $key=>$row) {
            if ($asJSON) {
                $data[$key]['training_id'] = Obscure::encode($data[$key]['training_id']);
            }
        }
		if ($withHeader) $data = mGrid::addHeader($header, $data);
        if ($asJSON) {
            $array = array("status" => "success", "total" => count($data), "records" => $data);
            $data = json_encode($array);
            //Yii::log(print_r($array,true)."\n\n".$data,'error','mReportsTraining.ComplianceReport');
        }
        return $data;
    }
}