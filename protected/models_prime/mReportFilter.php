<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class mReportFilter extends ControlFilter
{
	public $id;

	public $total_controls;
	public $total_complete;
	public $control_status_sums = array();
	public $control_summary_json;

	public $control_status_name;
	public $category_scores_json;
	public $total_score;
	public $max_score;

	private $defaultFilter = array(
		'control_filter_id'=>'1', // use id of 1 for the default filter
		'filter_name'=>'Comprehensive',
		'filter_desc'=>'Includes all the current baseline controls',
		'updated_on'=>null,
	);


	public function rules()
    {
		return CMap::mergeArray(
			parent::rules(),
			array(
				array('id', 'safe')
			)
		);
    }

    public function __construct($filter_id=null) {
    	if ($filter_id) {
    		$this->getFilter($filter_id);
		}
    }

    public function getList($company_id)
    {
    	$filterList = ControlFilter_Ex::GetFiltersByCompany($company_id);
		array_unshift($filterList, $this->defaultFilter);
		$filterList[0]['recid'] = obscure($filterList[0]['control_filter_id']);
    	return $filterList;
    }

	public function getFilter($filter_id)
	{
		$filter = ControlFilter_Ex::GetFilter($filter_id);
		if ($filter) {
			unset($filter['control_filter_id']);
			$this->attributes = $filter;
			$this->control_filter_id = obscure($filter_id);
		} else {
			$defaultFilter = $this->defaultFilter;
			unset($defaultFilter['control_filter_id']);
			$this->attributes = $defaultFilter;
			$this->control_filter_id = obscure($this->defaultFilter['control_filter_id']);
		}
		return $filter;
	}

	public function createFilter($filter_id, $filter_data)
	{
		$retVal = null;

		$this->attributes = $filter_data;
		$this->id = $filter_id;
		$this->control_filter_id = $filter_id;
		if ($this->validate()) {
			$this->isNewRecord = true;
			if ($this->save()) {
				$this->id = $this->control_filter_id;
				$retVal = true;
			}
		}

		return $retVal;
	}

	public function saveFilter($filter_id, $filter_data)
	{
		$retVal = null;

		if ($filter_id) {
			$filter = ControlFilter::model()->findByPk($filter_id);
		} else {
			$filter = new ControlFilter();
		}
		$filter->attributes = $filter_data;
		$filter->company_id = Utils::contextCompanyId();
		$filter->control_filter_id = $filter_id;
		if ($filter->validate()) {
			if ($filter_id) {
				$this->id = $filter_id;
				$filter->isNewRecord = false;
			} else {
				$filter->isNewRecord = true;
			}
			if ($x=$filter->save()) {
				$this->id = $filter->control_filter_id;
				$retVal = true;
			}
		}

		return $retVal;
	}

	public function getSummaryData($company_id, $baseline_id, $filter_id) {
		$filter = self::buildSearchFilter($filter_id);
		$this->getControlSummary($company_id, $baseline_id, $filter_id, $filter);
	}

	public function getDetailData($company_id, $baseline_id, $control_status_id, $filter_id) {
		$filter = self::buildSearchFilter($filter_id);
		$this->getRiskScoring($company_id, $baseline_id, $control_status_id, $filter_id, $filter);
	}

	public function getControlSummary($company_id, $baseline_id, $filter_id, $filter)
	{
		$colors = array();
		$colors[0] = 'FF0000'; //red
		$colors[1] = 'FFFF00'; // yellow
		$colors[2] = 'FFBF00'; // orange
		$colors[3] = '00BFFF'; // light blue
		$colors[4] = '0000FF'; // blue
		$colors[5] = '00FF00'; // green
		$this->total_controls = 0;
		$this->total_complete = 0;
		$sums = array();
		$this->control_status_sums = ControlBaseline_Ex::getControlStatusByFilter($company_id, $baseline_id, $filter);
		foreach ($this->control_status_sums as $row) {
			$sums[] =  array(
				'label'=>$row['control_status_name'],
				'value'=>$row['total'],
				'link'=> url('reportfilter/detailchart',
					array('id'=>obscure($filter_id), 'status'=>$row['control_status_id'])),
				'color'=> $colors[$row['control_status_id']],
				'labelFontColor'=> '000000'
			);
			if ($row['control_status_id'] == 5) $this->total_complete = $row['total'];
			$this->total_controls += $row['total'];
		}
		$this->control_summary_json = json_encode($sums);
		//Yii::log(print_r($sums,true)."\n\n".$this->control_summary_json,'error','mScoring.getControlSummary');
	}

	public function getRiskScoring($companyId, $baseline_id=null, $control_status_id=null, $filter_id, $filter)
	{
		if (!is_null($control_status_id)) {
			$this->control_status_name = ControlStatus_Ex::getStatusName($control_status_id);
		}
		$maxScore = ControlStatus_Ex::getMaxScore();
		$rows = ControlCategory_Ex::getCategory(null,null,"SELECT control_category_id as id, parent_id, category, 0 as score, 0 as maxscore");
		$nodes = ControlCategory_Ex::buildTree($rows);
		$scores = ControlBaseline_Ex::getControlScoresByFilter($companyId, $baseline_id, $filter);
		//Yii::log(print_r($scores,true)."\n",'error','mScoring.getRiskScoring');

		foreach($scores as $score) {
			$id = $score['cat_id'];
			if (!is_null($control_status_id)) {
				$score['compareControlStatusId'] = $control_status_id;
				$foundIt = ControlCategory_Ex::findRootOfNodeInTree($nodes, $id, $rootNode, function (&$node, &$data) {
					if ($data['compareControlStatusId'] == $data['control_status_id']) {
						$node['score'] += 1; // only count those matching status we looking for
					}
					$node['maxscore'] += 1; // total number of controls
				}, $score);
			} else {
				$score['maximumScore'] = $maxScore;
				$foundIt = ControlCategory_Ex::findRootOfNodeInTree($nodes, $id, $rootNode, function (&$node, &$data) {
					$node['score'] += $data['score'];
					$node['maxscore'] += $data['weighting'] * $data['maximumScore'];
				}, $score);
			}
			if (!$foundIt) {
				Yii::log('Failed to find root node for cat_id '.$score['cat_id'],'error','mScoring.getRiskScoring');
			}
		}
		//Yii::log(print_r($nodes,true)."\n",'error','mScoring.getRiskScoring');

		$this->total_score = 0;
		$this->max_score = 0;
		$category_scores = array();
		foreach ($nodes as $catNode)
		{
			if ($catNode['maxscore'] > 0) {
				$series_value = ($catNode['score'] / $catNode['maxscore']) * 100;
				$displayValue = ''.$catNode['score']."/".$catNode['maxscore'];
				if ($series_value < 25) {
					$color = 'FF0000';
				} else if ($series_value < 75) {
					$color = 'FFFF00';
				} else {
					$color = '00FF00';
				}
			} else {
				$series_value = 0;
				$displayValue = '0';
				$color = 'FF0000';
			}
			$this->total_score += $catNode['score'];
			$this->max_score += $catNode['maxscore'];
			$category_scores[] = array(
				'label' => $catNode['category'],
				'value' => $series_value,
				'link'=> url('controlbaseline/list',
					array('status'=>$control_status_id, 'fid'=>obscure($filter_id), 'rid'=>obscure($catNode['id']))),
				'displayValue' => $displayValue,
				'color' => $color,
				'labelFontColor'=> '000000'
			);
		}

		$this->category_scores_json = json_encode($category_scores);
		//Yii::log(print_r($category_scores,true)."\n\n".$this->category_scores_json,'error','mScoring.getRiskScoring');
	}

	public static function buildSearchFilter($filter_id) {
		$filter = array(
			'$cmd'=>'get-records', 'pageSize'=>3000, 'offsetBy'=>0,
			'search'=>array(),
			'searchLogic'=>'AND',
			'sort'=>array(),
		);

		$controlFilter = ControlFilter_Ex::GetFilterSearchFields($filter_id);
		if ($controlFilter) {
			foreach($controlFilter as $searchKey=>$searchValue) {
				if (trim($searchValue) !== '') {
					$search = array(
						'field'=>$searchKey,
						'type'=>'text',
						'operator'=>'contains',
						'value'=>$searchValue,
					);
					if ($searchKey == 'control_status_id') {
						$search['type'] = 'int';
						$search['operator'] = 'is';
					}
					$filter['search'][] = $search;
				}
			}
		}
		return $filter;
	}

}