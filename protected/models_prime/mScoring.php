<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class mScoring extends CFormModel
{
    // Risk Scoring Report
    public $total_score = 0;
    public $max_score = 0;
    public $category_scores_json;
    public $control_status_name;
    
    // Control Summary report
    public $control_status_sums;
    public $total_complete = 0;
    public $total_controls = 0;
    public $control_summary_json;

    // Security Role report
    public $role_status_sums;
    public $total_assigned = 0;
    public $total_roles = 0;
    public $role_summary_json;

    // Policy Summary report
    public $policy_status_sums;
    public $total_published = 0;
    public $total_policy = 0;
    public $policy_summary_json;

    // Policy Tracking report
    public $policy_tracking_status_sums;
    public $total_acknowledged = 0;
    public $total_viewed = 0;
    public $total_notviewed = 0;
    public $policy_tracking_summary_json;

    public function rules()
    {
        return array(
            array('total_score, max_score', 'safe'),
        );
    }

    public function __construct() {
        $this->total_score = 0;
        $this->max_score = 0;
    }

    public function getCompaniesRiskScoringJson(&$companies)
    {
        $data = array();
        foreach($companies as $key=>&$company) {
            unset($companies[$key]['company']);
            unset($companies[$key]['employee']);
            $baseline = Baseline_Ex::GetDefaultBaseline($company['company_id']);
            $companies[$key]['baseline_id'] = Obscure::encode($baseline->baseline_id);
            $companies[$key]['baseline_name'] = $baseline->baseline_name;
            $this->getRiskScoring($company['company_id'],$baseline->baseline_id);
            $companies[$key]['risk_score_total'] = $this->total_score;
            $companies[$key]['risk_score_max'] = $this->max_score;
            $risk_score = 0;
            if ($this->max_score > 0) {
                $risk_score = ceil(($this->total_score/$this->max_score)*100);
            }
            $companies[$key]['risk_score'] = $risk_score;

            // calculate inherent risk score for the company
            $model = CompanyInfo::model()->findByAttributes(array('company_id' => $company['company_id']));
            if (isset($model)) {
                $wizardInherentRiskScore = Wizard_Ex::calculateWizardInherentRisk($model->attributes);
                $companies[$key]['inherent_risk_score'] = $wizardInherentRiskScore['total_score'];
                $companies[$key]['inherent_risk_score_max'] = $wizardInherentRiskScore['max_score'];
                $inherent_risk_score_percent = 0;
                if ($this->max_score > 0) {
                    $inherent_risk_score_percent = ceil(($wizardInherentRiskScore['total_score'] / $wizardInherentRiskScore['max_score']) * 100);
                }
                $companies[$key]['inherent_risk_score_percent'] = $inherent_risk_score_percent;
            } else {
                $companies[$key]['inherent_risk_score'] = 0;
                $companies[$key]['inherent_risk_score_max'] = 0;
                $companies[$key]['inherent_risk_score_percent'] = 0;
            }

            $companies[$key]['company_id'] = Obscure::encode($company['company_id']);
            $data[] = $companies[$key];
            $this->total_score = 0;
            $this->max_score = 0;
        }
        $array = array("status"=>"success", "total"=>count($data), "records" => $data);
        $json = json_encode($array);
        //Yii::log(print_r($array,true)."\n\n".$json,'error','mScoring.getCompaniesRiskScoringJson');
        return $json;
    }

    public function getRiskScoring($companyId,$baseline_id=null,$control_status_id=null)
    {
        if (!is_null($control_status_id)) {
            $this->control_status_name = ControlStatus_Ex::getStatusName($control_status_id);
        }
        $maxScore = ControlStatus_Ex::getMaxScore();
        $rows = ControlCategory_Ex::getCategory(null,null,"SELECT control_category_id as id, parent_id, category, 0 as score, 0 as maxscore");
        $nodes = ControlCategory_Ex::buildTree($rows);
        $scores = ControlBaseline_Ex::getBaselineControlScores($companyId,$baseline_id);
        //Yii::log(print_r($scores,true)."\n",'error','mScoring.getRiskScoring');

        foreach($scores as $score) {
            $id = $score['cat_id'];
            if (!is_null($control_status_id)) {
                $score['compareControlStatusId'] = $control_status_id;
                $foundIt = ControlCategory_Ex::findRootOfNodeInTree($nodes, $id, $rootNode, function (&$node, &$data) {
                    if ($data['compareControlStatusId'] == $data['control_status_id']) {
                        $node['score'] += 1; // only count those matching status we looking for
                    }
                    $node['maxscore'] += 1; // total number of controls
                }, $score);
            } else {
                $score['maximumScore'] = $maxScore;
                $foundIt = ControlCategory_Ex::findRootOfNodeInTree($nodes, $id, $rootNode, function (&$node, &$data) {
                    $node['score'] += $data['score'];
                    $node['maxscore'] += $data['weighting'] * $data['maximumScore'];
                }, $score);
            }
            if (!$foundIt) {
                Yii::log('Failed to find root node for cat_id '.$score['cat_id'],'error','mScoring.getRiskScoring');
            }
        }
        //Yii::log(print_r($nodes,true)."\n",'error','mScoring.getRiskScoring');

        $this->total_score = 0;
        $this->max_score = 0;
        $category_scores = array();
        foreach ($nodes as $catNode)
        {
            if ($catNode['maxscore'] > 0) {
                $series_value = ($catNode['score'] / $catNode['maxscore']) * 100;
                $displayValue = ''.$catNode['score']."/".$catNode['maxscore'];
                if ($series_value < 25) {
                    $color = 'FF0000';
                } else if ($series_value < 75) {
                    $color = 'FFFF00';
                } else {
                    $color = '00FF00';
                }
            } else {
                $series_value = 0;
                $displayValue = '0';
                $color = 'FF0000';
            }
            $this->total_score += $catNode['score'];
            $this->max_score += $catNode['maxscore'];
            $category_scores[] = array(
                'label' => $catNode['category'],
                'value' => $series_value,
                'displayValue' => $displayValue,
                'color' => $color,
                'labelFontColor'=> '000000'
            );
        }

        $this->category_scores_json = json_encode($category_scores);
        //Yii::log(print_r($category_scores,true)."\n\n".$this->category_scores_json,'error','mScoring.getRiskScoring');
    }

    public function getRoleSummary($companyId)
    {
        $colors = array();
        $colors[0] = 'FF0000'; //red
        $colors[1] = '00FF00'; // green
        $colors[2] = '0000FF'; // blue
        $this->total_roles = 0;
        $this->total_assigned = 0;
        $sums = array();
        $this->role_status_sums = SecurityRole_Ex::getRoleStatusCounts($companyId);
        foreach($this->role_status_sums as $row) {
            $sums[] =  array(
                'label'=>$row['role_status_name'],
                'value'=>$row['total'],
                'link'=> url('securityrole/list',array('status'=>$row['role_status_id'])),
                'color'=> $colors[$row['role_status_id']],
                'labelFontColor'=> '000000'
            );
            if ($row['role_status_id'] == 1) $this->total_assigned = $row['total'];
            $this->total_roles += $row['total'];
        }
        $this->role_summary_json = json_encode($sums);
    }

    public function getControlSummaryOld($companyId,$baseline_id=null)
    {
        $colors = array();
        $colors[0] = 'FF0000'; //red
        $colors[1] = 'FFFF00'; // yellow
        $colors[2] = 'FFBF00'; // orange
        $colors[3] = '00BFFF'; // light blue
        $colors[4] = '0000FF'; // blue
        $colors[5] = '00FF00'; // green
        $this->total_controls = 0;
        $this->total_complete = 0;
        $sums = array();
        $this->control_status_sums = ControlBaseline_Ex::getControlStatusCounts($companyId,$baseline_id);
        foreach($this->control_status_sums as $row) {
            $sums[] =  array(
                'label'=>$row['control_status_name'],
                'value'=>$row['total'],
                'link'=> url('controlbaseline/list',array('status'=>$row['control_status_id'])),
                'color'=> $colors[$row['control_status_id']],
                'labelFontColor'=> '000000'
            );
            if ($row['control_status_id'] == 5) $this->total_complete = $row['total'];
            $this->total_controls += $row['total'];
        }
        $this->control_summary_json = json_encode($sums);
        //Yii::log(print_r($sums,true)."\n\n".$this->control_summary_json,'error','mScoring.getControlSummary');
    }

	public function getControlSummary($companyId,$baseline_id=null)
	{
		$colors = array();
		$colors[0] = 'FF0000'; //red
		$colors[1] = 'FFFF00'; // yellow
		$colors[2] = 'FFBF00'; // orange
		$colors[3] = '00BFFF'; // light blue
		$colors[4] = '0000FF'; // blue
		$colors[5] = '00FF00'; // green
		$this->total_controls = 0;
		$this->total_complete = 0;
		$sums = array();
		$this->control_status_sums = ControlBaseline_Ex::getControlStatusCounts($companyId,$baseline_id);
		foreach($this->control_status_sums as $row) {
			$sums[] =  array(
				'label'=>$row['control_status_name'],
				'value'=>$row['total'],
				'link'=> url('compliance/detailchart',array('status'=>$row['control_status_id'])),
				'color'=> $colors[$row['control_status_id']],
				'labelFontColor'=> '000000'
			);
			if ($row['control_status_id'] == 5) $this->total_complete = $row['total'];
			$this->total_controls += $row['total'];
		}
		$this->control_summary_json = json_encode($sums);
		//Yii::log(print_r($sums,true)."\n\n".$this->control_summary_json,'error','mScoring.getControlSummary');
	}

	public function getPolicySummary($companyId)
    {
        $colors = array();
        $colors[0] = 'FF0000'; //red - draft
        $colors[1] = 'FFFF00'; // yellow - review
        $colors[2] = '00FF00'; // green - published
        $colors[3] = '00BFFF'; // light blue - archived
        $this->total_policy = 0;
        $this->total_published = 0;
        $sums = array();
        $this->policy_status_sums = Policy_Ex::getPolicyStatusCounts($companyId);
        foreach($this->policy_status_sums as $row) {
            $sums[] =  array(
                'label'=>$row['policy_status_name'],
                'value'=>$row['total'],
                'link'=> url('policy/list',array('status'=>$row['policy_status_id'])),
                'color'=> $colors[$row['policy_status_id'] - 1],
                'labelFontColor'=> '000000'
            );
            if ($row['policy_status_id'] == 3) $this->total_published = $row['total'];
            $this->total_policy += $row['total'];
        }
        $this->policy_summary_json = json_encode($sums);
    }

    public function getPolicyTrackingSummary($companyId)
    {
        $colors = array();
        $colors[0] = 'FF0000'; //red - draft
        $colors[1] = 'FFFF00'; // yellow - review
        $colors[2] = '00FF00'; // green - published
        $this->total_acknowledged = 0;
        $this->total_viewed = 0;
        $this->total_notviewed = 0;
        $sums = array();
        $this->policy_tracking_status_sums = UserPolicyTracking_Ex::getPolicyTrackingStatusCounts($companyId);
        $url = url('policy/list');
        $total = 0;
        foreach($this->policy_tracking_status_sums as $row) {
            $color = false;
            $total += $row['total'];
            if ($row['status'] == 0) {
                $this->total_notviewed = $row['total'];
                $color = $colors[0];
            } else if ($row['status'] == 1) {
                $this->total_viewed = $row['total'];
                $color = $colors[1];
            } else if ($row['status'] == 2) {
                $this->total_acknowledged = $row['total'];
                $color = $colors[2];
            }
            if ($color !== false) {
                $sums[] = array(
                    'label' => $row['label'],
                    'value' => $row['total'],
                    'link' => $url,
                    'color' => $color,
                    'labelFontColor'=> '000000'
                );
            }
        }
        if ($total == 0) {
            $this->policy_tracking_status_sums = null;
            $this->policy_tracking_summary_json = json_encode(array());
        } else {
            $this->policy_tracking_summary_json = json_encode($sums);
        }
        //Yii::log($this->policy_tracking_summary_json,'error','getPolicyTrackingSummary');
    }
}