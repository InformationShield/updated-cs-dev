<?php

/**
 * This is the model class for table "filter".
 *
 * The followings are the available columns in table 'filter':
 * @property string $FilterId
 * @property string $UserId
 * @property string $FilterType
 * @property string $FilterName
 * @property string $DerivedFrom
 * @property string $QuickSearch
 * @property string $SearchBy
 * @property string $SortBy
 * @property string $GroupBy
 * @property integer $ShowGroup
 */
class Filter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'filter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UserId, FilterType, FilterName', 'required'),
			array('ShowGroup', 'numerical', 'integerOnly'=>true),
			array('UserId', 'length', 'max'=>20),
			array('FilterType', 'length', 'max'=>32),
			array('FilterName, DerivedFrom', 'length', 'max'=>64),
			array('QuickSearch, SearchBy, SortBy, GroupBy', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('FilterId, UserId, FilterType, FilterName, DerivedFrom, QuickSearch, SearchBy, SortBy, GroupBy, ShowGroup', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'FilterId' => 'Filter',
			'UserId' => 'User',
			'FilterType' => 'Filter Type',
			'FilterName' => 'Filter Name',
			'DerivedFrom' => 'Derived From',
			'QuickSearch' => 'Quick Search',
			'SearchBy' => 'Search By',
			'SortBy' => 'Sort By',
			'GroupBy' => 'Group By',
			'ShowGroup' => 'Show Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('FilterId',$this->FilterId,true);
		$criteria->compare('UserId',$this->UserId,true);
		$criteria->compare('FilterType',$this->FilterType,true);
		$criteria->compare('FilterName',$this->FilterName,true);
		$criteria->compare('DerivedFrom',$this->DerivedFrom,true);
		$criteria->compare('QuickSearch',$this->QuickSearch,true);
		$criteria->compare('SearchBy',$this->SearchBy,true);
		$criteria->compare('SortBy',$this->SortBy,true);
		$criteria->compare('GroupBy',$this->GroupBy,true);
		$criteria->compare('ShowGroup',$this->ShowGroup);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Filter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
