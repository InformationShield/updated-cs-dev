<?php

class mMenus
{
	public static function getMenus()
	{
		$menus = array(
			'title' => 'Compliance Shield',
			'title_url' => Yii::app()->createAbsoluteUrl('site/home'),
			'sub_title' => null,
			'logo'=> url('images/logo.png'),
			'logo_sm'=> url('images/logo-sm.png'),
			'company_logo'=> mCompanyContext::GetLogo(),
			'top_menu' => array(),
			'side_menu' => array(),
		);

		if (getSessionVar('site_type') == 'AdminSite' && Utils::isSuper()) {
			$menus['sub_title'] = 'Administration';
			$menus['logo'] = url('images/logoadmin.png');
			$menus['logo_sm'] = url('images/logoadmin-sm.png');
			$menus['top_menu'] = self::$topMenusAdmin;
			$menus['side_menu'] = self::$sideMenusAdmin;
		} else if (getSessionVar('site_type') == 'CompanySite') {
			$menus['sub_title'] = getSessionVar('context_company_name','Welcome');
			$menus['top_menu'] = self::$topMenusCompany;
			$menus['side_menu'] = self::getSideMenu();
			if (!Utils::isSuper()) { // only show admin menu for super user
				unset($menus['top_menu']['Account']['children']['Admin']);
			}
		} else if (getSessionVar('site_type') == 'UserSite') {
			$menus['sub_title'] = getSessionVar('context_company_name','Welcome');
			$menus['top_menu'] = self::$topMenusUser;
			$menus['side_menu'] = self::getSideMenu();
		} else if (getSessionVar('site_type') == 'InitSite') {
			$menus['sub_title'] = 'Account Options';
			$menus['logo'] = url('images/logoadmin.png');
			$menus['logo_sm'] = url('images/logoadmin-sm.png');
			$menus['top_menu'] = array();
			$menus['side_menu'] = self::$sideMenusInit;
		} else { // assume guest
			$menus['sub_title'] = 'Welcome';
			$menus['top_menu'] = self::$topMenusGuest;
			$menus['side_menu'] = self::$sideMenusGuest;
		}

		return $menus;
	}

	// for company site menu and user menu, check if license is valid and user role has access
	public static function getSideMenu() {
		$sideMenu = array();

		if (getSessionVar('site_type') == 'CompanySite') {
			$sideMenu = self::$sideMenusCompany;
			foreach (self::$sideMenusCompany as $menuGroupName => $groupMenu) {
				$sideMenu[$menuGroupName]['active'] = mPermission::hasAccess($menuGroupName, null) === true;
				if ($groupMenu['children']) {
					foreach ($groupMenu['children'] as $menuName => $menuUrl) {  // check permission on all children menu items
						$sideMenu[$menuGroupName]['children'][$menuName] = [
							'url' => $menuUrl,
							'active' => mPermission::hasAccess($menuGroupName, $menuName) === true,
						];
						if ($sideMenu[$menuGroupName]['children'][$menuName]['url'] == "site/registercompany"
                                && !$sideMenu[$menuGroupName]['children'][$menuName]['active']) {
                            $sideMenu[$menuGroupName]['children'][$menuName]['hide'] = true;
                        }
					}
				}
			}
		} else if (getSessionVar('site_type') == 'UserSite') {
			$sideMenu = self::$sideMenusUser;
			foreach (self::$sideMenusUser as $menuName => $menu) {	// check permission on all user menu items
				if (mPermission::hasAccess(null, $menuName) === true) {
					$sideMenu[$menuName]['active'] = true;
				} else {
					$sideMenu[$menuName]['active'] = false;
					$sideMenu[$menuName]['hide'] = true;
				}
			}
		}

		return $sideMenu;
	}

	public static $topMenusAdmin = [
		'Account' => [
			'url' => '#',
			'active' => true,
			'children' => [
				'Account Info' => [
					'url' => '/user/profile',
					'active' => true,
				],
				'Account Options' => [
					'url' => '/site/accountoptions',
					'active' => true,
				],
				"---" => [
					'url'=>'',
					'active'=>true,
				],
				'Exit Admin' => [
					'url' => '/site/CompanyHome',
					'active'=>true,
				],
				'Log Out' => [
					'url' => '/user/logout',
					'active'=>true,
				],
			],
		],
	];

	public static $topMenusCompany = [
		'Account' => [
			'url' => '#',
			'active' => true,
			'children' => [
				'Account Info' => [
					'url' => '/user/profile',
					'active' => true,
				],
				'Account Options' => [
					'url' => '/site/accountoptions',
					'active' => true,
				],
				"---" => [
					'url'=>'',
					'active'=>true,
				],
				'Admin' => [
					'url' => '/site/AdminHome',
					'active'=>true,
				],
				'Log Out' => [
					'url' => '/user/logout',
					'active'=>true,
				],
			],
		],
	];

	public static $topMenusUser = [
	];

	public static $topMenusGuest = [
		'Login' => [
			'url' => '/user/login',
			'active' => true,
		],
		'Register' => [
			'url' => '/user/registration',
			'active' => true,
		]
	];

	public static $sideMenusAdmin = [
		"Companies & Owners" => [
			"icon" => 'fa-building',
			"children" => null,
			"url" => 'admin/company',
			'active' => true,
		],
		"Manage User Accounts" => [
			"icon" => 'fa-male',
			"children" => null,
			"url" => 'user',
			'active' => true,
		],
		"Manage Employees" => [
			"icon" => 'fa-users',
			"children" => null,
			"url" => 'user/employee',
			'active' => true,
		],
		"Company Info" => [
			"icon" => 'fa-info',
			"children" => null,
			"url" => 'admin/companyinfo/admin',
			'active' => true,
		],
		"Control Category" => [
			"icon" => 'fa-cube',
			"children" => null,
			"url" => 'admin/controlcategory/admin',
			'active' => true,
		],
		"Control Library" => [
			"icon" => 'fa-cubes',
			"children" => null,
			"url" => 'admin/controllibrary/admin',
			'active' => true,
		],
		"Post Message" => [
			"icon" => 'fa-envelope',
			"children" => null,
			"url" => 'admin/postmessage',
			'active' => true,
		],
		"Exit Admin" => [
			"icon" => 'fa-sign-in',
			"children" => null,
			"url" => 'site/companyhome',
			'active' => true,
		],
	];

	public static $sideMenusCompany = [
		"Company" => [
			"icon" => 'fa-building',
			"url" => null,
			"role" => Cons::ROLE_AUTHOR,
			"children" => [
				"Dashboard" => 'site/CompanyHome',
				"Company Info" => 'site/company',
				"Register New Company" => 'site/registercompany',
				"User Profiles" => 'userprofile/list',
				"Users" => 'employee/list',
				"Import Users" => 'employee/import',
				"Security Roles" => 'securityrole/list',
				"Companies Risk Scores" => 'compliance/companies',
				"Invites" => 'invite/list',
			]
		],
		"Compliance" => [
			"icon" => 'fa-institution',
			"url" => null,
			"children" => [
				"Control Wizard" => 'wizard/control',
				"Cyber Inherent Risk" => 'wizard/inherentriskscore',
//				"Cyber Maturity Risk" => 'cyberriskscore',
				"Risk Scoring Report" => 'compliance/riskscoring',
//				"Control Summary" => 'compliance/controlsummary',
//				"Roles Summary" => 'compliance/roles',
//				"Policy Summary" => 'compliance/policysummary',
//				"Policy Tracking" => 'compliance/policytrackingsummary',
				"Manage Baselines" => 'baseline/list',
				"Control Baseline" => 'controlbaseline/list',
				"Control Library" => 'library/list',
				"Evidence" => 'evidence/list',
				"Tasks" => 'task/list',
			]
		],
		"Reports" => [
			"icon" => 'fa-pie-chart',
			"url" => null,
			"children" => [
				"Situation Report" => 'reportsituation',
				"User Compliance" => 'reportuser/usercompliance',
				"Profile Compliance" => 'reportprofile/compliance',
				"Policy Compliance" => 'reportpolicy/compliance',
				"Training Compliance" => 'reporttraining/compliance',
				"Quiz Compliance" => 'reportquiz/compliance',
				"Control Compliance" => 'reportfilter/controlsummary',
			]
		],
		"Policy Documents" => [
			"icon" => 'fa-shield',
			"url" => null,
			"children" => [
				"Policies" => 'policy/list',
				"Policy Library" => 'policylibrary/list',
			]
		],
		"Training" => [
			"icon" => 'fa-graduation-cap',
			"url" => null,
			"children" => [
				"Manage Training" => 'training/list',
				"Training Library" => 'traininglibrary/list',
				"Manage Videos" => 'videotraining/list',
				"Watch Videos" => 'videotraining/watchseries',
			]
		],
		"Quizzes" => [
			"icon" => 'fa-question-circle',
			"url" => null,
			"children" => [
				"Quizzes" => 'quiz/list',
				"Quiz Library" => 'quizlibrary/list',
			]
		],
		"Incidents" => [
			"icon" => 'fa-bell',
			"url" => null,
			"children" => [
				"Manage Incidents" => 'incident/list',
				"Manage Incident Types" => 'incidenttype/list',
			]
		],
		"Go to User Site" => [
			"icon" => 'fa-user',
			"url" => '/site/UserHome',
			"children" => null,
		],
		"Log Out" => [
			"icon" => 'fa-sign-out',
			"url" => '/user/logout',
			"children" => null,
		],
	];

	public static $sideMenusUser = [
		"My Inbox" => [
			"icon" => 'fa-inbox',
			"url" => 'my/inbox',
			"role" => Cons::ROLE_USER,
			"children" => null,
		],
		"Policies" => [
			"icon" => 'fa-file-text-o',
			"url" => 'my/policies',
			"role" => Cons::ROLE_USER,
			"children" => null,
		],
		"Training" => [
			"icon" => 'fa-graduation-cap',
			"url" => 'my/training',
			"role" => Cons::ROLE_USER,
			"children" => null,
		],
		"Quizzes" => [
			"icon" => 'fa-question-circle',
			"url" => 'my/quiz',
			"role" => Cons::ROLE_USER,
			"children" => null,
		],
		"My Tasks" => [
			"icon" => 'fa-tasks',
			"url" => 'my/tasks',
			"role" => Cons::ROLE_USER,
			"children" => null,
		],
		"My Incidents" => [
			"icon" => 'fa-bell',
			"url" => 'myincident/list',
			"role" => Cons::ROLE_USER,
			"children" => null,
		],
		"My Info" => [
			"icon" => 'fa-info-circle',
			"url" => 'my/info',
			"role" => Cons::ROLE_USER,
			"children" => null,
		],
		"Account Info" => [
			"icon" => 'fa-user',
			"url" => 'user/profile',
			"role" => Cons::ROLE_USER,
			"children" => null,
		],
//		"Account Options" => [
//			"icon" => 'fa-wrench',
//			"url" => 'site/AccountOptions',
//			"children" => null,
//		],
		"Go to Company Site" => [
			"icon" => 'fa-building',
			"url" => '/site/CompanyHome',
			"role" => Cons::ROLE_AUTHOR,
			"children" => null,
		],
		"Log Out" => [
			"icon" => 'fa-sign-out',
			"url" => '/user/logout',
			"role" => Cons::ROLE_USER,
			"children" => null,
		],
	];

	public static $sideMenusInit = [
		"Account Info" => [
			"icon" => 'fa-user',
			"url" => 'user/profile',
			"children" => null,
			"active" => true,
		],
		"Account Options" => [
			"icon" => 'fa-wrench',
			"url" => 'site/AccountOptions',
			"children" => null,
			"active" => true,
		],
		"Log Out" => [
			"icon" => 'fa-sign-out',
			"url" => '/user/logout',
			"children" => null,
			"active" => true,
		],
	];

	public static $sideMenusGuest = [
		"Login" => [
			"icon" => 'fa-inbox',
			"children" => null,
			'active' => true,
			"url" => 'user/login',
		],
		"Register" => [
			"icon" => 'fa-file-text-o',
			"url" => 'user/registration',
			'active' => true,
			"children" => null,
		],
		"Contact" => [
			"icon" => 'fa-graduation-cap',
			"url" => 'site/contact',
			'active' => true,
			"children" => null,
		],
		"Privacy Policy" => [
			"icon" => 'fa-question-circle',
			"url" => 'site/page/view/privacypolicy',
			'active' => true,
			"children" => null,
		],
	];

    public static $acl = [
		Cons::LIC_NONE => [
			"Company.Register New Company",
		],
		Cons::LIC_FREE => [
			"Company",
			"Company.Dashboard",
			"Company.Company Info",
//			"Company.Register New Company",
//			"Company.User Profiles",
//			"Company.Users",
//			"Company.Import Users",
//			"Company.Security Roles",
//			"Company.Companies Risk Scores",
//			"Company.Invites",

			"Compliance",
			"Compliance.Control Wizard",
			"Compliance.Cyber Inherent Risk",
//			"Compliance.Cyber Maturity Risk",
			"Compliance.Risk Scoring Report",
			"Compliance.Control Summary",
			"Compliance.Roles Summary",
			"Compliance.Policy Summary",
			"Compliance.Policy Tracking",
//			"Compliance.Manage Baselines",
//			"Compliance.Control Baseline",
//			"Compliance.Control Library",
//			"Compliance.Evidence",
//			"Compliance.Tasks",

			"Reports",
			"Reports.Situation Report",
//			"Reports.User Compliance",
//			"Reports.Profile Compliance",
//			"Reports.Policy Compliance",
//			"Reports.Training Compliance",
//			"Reports.Quiz Compliance",
//			"Reports.Control Compliance",

//			"Policy Documents.Policies",
//			"Policy Documents.Policy Library",

//			"Training.Manage Training",
//			"Training.Training Library",
//			"Training.Manage Videos",
//			"Training.Watch Videos",

//			"Quizzes.Quizzes",
//			"Quizzes.Quiz Library",

//			"Incidents.Manage Incidents",
//			"Incidents.Manage Incident Types",
//			"Incidents.My Incidents",

			"Go to User Site",
			"Log Out",
		],
		Cons::LIC_TRIAL => [
			"Company",
			"Company.Dashboard",
			"Company.Company Info",
//			"Company.Register New Company",
			"Company.User Profiles",
			"Company.Users",
			"Company.Import Users",
			"Company.Security Roles",
			"Company.Companies Risk Scores",
			"Company.Invites",

			"Compliance",
			"Compliance.Control Wizard",
			"Compliance.Cyber Inherent Risk",
			"Compliance.Cyber Maturity Risk",
			"Compliance.Risk Scoring Report",
			"Compliance.Control Summary",
			"Compliance.Roles Summary",
			"Compliance.Policy Summary",
			"Compliance.Policy Tracking",
			"Compliance.Manage Baselines",
			"Compliance.Control Baseline",
			"Compliance.Control Library",
			"Compliance.Evidence",
			"Compliance.Tasks",

			"Reports",
			"Reports.Situation Report",
			"Reports.User Compliance",
			"Reports.Profile Compliance",
			"Reports.Policy Compliance",
			"Reports.Training Compliance",
			"Reports.Quiz Compliance",
			"Reports.Control Compliance",

			"Policy Documents",
			"Policy Documents.Policies",
			"Policy Documents.Policy Library",

			"Training",
			"Training.Manage Training",
			"Training.Training Library",
			"Training.Manage Videos",
			"Training.Watch Videos",

			"Quizzes",
			"Quizzes.Quizzes",
			"Quizzes.Quiz Library",

			"Incidents",
			"Incidents.Manage Incidents",
			"Incidents.Manage Incident Types",

			"Go to User Site",
			"Log Out",
		],
		Cons::LIC_LEVEL1 => [
			"Company",
			"Company.Dashboard",
			"Company.Company Info",
//			"Company.Register New Company",
//			"Company.User Profiles",
//			"Company.Users",
//			"Company.Import Users",
//			"Company.Security Roles",
//			"Company.Companies Risk Scores",
//			"Company.Invites",

			"Compliance",
			"Compliance.Control Wizard",
			"Compliance.Cyber Inherent Risk",
			"Compliance.Cyber Maturity Risk",
			"Compliance.Risk Scoring Report",
			"Compliance.Control Summary",
			"Compliance.Roles Summary",
			"Compliance.Policy Summary",
			"Compliance.Policy Tracking",
//			"Compliance.Manage Baselines",
//			"Compliance.Control Baseline",
//			"Compliance.Control Library",
//			"Compliance.Evidence",
//			"Compliance.Tasks",

//			"Reports.Situation Report",
//			"Reports.User Compliance",
//			"Reports.Profile Compliance",
//			"Reports.Policy Compliance",
//			"Reports.Training Compliance",
//			"Reports.Quiz Compliance",
//			"Reports.Control Compliance",

			"Policy Documents",
//			"Policy Documents.Policies",
			"Policy Documents.Policy Library",

//			"Training.Manage Training",
//			"Training.Training Library",
//			"Training.Manage Videos",
//			"Training.Watch Videos",

//			"Quizzes.Quizzes",
//			"Quizzes.Quiz Library",

//			"Incidents.Manage Incidents",
//			"Incidents.Manage Incident Types",
//			"Incidents.My Incidents",

			"Go to User Site",
			"Log Out",
		],
		Cons::LIC_LEVEL2 => [
			"Company",
			"Company.Dashboard",
			"Company.Company Info",
//			"Company.Register New Company",
			"Company.User Profiles",
			"Company.Users",
			"Company.Import Users",
			"Company.Security Roles",
			"Company.Companies Risk Scores",
			"Company.Invites",

			"Compliance",
			"Compliance.Control Wizard",
			"Compliance.Cyber Inherent Risk",
			"Compliance.Cyber Maturity Risk",
			"Compliance.Risk Scoring Report",
			"Compliance.Control Summary",
			"Compliance.Roles Summary",
			"Compliance.Policy Summary",
			"Compliance.Policy Tracking",
			"Compliance.Manage Baselines",
			"Compliance.Control Baseline",
			"Compliance.Control Library",
			"Compliance.Evidence",
			"Compliance.Tasks",

			"Reports",
			"Reports.Situation Report",
			"Reports.User Compliance",
			"Reports.Profile Compliance",
			"Reports.Policy Compliance",
			"Reports.Training Compliance",
			"Reports.Quiz Compliance",
			"Reports.Control Compliance",

			"Policy Documents",
			"Policy Documents.Policies",
			"Policy Documents.Policy Library",

			"Training",
			"Training.Manage Training",
			"Training.Training Library",
			"Training.Manage Videos",
			"Training.Watch Videos",

			"Quizzes",
			"Quizzes.Quizzes",
			"Quizzes.Quiz Library",

			"Incidents",
			"Incidents.Manage Incidents",
			"Incidents.Manage Incident Types",

			"Go to User Site",
			"Log Out",
		],
		Cons::LIC_LEVEL3 => [
			"Company",
			"Company.Dashboard",
			"Company.Company Info",
//			"Company.Register New Company",
			"Company.User Profiles",
			"Company.Users",
			"Company.Import Users",
			"Company.Security Roles",
			"Company.Companies Risk Scores",
			"Company.Invites",

			"Compliance",
			"Compliance.Control Wizard",
			"Compliance.Cyber Inherent Risk",
			"Compliance.Cyber Maturity Risk",
			"Compliance.Risk Scoring Report",
			"Compliance.Control Summary",
			"Compliance.Roles Summary",
			"Compliance.Policy Summary",
			"Compliance.Policy Tracking",
			"Compliance.Manage Baselines",
			"Compliance.Control Baseline",
			"Compliance.Control Library",
			"Compliance.Evidence",
			"Compliance.Tasks",

			"Reports",
			"Reports.Situation Report",
			"Reports.User Compliance",
			"Reports.Profile Compliance",
			"Reports.Policy Compliance",
			"Reports.Training Compliance",
			"Reports.Quiz Compliance",
			"Reports.Control Compliance",

			"Policy Documents",
			"Policy Documents.Policies",
			"Policy Documents.Policy Library",

			"Training",
			"Training.Manage Training",
			"Training.Training Library",
			"Training.Manage Videos",
			"Training.Watch Videos",

			"Quizzes",
			"Quizzes.Quizzes",
			"Quizzes.Quiz Library",

			"Incidents",
			"Incidents.Manage Incidents",
			"Incidents.Manage Incident Types",

			"Go to User Site",
			"Log Out",
		],
		Cons::LIC_ALL => [
			"Company",
			"Company.Dashboard",
			"Company.Company Info",
			"Company.Register New Company",
			"Company.User Profiles",
			"Company.Users",
			"Company.Import Users",
			"Company.Security Roles",
			"Company.Companies Risk Scores",
			"Company.Invites",

			"Compliance",
			"Compliance.Control Wizard",
			"Compliance.Cyber Inherent Risk",
			"Compliance.Cyber Maturity Risk",
			"Compliance.Risk Scoring Report",
//			"Compliance.Control Summary",
//			"Compliance.Roles Summary",
//			"Compliance.Policy Summary",
//			"Compliance.Policy Tracking",
			"Compliance.Manage Baselines",
			"Compliance.Control Baseline",
			"Compliance.Control Library",
			"Compliance.Evidence",
			"Compliance.Tasks",

			"Reports",
			"Reports.Situation Report",
			"Reports.User Compliance",
			"Reports.Profile Compliance",
			"Reports.Policy Compliance",
			"Reports.Training Compliance",
			"Reports.Quiz Compliance",
			"Reports.Control Compliance",

			"Policy Documents",
			"Policy Documents.Policies",
			"Policy Documents.Policy Library",

			"Training",
			"Training.Manage Training",
			"Training.Training Library",
			"Training.Manage Videos",
			"Training.Watch Videos",

			"Quizzes",
			"Quizzes.Quizzes",
			"Quizzes.Quiz Library",

			"Incidents",
			"Incidents.Manage Incidents",
			"Incidents.Manage Incident Types",

			"Go to User Site",
			"Log Out",
		],
		'Access_By_Role' => [
			"Company" => Cons::ROLE_AUDITOR,
			"Company.Dashboard" => Cons::ROLE_AUDITOR,
			"Company.Company Info" => Cons::ROLE_ADMIN,
			"Company.Register New Company" => Cons::ROLE_SUPER,
			"Company.User Profiles" => Cons::ROLE_ADMIN,
			"Company.Users" => Cons::ROLE_ADMIN,
			"Company.Import Users" => Cons::ROLE_ADMIN,
			"Company.Security Roles" => Cons::ROLE_ADMIN,
			"Company.Companies Risk Scores" => Cons::ROLE_AUDITOR,
			"Company.Invites" => Cons::ROLE_ADMIN,

			"Compliance" => Cons::ROLE_AUDITOR,
			"Compliance.Control Wizard" => Cons::ROLE_AUTHOR,
			"Compliance.Cyber Inherent Risk" => Cons::ROLE_AUDITOR,
			"Compliance.Cyber Maturity Risk" => Cons::ROLE_AUDITOR,
			"Compliance.Risk Scoring Report" => Cons::ROLE_AUDITOR,
			"Compliance.Control Summary" => Cons::ROLE_AUDITOR,
			"Compliance.Roles Summary" => Cons::ROLE_AUDITOR,
			"Compliance.Policy Summary" => Cons::ROLE_AUDITOR,
			"Compliance.Policy Tracking" => Cons::ROLE_AUDITOR,
			"Compliance.Manage Baselines" => Cons::ROLE_AUTHOR,
			"Compliance.Control Baseline" => Cons::ROLE_AUDITOR,
			"Compliance.Control Library" => Cons::ROLE_AUDITOR,
			"Compliance.Evidence" => Cons::ROLE_AUDITOR,
			"Compliance.Tasks" => Cons::ROLE_AUDITOR,

			"Reports" => Cons::ROLE_AUDITOR,
			"Reports.Situation Report" => Cons::ROLE_AUDITOR,
			"Reports.User Compliance" => Cons::ROLE_AUDITOR,
			"Reports.Profile Compliance" => Cons::ROLE_AUDITOR,
			"Reports.Policy Compliance" => Cons::ROLE_AUDITOR,
			"Reports.Training Compliance" => Cons::ROLE_AUDITOR,
			"Reports.Quiz Compliance" => Cons::ROLE_AUDITOR,
			"Reports.Control Compliance" => Cons::ROLE_AUDITOR,

			"Policy Documents" => Cons::ROLE_AUDITOR,
			"Policy Documents.Policies" => Cons::ROLE_AUDITOR,
			"Policy Documents.Policy Library" => Cons::ROLE_AUTHOR,

			"Training" => Cons::ROLE_AUTHOR,
			"Training.Manage Training" => Cons::ROLE_AUTHOR,
			"Training.Training Library" => Cons::ROLE_AUTHOR,
			"Training.Manage Videos" => Cons::ROLE_AUTHOR,
			"Training.Watch Videos" => Cons::ROLE_AUTHOR,

			"Quizzes" => Cons::ROLE_AUTHOR,
			"Quizzes.Quizzes" => Cons::ROLE_AUTHOR,
			"Quizzes.Quiz Library" => Cons::ROLE_AUTHOR,

			"Incidents" => Cons::ROLE_AUTHOR,
			"Incidents.Manage Incidents" => Cons::ROLE_AUTHOR,
			"Incidents.Manage Incident Types" => Cons::ROLE_AUTHOR,

			"Go to User Site" => Cons::ROLE_USER,
			"Log Out" => Cons::ROLE_NONE,
		]
	];

}
