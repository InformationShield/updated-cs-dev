<?php

class mPermission
{
	public static $Licenses = array(
		1=>'Free',
		10=>'Trial',
		100=>'Level 1',
		200=>'Level 2',
		300=>'Level 3',
		1000=>'Ultimate',
	);
	// returns true if has access
	// returns either false, MSG_LICENSE_DENIED, or MSG_ACCESS_DENIED if does not have access
    public static function hasAccess($menuGroup, $menuItem) {
		$retVal = false;

		if (user()->isGuest) {
			app()->request->redirect(app()->createUrl('user/logout'));
			Yii::app()->end();
		}
		$licenseLevel = getSessionVar('license_level');
		$roleLevel = getSessionVar('context_company_role');
		if (Utils::isSuper()) {	// super user has access to everything
			$retVal = true;
		} else if ($menuGroup) { // check license and role
			if ($menuItem) { // sub menu item
				$menuItem = $menuGroup . '.' . $menuItem;
			} else { // group menu
				$menuItem = $menuGroup;
			}
			if ($licenseLevel && in_array($menuItem, mMenus::$acl[$licenseLevel])) { // License has access
				if ($roleLevel >= mMenus::$acl['Access_By_Role'][$menuItem]) { // Role has access
					$retVal = true;
				} else {
					$retVal = Cons::MSG_ROLE_DENIED;
				}
			} else {
				$retVal = Cons::MSG_LICENSE_DENIED;
			}
		} else if (getSessionVar('site_type') == 'UserSite' && !user()->isGuest) { // coming from user site.  everyone has access to everything there.
			if (mMenus::$sideMenusUser[$menuItem]['role'] <= $roleLevel) {
				$retVal = true;
			}
		}

		return $retVal;
	}

	// checks to see if user has license and role access to the feature
	// if $targetRole is specified, checks if the user is at least or higher than the $targetRole
	// $output param can be 'redirect', 'partial', 'json', or 'silent'
	public static function checkAccess($controller, $menuGroup, $menuItem, $roleOverride=null, $output='redirect') {
    	$results = self::hasAccess($menuGroup, $menuItem);
    	if ($results) {
			if ($roleOverride) {
				$contextCompanyRole = getSessionVar('context_company_role');
				if (!Utils::isSuper() && $contextCompanyRole < $roleOverride) {
					$results = Cons::MSG_ROLE_DENIED;
				}
			}
		}

    	if ($results !== true) {
    		if ($output == 'redirect') {
    			if ($results === false) $results = Cons::MSG_ACCESS_DENIED; // if access failed w/o specific reason
				$controller->render('//site/vMessage', array('messageId' => $results));
			} else if ($output == 'partial') {
				if ($results === false) $results = Cons::MSG_ACCESS_DENIED; // if access failed w/o specific reason
				$controller->renderPartial('//site/vMessage', array('messageId' => $results));
			} else if ($output == 'json') {
				if ($results == Cons::MSG_LICENSE_DENIED) {
					echo mGrid::toGridMessage('error', "Your company's license level is not authorized to perform this action, please upgrade your license by contacting us.");
				} else if ($results == Cons::MSG_ROLE_DENIED) {
					echo mGrid::toGridMessage('error', "Your role is not authorized to perform this action.");
				} else {
					echo mGrid::toGridMessage('error', 'Access denied.');
				}
			} // assume silent
			Yii::app()->end();
		}
		return $results;
	}

	// checks to see if the user is at the same level or higher as targetRole
	// $output param can be 'redirect', 'partial', 'json', or 'silent'
	public static function checkRole($controller, $targetRole, $output='redirect') {
		if (!Utils::isSuper() && getSessionVar('context_company_role', 0) < $targetRole) {
			if ($output == 'redirect') {
				$controller->render('//site/vMessage', array('messageId' => Cons::MSG_ROLE_DENIED));
			} else if ($output == 'partial') {
				$controller->renderPartial('//site/vMessage', array('messageId' => Cons::MSG_ROLE_DENIED));
			} else if ($output == 'json') {
				echo mGrid::toGridMessage('error', "Your role is not authorized to perform this action.");
			} else { // assume silent
				return false;
			}
			Yii::app()->end();
		}
		return true;
	}

	// checks to see if the user is at the same level or higher as targetLicense
	// $output param can be 'redirect', 'partial', 'json', or 'silent'
	public static function checkLicense($controller, $targetLicense, $output='redirect') {
		if (!Utils::isSuper() && getSessionVar('license_level', 0) < $targetLicense) {
			if ($output == 'redirect') {
				$controller->render('//site/vMessage', array('messageId' => Cons::MSG_LICENSE_DENIED));
			} else if ($output == 'partial') {
				$controller->renderPartial('//site/vMessage', array('messageId' => Cons::MSG_LICENSE_DENIED));
			} else if ($output == 'json') {
				echo mGrid::toGridMessage('error', "Your company's license level is not authorized to perform this action, please upgrade your license by contacting us.");
			} else { // assume silent
				return false;
			}
			Yii::app()->end();
		}
		return true;
	}

	// checks to see if the user is at the same level or higher as targetLicense
	// $output param can be 'redirect', 'partial', 'json', or 'silent'
	public static function checkLicenseRole($controller, $targetLicense=Cons::LIC_TRIAL, $targetRole=Cons::ROLE_AUDITOR, $output='redirect')
	{
		$results = self::checkLicense($controller, $targetLicense, $output);
		if ($results === true) {
			$results = self::checkRole($controller, $targetRole, $output);
		}
		if ($output == 'silent') {
			return $results;
		}
	}

	public static function companyRoleName($role=null){
		$roles=array(0=>'undefined',
			Cons::ROLE_USER=>'User',
			Cons::ROLE_AUTHOR=>'Author',
			Cons::ROLE_AUDITOR=>'Auditor',
			Cons::ROLE_ADMIN=>'Administrator',
			Cons::ROLE_OWNER=>'Owner',
		);
		if (isset($roles[$role])) {
			return $roles[$role];
		} else {
			return $roles[0];
		}
	}

	public static function checkTrialLicense($controller) {
		$retVal = null;
		$licenseLevel = getSessionVar('license_level');
		if ($licenseLevel == Cons::LIC_TRIAL) {
			$trialDaysRemaining = Company_Ex::getCompanyLicenseRemaining(Utils::contextCompanyId());
			if ($trialDaysRemaining > 0) {
				$retVal = $trialDaysRemaining;
			} else {
				$company = Company::model()->findByPk(Utils::contextCompanyId());
				if ($company) {
					$company->license_level = Cons::LIC_FREE;
					$company->save();
				}
				setSessionVar('license_level', Cons::LIC_FREE);
				$retVal = 0;
			}
		}
		return $retVal;
	}
}
