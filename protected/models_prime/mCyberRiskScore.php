<?php
/* SYP created */

class mCyberRiskScore extends CFormModel
{
	const CYBER_BASELINE_COMPANY_ID = 9;

    public function rules()
    {
        return array(
            array('', 'safe'),
        );
    }

    public function __construct()
    {
    }

    public function initialize()
    {
    }

    public function getCyberRiskScore($companyId, $baselineId) {
    	$retVal = null;

    	CyberRiskScore_Ex::calcCyberRiskScore($companyId, $baselineId);
		$results = CyberRiskScore_Ex::getCyberRiskScore($companyId, $baselineId);
		if ($results) {
			$results['cyber_risk_score_comparison'] = $this->getCyberRiskScoreComparison($results['total_score']);
			$retVal = $results;
		}

		return $retVal;
	}

	private static function getCyberRiskScoreComparison($companyCyberRiskScore)
	{
		$cyberRiskScoreComparison[] = array(
			'label' => 'Your Score',
			'value' => $companyCyberRiskScore,
			'displayValue' => $companyCyberRiskScore,
			'color' => Utils::maturityScoreColor($companyCyberRiskScore / 10),
			'labelFontColor'=> '000000'
		);

		$cyberRiskScoreAvg = self::getCyberRiskScoreAvg();
		$cyberRiskScoreComparison[] = array(
			'label' => 'Industry Average',
			'value' => $cyberRiskScoreAvg,
			'displayValue' => $cyberRiskScoreAvg,
			'color' => Utils::maturityScoreColor($cyberRiskScoreAvg / 10),
			'labelFontColor'=> '000000'
		);
		return $cyberRiskScoreComparison;
	}

	private static function getCyberRiskScoreAvg() {
		$retVal = 0;
		$sql = "
			SELECT ROUND(SUM(cyber_risk_score) / COUNT(company_info_id)) CyberRiskScoreAvg
			FROM company_info
			WHERE cyber_risk_score > 0

		";
		$command = Yii::app()->db->createCommand($sql);
		$results = $command->query()->read();
		if ($results) {
			$retVal = $results['CyberRiskScoreAvg'];
		}
		return $retVal;
	}
}