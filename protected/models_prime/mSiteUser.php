<?php
/*
 *  This class supports the auto account creation and signin feature for partner integration. For example eRiskHub.
 *
 *  First time eRiskHub user without an account already.
 *  If the email address does not exist as a user in our system already then this happens:
 *      - We create a new site user account and auto activate it and set the password to a long random string.
 *        They do not really need a password if always arriving to us from the partner site via a auto signin link.
 *        If they they can use the password reset feature to reset password anytime to login without the partner link.
 *      - We auto login the user into the site.
 *      - We then create a company for this user with a Free license and the company name is the email address so it is unique.
 *      - We then show them the Profile edit page so they can enter their First and Last name and change the username if they wish from the default of the email address.
 *      - Once they Save the Profile, then they are shown the Company Profile edit screen to save changes.
 *      - Then finally they see the Control Wizard to add controls.
 *
 *  First time eRishHub user with an account already.
 *  If the email address already exist as a user in our system then this happens:
 *      - We auto login the user into the site.
 *      - We check if the user has registered a company, and if not then create a company for this user with a Free license and the company name is the email address so it is unique.
 *      - If the company has no Controls defined then we take them to the Control Wizard, other wise the home page.
 *
 *  Returning eRiskHub user.
 *      - For the typical use case of a returning eRiskHub user, they would just auto login to the site home page.
 */

class mSiteUser
{
    public $email;
    public $username;
    public $password;
    public $first_name;
    public $last_name;
    public $partner_id = NULL;
    private $userExistAlready = false;
    private $user_id = NULL;

    public function __construct($email,$partner_id=NULL)
    {
        $find = User::model()->notsafe()->findByAttributes(array('email'=>$email));
        if (isset($find)) {
            $this->user_id = $find->id;
            $this->userExistAlready = true;
            $this->email = $find->email;
            $this->username = $find->username;
            $this->password = $find->password;
            $this->partner_id = $find->partner_id;
            $names = Utils::getUserFirstLastName($find->id);
            $this->first_name = $names['first_name'];
            $this->last_name = $names['last_name'];
        } else {
            $this->email = $email;
            $this->username = $email;
            $this->password = str_replace('-','',Utils::create_guid());
            $this->first_name = '';
            $this->last_name = '';
            $this->partner_id = $partner_id;
        }
    }

    public function IsNewUser()
    {
        return ($this->userExistAlready == false);
    }
    
    public function AutoCreateUser() {
        if ($this->userExistAlready) {
            return true;
        }

        /* @var $user User */
        $user = new User;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->activkey = UserModule::encrypting(microtime().$this->password);
        $user->password = UserModule::encryptPassword($this->password);
        $user->superuser = 0;
        $user->partner_id = $this->partner_id;
        $user->status = Cons::USER_STATUS_ACTIVE;
        $user->create_at = date('Y-m-d H:i:s');
        $user->lastvisit_at = date('Y-m-d H:i:s');

        $profile = new Profile;
        $profile->regMode = true;
        $profile->first_name = $this->first_name;
        $profile->last_name = $this->last_name;
        if ($user->save()) {
            $this->user_id = $user->id;
            $profile->user_id = $user->id;
            $profile->save();
            return true;
        }
        return false;
    }
    
    public function AutoLogin()
    {
        $identity = new UserIdentity($this->username,$this->password);
        $identity->authenticate();
        Yii::app()->user->login($identity,0);
    }

    public function RegisterCompany($license_level = Cons::LIC_FREE)
    {
        $companiesUserOwns = Company_Ex::getCompaniesUserOwns($this->user_id);
        if (!empty($companiesUserOwns)) {
            return mCompanyContext::setSessionVars($this->user_id, $companiesUserOwns[0]['company_id']);
        }

        $company = new Company();
        $company->isNewRecord = true;
        $company->user_id = $this->user_id;
        $company->license_level = $license_level;
        $company->status = 1;
        $company->company_guid = Utils::create_guid();
        $company->password = str_replace('-','',Utils::create_guid());
        $company->name = $this->email;
        $find = Company::model()->findByAttributes(array('name'=>$company->name));
        if (isset($find)) {
            $company->name = $company->name.' '.($find->company_id + 1);
            $find = Company::model()->findByAttributes(array('name'=>$company->name));
            if (isset($find)) {
                $company->name = 'Company '.str_replace('-','',Utils::create_guid());
            }
        }

        if ($company->save())
        {
            // create default 1st company user
            $employeeModel = new Employee();
            $employeeModel->company_role_id = Cons::ROLE_OWNER;
            $employeeModel->company_id = $company->company_id;
            $employeeModel->firstname = (!empty($this->first_name)) ? $this->first_name : 'Company';
            $employeeModel->lastname = (!empty($this->last_name)) ? $this->last_name : 'Owner';
            $employeeModel->email = $this->email;
            if (!$employeeModel->save()) {
                Yii::log(print_r($employeeModel->getErrors(), true), 'error', 'mSiteUser.RegisterCompany');
            }

            // create new company context
            mCompanyContext::setSessionVars($this->user_id);
            // create default security roles if company has none
            SecurityRole_Ex::copyDefaultRoles($company->company_id, $company->name);
            // create default incident types if company has none
            IncidentType_Ex::copyDefault($company->company_id);
            return $company->company_id;
        } else {
            Yii::log(print_r($company->getErrors(), true), 'error', 'mSiteUser.RegisterCompany');
        }
        return false;
    }

    // call this after login and company registration
    public function AddDemoLicenseContent()
    {
        // Acceptable Use
        $status = PolicyLibrary_Ex::copyPolicy(54,Utils::contextCompanyId(),userId(),$message);
        // Access Control
        $status = PolicyLibrary_Ex::copyPolicy(108,Utils::contextCompanyId(),userId(),$message);
    }

}