<?php

class mReportsPolicy
{
    public $header;

    public function ComplianceReportDetail($company_id,$policy_id,$asJSON=true,$withHeader=false,$policyInfo=null)
    {
        $header = array(
            'firstname'=>'First Name',
            'lastname'=>'Last Name',
            'department_name'=>'Department',
            'status'=>'Status',
            'status_name'=>'Status',
            'ack_date'=>'Acknowledged On',
            'view_date'=>'Viewed On',
        );
        $sql = "
			SELECT 
				cu.firstname, cu.lastname, cu.department_name,
				CASE
					WHEN upt.ack > 0 then
						2
					WHEN upt.view > 0 then
						1
					ELSE
						0
				END status,
				CASE
					WHEN upt.ack > 0 then
						'Acknowleged'
					WHEN upt.view > 0 then
						'Viewed'
					ELSE
						'' -- 'Unopened'
				END status_name,
				DATE_FORMAT(IF(upt.ack = 0 OR upt.ack_date = '0000-00-00', null, upt.ack_date),'%m/%d/%Y %h:%i %p') ack_date,
				DATE_FORMAT(IF(upt.view = 0 OR upt.view_date = '0000-00-00', null, upt.view_date),'%m/%d/%Y %h:%i %p') view_date
			FROM company cp
				INNER JOIN employee cu ON cu.company_id = cp.company_id
				INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
				INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
				INNER JOIN policy p ON p.company_id = cp.company_id AND p.policy_status_id = 3 AND p.status = 1
				INNER JOIN user_profile_policy upp ON upp.policy_id = p.policy_id AND upp.user_profile_id = up.user_profile_id
				LEFT JOIN tbl_users u ON u.email = cu.email
				LEFT JOIN user_policy_tracking upt ON upt.policy_id = p.policy_id AND upt.user_id = u.id
			WHERE cp.company_id = :company_id
				AND p.policy_id = :policy_id
			GROUP BY cu.employee_id
			ORDER BY cu.lastname, cu.firstname
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $command->bindParam(":policy_id", $policy_id, PDO::PARAM_INT);
        $data = $command->query()->readAll();
        if (!isset($data)) $data = array();
		if ($withHeader) $data = mGrid::addHeader($header, $data);
        if (!empty($policyInfo)) {
            $details = array();
            $details[] = array('title' => 'Policy Compliance Detail Report');
            $details[] = array('policy' => $policyInfo['doc_title']);
            //$details[] = array('description' => strip_tags($policyInfo['description']));
            $details[] = array('blankline' => '   ');
            $data = array_merge($details,$data);
        }
        if ($asJSON) {
            $array = array("status" => "success", "total" => count($data), "records" => $data);
            $data = json_encode($array);
            //Yii::log(print_r($array,true)."\n\n".$data,'error','mReportsPolicy.ComplianceReport');
        }
        return $data;
    }

	public function ComplianceReport($company_id,$asJSON=true,$withHeader=false)
	{
		$header = array(
			'doc_title'=>'Title',
			'description'=>'Description',
			'publish_date'=>'Published',

			'policy_count'=>'Policy Count',
			'policy_acks'=>'Acknowledged',
			'policy_acks_per'=>'%',
			'policy_views'=>'Viewed',
			'policy_views_per'=>'%',
			'policy_unopened'=>'Unopened',
			'policy_unopened_per'=>'%',
		);

		$sql = "
			SELECT *,
			IF (policy_count > 0, ROUND(policy_views / policy_count * 100, 0), 0) policy_views_per,
			IF (policy_count > 0, ROUND(policy_acks / policy_count * 100, 0), 0) policy_acks_per,
			IF (policy_count > 0, 100 - ROUND(policy_views / policy_count * 100, 0) - ROUND(policy_acks / policy_count * 100, 0), 0) policy_unopened_per
			FROM (

				SELECT p.policy_id, p.doc_title, p.description, p.publish_date, 
				COUNT(p.policy_id) policy_count, 
				COUNT(p.policy_id) - IFNULL(SUM(upt.view), 0) policy_unopened, 
				IFNULL(SUM(upt.view = 1 AND upt.ack != 1), 0) policy_views, 
				IFNULL(SUM(upt.ack = 1), 0) policy_acks
				FROM company cp
				INNER JOIN employee cu ON cu.company_id = cp.company_id
				INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
				INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
				INNER JOIN policy p ON p.company_id = cp.company_id AND p.policy_status_id = 3 AND p.status = 1
				INNER JOIN user_profile_policy upp ON upp.policy_id = p.policy_id AND upp.user_profile_id = up.user_profile_id
				LEFT JOIN tbl_users u ON u.email = cu.email
				LEFT JOIN user_policy_tracking upt ON upt.policy_id = p.policy_id AND upt.user_id = u.id
				WHERE cp.company_id = :company_id
 				GROUP BY p.policy_id
			) AS PolicyStats
			ORDER BY doc_title
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
		$data = $command->query()->readAll();
		if (!isset($data)) $data = array();
		foreach($data as $key=>$row) {
			if ($asJSON) {
				$data[$key]['policy_id'] = Obscure::encode($data[$key]['policy_id']);
			}
		}
		if ($withHeader) $data = mGrid::addHeader($header, $data);
		if ($asJSON) {
			$array = array("status" => "success", "total" => count($data), "records" => $data);
			$data = json_encode($array);
			//Yii::log(print_r($array,true)."\n\n".$data,'error','mReportsPolicy.ComplianceReport');
		}
		return $data;
	}

}