<?php

require_once Yii::app()->basePath . DIRECTORY_SEPARATOR . 'extensions/PHPWord-0.12.1/src/PhpWord/Autoloader.php';

class mCreateWordDocument
{
    private $mCompanyName;
    private $mCompanyContactEmail;
    private $mCompanyContactUserFirstLastName;
    
    function __construct() {
        PhpOffice\PhpWord\Autoloader::register();
        
        $this->mCompanyName = '';
        $this->mCompanyContactEmail = '';
        $this->mCompanyContactUserFirstLastName = '';
    }
    /*
     * @param companyData is an array with all company info such as
     * string company_name
     * string company_contact_email
     * string company_contact_user_first_last_name
     * 
     */
    public function SetCompanyData($companyData){
        if(is_array($companyData)){
            $this->mCompanyName = (isset($companyData['company_name'])) ? $companyData['company_name'] : '';
            $this->mCompanyContactEmail = (isset($companyData['company_contact_email'])) ? $companyData['company_contact_email'] : '';
            $this->mCompanyContactUserFirstLastName = (isset($companyData['company_contact_user_first_last_name'])) ? $companyData['company_contact_user_first_last_name'] : '';
        }
    }
    
    public function CreatePoliciesDocument($polices = array(), $references = array()){
        $wordDoc = new PhpOffice\PhpWord\PhpWord();
        
        // Total page width 11906 twips
        // Creating main document section
        $documentSection = $wordDoc->addSection(array('marginLeft' => 1251, 'marginRight' => 1051));
        
        /* 
         * Header logo and page url start
         */
        /*$headerTable = $documentSection->addTable(array('borderTopSize' => 0, 'borderRightSize' => 0, 'borderBottomSize' => 0, 'borderLeftSize' => 0, 'borderColor' => 'FFFFFF'));
        $headerTable->addRow();
        
        $headerTable
                ->addCell(4500, array('valign' => 'center','borderTopSize' => 0, 'borderRightSize' => 0, 'borderBottomSize' => 0, 'borderLeftSize' => 0, 'borderColor' => 'FFFFFF'))
                ->addImage("https://placehold.it/150x50", array(
                    'width' => 150,
                    'height' => 50
                ));
        
        $headerTable
                ->addCell(5100, array('valign' => 'bottom','borderTopSize' => 0, 'borderRightSize' => 0, 'borderBottomSize' => 0, 'borderLeftSize' => 0, 'borderColor' => 'FFFFFF'))
                ->addText("www.informationshield.com", array('name' => 'Arial', 'size' => '10', 'color' => '005D83'), array('align' => 'right'));
        
        $documentSection->addTextBreak(1);
        /* 
         * Header logo and page url End
         */
        
        
        /* 
         * Policy contact, date, version information start
         */
        $policiesTable = $documentSection->addTable(array('borderTopSize' => 1, 'borderRightSize' => 1, 'borderBottomSize' => 1, 'borderLeftSize' => 1, 'borderColor' => '000000'));
        $policiesTable->addRow(600);
        
        $cell = $policiesTable
                ->addCell(9600, array('valign' => 'center'));
        $cell->getStyle()->setGridSpan(6);
        $cell->addText('Information Security Policies', array('name' => 'Arial', 'size' => '18', 'color' => '005D83'), array('align' => 'center'));
        
        $policiesTable->addRow(500);
        $cell = $policiesTable
                ->addCell(9600, array('valign' => 'center', 'bgcolor' => 'CCCCCC'));
        $cell->getStyle()->setGridSpan(6);
                
        $cell->addText($this->mCompanyName . " Security Policy", array('name' => 'Arial', 'size' => '16', 'color' => '000000', 'bold' => true), array('align' => 'center'));
        
        
        $piliciesTableHeaderCellTextStyle = array('name' => 'Arial', 'size' => '10', 'color' => '005D83');
        $piliciesTableHeaderCellParagraphStyle = array('align' => 'center', 'spaceBefore' => 100, 'spaceAfter' => 100);
        $policiesTableContentCellTextStyle = array('name' => 'Arial', 'size' => '10', 'color' => '000000');
        $policiesTableContentCellParagraphStyle = array('align' => 'center', 'spaceBefore' => 100, 'spaceAfter' => 100);
        
        $policiesTable->addRow();
        $policiesTable
                ->addCell(1100, array('valign' => 'center', 'borderRightSize' => 1, 'borderBottomSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText('Policy #', $piliciesTableHeaderCellTextStyle, $piliciesTableHeaderCellParagraphStyle);
        
        $policiesTable
                ->addCell(1600, array('valign' => 'center', 'borderRightSize' => 1, 'borderBottomSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText('1', $policiesTableContentCellTextStyle, $policiesTableContentCellParagraphStyle);
        
        $policiesTable
                ->addCell(1600, array('valign' => 'center', 'borderRightSize' => 1, 'borderBottomSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText('Effective Date', $piliciesTableHeaderCellTextStyle, $piliciesTableHeaderCellParagraphStyle);
        
        $policiesTable
                ->addCell(2100, array('valign' => 'center', 'borderRightSize' => 1, 'borderBottomSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText(date("m/d/Y"), $policiesTableContentCellTextStyle, $policiesTableContentCellParagraphStyle);
        
        $policiesTable
                ->addCell(1100, array('valign' => 'center', 'borderBottomSize' => 1, 'borderRightSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText('Email', $piliciesTableHeaderCellTextStyle, $piliciesTableHeaderCellParagraphStyle);
        
        $policiesTable
                ->addCell(2100, array('valign' => 'center', 'borderBottomSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText($this->mCompanyContactEmail, $policiesTableContentCellTextStyle, $policiesTableContentCellParagraphStyle);
        
        $policiesTable->addRow();
        $policiesTable
                ->addCell(1100, array('valign' => 'center', 'borderRightSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText('Version', $piliciesTableHeaderCellTextStyle, $piliciesTableHeaderCellParagraphStyle);
        
        $policiesTable
                ->addCell(1600, array('valign' => 'center', 'borderRightSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText('1.0', $policiesTableContentCellTextStyle, $policiesTableContentCellParagraphStyle);
        
        $policiesTable
                ->addCell(1600, array('valign' => 'center', 'borderRightSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText('Contact', $piliciesTableHeaderCellTextStyle, $piliciesTableHeaderCellParagraphStyle);
        
        $policiesTable
                ->addCell(2100, array('valign' => 'center', 'borderRightSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText($this->mCompanyContactUserFirstLastName, $policiesTableContentCellTextStyle, $policiesTableContentCellParagraphStyle);
        
        $policiesTable
                ->addCell(1100, array('valign' => 'center', 'borderRightSize' => 1, 'borderColor' => 'CCCCCC'))
                ->addText('Phone', $piliciesTableHeaderCellTextStyle, $piliciesTableHeaderCellParagraphStyle);
        
        $policiesTable
                ->addCell(2100, array('valign' => 'center'))
                ->addText('(xxx) xxx-xxxx', $policiesTableContentCellTextStyle, $policiesTableContentCellParagraphStyle);
        $documentSection->addTextBreak(2);
        /* 
         * Policy contact, date, version information end
         */
        
        /*
         * Title, font styles
         */
        $wordDoc->addTitleStyle(1, array('name' => 'Arial', 'size' => 14, 'allCaps' => true, 'color'=>'005D83'));
        $wordDoc->addTitleStyle(2, array('name' => 'Arial', 'size' => 12, 'color'=>'005D83'));
        $documentRegularFontStyle = array('name' => 'Arial', 'size' => 11);
        $documentRegularFontStyleBold = array('name' => 'Arial', 'size' => 11, 'bold' => true);
        
        /* 
         * Table of contents start
         */
        $documentSection->addText("Table of Contents", array('name' => 'Arial', 'size' => 14, 'color'=>'005D83'), array());
        $documentSection->addTextBreak(1);
        $documentSection->addTOC(array('size' => 10), array('tabLeader' => 'dot', 'indent' => 300));
        $documentSection->addTextBreak(1);
        /* 
         * Table of contents end
         */
        
        /*
         * Document content start
         */
        $documentSection->addTitle("Purpose", 1);
        $documentSection->addText(htmlspecialchars("This policy defines the risk management requirements for the identification of the appropriate control posture for all " . $this->mCompanyName . " computer and communications information system assets."), $documentRegularFontStyle);
        
        $documentSection->addTextBreak(1);
        $documentSection->addTitle("Scope", 1);
        $documentSection->addText(htmlspecialchars("This policy applies to all " . $this->mCompanyName . " computer systems and facilities, with a target audience of " . $this->mCompanyName . " Executive Management, Information Technology employees and partners."), $documentRegularFontStyle);
        
        if(!empty($polices)){
            $documentSection->addPageBreak();
            $documentSection->addTitle("Policy", 1);
            
            foreach ($polices as $p) {
                $policyCategory = preg_replace("/^[^\s]*[\s]/i", "", $p['category']); // remove numbers from string beginning
                $documentSection->addTextBreak(1);
                $documentSection->addTitle(htmlspecialchars($policyCategory), 2);
                
                foreach ($p['policies'] as $policy) {
                    $textrun = $documentSection->addTextRun(array('align' => 'left', 'spaceBefore' => 70, 'indent' => 1));
                    $textrun->addText(htmlspecialchars($policy['title']), $documentRegularFontStyleBold);
                    $textrun->addText(htmlspecialchars(" - " . $policy['rel_policy']), $documentRegularFontStyle);
                    $documentSection->addTextBreak(1);
                }
            }
        }
        
        
        $documentSection->addTextBreak(1);
        $documentSection->addTitle("Violations", 1);
        $documentSection->addText(htmlspecialchars("Any violation of this policy may result in disciplinary action, up to and including termination of employment. " . $this->mCompanyName . " reserves the right to notify the appropriate law enforcement authorities of any unlawful activity and to cooperate in any investigation of such activity. " . $this->mCompanyName . " does not consider conduct in violation of this policy to be within an employee’s or partner’s course and scope of employment, or the direct consequence of the discharge of the employee’s or partner’s duties. Accordingly, to the extent permitted by law, " . $this->mCompanyName . " reserves the right not to defend or pay any damages awarded against employees or partners that result from violation of this policy."), $documentRegularFontStyle);
        
        $documentSection->addTextBreak(1);
        $documentSection->addTitle("Definitions", 1);
        
        $textrun = $documentSection->addTextRun();
        $textrun->addText(htmlspecialchars('Information Asset'), $documentRegularFontStyleBold);
        $textrun->addText(htmlspecialchars(" - Any " . $this->mCompanyName . " data in any form, and the equipment used to manage, process, or store " . $this->mCompanyName . " data, that is used in the course of executing business.  This includes, but is not limited to, corporate, customer, and partner data."), $documentRegularFontStyle);
        $documentSection->addTextBreak(1);
        
        $textrun = $documentSection->addTextRun();
        $textrun->addText(htmlspecialchars('Partner'), $documentRegularFontStyleBold);
        $textrun->addText(htmlspecialchars(" - Any non-employee of " . $this->mCompanyName . " who is contractually bound to provide some form of service to " . $this->mCompanyName . "."), $documentRegularFontStyle);
        $documentSection->addTextBreak(1);
        
        $textrun = $documentSection->addTextRun();
        $textrun->addText(htmlspecialchars('Password'), $documentRegularFontStyleBold);
        $textrun->addText(htmlspecialchars(" – An arbitrary string of characters chosen by a user that is used to authenticate the user when he attempts to log on, in order to prevent unauthorized access to his account."), $documentRegularFontStyle);
        $documentSection->addTextBreak(1);
        
        $textrun = $documentSection->addTextRun();
        $textrun->addText(htmlspecialchars('User'), $documentRegularFontStyleBold);
        $textrun->addText(htmlspecialchars(" - Any " . $this->mCompanyName . " employee or partner who has been authorized to access any " . $this->mCompanyName . " electronic information resource."), $documentRegularFontStyle);
        
        if(!empty($references)){
            $documentSection->addTextBreak(1);
            $documentSection->addTitle("References", 1);
            foreach ($references as $reference) {
                $documentSection->addText(htmlspecialchars($reference), $documentRegularFontStyle);
            }
        }
        
        /*
         * Document content end
         */
        
        
        /*
         * Revision table start
         */
        
        $documentSection->addTextBreak(1);
        $documentSection->addTitle("Revision History", 1);
        
        $revisionTable = $documentSection->addTable(array('borderTopSize' => 1, 'borderRightSize' => 0, 'borderBottomSize' => 0, 'borderLeftSize' => 1, 'borderColor' => '000000'));
        $revisionTable->addRow();
        
        $revisionTableHeaderCellStyle = array('valign' => 'center', 'bgColor' => 'EEEEEE', 'borderRightSize' => 1, 'borderBottomSize' => 1, 'borderColor' => '000000');
        $revisionTableHeaderTextStyle = array('name' => 'Arial', 'size' => 10, 'color' => '005D83', 'bold' => true);
        $revisionTableHeaderParagraphStyle = array('align' => 'center', 'spaceBefore' => 50, 'spaceAfter' => 50);
        $revisionTableCellStyle = array('valign' => 'center', 'borderRightSize' => 1, 'borderBottomSize' => 1, 'borderColor' => '000000');
        $revisionTableTextStyle = array('name' => 'Arial', 'size' => 10);
        $revisionTableParagraphStyle = array('align' => 'center', 'spaceBefore' => 50, 'spaceAfter' => 50);
        
        $revisionTable
                ->addCell(1300, $revisionTableHeaderCellStyle)
                ->addText('Version', $revisionTableHeaderTextStyle, $revisionTableHeaderParagraphStyle);
        
        $revisionTable
                ->addCell(3100, $revisionTableHeaderCellStyle)
                ->addText('Description', $revisionTableHeaderTextStyle, $revisionTableHeaderParagraphStyle);
        
        $revisionTable
                ->addCell(1600, $revisionTableHeaderCellStyle)
                ->addText('Revision Date', $revisionTableHeaderTextStyle, $revisionTableHeaderParagraphStyle);
        
        $revisionTable
                ->addCell(1500, $revisionTableHeaderCellStyle)
                ->addText('Review Date', $revisionTableHeaderTextStyle, $revisionTableHeaderParagraphStyle);
        
        $revisionTable
                ->addCell(2100, $revisionTableHeaderCellStyle)
                ->addText('Reviewer/Approver Name', $revisionTableHeaderTextStyle, $revisionTableHeaderParagraphStyle);
        
        
        $revisionTable->addRow();
        $revisionTable
                ->addCell(1300, $revisionTableCellStyle)
                ->addText('1.0', $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        $revisionTable
                ->addCell(3100, $revisionTableCellStyle)
                ->addText('Initial Version', $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        $revisionTable
                ->addCell(1600, $revisionTableCellStyle)
                ->addText(date("m/d/Y"), $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        $revisionTable
                ->addCell(1500, $revisionTableCellStyle)
                ->addText(date("m/d/Y"), $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        $revisionTable
                ->addCell(2100, $revisionTableCellStyle)
                ->addText('', $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        
        $revisionTable->addRow();
        $revisionTable
                ->addCell(1300, $revisionTableCellStyle)
                ->addText(' ', $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        $revisionTable
                ->addCell(3100, $revisionTableCellStyle)
                ->addText(' ', $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        $revisionTable
                ->addCell(1600, $revisionTableCellStyle)
                ->addText(' ', $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        $revisionTable
                ->addCell(1500, $revisionTableCellStyle)
                ->addText(' ', $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        $revisionTable
                ->addCell(2100, $revisionTableCellStyle)
                ->addText(' ', $revisionTableTextStyle, $revisionTableParagraphStyle);
        
        /*
         * Revision table end
         */
        
        /* 
         * Footer information start
         */
        $footer = $documentSection->addFooter();        
        $footerTable = $footer->addTable(array('borderTopSize' => 0, 'borderRightSize' => 0, 'borderBottomSize' => 0, 'borderLeftSize' => 0, 'borderColor' => 'FFFFFF'));
        $footerTable->addRow();
        
        $footerTable
                ->addCell(4200, array('valign' => 'center'))
                ->addText(htmlspecialchars($this->mCompanyName . " Security Policy"), array('name' => 'Arial', 'size' => '10', 'color' => '005D83'), array('align' => 'left'));
        $footerTable
                ->addCell(3200, array('valign' => 'center'))
                ->addText(htmlspecialchars('CONFIDENTIAL'), array('name' => 'Arial', 'size' => '10', 'color' => '005D83'), array('align' => 'left'));
        
        $footerTable
                ->addCell(2200, array('valign' => 'center'))
                ->addPreserveText('Page {PAGE}', array('name' => 'Arial', 'size' => '10', 'color' => '005D83'), array('align' => 'center'));
        
        /* 
         * Footer information End
         */
        
        /* 
         * Create document
         */
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($wordDoc, 'Word2007');
        
        ob_start();
        $objWriter->save('php://output');
        $buffer =  ob_get_contents();
        ob_end_clean();
        
        return $buffer;
    }
}

?>