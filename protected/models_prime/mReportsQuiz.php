<?php

class mReportsQuiz
{
    public $header;

    public function ComplianceReportDetail($company_id,$quiz_id,$asJSON=true,$withHeader=false,$quizInfo=null)
    {
        $header = array(
            'firstname'=>'First Name',
            'lastname'=>'Last Name',
            'department_name'=>'Department',
            'status'=>'Status',
            'status_name'=>'Status',
            'passed_timestamp'=>'Passed On',
            'completed_timestamp'=>'Completed On',
            'viewed_timestamp'=>'Viewed On',
        );
		$sql = "
			SELECT 
				cu.firstname, cu.lastname, cu.department_name,
				CASE
					WHEN qt.passed > 0 then
						3
					WHEN qt.completed > 0 then
						2
					WHEN qt.viewed > 0 then
						1
					ELSE
						0
				END status,
				CASE
					WHEN qt.passed > 0 then
						'Passed'
					WHEN qt.completed > 0 then
						'Completed'
					WHEN qt.viewed > 0 then
						'Viewed'
					ELSE
						'' -- 'Unopened'
				END status_name,
				DATE_FORMAT(IF(qt.passed = 0 OR qt.passed_timestamp = '0000-00-00', null, qt.passed_timestamp),'%m/%d/%Y %h:%i %p') passed_timestamp,
				DATE_FORMAT(IF(qt.completed = 0 OR qt.completed_timestamp = '0000-00-00', null, qt.completed_timestamp),'%m/%d/%Y %h:%i %p') completed_timestamp,
				DATE_FORMAT(IF(qt.viewed = 0 OR qt.viewed_timestamp = '0000-00-00', null, qt.viewed_timestamp),'%m/%d/%Y %h:%i %p') viewed_timestamp
			FROM company cp
				INNER JOIN employee cu ON cu.company_id = cp.company_id
				INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
				INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
				INNER JOIN quiz q ON q.company_id = cp.company_id AND q.quiz_status_id = 3 AND q.status = 1
				INNER JOIN user_profile_quiz upq ON upq.quiz_id = q.quiz_id AND upq.user_profile_id = up.user_profile_id
				LEFT JOIN tbl_users u ON u.email = cu.email
				LEFT JOIN quiz_tracking qt ON qt.quiz_id = q.quiz_id AND qt.user_id = u.id
			WHERE cp.company_id = :company_id
				AND q.quiz_id = :quiz_id
			GROUP BY cu.employee_id
			ORDER BY cu.lastname, cu.firstname
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $command->bindParam(":quiz_id", $quiz_id, PDO::PARAM_INT);
        $data = $command->query()->readAll();
        if (!isset($data)) $data = array();
		if ($withHeader) $data = mGrid::addHeader($header, $data);
        if (!empty($quizInfo)) {
            $details = array();
            $details[] = array('title' => 'Quiz Compliance Detail Report');
            $details[] = array('quiz' => $quizInfo['quiz_title']);
            //$details[] = array('description' => strip_tags($quizInfo['description']));
            $details[] = array('blankline' => '   ');
            $data = array_merge($details,$data);
        }
        if ($asJSON) {
            $array = array("status" => "success", "total" => count($data), "records" => $data);
            $data = json_encode($array);
            //Yii::log(print_r($array,true)."\n\n".$data,'error','mReportsQuiz.ComplianceReport');
        }
        return $data;
    }

    public function ComplianceReport($company_id,$asJSON=true,$withHeader=false)
    {
        $header = array(
            'quiz_title'=>'Title',
            'description'=>'Description',
            'publish_date'=>'Published',
            'passed'=>'Passed',
            'passed_per'=>'%',
            'completed'=>'Completed',
            'completed_per'=>'%',
            'viewed'=>'Viewed',
            'viewed_per'=>'%',
            'unopened'=>'Unopened',
            'unopened_per'=>'%',
            'quiz_count'=>'Total',
        );

		$sql = "
			SELECT *,
			IF (quiz_count > 0, ROUND(quiz_viewed / quiz_count * 100, 0), 0) quiz_viewed_per,
			IF (quiz_count > 0, ROUND(quiz_completed / quiz_count * 100, 0), 0) quiz_completed_per,
			IF (quiz_count > 0, ROUND(quiz_passed / quiz_count * 100, 0), 0) quiz_passed_per,
			IF (quiz_count > 0, 100 - ROUND(quiz_viewed / quiz_count * 100, 0) - ROUND(quiz_completed / quiz_count * 100, 0), 0) quiz_unopened_per
			FROM (
				SELECT q.quiz_id, q.quiz_title, q.description, q.publish_date,
				COUNT(q.quiz_id) quiz_count, 
				COUNT(q.quiz_id) - IFNULL(SUM(qt.viewed), 0) quiz_unopened, 
				IFNULL(SUM(qt.viewed = 1 AND qt.passed != 1), 0) quiz_viewed, 
				IFNULL(SUM(qt.completed = 1), 0) quiz_completed, 
				IFNULL(SUM(qt.passed = 1), 0) quiz_passed
				FROM company cp
				INNER JOIN employee cu ON cu.company_id = cp.company_id
				INNER JOIN employee2profile e2p ON e2p.employee_id = cu.employee_id
				INNER JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id AND up.status = 1
				INNER JOIN quiz q ON q.company_id = cp.company_id AND q.quiz_status_id = 3 AND q.status = 1
				INNER JOIN user_profile_quiz upq ON upq.quiz_id = q.quiz_id AND upq.user_profile_id = up.user_profile_id
				LEFT JOIN tbl_users u ON u.email = cu.email
				LEFT JOIN quiz_tracking qt ON qt.quiz_id = q.quiz_id AND qt.user_id = u.id
				WHERE cp.company_id = :company_id
				GROUP BY up.user_profile_id
				
			) AS PolicyStats
			GROUP BY quiz_id
			ORDER BY quiz_title
		";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $data = $command->query()->readAll();
        if (!isset($data)) $data = array();
        foreach($data as $key=>$row) {
            if ($asJSON) {
                $data[$key]['quiz_id'] = Obscure::encode($data[$key]['quiz_id']);
            }
        }
		if ($withHeader) $data = mGrid::addHeader($header, $data);
        if ($asJSON) {
            $array = array("status" => "success", "total" => count($data), "records" => $data);
            $data = json_encode($array);
            //Yii::log(print_r($array,true)."\n\n".$data,'error','mReportsQuiz.ComplianceReport');
        }
        return $data;
    }
}