<?php
/* SYP created */
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class mControlBaseline extends CFormModel
{
	public $companyId;
	public $baselineId;

	public $controlBaselines;
	public $baseline;
	public $answerChoices;

	public $cyberRiskBaselineId;
	public $isCyberRiskBaseline = false;

    public function rules()
    {
        return array(
            array('', 'safe'),
        );
    }

    public function __construct($companyId=null, $baselineId=null)
    {
    	if ($companyId && $baselineId) {
			$this->getControlBaseLines($companyId, $baselineId);
		}
    }

    public function initialize()
    {
    }

	public function getControlBaseLines($companyId, $baselineId) {
		$retVal = NULL;
		$this->cyberRiskBaselineId = Baseline_Ex::getCyberRiskBaselineId();
		if ($baselineId == $this->cyberRiskBaselineId) {
			$this->baseline = Baseline_Ex::getBaselineAsessmentV2($companyId, $baselineId);
			$this->isCyberRiskBaseline = true;
		} else {
			$this->baseline = Baseline_Ex::getBaselineAsessment($count, $companyId, $baselineId);
		}
		if ($this->baseline) {
			if ($baselineId == $this->cyberRiskBaselineId) {
				$this->controlBaselines = ControlBaseline_Ex::getControlsQaV2($companyId, $baselineId);
			} else {
				$this->controlBaselines = ControlBaseline_Ex::getBaselineControlsQuestionsArray($companyId, $baselineId);
			}
			$this->answerChoices = ControlStatus_Ex::getAllBySortOrder();
			$retVal = true;
		}
		return $retVal;
	}

}