<?php

// adjust these paths to match you environment
require_once __DIR__ . '/../vendor/php-jwt/src/BeforeValidException.php';
require_once __DIR__ . '/../vendor/php-jwt/src/ExpiredException.php';
require_once __DIR__ . '/../vendor/php-jwt/src/SignatureInvalidException.php';
require_once __DIR__ . '/../vendor/php-jwt/src/JWT.php';

use \Firebase\JWT\JWT;

/**
 * See this website http://jwt.io/ for libraries and info
 * Standards info https://tools.ietf.org/html/rfc7519
 *
 * JSON Web Token php implementation from https://github.com/firebase/php-jwt
 *
 * This class is used to create and decode special login urls for the https://app.easyitcompliance.com website.
 * Used for sending Invites to partner companies.
 *
 */
class mWebToken
{
    // default secret key for the given keyId.
    public $secretKey = "fasd14742glksd57850kfhjiew431efs9907uwe";
    // default key id
    public $keyId = '1';
    // handle clock differences and latency in use (seconds)
    public $timeleeway =  7776000; //90days (60*60*60*24*90);
    // encryption algorithm - do not change value
    public $algorithm = 'HS256';

    // set of keys useds throughout time of app
    public static function SecretKeys()
    {
        $secretKeys = array(
            '1'=>"fasd14742glksd57850kfhjiew431efs9907uwe",
        );
        return $secretKeys;
    }

    public function __construct($keyId=NULL, $secretKey=NULL, $timeleeway=NULL, $algorithm=NULL)
    {
        if (!empty($keyId)) {
            $this->keyId = $keyId;
        }
        if (!empty($secretKey)) {
            $this->secretKey = $secretKey;
        } else {
            $secretKeys = self::SecretKeys();
            if (isset($secretKeys[$this->keyId])) {
                $this->secretKey = $secretKeys[$this->keyId];
            }
        }
        if (!empty($timeleeway)) $this->timeleeway = $timeleeway;
        if (!empty($algorithm)) $this->algorithm = $algorithm;
    }

    /**
     * Creates a auto sig-in url for the app.easyitcompliance.com website.
     *
     * @param string    $secret  A secret to send.
     * @param string    $urlBase    The base url to append the token to.
     *
     * @return string   The url to auto sign-in user by email
     *
     */
    public function createUrl($secret, $urlBase)
    {
        $encodedToken = $this->encodeToken($secret);
        $url = $urlBase."/k/".$this->keyId."/token/".$encodedToken;
        return $url;
    }

    /**
     * Encodes the token for the url.
     *
     * @param string    $secret A secret to send.
     *
     * @return string   Encoded token.
     *
     */
    public function encodeToken($secret)
    {
        $token = array(
            "iss" => $this->keyId,
            "nbf" => time() - $this->timeleeway, // not valid before this time
            "exp" => time() + $this->timeleeway, // not valid after this time
            "sub" => $secret
        );
        $encodedToken = JWT::encode($token, $this->secretKey, $this->algorithm);
        return $encodedToken;
    }

    /**
     * Decodes a token.
     *
     * @param string    $encodedToken  The encoded token.
     *
     * @return object   Decoded json as php object.
     *
     */
    public function decodeToken($encodedToken)
    {
        $tokenObj = JWT::decode($encodedToken, $this->secretKey, array($this->algorithm));
        return $tokenObj;
    }

}