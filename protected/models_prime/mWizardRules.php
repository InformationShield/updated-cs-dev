<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class mWizardRules
{
    private $rules;
    public function __construct()
    {
        $this->rules = array(
            //'highly_sensitive_data'=>array(),
            'individual_consumer_data'=>array(312, 313, 315, 316, 317),
            'credit_card_data'=>array(102, 114, 183, 184, 270, 271, 275),
            'individual_health_data'=>array(102, 114, 183, 184),
            'employee_mobile_customer_data'=>array(222,224),
            'remote_network_access'=>array(222,224),
            'third_party_data'=>array(179, 180, 181),
            'internally_developed_applications'=>array(268, 270, 271, 272, 273),
            'online_customer_transactions'=>array(274, 275),
            //'local_it_datacenters'=>array(),
            //'wireless_networks'=>array(),
        );
    }

    public function copyControlsByRules($companyInfo,$user_id)
    {
        $count = 0;
        $ids = array();
        $priority = array();
        foreach($this->rules as $key=>$controls) {
            if ($companyInfo[$key] > 0) {
                $ids = array_unique(array_merge($ids, $controls));
            }
        }
        if ($companyInfo['security_maturity'] == 1) {
            $priority = ControlLibrary_Ex::getControlIdsByPriority(array(1),$ids);
        } else if ($companyInfo['security_maturity'] == 2) {
            $priority = ControlLibrary_Ex::getControlIdsByPriority(array(1,2),$ids);
        } else if ($companyInfo['security_maturity'] == 3) {
            $priority = ControlLibrary_Ex::getControlIdsByPriority(array(1,2,3),$ids);
        }
        if (count($priority) > 0) {
            $ids = array_unique(array_merge($ids, $priority));
        }
        $company_name = trim($companyInfo->business_name);
        $company_id = $companyInfo->company_id;
        $baseline_id = Baseline_Ex::getCurrentBaselineId();
        
        foreach($ids as $control_id) {
            $success = ControlLibrary_Ex::copyToControlBaseline($control_id,$baseline_id,$company_id,$user_id,$company_name);
            if ($success == 0 || $success == 2) {
                $count++; // count add or dups
            }
        }
        return $count;
    }
}