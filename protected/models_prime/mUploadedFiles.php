<?php

class mUploadedFiles
{
    public $tmpFolderName = "temp";
    public $destinationFolder = null;
    public $companyGuid = null;
    private $password = null;
    private $destinationRoot = null;
    private $company_id = null;
    private $destinationDirPath = null;
    private $encryptionOn = false;

    public function __construct($destinationFolder = "temp/", $company_id = null)
    {
        $this->destinationRoot = __DIR__.'/../../';
        $this->destinationFolder = $destinationFolder;
        $this->destinationDirPath = $this->destinationRoot . $this->destinationFolder;

        if (!empty($company_id)) {
            $this->encryptionOn = true;
            $this->company_id = $company_id;
            $this->companyGuid = Company_Ex::getCompanyGuid($company_id, $this->password);
            $this->destinationDirPath = Utils::createFolder($this->destinationRoot.$this->destinationFolder,$this->companyGuid).'/';
            $this->destinationFolder .= $this->companyGuid.'/';
        }
    }

    public function getFilepath($filename)
    {
        $destinationFullPath = $this->destinationDirPath.$filename;
        if ($this->encryptionOn) {
            $destinationFullPath .= ".gz.aes";
        }
        return $destinationFullPath;
    }

    public function copyFile($srcFileFullPath,$filename)
    {
        $retVal = false;
        $destinationFullPath = $this->destinationDirPath.$filename;
        // if replacing old file delete it first
        if (file_exists($destinationFullPath)) {
            unlink($destinationFullPath);
        }
        try {
            if (@copy($srcFileFullPath, $destinationFullPath)) {
                chmod($destinationFullPath,0664);
                if ($this->encryptionOn && strripos($srcFileFullPath, ".gz.aes") === false) {
                    exec("gzip " . $destinationFullPath);
                    mAESmcrypt::encryptFile($destinationFullPath . ".gz", $this->password);
                    @unlink($destinationFullPath . ".gz");
                }
                $retVal = true;
            } else {
                Yii::log("Failed to copyFile({$srcFileFullPath},{$destinationFullPath}).\n", 'error', 'mUploadedFiles::copyFile');
            }
        } catch(Exception $e) {
            Yii::log("Exception: ".$e->__toString(),'error','mUploadedFiles::copyFile');
        }
        return $retVal;
    }

    public function saveFile($tmpFileFullPath,$filename)
    {
        $retVal = false;
        $destinationFullPath = $this->destinationDirPath.$filename;
        // if replacing old file delete it first
        if (file_exists($destinationFullPath)) {
            unlink($destinationFullPath);
        }
        if (move_uploaded_file($tmpFileFullPath,$destinationFullPath)) {
            chmod($destinationFullPath,0664);
            if ($this->encryptionOn) {
                exec("gzip " . $destinationFullPath);
                mAESmcrypt::encryptFile($destinationFullPath . ".gz", $this->password);
                @unlink($destinationFullPath . ".gz");
            }
            $retVal = true;
        } else {
            Yii::log("Failed to move_uploaded_file({$tmpFileFullPath},{$destinationFullPath}).\n",'error','mUploadedFiles::saveFile');
        }
        return $retVal;
    }

    public function removeFile($filename)
    {
        $retVal = false;
        $destinationFullPath = $this->destinationDirPath.$filename;
        if (file_exists($destinationFullPath.".gz.aes")) {
            if (unlink($destinationFullPath.".gz.aes")) {
                $retVal = true;
            } else if ($this->encryptionOn) {
                Yii::log("Failed to unlink({$destinationFullPath}.gz.aes).\n",'error','mUploadedFiles::removeFile');
            }
        }
        // check for decrypted version also
        if (file_exists($destinationFullPath.".gz")) {
            unlink($destinationFullPath.".gz");
        }
        // check for decrypted version also
        if (file_exists($destinationFullPath)) {
            if (unlink($destinationFullPath)) {
                if (!$this->encryptionOn) {
                    $retVal = true;
                }
            } else if (!$this->encryptionOn) {
                Yii::log("Failed to unlink({$destinationFullPath}).\n",'error','mUploadedFiles::removeFile');
            }
        }
        return $retVal;
    }

    public function downloadFile($filename,$file_type,$attachement_filename)
    {
        $destinationFullPath = $this->destinationDirPath.$filename;
        $f_ext = ($this->encryptionOn) ? ".gz.aes" : "";
        // make sure it's a file before doing anything!
        if (is_file($destinationFullPath.$f_ext)) {
            if ($this->encryptionOn) {
                mAESmcrypt::decryptFile($destinationFullPath . ".gz", $this->password);
                exec("gunzip " . $destinationFullPath . ".gz");
            }
            // required for IE
            if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off');	}
            // set header for download
            header('Pragma: public'); 	// required
            header('Expires: 0');		// no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($destinationFullPath)).' GMT');
            header('Cache-Control: private',false);
            header('Content-Type: '.$file_type);
            header('Content-Disposition: attachment; filename="'.basename($attachement_filename).'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($destinationFullPath));	// provide file size
            header('Connection: close');
            readfile($destinationFullPath);		// push it out
            if ($this->encryptionOn) {
                @unlink($destinationFullPath);
            }
            exit();
        } else {
            return "Can not find file.";
        }
    }

    public function getTmpFileUrl($internal_filename,$outside_filename)
    {
        $retVal = false;
        try {
            $destinationFullPath = $this->destinationDirPath.$internal_filename;
            $f_ext = ($this->encryptionOn) ? ".gz.aes" : "";
            // make sure it's a file before doing anything!
            if (is_file($destinationFullPath.$f_ext)) {
                if ($this->encryptionOn) {
                    mAESmcrypt::decryptFile($destinationFullPath . ".gz", $this->password);
                    exec("gunzip " . $destinationFullPath . ".gz");
                }
                $tmpDir = Utils::createFolder($this->destinationRoot, $this->tmpFolderName);
                $tmpFilename = time().'t'.mt_rand(100000,999999).'-'.$outside_filename;
                $tmpFilepath = $tmpDir.'/'.$tmpFilename;
                if (@rename($destinationFullPath, $tmpFilepath)) {
                    chmod($tmpFilepath, 0664);
                    $retVal = url($this->tmpFolderName.'/'.$tmpFilename);
                } else {
                    Yii::log("Failed to rename({$destinationFullPath},{$tmpFilepath}).\n", 'error', 'mUploadedFiles::getTmpFileUrl');
                }
            } else {
                Yii::log("Problem it is not a file ".$destinationFullPath.$f_ext, 'error', 'mUploadedFiles::getTmpFileUrl');
            }
        } catch(Exception $e) {
            Yii::log("Exception: ".$e->__toString(),'error','mUploadedFiles::getTmpFileUrl');
        }
        return $retVal;
    }

    public function deleteTmpFileUrl($file)
    {
        $retVal = false;
        try {
            $tmpFilepath = $this->destinationRoot.'/'.$this->tmpFolderName.'/'.$file;
            if (is_file($tmpFilepath) && file_exists($tmpFilepath)) {
                if (@unlink($tmpFilepath)) {
                    $retVal = true;
                } else if (!$this->encryptionOn) {
                    Yii::log("Failed to unlink({$tmpFilepath}).\n",'error','mUploadedFiles::deleteTmpFileUrl');
                }
            }
        } catch(Exception $e) {
            Yii::log("Exception: ".$e->__toString(),'error','mUploadedFiles::deleteTmpFileUrl');
        }
        return $retVal;
    }
}