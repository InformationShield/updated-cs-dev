<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class mCompanyContext extends CFormModel
{
    public $context_company_id = null;
    public $context_company_name = '';
    public $context_company_role = 0;
    public $context_license_level = 0;
    public $context_company_count = 0;
    public $context_company_logo = '';
    public $company_list = null;

    public function rules()
    {
        return array(
            array('context_company_id, context_company_name, context_company_role, context_company_logo', 'safe'),
        );
    }

    public function __construct($userId,$email,$defaultCompanyId=null)
    {
        $companiesUserOwns = Company_Ex::getCompaniesUserOwns($userId);
        $userEmployedList = Employee_Ex::getUserEmployedList($email);
        $this->company_list = array();
        if (!empty($companiesUserOwns)) {
            foreach ($companiesUserOwns as $company) {
                $this->company_list[$company['name']] = array(
                	'name' => $company['name'],
					'logo' => $company['logo'],
					'company_id' => $company['company_id'],
					'license_level'=>$company['license_level'],
					'owner' => true,
					'role' => Cons::ROLE_OWNER,
					'company' => $company,
					'employee' => null
				);
            }
        }
        if (!empty($userEmployedList)) {
            foreach ($userEmployedList as $employee) {
                if (!isset($this->company_list[$employee['company_id']])) {
                    $this->company_list[$employee['name']] = array(
                    	'name' => $employee['name'],
						'logo' => $employee['logo'],
						'company_id' => $employee['company_id'],
						'license_level'=>$employee['license_level'],
						'owner' => false,
						'role' => $employee['company_role_id'],
						'company' => null,
						'employee' => $employee
					);
                } else {
                    $this->company_list[$employee['name']]['employee'] = $employee;
                }
            }
        }
        // sort by company name
        ksort($this->company_list);
        // get first company
        $first = reset($this->company_list);
        // if not asking for a specific default then use last one set by user
        if (is_null($defaultCompanyId)) {
            $defaultCompanyId = Usersetting_Ex::GetValue($userId,'defaultCompanyId');
        }
        if ($defaultCompanyId > 0) {
            foreach($this->company_list as $company) {
                if ($company['company_id'] == $defaultCompanyId) {
                    $first = $company;
                }
            }
        }
        $defaultCompany = self::getDefaultCompany($userId);
        if ($defaultCompany) {
            // default is to use first company as context
            $this->context_company_id = $defaultCompany->company_id;//$first['company_id'];
            $this->context_company_name = $defaultCompany->name;//$first['name'];
            $this->context_company_role = $defaultCompany->role;//$first['role'];
            $this->context_license_level = $defaultCompany->license_level;//$first['license_level'];
            $this->context_company_count = count($this->company_list);
            $this->context_company_logo = $defaultCompany->logo;//$first['logo'];
        }
    }

    public static function getDefaultCompany($userId) {
        $defaultCompanyId = Usersetting_Ex::GetValue($userId,'defaultCompanyId');
        $defaultCompany = null;
        if ($defaultCompanyId) {
            $defaultCompany = Company_Ex::getCompanyEmployee($defaultCompanyId, $userId);
        }
        if (!$defaultCompany) {
            $company = new Company();
            $defaultCompany = $company->Attributes;
            $userCompanies = Company_Ex::getUserCompanies($userId);
            if ($userCompanies) {
                $defaultCompany = $userCompanies[0];  // first one on the list of companies user is associated with
            }
        }
        return (object) $defaultCompany;
    }

    public static function getDefaultCompanyId($userId) {
        $currentCompany = self::getDefaultCompany($userId);
        return $currentCompany->company_id;
    }

    public static function setSessionVars($userId=null, $companyId=null)
    {
        $retVal = null;
        if ($companyId) {
            $defaultCompany = (object) Company_Ex::getCompanyEmployee($companyId, $userId);
        } else {
            $defaultCompany = self::getDefaultCompany($userId);
        }
        if ($defaultCompany->company_id) {
            $userCompanies = Company_Ex::getUserCompanies($userId);
            if ($userCompanies) {
                setSessionVar('context_company_count', count($userCompanies));
            } else {
                setSessionVar('context_company_count', 0);
            }
            setSessionVar('context_company_id', $defaultCompany->company_id);
            setSessionVar('context_company_name', $defaultCompany->name);
            setSessionVar('context_company_role', $defaultCompany->role);
            setSessionVar('context_company_logo', $defaultCompany->logo);
            setSessionVar('license_level', $defaultCompany->license_level);
            setSessionVar('context_user_site', 0);
            // set current Baseline session vars for user
            $baseline_id = Baseline_Ex::getCurrentBaselineId(true, $userId); // this also sets session variable
            Usersetting_Ex::SetValue($userId, 'defaultCompanyId', $defaultCompany->company_id);
            $retVal = $defaultCompany->company_id;
        }
        return $retVal;
    }

    public static function getUserCompaniesInRole($role)
    {
        $companiesInRole = array();
        $userCompaniesInRole = Company_Ex::getUserCompanies(userId(), $role);
        foreach($userCompaniesInRole as $company) {
            $companiesInRole[$company['name']] = $company;
        }
        return $companiesInRole;
    }

    public static function GetLogo()
    {
        $logo = getSessionVar('context_company_logo','');
        if (empty($logo) || !file_exists(__DIR__.'/../../files/logo/'.$logo)) {
            $company_id = getSessionVar('context_company_id',null);
            if (!empty($company_id)) {
                $logo = Company_Ex::getCompanyLogo($company_id);
                setSessionVar('context_company_logo', $logo);
            }
        }
        return $logo;
    }
}