<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class mEmployee extends Employee
{
	public $errorMessages = array();
	public $builtinError = null;
	public $employeeProfiles;
	public $selectedProfiles;

	public $status = 1;

    public function rules()
    {
    	$rules = parent::rules();
    	$rules[] = array('employee_id', 'safe');
    	$rules[] = array('email', 'email');
    	return $rules;
    }

    public function __construct($employeeId=0) {
		$this->company_role_id = Cons::ROLE_USER;
		if ($employeeId) {
			$this->attributes = self::model()->findByPk($employeeId)->attributes;
		}
		$this->employeeProfiles = UserProfile_Ex::getEmployeeProfile(Utils::contextCompanyId(), $employeeId);
    }

	public function editEmployee($employeeData=null) {
		$retVal = null;
		$this->attributes = $employeeData['mEmployee'];
		$this->selectedProfiles = isset($employeeData['selectedProfiles']) ? $employeeData['selectedProfiles'] : null;
		$inviteByEmail = $employeeData['invitebyemail'] == '1' ? true : false;
		$this->company_id = Utils::contextCompanyId();

		// use previous or newly create role to compare
		$company_role_id = intval($this->company_role_id);
		$myRole = intval(Utils::contextCompanyRole());
		// DO NOT ALLOW USER TO EDIT USER WITH GREATER OR EQUAL ROLE UNLESS OWNERS Check before render form and post
		if ($myRole < Cons::ROLE_OWNER && $company_role_id >= $myRole) {
			$this->builtinError = array('messageId'=>6,'higherRole'=>mPermission::companyRoleName($company_role_id));
		} else {
			$results = true;
			if ($this->employee_id == 0) { // make sure not duplicating same user for same company
				$existingEmployee = Employee::model()->findByAttributes(
					array('email'=>$this->email, 'company_id'=>$this->company_id));
				if ($existingEmployee) {
					$this->errorMessages[] = $this->addError(get_class($this), "A user with same email already exists.  Please edit the existing user or use a different email.");
					$results = false;
				}
			}

			if ($results && $this->validate()) {
				$results = false;
				if ($this->employee_id == 0) {
					$employee = ViewModel::createFromInput($this, 'Employee', 'employee' , $this->attributes);
					if ($employee) {
						$this->employee_id = $employee->employee_id;
						$results = true;
					}
				} else {
					$employee = ViewModel::saveInput($this, 'Employee', 'employee', $this->attributes, $this->employee_id);
					if ($employee)
						$results = true;
				}

				if ($results) {
					Employee2profile::model()->deleteAllByAttributes(array('employee_id'=>$this->employee_id));
					if ($this->selectedProfiles) {
						foreach ($this->selectedProfiles as $userProfileId) {
							$employeeProfile = new Employee2profile();
							$employeeProfile->IsNewRecord = true;
							$employeeProfile->employee_id = $this->employee_id;
							$employeeProfile->user_profile_id = $userProfileId;
							$employeeProfile->save();
						}
					}

					if ($inviteByEmail) {
						$mInvite = new mInvite();
						$mInvite->invitee_email = $this->email;
						$mInvite->invitee_name = $this->firstname . ' ' . $this->lastname;
						$mInvite->sendUserInvite();
					}
					$retVal = true;
				} else {
					Yii::log(print_r($this->getErrors(), true), 'error', 'mEmployee.edit');
				}
			}
		}
		return $retVal;
	}

	public static function deleteEmployee($employeeId) {
        Task::model()->deleteAllByAttributes(array('employee_id'=>$employeeId));
        Incident::model()->deleteAllByAttributes(array('employee_id'=>$employeeId));
        $securityRole = SecurityRole::model()->findByAttributes(array('employee_id'=>$employeeId));
        if ($securityRole) {
            $securityRole->employee_id = NULL;
            $securityRole->isNewRecord = false;
            $securityRole->save();
        }
		Employee2profile::model()->deleteAllByAttributes(array('employee_id'=>$employeeId));
		Employee::model()->deleteAllByAttributes(array('employee_id'=>$employeeId));
		return true;
	}

}