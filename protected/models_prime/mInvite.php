<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class mInvite extends CFormModel
{
    public $invite_guid;
    public $companyName = "";
    public $fromName = "";
    public $fromEmail = "";
    public $invitee_name = "";
    public $invitee_email = "";
    public $baselines = array();
    public $baseline_id = '1';
    public $company_id;
    public $inviteUrl = "";
    public $salutation = "Dear [invitee_name], <br/><br/> ";
    public $msgBody = "";
    public $msgSubject = "";

    public function rules()
    {
        return array(
            array('invitee_name, invitee_email, baseline_id', 'required'),
            array('invitee_email', 'email'),
            array('invitee_name, invitee_email', 'length', 'max'=>256),
            array('invite_guid, companyName, fromName, baselines, company_id, inviteUrl', 'safe'),
        );
    }

    /**
     * @return array customized attribute labels (invitee_name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'invitee_name' => 'First and last name of invitee',
            'invitee_email' => 'Email address of the invitee',
            'baselines' => 'Baselines',
            'baseline_id' => 'Baseline of controls to be suggested to invitee',
            'company_id' => 'company_id',
            'inviteUrl' => 'inviteUrl'
        );
    }
    
    public function __construct()
    {
        $this->company_id = Utils::contextCompanyId();
        $this->companyName = Utils::contextCompanyName();
        $this->fromEmail = userEmail();
        $names = Utils::getUserFirstLastName(userId());
        $this->fromName = $names['full_name'];
    }

    // do this once to create form
    public function initialize()
    {
        $this->invite_guid = Utils::create_guid();
        $mWebToken = new mWebToken();
        $this->inviteUrl = $mWebToken->createUrl($this->invite_guid,url('invite/signin'));
        $this->createMessage();
    }

    public function createMessage($msgId=1)
    {
        if ($msgId == 1) {
            $this->msgSubject = "You have been invited by " . $this->companyName . " to join ComplianceShield.";
            $this->msgBody = 'You have been invited by ' . $this->fromName . ' at ' . $this->companyName . ' to join ComplianceShield. '
                . 'Please click the link below to register your organization. Once registered, you will be given the option to '
                . '(1) adopt the security recommendations of ' . $this->companyName . ' or '
                . '(2) run our Getting Started Wizard to define your own program.<br/> '
                . '<br/> '
                . $this->inviteUrl . '<br/> '
                . '<br/> '
                . 'Please call 1.888.641.0500 if you have any questions. <br/>'
                . '<br/> ';
        } else if ($msgId == 2) {
            $this->msgSubject = "You have been invited by " . $this->companyName . " to join ComplianceShield.";
            $this->msgBody = 'You have been invited by ' . $this->fromName . ' at ' . $this->companyName . ' to join ComplianceShield. '
                . 'Please click the link below to register an account with this email address.  This invitation will expire in 14 days.<br/> '
                . '<br/> '
                . $this->inviteUrl . '<br/> '
                . '<br/> '
                . 'Please call 1.888.641.0500 if you have any questions. <br/>'
                . '<br/> ';
        }
    }

    public function getBaselines()
    {
        $systemBaselines = Baseline_Ex::getBaselineAsessment($count,NULL);
        $companyBaselines = Baseline_Ex::getBaselineAsessment($count,Utils::contextCompanyId());
        $this->baselines = array_merge($systemBaselines,$companyBaselines);
        return $this->baselines;
    }
    
    public function sendInvite() 
    {
        $msgBody = str_ireplace("[invitee_name]", $this->invitee_name, $this->salutation).$this->msgBody;
        $retVal = EmailUtils::sendMail($this->invitee_email, $this->msgSubject, str_ireplace("<br/>", "\n", $msgBody), $msgBody, $this->fromEmail, $this->fromName);
        if ($retVal) {
            $invite = new Invite();
            $invite->invite_guid = $this->invite_guid;
            $invite->baseline_id = $this->baseline_id;
            $invite->company_id = $this->company_id;
            $invite->user_id = userId();
            $invite->status = 1;
            $invite->accepted = 0;
            $invite->created_on = date('Y-m-d H:i:s');
            $invite->expiry_date = DateHelper::AddDays(NULL,'90');
            $invite->invitee_name = $this->invitee_name;
            $invite->invitee_email = $this->invitee_email;
            $invite->sent = 1;
            $invite->sent_date = date('Y-m-d');
            if (!$invite->save()) {
                $this->addError('invitee_name','Failed to save invite record, and thus could not send invite. Please contact support.');
                Yii::log('Failed to update invite record after sending email.','error','mInvite.sendInvite');
            }
        } else {
            $this->addError('invitee_email','Invite email could not be sent. Please verify invitee email address and try again, or contact support.');
        }
        return $retVal;
    }

    public function sendUserInvite()
    {
        $user = User::model()->findByAttributes(array('email'=>$this->invitee_email));
        if ($user) {
            $this->inviteUrl = url('user/login');
        } else {
            $invite = new Invite();
            $invite->invite_guid = substr(Utils::EncryptToken($this->invitee_email), 0, 128);
            $invite->company_id = $this->company_id;
            $invite->user_id = userId();
            $invite->status = 2;
            $invite->accepted = 0;
            $invite->created_on = date('Y-m-d H:i:s');
            $invite->expiry_date = DateHelper::AddDays(NULL,'14');
            $invite->invitee_name = $this->invitee_name;
            $invite->invitee_email = $this->invitee_email;
            $invite->sent = 1;
            $invite->sent_date = date('Y-m-d');
            $invite->save();
            $this->inviteUrl = url('user/registration/registration/t/'.$invite->invite_guid);
        }
        $this->createMessage(2);
        $msgBody = str_ireplace("[invitee_name]", $this->invitee_name, $this->salutation).$this->msgBody;
        $retVal = EmailUtils::sendMail($this->invitee_email, $this->msgSubject, str_ireplace("<br/>", "\n", $msgBody), $msgBody, $this->fromEmail, $this->fromName);
        if (!$retVal) {
            Yii::log('Invite email could not be sent to '.$this->invitee_email.' from '.$this->fromName.' at '.$this->companyName,'error','mInvite.sendInvite');
            $this->addError('invitee_email','Invite email could not be sent. Please verify invitee email address and try again, or contact support.');
        }
        return $retVal;
    }

    public static function getUserInvite($token) {
        $invite = Invite::model()->findByAttributes(array('invite_guid'=>$token, 'accepted'=>0, 'status'=>2));
        if ($invite) {
            if (date($invite->expiry_date) >= date('Y-m-d H:i:s')) {
                $retVal = $invite;
            } else {
                $invite = false;
            }
        }
        if (!$invite) {
            $invite = new Invite();
            $invite->addError('error', 'This invitation has expired or is invalid.  Please request for a new invitation.');
            $retVal = $invite;
        }
        return $retVal;
    }
}
