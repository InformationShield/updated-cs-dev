<?php

require_once __DIR__ . '/../vendor/PHPAESLib/AESCryptFileLib.php';
require_once __DIR__ . '/../vendor/PHPAESLib/aes256/MCryptAES256Implementation.php';

class mAESmcrypt
{
    public static function encryptFile($filePath,$password)
    {
        $retVal = false;
        $mcrypt = new MCryptAES256Implementation();
        $lib = new AESCryptFileLib($mcrypt);
        $aesFile = $filePath.".aes";
        if (file_exists($aesFile)) unlink($aesFile);
        $lib->encryptFile($filePath, $password, $aesFile);
        $retVal = (file_exists($aesFile));
        return $retVal;
    }

    public static function decryptFile($filePath,$password)
    {
        $retVal = false;
        $mcrypt = new MCryptAES256Implementation();
        $lib = new AESCryptFileLib($mcrypt);
        if (file_exists($filePath)) unlink($filePath);
        $lib->decryptFile($filePath.".aes", $password, $filePath);
        $retVal = (file_exists($filePath));
        return $retVal;
    }
}