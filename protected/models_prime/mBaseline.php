<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class mBaseline extends FormModel
{
	public $baseline_id;
	public $company_id;
	public $user_id;
	public $special_type;
	public $status;
	public $sort_order;
	public $created_on;
	public $baseline_name;
	public $baseline_description;

	public function rules() {
		return array(
			array('baseline_id, company_id, user_id, special_type, status, sort_order, created_on, baseline_name, baseline_description', 'safe'),
		);
	}

	public function __construct($baseline_id = null) {
		if ($baseline_id) {
			$this->getBaseLine($baseline_id);
		}
	}

	public function initialize()
	{
	}

	public function getBaseLine($baseline_id) {
		$retVal = NULL;
		$baseline = Baseline_Ex::getBaseline($baseline_id);
		if ($baseline) {
			$this->attributes = $baseline;
			$retVal = true;
		}
		return $retVal;
	}

	public static function copyBaseline($baseline_id, $baseline_name, $baseline_description) {
		$baseline = new Baseline();
		$baseline->company_id = Utils::contextCompanyId();
		$baseline->user_id = userId();
		$baseline->baseline_name = $baseline_name;
		$baseline->baseline_description = $baseline_description;
		$baseline->isNewRecord = true;
		$baseline->save();
		ControlBaseline_Ex::copyFromBaseline($baseline_id, $baseline->baseline_id, Utils::contextCompanyId(), userId());
		return $baseline->baseline_id;
	}

	public static function getStockBaselines() {
		$retVal = null;
		$baselines = Baseline_Ex::getStockBaseline();
		return $baselines;
	}
}