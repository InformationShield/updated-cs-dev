<?php

class mAppApi {
	public $objectName;
	public $objectId;
	public $objectField;
    public $objectCondition;

    // $resultData can be used by API methods so that direct callers can get the
    // non converted data that is typically parse to json or csv format before sent
    public $resultData = null;
    public $userId = null;

    // send response parameters (output)
    public $status = Rest::REST_OK;
    public $body = '';
    public $content_type = 'text/html';

    public $files;

    protected $request_vars = array();
    protected $data = '';
    protected $http_accept = 'json';
    protected $method = 'get';


    public function __construct() {
		$this->objectName = ucwords(getGet('object'));
		$this->objectId = getGet('objectid');
        $this->objectCondition = getGet('objectid');
		$this->objectField = getGet('field');
        $this->checkSecretKey();
        $this->processRequest();
	}

    private function processRequest()
    {
        $request_method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->method = $request_method;

        // get request accept type
        $http_accept = strtolower($_SERVER['HTTP_ACCEPT']);
        if (strpos($http_accept, 'json') !== false) {
            $this->http_accept = 'json';
        } else if (strpos($http_accept, 'html') !== false) {
            $this->http_accept = 'html';
        } else if (strpos($http_accept, 'xml') !== false) {
            $this->http_accept = 'xml';
        } else if (strpos($http_accept, 'text') !== false) {
            $this->http_accept = 'text';
        } else {
            $this->http_accept = $http_accept;
        }

        // we'll store our data here
        $data = array();
        switch ($request_method)
        {
            // gets are easy...
            case 'get':
                $data = $_GET;
                break;
            // so are posts
            case 'post':
                $data = $_POST;
                break;
            // here's the tricky bit...
            case 'put':
                // basically, we read a string from PHP's special input location,
                // and then parse it out into an array via parse_str... per the PHP docs:
                // Parses str  as if it were the query string passed via a URL and sets
                // variables in the current scope.
                parse_str(file_get_contents('php://input'), $put_vars);
                $data = $put_vars;
                break;
        }

        // set the raw data, so we can access it if needed (there may be other pieces to your requests)
        $this->request_vars = $data;

        if(isset($data['data'])) {  // jQuery ajax calls pass content with "data" variable
            // translate the JSON to an Object for use however you want
            $this->data = json_decode($data['data']);
        }

        if (isset($_FILES)) {
            $this->files = $_FILES;
        }

    }

    public static function Authenticate(&$resultData, $params)
    {
        $retVal = Rest::REST_INTERNAL_SERVER_ERROR;
        $uIdent = new UserIdentity($params["userlogin"], $params["password"]);
        $result = $uIdent->authenticate();
        if ($result) {
            $uid = $uIdent->id;

            $expiry = DateTimeTools::XTimeFromNowEpoch('6 hours');
            $tokenPrep = json_encode(array($uid, $expiry));
            $token = Utils::EncryptToken($tokenPrep);
            $resultData = $token;
            $retVal = Rest::REST_OK;
        } else {
            $retVal = Rest::REST_FORBIDDEN;
        }
        return $retVal;
    }

    // Check if known application is making the request
    public function checkApplicationKey()
    {
        return true; /************************ Implement this ****************************/

        $retVal = false;
        if(isset($_SERVER['HTTP_X_APP_APPLICATION_KEY'])) {
            $secretKeyInHeader = $_SERVER['HTTP_X_APP_APPLICATION_KEY'];
            if($secretKeyInHeader == APP_APPLICATION_KEY) {
                $retVal = true;
            }
        }
        if (!$retVal) {
			$this->status = Rest::REST_FORBIDDEN;
        }
        return $retVal;
    }

    // Check if user already authenticated
    private function checkSecretKey() {
        $retVal = false;
        if (isset($_SERVER['HTTP_X_APP_SECRET_KEY'])) {
            $secretKey = $_SERVER['HTTP_X_APP_SECRET_KEY'];
            $tokens = json_decode(Utils::DecryptToken($secretKey));
            $expiry = $tokens[1];
            $now = DateTimeTools::XTimeFromNowEpoch();
            if ($expiry > $now) {
                $this->userId = $tokens[0];
                setSessionVar('userId', $this->userId);
                setSessionVar('clientMode', 'api');
                $retVal = true;
            }
        }
        if (!$retVal) {
            $this->status = Rest::REST_FORBIDDEN;
        }
        return $retVal;
    }

    public function sendResponse() {
        $this->convertResultData();
        setSessionVar('userId', null);
        Rest::sendResponse($this->status, $this->body, $this->content_type);
    }

    /*
     *  Generic method to convert $this->resultData to the needed output type
     */
    public function convertResultData() {

        switch ($this->http_accept) {
            case 'json':
                $this->content_type = 'application/json';
                $this->body = json_encode($this->resultData);
                break;
            case "html":
                $this->content_type = 'text/html';
                $this->body = "" . $this->resultData;
                break;
            default:
                $this->content_type = 'text/html';
                $this->body = '';
                break;
        }
        return $this->status;
    }

}
