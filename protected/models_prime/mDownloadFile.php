<?php

class mDownloadFile
{
    const None=0;
    const Evidence=1;
    const Policy=2;
    const PolicyLibrary=3;
    const Training=4;
    const TrainingLibrary=5;
    const ControlBaselineExport=6;
    const ControlBaselineExportToWORD=7;

    public static function send($id,$downloadType,$company_id)
    {
        switch ($downloadType) {

            case mDownloadFile::Evidence:
                $model = Evidence::model()->findByAttributes(array('evidence_id' => $id, 'company_id' => $company_id));
                if (isset($model)) {
                    $message = Evidence_Ex::downloadFile($model); // send headers and exits
                } else {
                    $message = 'The evidence record was not found.';
                }
                break;

            case mDownloadFile::Policy:
                $model = Policy::model()->findByAttributes(array('policy_id' => $id, 'company_id' => $company_id));
                if (isset($model)) {
                    $message = Policy_Ex::downloadFile($model); // send headers and exits
                } else {
                    $message = 'The policy record was not found.';
                }
                break;

            case mDownloadFile::PolicyLibrary:
                $model = PolicyLibrary::model()->findByAttributes(array('policy_library_id' => $id));
                if ($model->license_level <= getSessionVar('license_level')) {
					if (isset($model)) {
						$message = PolicyLibrary_Ex::downloadFile($model); // send headers and exits
					} else {
						$message = 'The policy library record was not found.';
					}
				} else {
					$message = "Available in the paid version.";
				}
                break;

            case mDownloadFile::Training:
                $model = Training::model()->findByAttributes(array('training_id' => $id, 'company_id' => $company_id));
                if (isset($model)) {
                    $message = Training_Ex::downloadFile($model); // send headers and exits
                } else {
                    $message = 'The training record was not found.';
                }
                break;

            case mDownloadFile::TrainingLibrary:
                $model = TrainingLibrary::model()->findByAttributes(array('training_library_id' => $id));
                if (isset($model)) {
                    $message = TrainingLibrary_Ex::downloadFile($model); // send headers and exits
                } else {
                    $message = 'The training library record was not found.';
                }
                break;
            
            case mDownloadFile::ControlBaselineExport:
                $data = ControlBaseline_Ex::createCSVReportData($id,$company_id,$filename);
                if (count($data) && $filename) {
                    mDownloadFile::outputCSV($data,str_ireplace('.csv','',$filename));
                    Yii::app()->end();
                } else {
                    $message = 'The report record was not found.';
                }
                break;
                
            case mDownloadFile::ControlBaselineExportToWORD:
                $model = ExportReport::model()->findByPk($id);
                if (isset($model) && $model->company_id == $company_id) {
                    $filename = $model->file_name;
                    $reportData = $model->report;
                    mDownloadFile::outputWORD($reportData,str_ireplace('.docx','',$filename));
                    $model->delete(); // no longer need report record
                    Yii::app()->end();
                } else {
                    $message = 'The report record was not found.';
                }
                break;
            
            default:
                $message = "Unknown download file type.";
                break;
        }
        return $message;
    }

    public static function outputCSV($data,$filename='export')
    {
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $outputBuffer = fopen("php://output", 'w');
        foreach($data as $val) {
            fputcsv($outputBuffer, $val);
        }
        fclose($outputBuffer);
    }
    
    public static function outputWORD($data, $filename = 'export')
    {
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $filename . '.docx"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        echo $data;
    }

}