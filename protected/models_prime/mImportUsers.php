<?php

require_once __DIR__.'/../vendor/Excel/reader.php';

class mImportUsers
{
    private $companyId = null;
    private $currentUsers = null;

    public function importUsersFromExcel($excelFile,$companyId)
    {
        $this->companyId = $companyId;
        $line_count = 0;
        $data = new Spreadsheet_Excel_Reader();
        // Set output Encoding.
        $data->setOutputEncoding('CP1251');
        /***
         * if you want you can change 'iconv' to mb_convert_encoding:
         * $data->setUTFEncoder('mb');
         *
         **/
        /***
         * By default rows & cols indeces start with 1
         * For change initial index use:
         * $data->setRowColOffset(0);
         *
         **/
        /***
         *  Some function for formatting output.
         * $data->setDefaultFormat('%.2f');
         * setDefaultFormat - set format for columns with unknown formatting
         *
         * $data->setColumnFormat(4, '%.3f');
         * setColumnFormat - set format for column (apply only to number fields)
         *
         **/

        $data->read($excelFile); // throws exception if fails
        
        $rows = array();
        $errors = array();
        
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            // params
            $first_name = (isset($data->sheets[0]['cells'][$i][1])) ? trim($data->sheets[0]['cells'][$i][1]) : '';
            $last_name = (isset($data->sheets[0]['cells'][$i][2])) ? trim($data->sheets[0]['cells'][$i][2]) : '';
            $email = (isset($data->sheets[0]['cells'][$i][3])) ? trim($data->sheets[0]['cells'][$i][3]) : '';
            $departament = (isset($data->sheets[0]['cells'][$i][4])) ? trim($data->sheets[0]['cells'][$i][4]) : '';
            $profile_id = (isset($data->sheets[0]['cells'][$i][5])) ? trim($data->sheets[0]['cells'][$i][5]) : '';
            
            // document validation
            if(empty($first_name)){ // missing first name
                $errors['row_' . $i][] = "First Name is missing in row " . $i;
            }
            
            if(empty($last_name)){ // missing last name
                $errors['row_' . $i][] = "Last Name is missing in row " . $i;
            }
            
            if(empty($email)){ // missing email
                $errors['row_' . $i][] = "Email is missing in row " . $i;
            }else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ // wrong email
                $errors['row_' . $i][] = $email . " is not a valid email address in row " . $i;
            }

            // profile ids are optional so if empty string set to null
            if (empty($profile_id)) {
                $profile_id = null;
            } else if(strpos($profile_id, ",") !== false) { // profile id has comma separated values
                $profile_id = explode(",", $profile_id);
            } else if (is_numeric($profile_id)) {
                $profile_id = array(intval($profile_id));
            }
            
            if($profile_id !== null && !is_array($profile_id)){ // if profile id is not an integer and not an array
                $errors['row_' . $i][] = "Profile Id must be an integer or comma separated value in row " . $i;
            }

            if(!isset($errors['row_' . $i])){
                // add row
                $rows[] = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'departament' => $departament,
                    'profile_id' => $profile_id,
                );
            }
        }

        // delete uploaded file
        unlink($excelFile);

        // throw on errors
        if(!empty($errors)){
            throw new Exception(json_encode($errors));
        }
        
        $rowNum = 2;
        foreach ($rows as $row) {
            $model = new Employee();
            $model->company_role_id = Cons::ROLE_USER;
            $model->company_id = $companyId;
            $model->firstname = $row['first_name'];
            $model->lastname = $row['last_name'];
            // let's save email addresses as lower case only
            $model->email = strtolower($row['email']);
            $model->department_name = $row['departament'];

            // if email address already exist then assume update of that record
            $employee_id = $this->checkExists($model->email);
            if ($employee_id !== false) {
                $model->employee_id = $employee_id;
                $model->isNewRecord = FALSE;
            }            
            
            // create or update
            if ($model->save()) {
                // bind profiles
                $this->bindUserProfiles($companyId, $model->employee_id, $row['profile_id']);
            } else {  
                $errors['row'][] = 'Unable to save record in database for row '.$rowNum.' with FirstName: ' . $model->firstname . ', LastName: ' . $model->lastname . ', Email: ' . $model->email;
            }
            $rowNum++;
        }

        // throw error
        if(!empty($errors)){
            throw new Exception(json_encode($errors));
        }
    }

    // checks if user exist and return false or the employee_id of user
    private function checkExists($email)
    {
        // if haven't got the data yet for the search get it once here
        if (is_null($this->currentUsers)) {
            // get current users to update existing users by email address instead of adding duplicates
            $select = "SELECT e.employee_id, e.email";
            $this->currentUsers = Employee_Ex::getEmployee($count, $this->companyId, null, 0, 0, $select);
            if (empty($this->currentUsers)) {
                // we need this to be an array
                $this->currentUsers = array();
            } else {
                // want simple array of lowercase email addresses keyed by employee_id
                $this->currentUsers = array_column($this->currentUsers, 'email', 'employee_id');
                $this->currentUsers = array_map('strtolower', $this->currentUsers);
            }
        }

        $employee_id = false;
        if (!empty($this->currentUsers)) {
            $employee_id = array_search($email, $this->currentUsers);
        }
        return $employee_id;
    }
    
    // bind selected profiles to user
    private function bindUserProfiles($companyId, $employeeId, $profileIds) {
        if($profileIds && is_array($profileIds)){
            // get all profiles with given id's
            $inQuery = "";
            foreach ($profileIds as $k => $id){
                $inQuery .= ":p$k,";
            }
            $inQuery = substr($inQuery, 0, -1);

            $c = Yii::app()->db
                    ->createCommand("SELECT user_profile_id, profile_id FROM `user_profile` WHERE company_id = :company_id AND profile_id IN ($inQuery)")
                    ->bindParam(":company_id", $companyId, PDO::PARAM_INT);
            foreach ($profileIds as $k => $id){
                $c->bindValue(":p$k", $id, PDO::PARAM_INT);
            }
            $profiles = $c->query()->readAll();

            if(!empty($profiles)){
                $profile_to_bind = array();
                foreach ($profiles as $profile) {
                    $profile_to_bind[] = $profile['user_profile_id'];
                }

                UserProfile_Ex::saveUserProfile($employeeId, $profile_to_bind);
            }
        }
    }
}