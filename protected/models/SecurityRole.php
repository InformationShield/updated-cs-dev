<?php

/**
 * This is the model class for table "security_role".
 *
 * The followings are the available columns in table 'security_role':
 * @property integer $security_role_id
 * @property integer $company_id
 * @property integer $employee_id
 * @property integer $role_status
 * @property string $role_title
 * @property string $role_description
 * @property string $role_department
 * @property string $role_summary
 * @property integer $status
 * @property string $created_on
 *
 * The followings are the available model relations:
 * @property ControlBaseline[] $controlBaselines
 * @property Company $company
 * @property Employee $employee
 * @property SecurityRoleStatus $roleStatus
 */
class SecurityRole extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'security_role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, employee_id, role_status, status', 'numerical', 'integerOnly'=>true),
			array('role_title, role_department', 'length', 'max'=>100),
			array('role_description, role_summary, created_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('security_role_id, company_id, employee_id, role_status, role_title, role_description, role_department, role_summary, status, created_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'controlBaselines' => array(self::HAS_MANY, 'ControlBaseline', 'security_role_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
			'roleStatus' => array(self::BELONGS_TO, 'SecurityRoleStatus', 'role_status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'security_role_id' => 'Security Role',
			'company_id' => 'Company',
			'employee_id' => 'Employee',
			'role_status' => 'Role Status',
			'role_title' => 'Role Title',
			'role_description' => 'Role Description',
			'role_department' => 'Role Department',
			'role_summary' => 'Role Summary',
			'status' => 'Status',
			'created_on' => 'Created On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('security_role_id',$this->security_role_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('role_status',$this->role_status);
		$criteria->compare('role_title',$this->role_title,true);
		$criteria->compare('role_description',$this->role_description,true);
		$criteria->compare('role_department',$this->role_department,true);
		$criteria->compare('role_summary',$this->role_summary,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_on',$this->created_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SecurityRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
