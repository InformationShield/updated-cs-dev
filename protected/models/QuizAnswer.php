<?php

/**
 * This is the model class for table "quiz_answer".
 *
 * The followings are the available columns in table 'quiz_answer':
 * @property integer $quiz_answer_id
 * @property integer $quiz_question_id
 * @property integer $taken_number
 * @property integer $quiz_id
 * @property integer $user_id
 * @property integer $company_id
 * @property integer $answer
 * @property integer $correct
 * @property integer $score
 * @property string $answer_timestamp
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Quiz $quiz
 * @property QuizQuestion $quizQuestion
 * @property Users $user
 */
class QuizAnswer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quiz_answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quiz_question_id, taken_number, quiz_id, user_id, company_id', 'required'),
			array('quiz_question_id, taken_number, quiz_id, user_id, company_id, answer, correct, score', 'numerical', 'integerOnly'=>true),
			array('answer_timestamp', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('quiz_answer_id, quiz_question_id, taken_number, quiz_id, user_id, company_id, answer, correct, score, answer_timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'quiz' => array(self::BELONGS_TO, 'Quiz', 'quiz_id'),
			'quizQuestion' => array(self::BELONGS_TO, 'QuizQuestion', 'quiz_question_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'quiz_answer_id' => 'Quiz Answer',
			'quiz_question_id' => 'Quiz Question',
			'taken_number' => 'Taken Number',
			'quiz_id' => 'Quiz',
			'user_id' => 'User',
			'company_id' => 'Company',
			'answer' => 'Answer',
			'correct' => 'Correct',
			'score' => 'Score',
			'answer_timestamp' => 'Answer Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('quiz_answer_id',$this->quiz_answer_id);
		$criteria->compare('quiz_question_id',$this->quiz_question_id);
		$criteria->compare('taken_number',$this->taken_number);
		$criteria->compare('quiz_id',$this->quiz_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('answer',$this->answer);
		$criteria->compare('correct',$this->correct);
		$criteria->compare('score',$this->score);
		$criteria->compare('answer_timestamp',$this->answer_timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuizAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
