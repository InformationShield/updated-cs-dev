<?php

/**
 * This is the model class for table "training".
 *
 * The followings are the available columns in table 'training':
 * @property integer $training_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $training_library_id
 * @property integer $training_type
 * @property integer $approved
 * @property integer $training_status_id
 * @property integer $status
 * @property integer $file_size
 * @property string $file_ext
 * @property string $file_type
 * @property string $file_name
 * @property string $doc_guid
 * @property string $doc_title
 * @property string $created_on
 * @property string $publish_date
 * @property string $author
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property TrainingLibrary $trainingLibrary
 * @property TrainingStatus $trainingStatus
 * @property Users $user
 * @property UserProfileTraining[] $userProfileTrainings
 * @property UserTrainingTracking[] $userTrainingTrackings
 * @property VideoTraining[] $videoTrainings
 */
class Training extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'training';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, user_id, doc_guid, doc_title', 'required'),
			array('company_id, user_id, training_library_id, training_type, approved, training_status_id, status, file_size', 'numerical', 'integerOnly'=>true),
			array('file_ext', 'length', 'max'=>10),
			array('file_type', 'length', 'max'=>127),
			array('file_name, doc_title, author', 'length', 'max'=>256),
			array('doc_guid', 'length', 'max'=>36),
			array('created_on, publish_date, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('training_id, company_id, user_id, training_library_id, training_type, approved, training_status_id, status, file_size, file_ext, file_type, file_name, doc_guid, doc_title, created_on, publish_date, author, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'trainingLibrary' => array(self::BELONGS_TO, 'TrainingLibrary', 'training_library_id'),
			'trainingStatus' => array(self::BELONGS_TO, 'TrainingStatus', 'training_status_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'userProfileTrainings' => array(self::HAS_MANY, 'UserProfileTraining', 'training_id'),
			'userTrainingTrackings' => array(self::HAS_MANY, 'UserTrainingTracking', 'training_id'),
			'videoTrainings' => array(self::HAS_MANY, 'VideoTraining', 'training_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'training_id' => 'Training',
			'company_id' => 'Company',
			'user_id' => 'User',
			'training_library_id' => 'Training Library',
			'training_type' => 'Training Type',
			'approved' => 'Approved',
			'training_status_id' => 'Training Status',
			'status' => 'Status',
			'file_size' => 'File Size',
			'file_ext' => 'File Ext',
			'file_type' => 'File Type',
			'file_name' => 'File Name',
			'doc_guid' => 'Doc Guid',
			'doc_title' => 'Doc Title',
			'created_on' => 'Created On',
			'publish_date' => 'Publish Date',
			'author' => 'Author',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('training_id',$this->training_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('training_library_id',$this->training_library_id);
		$criteria->compare('training_type',$this->training_type);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('training_status_id',$this->training_status_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('file_size',$this->file_size);
		$criteria->compare('file_ext',$this->file_ext,true);
		$criteria->compare('file_type',$this->file_type,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('doc_guid',$this->doc_guid,true);
		$criteria->compare('doc_title',$this->doc_title,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('publish_date',$this->publish_date,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Training the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
