<?php

/**
 * This is the model class for table "policy".
 *
 * The followings are the available columns in table 'policy':
 * @property integer $policy_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $policy_library_id
 * @property integer $cat_id
 * @property integer $approved
 * @property integer $policy_type
 * @property integer $policy_status_id
 * @property integer $status
 * @property integer $file_size
 * @property string $file_ext
 * @property string $file_type
 * @property string $file_name
 * @property string $doc_guid
 * @property string $doc_title
 * @property string $created_on
 * @property string $publish_date
 * @property string $expiry_date
 * @property string $author
 * @property string $keywords
 * @property string $description
 *
 * The followings are the available model relations:
 * @property ControlCategory $cat
 * @property Company $company
 * @property PolicyLibrary $policyLibrary
 * @property PolicyStatus $policyStatus
 * @property Users $user
 * @property UserPolicyTracking[] $userPolicyTrackings
 * @property UserProfilePolicy[] $userProfilePolicies
 */
class Policy extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'policy';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, user_id, file_name, doc_guid, doc_title', 'required'),
			array('company_id, user_id, policy_library_id, cat_id, approved, policy_type, policy_status_id, status, file_size', 'numerical', 'integerOnly'=>true),
			array('file_ext', 'length', 'max'=>10),
			array('file_type', 'length', 'max'=>127),
			array('file_name, doc_title, author', 'length', 'max'=>256),
			array('doc_guid', 'length', 'max'=>36),
			array('keywords', 'length', 'max'=>2000),
			array('created_on, publish_date, expiry_date, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('policy_id, company_id, user_id, policy_library_id, cat_id, approved, policy_type, policy_status_id, status, file_size, file_ext, file_type, file_name, doc_guid, doc_title, created_on, publish_date, expiry_date, author, keywords, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cat' => array(self::BELONGS_TO, 'ControlCategory', 'cat_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'policyLibrary' => array(self::BELONGS_TO, 'PolicyLibrary', 'policy_library_id'),
			'policyStatus' => array(self::BELONGS_TO, 'PolicyStatus', 'policy_status_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'userPolicyTrackings' => array(self::HAS_MANY, 'UserPolicyTracking', 'policy_id'),
			'userProfilePolicies' => array(self::HAS_MANY, 'UserProfilePolicy', 'policy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'policy_id' => 'Policy',
			'company_id' => 'Company',
			'user_id' => 'User',
			'policy_library_id' => 'Policy Library',
			'cat_id' => 'Cat',
			'approved' => 'Approved',
			'policy_type' => 'Policy Type',
			'policy_status_id' => 'Policy Status',
			'status' => 'Status',
			'file_size' => 'File Size',
			'file_ext' => 'File Ext',
			'file_type' => 'File Type',
			'file_name' => 'File Name',
			'doc_guid' => 'Doc Guid',
			'doc_title' => 'Doc Title',
			'created_on' => 'Created On',
			'publish_date' => 'Publish Date',
			'expiry_date' => 'Expiry Date',
			'author' => 'Author',
			'keywords' => 'Keywords',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('policy_id',$this->policy_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('policy_library_id',$this->policy_library_id);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('policy_type',$this->policy_type);
		$criteria->compare('policy_status_id',$this->policy_status_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('file_size',$this->file_size);
		$criteria->compare('file_ext',$this->file_ext,true);
		$criteria->compare('file_type',$this->file_type,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('doc_guid',$this->doc_guid,true);
		$criteria->compare('doc_title',$this->doc_title,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('publish_date',$this->publish_date,true);
		$criteria->compare('expiry_date',$this->expiry_date,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Policy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
