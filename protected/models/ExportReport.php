<?php

/**
 * This is the model class for table "export_report".
 *
 * The followings are the available columns in table 'export_report':
 * @property integer $export_report_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $status
 * @property string $created_on
 * @property string $report_type
 * @property string $file_ext
 * @property string $file_type
 * @property string $file_name
 * @property string $title
 * @property string $data
 * @property string $report
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Users $user
 */
class ExportReport extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'export_report';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('report_type, file_name, title', 'required'),
			array('company_id, user_id, status', 'numerical', 'integerOnly'=>true),
			array('report_type', 'length', 'max'=>32),
			array('file_ext', 'length', 'max'=>10),
			array('file_type', 'length', 'max'=>127),
			array('file_name, title', 'length', 'max'=>256),
			array('created_on, data, report', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('export_report_id, company_id, user_id, status, created_on, report_type, file_ext, file_type, file_name, title, data, report', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'export_report_id' => 'Export Report',
			'company_id' => 'Company',
			'user_id' => 'User',
			'status' => 'Status',
			'created_on' => 'Created On',
			'report_type' => 'Report Type',
			'file_ext' => 'File Ext',
			'file_type' => 'File Type',
			'file_name' => 'File Name',
			'title' => 'Title',
			'data' => 'Data',
			'report' => 'Report',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('export_report_id',$this->export_report_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('report_type',$this->report_type,true);
		$criteria->compare('file_ext',$this->file_ext,true);
		$criteria->compare('file_type',$this->file_type,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('report',$this->report,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExportReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
