<?php

/**
 * This is the model class for table "incident_type".
 *
 * The followings are the available columns in table 'incident_type':
 * @property integer $incident_type_id
 * @property integer $company_id
 * @property string $incident_type
 * @property string $incident_description
 *
 * The followings are the available model relations:
 * @property Incident[] $incidents
 * @property Company $company
 */
class IncidentType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'incident_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('incident_type', 'required'),
			array('company_id', 'numerical', 'integerOnly'=>true),
			array('incident_type', 'length', 'max'=>80),
			array('incident_description', 'length', 'max'=>2000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('incident_type_id, company_id, incident_type, incident_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'incidents' => array(self::HAS_MANY, 'Incident', 'incident_type_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'incident_type_id' => 'Incident Type',
			'company_id' => 'Company',
			'incident_type' => 'Incident Type',
			'incident_description' => 'Incident Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('incident_type_id',$this->incident_type_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('incident_type',$this->incident_type,true);
		$criteria->compare('incident_description',$this->incident_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IncidentType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
