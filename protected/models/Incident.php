<?php

/**
 * This is the model class for table "incident".
 *
 * The followings are the available columns in table 'incident':
 * @property integer $incident_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $employee_id
 * @property integer $incident_type_id
 * @property integer $incident_status_id
 * @property string $updated_on
 * @property string $created_on
 * @property string $system
 * @property string $summary
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Users $user
 * @property IncidentStatus $incidentStatus
 * @property IncidentType $incidentType
 */
class Incident extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'incident';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, summary', 'required'),
			array('company_id, user_id, employee_id, incident_type_id, incident_status_id', 'numerical', 'integerOnly'=>true),
			array('system', 'length', 'max'=>256),
			array('updated_on, created_on, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('incident_id, company_id, user_id, employee_id, incident_type_id, incident_status_id, updated_on, created_on, system, summary, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'incidentStatus' => array(self::BELONGS_TO, 'IncidentStatus', 'incident_status_id'),
			'incidentType' => array(self::BELONGS_TO, 'IncidentType', 'incident_type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'incident_id' => 'Incident',
			'company_id' => 'Company',
			'user_id' => 'User',
			'employee_id' => 'Employee',
			'incident_type_id' => 'Incident Type',
			'incident_status_id' => 'Incident Status',
			'updated_on' => 'Updated On',
			'created_on' => 'Created On',
			'system' => 'System',
			'summary' => 'Summary',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('incident_id',$this->incident_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('incident_type_id',$this->incident_type_id);
		$criteria->compare('incident_status_id',$this->incident_status_id);
		$criteria->compare('updated_on',$this->updated_on,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('system',$this->system,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Incident the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
