<?php

/**
 * This is the model class for table "invite".
 *
 * The followings are the available columns in table 'invite':
 * @property integer $invite_id
 * @property string $invite_guid
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $status
 * @property integer $invited_company_id
 * @property integer $invited_user_id
 * @property integer $baseline_id
 * @property integer $sent
 * @property integer $accepted
 * @property integer $adopted_baseline
 * @property string $created_on
 * @property string $sent_date
 * @property string $expiry_date
 * @property string $accepted_date
 * @property string $invitee_name
 * @property string $invitee_email
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Users $user
 */
class Invite extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invite';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invitee_name, invitee_email', 'required'),
			array('company_id, user_id, status, invited_company_id, invited_user_id, baseline_id, sent, accepted, adopted_baseline', 'numerical', 'integerOnly'=>true),
			array('invite_guid', 'length', 'max'=>128),
			array('invitee_name, invitee_email', 'length', 'max'=>256),
			array('created_on, sent_date, expiry_date, accepted_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('invite_id, invite_guid, company_id, user_id, status, invited_company_id, invited_user_id, baseline_id, sent, accepted, adopted_baseline, created_on, sent_date, expiry_date, accepted_date, invitee_name, invitee_email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'invite_id' => 'Invite',
			'invite_guid' => 'Invite Guid',
			'company_id' => 'Company',
			'user_id' => 'User',
			'status' => 'Status',
			'invited_company_id' => 'Invited Company',
			'invited_user_id' => 'Invited User',
			'baseline_id' => 'Baseline',
			'sent' => 'Sent',
			'accepted' => 'Accepted',
			'adopted_baseline' => 'Adopted Baseline',
			'created_on' => 'Created On',
			'sent_date' => 'Sent Date',
			'expiry_date' => 'Expiry Date',
			'accepted_date' => 'Accepted Date',
			'invitee_name' => 'Invitee Name',
			'invitee_email' => 'Invitee Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('invite_id',$this->invite_id);
		$criteria->compare('invite_guid',$this->invite_guid,true);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('invited_company_id',$this->invited_company_id);
		$criteria->compare('invited_user_id',$this->invited_user_id);
		$criteria->compare('baseline_id',$this->baseline_id);
		$criteria->compare('sent',$this->sent);
		$criteria->compare('accepted',$this->accepted);
		$criteria->compare('adopted_baseline',$this->adopted_baseline);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('sent_date',$this->sent_date,true);
		$criteria->compare('expiry_date',$this->expiry_date,true);
		$criteria->compare('accepted_date',$this->accepted_date,true);
		$criteria->compare('invitee_name',$this->invitee_name,true);
		$criteria->compare('invitee_email',$this->invitee_email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Invite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
