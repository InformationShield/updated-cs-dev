<?php

/**
 * This is the model class for table "user_training_tracking".
 *
 * The followings are the available columns in table 'user_training_tracking':
 * @property integer $user_training_tracking_id
 * @property integer $user_id
 * @property integer $company_id
 * @property integer $training_id
 * @property integer $ack
 * @property integer $view
 * @property string $ack_date
 * @property string $view_date
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Training $training
 * @property Users $user
 */
class UserTrainingTracking extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_training_tracking';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, company_id, training_id', 'required'),
			array('user_id, company_id, training_id, ack, view', 'numerical', 'integerOnly'=>true),
			array('ack_date, view_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_training_tracking_id, user_id, company_id, training_id, ack, view, ack_date, view_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'training' => array(self::BELONGS_TO, 'Training', 'training_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_training_tracking_id' => 'User Training Tracking',
			'user_id' => 'User',
			'company_id' => 'Company',
			'training_id' => 'Training',
			'ack' => 'Ack',
			'view' => 'View',
			'ack_date' => 'Ack Date',
			'view_date' => 'View Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_training_tracking_id',$this->user_training_tracking_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('training_id',$this->training_id);
		$criteria->compare('ack',$this->ack);
		$criteria->compare('view',$this->view);
		$criteria->compare('ack_date',$this->ack_date,true);
		$criteria->compare('view_date',$this->view_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserTrainingTracking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
