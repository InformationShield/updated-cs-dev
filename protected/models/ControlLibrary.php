<?php

/**
 * This is the model class for table "control_library".
 *
 * The followings are the available columns in table 'control_library':
 * @property integer $control_library_id
 * @property integer $control_id
 * @property integer $cat_id
 * @property string $cat_code
 * @property string $cat_sub_code
 * @property integer $sort_order
 * @property integer $priority
 * @property integer $weighting
 * @property integer $status
 * @property string $created_on
 * @property string $title
 * @property integer $control_status_id
 * @property string $jobrole
 * @property integer $score
 * @property string $evidence
 * @property string $rel_policy
 * @property string $detail
 * @property string $guidance
 * @property string $reference
 * @property string $question
 *
 * The followings are the available model relations:
 * @property ControlCategory $cat
 * @property ControlStatus $controlStatus
 */
class ControlLibrary extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'control_library';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('control_id, cat_id, cat_code, cat_sub_code, title, jobrole', 'required'),
			array('control_id, cat_id, sort_order, priority, weighting, status, control_status_id, score', 'numerical', 'integerOnly'=>true),
			array('cat_code', 'length', 'max'=>2),
			array('cat_sub_code', 'length', 'max'=>5),
			array('title', 'length', 'max'=>255),
			array('jobrole', 'length', 'max'=>100),
			array('evidence', 'length', 'max'=>512),
			array('created_on, rel_policy, detail, guidance, reference, question', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('control_library_id, control_id, cat_id, cat_code, cat_sub_code, sort_order, priority, weighting, status, created_on, title, control_status_id, jobrole, score, evidence, rel_policy, detail, guidance, reference, question', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cat' => array(self::BELONGS_TO, 'ControlCategory', 'cat_id'),
			'controlStatus' => array(self::BELONGS_TO, 'ControlStatus', 'control_status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'control_library_id' => 'Control Library',
			'control_id' => 'Control',
			'cat_id' => 'Cat',
			'cat_code' => 'Cat Code',
			'cat_sub_code' => 'Cat Sub Code',
			'sort_order' => 'Sort Order',
			'priority' => 'Priority',
			'weighting' => 'Weighting',
			'status' => 'Status',
			'created_on' => 'Created On',
			'title' => 'Title',
			'control_status_id' => 'Control Status',
			'jobrole' => 'Jobrole',
			'score' => 'Score',
			'evidence' => 'Evidence',
			'rel_policy' => 'Rel Policy',
			'detail' => 'Detail',
			'guidance' => 'Guidance',
			'reference' => 'Reference',
			'question' => 'Question',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('control_library_id',$this->control_library_id);
		$criteria->compare('control_id',$this->control_id);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('cat_code',$this->cat_code,true);
		$criteria->compare('cat_sub_code',$this->cat_sub_code,true);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('weighting',$this->weighting);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('control_status_id',$this->control_status_id);
		$criteria->compare('jobrole',$this->jobrole,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('evidence',$this->evidence,true);
		$criteria->compare('rel_policy',$this->rel_policy,true);
		$criteria->compare('detail',$this->detail,true);
		$criteria->compare('guidance',$this->guidance,true);
		$criteria->compare('reference',$this->reference,true);
		$criteria->compare('question',$this->question,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ControlLibrary the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
