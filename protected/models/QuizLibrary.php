<?php

/**
 * This is the model class for table "quiz_library".
 *
 * The followings are the available columns in table 'quiz_library':
 * @property integer $quiz_library_id
 * @property integer $quiz_status_id
 * @property integer $status
 * @property integer $passing_score
 * @property integer $max_retakes
 * @property string $quiz_title
 * @property string $created_on
 * @property string $publish_date
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Quiz[] $quizs
 * @property QuizStatus $quizStatus
 * @property QuizQuestion[] $quizQuestions
 */
class QuizLibrary extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quiz_library';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quiz_title', 'required'),
			array('quiz_status_id, status, passing_score, max_retakes', 'numerical', 'integerOnly'=>true),
			array('quiz_title', 'length', 'max'=>256),
			array('created_on, publish_date, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('quiz_library_id, quiz_status_id, status, passing_score, max_retakes, quiz_title, created_on, publish_date, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'quizs' => array(self::HAS_MANY, 'Quiz', 'quiz_library_id'),
			'quizStatus' => array(self::BELONGS_TO, 'QuizStatus', 'quiz_status_id'),
			'quizQuestions' => array(self::HAS_MANY, 'QuizQuestion', 'quiz_library_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'quiz_library_id' => 'Quiz Library',
			'quiz_status_id' => 'Quiz Status',
			'status' => 'Status',
			'passing_score' => 'Passing Score',
			'max_retakes' => 'Max Retakes',
			'quiz_title' => 'Quiz Title',
			'created_on' => 'Created On',
			'publish_date' => 'Publish Date',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('quiz_library_id',$this->quiz_library_id);
		$criteria->compare('quiz_status_id',$this->quiz_status_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('passing_score',$this->passing_score);
		$criteria->compare('max_retakes',$this->max_retakes);
		$criteria->compare('quiz_title',$this->quiz_title,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('publish_date',$this->publish_date,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuizLibrary the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
