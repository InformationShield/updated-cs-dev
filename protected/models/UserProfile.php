<?php

/**
 * This is the model class for table "user_profile".
 *
 * The followings are the available columns in table 'user_profile':
 * @property integer $user_profile_id
 * @property integer $company_id
 * @property integer $profile_id
 * @property string $profile_title
 * @property string $profile_description
 * @property integer $status
 * @property string $created_on
 *
 * The followings are the available model relations:
 * @property Employee2profile[] $employee2profiles
 * @property Company $company
 * @property UserProfilePolicy[] $userProfilePolicies
 * @property UserProfileQuiz[] $userProfileQuizs
 * @property UserProfileTraining[] $userProfileTrainings
 */
class UserProfile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, profile_title', 'required'),
			array('company_id, profile_id, status', 'numerical', 'integerOnly'=>true),
			array('profile_title', 'length', 'max'=>100),
			array('profile_description, created_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_profile_id, company_id, profile_id, profile_title, profile_description, status, created_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'employee2profiles' => array(self::HAS_MANY, 'Employee2profile', 'user_profile_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'userProfilePolicies' => array(self::HAS_MANY, 'UserProfilePolicy', 'user_profile_id'),
			'userProfileQuizs' => array(self::HAS_MANY, 'UserProfileQuiz', 'user_profile_id'),
			'userProfileTrainings' => array(self::HAS_MANY, 'UserProfileTraining', 'user_profile_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_profile_id' => 'User Profile',
			'company_id' => 'Company',
			'profile_id' => 'Profile',
			'profile_title' => 'Profile Title',
			'profile_description' => 'Profile Description',
			'status' => 'Status',
			'created_on' => 'Created On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_profile_id',$this->user_profile_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('profile_id',$this->profile_id);
		$criteria->compare('profile_title',$this->profile_title,true);
		$criteria->compare('profile_description',$this->profile_description,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_on',$this->created_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
