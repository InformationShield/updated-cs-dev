<?php

/**
 * This is the model class for table "employee".
 *
 * The followings are the available columns in table 'employee':
 * @property integer $employee_id
 * @property integer $company_id
 * @property integer $company_role_id
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property string $department_name
 * @property integer $department_id
 * @property integer $profile_id
 * @property string $created_on
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property CompanyRole $companyRole
 * @property Employee2profile[] $employee2profiles
 * @property SecurityRole[] $securityRoles
 * @property Task[] $tasks
 */
class Employee extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'employee';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, email', 'required'),
			array('company_id, company_role_id, department_id, profile_id, status', 'numerical', 'integerOnly'=>true),
			array('email', 'length', 'max'=>256),
			array('firstname, lastname', 'length', 'max'=>255),
			array('department_name', 'length', 'max'=>100),
			array('created_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('employee_id, company_id, company_role_id, email, firstname, lastname, department_name, department_id, profile_id, created_on, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'companyRole' => array(self::BELONGS_TO, 'CompanyRole', 'company_role_id'),
			'employee2profiles' => array(self::HAS_MANY, 'Employee2profile', 'employee_id'),
			'securityRoles' => array(self::HAS_MANY, 'SecurityRole', 'employee_id'),
			'tasks' => array(self::HAS_MANY, 'Task', 'employee_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'employee_id' => 'Employee',
			'company_id' => 'Company',
			'company_role_id' => 'Company Role',
			'email' => 'Email',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'department_name' => 'Department Name',
			'department_id' => 'Department',
			'profile_id' => 'Profile',
			'created_on' => 'Created On',
			'status' => 'Enabled',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('company_role_id',$this->company_role_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('department_name',$this->department_name,true);
		$criteria->compare('department_id',$this->department_id);
		$criteria->compare('profile_id',$this->profile_id);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Employee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
