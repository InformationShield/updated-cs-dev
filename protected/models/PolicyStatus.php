<?php

/**
 * This is the model class for table "policy_status".
 *
 * The followings are the available columns in table 'policy_status':
 * @property integer $policy_status_id
 * @property string $policy_status_name
 *
 * The followings are the available model relations:
 * @property Policy[] $policies
 * @property PolicyLibrary[] $policyLibraries
 */
class PolicyStatus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'policy_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('policy_status_id, policy_status_name', 'required'),
			array('policy_status_id', 'numerical', 'integerOnly'=>true),
			array('policy_status_name', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('policy_status_id, policy_status_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'policies' => array(self::HAS_MANY, 'Policy', 'policy_status_id'),
			'policyLibraries' => array(self::HAS_MANY, 'PolicyLibrary', 'policy_status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'policy_status_id' => 'Policy Status',
			'policy_status_name' => 'Policy Status Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('policy_status_id',$this->policy_status_id);
		$criteria->compare('policy_status_name',$this->policy_status_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PolicyStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
