<?php

/**
 * This is the model class for table "control_answer".
 *
 * The followings are the available columns in table 'control_answer':
 * @property integer $control_answer_id
 * @property integer $control_baseline_id
 * @property integer $company_id
 * @property integer $control_status_id
 * @property string $updated_on
 * @property integer $updated_user_id
 * @property integer $score
 * @property string $assessed_on
 *
 * The followings are the available model relations:
 * @property ControlStatus $controlStatus
 * @property Users $updatedUser
 * @property Company $company
 * @property ControlBaseline $controlBaseline
 */
class ControlAnswer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'control_answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('control_baseline_id, company_id, control_status_id, updated_user_id, score', 'numerical', 'integerOnly'=>true),
			array('updated_on, assessed_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('control_answer_id, control_baseline_id, company_id, control_status_id, updated_on, updated_user_id, score, assessed_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'controlStatus' => array(self::BELONGS_TO, 'ControlStatus', 'control_status_id'),
			'updatedUser' => array(self::BELONGS_TO, 'Users', 'updated_user_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'controlBaseline' => array(self::BELONGS_TO, 'ControlBaseline', 'control_baseline_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'control_answer_id' => 'Control Answer',
			'control_baseline_id' => 'Control Baseline',
			'company_id' => 'Company',
			'control_status_id' => 'Control Status',
			'updated_on' => 'Updated On',
			'updated_user_id' => 'Updated User',
			'score' => 'Score',
			'assessed_on' => 'Assessed On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('control_answer_id',$this->control_answer_id);
		$criteria->compare('control_baseline_id',$this->control_baseline_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('control_status_id',$this->control_status_id);
		$criteria->compare('updated_on',$this->updated_on,true);
		$criteria->compare('updated_user_id',$this->updated_user_id);
		$criteria->compare('score',$this->score);
		$criteria->compare('assessed_on',$this->assessed_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ControlAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
