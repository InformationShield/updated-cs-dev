<?php

/**
 * This is the model class for table "control_category".
 *
 * The followings are the available columns in table 'control_category':
 * @property integer $control_category_id
 * @property string $parent_id
 * @property string $root_id
 * @property integer $status
 * @property string $category
 * @property string $description
 *
 * The followings are the available model relations:
 * @property ControlBaseline[] $controlBaselines
 * @property ControlLibrary[] $controlLibraries
 * @property Policy[] $policies
 * @property PolicyLibrary[] $policyLibraries
 */
class ControlCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'control_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('parent_id, root_id', 'length', 'max'=>11),
			array('category, description', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('control_category_id, parent_id, root_id, status, category, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'controlBaselines' => array(self::HAS_MANY, 'ControlBaseline', 'cat_id'),
			'controlLibraries' => array(self::HAS_MANY, 'ControlLibrary', 'cat_id'),
			'policies' => array(self::HAS_MANY, 'Policy', 'cat_id'),
			'policyLibraries' => array(self::HAS_MANY, 'PolicyLibrary', 'cat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'control_category_id' => 'Control Category',
			'parent_id' => 'Parent',
			'root_id' => 'Root',
			'status' => 'Status',
			'category' => 'Category',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('control_category_id',$this->control_category_id);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('root_id',$this->root_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ControlCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
