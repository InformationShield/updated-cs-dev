<?php

/**
 * This is the model class for table "control_baseline".
 *
 * The followings are the available columns in table 'control_baseline':
 * @property integer $control_baseline_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $baseline_id
 * @property integer $control_id
 * @property integer $version
 * @property integer $cat_id
 * @property string $cat_code
 * @property string $cat_sub_code
 * @property integer $cat_override
 * @property integer $sort_order
 * @property integer $priority
 * @property integer $weighting
 * @property integer $status
 * @property string $created_on
 * @property string $assessed_on
 * @property string $target_date
 * @property string $title
 * @property integer $control_status_id
 * @property integer $score
 * @property string $evidence
 * @property string $rel_policy
 * @property string $detail
 * @property string $guidance
 * @property string $reference
 * @property string $question
 * @property string $label1
 * @property string $label2
 * @property integer $security_role_id
 * @property string $updated_on
 * @property integer $updated_user_id
 *
 * The followings are the available model relations:
 * @property ControlAnswer[] $controlAnswers
 * @property Users $updatedUser
 * @property Baseline $baseline
 * @property ControlCategory $cat
 * @property Company $company
 * @property ControlStatus $controlStatus
 * @property SecurityRole $securityRole
 * @property Users $user
 * @property Evidence[] $evidences
 * @property Task[] $tasks
 */
class ControlBaseline extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'control_baseline';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('control_id, cat_id, cat_code, cat_sub_code, title', 'required'),
			array('company_id, user_id, baseline_id, control_id, version, cat_id, cat_override, sort_order, priority, weighting, status, control_status_id, score, security_role_id, updated_user_id', 'numerical', 'integerOnly'=>true),
			array('cat_code', 'length', 'max'=>2),
			array('cat_sub_code', 'length', 'max'=>5),
			array('title, label1, label2', 'length', 'max'=>255),
			array('created_on, assessed_on, target_date, evidence, rel_policy, detail, guidance, reference, question, updated_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('control_baseline_id, company_id, user_id, baseline_id, control_id, version, cat_id, cat_code, cat_sub_code, cat_override, sort_order, priority, weighting, status, created_on, assessed_on, target_date, title, control_status_id, score, evidence, rel_policy, detail, guidance, reference, question, label1, label2, security_role_id, updated_on, updated_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'controlAnswers' => array(self::HAS_MANY, 'ControlAnswer', 'control_baseline_id'),
			'updatedUser' => array(self::BELONGS_TO, 'Users', 'updated_user_id'),
			'baseline' => array(self::BELONGS_TO, 'Baseline', 'baseline_id'),
			'cat' => array(self::BELONGS_TO, 'ControlCategory', 'cat_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'controlStatus' => array(self::BELONGS_TO, 'ControlStatus', 'control_status_id'),
			'securityRole' => array(self::BELONGS_TO, 'SecurityRole', 'security_role_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'evidences' => array(self::HAS_MANY, 'Evidence', 'control_baseline_id'),
			'tasks' => array(self::HAS_MANY, 'Task', 'control_baseline_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'control_baseline_id' => 'Control Baseline',
			'company_id' => 'Company',
			'user_id' => 'User',
			'baseline_id' => 'Baseline',
			'control_id' => 'Control',
			'version' => 'Version',
			'cat_id' => 'Cat',
			'cat_code' => 'Cat Code',
			'cat_sub_code' => 'Cat Sub Code',
			'cat_override' => 'Cat Override',
			'sort_order' => 'Sort Order',
			'priority' => 'Priority',
			'weighting' => 'Weighting',
			'status' => 'Status',
			'created_on' => 'Created On',
			'assessed_on' => 'Assessed On',
			'target_date' => 'Target Date',
			'title' => 'Title',
			'control_status_id' => 'Control Status',
			'score' => 'Score',
			'evidence' => 'Evidence',
			'rel_policy' => 'Rel Policy',
			'detail' => 'Detail',
			'guidance' => 'Guidance',
			'reference' => 'Reference',
			'question' => 'Question',
			'label1' => 'Label1',
			'label2' => 'Label2',
			'security_role_id' => 'Security Role',
			'updated_on' => 'Updated On',
			'updated_user_id' => 'Updated User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('control_baseline_id',$this->control_baseline_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('baseline_id',$this->baseline_id);
		$criteria->compare('control_id',$this->control_id);
		$criteria->compare('version',$this->version);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('cat_code',$this->cat_code,true);
		$criteria->compare('cat_sub_code',$this->cat_sub_code,true);
		$criteria->compare('cat_override',$this->cat_override);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('weighting',$this->weighting);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('assessed_on',$this->assessed_on,true);
		$criteria->compare('target_date',$this->target_date,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('control_status_id',$this->control_status_id);
		$criteria->compare('score',$this->score);
		$criteria->compare('evidence',$this->evidence,true);
		$criteria->compare('rel_policy',$this->rel_policy,true);
		$criteria->compare('detail',$this->detail,true);
		$criteria->compare('guidance',$this->guidance,true);
		$criteria->compare('reference',$this->reference,true);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('label1',$this->label1,true);
		$criteria->compare('label2',$this->label2,true);
		$criteria->compare('security_role_id',$this->security_role_id);
		$criteria->compare('updated_on',$this->updated_on,true);
		$criteria->compare('updated_user_id',$this->updated_user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ControlBaseline the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
