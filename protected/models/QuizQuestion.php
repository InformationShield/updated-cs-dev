<?php

/**
 * This is the model class for table "quiz_question".
 *
 * The followings are the available columns in table 'quiz_question':
 * @property integer $quiz_question_id
 * @property integer $quiz_library_id
 * @property integer $quiz_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $status
 * @property string $created_on
 * @property integer $sort_order
 * @property integer $question_type
 * @property integer $correct_answer
 * @property integer $weight
 * @property integer $score
 * @property string $category_name
 * @property string $answer1
 * @property string $answer2
 * @property string $answer3
 * @property string $answer4
 * @property string $answer5
 * @property string $question
 * @property string $hint
 *
 * The followings are the available model relations:
 * @property QuizAnswer[] $quizAnswers
 * @property Company $company
 * @property Quiz $quiz
 * @property QuizLibrary $quizLibrary
 * @property Users $user
 */
class QuizQuestion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quiz_question';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('question', 'required'),
			array('quiz_library_id, quiz_id, company_id, user_id, status, sort_order, question_type, correct_answer, weight, score', 'numerical', 'integerOnly'=>true),
			array('category_name, answer1, answer2, answer3, answer4, answer5', 'length', 'max'=>80),
			array('question, hint', 'length', 'max'=>512),
			array('created_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('quiz_question_id, quiz_library_id, quiz_id, company_id, user_id, status, created_on, sort_order, question_type, correct_answer, weight, score, category_name, answer1, answer2, answer3, answer4, answer5, question, hint', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'quizAnswers' => array(self::HAS_MANY, 'QuizAnswer', 'quiz_question_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'quiz' => array(self::BELONGS_TO, 'Quiz', 'quiz_id'),
			'quizLibrary' => array(self::BELONGS_TO, 'QuizLibrary', 'quiz_library_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'quiz_question_id' => 'Quiz Question',
			'quiz_library_id' => 'Quiz Library',
			'quiz_id' => 'Quiz',
			'company_id' => 'Company',
			'user_id' => 'User',
			'status' => 'Status',
			'created_on' => 'Created On',
			'sort_order' => 'Sort Order',
			'question_type' => 'Question Type',
			'correct_answer' => 'Correct Answer',
			'weight' => 'Weight',
			'score' => 'Score',
			'category_name' => 'Category Name',
			'answer1' => 'Answer1',
			'answer2' => 'Answer2',
			'answer3' => 'Answer3',
			'answer4' => 'Answer4',
			'answer5' => 'Answer5',
			'question' => 'Question',
			'hint' => 'Hint',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('quiz_question_id',$this->quiz_question_id);
		$criteria->compare('quiz_library_id',$this->quiz_library_id);
		$criteria->compare('quiz_id',$this->quiz_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('question_type',$this->question_type);
		$criteria->compare('correct_answer',$this->correct_answer);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('score',$this->score);
		$criteria->compare('category_name',$this->category_name,true);
		$criteria->compare('answer1',$this->answer1,true);
		$criteria->compare('answer2',$this->answer2,true);
		$criteria->compare('answer3',$this->answer3,true);
		$criteria->compare('answer4',$this->answer4,true);
		$criteria->compare('answer5',$this->answer5,true);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('hint',$this->hint,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuizQuestion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
