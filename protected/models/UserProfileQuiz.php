<?php

/**
 * This is the model class for table "user_profile_quiz".
 *
 * The followings are the available columns in table 'user_profile_quiz':
 * @property integer $user_profile_quiz_id
 * @property integer $user_profile_id
 * @property integer $company_id
 * @property integer $quiz_id
 * @property integer $sort_order
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Quiz $quiz
 * @property UserProfile $userProfile
 */
class UserProfileQuiz extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_profile_quiz';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_profile_id, company_id, quiz_id', 'required'),
			array('user_profile_id, company_id, quiz_id, sort_order', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_profile_quiz_id, user_profile_id, company_id, quiz_id, sort_order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'quiz' => array(self::BELONGS_TO, 'Quiz', 'quiz_id'),
			'userProfile' => array(self::BELONGS_TO, 'UserProfile', 'user_profile_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_profile_quiz_id' => 'User Profile Quiz',
			'user_profile_id' => 'User Profile',
			'company_id' => 'Company',
			'quiz_id' => 'Quiz',
			'sort_order' => 'Sort Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_profile_quiz_id',$this->user_profile_quiz_id);
		$criteria->compare('user_profile_id',$this->user_profile_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('quiz_id',$this->quiz_id);
		$criteria->compare('sort_order',$this->sort_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserProfileQuiz the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
