<?php

/**
 * This is the model class for table "quiz_status".
 *
 * The followings are the available columns in table 'quiz_status':
 * @property integer $quiz_status_id
 * @property string $quiz_status_name
 *
 * The followings are the available model relations:
 * @property Quiz[] $quizs
 * @property QuizLibrary[] $quizLibraries
 */
class QuizStatus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quiz_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quiz_status_id, quiz_status_name', 'required'),
			array('quiz_status_id', 'numerical', 'integerOnly'=>true),
			array('quiz_status_name', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('quiz_status_id, quiz_status_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'quizs' => array(self::HAS_MANY, 'Quiz', 'quiz_status_id'),
			'quizLibraries' => array(self::HAS_MANY, 'QuizLibrary', 'quiz_status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'quiz_status_id' => 'Quiz Status',
			'quiz_status_name' => 'Quiz Status Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('quiz_status_id',$this->quiz_status_id);
		$criteria->compare('quiz_status_name',$this->quiz_status_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuizStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
