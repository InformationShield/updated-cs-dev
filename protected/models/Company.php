<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $company_id
 * @property integer $user_id
 * @property string $company_guid
 * @property string $name
 * @property string $logo
 * @property string $website
 * @property integer $license_level
 * @property integer $status
 * @property string $created_on
 * @property string $trial_expiry
 * @property string $password
 * @property string $policy_contactname
 * @property string $policy_email
 * @property string $incident_contactname
 * @property string $incident_email
 * @property string $address
 * @property string $industry
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Baseline[] $baselines
 * @property LicenseLevel $licenseLevel
 * @property Users $user
 * @property CompanyInfo[] $companyInfos
 * @property ControlAnswer[] $controlAnswers
 * @property ControlBaseline[] $controlBaselines
 * @property ControlFilter[] $controlFilters
 * @property CyberRiskScore[] $cyberRiskScores
 * @property Employee[] $employees
 * @property Evidence[] $evidences
 * @property ExportReport[] $exportReports
 * @property Incident[] $incidents
 * @property IncidentType[] $incidentTypes
 * @property Invite[] $invites
 * @property Policy[] $policies
 * @property Quiz[] $quizs
 * @property QuizAnswer[] $quizAnswers
 * @property QuizQuestion[] $quizQuestions
 * @property QuizTracking[] $quizTrackings
 * @property SecurityRole[] $securityRoles
 * @property Task[] $tasks
 * @property Training[] $trainings
 * @property UserPolicyTracking[] $userPolicyTrackings
 * @property UserProfile[] $userProfiles
 * @property UserProfilePolicy[] $userProfilePolicies
 * @property UserProfileQuiz[] $userProfileQuizs
 * @property UserProfileTraining[] $userProfileTrainings
 * @property UserTrainingTracking[] $userTrainingTrackings
 * @property VideoTracking[] $videoTrackings
 * @property VideoTraining[] $videoTrainings
 */
class Company extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, name', 'required'),
			array('user_id, license_level, status', 'numerical', 'integerOnly'=>true),
			array('company_guid', 'length', 'max'=>36),
			array('name', 'length', 'max'=>100),
			array('logo, industry', 'length', 'max'=>60),
			array('website, policy_email, incident_email, address', 'length', 'max'=>256),
			array('password', 'length', 'max'=>32),
			array('policy_contactname, incident_contactname', 'length', 'max'=>200),
			array('created_on, trial_expiry, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('company_id, user_id, company_guid, name, logo, website, license_level, status, created_on, trial_expiry, password, policy_contactname, policy_email, incident_contactname, incident_email, address, industry, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'baselines' => array(self::HAS_MANY, 'Baseline', 'company_id'),
			'licenseLevel' => array(self::BELONGS_TO, 'LicenseLevel', 'license_level'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'companyInfos' => array(self::HAS_MANY, 'CompanyInfo', 'company_id'),
			'controlAnswers' => array(self::HAS_MANY, 'ControlAnswer', 'company_id'),
			'controlBaselines' => array(self::HAS_MANY, 'ControlBaseline', 'company_id'),
			'controlFilters' => array(self::HAS_MANY, 'ControlFilter', 'company_id'),
			'cyberRiskScores' => array(self::HAS_MANY, 'CyberRiskScore', 'company_id'),
			'employees' => array(self::HAS_MANY, 'Employee', 'company_id'),
			'evidences' => array(self::HAS_MANY, 'Evidence', 'company_id'),
			'exportReports' => array(self::HAS_MANY, 'ExportReport', 'company_id'),
			'incidents' => array(self::HAS_MANY, 'Incident', 'company_id'),
			'incidentTypes' => array(self::HAS_MANY, 'IncidentType', 'company_id'),
			'invites' => array(self::HAS_MANY, 'Invite', 'company_id'),
			'policies' => array(self::HAS_MANY, 'Policy', 'company_id'),
			'quizs' => array(self::HAS_MANY, 'Quiz', 'company_id'),
			'quizAnswers' => array(self::HAS_MANY, 'QuizAnswer', 'company_id'),
			'quizQuestions' => array(self::HAS_MANY, 'QuizQuestion', 'company_id'),
			'quizTrackings' => array(self::HAS_MANY, 'QuizTracking', 'company_id'),
			'securityRoles' => array(self::HAS_MANY, 'SecurityRole', 'company_id'),
			'tasks' => array(self::HAS_MANY, 'Task', 'company_id'),
			'trainings' => array(self::HAS_MANY, 'Training', 'company_id'),
			'userPolicyTrackings' => array(self::HAS_MANY, 'UserPolicyTracking', 'company_id'),
			'userProfiles' => array(self::HAS_MANY, 'UserProfile', 'company_id'),
			'userProfilePolicies' => array(self::HAS_MANY, 'UserProfilePolicy', 'company_id'),
			'userProfileQuizs' => array(self::HAS_MANY, 'UserProfileQuiz', 'company_id'),
			'userProfileTrainings' => array(self::HAS_MANY, 'UserProfileTraining', 'company_id'),
			'userTrainingTrackings' => array(self::HAS_MANY, 'UserTrainingTracking', 'company_id'),
			'videoTrackings' => array(self::HAS_MANY, 'VideoTracking', 'company_id'),
			'videoTrainings' => array(self::HAS_MANY, 'VideoTraining', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'company_id' => 'Company',
			'user_id' => 'User',
			'company_guid' => 'Company Guid',
			'name' => 'Name',
			'logo' => 'Logo',
			'website' => 'Website',
			'license_level' => 'License Level',
			'status' => 'Status',
			'created_on' => 'Created On',
			'trial_expiry' => 'Trial Expiry',
			'password' => 'Password',
			'policy_contactname' => 'Policy Contactname',
			'policy_email' => 'Policy Email',
			'incident_contactname' => 'Incident Contactname',
			'incident_email' => 'Incident Email',
			'address' => 'Address',
			'industry' => 'Industry',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('company_guid',$this->company_guid,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('license_level',$this->license_level);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('trial_expiry',$this->trial_expiry,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('policy_contactname',$this->policy_contactname,true);
		$criteria->compare('policy_email',$this->policy_email,true);
		$criteria->compare('incident_contactname',$this->incident_contactname,true);
		$criteria->compare('incident_email',$this->incident_email,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('industry',$this->industry,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
