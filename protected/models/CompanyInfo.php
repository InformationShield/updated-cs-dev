<?php

/**
 * This is the model class for table "company_info".
 *
 * The followings are the available columns in table 'company_info':
 * @property integer $company_info_id
 * @property integer $company_id
 * @property integer $user_id
 * @property string $updated_on
 * @property string $created_on
 * @property string $business_name
 * @property string $num_employees
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property string $country
 * @property string $num_business_locations
 * @property string $primary_business_type
 * @property integer $security_maturity
 * @property integer $highly_sensitive_data
 * @property integer $individual_consumer_data
 * @property integer $credit_card_data
 * @property integer $individual_health_data
 * @property integer $employee_mobile_customer_data
 * @property integer $remote_network_access
 * @property integer $third_party_data
 * @property integer $internally_developed_applications
 * @property integer $online_customer_transactions
 * @property integer $local_it_datacenters
 * @property integer $wireless_networks
 * @property integer $inherent_risk_score
 * @property integer $cyber_risk_score
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Users $user
 */
class CompanyInfo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, user_id', 'required'),
			array('company_id, user_id, security_maturity, highly_sensitive_data, individual_consumer_data, credit_card_data, individual_health_data, employee_mobile_customer_data, remote_network_access, third_party_data, internally_developed_applications, online_customer_transactions, local_it_datacenters, wireless_networks, inherent_risk_score, cyber_risk_score', 'numerical', 'integerOnly'=>true),
			array('business_name, address', 'length', 'max'=>100),
			array('num_employees, num_business_locations', 'length', 'max'=>10),
			array('city, primary_business_type', 'length', 'max'=>60),
			array('state, country', 'length', 'max'=>2),
			array('zipcode', 'length', 'max'=>20),
			array('updated_on, created_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('company_info_id, company_id, user_id, updated_on, created_on, business_name, num_employees, address, city, state, zipcode, country, num_business_locations, primary_business_type, security_maturity, highly_sensitive_data, individual_consumer_data, credit_card_data, individual_health_data, employee_mobile_customer_data, remote_network_access, third_party_data, internally_developed_applications, online_customer_transactions, local_it_datacenters, wireless_networks, inherent_risk_score, cyber_risk_score', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'company_info_id' => 'Company Info',
			'company_id' => 'Company',
			'user_id' => 'User',
			'updated_on' => 'Updated On',
			'created_on' => 'Created On',
			'business_name' => 'Business Name',
			'num_employees' => 'Num Employees',
			'address' => 'Address',
			'city' => 'City',
			'state' => 'State',
			'zipcode' => 'Zipcode',
			'country' => 'Country',
			'num_business_locations' => 'Num Business Locations',
			'primary_business_type' => 'Primary Business Type',
			'security_maturity' => 'Security Maturity',
			'highly_sensitive_data' => 'Highly Sensitive Data',
			'individual_consumer_data' => 'Individual Consumer Data',
			'credit_card_data' => 'Credit Card Data',
			'individual_health_data' => 'Individual Health Data',
			'employee_mobile_customer_data' => 'Employee Mobile Customer Data',
			'remote_network_access' => 'Remote Network Access',
			'third_party_data' => 'Third Party Data',
			'internally_developed_applications' => 'Internally Developed Applications',
			'online_customer_transactions' => 'Online Customer Transactions',
			'local_it_datacenters' => 'Local It Datacenters',
			'wireless_networks' => 'Wireless Networks',
			'inherent_risk_score' => 'Inherent Risk Score',
			'cyber_risk_score' => 'Cyber Risk Score',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('company_info_id',$this->company_info_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('updated_on',$this->updated_on,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('business_name',$this->business_name,true);
		$criteria->compare('num_employees',$this->num_employees,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('num_business_locations',$this->num_business_locations,true);
		$criteria->compare('primary_business_type',$this->primary_business_type,true);
		$criteria->compare('security_maturity',$this->security_maturity);
		$criteria->compare('highly_sensitive_data',$this->highly_sensitive_data);
		$criteria->compare('individual_consumer_data',$this->individual_consumer_data);
		$criteria->compare('credit_card_data',$this->credit_card_data);
		$criteria->compare('individual_health_data',$this->individual_health_data);
		$criteria->compare('employee_mobile_customer_data',$this->employee_mobile_customer_data);
		$criteria->compare('remote_network_access',$this->remote_network_access);
		$criteria->compare('third_party_data',$this->third_party_data);
		$criteria->compare('internally_developed_applications',$this->internally_developed_applications);
		$criteria->compare('online_customer_transactions',$this->online_customer_transactions);
		$criteria->compare('local_it_datacenters',$this->local_it_datacenters);
		$criteria->compare('wireless_networks',$this->wireless_networks);
		$criteria->compare('inherent_risk_score',$this->inherent_risk_score);
		$criteria->compare('cyber_risk_score',$this->cyber_risk_score);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
