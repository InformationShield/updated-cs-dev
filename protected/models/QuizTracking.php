<?php

/**
 * This is the model class for table "quiz_tracking".
 *
 * The followings are the available columns in table 'quiz_tracking':
 * @property integer $quiz_tracking_id
 * @property integer $quiz_id
 * @property integer $user_id
 * @property integer $company_id
 * @property integer $taken_count
 * @property integer $possible_score
 * @property integer $low_score
 * @property integer $high_score
 * @property integer $viewed
 * @property integer $completed
 * @property integer $passed
 * @property integer $last_score
 * @property integer $last_correct
 * @property integer $last_wrong
 * @property string $viewed_timestamp
 * @property string $completed_timestamp
 * @property string $passed_timestamp
 * @property string $answer_timestamp
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Quiz $quiz
 * @property Users $user
 */
class QuizTracking extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quiz_tracking';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quiz_id, user_id, company_id', 'required'),
			array('quiz_id, user_id, company_id, taken_count, possible_score, low_score, high_score, viewed, completed, passed, last_score, last_correct, last_wrong', 'numerical', 'integerOnly'=>true),
			array('viewed_timestamp, completed_timestamp, passed_timestamp, answer_timestamp', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('quiz_tracking_id, quiz_id, user_id, company_id, taken_count, possible_score, low_score, high_score, viewed, completed, passed, last_score, last_correct, last_wrong, viewed_timestamp, completed_timestamp, passed_timestamp, answer_timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'quiz' => array(self::BELONGS_TO, 'Quiz', 'quiz_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'quiz_tracking_id' => 'Quiz Tracking',
			'quiz_id' => 'Quiz',
			'user_id' => 'User',
			'company_id' => 'Company',
			'taken_count' => 'Taken Count',
			'possible_score' => 'Possible Score',
			'low_score' => 'Low Score',
			'high_score' => 'High Score',
			'viewed' => 'Viewed',
			'completed' => 'Completed',
			'passed' => 'Passed',
			'last_score' => 'Last Score',
			'last_correct' => 'Last Correct',
			'last_wrong' => 'Last Wrong',
			'viewed_timestamp' => 'Viewed Timestamp',
			'completed_timestamp' => 'Completed Timestamp',
			'passed_timestamp' => 'Passed Timestamp',
			'answer_timestamp' => 'Answer Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('quiz_tracking_id',$this->quiz_tracking_id);
		$criteria->compare('quiz_id',$this->quiz_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('taken_count',$this->taken_count);
		$criteria->compare('possible_score',$this->possible_score);
		$criteria->compare('low_score',$this->low_score);
		$criteria->compare('high_score',$this->high_score);
		$criteria->compare('viewed',$this->viewed);
		$criteria->compare('completed',$this->completed);
		$criteria->compare('passed',$this->passed);
		$criteria->compare('last_score',$this->last_score);
		$criteria->compare('last_correct',$this->last_correct);
		$criteria->compare('last_wrong',$this->last_wrong);
		$criteria->compare('viewed_timestamp',$this->viewed_timestamp,true);
		$criteria->compare('completed_timestamp',$this->completed_timestamp,true);
		$criteria->compare('passed_timestamp',$this->passed_timestamp,true);
		$criteria->compare('answer_timestamp',$this->answer_timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuizTracking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
