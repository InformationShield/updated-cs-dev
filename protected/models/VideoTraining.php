<?php

/**
 * This is the model class for table "video_training".
 *
 * The followings are the available columns in table 'video_training':
 * @property integer $video_training_id
 * @property integer $training_library_id
 * @property integer $training_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $status
 * @property string $created_on
 * @property integer $sort_order
 * @property string $video_title
 * @property string $video_code
 *
 * The followings are the available model relations:
 * @property VideoTracking[] $videoTrackings
 * @property Company $company
 * @property Training $training
 * @property TrainingLibrary $trainingLibrary
 * @property Users $user
 */
class VideoTraining extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'video_training';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('video_title, video_code', 'required'),
			array('training_library_id, training_id, company_id, user_id, status, sort_order', 'numerical', 'integerOnly'=>true),
			array('video_title', 'length', 'max'=>256),
			array('video_code', 'length', 'max'=>4096),
			array('created_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('video_training_id, training_library_id, training_id, company_id, user_id, status, created_on, sort_order, video_title, video_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'videoTrackings' => array(self::HAS_MANY, 'VideoTracking', 'video_training_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'training' => array(self::BELONGS_TO, 'Training', 'training_id'),
			'trainingLibrary' => array(self::BELONGS_TO, 'TrainingLibrary', 'training_library_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'video_training_id' => 'Video Training',
			'training_library_id' => 'Training Library',
			'training_id' => 'Training',
			'company_id' => 'Company',
			'user_id' => 'User',
			'status' => 'Status',
			'created_on' => 'Created On',
			'sort_order' => 'Sort Order',
			'video_title' => 'Video Title',
			'video_code' => 'Video Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('video_training_id',$this->video_training_id);
		$criteria->compare('training_library_id',$this->training_library_id);
		$criteria->compare('training_id',$this->training_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('video_title',$this->video_title,true);
		$criteria->compare('video_code',$this->video_code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VideoTraining the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
