<?php

/**
 * This is the model class for table "control_filter".
 *
 * The followings are the available columns in table 'control_filter':
 * @property integer $control_filter_id
 * @property integer $company_id
 * @property string $filter_name
 * @property string $filter_desc
 * @property string $label1
 * @property string $label2
 * @property string $reference
 * @property integer $status
 * @property string $updated_on
 *
 * The followings are the available model relations:
 * @property Company $company
 */
class ControlFilter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'control_filter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('filter_name', 'required'),
			array('company_id, status', 'numerical', 'integerOnly'=>true),
			array('filter_name', 'length', 'max'=>255),
			array('filter_desc, label1, label2, reference', 'length', 'max'=>1024),
			array('updated_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('control_filter_id, company_id, filter_name, filter_desc, label1, label2, reference, status, updated_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'control_filter_id' => 'Control Filter',
			'company_id' => 'Company',
			'filter_name' => 'Filter Name',
			'filter_desc' => 'Filter Desc',
			'label1' => 'Label1',
			'label2' => 'Label2',
			'reference' => 'Reference',
			'status' => 'Status',
			'updated_on' => 'Updated On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('control_filter_id',$this->control_filter_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('filter_name',$this->filter_name,true);
		$criteria->compare('filter_desc',$this->filter_desc,true);
		$criteria->compare('label1',$this->label1,true);
		$criteria->compare('label2',$this->label2,true);
		$criteria->compare('reference',$this->reference,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('updated_on',$this->updated_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ControlFilter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
