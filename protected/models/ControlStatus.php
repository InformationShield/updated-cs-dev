<?php

/**
 * This is the model class for table "control_status".
 *
 * The followings are the available columns in table 'control_status':
 * @property integer $control_status_id
 * @property string $control_status_name
 * @property string $status_control_desc
 * @property integer $sort_order
 *
 * The followings are the available model relations:
 * @property ControlAnswer[] $controlAnswers
 * @property ControlBaseline[] $controlBaselines
 * @property ControlLibrary[] $controlLibraries
 */
class ControlStatus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'control_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('control_status_id, control_status_name', 'required'),
			array('control_status_id, sort_order', 'numerical', 'integerOnly'=>true),
			array('control_status_name', 'length', 'max'=>32),
			array('status_control_desc', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('control_status_id, control_status_name, status_control_desc, sort_order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'controlAnswers' => array(self::HAS_MANY, 'ControlAnswer', 'control_status_id'),
			'controlBaselines' => array(self::HAS_MANY, 'ControlBaseline', 'control_status_id'),
			'controlLibraries' => array(self::HAS_MANY, 'ControlLibrary', 'control_status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'control_status_id' => 'Control Status',
			'control_status_name' => 'Control Status Name',
			'status_control_desc' => 'Status Control Desc',
			'sort_order' => 'Sort Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('control_status_id',$this->control_status_id);
		$criteria->compare('control_status_name',$this->control_status_name,true);
		$criteria->compare('status_control_desc',$this->status_control_desc,true);
		$criteria->compare('sort_order',$this->sort_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ControlStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
