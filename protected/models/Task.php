<?php

/**
 * This is the model class for table "task".
 *
 * The followings are the available columns in table 'task':
 * @property integer $task_id
 * @property integer $company_id
 * @property integer $employee_id
 * @property integer $user_id
 * @property integer $task_frequency_id
 * @property integer $task_status_id
 * @property integer $status
 * @property integer $control_baseline_id
 * @property string $created_on
 * @property string $target_date
 * @property string $task_name
 * @property string $task_description
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property ControlBaseline $controlBaseline
 * @property Employee $employee
 * @property TaskFrequency $taskFrequency
 * @property TaskStatus $taskStatus
 * @property Users $user
 */
class Task extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, task_name', 'required'),
			array('company_id, employee_id, user_id, task_frequency_id, task_status_id, status, control_baseline_id', 'numerical', 'integerOnly'=>true),
			array('task_name', 'length', 'max'=>256),
			array('created_on, target_date, task_description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('task_id, company_id, employee_id, user_id, task_frequency_id, task_status_id, status, control_baseline_id, created_on, target_date, task_name, task_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'controlBaseline' => array(self::BELONGS_TO, 'ControlBaseline', 'control_baseline_id'),
			'employee' => array(self::BELONGS_TO, 'Employee', 'employee_id'),
			'taskFrequency' => array(self::BELONGS_TO, 'TaskFrequency', 'task_frequency_id'),
			'taskStatus' => array(self::BELONGS_TO, 'TaskStatus', 'task_status_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'task_id' => 'Task',
			'company_id' => 'Company',
			'employee_id' => 'Employee',
			'user_id' => 'User',
			'task_frequency_id' => 'Task Frequency',
			'task_status_id' => 'Task Status',
			'status' => 'Status',
			'control_baseline_id' => 'Control Baseline',
			'created_on' => 'Created On',
			'target_date' => 'Target Date',
			'task_name' => 'Task Name',
			'task_description' => 'Task Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('task_frequency_id',$this->task_frequency_id);
		$criteria->compare('task_status_id',$this->task_status_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('control_baseline_id',$this->control_baseline_id);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('target_date',$this->target_date,true);
		$criteria->compare('task_name',$this->task_name,true);
		$criteria->compare('task_description',$this->task_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Task the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
