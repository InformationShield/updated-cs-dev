<?php

/**
 * This is the model class for table "baseline".
 *
 * The followings are the available columns in table 'baseline':
 * @property integer $baseline_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $special_type
 * @property integer $status
 * @property integer $sort_order
 * @property string $created_on
 * @property string $baseline_name
 * @property string $baseline_description
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Users $user
 * @property ControlBaseline[] $controlBaselines
 */
class Baseline extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'baseline';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('baseline_name', 'required'),
			array('company_id, user_id, special_type, status, sort_order', 'numerical', 'integerOnly'=>true),
			array('baseline_name', 'length', 'max'=>255),
			array('created_on, baseline_description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('baseline_id, company_id, user_id, special_type, status, sort_order, created_on, baseline_name, baseline_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'controlBaselines' => array(self::HAS_MANY, 'ControlBaseline', 'baseline_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'baseline_id' => 'Baseline',
			'company_id' => 'Company',
			'user_id' => 'User',
			'special_type' => 'Special Type',
			'status' => 'Status',
			'sort_order' => 'Sort Order',
			'created_on' => 'Created On',
			'baseline_name' => 'Baseline Name',
			'baseline_description' => 'Baseline Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('baseline_id',$this->baseline_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('special_type',$this->special_type);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('baseline_name',$this->baseline_name,true);
		$criteria->compare('baseline_description',$this->baseline_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Baseline the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
