<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>
<?
    $pieHeight = 600;
    $lnkAttr = 'data-recid="\'+record.recid+\'" data-toggle="tooltip" data-placement="left" title="\'+((record.description) ? w2utils.stripTags(record.description) : w2utils.stripTags(record.doc_title))+\'"';
?>
<h2>Training Compliance Report</h2>
<div class="left" style="margin: 0 0 10px 0;">
    <p>
        Click on the <strong>Title</strong> to view the chart for that training.<br/>
        Click the <i class="fa fa-table purple" style="font-size: 16px;"></i> see a detailed user report with export and print features.
    </p>
</div>
<div class="right" style="margin: 0 0 10px 0;">
    <a href="" id="lnkExport" class="btn btn-success" >Export to CSV</a>
    <a href="<?= $printUrl; ?>" target="_blank" class="btn btn-success">Print</a>
</div>

<style type="text/css">
    .piechart { margin: 0 0 20px 0; height: <?= $pieHeight; ?>px;}
    .pieloader { margin: 75px 0 0 75px; }
</style>
<script type="text/javascript">
    function trainingTracking(recid) {
        var record = w2ui['grid'].get(recid);
        if (record) {
            $('#spnTitle').text(record.doc_title);
            $('#divDescription').html(record.description);
            var trainingTrackingChart = new FusionCharts({
                type: 'pie3D',
                renderAt: 'divTrainingTracking',
                width: '100%',
                height: '<?= $pieHeight; ?>',
                dataFormat: 'json',
                dataSource: {
                    "chart": {
                        "caption": "Completed " + record.training_completed + " of " + record.training_count,
                        "subCaption": "",
                        "numberPrefix": "",
                        "showPercentInTooltip": "1",
                        "showLegend": "1",
                        "legendShadow": '1',
                        "decimals": "0",
                        "useDataPlotColorForLabels": "1",
                        "theme": "fint"
                    },
                    "data": [
                        {"label": "Unopened", "value": record.training_unopened, "link": "", "color": "FF0000", "labelFontColor": "000000"},
                        {"label": "Completed", "value": record.training_completed, "link": "", "color": "00FF00", "labelFontColor": "000000"}
                    ]
                }
            }).render();
            return true;
        } else {
            return false;
        }
    };

    var _showChart = true;
    var _ajaxListGridColumns = [
        { field: 'training_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'action', caption: 'Detail', size: '45px',  resizable: false, sortable: false,
            render: function(record, index, column_index) {
                if (_showChart) {
                    _showChart = false;
                    $('#divShowChart').show();
                    FusionCharts.ready(function () {
                        trainingTracking(record.recid);
                    });
                }
                return '<div>&nbsp;&nbsp;&nbsp;<a href="<?= url('reporttraining/compliancedetail'); ?>/id/' + record.recid + '"><i class="fa fa-table purple" style="font-size: 16px;"></i></a></div>';
            }
        },
        { field: 'doc_title', caption: 'Title', size: '100%',  resizable: true, sortable: false,
            render: function (record, index, column_index) {
                return '<div><a class="lnkChart" href="" <?= $lnkAttr; ?>>' + record.doc_title+ '</a></div>';
            }
        },
        { field: 'publish_date', caption: 'Published', size: '90px',  resizable: true, sortable: false},
        { field: 'training_completed_per', caption: 'Completed', size: '100px',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                return '<div>'+record.training_completed_per+'%</div>';
            }
        },
        { field: 'training_unopened_per', caption: 'Unopened', size: '75px',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                return '<div>'+record.training_unopened_per+'%</div>';
            }
        }
    ];

    var _ajaxListGridAddFunction = false;
</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = url('reporttraining/getcompliance');
    $gmAjaxListGrid->title = "";
    $gmAjaxListGrid->intro = "";
    $gmAjaxListGrid->controller = "reporttraining";
    $gmAjaxListGrid->recid = "training_id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = false;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    $gmAjaxListGrid->showToolbar = false;
    $gmAjaxListGrid->showSearch = false;
    $gmAjaxListGrid->showFooter = false;
    $gmAjaxListGrid->fixedBody = true;
    $gmAjaxListGrid->fixedHeight = $height;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
<div id="divShowChart" style="display:none;">
    <div style="margin: 10px 0 10px 10px;">
        <h2><span id="spnTitle">&nbsp;</span></h2>
        <div id="divDescription">&nbsp;</div>
    </div>
    <div id="divTrainingTracking" class="col-md-12 col-sm-12 col-xs-12 piechart">
        <img src="/images/ajax-loader.gif" class="pieloader" />
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(document).on('click', '.lnkChart', function () {
            var recid = $(this).data('recid');
            trainingTracking(recid);
            return false;
        });
        $(document).on('click', '#lnkExport', function () {
            new PNotify({
                title: 'Downloading...',
                text: 'Your file is downloading now.',
                delay: 3500,
                type: 'info'
            });
            window.location = '<?= $exportUrl; ?>';
            return false;
        });
    });
</script>
