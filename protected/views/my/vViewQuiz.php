<?php
/* @var $this MyController */
/* @var $model Quiz */
/* @var $question QuizQuestion */

$quizTitle = '"'.$model->quiz_title.'"';
if (!empty($model->publish_date)) {
    $quizTitle .= ' published ' . DateHelper::FormatDateTimeString($model->publish_date, 'm/d/Y');
}
?>
<style type="text/css">
    .answers { margin: 10px 0px 0px 10px; }
</style>

<div style="margin-bottom: 20px;">
    <? if ($isTest) { ?>
        <h4>TEST MODE</h4>
        <p>In TEST MODE your answers are not actually saved, you are just simply seeing the Quiz as a user would see it.</p>
        <hr/>
    <? } ?>
    <h4><i class="fa fa-question-circle"></i> Quiz: <?= $quizTitle; ?></h4>
    <p><?= $model->description; ?></p>
</div>
<div id="content" class="x_panel" style="width: 100%; height: 100%;">
    <div class="x_title">
        <h4>Quiz Questions</h4>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <p>
            <strong>Please answer all the questions below then click the Save button.</strong>
        </p>

        <? $form=$this->beginWidget('CActiveForm', array('action'=>$editUrl,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'id'=>'edit-form',
            'enableAjaxValidation'=>false,
        )); ?>

            <? $correctSpan = '<span style="color: green;">Correct!</span>'; ?>
            <? foreach($questions as $question) { ?>
                <? $correctAnswer = (isset($correctAnswers[$question->quiz_question_id])) ? $correctAnswers[$question->quiz_question_id] : 0; ?>
                <? $markCorrect = ($correctAnswer) ? $correctSpan : ''; ?>
                <? if (!empty($question->category_name)) { ?>
                    <div class="right">
                        <small><?= $question->category_name; ?></small>
                    </div>
                    <div class="clearfix"></div>
                <? } ?>
                <p>
                    <strong>Question:&nbsp;&nbsp;</strong><?= $question->question; ?>
                    <? if (!empty($question->hint)) { ?>
                        <br/><em><?= $question->hint; ?></em>
                    <? } ?>
                </p>
                <div style="margin: 10px 0px 20px 0px;">
                    <strong>Answer:&nbsp;&nbsp;<?= $markCorrect; ?></strong>
                    <? if ($question->question_type == 1) { ?>
                        <div class="answers">
                            <input class="flat" name="Answer[<?= $question->quiz_question_id; ?>]" type="radio" value="1" <?=($correctAnswer == "1") ? 'checked': ''; ?>>&nbsp;True
                        </div>
                        <div class="answers">
                            <input class="flat" name="Answer[<?= $question->quiz_question_id; ?>]" type="radio" value="2" <?=($correctAnswer == "2") ? 'checked': ''; ?>>&nbsp;False
                        </div>
                    <? } else if ($question->question_type == 2) { ?>
                        <div class="answers">
                            <input class="flat" name="Answer[<?= $question->quiz_question_id; ?>]" type="radio" value="1" <?=($correctAnswer == "1") ? 'checked': ''; ?>>&nbsp;Yes
                        </div>
                        <div class="answers">
                            <input class="flat" name="Answer[<?= $question->quiz_question_id; ?>]" type="radio" value="2" <?=($correctAnswer == "2") ? 'checked': ''; ?>>&nbsp;No
                        </div>
                <? } else { ?>
                        <? if (!empty($question->answer1)) { ?>
                            <div class="answers">
                                <input class="flat" name="Answer[<?= $question->quiz_question_id; ?>]" type="radio" value="1" <?=($correctAnswer == "1") ? 'checked': ''; ?>>&nbsp;<?= $question->answer1; ?>
                            </div>
                        <? } ?>
                        <? if (!empty($question->answer2)) { ?>
                            <div class="answers">
                                <input class="flat" name="Answer[<?= $question->quiz_question_id; ?>]" type="radio" value="2" <?=($correctAnswer == "2") ? 'checked': ''; ?>>&nbsp;<?= $question->answer2; ?>
                            </div>
                        <? } ?>
                        <? if (!empty($question->answer3)) { ?>
                            <div class="answers">
                                <input class="flat" name="Answer[<?= $question->quiz_question_id; ?>]" type="radio" value="3" <?=($correctAnswer == "3") ? 'checked': ''; ?>>&nbsp;<?= $question->answer3; ?>
                            </div>
                        <? } ?>
                        <? if (!empty($question->answer4)) { ?>
                            <div class="answers">
                                <input class="flat" name="Answer[<?= $question->quiz_question_id; ?>]" type="radio" value="4" <?=($correctAnswer == "4") ? 'checked': ''; ?>>&nbsp;<?= $question->answer4; ?>
                            </div>
                        <? } ?>
                        <? if (!empty($question->answer5)) { ?>
                            <div class="answers">
                                <input class="flat" name="Answer[<?= $question->quiz_question_id; ?>]" type="radio" value="5" <?=($correctAnswer == "5") ? 'checked': ''; ?>>&nbsp;<?= $question->answer5; ?>
                            </div>
                        <? } ?>
                    <? } ?>
                </div>
            <? } ?>

            <div class="row buttons" style="margin: 30px 0px 0px 10px;">
                <?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
                <?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
            </div>

        <? $this->endWidget(); ?>
    </div><!-- form -->

</div>

<script type="text/javascript">
    $(function(){
        $("#btnSave").click(function(){
            var checked = $("input[type='radio']:checked").length;
            if (checked < <?= count($questions); ?>) {
                w2alert('You have not answered all the questions.<br/>Please complete quiz first.');
            } else {
                $('#edit-form').submit();
            }
            return false;
        });

        $("#btnCancel").click(function(){
            window.history.back();
            return false;
        });
    });
</script>
