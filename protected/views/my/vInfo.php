<?
/* @var $this MyController */
/* @var $gmAjaxListView gmAjaxListView */
?>
<h1><i class="fa fa-info-circle"></i> My Info</h1>
<? if ($gmAjaxListView) { ?>
    <p>
        This information is assigned by your Company Administrator when they registered the Company Users.
        If any of this information is incorrect, please contact your Company Administrator and report the issue.
    </p>
    <? AjaxListViewGadget::show($gmAjaxListView); // render subview ?>
<? } else { ?>
    <p>
        No information is available. Contact your company administrator or technical support.
    </p>
<? } ?>
