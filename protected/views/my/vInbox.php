<? /* @var $this MyController */ ?>
<h1><i class="fa fa-inbox"></i> My Inbox</h1>
<? $this->renderPartial('_vPolicyGrid',array('header'=>'Policies','fixedBody'=>false)); ?>
<br/>
<? $this->renderPartial('_vTrainingGrid',array('header'=>'Training','fixedBody'=>false)); ?>
<br/>
<? $this->renderPartial('_vQuizGrid',array('header'=>'Quizzes','fixedBody'=>false)); ?>
<br/>
<? $this->renderPartial('_vTasksGrid',array('header'=>'Tasks','fixedBody'=>false)); ?>

