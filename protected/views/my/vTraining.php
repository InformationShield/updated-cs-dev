<? /* @var $this MyController */ ?>
<h1><i class="fa fa-graduation-cap"></i> Training</h1>
<p>
    <i class="fa fa-check green" style="font-size: 20px;"></i> marks items that has been acknowledged.
    <br/>Click the <i class="fa fa-check" style="color: lightgrey; font-size: 20px;"></i> to acknowledged that a <i class="fa fa-cloud-download purple" style="font-size: 20px;"></i> downloaded file has been viewed.
</p>
<? $this->renderPartial('_vTrainingGrid',array('header'=>false,'fixedBody'=>false)); ?>
