<?php
/* @var $this MyController */

    $lnkAttr = 'data-recid="\'+record.recid+\'" data-toggle="tooltip" data-placement="left" title="\'+((record.description) ? w2utils.stripTags(record.description) : w2utils.stripTags(record.doc_title))+\'"';
?>
<p>Click these <i class="fa fa-file-pdf-o purple" style="font-size: 20px;"></i> <i class="fa fa-cloud-download purple" style="font-size: 20px;"></i> <i class="fa fa-play purple" style="font-size: 20px;"></i> or <strong>Title</strong> to view or download the training document or watch the video series.</p>

<script type="text/javascript">
    var _ack2 = '<i class="fa fa-check green" style="font-size: 20px;"></i>';
    var _ajaxListGridColumns2 = [
        { field: 'training_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'ack', caption: '<i class="fa fa-check green" style="font-size: 20px;"></i>', size: '35px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                if (record.training_type == '2' || record.file_type == 'application/pdf' || record.file_type.substr(0,5) == 'text/') {
                    return (record.ack == '1') ? _ack2 : '';
                } else {
                    return (record.ack == '1') ? _ack2 : '<a class="acktraining" href="#" data-recid="'+record.recid+'"><i class="fa fa-check" style="color: lightgrey; font-size: 20px;" ></i></a>';
                }
            }
        },
        { field: 'action', caption: 'View', size: '45px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                var icon = 'fa-cloud-download';
                var lnkClass = 'lnkDownload';
                if (record.training_type == '2') {
                    icon = 'fa-play';
                    lnkClass = 'lnkGo';
                } else {
                    if (record.file_type == 'application/pdf') {
                        icon = 'fa-file-pdf-o';
                        lnkClass = 'lnkGo';
                    } else if (record.file_type.substr(0, 5) == 'text/') {
                        icon = 'fa-file-text-o';
                        lnkClass = 'lnkGo';
                    }
                }
                icon = '<i class="fa '+icon+' purple" style="font-size: 20px;"></i>';
                return '<div>&nbsp;&nbsp;<a class="'+lnkClass+'" href="<?= url('my/viewtraining'); ?>/id/' + record.recid + '" <?= $lnkAttr; ?>>'+icon+'</a></div>';
            }
        },
        { field: 'doc_title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                var lnkClass = 'lnkDownload';
                if (record.training_type == '2' || record.file_type == 'application/pdf' || record.file_type.substr(0, 5) == 'text/') {
                    lnkClass = 'lnkGo';
                }
                return '<div><a class="'+lnkClass+'" href="<?= url('my/viewtraining'); ?>/id/' + record.recid + '" <?= $lnkAttr; ?>>' + record.doc_title + '</a></div>';
            }
        },
        { field: 'author', caption: 'Author', size: '150px', searchable: 'text', resizable: true, sortable: false },
        { field: 'publish_date', caption: 'Published', size: '75px', searchable: 'text', resizable: true, sortable: true }
    ];

    var _ajaxListGridAddFunction2 = false;

    var _ajaxListGridDownloadHook2 = function(scope,recid,href) {
        $.ajax({
            method: 'POST',
            dataType: "json",
            url: '<?= url('my/ack'); ?>',
            data: {
                type: 'training',
                id: recid,
                file: '.' //signals a download
            },
            success: function (response) {
            }
        });
    }

    $(function(){
        $('#maintreegrid2').on('click','.acktraining',function(){
            var recid = $(this).data('recid');
            var mythis = this;
            w2confirm('I acknowledge that I have read the contents of this training document.', 'Training Acknowledgement', function(btn){
                if (btn=='Yes') {
                    $.ajax({
                        method: 'POST',
                        dataType: "json",
                        url: '<?= url('my/ack'); ?>',
                        data: {
                            type: 'training',
                            ack: 'yes',
                            id: recid,
                            file: '.'
                        },
                        success: function (response) {
                            if (response.status == 'success') {
                                $(mythis).html(_ack2);
                            } else {
                                w2alert(response.message);
                            }
                        }
                    });
                }
            });
            return false;
        });
    });
</script>
<?
    $gmAjaxListGrid2 = new gmAjaxListGrid();
    $gmAjaxListGrid2->gridNum = '2';
    $gmAjaxListGrid2->fixedBody = $fixedBody;
    $gmAjaxListGrid2->columnHeaders = true;
    $gmAjaxListGrid2->listUrl = url('my/gettraining');
    $gmAjaxListGrid2->header = $header;
    $gmAjaxListGrid2->controller = "training";
    $gmAjaxListGrid2->recid = "training_id";
    $gmAjaxListGrid2->allowView = false;
    $gmAjaxListGrid2->allowAdd = false;
    $gmAjaxListGrid2->allowEdit = false;
    $gmAjaxListGrid2->allowDownload = true;
    $gmAjaxListGrid2->allowDelete = false;
    $gmAjaxListGrid2->allowCopy = false;
    $gmAjaxListGrid2->reloadOnCopy = false;
    $gmAjaxListGrid2->showToolbar = false;
    $gmAjaxListGrid2->showFooter = false;
    AjaxListGridGadget::show($gmAjaxListGrid2);
?>