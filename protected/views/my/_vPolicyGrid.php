<?php
/* @var $this MyController */

    $lnkAttr = 'data-recid="\'+record.recid+\'" data-toggle="tooltip" data-placement="left" title="\'+((record.description) ? w2utils.stripTags(record.description) : w2utils.stripTags(record.doc_title))+\'"';
?>
<p>
    <i class="fa fa-check green" style="font-size: 20px;"></i> marks items that has been acknowledged.
    <br/>Click the <i class="fa fa-check" style="color: lightgrey; font-size: 20px;"></i> to acknowledged that a <i class="fa fa-cloud-download purple" style="font-size: 20px;"></i> downloaded file has been viewed.
</p>
<p>
    Click these <i class="fa fa-file-pdf-o purple" style="font-size: 20px;"></i> <i class="fa fa-cloud-download purple" style="font-size: 20px;"></i> or the <strong>Title</strong> to view or download a policy document.
</p>

<script type="text/javascript">
    var _ack1 = '<i class="fa fa-check green" style="font-size: 20px;"></i>';
    var _ajaxListGridColumns1 = [
        { field: 'policy_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'ack', caption: '<i class="fa fa-check green" style="font-size: 20px;"></i>', size: '35px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                if (record.file_type == 'application/pdf' || record.file_type.substr(0,5) == 'text/') {
                    return (record.ack == '1') ? _ack1 : '';
                } else {
                    return (record.ack == '1') ? _ack1 : '<a class="ackpolicy" href="#" data-recid="'+record.recid+'"><i class="fa fa-check" style="color: lightgrey; font-size: 20px;" ></i></a>';
                }
            }
        },
        { field: 'action', caption: 'View', size: '45px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                var icon = 'fa-cloud-download';
                var lnkClass = 'lnkDownload';
                if (record.file_type == 'application/pdf') {
                    icon = 'fa-file-pdf-o';
                    lnkClass = 'lnkGo';
                } else if (record.file_type.substr(0,5) == 'text/') {
                    icon = 'fa-file-text-o';
                    lnkClass = 'lnkGo';
                }
                icon = '<i class="fa '+icon+' purple" style="font-size: 20px;"></i>';
                return '<div>&nbsp;&nbsp;<a class="'+lnkClass+'" href="<?= url('my/viewpolicy'); ?>/id/' + record.recid + '" <?= $lnkAttr; ?>>'+icon+'</a></div>';
            }
        },
        { field: 'doc_title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: false,
            render: function (record, index, column_index) {
                var lnkClass = 'lnkDownload';
                if (record.file_type == 'application/pdf' || record.file_type.substr(0,5) == 'text/') {
                    lnkClass = 'lnkGo';
                }
                return '<div><a class="'+lnkClass+'" href="<?= url('my/viewpolicy'); ?>/id/' + record.recid + '" <?= $lnkAttr; ?>>' + record.doc_title + '</a></div>';
            }
        },
        { field: 'author', caption: 'Author', size: '150px', searchable: 'text', resizable: true, sortable: false },
        { field: 'publish_date', caption: 'Published', size: '75px', searchable: 'text', resizable: true, sortable: false }
    ];

    var _ajaxListGridAddFunction1 = false;

    var _ajaxListGridDownloadHook1 = function(scope,recid,href) {
        $.ajax({
            method: 'POST',
            dataType: "json",
            url: '<?= url('my/ack'); ?>',
            data: {
                type: 'policy',
                id: recid,
                file: '.'
            },
            success: function (response) {
            }
        });
    }

    $(function(){
        $('#maintreegrid1').on('click','.ackpolicy',function(){
            var recid = $(this).data('recid');
            var mythis = this;
            w2confirm('I acknowledge that I have read and understood the contents of this policy document.', 'Policy Acknowledgement', function(btn){
                if (btn=='Yes') {
                    $.ajax({
                        method: 'POST',
                        dataType: "json",
                        url: '<?= url('my/ack'); ?>',
                        data: {
                            type: 'policy',
                            ack: 'yes',
                            id: recid,
                            file: '.'
                        },
                        success: function (response) {
                            if (response.status == 'success') {
                                $(mythis).html(_ack1);
                            } else {
                                w2alert(response.message);
                            }
                        }
                    });
                }
            });
            return false;
        });
    });
</script>
<?
    $gmAjaxListGrid1 = new gmAjaxListGrid();
    $gmAjaxListGrid1->gridNum = '1';
    $gmAjaxListGrid1->fixedBody = $fixedBody;
    $gmAjaxListGrid1->listUrl = url('my/getpolicy');
    $gmAjaxListGrid1->header = $header;
    $gmAjaxListGrid1->controller = "policy";
    $gmAjaxListGrid1->recid = "policy_id";
    $gmAjaxListGrid1->allowView = false;
    $gmAjaxListGrid1->allowAdd = false;
    $gmAjaxListGrid1->allowEdit = false;
    $gmAjaxListGrid1->allowDownload = true;
    $gmAjaxListGrid1->allowDelete = false;
    $gmAjaxListGrid1->allowCopy = false;
    $gmAjaxListGrid1->reloadOnCopy = false;
    $gmAjaxListGrid1->showToolbar = false;
    $gmAjaxListGrid1->showFooter = false;
    AjaxListGridGadget::show($gmAjaxListGrid1);
?>
