<?php
/* @var $this MyController */
?>

<p>
    Click the <strong>Title</strong> to view task details.
</p>

<p>
    To change task status select new status from dropdown and click the <strong>Update</strong> link.
</p>

<script type="text/javascript">
    var _ajaxListGridColumns4 = [
        { field: 'task_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'task_name', caption: 'Name', size: '100%', hidden: false, sortable: true,  resizable: false,
            render: function (record, index, column_index) {                
                return '<div><a class="task-preview" data-task-id="' + record.task_id + '" href="#">' + record.task_name + '</a></div><div style="display:none;" id="task_content_' + record.task_id + '">' + record.task_description + '</div>';
                
            }},
        { field: 'target_date', caption: 'Target Date', size: '100px', hidden: false, sortable: true,  resizable: false },
        { field: 'update_status', caption: 'Status', size: '300px', hidden: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {                
                return '<div style="display:inline-table; margin:8px 10px 8px 10px;">\n\
        <select name="update_task_status_' + record.task_id +'" data-current-status="' + record.task_status_id + '">\n\
            <?php $task_statuses = TaskStatus::model()->findAll(array('order' => 'task_status_id ASC')); ?>\n\
            <?php foreach($task_statuses as $ts) { ?>\n\
                        <option value="<?php echo $ts->task_status_id; ?>" ' + ((record.task_status_id == <?php echo $ts->task_status_id; ?>) ? 'selected="selected"' : '') + '><?php echo $ts->task_status_name; ?></option>\n\
            <?php } ?>\n\
</select>\n\
</div>\n\
<div style="display:inline-table; margin:8px 0px 8px 0px;">\n\
<a href="#" class="update-task-btn" data-task-id="' + record.task_id +'">Update<a>\n\
</div>';
                
            }},
    ];

    var _ajaxListGridAddFunction4 = false;

    

    $(function(){
        $(document).on('click', 'a.update-task-btn', function(e){
            e.preventDefault();
            var task_id = $(this).data('task-id');
            var new_status_id = $('select[name=update_task_status_' + task_id + ']').val();
            var current_status_id = $('select[name=update_task_status_' + task_id + ']').data('current-status');
            
            if(new_status_id == current_status_id){
                return;
            }
            
            $.ajax({
                method: 'POST',
                dataType: "json",
                url: '<?php echo url('my/updatetaskstatus'); ?>',
                data: {
                    task_id: task_id,
                    status_id: new_status_id,
                },
                success: function (response) {
                    if(response.status == 'success'){
                        new PNotify({
                            title: 'Task Status Updated',
                            text: false,
                            type: 'success',
                            delay: 1000,
                            animate_speed: "normal"
                        });
                        
                        // change current status
                        $('select[name=update_task_status_' + task_id + ']').data('current-status', new_status_id)
                    }
                }
            });
        });
        
        $(document).on('click', 'a.task-preview', function(e){
            console.log($('#task_content_' + $(this).data('task-id')));
            e.preventDefault();
            var w = $('body').width() - ($('body').width() * 0.2);
            var h  = $('body').height();
            h = (h > 800) ? 800 : h;
            $('#task_content_' + $(this).data('task-id')).w2popup({
                    showMax: true,
                    width: w,     // width in px
                    height: h,     // height in px
                    title: $(this).text(),
                    body: $('#task_content_' + $(this).data('task-id')).html(),
                    buttons : '<button class="btn" onclick="w2popup.close();">Close</button> '
            });
        });
    });
</script>

<?
    $gmAjaxListGrid4 = new gmAjaxListGrid();
    $gmAjaxListGrid4->gridNum = '4';
    $gmAjaxListGrid4->fixedBody = $fixedBody;
    $gmAjaxListGrid4->listUrl = url('my/gettasks');
    $gmAjaxListGrid4->header = $header;
    $gmAjaxListGrid4->controller = "task";
    $gmAjaxListGrid4->recid = "task_id";
    $gmAjaxListGrid4->allowView = false;
    $gmAjaxListGrid4->allowAdd = false;
    $gmAjaxListGrid4->allowEdit = false;
    $gmAjaxListGrid4->allowDownload = false;
    $gmAjaxListGrid4->allowDelete = false;
    $gmAjaxListGrid4->allowCopy = false;
    $gmAjaxListGrid4->reloadOnCopy = false;
    $gmAjaxListGrid4->showToolbar = false;
    $gmAjaxListGrid4->showFooter = false;
    AjaxListGridGadget::show($gmAjaxListGrid4);
?>