<?php
/* @var $this MyController */
/* @var $model Policy */

$policyTitle = '"'.$model->doc_title.'"';
if (!empty($model->publish_date)) {
    $policyTitle .= ' published ' . DateHelper::FormatDateTimeString($model->publish_date, 'm/d/Y');
}
$icon = 'fa-file-text-o';
if ($model->file_type == 'application/pdf') {
    $icon = 'fa-file-pdf-o';
}
?>
<script language="javascript" type="text/javascript">

    $(function(){

        var w = $('#divFrame').width();
        var t = $('#divFrame').offset();
        var f = $('#footer').height();
        var a = $('#above').height();
        var b = $('#below').height();
        var adj = (t.top + f + a + b + 50);
        var h = $(window).height() - adj;
        var bh = $('body').height();
        h = (h < (bh-adj)) ? (bh-adj) : h;
        var iframe = '<iframe id="frmDoc" name="frmDoc" width="'+w+'px" height="'+h+'px" frameborder="0" scrolling="auto" />';
        $('#divFrame').html(iframe);

        $('#frmDoc').on('load', function(){
            setTimeout(function(){
                $.ajax({
                    method: 'POST',
                    dataType: "json",
                    url: '<?= url('my/ack'); ?>',
                    data: {
                        type: 'policy',
                        id: '<?= Obscure::encode($model->policy_id); ?>',
                        file: '<?= basename($fileUrl); ?>'
                    },
                    success: function (response) {
                    }
                });
            }, 100);
        });

        setTimeout(function() {
            $('#frmDoc').attr('src', "<?= $fileUrl; ?>");
        }, 10);

        $( window ).resize(function() {
            var w = $('#divFrame').width();
            $('#frmDoc').css('width',w);
        });

        $('#btnAck').click(function(){
			$.ajax({
				method: 'POST',
				dataType: "json",
				url: '<?= url('my/ack'); ?>',
				data: {
					type: 'policy',
					ack: 'yes',
					id: '<?= Obscure::encode($model->policy_id); ?>'
				},
				success: function (response) {
					if (response.status == 'success') {
						window.location = '<?= url('my/inbox'); ?>';
					} else {
						alert(response.message);
					}
				}
			});
            return false;
        });

    });

</script>
<div id="above">
	<h4><i class="fa <?= $icon; ?>"></i> Policy: <?= $policyTitle; ?></h4>
	<p>Please read the document completely then click the <strong>Acknowledge</strong> button below at the end of the document.</p>
</div>
<div id="divFrame" style="width: 100%; height: 100%;"></div>
<div id="below">
	<? if ($ackDate) { ?>
		<h4>
			<i class="fa fa-check green" style="margin-top: 5px; font-size: 20px;"></i>
			<span>You have acknowledged this policy document on <?= DateHelper::FormatDateTimeString($ackDate, 'm/d/Y'); ?>.</span>
		</h4>
	<? } else { ?>
		<a href="" id="btnAck" class="btn btn-success" style="margin-top: 5px;">Acknowledge</a>
		<span>I acknowledge that I have read and understood the contents of this policy document.</span>
	<? } ?>
</div>
