<?
$trainingTitle = '"'.$trainingInfo['doc_title'].'"';
if (!empty($trainingInfo['publish_date'])) {
    $trainingTitle .= ' published ' . DateHelper::FormatDateTimeString($trainingInfo['publish_date'], 'm/d/Y');
}
?>
<h1><i class="fa fa-graduation-cap"></i> Watch Training Video Series</h1>
<h2><?= $trainingTitle; ?></h2>
<p>Please check the box on the left when you watch each video. These acknowledgements are saved. After all the videos have been checked, the Training will also be mark completed in your inbox.</p>
<div class="col-md-5 col-sm-5 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Watch List</h2>
            <div class="clearfix"></div>
        </div>
        <div id="divVideos" class="x_content">
            <div class="">
                <? if (empty($models)) { ?>
                    <p>There are no videos available for this series.</p>
                <? } ?>
                <ul class="to_do">
                    <? $i=1; ?>
                    <? foreach($models as $model) { ?>
                            <li>
                                <p>
                                    <input id="chkWatch-<?= $i; ?>" class="chkWatch flat" type="checkbox" <?= ($model['ack']=='1') ? 'checked':''; ?> data-recid="<?= Obscure::encode($model['video_training_id']); ?>" data-tid="<?= Obscure::encode($trainingInfo['training_id']); ?>"/>
                                    <a href="" class="lnkPlay" id="btnPlay-<?= $i; ?>" style="margin-left: 1px; text-decoration: none;">
                                        <?= $model['video_title']; ?>
                                        <input name="VideoTraining[<?= $i; ?>][video_title]" id="VideoTraining_<?= $i; ?>_video_title" type="hidden" value="<?= CHtml::encode($model['video_title']); ?>"/>
                                        <input name="VideoTraining[<?= $i; ?>][video_title]" id="VideoTraining_<?= $i; ?>_video_code" type="hidden" value="<?= CHtml::encode($model['video_code']); ?>"/>
                                    </a>
                                </p>
                            </li>
                        <? $i++; ?>
                    <? } ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<? if (!empty($models)) { ?>
    <div class="col-md-7 col-sm-7 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="hdrVideoTitle"></h2>
                <div class="clearfix"></div>
            </div>
            <div id="divPlayer" class="x_content">
                <p>Click on a video in the Watch List to start.</p>
            </div>
        </div>
    </div>
<? } ?>

<script type="text/javascript">
    var __currentVideoNum = '1';
    $(function(){
        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }

        $('#divVideos').on('click','.lnkPlay',function(){
            var id = $(this).attr('id');
            id =  id.replace('btnPlay-','');
            __currentVideoNum = id;
            var title = $('#VideoTraining_'+id+'_video_title').val().trim();
            var html = $('#VideoTraining_'+id+'_video_code').val().trim();
            var pw = $('#divPlayer').width();
            var ph = Math.ceil((pw * 9) / 16);
            if (html.indexOf('height="720"') > 0 && html.indexOf('width="1280"') > 0) {
                ph = Math.ceil(pw*0.5625);
            }
            html = html.replace(/(width=")\d+/, '$1'+pw);
            html = html.replace(/(height=")\d+/, '$1'+ph);
            html = html.replace(/http\:/g, 'https:');
            var start = html.indexOf('<iframe');
            var stop = html.indexOf('</iframe>');
            $('#hdrVideoTitle').html(title);
            if (start >= 0 && stop > start+7) {
                var iframe =  $($.parseHTML(html));
                if (iframe && iframe.length > 0) {
                    $('#hdrVideoTitle').html(title);
                    $('#divPlayer').html(html);
                    var chk = $('#chkWatch-'+__currentVideoNum);
                    // if not already checked, then check off video
                    if (!chk.is(':checked')) {
                        chk.iCheck('check');
                    }
                } else {
                    $('#divPlayer').html('<p>Could not play video because the iframe video code is missing or invalid.<p>');
                }
            } else {
                $('#divPlayer').html('<p>Could not play video because the video code is not a valid iframe.</p>');
            }
            return false;
        });

        /*
        $('#btnPlay-'+__currentVideoNum).click();
        $( window ).resize(function() {
            $('#btnPlay-'+__currentVideoNum).click();
        });
        */

        $('input.chkWatch').on('ifChanged', function(){
            var recid = $(this).data('recid');
            var tid = $(this).data('tid');
            $.ajax({
                method: 'POST',
                dataType: "json",
                url: '<?= url('my/ack'); ?>',
                data: {
                    type: 'video',
                    ack: ($(this).is(":checked") ? 'yes' : 'no'),
                    id: recid,
                    tid: tid
                },
                success: function (response) {
                    if (response.status !== 'success') {
                        w2alert(response.message);
                    }
                }
            });
        });
    });
</script>
