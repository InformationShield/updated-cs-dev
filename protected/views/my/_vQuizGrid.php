<?php
/* @var $this MyController */

    $lnkAttr = 'data-recid="\'+record.recid+\'" data-toggle="tooltip" data-placement="left" title="\'+((record.description) ? w2utils.stripTags(record.description) : w2utils.stripTags(record.quiz_title))+\'"';
?>
<p>
    Click these <i class="fa fa-question-circle purple" style="font-size: 20px;"></i> or <strong>Title</strong> to take the quiz.<br/>
    These icons mark quizzes that have been <i class="fa fa-eye purple" style="font-size: 20px;"></i> viewed,&nbsp;
    <i class="fa fa-dot-circle-o purple" style="font-size: 20px;"></i> taken, or&nbsp;
    <i class="fa fa-check green" style="font-size: 20px;"></i> passed.
</p>

<script type="text/javascript">
    var _viewed3 = '<i class="fa fa-eye purple" style="font-size: 20px;"></i>';
    var _completed3 = '<i class="fa fa-dot-circle-o purple" style="font-size: 20px;"></i>';
    var _passed3 = '<i class="fa fa-check green" style="font-size: 20px;"></i>';
    var _ajaxListGridColumns3 = [
        { field: 'quiz_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'passed', caption: '<i class="fa fa-check green" style="font-size: 20px;"></i>', size: '35px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                return (record.passed == '1') ? _passed3 : (record.completed == '1') ? _completed3 : (record.viewed == '1') ? _viewed3 : '';
            }
        },
        { field: 'action', caption: 'View', size: '45px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                var icon = 'fa-question-circle';
                var lnkClass = 'lnkGo';
                icon = '<i class="fa '+icon+' purple" style="font-size: 20px;"></i>';
                return '<div>&nbsp;&nbsp;<a class="'+lnkClass+'" href="<?= url('my/viewquiz'); ?>/id/' + record.recid + '" <?= $lnkAttr; ?>>'+icon+'</a></div>';
            }
        },
        { field: 'quiz_title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                return '<div><a class="lnkGo" href="<?= url('my/viewquiz'); ?>/id/' + record.recid + '" <?= $lnkAttr; ?>>' + record.quiz_title + '</a></div>';
            }
        },
        { field: 'quiz_score', caption: 'Score', size: '70px', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                var score=0;
                if (record.possible_score > 0 && record.high_score > 0) {
                    score = Math.ceil((record.high_score/record.possible_score)*100);
                }
                return '<div>'+score+'%</div>';
            }
        },
        { field: 'quiz_status', caption: 'Status', size: '100px', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                var status='Un-Opened';
                status = (record.viewed==1) ? 'Viewed' : status;
                status = (record.completed==1) ? 'Completed' : status;
                status = (record.passed==1) ? 'Passed' : status;
                return '<div>'+status+'</div>';
            }
        },
        { field: 'publish_date', caption: 'Published', size: '75px', searchable: 'text', resizable: true, sortable: true }
    ];

    var _ajaxListGridAddFunction3 = false;
</script>
<?
    $gmAjaxListGrid3 = new gmAjaxListGrid();
    $gmAjaxListGrid3->gridNum = '3';
    $gmAjaxListGrid3->fixedBody = $fixedBody;
    $gmAjaxListGrid3->columnHeaders = true;
    $gmAjaxListGrid3->listUrl = url('my/getquiz');
    $gmAjaxListGrid3->header = $header;
    $gmAjaxListGrid3->controller = "quiz";
    $gmAjaxListGrid3->recid = "quiz_id";
    $gmAjaxListGrid3->allowView = false;
    $gmAjaxListGrid3->allowAdd = false;
    $gmAjaxListGrid3->allowEdit = false;
    $gmAjaxListGrid3->allowDownload = true;
    $gmAjaxListGrid3->allowDelete = false;
    $gmAjaxListGrid3->allowCopy = false;
    $gmAjaxListGrid3->reloadOnCopy = false;
    $gmAjaxListGrid3->showToolbar = false;
    $gmAjaxListGrid3->showFooter = false;
    AjaxListGridGadget::show($gmAjaxListGrid3);
?>