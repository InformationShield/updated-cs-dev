<h2>User Compliance Report for All Users</h2>
<p>
    <a href="" id="lnkExport" class="btn btn-success" style="margin: 0 0 10px 0;">Export to CSV</a>
    &nbsp;
    <a href="<?= $printUrl; ?>" target="_blank" class="btn btn-success" style="margin: 0 0 10px 0;">Print</a>
</p>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'firstname', caption: 'First Name', size: '180px',  resizable: true, sortable: false },
        { field: 'lastname', caption: 'Last Name', size: '100px',  resizable: true, sortable: false },
        { field: 'department_name', caption: 'Department', size: '90px',  resizable: true, sortable: false },
        { field: 'lastvisit_at', caption: 'Last Visit', size: '100px',  resizable: true, sortable: false },
        { field: 'policy_unopened', caption: 'Policy Unopened', size: '120px',  resizable: true, sortable: false },
        { field: 'policy_unopened_per', caption: '%', size: '30px',  resizable: true, sortable: false },
        { field: 'policy_views', caption: 'Policy Viewed', size: '120px',  resizable: true, sortable: false },
        { field: 'policy_views_per', caption: '%', size: '30px',  resizable: true, sortable: false },
        { field: 'policy_acks', caption: 'Policy Acknowledge', size: '120px',  resizable: true, sortable: false },
        { field: 'policy_acks_per', caption: '%', size: '30px',  resizable: true, sortable: false },
        { field: 'training_unopened', caption: 'Training Unopened', size: '120px',  resizable: true, sortable: false },
        { field: 'training_unopened_per', caption: '%', size: '30px',  resizable: true, sortable: false },
        { field: 'training_views', caption: 'Training Viewed', size: '120px',  resizable: true, sortable: false },
        { field: 'training_views_per', caption: '%', size: '30px',  resizable: true, sortable: false },
        { field: 'training_completed', caption: 'Training Completed', size: '120px',  resizable: true, sortable: false },
        { field: 'training_completed_per', caption: '%', size: '30px',  resizable: true, sortable: false },
        { field: 'quiz_unopened', caption: 'Quiz Unopened', size: '120px',  resizable: true, sortable: false },
        { field: 'quiz_unopened_per', caption: '%', size: '30px',  resizable: true, sortable: false },
        { field: 'quiz_viewed', caption: 'Quiz Viewed', size: '120px',  resizable: true, sortable: false },
        { field: 'quiz_viewed_per', caption: '%', size: '30px',  resizable: true, sortable: false },
        { field: 'quiz_completed', caption: 'Quiz Completed', size: '120px',  resizable: true, sortable: false },
        { field: 'quiz_completed_per', caption: '%', size: '30px',  resizable: true, sortable: false },
        { field: 'quiz_passed', caption: 'Quiz Passed', size: '120px',  resizable: true, sortable: false },
        { field: 'quiz_passed_per', caption: '%', size: '30px',  resizable: true, sortable: false }
    ];
    var _ajaxListGridAddFunction = false;
</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = $dataUrl;
    $gmAjaxListGrid->title = '';
    $gmAjaxListGrid->intro = '';
    $gmAjaxListGrid->controller = "reportuser";
    $gmAjaxListGrid->recid = "id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = false;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    $gmAjaxListGrid->showToolbar = false;
    $gmAjaxListGrid->showSearch = false;
    $gmAjaxListGrid->showFooter = false;
    $gmAjaxListGrid->fixedBody = false;
    $gmAjaxListGrid->fixedHeight = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>

<script type="text/javascript">
    $(function(){

        $(document).on('click', '#lnkExport', function () {
            new PNotify({
                title: 'Downloading...',
                text: 'Your file is downloading now.',
                delay: 3500,
                type: 'info'
            });
            window.location = '<?= $exportUrl; ?>';
            return false;
        });

    });
</script>
