<h2>User Compliance Detail Report for <?= $userInfo['firstname'].' '.$userInfo['lastname']; ?></h2>
<p>
    Email: <?= $userInfo['email']; ?><br/>
    Company: <?= $userInfo['company_name']; ?><br/>
    Department: <?= $userInfo['department_name']; ?><br/>
    User Profile: <?= $userInfo['profile_title']; ?><br/>
    Role: <?= $userInfo['company_role_name']; ?><br/>
</p>
<table style="width: 100%; border-spacing: 10px;">
    <? foreach ($data as $key=>$row) { ?>
        <? if ($key == 0) { ?>
            <thead align="left">
                <? foreach($row as $field) { ?>
                    <th><?= $field; ?></th>
                <? } ?>
            </thead>
            <tbody>
        <? } else { ?>
            <tr>
                <? foreach($row as $field) { ?>
                    <td><?= $field; ?></td>
                <? } ?>
            </tr>
        <? } ?>
    <? } ?>
    </tbody>
</table>
