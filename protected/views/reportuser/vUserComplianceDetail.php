<h2>User Compliance Detail Report for <?= $userInfo['firstname'].' '.$userInfo['lastname']; ?></h2>
<div class="left" style="margin: 0 0 10px 0;">
    Email: <?= $userInfo['email']; ?><br/>
    Company: <?= $userInfo['company_name']; ?><br/>
    Department: <?= $userInfo['department_name']; ?><br/>
    User Profile: <?= $userInfo['profile_title']; ?><br/>
    Role: <?= $userInfo['company_role_name']; ?><br/>
</div>
<? if (!isset($userInfo['user_id'])) { ?>
    <div class="clearfix"></div>
    <div>
        <p><strong>This user has not yet registered and has no relevant data.</strong></p>
    </div>
<? } else { ?>
    <div class="right" style="margin: 30px 0 10px 0;">
        <a href="" id="lnkExport" class="btn btn-success" >Export to CSV</a>
        <a href="<?= url('reportuser/printusercompliancedetail', array('id'=>$id)); ?>" target="_blank" class="btn btn-success">Print</a>
    </div>
    <div class="clearfix"></div>
    <script type="text/javascript">
        var _ajaxListGridColumns = [
			{ field: 'profile_title', caption: 'Profile', size: '100%',  resizable: true, sortable: false },
            { field: 'document_type', caption: 'Type', size: '100px',  resizable: true, sortable: false },
            { field: 'document_title', caption: 'Title', size: '100%',  resizable: true, sortable: false },
			{ field: 'status_name', caption: 'Status', size: '100px', hidden: false,  resizable: true, sortable: false },
			{ field: 'ack_date', caption: 'Acknowledged On', size: '160px', hidden: false,  resizable: true, sortable: false },
			{ field: 'passed_date', caption: 'Passed On (Quiz only)', size: '160px', hidden: false,  resizable: true, sortable: false },
			{ field: 'view_date', caption: 'Viewed On', size: '160px', hidden: false,  resizable: true, sortable: false }
        ];
        var _ajaxListGridAddFunction = false;
    </script>
    <?
        $gmAjaxListGrid = new gmAjaxListGrid();
        $gmAjaxListGrid->listUrl = url('reportuser/getusercompliancedetail', array('id'=>$id));
        $gmAjaxListGrid->title = '';
        $gmAjaxListGrid->intro = '';
        $gmAjaxListGrid->controller = "reportuser";
        $gmAjaxListGrid->recid = "id";
        $gmAjaxListGrid->allowAdd = false;
        $gmAjaxListGrid->allowEdit = false;
        $gmAjaxListGrid->allowDelete = false;
        $gmAjaxListGrid->allowCopy = false;
        $gmAjaxListGrid->reloadOnCopy = false;
        $gmAjaxListGrid->showToolbar = false;
        $gmAjaxListGrid->showSearch = false;
        $gmAjaxListGrid->showFooter = false;
        $gmAjaxListGrid->fixedBody = false;
        $gmAjaxListGrid->fixedHeight = false;
        AjaxListGridGadget::show($gmAjaxListGrid);
    ?>

    <script type="text/javascript">
        $(function(){

            $(document).on('click', '#lnkExport', function () {
                new PNotify({
                    title: 'Downloading...',
                    text: 'Your file is downloading now.',
                    delay: 3500,
                    type: 'info'
                });
                window.location = '<?= url('reportuser/exportusercompliancedetail', array('id'=>$id, 'export'=>'csv')); ?>';
                return false;
            });

        });
    </script>
<? } ?>