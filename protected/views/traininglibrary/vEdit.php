<?
/* @var $this TraininglibraryController */
/* @var $model TrainingLibrary */
/* @var $form CActiveForm */
$training_type_label = ($model->training_type == 1) ? 'Document' : 'Video';
$this->pageTitle=Yii::app()->name.' - Edit Training Library '.$training_type_label;
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit Training Library <?= $training_type_label; ?></h3>

<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array('action'=>url('traininglibrary/edit',array('id'=>Obscure::encode($model->training_library_id),'type'=>$model->training_type)),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'id'=>'edit-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->labelEx($model,'doc_title'); ?>
		<?= $form->textField($model,'doc_title',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'doc_title'); ?>
	</div>

	<? if (!empty($model->publish_date)) { ?>
		<div class="row">
			<?= $form->labelEx($model,'publish_date'); ?>
			<p style="color: black;"><?= $model->publish_date; ?></p>
		</div>
	<? } ?>

	<div class="row">
		<?= $form->labelEx($model,'author'); ?>
		<?= $form->textField($model,'author',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'author'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'approved'); ?>
		<select name="TrainingLibrary[approved]" id="TrainingLibrary_approved">
			<option value="0"  <?= ($model->approved=='0') ? 'selected="selected"' :'';?>>Un-Approved</option>
			<option value="1"  <?= ($model->approved=='1') ? 'selected="selected"' :'';?>>Approved</option>
		</select>
		<?= $form->error($model,'approved'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'training_status_id'); ?>
		<select name="TrainingLibrary[training_status_id]" id="TrainingLibrary_training_status_id">
			<? $trainingstatus = TrainingStatus::model()->findAll(array('order'=>'training_status_id asc')); ?>
			<? foreach($trainingstatus as $ts) { ?>
				<option value="<?= $ts->training_status_id; ?>"  <?= ($model->training_status_id==$ts->training_status_id) ? 'selected="selected"' :'';?>><?= $ts->training_status_name; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'training_status_id'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'description'); ?>
		<?= $form->textArea($model,'description',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'description'); ?>
	</div>

	<? if ($model->training_type == 1) { ?>
		<div class="row">
			<?= $form->labelEx($model,'file_name'); ?>
			<? if (empty($model->file_name)) { ?>
				<p>Choose file to upload a document.</p>
			<? } else { ?>
				<p>Current document: <b><?= $model->file_name; ?></b></p>
				<p>Optionally, you may choose another file to upload to replace the current document.</p>
			<? } ?>
			<input type="file" name="Training" id="Training" value="" style="width: 99%; border: none; background: transparent;" />
		</div>
	<? } else { ?>
		<div class="row" style="margin-top: 20px; margin-bottom: 10px;">
			<p>
				You will be able to edit the video series for this training after you hit Save.<br/>
				Or you may click on the "Edit Video Series only" button, if you do not need to edit settings on this page.
			</p>
		</div>
	<? } ?>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
		<? if ($model->training_type == 2) { ?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<?= CHtml::button('Edit Video Series only',array('id'=>"btnEditSeries",'class'=>"btn btn-default")); ?>
		<? } ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('traininglibrary/list'); ?>";
			return false;
		});
		<? if ($model->training_type == 2) { ?>
		$("#btnEditSeries").click(function(){
			window.location = "<?= url('videotraining/editseries',array('ret'=>2,'library'=>1,'id'=>Obscure::encode($model->training_library_id))); ?>";
			return false;
		});
		<? } ?>
	});
</script>