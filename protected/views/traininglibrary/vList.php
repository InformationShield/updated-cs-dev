<? $isAuthor = Utils::isRoleAuthor(); ?>
<? $isSuperOwner = (Utils::isSuper() && Utils::isRoleOwner()); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'training_library_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'doc_title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                if (record.training_type == 'Video') {
                    return '<div><a class="lnkWatch" href="<?= url('videotraining/watchseries'); ?>/ret/2/library/1/id/' + record.recid + '">' + record.doc_title + '</a></div>';
                } else {
                    return '<div><a class="lnkDownload" href="<?= url('traininglibrary/download'); ?>/id/' + record.recid + '">' + record.doc_title + '</a></div>';
                }
            }
        },
        { field: 'author', caption: 'Author', size: '150px', searchable: 'text', resizable: true, sortable: true },
        { field: 'training_type', caption: 'Type', size: '65px', searchable: 'text', resizable: true, sortable: true },
        { field: 'publish_date', caption: 'Published', size: '75px', searchable: 'date', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isSuperOwner) ? '130px' : '70px'; ?>', hideable: false, sortable: false,  resizable: false, hidden: <?= (!$isAuthor) ? 'true':'false'; ?>,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkCopy" href="" data-recid="'+record.recid+'">Copy</a>'
                <? if ($isSuperOwner) { ?>
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkEdit" href="<?= url('traininglibrary/edit'); ?>/id/'+record.recid+'">Edit</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                <? } ?>
                    +'</div>';
                <? } else { ?>
                var html = '<div></div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = function(event) {
        w2popup.open({
            title     : 'Add New Training',
            body      : '<div class="w2ui-centered">Click on <strong>Create Training Document</strong> to create a training document.<br/>'+
            '<br/>Or click on <strong>Create Training Video</strong> to create a training video or video series.'+
            '</div>',
            buttons   : '<button class="btn" onclick="w2popup.close();">Cancel</button> '+
            '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('traininglibrary/edit/type/1'); ?>\';"><strong>Create Training Document</strong></button>'+
            '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('traininglibrary/edit/type/2'); ?>\';"><strong>Create Training Video</strong></button>',
            width     : 550,
            height    : 250,
            overflow  : 'hidden',
            opacity   : '0.2',
            modal     : true,
            showClose : true,
            showMax   : false
        });
        return false;
    };

</script>
<?
/* @var $this TraininglibraryController */
/* @var $model TrainingLibrary */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Training Library</h2>";
    if ($isAuthor) {
        $gmAjaxListGrid->intro = "<p>Click on the <strong>Title</strong> to download the training document or view the video series.</p>";
    }
    $gmAjaxListGrid->controller = "traininglibrary";
    $gmAjaxListGrid->recid = "training_library_id";
    $gmAjaxListGrid->allowAdd = $isSuperOwner;
    $gmAjaxListGrid->allowEdit = $isSuperOwner;
    $gmAjaxListGrid->allowDelete = $isSuperOwner;
    $gmAjaxListGrid->allowCopy = $isAuthor;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
