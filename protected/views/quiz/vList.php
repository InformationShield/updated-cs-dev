<? $isAuthor = Utils::isRoleAuthor(); ?>
<? $lnkAttr = 'data-recid="\'+record.recid+\'" data-toggle="tooltip" data-placement="left" title="\'+((record.description) ? w2utils.stripTags(record.description) : w2utils.stripTags(record.quiz_title))+\'"'; ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'quiz_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'quiz_title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                return '<div><a class="lnkWatch" href="<?= url('quiz/edit'); ?>/ret/1/library/0/id/' + record.recid + '" <?= $lnkAttr; ?>>' + record.quiz_title + '</a></div>';
            }
        },
        { field: 'quiz_status_name', caption: 'Status', size: '75px', searchable: 'text', resizable: true, sortable: true },
        { field: 'publish_date', caption: 'Published', size: '75px', searchable: 'date', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '130px' : '70px'; ?>', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkTest" href="<?= url('quiz/test'); ?>/id/'+record.recid+'">Test</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkEdit" href="<?= url('quiz/edit'); ?>/id/'+record.recid+'">Edit</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                    +'</div>';
                <? } else { ?>
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkTest" href="<?= url('quiz/test'); ?>/id/'+record.recid+'">Test</a>'
                    +'</div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = function(event) {
        w2popup.open({
            title     : 'Add New Quiz',
            body      : '<div class="w2ui-centered">Click on <strong>New Quiz</strong> to create a quiz.'+
            '<br/>Or click on <strong>Copy from Library</strong> to copy existing quiz from the Quiz Library.'+
            '</div>',
            buttons   : '<button class="btn" onclick="w2popup.close();">Cancel</button> '+
            '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('quiz/edit'); ?>\';"><strong>New Quiz</strong></button>'+
            '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('quizlibrary/list'); ?>\';"><strong>Copy from Library</strong></button>',
            width     : 550,
            height    : 250,
            overflow  : 'hidden',
            opacity   : '0.2',
            modal     : true,
            showClose : true,
            showMax   : false
        });
        return false;
    };

</script>
<?
/* @var $this QuizController */
/* @var $model Quiz */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Manage Quizzes</h2>";
    $gmAjaxListGrid->intro = "<p>Click on the <strong>Title</strong> to edit the quiz questions.</p>";
    $gmAjaxListGrid->controller = "quiz";
    $gmAjaxListGrid->recid = "quiz_id";
    $gmAjaxListGrid->allowAdd = $isAuthor;
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->allowDelete = $isAuthor;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
