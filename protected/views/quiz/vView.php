<?php
/* @var $this MyController */
/* @var $model Quiz */
/* @var $question QuizQuestion */
?>
<style type="text/css">
    .correct { font-weight: bold; }
</style>

<div>
    <? AjaxListViewGadget::show($gmAjaxListView); // renderPartial a view ?>
    <div style="margin: -20px 0 20px 10px; ">
        <p><em>Questions and Answers with the correct answer in <strong>bold</strong> text.</em></p>
        <? $qnum = 0; ?>
        <? foreach($questions as $question) { ?>
            <? $qnum++; ?>
            <p>
                <span style="text-decoration: underline;">&nbsp;&nbsp;<?= $qnum; ?>&nbsp;&nbsp;</span><br/>
                <? if (!empty($question->category_name)) { ?>
                    <small><?= $question->category_name; ?></small><br/>
                <? } ?>
                <strong>Question:&nbsp;&nbsp;</strong><?= $question->question; ?>
                <? if (!empty($question->hint)) { ?>
                    <br/><em><?= $question->hint; ?></em>
                <? } ?>
                <div style="margin: 0px 0px 10px 0px;">
                    <strong>Answers:&nbsp;&nbsp;</strong>
                    <? if ($question->question_type == 1) { ?>
                        <div class="<?= ($question->correct_answer == 1) ? 'correct' : ''; ?>">
                            True
                        </div>
                        <div class="<?= ($question->correct_answer == 2) ? 'correct' : ''; ?>">
                            False
                        </div>
                    <? } else if ($question->question_type == 2) { ?>
                        <div class="<?= ($question->correct_answer == 1) ? 'correct' : ''; ?>">
                            Yes
                        </div>
                        <div class="<?= ($question->correct_answer == 2) ? 'correct' : ''; ?>">
                            No
                        </div>
                    <? } else { ?>
                        <? if (!empty($question->answer1)) { ?>
                            <div class="<?= ($question->correct_answer == 1) ? 'correct' : ''; ?>">
                                <?= $question->answer1; ?>
                            </div>
                        <? } ?>
                        <? if (!empty($question->answer2)) { ?>
                            <div class="<?= ($question->correct_answer == 2) ? 'correct' : ''; ?>">
                                <?= $question->answer2; ?>
                            </div>
                        <? } ?>
                        <? if (!empty($question->answer3)) { ?>
                            <div class="<?= ($question->correct_answer == 3) ? 'correct' : ''; ?>">
                                <?= $question->answer3; ?>
                            </div>
                        <? } ?>
                        <? if (!empty($question->answer4)) { ?>
                            <div class="<?= ($question->correct_answer == 4) ? 'correct' : ''; ?>">
                                <?= $question->answer4; ?>
                            </div>
                        <? } ?>
                        <? if (!empty($question->answer5)) { ?>
                            <div class="<?= ($question->correct_answer == 5) ? 'correct' : ''; ?>">
                                <?= $question->answer5; ?>
                            </div>
                        <? } ?>
                    <? } ?>
                </div>
                <? if (isset($answerCounts[$question->quiz_question_id])) { ?>
                    <? $stats = $answerCounts[$question->quiz_question_id]; ?>
                    <? $stats['total'] = $stats['correct']+$stats['wrong']; ?>
                    <? if ($stats['total'] > 0) { ?>
                        <? $stats['correctpercent'] = ($stats['correct'] > 0 && $stats['total'] > 0) ? ceil(($stats['correct']/$stats['total'])*100): 0; ?>
                        <? $stats['wrongpercent'] = ($stats['wrong'] > 0 && $stats['total'] > 0) ? ceil(($stats['wrong']/$stats['total'])*100): 0; ?>
                        <div style="margin: 0px 0px 20px 0px;">
                            <strong>User Results: </strong>Users had <?= $stats['correct']; ?> (<?= $stats['correctpercent'];?>%) correct and <?= $stats['wrong']; ?> (<?= $stats['wrongpercent'];?>%) wrong answers for a total of <?= $stats['total'];?> answers.
                        </div>
                    <? } else { ?>
                        <div style="margin: 0px 0px 20px 0px;">
                            <strong>User Results: </strong>No users have answered this question.
                        </div>
                    <? } ?>
                <? } ?>
            </p>
        <? } ?>
    </div>
</div>
