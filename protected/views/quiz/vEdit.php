<?
/* @var $this QuizLibraryController */
/* @var $model Quiz */
/* @var $form CActiveForm */
$listUrl = url('quiz/list');
if (stripos($editUrl, 'quizlibrary')) {
	$listUrl = url('quizlibrary/list');
}
?>

<style type="text/css">
	.sortablelist { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	.sortablelist li { margin: 0; padding: 0; font-size: 1.2em; }
	.sortablelist li { height: auto; }
	.ui-state-highlight { height: 3em; min-height: 3em; margin: 0; }
	.sortablelist li .x_panel { padding: 0; margin: 0; }
	.sortablelist .panel_toolbox { min-width: 45px; }
	.sortablelist li .x_title { border: none; padding: 0; margin: 0; }
	.sortablelist li .panel_toolbox { margin: 8px 5px 0 5px; }
	.sortablelist li .x_title h1 { margin: 10px; font-size: 1em; height: 1.5em; line-height: 1.5em; }
</style>

<h3>Edit Quiz</h3>

<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array('action'=>$editUrl,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'id'=>'edit-form',
		'enableAjaxValidation'=>false,
	)); ?>
	<input type="hidden" id="hidExit" name="exit" value="">

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<? if (!empty($errorMessages)) { ?>
		<div class="errorSummary">
			<p>The following errors occured while saving changes:</p>
			<ul>
				<? foreach($errorMessages as $errorMessage) { ?>
					<li><?= $errorMessage; ?></li>
				<? } ?>
			</ul>
		</div>
	<? } ?>

	<div class="row">
		<?= $form->labelEx($model,'quiz_title'); ?>
		<?= $form->textField($model,'quiz_title',array('style'=>"width: 99%;",'maxlength'=>"256")); ?>
		<?= $form->error($model,'quiz_title'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'quiz_status_id'); ?>
		<select name="<?= get_class($model); ?>[quiz_status_id]" id="<?= get_class($model); ?>_quiz_status_id">
			<? $quizstatus = QuizStatus::model()->findAll(array('order'=>'quiz_status_id asc')); ?>
			<? foreach($quizstatus as $ts) { ?>
				<option value="<?= $ts->quiz_status_id; ?>"  <?= ($model->quiz_status_id==$ts->quiz_status_id) ? 'selected="selected"' :'';?>><?= $ts->quiz_status_name; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'quiz_status_id'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'description'); ?>
		<?= $form->textArea($model,'description',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'passing_score'); ?>
		<?= $form->textField($model,'passing_score',array('maxlength'=>"6",'size'=>'6')); ?>
		<?= $form->error($model,'passing_score'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'max_retakes'); ?>
		<?= $form->textField($model,'max_retakes',array('maxlength'=>"6",'size'=>'6')); ?>
		<?= $form->error($model,'max_retakes'); ?>
	</div>

	<div class="row">
		<label>Questions List</label>
		<p class="hasQuestions">
			Changes to the list are not saved until you click the "Save Changes" button.<br/>
			Use drag and drop on the questions to change the order they will appear in the quiz.
		</p>
	</div>
	<ul id="sortableQuestions" class="sortablelist">
		<? $iQuestions=1; ?>
		<? foreach($questionList as $question) { ?>
			<? $this->renderPartial('//quiz/_EditQuestion',array('question'=>$question,'form'=>$form,'iQuestions'=>$iQuestions)); ?>
			<? $iQuestions++; ?>
		<? } ?>
	</ul>
	<ul class="sortablelist emptyQuestions" <?= ($iQuestions > 1) ? 'style="display: none;"' : ''; ?>>
		<li>
			<div class="x_panel tile">
				<div class="x_title">
					<h1>The Questions List is currently empty.</h1>
				</div>
			</div>
		</li>
	</ul>
	<div>
		<a id="btnAddQuestions" href="" class="btn btn-default"><span class="fa fa-plus"></span> Add Question</a>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save', array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Save & Exit', array('id'=>"btnSaveExit",'class'=>"btn btn-success")); ?>
		<a href="<?= $listUrl; ?>" id="btnCancel" class="btn btn-primary" type="button">Cancel</a>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<? $newQuestion = array('quiz_question_id'=>'0','question_type'=>'1','correct_answer'=>'1','score'=>'10','question'=>'','hint'=>'','category_name'=>'','answer1'=>'','answer2'=>'','answer3'=>'','answer3'=>'','answer4'=>'','answer5'=>''); ?>
<ul id="blankQuestions" style="display: none;">
	<? $this->renderPartial('//quiz/_EditQuestion',array('question'=>$newQuestion,'form'=>$form,'iQuestions'=>0)); ?>
</ul>

<script type="text/javascript">
	var __nextQuestions = <?= $iQuestions; ?>;

	function isEmptyQuestions() {
		var li = $('#sortableQuestions').children();
		if (li.length > 0) {
			$('.emptyQuestions').hide();
			$('.hasQuestions').show();
		} else {
			$('.emptyQuestions').show();
			$('.hasQuestions').hide();
		}
	}
	
	function AddQuestions(){
		var blank = $('#blankQuestions').html();
		blank = blank.replace(/\[0\]/g,'['+__nextQuestions+']');
		blank = blank.replace(/_0_/g,'_'+__nextQuestions+'_');
		blank = blank.replace(/-0/g,'-'+__nextQuestions);
		var btnEditId = 'btnEditQuestions-'+__nextQuestions;
		$('#sortableQuestions').append(blank);
		isEmptyQuestions();
		setTimeout(function(){
			$('#'+btnEditId).click();
		}, 10);
		__nextQuestions++;
		return false;
	}

	function SaveQuestion($control) {
		var save = true;
		$('.inpQuestion').each(function(){
			if (save && $control.val().trim().length <= 0) {
				var id = $control.closest('.x_content').attr('id');
				id = id.replace('frmEdit-', '');
				if (id > 0) {
					save = false;
					w2alert('A question is not filled out correctly. Must have a question.');
					$('.x_content').hide();
					$('#btnEditQuestions-'+id).click();
				}
			}
		});
		if (save) {
			$('#edit-form').submit();
		}
	}

	$(function(){
		$( "#sortableQuestions" ).sortable({
			//placeholder: "ui-state-highlight"
			handle: '.handle'
		});
		$( "#sortableQuestions" ).disableSelection();

		isEmptyQuestions();

		$('input[type=us-date]').w2field('date');

		$("#btnSave").click(function(){
			$('#hidExit').val('0');
			SaveQuestion($(this));
			return false;
		});

		$("#btnSaveExit").click(function() {
			$('#hidExit').val('1');
			SaveQuestion($(this));
			return false;
		});

		$("#btnAddQuestions").click(function(){
			AddQuestions('');
			return false;
		});

		$('#sortableQuestions').on('click','.lnkDeleteQuestions',function(){
			var idname = $(this).data('idname');
			w2confirm('Are you sure you want to delete question?').yes(function(){
				$('#'+idname).remove();
				isEmptyQuestions();
				return false;
			});
			return false;
		});

		$('#sortableQuestions').on('click','.lnkEditQuestions',function(){
			var id = $(this).attr('id');
			id = id.replace('btnEditQuestions-','');
			var v = $('#frmEdit-'+id).is(':visible');
			$('.x_content').hide();
			if (!v) $('#frmEdit-'+id).show();
			return false;
		});

		$('#sortableQuestions').on('mousedown','.handle',function(){
			$('.x_content').hide();
			return true;
		});

		$('#sortableQuestions').on('change','.qtypeselect',function () {
			var id = $(this).parent().attr('id');
			if (id) {
				id = id.replace('qtype-', '');
				var qtype = $(this).val();
				$('.qtypeanswer','#frmEdit-'+id).hide();
				$('#qtype' + qtype + '-' + id).show();
			}
			return false;
		});

        $('#sortableQuestions').on('change','.inpQuestion',function () {
            var id = $(this).closest('.x_content').attr('id');
            if (id) {
                id = id.replace('frmEdit-', '');
                $('#questiontitle-'+id).html($(this).val());
            }
            return false;
        });

	});
</script>
