<div class="x_panel">
    <div class="x_title">
        <h3>Baseline Adopted!</h3>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <p>
            You have adopted the baseline entitled <strong><?= $details['baseline_name']; ?></strong> from <strong><?= $details['company_name']; ?></strong>.
            We have copied a group of<?= ($count > 1) ? ' '.$count : ''; ?> security controls to your security program.
            Your next step is to evaluate each of these Controls and see how they apply to your business.
            <br/><br/>
            Next Steps:<br/>
            <br/><a href="<?= url('controlbaseline/list'); ?>" class="btn btn-success">1.&nbsp;&nbsp;Review/Refine Security Program Baseline</a><br/>
            <br/>(or)<br/>
            <br/><a href="<?= url('policy/list'); ?>" class="btn btn-success">2.&nbsp;&nbsp;Begin developing Security Policies for your program.</a><br/>
        </p>
    </div>
</div>