<?
/* @var $this WizardController */
/* @var $model Invite */

$this->pageTitle=Yii::app()->name . ' - Accept Invitation';
$this->breadcrumbs=array(
    'Accept Invitation',
);
?>

<h1>Accept Invitation and Adopt Baseline</h1>

<p>
    You have been invited by <strong><?= $details['company_name']; ?></strong> to adopt there suggested baseline of security controls entitled <strong><?= $details['baseline_name']; ?></strong>.
    If you choose to adopt the baseline, the security controls will be added to your Control Baseline if they currently are not present. If you reject the baseline you will be taken to the control wizard.
    <br/><br/>
    NOTE: If you chose to Adopt or Reject the baseline from <strong><?= $details['company_name']; ?></strong>, they will be notified of your choice.
    You can also choose to ignore the invitation, and <strong><?= $details['company_name']; ?></strong> will not be notified.
    <br/><br/>
    <a href="<?= Yii::app()->createAbsoluteUrl('invite/accept'); ?>" class="btn btn-success">Adopt Baseline</a>
    <a href="<?= Yii::app()->createAbsoluteUrl('invite/reject'); ?>" class="btn btn-warning">Reject Baseline</a>
    <a href="<?= Yii::app()->createAbsoluteUrl('invite/ignore'); ?>" class="btn btn-primary">Ignore Invitation</a>
</p>
