<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'invite_id', caption: 'iid', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'invitee_name', caption: 'Invitee Name', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'invitee_email', caption: 'Invitee Email', size: '100%', hideable: false, sortable: true, searchable: 'text', resizable: true },
        { field: 'sent_date', caption: 'Sent Date', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'expiry_date', caption: 'Invitation Expiry Date', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'accepted', caption: 'Status', size: '100%', resizable: true, sortable: false,
            render: function(record, index, column_index){
                var html = '';
                if(record.accepted == "0"){
                    html = 'Pending';
                }else if(record.accepted == "1"){
                    html = 'Accepted at (' + record.accepted_date + ')';
                }else{
                    html = 'Rejected at (' + record.accepted_date + ')';
                }
                return html;
            }
        },
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Invites</h2>";
    $gmAjaxListGrid->intro = '<p>This is the list of partners or vendors you have sent invites to and the status of the invite. '
        .'If they have not taking action then the status will be Pending. If they accept or reject an invite, then the status '
        .'will show Accepted or Rejected and the date of the action.</p>';
    $gmAjaxListGrid->controller = "invite";
    $gmAjaxListGrid->recid = "invite_id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = false;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = true;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
