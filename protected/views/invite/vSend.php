<?
/* @var $this SiteController */
/* @var $model mInvite */
/* @var $form CActiveForm */
?>

<h3>Invite a Partner Or Vendor</h3>

<div class="form">

<? $form=$this->beginWidget('CActiveForm', array('action'=>url('invite/send'),
	'id'=>'edit-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->hiddenField($model,'invite_guid'); ?>
		<?= $form->hiddenField($model,'inviteUrl'); ?>
        <?= $form->labelEx($model,'invitee_name'); ?>
		<?= $form->textField($model,'invitee_name',array('id'=>'inpInvitee','style'=>"width: 99%;",'maxlength'=>256)); ?>
		<?= $form->error($model,'invitee_name'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'invitee_email'); ?>
		<?= $form->textField($model,'invitee_email',array('id'=>'inpEmail','style'=>"width: 99%;",'maxlength'=>256)); ?>
		<?= $form->error($model,'invitee_email'); ?>
	</div>

	<div class="row">
		<? $baselines = $model->getBaselines(); ?>
		<? if (!empty($baselines)) { ?>
			<?= $form->labelEx($model,'baseline_id'); ?>
			<select name="mInvite[baseline_id]" id="mInvite_baseline_id" style="width: 320px;">
				<? foreach ($baselines as $baseline) { ?>
					<option value="<?= $baseline['baseline_id']; ?>" <?= ($model->baseline_id == $baseline['baseline_id']) ? 'selected="selected"' : ''; ?>><?= $baseline['baseline_name']; ?></option>
				<? } ?>
			</select>
			<?= $form->error($model,'baseline_id'); ?>
		<? } ?>
	</div>

	<h4>Email to be sent on your behalf:</h4>
	<div class="x_panel">
		<div style="width: 100%; overflow: hidden; margin: 10px;">
			<p>
				<strong>To:</strong>&nbsp;<span id="toEmail">[email]</span><br/>
				<strong>CC:</strong>&nbsp;<?= $model->fromEmail; ?><br/>
				<strong>Subject:</strong>&nbsp;<?= $model->msgSubject; ?><br/>
				<br/>
				Dear <span id="invitee">[name]</span>,<br/>
				<br/>
				<?= $model->msgBody; ?>
			</p>
		</div>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Invite',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$("#inpEmail").on('change keydown paste input', function(){
			$('#toEmail').text($(this).val());
		});
		$("#inpInvitee").on('change keydown paste input', function(){
			$('#invitee').text($(this).val());
		});
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('compliance/companies'); ?>";
			return false;
		});
	});
</script>