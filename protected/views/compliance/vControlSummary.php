<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>
<? $isAdmin = Utils::isRoleAdmin(); ?>
<?
    $pieHeight = 600;
    $lnkAttr = 'data-recid="\'+record.recid+\'" data-toggle="tooltip" data-placement="left" title="\'+((record.description) ? w2utils.stripTags(record.description) : w2utils.stripTags(record.quiz_title))+\'"';
?>
<h2 style="text-align: center;">Control Summary Reports</h2>
<div class="left" style="margin: 0 0 10px 0;">
    <p>
        Click on the <strong>Report Filter Name</strong> to view the chart for that report.<br/>
    </p>
</div>
<!--<div class="right" style="margin: 0 0 10px 0;">-->
<!--    <a href="" id="lnkExport" class="btn btn-success" >Export to CSV</a>-->
<!--    <a href="--><?//= $printUrl; ?><!--" target="_blank" class="btn btn-success">Print</a>-->
<!--</div>-->

<style type="text/css">
    .piechart { margin: 0 0 20px 0; height: <?= $pieHeight; ?>px;}
    .pieloader { margin: 75px 0 0 75px; }
</style>

<? /* Grid code */ ?>
<script type="text/javascript">
    function reportTemplate(recid) {
        var record = w2ui['grid'].get(recid);
        if (record) {
            $('#spnTitle').text(record.template_name);
            $('#divDescription').html(record.template_filter);
            var quizTrackingChart = new FusionCharts({
                type: 'pie3D',
                renderAt: 'divChart',
                width: '100%',
                height: '<?= $pieHeight; ?>',
                dataFormat: 'json',
                dataSource: {
                    "chart": {
                        "caption": "Passed " + record.quiz_passed + " of " + record.quiz_count,
                        "subCaption": "",
                        "numberPrefix": "",
                        "showPercentInTooltip": "1",
                        "showLegend": "1",
                        "legendShadow": '1',
                        "decimals": "0",
                        "useDataPlotColorForLabels": "1",
                        "theme": "fint"
                    },
                    "data": [
                        {"label": "Unopened", "value": record.quiz_unopened, "link": "", "color": "FF0000", "labelFontColor": "000000"},
                        {"label": "Viewed", "value": record.quiz_viewed, "link": "", "color": "FFFF00", "labelFontColor": "000000"},
//                        {"label": "Completed", "value": record.quiz_completed, "link": "", "color": "00BFFF", "labelFontColor": "000000"},
                        {"label": "Passed", "value": record.quiz_passed, "link": "", "color": "00FF00", "labelFontColor": "000000"}
                    ]
                }
            }).render();
            return true;
        } else {
            return false;
        }
    };

    var _showChart = true;
    var _ajaxListGridColumns = [
        { field: 'filter_name', caption: 'Report Filter Name', size: '100%',  resizable: true, sortable: false,
            render: function (record, index, column_index) {
                return '<div><a class="lnkChart" href="" <?= $lnkAttr; ?>>' + record.filter_name+ '</a></div>';
            }
        },
		{ field: 'filter_desc', caption: 'Filter Description', size: '100%',  resizable: true, sortable: false,
			render: function (record, index, column_index) {
				return '<div><a class="lnkChart" href="" <?= $lnkAttr; ?>>' + record.filter_desc+ '</a></div>';
			}
		},
        { field: 'updated_on', caption: 'Last Modified', size: '90px',  resizable: true, sortable: false},
		{ field: 'action', caption: 'Actions', size: '100px', hideable: false, sortable: false,  resizable: false, hidden: <?= (!$isAdmin) ? 'true':'false'; ?>,
			render: function (record, index, column_index) {
				var html = '<div></div>';
				if (index > 0) {
					<? if ($isAdmin) { ?>
									html = '<div>'
										+'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
										+'<span>&nbsp;</span>'
										+'<a class="lnkEdit" href="<?= url('reportfilter/edit'); ?>/id/'+record.recid+'">Edit</a>'
										+'<span>&nbsp;</span>'
										+'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
										+'</div>';
					<? } ?>
				}
				return html;
			}
		}
    ];

    var _ajaxListGridAddFunction = false;
</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = url('reportfilter/list');
    $gmAjaxListGrid->title = "";
    $gmAjaxListGrid->intro = "";
    $gmAjaxListGrid->controller = "reportfilter";
    $gmAjaxListGrid->recid = "recid";
    $gmAjaxListGrid->allowAdd = true;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = true;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    $gmAjaxListGrid->showToolbar = true;
    $gmAjaxListGrid->showSearch = false;
    $gmAjaxListGrid->showFooter = false;
	$gmAjaxListGrid->fixedBody = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>

<? /* Chart code */ ?>
<div id="divChart">
	<div style="width: 100%; height: 665px; text-align: center;">
		<img src="/images/ajax-loader.gif" style="margin-top: 300px;"/>
	</div>
</div>
<script type="text/javascript">
function getChart() {
	$.ajax({
		method: 'POST',
		dataType: "html",
		url: '<?= ($baselineId) ? url('compliance/getcontrolsummary/id/' . $baselineId) : url('compliance/getcontrolsummary'); ?>',
		success: function(response){
			if (response && response.length > 10) {
				$("#divChart").html(response);
			} else {
				w2alert('Failed to load chart.');
			}
		},
		error: function (xhr, status) {
			alert("Sorry, failed to load chart.");
		}
	});
}
FusionCharts.ready(function() {
	getChart();
});
</script>
