<?php
/* @var $this ComplianceController */
/* @var $model mScoring */
?>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>
<style type="text/css">
    .w2ui-grid-data .progress { margin: 0!important; padding:0!important; max-height: 20px!important; }
    .w2ui-grid-data .progress-bar { box-shadow: none; top: 2px; }
    .w2ui-grid-data .progress-bar.red { background-color: red!important; }
    .w2ui-grid-data .progress-bar.yellow { background-color: yellow!important; }
    .w2ui-grid-data .progress-bar.green { background-color: green!important; }
</style>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'company_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'action', caption: 'Login', size: '44px',  resizable: false, sortable: false,
            render: function(record, index, column_index) {
                if (record.recid == '<?= Obscure::encode(Utils::contextCompanyId()); ?>') {
                    return '<div>&nbsp;&nbsp;</div>';
                } else {
                    return '<div>&nbsp;&nbsp;<a href="<?= url('site/context'); ?>/id/' + record.recid + '"><i class="fa fa-sign-in purple" style="font-size: 16px;"></i></a></div>';
                }
            }
        },
        { field: 'name', caption: 'Company Name', size: '100%',  resizable: true, sortable: false,
            render: function (record, index, column_index) {
                return '<div><a class="lnkChart" href="" data-geturl="<?= url('compliance/getriskscoring'); ?>/id/' + record.recid + '/baseline/' + record.baseline_id +'">' + record.name + '</a></div>';
            }
        },
        { field: 'inherent_risk_score', caption: 'Cyber Inherent Risk Score', size: '125px',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                return '<div>'+record.inherent_risk_score+'/'+record.inherent_risk_score_max+' ('+record.inherent_risk_score_percent+'%)</div>';
            }
        },
//        { field: 'baseline_name', caption: 'Baseline Name', size: '100px', resizable: true, sortable: false },
//        { field: 'risk_score_total', caption: 'Total Points', size: '80px', hidden: true,  resizable: true, sortable: false },
//        { field: 'risk_score_max', caption: 'Possible Points', size: '100px', hidden: true,  resizable: true, sortable: false },
        { field: 'risk_score', caption: 'Risk Score', size: '125px',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                return '<div>'+record.risk_score_total+'/'+record.risk_score_max+' ('+record.risk_score+'%)</div>';
            }
        },
        { field: 'risk_score_progress', caption: '% Progress', size: '101px',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                var score = (record.risk_score < 0) ? 0 : ((record.risk_score > 100) ? 100 : record.risk_score);
                var color = (score < 25) ? 'red' : ((score < 75) ? 'yellow' : 'green');
                return '<div class="progress">'
                        +'<div class="progress-bar '+color+'" role="progressbar" aria-valuenow="'+score+'" aria-valuemin="0" aria-valuemax="100" style="width:'+score+'%">'
                        +'</div>'
                    +'</div>';
            }
        }
    ];

    var _ajaxListGridAddFunction = false;
</script>
<?
/* @var $this QuizController */
/* @var $model Quiz */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = url('compliance/getcompanies');
    $gmAjaxListGrid->title = "<h2>Companies Risk Scores</h2>";
    if (Utils::isRoleAdmin()) {
        $gmAjaxListGrid->intro = "<p><a href=\"" . url('invite/send') . "\" class=\"btn btn-success\" style=\"float: right; margin: 0 0 0 0;\">Invite a Partner or Vendor</a></p>";
    } else {
        $gmAjaxListGrid->intro = "";
    }
    $gmAjaxListGrid->intro .= "<p>Click on the <strong>Company Name</strong> to view the risk score report for that company.<br/>";
    $gmAjaxListGrid->intro .= "Click the <i class=\"fa fa-sign-in purple\" style=\"font-size: 16px;\"></i> to switch your current login to that company's site.<br/></p>";
    $gmAjaxListGrid->controller = "compliance";
    $gmAjaxListGrid->recid = "company_id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = false;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    $gmAjaxListGrid->showToolbar = false;
    $gmAjaxListGrid->showSearch = false;
    $gmAjaxListGrid->showFooter = false;
    $gmAjaxListGrid->fixedBody = true;
    $gmAjaxListGrid->fixedHeight = $height;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>

<div id="divChart" class="x_panel" style="display: none;">
    <div style="width: 100%; height: 665px; text-align: center;">
        <img src="/images/ajax-loader.gif" style="margin-top: 300px;"/>
    </div>
</div>

<div id="divChartNew" style="display: none;">
    <div style="width: 100%; height: 665px; text-align: center;">
        <img src="/images/ajax-loader.gif" style="margin-top: 300px;"/>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        function getChart(dataUrl) {
            if (dataUrl.length > 0) {
                $('#divChart').html($('#divChartNew').html());
                $('#divChart').show();
                $.ajax({
                    method: 'POST',
                    dataType: "html",
                    url: dataUrl,
                    success: function (response) {
                        if (response && response.length > 10) {
                            $("#divChart").html(response);
                        } else {
                            w2alert('Failed to load chart.');
                        }
                    },
                    error: function (xhr, status) {
                        alert("Sorry, failed to load chart.");
                    }
                });
            } else {
                $('#divChart').hide();
            }
        }
        FusionCharts.ready(function() {
            <? if ($dataUrl) { ?>
                getChart('<?= $dataUrl; ?>');
            <? } ?>
        });
        $(document).on('click','.lnkChart',function(){
            var dataUrl = $(this).data('geturl');
            getChart(dataUrl);
            return false;
        });
    });
</script>
