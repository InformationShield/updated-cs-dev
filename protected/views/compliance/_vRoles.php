<?php
/* @var $this ComplianceController */
/* @var $model mScoring */
?>
<? if (empty($model->role_status_sums)) { ?>
    <div style="margin: 30px 20px 30px 20px;">
        <p>
            Security Roles are used to document the employee roles of your information security program.
            You assign Security Roles to a specific user.
            <br/><br/>
            <a href="<?= url('securityrole/list'); ?>" class="btn btn-success">Go to Security Roles</a><br/>
            <br/><br/>
            <br/>
        </p>
    </div>
<? } else { ?>
    <div style="margin: 20px;">
        <p>
            <? foreach($model->role_status_sums as $row) { ?>
                <a href="<?= url('securityrole/list',array('status'=>$row['role_status_id'])) ?>">Roles <?= $row['role_status_name']; ?></a>:&nbsp;<b><?= $row['total']; ?></b>
                <br/>
            <? } ?>
            &raquo; &nbsp;<a href="<?= url('securityrole/list'); ?>">Roles Status Details</a>
        </p>
    </div>
    <div id="divRoleSummaryChart">
        <img src="/images/ajax-loader.gif" style="margin: 250px 0 250px 100px;" />
    </div>

    <script type="text/javascript">
        var roleSummaryChart = new FusionCharts({
            type: 'pie3D',
            renderAt: 'divRoleSummaryChart',
            width: '100%',
            height: '600',
            dataFormat: 'json',
            dataSource:  {
                "chart": {
                    "caption": "Assigned <?= $model->total_assigned; ?> of <?= $model->total_roles; ?> roles",
                    "subCaption": "",
                    "numberPrefix": "",
                    "showPercentInTooltip": "1",
                    "showLegend": "1",
                    "legendShadow": '1',
                    "decimals": "0",
                    "useDataPlotColorForLabels": "1",
                    "theme": "fint"
                },
                "data": <?= $model->role_summary_json; ?>
            }
        }).render();
    </script>
<? } ?>