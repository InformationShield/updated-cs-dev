<?php
/* @var $this ComplianceController */
/* @var $model mScoring */
?>
<div>
    <h4>Policy Summary</h4>
    <? if (empty($model->policy_status_sums)) { ?>
        <div style="margin: 30px 20px 30px 20px;">
            <p>
                There are no Policy documents available. You can create Policy documents and publish them to your users.
                Policy Tracking will record which users have opened your policies as well as record user "acknowledgement"
                actions on your policies.
                <br/><br/>
                <a href="<?= url('policy/list'); ?>" class="btn btn-success">Go to Policies</a><br/>
            </p>
        </div>
    <? } else { ?>
        <div style="margin: 10px;">
            <p>
                <small>
                    <? foreach($model->policy_status_sums as $row) { ?>
                        <a href="<?= url('policy/list',array('status'=>$row['policy_status_id'])) ?>"><?= $row['policy_status_name']; ?></a>:&nbsp;<b><?= $row['total']; ?></b>
                        <br/>
                    <? } ?>
                    &raquo; &nbsp;<a href="<?= url('policy/list'); ?>">Policy Status Details</a>
                </small>
            </p>
        </div>
        <div id="divPolicySummaryChart">
            <img src="/images/ajax-loader.gif" />
        </div>
    <? } ?>
</div>

<script type="text/javascript">
    var policySummaryChart = new FusionCharts({
        type: 'pie3D',
        renderAt: 'divPolicySummaryChart',
        width: '100%',
        height: '200',
        dataFormat: 'json',
        dataSource:  {
            "chart": {
                "caption": "Published <?= $model->total_published; ?> of <?= $model->total_policy; ?> Policies",
                "subCaption": "",
                "numberPrefix": "",
                "showPercentInTooltip": "1",
                "showLegend": "0",
                "legendShadow": '1',
                "decimals": "0",
                "useDataPlotColorForLabels": "1",
                "theme": "fint"
            },
            "data": <?= $model->policy_summary_json; ?>
        }
    }).render();
</script>