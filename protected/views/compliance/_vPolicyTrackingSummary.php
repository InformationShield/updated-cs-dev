<?php
/* @var $this ComplianceController */
/* @var $model mScoring */
?>
<? if (empty($model->policy_tracking_status_sums)) { ?>
    <div style="margin: 30px 20px 30px 20px;">
        <p>
            There is no Policy Tracking data available. Publish policy documents and assign them to User Profiles that
            define your company users. Then you can track the progress of published policies opened and acknowledged by your users.
            <br/><br/>
            <a href="<?= url('policy/list'); ?>" class="btn btn-success">Go to Policies</a><br/>
        </p>
    </div>
<? } else { ?>
    <div style="margin: 20px;">
        <p>
            <? foreach($model->policy_tracking_status_sums as $row) { ?>
                <a href="<?= url('policy/list') ?>"><?= $row['label']; ?></a>:&nbsp;<b><?= $row['total']; ?></b>
                <br/>
            <? } ?>
            &raquo; &nbsp;<a href="<?= url('policy/list'); ?>">Policy Details</a>
        </p>
    </div>
    <div id="divPolicyTrackingSummaryChart">
        <img src="/images/ajax-loader.gif" style="margin: 250px 0 250px 100px;" />
    </div>

    <script type="text/javascript">
        var controlSummaryChart = new FusionCharts({
            type: 'pie3D',
            renderAt: 'divPolicyTrackingSummaryChart',
            width: '100%',
            height: '600',
            dataFormat: 'json',
            dataSource:  {
                "chart": {
                    "caption": "Acknowledged <?= $model->total_acknowledged; ?> of <?= $model->total_acknowledged + $model->total_viewed; ?> viewed Policies",
                    "subCaption": "",
                    "numberPrefix": "",
                    "showPercentInTooltip": "1",
                    "showLegend": "1",
                    "legendShadow": '1',
                    "decimals": "0",
                    "useDataPlotColorForLabels": "1",
                    "theme": "fint"
                },
                "data": <?= $model->policy_tracking_summary_json; ?>
            }
        }).render();
    </script>
<? } ?>