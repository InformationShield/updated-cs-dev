<?php
/* @var $this ComplianceController */
/* @var $model mScoring */
?>
<h4>Baseline: <?= $baseline_name; ?></h4>
<h4 style="text-align: center;">Total <?= $model->control_status_name; ?> is <?= $model->total_score ?> of <?= $model->max_score ?> controls</h4>
<div id="divComplianceDetailChart">
    <img src="/images/ajax-loader.gif" style="margin: 250px;" />
</div>

<script type="text/javascript">
    var complianceDetailChart = new FusionCharts({
        "type": "bar2D",
        "renderAt": "divComplianceDetailChart",
        "width": "100%",
        "height": "600",
        "dataFormat": "json",
        "dataSource": {
            "chart": {
                "caption": "Compliance Detail for <?= $model->control_status_name; ?> by Category",
                "subCaption": "",
                "xAxisName": "Category",
                "yAxisName": "Percentage with status of <?= $model->control_status_name; ?>",
                "yAxisMaxValue": "100",
                "numberPrefix": "",
                "theme": "fint"
            },
            "data": <?= $model->category_scores_json; ?>
        }
    }).render();
</script>