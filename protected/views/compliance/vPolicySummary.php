<?php
/* @var $this ComplianceController */
/* @var $model mScoring */
?>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>

<h2 style="text-align: center;">Policy Summary</h2>

<div id="divChart" class="x_panel">
    <div style="width: 100%; height: 665px; text-align: center;">
        <img src="/images/ajax-loader.gif" style="margin-top: 300px;"/>
    </div>
</div>

<script type="text/javascript">
    function getChart() {
        $.ajax({
            method: 'POST',
            dataType: "html",
            url: '<?= url('compliance/getpolicysummary'); ?>',
            success: function(response){
                if (response && response.length > 10) {
                    $("#divChart").html(response);
                } else {
                    w2alert('Failed to load chart.');
                }
            },
            error: function (xhr, status) {
                alert("Sorry, failed to load chart.");
            }
        });
    }
    FusionCharts.ready(function() {
        getChart();
    });
</script>