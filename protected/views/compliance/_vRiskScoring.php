<?php
/* @var $this ComplianceController */
/* @var $model mScoring */
/* @var $company Company */
/* @var $baseline Baseline */
?>
<? if (isset($company)) { ?>
    <h4 style="text-align: center;"><?= $company->name; ?></h4>
<? } ?>
<? if (isset($baseline) && !empty($baseline->baseline_name)) { ?>
    <h4>Baseline: <?= $baseline->baseline_name; ?></h4>
<? } ?>
<h4 style="text-align: center;">Score: <?= $model->total_score ?> of <?= $model->max_score ?> points</h4>
<div id="divRiskScoringChart">
    <img src="/images/ajax-loader.gif" style="margin: 250px;" />
</div>

<script type="text/javascript">
    var riskScoringChart = new FusionCharts({
        "type": "bar2D",
        "renderAt": "divRiskScoringChart",
        "width": "100%",
        "height": "600",
        "dataFormat": "json",
        "dataSource": {
            "chart": {
                "caption": "Compliance Detail by Category",
                "subCaption": "",
                "xAxisName": "Category",
                "yAxisName": "Percentage of Possible Score",
                "yAxisMaxValue": "100",
                "numberPrefix": "",
                "theme": "fint"
            },
            "data": <?= $model->category_scores_json; ?>
        }
    }).render();
</script>