<?php
/* @var $this ComplianceController */
/* @var $model mScoring */
?>
<? if (empty($model->control_status_sums)) { ?>
	<h4>Baseline: <?= $baseline_name; ?></h4>
    <div style="margin: 30px 20px 30px 20px;">
        <p>
            You have not established the baseline for your information security program.
            Run the Control Wizard to create a selected group of security functions ("controls") that define your program.
            After running the wizard, your next step is to evaluate each of these Controls and see how they apply to your business.
            <br/><br/>
            <a href="<?= url('wizard/control'); ?>" class="btn btn-success">Run the Control Wizard</a><br/>
        </p>
    </div>
<? } else { ?>
	<? /* h4>Baseline: zzz<?= $baseline_name; ?></h4>
    <div style="margin: 20px;">
        <p>
            <? foreach($model->control_status_sums as $row) { ?>
                <a href="<?= url('controlbaseline/list',array('status'=>$row['control_status_id'])) ?>"><?= $row['control_status_name']; ?></a>:&nbsp;<b><?= $row['total']; ?></b>
                <br/>
            <? } ?>
            &raquo; &nbsp;<a href="<?= url('controlbaseline/list'); ?>">Control Status Details</a>
        </p>
    </div */ ?>
	<div style="margin: 20px;"></div>

    <div id="divControlSummaryChart">
        <img src="/images/ajax-loader.gif" style="margin: 250px 0 250px 100px;" />
    </div>

    <script type="text/javascript">
        var controlSummaryChart = new FusionCharts({
            type: 'pie3D',
            renderAt: 'divControlSummaryChart',
            width: '100%',
            height: '600',
            dataFormat: 'json',
            dataSource:  {
                "chart": {
                    "caption": "Completed <?= $model->total_complete; ?> of <?= $model->total_controls; ?> controls",
                    "subCaption": "",
                    "numberPrefix": "",
                    "showPercentInTooltip": "1",
                    "showLegend": "1",
                    "legendShadow": '1',
                    "decimals": "0",
                    "useDataPlotColorForLabels": "1",
                    "theme": "fint"
                },
                "data": <?= $model->control_summary_json; ?>
            }
        }).render();
    </script>
<? } ?>