<? $isAuthor = Utils::isRoleAuthor();
$isSuperUser = Utils::isSuper();?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'control_baseline_id', caption: 'ID', size: '50px', hidden: true, hideable: false, sortable: false, resizable: false },
        { field: 'id', caption: 'Id', size: '50px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'int' },
        { field: 'title', caption: 'Title', size: '50%', hidden: false, hideable: true, sortable: true, searchable: 'text', resizable: true },
        { field: 'category', caption: 'Category', size: '300px', searchable: 'text', hidden: false, hideable: true, resizable: true, sortable: true },
        { field: 'control_status_name', caption: 'Status', size: '90px', searchable: 'text', resizable: true, sortable: true },
        { field: 'priority', caption: 'Priority', size: '55px', hidden: true, hideable: true, sortable: true, searchable: 'int', resizable: true },
        { field: 'target_date', caption: 'Target', size: '85px', hidden: true, hideable: true, searchable: 'date', resizable: true, sortable: true },
        { field: 'label1', caption: 'Label 1', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'label2', caption: 'Label 2', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'reference', caption: 'Regulatory Reference', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'cat_id', caption: 'Category Id', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'int' },
        { field: 'control_id', caption: 'Control Id', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'int' },
        { field: 'detail', caption: 'Detail', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'guidance', caption: 'Guidance', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'jobrole', caption: 'Job Role', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'evidence', caption: 'Evidence', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'rel_policy', caption: 'Related Policy', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'question', caption: 'Question', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'score', caption: 'Score', size: '50px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'int' },
        { field: 'weighting', caption: 'Weighting', size: '50px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'int' },
        { field: 'role_title', caption: 'Security Role', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '135px' : '55px'; ?>', hidden: false, hideable: false, sortable: false, resizable: false,
            render: function (record, index, column_index) {
				<? if ($isAuthor) { ?>
					var html = '<div>'
						+'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
						+'<span>&nbsp;</span>'
						+'<a class="lnkEdit" href="<?= url('controlbaseline/edit'); ?>/id/'+record.recid+'">Edit</a>'
						+'<span>&nbsp;</span>'
						+'<a class="lnkCopy" href="" data-recid="'+record.recid+'">Copy</a>'
						+'<span>&nbsp;</span>'
						+'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
						+'</div>';
				<? } else { ?>
					var html = '<div>'
						+'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
						+'</div>';
				<? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = function(event) {
        w2popup.open({
            title     : 'Add New Control',
            body      : '<div class="w2ui-centered">Click on <strong>Create New</strong> to create a control from scratch.<br/>'+
						'<br/>Or click on <strong>Add from the Control Library</strong> to select a control from our library of controls, '+
						'then you may edit it from your baseline list to make further customizations.</div>',
            buttons   : '<button class="btn" onclick="w2popup.close();">Cancel</button> '+
						'<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('controlbaseline/edit'); ?>\';"><strong>Create New</strong></button>'+
						'<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('library/list'); ?>\';"><strong>Add from the Control Library</strong></button>',
            width     : 550,
            height    : 250,
            overflow  : 'hidden',
            opacity   : '0.2',
            modal     : true,
            showClose : true,
            showMax   : false
        });
        return false;
    };

</script>
<?
	/* @var $this ControlbaselineController */

	$gmAjaxListGrid = new gmAjaxListGrid();
	$gmAjaxListGrid->listUrl = $listUrl;
	$gmAjaxListGrid->title = '<h2>Control Baseline: <span id="divBaselineMenu"></span></h2>';
	$gmAjaxListGrid->intro = '<p>Establish a baseline of security functions (“controls”) that define your information security program. '
		.'Your next step is to evaluate each of these Controls and see how they apply to your business. ';
	if ($isAuthor) {
		$gmAjaxListGrid->intro .= 'View the <a href="'.url('library/list').'" style="color: blue;">Control Library</a> to add to your Control Baseline as desired. You can also run the Control Wizard to assist you in selecting Controls.</p>'
			.'<a href="'.url('wizard/control').'" class="btn btn-success" style="margin: 0 0 10px 0;">Run the Control Wizard</a>';
	}
	if (Utils::isRoleOwner() || Utils::isSuper()) {
		if($baseline['assessment_questions_answered'] == $baseline['control_count']){
			$text = 'Re-Take Assessment';
		}else{
			$text = 'Take Assessment';
		}
		$gmAjaxListGrid->intro .= '<a href="'.url('assessment/take/id/' . Obscure::encode($baseline['baseline_id'])).'" class="btn btn-primary" style="margin: 0 0 10px 10px;">' . $text . '</a>';
	}
	$gmAjaxListGrid->intro .= '</p>';
	$gmAjaxListGrid->controller = "controlbaseline";
	$gmAjaxListGrid->recid = "control_baseline_id";
	$gmAjaxListGrid->allowAdd = $isAuthor;
	$gmAjaxListGrid->allowEdit = $isAuthor;
	$gmAjaxListGrid->allowDelete = $isAuthor;
	$gmAjaxListGrid->allowCopy = $isAuthor;
	$gmAjaxListGrid->reloadOnCopy = true;
	$gmAjaxListGrid->toolbarColumns = true;
	$gmAjaxListGrid->limit = 3000;
	$gmAjaxListGrid->showExportCSV = true;
	$gmAjaxListGrid->stateSave = true;
	if (mPermission::checkLicenseRole(null,Cons::LIC_TRIAL, Cons::ROLE_OWNER, 'silent')) {
		$gmAjaxListGrid->showExportWORD = true;
	}
	if ($savedSearch) {
		$gmAjaxListGrid->searchData = $savedSearch->search;
	}
	AjaxListGridGadget::show($gmAjaxListGrid);
?>
<div id="divBaselineMenuTemp" style="display: none;">
	<select name="baseline_id" id="baseline_id">
		<? $baseline_id = Baseline_Ex::getCurrentBaselineId(); ?>
		<? $baselines = Baseline_Ex::GetBaselines(Utils::contextCompanyId()); ?>
		<? foreach ($baselines as $baseline) { ?>
			<option value="<?= Obscure::encode($baseline->baseline_id); ?>" <?= ($baseline_id==$baseline->baseline_id) ? 'selected="selected"' :'';?>><?= $baseline->baseline_name; ?></option>
		<? } ?>
	</select>
</div>
<script type="text/javascript">
    $(function () {
        var html = $('#divBaselineMenuTemp').html();
        $('#divBaselineMenu').html(html);
        $('#divBaselineMenuTemp').remove();

        $('#baseline_id').on('change', function() {
            var baselineId = $(this).val();
            if (baselineId == '<?= Obscure::encode(Baseline_Ex::getCurrentBaselineId()); ?>//') return false;
            var name = $('#baseline_id option:selected').text();
            w2confirm({
                msg         : 'Are you sure you want to change the current baseline context to the "'+name+'" baseline?',
                title       : 'Change Baseline Context',
                width       : 350,
                height      : 200,
                yes_text    : 'OK',
                yes_callBack: function(){
                    window.location = "<?= url('controlbaseline/changebaseline'); ?>/id/"+baselineId;
                    return false;
                },
                no_text     : 'Cancel',
                no_callBack : function(){
                    $('#baseline_id').val('<?= Obscure::encode(Baseline_Ex::getCurrentBaselineId()); ?>');
                    return false;
                }
            });
            return false;
        });
    });
</script>

