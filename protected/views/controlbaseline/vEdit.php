<?
/* @var $this ControlbaselineController */
/* @var $model ControlBaseline */
/* @var $form CActiveForm */
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>
<div class="form">

<? $form=$this->beginWidget('CActiveForm', array('action'=>url('controlbaseline/edit',array('id'=>Obscure::encode($model->control_baseline_id))),
	'id'=>'edit-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>
    <? $isNew = $model->isNewRecord; ?>
    <input type="hidden" name="filter_id" value="<?= $filter_id; ?>">
	<input type="hidden" name="root_id" value="<?= $root_id; ?>">
	<input type="hidden" name="status_id" value="<?= $status_id; ?>">

	<div class="row">
		<div class="col-md-6">
			<?= $form->labelEx($model,'control_id'); ?>
			<? if ($isNew) { ?>
				<?= $form->textField($model,'control_id'); ?>
			<? } else { ?>
				<p><?= $model->control_id; ?></p>
			<? } ?>
			<?= $form->error($model,'control_id'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<label for="ControlBaseline_cat_id" class="required">Category <span class="required">*</span></label>
			<select name="ControlBaseline[cat_id]" id="ControlBaseline_cat_id" style="width: 100%;">
				<? $cats = ControlCategory_Ex::getCategory(); ?>
				<? foreach ($cats as $cat) { ?>
					<option value="<?= $cat['control_category_id']; ?>" <?= ($model->cat_id == $cat['control_category_id']) ? 'selected="selected"' : ''; ?>><?= $cat['category']; ?></option>
				<? } ?>
			</select>
			<?= $form->error($model,'cat_id'); ?>
		</div>

		<?/* if ( Utils::isRoleOwner() && Utils::contextCompanyId() == mCyberRiskScore::CYBER_BASELINE_COMPANY_ID ) { ?>
			<div class="col-md-6">
				<?= $form->labelEx($model,'Category Override'); ?> (for Cyber Risk)
				<select name="ControlBaseline[cat_override]" id="ControlBaseline_cat_override" style="width: 100%;">
					<? $rootCats = ControlCategory_Ex::getRootCategories(); ?>
					<option value="" ></option>
					<? foreach ($rootCats as $rootCatId=>$rootCatDesc) { ?>
						<option value="<?= $rootCatId; ?>" <?= ($model->cat_override == $rootCatId) ? 'selected="selected"' : ''; ?>><?= $rootCatDesc; ?></option>
					<? } ?>
				</select>
				<?= $form->error($model,'cat_override'); ?>
			</div>
		<? } */?>
	</div>

    <? if ($isNew && false) { ?>
		<div class="row">
			<div class="col-md-12">
				<?= $form->labelEx($model,'cat_code'); ?>
				<?= $form->textField($model,'cat_code'); ?>
				<?= $form->error($model,'cat_code'); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<?= $form->labelEx($model,'cat_sub_code'); ?>
				<?= $form->textField($model,'cat_sub_code'); ?>
				<?= $form->error($model,'cat_sub_code'); ?>
			</div>
        </div>
    <? } ?>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'title'); ?>
			<?= $form->textField($model,'title',array('style'=>"width: 100%;")); ?>
			<?= $form->error($model,'title'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
        	<?= $form->labelEx($model,'control_status_id'); ?>
			<select name="ControlBaseline[control_status_id]" id="ControlBaseline_control_status_id">
				<? $controlstatuses = ControlStatus_Ex::getAllBySortOrder(); ?>
				<? foreach ($controlstatuses as $controlstatus) { ?>
					<option value="<?= $controlstatus['control_status_id']; ?>" <?= ($model->control_status_id==$controlstatus['control_status_id']) ? 'selected="selected"' :'';?>><?= $controlstatus['control_status_name']; ?></option>
				<? } ?>
			</select>
			<?= $form->error($model,'control_status_id'); ?>
		</div>
		<div class="col-md-4">
			<?= $form->labelEx($model,'target_date'); ?>
			<input name="ControlBaseline[target_date]" id="ControlBaseline_target_date" type="us-date" value="<?= $model->target_date; ?>">
			<?= $form->error($model,'target_date'); ?>
		</div>
	</div>


	<div class="row">
		<div class="col-md-4">
			<?= $form->labelEx($model,'priority'); ?>
			<select name="ControlBaseline[priority]" id="ControlBaseline_priority">
				<option value="1"  <?= ($model->priority=='1') ? 'selected="selected"' :'';?>>1</option>
				<option value="2"  <?= ($model->priority=='2') ? 'selected="selected"' :'';?>>2</option>
				<option value="3"  <?= ($model->priority=='3') ? 'selected="selected"' :'';?>>3</option>
			</select>
			<?= $form->error($model,'priority'); ?>
		</div>
		<div class="col-md-4">
			<?= $form->labelEx($model,'security_role_id'); ?>
			<select name="ControlBaseline[security_role_id]" id="ControlBaseline_security_role_id" style="width: 100%;">
				<option value="0" <?= (empty($model->security_role_id)) ? 'selected="selected"' :'';?>>unassigned</option>
				<? $roles = SecurityRole_Ex::getSecurityRole($count,Utils::contextCompanyId()); ?>
				<? if ($roles) { ?>
					<? foreach ($roles as $role) { ?>
						<option value="<?= $role['security_role_id']; ?>" <?= ($model->security_role_id==$role['security_role_id']) ? 'selected="selected"' :''; ?>><?= $role['role_title']; ?></option>
					<? } ?>
				<? } ?>
			</select>
			<?= $form->error($model,'security_role_id'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<?= $form->labelEx($model,'score'); ?>
			<?= $form->textField($model,'score',array('size'=>11,'maxlength'=>11)); ?>
			<?= $form->error($model,'score'); ?>
		</div>
		<div class="col-md-4">
			<?= $form->labelEx($model,'weighting'); ?>
			<?= $form->textField($model,'weighting',array('size'=>4,'maxlength'=>4)); ?>
			<?= $form->error($model,'weighting'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'label1'); ?>
			<?= $form->textField($model,'label1',array('style'=>"width: 99%;")); ?>
			<?= $form->error($model,'label1'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'label2'); ?>
			<?= $form->textField($model,'label2',array('style'=>"width: 99%;")); ?>
			<?= $form->error($model,'label2'); ?>
		</div>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'detail'); ?>
		<?= $form->textArea($model,'detail',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'detail'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'rel_policy'); ?>
		<?= $form->textArea($model,'rel_policy',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'rel_policy'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'guidance'); ?>
		<?= $form->textArea($model,'guidance',array('rows'=>'10','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'guidance'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'reference'); ?>
		<?= $form->textArea($model,'reference',array('rows'=>'10','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'reference'); ?>
	</div>

	<div class="row">
        <?= $form->labelEx($model,'question'); ?>
        <?= $form->textArea($model,'question',array('rows'=>'5','style'=>"width: 99%;")); ?>
        <?= $form->error($model,'question'); ?>
	</div>

	<div class="row">
        <?= $form->labelEx($model,'evidence'); ?>
        <?= $form->textArea($model,'evidence',array('rows'=>'5','style'=>"width: 99%;")); ?>
        <?= $form->error($model,'evidence'); ?>
        <? if ($model->evidences) { ?>
			<br>
			<label>Existing Evidence(s):</label>
			<? foreach($model->evidences as $evidence) { ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?= url('evidence/viewevidence/id/'.obscure($evidence->evidence_id)); ?>"><?= $evidence->doc_title; ?> (<?= $evidence->file_name; ?>)</a>
				<br>
            <? } ?>
		<? } ?>
	</div>
	<div class="row"></div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<? if (!$filter_id) { ?>
			<?= (!$isNew && $nextControlBaselineId) ? CHtml::button('Save and Next',array('id'=>"btnSaveAndNext",'class'=>"btn btn-info")) : ''; ?>
		<? } ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
		<? if (!$isNew) { ?>
			<?= CHtml::button('Add Evidence',array('id'=>"btnAddEvidence",'class'=>"btn btn-success")); ?>
		<? } ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
$(function(){
	$('input[type=us-date]').w2field('date');

	$("#btnSave").click( function() {
		$('#edit-form').submit();
		return false;
	});

	$("#btnSaveAndNext").click( function() {
		var $input = $('<input>').attr({
			type: 'hidden',
			name: 'SaveAndNext'
		}).val(1);
		$('#edit-form').append($input);
		$('#edit-form').submit();
		return false;
	});

	$("#btnCancel").click( function() {
	    <? if ($filter_id) { ?>
        	window.history.back();; //"<?= url('controlbaseline/list'); ?>";
		<? } else { ?>
			window.location = "<?= url('controlbaseline/list'); ?>";
        <? } ?>
        return false;
	});

	$("#btnAddEvidence").click( function() {
		window.open("<?= url('evidence/edit',array('cid'=>Obscure::encode($model->control_baseline_id))); ?>",'_blank');
		return false;
	});
});
</script>