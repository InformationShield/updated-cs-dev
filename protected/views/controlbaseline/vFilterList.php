<? $isAuthor = Utils::isRoleAuthor();
$isSuperUser = Utils::isSuper();?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'control_baseline_id', caption: 'ID', size: '50px', hidden: true, hideable: false, sortable: false, resizable: false },
        { field: 'id', caption: 'Id', size: '30px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'int' },
        { field: 'title', caption: 'Title', size: '50%', hideable: false, sortable: true, searchable: 'text', resizable: true },
        { field: 'category', caption: 'Category', size: '300px', searchable: 'text', resizable: true, sortable: true },
        { field: 'control_status_name', caption: 'Status', size: '90px', searchable: 'text', resizable: true, sortable: true },
        { field: 'doc_title', caption: 'Evidence', size: '250px', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                var lnkClass = 'lnkDownload';
                if (record.file_type == null) {
                    return '';
                } else if (record.file_type == 'application/pdf') {
                    lnkClass = 'lnkGo';
                } else if (record.file_type.substr(0,5) == 'text/') {
                    lnkClass = 'lnkGo';
                }  else if (record.file_type.substr(0,6) == 'image/') {
                    lnkClass = 'lnkGo';
                }
                return '<div><a class="'+lnkClass+'" href="<?= url('evidence/viewevidence'); ?>/id/' + record.evidence_id + '">'+record.doc_title+'</a></div>';
            }
		},
        { field: 'priority', caption: 'Priority', size: '55px', hidden: true, hideable: true, sortable: true, searchable: 'int', resizable: true },
        { field: 'target_date', caption: 'Target', size: '85px', hidden: true, hideable: true, searchable: 'date', resizable: true, sortable: true },
        { field: 'label1', caption: 'Label 1', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'label2', caption: 'Label 2', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'reference', caption: 'Regulatory Reference', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'cat_id', caption: 'Category', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'int' },
        { field: 'control_id', caption: 'Control Id', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'int' },
        { field: 'detail', caption: 'Detail', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'guidance', caption: 'Guidance', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'jobrole', caption: 'Job Role', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'evidence', caption: 'Evidence', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'rel_policy', caption: 'Related Policy', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'question', caption: 'Question', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'score', caption: 'Score', size: '50px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'int' },
        { field: 'weighting', caption: 'Weighting', size: '50px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'int' },
        { field: 'role_title', caption: 'Security Role', size: '100px', hidden: true, hideable: true, sortable: true, resizable: true, searchable: 'text' },
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '135px' : '55px'; ?>', hideable: false, sortable: false, resizable: false,
            render: function (record, index, column_index) {
                    <? if ($isAuthor) { ?>
                        var html = '<div>'
                            +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                            +'<span>&nbsp;</span>'
                            +'<a class="lnkEdit" href="<?= url('controlbaseline/edit'); ?>/id/'+record.recid+'/fid/<?= $controlFilter['id']; ?>'+'/status/<?= $status_id; ?>'+'/rid/<?= $root_id; ?>'+'">Edit</a>'
                            +'<span>&nbsp;</span>'
                    <? } else { ?>
                        var html = '<div>'
                            +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                            +'</div>';
                    <? } ?>
                return html;
            }
        }
    ];

</script>
<?
/* @var $this ControlbaselineController */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = $listUrl;
    $gmAjaxListGrid->title ='<h2><strong>'.$controlFilter['filter_name'].'</strong> Report Filter Controls';
	if (!is_null($status_id) || $root_id) {
		$gmAjaxListGrid->title .= ' (partial list)';
	}
//	if (!is_null($status_id)) {
//		$gmAjaxListGrid->title .= ' with <strong>'.ControlStatus_Ex::getStatusName($status_id).'</strong> status.';
//	}
	$gmAjaxListGrid->title .= '</h2>';
    $gmAjaxListGrid->intro = '';
    $gmAjaxListGrid->intro .= '</p>';
    $gmAjaxListGrid->controller = "controlbaseline";
    $gmAjaxListGrid->recid = "control_baseline_id";
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->toolbarColumns = true;
    $gmAjaxListGrid->limit = 3000;
    $gmAjaxListGrid->showExportCSV = true;
    if (mPermission::checkLicenseRole(null,Cons::LIC_TRIAL, Cons::ROLE_OWNER, 'silent')) {
        $gmAjaxListGrid->showExportWORD = true;
    }
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
<div id="divBaselineMenuTemp" style="display: none;">
    <select name="baseline_id" id="baseline_id">
        <? $baseline_id = Baseline_Ex::getCurrentBaselineId(); ?>
        <? $baselines = Baseline_Ex::GetBaselines(Utils::contextCompanyId()); ?>
        <? foreach ($baselines as $baseline) { ?>
            <option value="<?= Obscure::encode($baseline->baseline_id); ?>" <?= ($baseline_id==$baseline->baseline_id) ? 'selected="selected"' :'';?>><?= $baseline->baseline_name; ?></option>
        <? } ?>
    </select>
</div>
<script type="text/javascript">
    $(function () {
        var html = $('#divBaselineMenuTemp').html();
        $('#divBaselineMenu').html(html);
        $('#divBaselineMenuTemp').remove();

        $('#baseline_id').on('change', function() {
            var baselineId = $(this).val();
            if (baselineId == '<?= Obscure::encode(Baseline_Ex::getCurrentBaselineId()); ?>') return false;
            var name = $('#baseline_id option:selected').text();
            w2confirm({
                msg         : 'Are you sure you want to change the current baseline context to the "'+name+'" baseline?',
                title       : 'Change Baseline Context',
                width       : 350,
                height      : 200,
                yes_text    : 'OK',
                yes_callBack: function(){
                    window.location = "<?= url('controlbaseline/changebaseline'); ?>/id/"+baselineId;
                    return false;
                },
                no_text     : 'Cancel',
                no_callBack : function(){
                    $('#baseline_id').val('<?= Obscure::encode(Baseline_Ex::getCurrentBaselineId()); ?>');
                    return false;
                }
            });
            return false;
        });
    });
</script>