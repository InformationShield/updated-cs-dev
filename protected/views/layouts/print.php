<!DOCTYPE html>
<html lang="en">
<head>
    <? require_once(Yii::app()->basePath.'/appinclude/Helper.php'); ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<?
		require_once(__DIR__.'/_version.php');
		require_once(__DIR__.'/_css_common.php');
		require_once(__DIR__.'/_js_common.php');
	?>
    <link rel="stylesheet" type="text/css" href="/css/print.css?v=1.03" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
	<div class="" id="page">
        <?php echo $content; ?>
        <div class="clear"></div>
	</div><!-- page -->
</body>
</html>
