<? require_once(Yii::app()->basePath.'/appinclude/Helper.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<?
		require_once(__DIR__.'/_version.php');
		require_once(__DIR__.'/_css_common.php');
		require_once(__DIR__.'/_js_common.php'); 
	?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
	<div class="" id="page">
        <header id="uHeader">
            <hgroup>
                <a href="/" id="uLogo"><img
                        src="/images/<?= BRAND_COMPANY_LOGO; ?>"></a>

                <div class="userBlock">
                    <img src="/old/images/f_spacer.gif" class="navIcon user" alt="">
                    <span><?= userLogin(); ?></span>
                    <a href="<?= url('home/Profile'); ?>">My Profile</a>
                    <a id="lnkFeedback" href="#">Feedback</a>
                    <a href="<?= url('home/logout'); ?>">Logout</a>
                </div>

            </hgroup>
        </header>
        <div class="modal fade" id="dlgFeedback" tabindex="-1" role="dialog" aria-labelledby="dlFeedback" aria-hidden="true"></div>

        <?php echo $content; ?>
        
	</div><!-- page -->
    <? require_once(__DIR__.'/_footer.php'); ?>
</body>
</html>

<script type="text/javascript">
$(document).ready(function(){
    $(document).on('click', '#lnkFeedback', function() {
        $.ajax({
            type: "post",
            url: "<?= url('home/Feedback') ?>",
            data: {
            },
            success: function(results, textStatus, xhr) {
                $('#dlgFeedback').html(results);
                $('#dlgFeedback').modal()
            },
            error: function(xhr) {
                errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ': ' + (xhr.responseText);
                console.debug(errorResult);
            }
        });
        return false;
    });

});
</script>


