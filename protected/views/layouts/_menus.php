<? /*
    *  Builds the side menu and the top menu bars from a defined $menus array created in the _header.php _admin_header.php file
    */
	$menus = mMenus::getMenus();
?>
<!-- left menu panel -->
<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
		<div class="navbar nav_title">
			<a href="<?= CHtml::normalizeUrl($menus['title_url']); ?>">
				<img width="230" src="<?= $menus['logo']; ?>" class="logo" />
				<img width="70" src="<?= $menus['logo_sm']; ?>" class="logosm"/>
			</a>
		</div>
		<div class="clearfix"></div>
		<br />
		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3><?= $menus['sub_title']; ?></h3>
				<ul class="nav side-menu">
					<? foreach ($menus['side_menu'] as $menuName=>$menu) { ?>
						<? if (isset($menu['hide']) && $menu['hide']) continue; ?>
						<li class="<?= $menu['active'] ? null : "noaccess"; ?>">
							<? $link = $menu['url'] ? 'href="'.absoluteUrl($menu['url']).'"' : null; ?>
							<a <?= $link; ?> >
								<i class="fa <?= $menu['icon']; ?>"></i>
								<?= $menuName; ?>
								<? if ($menu['children']) { ?>
									<span class="fa fa-chevron-down"></span>
								<? } ?>
							</a>
							<? if ($menu['children']) { ?>
								<ul class="nav child_menu" style="display: none">
									<? foreach ($menu['children'] as $subMenuName=>$subMenu) { ?>
										<? if (isset($subMenu['hide']) && $subMenu['hide']) continue; ?>
										<li class="<?= $subMenu['active'] ? null : "noaccess"; ?>">
											<a href="<?= absoluteUrl($subMenu['url']); ?>"><?= $subMenuName; ?></a>
										</li>
									<? } ?>
								</ul>
							<? } ?>
						</li?>
					<? } ?>
				</ul>
			</div>

		</div>
		<!-- /sidebar menu -->
	</div>
</div>

<!-- top navigation -->
<div class="top_nav">
	<div class="nav_menu" style="background:<?= HEADER_BAR_COLOR; ?>;">
		<nav class="" role="navigation">
			<div class="nav toggle">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>
			</div>
			<? if ($menus['company_logo']) { ?>
				<div class="nav toggle" style="padding: 0; width: auto; max-width: 300px; overflow: hidden;">
					<img height='58' src='<?= url("/files/logos/" . $menus['company_logo']); ?>'>
				</div>
			<? } ?>
			<ul class="nav navbar-nav navbar-right">
				<? if (isset($menus['top_menu']) && count($menus['top_menu']) > 0) { ?>
					<? foreach($menus['top_menu'] as $menuName=>$menu) { ?>
						<? if (isset($menu['active']) && $menu['active']) { ?>
							<li>
								<? if (isset($menu['children'])) { ?>
									<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<?= $menuName; ?> (<?= userLogin(); ?>)
										<span class=" fa fa-angle-down"></span>
									</a>
									<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
										<? foreach($menu['children'] as $subMenuName=>$subMenu) { ?>
											<? if (isset($menu['active']) && !$menu['active']) continue; ?>
											<? if ($subMenuName !== '---') { ?>
												<li>
													<a href="<?= absoluteUrl($subMenu['url']); ?>">
														<?= $subMenuName; ?>
													</a>
												</li>
											<? } else { ?>
												<li class="divider"></li>
											<? } ?>
										<? } ?>
									</ul>
								<? } else { ?>
									<a href="<?= absoluteUrl($menu['url']); ?>"><?= $menuName; ?></a>
								<? } ?>
							</li>
						<? } ?>
					<? } ?>
				<? } ?>
				<li id="liMessageMenu" role="presentation" class="dropdown"></li>
				<? if (getSessionVar('license_level') == Cons::LIC_TRIAL) { ?>
					<li id="liTrial" role="presentation" class="dropdown"><a href="#" class="trial-msg">Your trial expires in <?= mPermission::checkTrialLicense($this); ?> day(s).</a></li>
				<? } else if (getSessionVar('license_level') == Cons::LIC_FREE) { ?>
					<li id="liTrial" role="presentation" class="dropdown"><a href="#" class="trial-msg-expired">Your trial period has expired.</a></li>
				<? } ?>
			</ul>
		</nav>
	</div>
</div>
<script type="text/javascript">
	var _ajaxLoadMessageMenu = function() {
		$.ajax({
			method: 'POST',
			dataType: "html",
			url: '<?= url('message/messagemenu'); ?>',
			success: function(response){
				$('#liMessageMenu').html(response);
			}
		});
	};
	$(function () {
		_ajaxLoadMessageMenu();

		$('.trial-msg, .trial-msg-expired').click(function() {
			w2alert('Please <a href="<?= url("site/contact"); ?>">contact</a> ComplianceShield to upgrade your license.');
			return false;
		});
	});
</script>
<!-- /top navigation -->