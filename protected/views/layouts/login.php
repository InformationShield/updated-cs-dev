<? require_once(Yii::app()->basePath.'/appinclude/Helper.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <?
		require_once(__DIR__.'/_version.php');
        require_once(__DIR__.'/_css_common.php');
        require_once(__DIR__.'/_js_common.php');
    ?>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body id="uLogin">
    <div id="page">
        <?php echo $content; ?>
    </div><!-- page -->
</body>
</html>
