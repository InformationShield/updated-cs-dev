<?php /* @var $this Controller */ ?>
<? require_once('protected/appinclude/Helper.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="Information security compliance made easy." />
        <meta name="keywords" content="information,security,compliance,shield,it,controls,risk,assessment,maturity,model,evidence" />
        <meta name="author" content="Compliance Shield" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
<!--		<link rel="manifest" href="/icons/manifest.json">-->
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<?
			require_once(__DIR__.'/_version.php');
			require_once(__DIR__.'/_css_common.php');
			require_once(__DIR__.'/_js_common.php');
        ?>
        <title><?= Yii::app()->name; ?></title>
    </head>

    <body class="nav-md">
        <? require_once(Yii::app()->basePath.'/views/layouts/_dlgGeneric.php'); ?>
        <div class="container body">
            <div class="main_container">

                <!-- header starts here -->
                <? require_once(__DIR__.'/_admin_header.php'); ?>
                <? require_once(__DIR__.'/_menus.php'); ?>
                <!-- header ends here -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <!-- content starts here -->
                            <?= $content; ?>
                            <!-- content ends here -->

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->

                <!-- footer starts here -->
                <? require_once(__DIR__.'/_footer.php'); ?>
                <!-- footer ends here -->
            </div>
        </div>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/3rdparty/icheck/icheck.min.js"></script>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/custom.js"></script>
    </body>
</html>