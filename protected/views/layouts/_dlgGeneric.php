<style>
    #dlgGeneric .modal-body label {
        font-size: 16px;
        margin: 10px 0 0 7px;
    }
</style>

<div class="modal fade" id="dlgGeneric">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"><label></label></div>
            <div id="ftrQuestion" class="modal-footer">
                <button id="button1" type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                <button id="button2" type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
            <div id="ftrMessage" class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var dlgGenericCallback;

function showDlgGeneric(title, message, button1Text, button2Text, fnCallback) {
    title !== undefined ? $('#dlgGeneric .modal-title').html(title) : $.noop;
    message !== undefined ? $('#dlgGeneric .modal-body label').html(message) : $.noop;
    button1Text !== undefined ? $('#dlgGeneric #button1').html(button1Text) : $.noop;
    button2Text !== undefined ? $('#dlgGeneric .#button2').html(button2Text) : $.noop;
    fnCallback !== undefined ? dlgGenericCallback = fnCallback : dlgGenericCallback = undefined;

    if (button1Text === undefined && button2Text === undefined) { // assume just a msg box
        $('#ftrMessage').show();
        $('#ftrQuestion').hide();
    } else {
        $('#ftrQuestion').show();
        $('#ftrMessage').hide();
    }
    $('#dlgGeneric').modal();
}

$('#dlgGeneric #button1').click(function() {
    if (dlgGenericCallback !== undefined) {
        dlgGenericCallback();
    }
});

function msgBox(message, title, buttonText) {
    if (title === undefined) {
        title = '<?= CHtml::encode($this->pageTitle); ?>';
    }
    showDlgGeneric(title, message);
}

</script>