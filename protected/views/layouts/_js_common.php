<? Yii::app()->clientScript->scriptMap=array('jquery.js'=>false); // tell yii we are using our own jquery library ?>
<script type="text/javascript">
    var BASEURL = <?= CJSON::encode(Yii::app()->baseUrl); ?>;
</script>

<? /*
<!--[if lt IE 9]>
<script type="text/javascript" src="<?= url("/3rdparty/html5shiv-master/dist/html5shiv.min.js"); ?>"></script>
<![endif]-->
 */ ?>
<script type="text/javascript" src="<?= url("3rdparty/jquery-2.1.3/jquery-2.1.3.min.js"); ?>"></script>
<!--<script type="text/javascript" src="<?= url("js/jquery/jquery.widget.min.js"); ?>">-->
<script type="text/javascript" src="<?= url("3rdparty/jquery-ui-1.11.4.cupertino/jquery-ui.min.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/jquery.blockUI/jquery.blockUI.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/momentjs/moment.min.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fullcalendar-2.3.1/fullcalendar.min.js"); ?>"></script>

<? // UI Framework controls added ?>
<script type="text/javascript" src="<?= url("3rdparty/nicescroll/jquery.nicescroll.min.js"); ?>"></script>

<script type="text/javascript" src="<?= url("js/common.js"); ?>"></script>
<script type="text/javascript" src="<?= url("js/w2ui-1.4.3.min.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/bootstrap-3.3.4-dist/js/bootstrap.min.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/notify/pnotify.core.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/notify/pnotify.nonblock.js"); ?>"></script>




<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->