<div id="footer">
        <div>
            <center>
                <? /*
                <?= CHtml::link('About',array('/site/page', 'view'=>'about')); ?>
                <span>&nbsp;|&nbsp;</span>
                */?>
                <?= CHtml::link('Contact',array('/site/contact')); ?>
                <span>&nbsp;|&nbsp;</span>
                <?= CHtml::link('Privacy Policy',array('/site/page', 'view'=>'privacypolicy')); ?>
            </center>
        </div>
        <div>
            <center>Copyright &copy; 2014-<?= date('Y'); ?> by <?= Yii::app()->params['companyName']; ?> All Rights Reserved.</center>
        </div>
</div>
