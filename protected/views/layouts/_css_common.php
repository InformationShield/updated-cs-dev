<!-- blueprint CSS framework -->
<link rel="stylesheet" type="text/css" href="<?= url("css/screen.css");?>" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="<?= url("css/print.css");?>" media="print" />
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="<?= url("css/ie.css");?>" media="screen, projection" />
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?= url("3rdparty/font-awesome-4.7.0/css/font-awesome.min.css");?>" />
<link rel="stylesheet" type="text/css" href="<?= url("3rdparty/jquery-ui-1.11.4.cupertino/jquery-ui.min.css");?>" />
<link rel="stylesheet" type="text/css" href="<?= url("3rdparty/fullcalendar-2.3.1/fullcalendar.min.css");?>" />

<link rel="stylesheet" type="text/css" href="<?= url("css/w2ui-1.4.3.min.css");?>" />
<link rel="stylesheet" type="text/css" href="<?= url("css/main.css");?>" />
<link rel="stylesheet" type="text/css" href="<?= url("css/form.css");?>" />

<!-- Bootstrap core CSS -->

<link rel="stylesheet" type="text/css" href="<?= url("3rdparty/bootstrap-3.3.4-dist/css/bootstrap.min.css");?>" />
<link rel="stylesheet" type="text/css" href="<?= url("css/animate.min.css");?>" />
<link rel="stylesheet" type="text/css" href="<?= url("css/icheck/flat/green.css");?>" />

<!-- Theme styling -->
<link rel="stylesheet" type="text/css" href="<?= url("css/custom.css");?>?v=<?= VERSION; ?>" />
<link rel="stylesheet" type="text/css" href="<?= url("css/styles.css");?>?v=<?= VERSION; ?>" />