<h2>Policy Compliance Detail Report for <?= $policy['doc_title']; ?></h2>
<div class="left" style="margin: 0 0 10px 0;">
    <p>
        <?= $policy['description']; ?>
    </p>
</div>
<div class="right" style="margin: 0 0 10px 0;">
    <a href="" id="lnkExport" class="btn btn-success" >Export to CSV</a>
    <a href="<?= $printUrl; ?>" target="_blank" class="btn btn-success">Print</a>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'firstname', caption: 'User Name', size: '100%',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                return '<div>' + record.firstname + ' ' + record.lastname + '</div>';
            }
        },
        { field: 'department_name', caption: 'Department', size: '130px',  resizable: true, sortable: false },
        { field: 'status_name', caption: 'Status', size: '100px', hidden: false,  resizable: true, sortable: false },
        { field: 'ack_date', caption: 'Acknowledged On', size: '160px', hidden: false,  resizable: true, sortable: false },
        { field: 'view_date', caption: 'Viewed On', size: '160px', hidden: false,  resizable: true, sortable: false }
    ];
    var _ajaxListGridAddFunction = false;
</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = $dataUrl;
    $gmAjaxListGrid->title = '';
    $gmAjaxListGrid->intro = '';
    $gmAjaxListGrid->controller = "reportpolicy";
    $gmAjaxListGrid->recid = "id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = false;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    $gmAjaxListGrid->showToolbar = false;
    $gmAjaxListGrid->showSearch = false;
    $gmAjaxListGrid->showFooter = false;
    $gmAjaxListGrid->fixedBody = false;
    $gmAjaxListGrid->fixedHeight = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>

<script type="text/javascript">
    $(function(){

        $(document).on('click', '#lnkExport', function () {
            new PNotify({
                title: 'Downloading...',
                text: 'Your file is downloading now.',
                delay: 3500,
                type: 'info'
            });
            window.location = '<?= $exportUrl; ?>';
            return false;
        });

    });
</script>
