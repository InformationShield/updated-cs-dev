<h2>Policy Compliance Detail Report</h2>
<h3><?= $policy['doc_title']; ?></h3>
<p>
    <?= $policy['description']; ?>
</p>
<table style="width: 100%; border-spacing: 10px;">
    <? foreach ($data as $key=>$row) { ?>
        <? if ($key == 0) { ?>
            <thead align="left">
                <? foreach($row as $field) { ?>
                    <th><?= $field; ?></th>
                <? } ?>
            </thead>
            <tbody>
        <? } else { ?>
            <tr>
                <? foreach($row as $field) { ?>
                    <td><?= $field; ?></td>
                <? } ?>
            </tr>
        <? } ?>
    <? } ?>
    </tbody>
</table>
