<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>
<div class="row x_color">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_content text-center">
						<h3 style="color: #73879C;">Cyber Risk Score™ Maturity Report</h3>
						<div style="font-size:15px; font-weight: 400;"><strong>Organization:</strong> <?= Utils::contextCompanyName(); ?></div>
					</div>
				</div>
                
                <div class="gauge-risk-score col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_content">
						<div id="assessmentCyberRiskReportChart" style="height:370px;"></div>
						<div class="text-center" style="font-size:16px;">
							<p style="display:inline; margin: 0px 20px;"><strong>Completed</strong>: <?= $cyberRiskScore['assessed_on']; ?></p>
							<p>
								<strong>Score: </strong><?= $cyberRiskScore['total_score']; ?>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<strong>
									Maturity Level:
									<? $percentage = $cyberRiskScore['total_percentage']; ?>
									<? $maturity_level = ''; ?>
									<? if($percentage > 90) { ?>
										<? $maturity_level = 'EXTREMELY_HIGH'; ?>
										<span style="color: #006600;">Extremely High</span>
									<? } else if($percentage >= 70) { ?>
										<? $maturity_level = 'HIGH'; ?>
										<span style="color: #CEF357;">High</span>
									<? } else if($percentage >= 30) { ?>
										<? $maturity_level = 'MODERATE'; ?>
										<span style="color: #FFE65B;">Moderate</span>
									<? } else { ?>
										<? $maturity_level = 'LOW'; ?>
										<span style="color: #FF5F5B;">Low</span>
									<? } ?>
								</strong>
							</p>
						</div>
					</div>
                </div>
				<div class="comparison-risk-score col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_content">
						<div id="divCyberRiskComparisonChart" style="height:270px;"></div>
					</div>
				</div>

				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">&nbsp;</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">&nbsp;</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_title"><h4 style="color:#445468;">Cyber Maturity Risk Summary</div>
					<div class="x_content">
						<p><strong>
							Your organizations total risk score was <?= $cyberRiskScore['total_score']; ?> out of a possible 1000.
							Your overall cyber risk maturity is classified as <?= $maturity_level; ?>.
							<? /* Your score is better than 65% of the overall Cyber Risk Scores across all industries. */ ?>
						</strong></p>
						<? if ($cyberRiskScore['total_percentage'] > 90) { ?>
							<p>
								Organizations in this category have implemented most or all of the information security
								controls in each key domain and had these control validated by an independent third party.
								Organizations at this level will be generally the most resistant to cyber-attacks and be in a good
								position to respond and recover if an attack does occur.
							</p>
						<? } else if ($cyberRiskScore['total_percentage'] >= 70) { ?>
							<p>
								Organizations in this category have implemented most or all of the information security controls
								in each key domain, however these have probably not been validated by internal audits or external
								third parties.   These organization will be very resistant to cyber attacks and well prepared to
								respond quickly in the event of a successful attack.   Organizations in the HIGH program maturity
								have, at a minimum, implemented all of the CPL Essential Security controls.
							</p>
						<? } else if ($cyberRiskScore['total_percentage'] >= 30) { ?>
							<p>
								Organizations at this level typically have some gaps in key areas of their information security
								program compared with both their peers and industry best-practices.  Organizations at this level
								are somewhat vulnerable to cyber-attacks, and generally less prepared to respond in the event of
								a successful attack.   Organizations that have HIGH inherent risk and still in the Moderate
								category of program maturity should take immediate action to improve their program.  Organizations
								at this level can improve their score by implementing and testing more of the controls with the
								key information security areas.
							</p>
						<? } else { ?>
							<p>
								Organizations at this level typically do not have any type of formal security program in
								place or are operating without a documented plan.   Organizations at this level have
								implemented very few of the key information security controls and have significant gaps
								in their cyber defenses.   Organizations at this level are extremely vulnerable to a
								successful cyber-attack and are generally not equipped to recover from such events.
								Organizations at this level can improve their score dramatically by implementing and
								testing some of the essential controls with the key information security areas.
							</p>
						<? } ?>
					</div>
				</div>

				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_title">
						<h4 style="color:#445468;">Understanding the CPL Cyber Maturity Report</h4>
					</div>
					<div class="x_content">
						<p>
							<strong>Scoring:</strong> The CPL Cyber Risk Report is scored based on the maturity of your cyber
							security program across key areas.   The general idea is that the more security controls you have
							implemented and maintain on a regular basis, the less likely your organization will be subject to
							a successful attack.  So a <span style="color: green"> higher score implies a more secure
							environment</span>, while a <span style="color: red"> lower score implies	a less secure
							environment</span>.
						</p>
						<p>
							For each of the key security areas, the CPL Program Maturity asks relevant questions to determine
							the level of progress your organization has made implemented key security elements.   To determine
							the score, each question is worth a certain number of points, and these points are then scored
							according the maturity of your program.  For example, if your organization has not implemented a
							certain control (for example, Access Control to Systems), then you would have the lowest score.
							If your organization has implemented this same control, your score would be higher.   If your
							organization has had the specific control implemented and validated by an outside party, then you
							would have the highest possible score for that question.
						</p>
					</div>
				</div>

				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_content">
						<table class="table table-hover">
							<thead>
							<tr>
								<th>Cyber Maturity Risk Category</th>
								<th>Score</th>
								<th>Max Score</th>
								<th>Percentage</th>
							</tr>
							</thead>
							<tbody>
							<? foreach ($cyberRiskScore['category_scores'] as $i=>$categoryScore) { ?>
								<tr>
									<td><?= $categoryScore['category']; ?></td>
									<td><?= $categoryScore['score']; ?></td>
									<td><?= $categoryScore['max_score']; ?></td>
									<td><?= $categoryScore['percentage']; ?> %</td>
								</tr>
							<? } ?>
							</tbody>
							<tfoot>
							<tr>
								<td>TOTAL</td>
								<td class="assessment-total-score"><?= $cyberRiskScore['total_score']; ?></td>
								<td class="assessment-max-score"><?= $cyberRiskScore['total_max']; ?></td>
								<td><?= $cyberRiskScore['total_percentage']; ?> %</td>
							</tr>
							</tfoot>
						</table>
					</div>
				</div>

				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
                    <div class="x_panel">
                        <div class="x_content text-center">                            
                            <a href="<?= url('cyberriskscore/Newassessment'); ?>" class="btn btn-primary">Re-Take Assessment</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<script src="<?= url("3rdparty/echart/echarts-all.js"); ?>"></script>
<script src="<?= url("3rdparty/echart/green.js"); ?>"></script>
<script>
$(document).ready(function () {
	if ($('#assessmentCyberRiskReportChart').length > 0) {
		var maxScore = parseInt($('td.assessment-max-score').text());
		var totalScore = parseInt($('td.assessment-total-score').text());

		var assessmentReportOptions = {
			animation: false,

			tooltip : {
				show: false
			},

			toolbox: {
				show : false
			},

			series : [
				{
					name:'the chart',
					type:'gauge',
					clickable: false,
					center : ['50%', '80%'],
					radius : [0, '100%'],
					startAngle: 180,
					endAngle: 0,
					min: 0,
					max: maxScore,
					splitNumber: maxScore,

					axisLine: {
						show: true,
						lineStyle: {
							color: [
								[0.30, '#FF5F5B'],
								[0.70, '#FFE65B'],
								[0.9, '#CEF357'],
								[1.00, '#7DDD4F']
							],
							width: 20
						}
					},

					axisTick: {
						show: true,
						splitNumber: 1,
						length :0,
					},

					axisLabel : {
						show: true,
						margin: 0,
						formatter: function(v){
							var low_label_position = Math.floor(maxScore * 0.1);
							var medium_label_position = Math.floor(maxScore * 0.5);
							var high_label_position = Math.floor(maxScore * 0.9);

							if(v == 0){
								return 0;
							}

							if(v == maxScore){
								return maxScore;
							}

							if((v % 100) == 0){
								return v;
							}

							/*switch (v){
								case low_label_position: return 'LOW';
								case medium_label_position: return 'MEDIUM';
								case high_label_position: return 'HIGH';
								default: return '';
							}*/
						},
						 textStyle: {
							color: '#000',
							fontSize: 13,
							fontWeight: 'bold'
						}
					},

					splitLine: {
						show: false
					},

					pointer: {
						length : '80%',
						width : 8,
						color : 'auto'
					},

					title: {
						show : false,
					},

					detail: {
						show: true,
						offsetCenter: ['0%', '10%'],
						textStyle: {
							color: '#000',
							align: 'left',
							fontSize: 17
						},
						formatter: function(value){
							//return '\n\n' + value + ' <b>out of</b> ' + maxScore + ' points';
						}
					},

					legendHoverLink: false,

					data:[
						{value: totalScore}
					]
				}
			]
		};

		var assessmentReportChart = echarts.init(document.getElementById('assessmentCyberRiskReportChart'), null);
		assessmentReportChart.setOption(assessmentReportOptions,true);
		$(window).on('resize', function(){
			assessmentReportChart.resize();
		});

	}

	var cyberRiskComparisonChart = new FusionCharts({
		"type": "bar2D",
		"renderAt": "divCyberRiskComparisonChart",
		"width": "100%",
		"height": "270",
		"dataFormat": "json",
		"dataSource": {
			"chart": {
				"caption": "Cyber Maturity Risk Comparison",
				"subCaption": "",
				"yAxisMaxValue": "100",
				"numberPrefix": "",
				"theme": "fint"
			},
			"data": <?= json_encode($cyberRiskScore['cyber_risk_score_comparison']); ?>
		}
	}).render();

});
</script>