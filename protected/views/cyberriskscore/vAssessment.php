<style>
    .label-spacing{margin:5px 5px;}
</style>
<div class="row">
    <h3>Cyber Risk Score</h3>
</div>

<?php echo CHtml::beginForm(url('cyberriskscore/update'), 'post', array('id' => 'assessment-form')); ?>
<?php echo Chtml::hiddenField('save', '1'); ?>

<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h4>Organization Size and Score</h4>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>1. What is your approximate annual revenue?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_1', ($answers && $answers->question1) ? $answers->question1 : 0, array(
                    0 => '< $5 Million',
                    1 => '$5 -$50 Million ',
                    2 => '$50 - $500 Million ',
                    3 => '$500M - $1 Billion ',
                    4 => '$1+ Billion ',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>2. How many business locations do you have that process information?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_2', ($answers && $answers->question2) ? $answers->question2 : 0, array(
                    0 => 'None (Virtual)',
                    1 => 'Single location',
                    2 => 'Small (2 -10 locations)',
                    3 => 'Medium (10 - 100 locations)',
                    4 => 'Large (> 100 locations)',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>3. How many Employees and Contactors have access to sensitive information?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_3', ($answers && $answers->question3) ? $answers->question3 : 0, array(
                    0 => 'None ',
                    1 => 'Minimal (1 - 50) ',
                    2 => 'Moderate (50 - 500) ',
                    3 => 'Large (500 - 5000)',
                    4 => 'Very Large (5000+)',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h4>Data Types and Volume</h4>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>4. Does your organization handle information of your customers that they would consider highly sensitive? If yes, how many types of data?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_4', ($answers && $answers->question4) ? $answers->question4 : 0, array(
                    0 => 'No - We do not process sensitive data',
                    1 => 'Yes - Single Data Types (Non PII)',
                    2 => 'Yes - Multiple Data Types (Non PII)',
                    3 => 'Yes - PII Data - Single Type',
                    4 => 'Yes - PII Data - Multiple Types',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>

        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>5. Approximately how many records does your organization handle that would be considered "sensitive personal information"? (Examples include SSN, personal financial data or health records.)</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_5', ($answers && $answers->question5) ? $answers->question5 : 0, array(
                    0 => 'None',
                    1 => 'Less than 500',
                    2 => '500 to 5000',
                    3 => '5000 to 50,000',
                    4 => 'Greater than 50,000',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>6. How many employees and regular contractors have sensitive data on laptops, tablets, or smartphones - personal or company owned?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_6', ($answers && $answers->question6) ? $answers->question6 : 0, array(
                    0 => 'None ',
                    1 => 'Limited (< 5% of workforce)',
                    2 => 'Some (5 - 25% of workforce)',
                    3 => 'Moderate (25 - 50% of workforce)',
                    4 => 'Most ( > 50% of workforce)',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h4>Information Technology Infrastructure</h4>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>7. How many systems store or process sensitive information in your entire organization?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_7', ($answers && $answers->question7) ? $answers->question7 : 0, array(
                    0 => 'None',
                    1 => 'Single System',
                    2 => 'Minimal (2-10)',
                    3 => 'Moderate (20-100)',
                    4 => 'High (100-200)',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>

        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>8. How many internally developed applications that process sensitive information are within your organization?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_8', ($answers && $answers->question8) ? $answers->question8 : 0, array(
                    0 => 'None',
                    1 => 'Single Primary ',
                    2 => 'Minimal (2 - 15)',
                    3 => 'Moderate (15 - 50)',
                    4 => 'High (> 50)',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>9. How many wireless networks are in your organization? (Do not count home networks)</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_9', ($answers && $answers->question9) ? $answers->question9 : 0, array(
                    0 => 'None ',
                    1 => 'Single Network, Limited Access Points (<2)',
                    2 => 'Single Network, More than 2 access Points',
                    3 => 'Multiple (More than 5 access Points)',
                    4 => 'Multiple (More than 20 access points)',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h4>Third Party Risk</h4>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>10. How many third parties do you use to store or process customer information?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_10', ($answers && $answers->question10) ? $answers->question10 : 0, array(
                    0 => 'None',
                    1 => 'Single Primary',
                    2 => 'Minimal (2-25)',
                    3 => 'Moderate (26-100)',
                    4 => 'High (> 100)',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>11. How many third parties have direct or VPN remote access to your systems or your network?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_11', ($answers && $answers->question11) ? $answers->question11 : 0, array(
                    0 => 'None - No Remote Access ',
                    1 => 'Single Third Party with remote access',
                    2 => 'Limited (1 - 5) organizations can access our network',
                    3 => 'Moderate (11-50) organizations can access our network',
                    4 => 'High ( > 50 third parties)',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h4>Web Site and External Exposure</h4>
        </div>
        
        <div class="x_content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>12. How critical to your business is your public web site?</p>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <?php echo CHtml::radioButtonList('question_12', ($answers && $answers->question12) ? $answers->question12 : 0, array(
                    0 => 'Not Critical',
                    1 => 'Minimal (Our site is information only)',
                    2 => 'Moderate (Less than 10% of our sales or leads are from our website',
                    3 => 'Important (More than 50% of our sales or leads are generated through our website)',
                    4 => 'Critical (It is the sole delivery channel for our business)',
                ),
                        array('class'=>"flat", 'labelOptions' => array('class' => 'label-spacing'))); ?>
                </div>
            </div>
        </div>
        
        <div class="x_content">
            <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <a href="#" class="btn btn-success assessment-submit">Save</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo CHtml::endForm(); ?>

<script>
    $(function(){
       $('.assessment-submit').on('click', function(e){
           e.preventDefault();
           $('#assessment-form').submit();
       }) 
    });
</script>