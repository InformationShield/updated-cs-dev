<? $isAuthor = Utils::isRoleAuthor(); ?>
<? $isSuperOwner = (Utils::isSuper() && Utils::isRoleOwner()); ?>
<? $lnkAttr = 'data-recid="\'+record.recid+\'" data-toggle="tooltip" data-placement="left" title="\'+((record.description) ? w2utils.stripTags(record.description) : w2utils.stripTags(record.quiz_title))+\'"'; ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'quiz_library_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'quiz_title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                <? if ($isSuperOwner) { ?>
                    return '<div><a class="lnkWatch" href="<?= url('quizlibrary/edit'); ?>/id/' + record.recid + '" <?= $lnkAttr; ?>>' + record.quiz_title + '</a></div>';
                <? } else { ?>
                    return '<div><a class="lnkView" href="<?= url('quizlibrary/view'); ?>/id/' + record.recid + '" <?= $lnkAttr; ?>>' + record.quiz_title + '</a></div>';
                <? } ?>
            }
        },
        <? if ($isSuperOwner) { ?>
        { field: 'quiz_status_name', caption: 'Status', size: '75px', searchable: 'text', resizable: true, sortable: true },
        <? } ?>
        { field: 'publish_date', caption: 'Published', size: '75px', searchable: 'date', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isSuperOwner) ? '130px' : '70px'; ?>', hideable: false, sortable: false,  resizable: false, hidden: <?= (!$isAuthor) ? 'true':'false'; ?>,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkCopy" href="" data-recid="'+record.recid+'">Copy</a>'
                <? if ($isSuperOwner) { ?>
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkEdit" href="<?= url('quizlibrary/edit'); ?>/id/'+record.recid+'">Edit</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                <? } ?>
                    +'</div>';
                <? } else { ?>
                var html = '<div></div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this QuizlibraryController */
/* @var $model QuizLibrary */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Quiz Library</h2>";
    if ($isSuperOwner) {
        $gmAjaxListGrid->intro = "<p>Click on the <strong>Title</strong> to edit quiz questions directly.</p>";
    } else {
        $gmAjaxListGrid->intro = "<p>Click on the <strong>Title</strong> to view quiz.</p>";
    }
    $gmAjaxListGrid->controller = "quizlibrary";
    $gmAjaxListGrid->recid = "quiz_library_id";
    $gmAjaxListGrid->allowAdd = $isSuperOwner;
    $gmAjaxListGrid->allowEdit = $isSuperOwner;
    $gmAjaxListGrid->allowDelete = $isSuperOwner;
    $gmAjaxListGrid->allowCopy = $isAuthor;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
