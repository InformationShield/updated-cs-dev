<?
/* @var $this TaskController */
/* @var $model Task */
/* @var $form CActiveForm */
$this->pageTitle=Yii::app()->name.' - Edit Task';
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
    tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit Task</h3>


<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array('action'=>url('task/edit',array('id'=>Obscure::encode($model->task_id))),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'id'=>'edit-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->labelEx($model,'task_name'); ?>
		<?= $form->textField($model,'task_name',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'task_name'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'employee_id'); ?>
		<select name="Task[employee_id]" id="Task_employee_id" style="width: 100%;">
			<option value="" <?= (empty($model->profile_id)) ? 'selected="selected"' : ''; ?>>no one</option>
			<? $users = Employee::model()->findAllByAttributes(array('company_id' => Utils::contextCompanyId()),array('order' => 'firstname ASC, lastname ASC')); ?>
			<? foreach ($users as $user) { ?>
				<option value="<?= $user->employee_id; ?>" <?= ($model->employee_id == $user->employee_id) ? 'selected="selected"' : ''; ?>><?= $user->firstname.' '.$user->lastname; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'employee_id'); ?>
	</div>
	
	<div class="row">
		<?= $form->labelEx($model,'task_description'); ?>
		<?= $form->textArea($model,'task_description',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'task_description'); ?>
	</div>
	
	<div class="row">
		<?= $form->labelEx($model,'task_status_id'); ?>
		<select name="Task[task_status_id]" id="Task_task_status_id">
			<? $task_statuses = TaskStatus::model()->findAll(array('order' => 'task_status_id ASC')); ?>
			<? foreach($task_statuses as $ts) { ?>
				<option value="<?= $ts->task_status_id; ?>"  <?= ($model->task_status_id==$ts->task_status_id) ? 'selected="selected"' :'';?>><?= $ts->task_status_name; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'task_status_id'); ?>
	</div>
	
	<div class="row">
		<?= $form->labelEx($model,'target_date'); ?>
		<input name="Task[target_date]" id="Task_target_date" type="us-date" value="<?= $model->target_date; ?>">
		<?= $form->error($model,'target_date'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'task_frequency_id'); ?>
		<select name="Task[task_frequency_id]" id="Task_task_frequency_id">
			<? $task_frequency = TaskFrequency::model()->findAll(array('order' => 'task_frequency_id ASC')); ?>
			<? foreach($task_frequency as $tf) { ?>
				<option value="<?= $tf->task_frequency_id; ?>"  <?= ($model->task_frequency_id==$tf->task_frequency_id) ? 'selected="selected"' :'';?>><?= $tf->task_frequency_name; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'task_frequency_id'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'control_baseline_id'); ?>
		<select name="Task[control_baseline_id]" id="Task_control_baseline_id" style="width: 99%;">
			<option value="" <?= (empty($model->company_id)) ? 'selected="selected"' : ''; ?>>None</option>
			<? $controls = ControlBaseline_Ex::getRelatedControls(); ?>
			<? foreach ($controls as $control) { ?>
				<option value="<?= $control['control_baseline_id']; ?>" <?= ($model->control_baseline_id == $control['control_baseline_id']) ? 'selected="selected"' : ''; ?>><?= $control['category'].' - '.$control['title']; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'control_baseline_id'); ?>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('task/list'); ?>";
			return false;
		});
	});
</script>