<? $isAuthor = Utils::isRoleAuthor(); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'task_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'task_name', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'task_status_name', caption: 'Status', size: '80px', searchable: 'text', resizable: true, sortable: true },
        { field: 'firstname', caption: 'Assigned To First Name', hidden: true, sortable: true, searchable: 'text' },
        { field: 'lastname', caption: 'Assigned To Last Name', hidden: true, sortable: true, searchable: 'text' },
        { field: 'employee_name', caption: 'Assigned To', size: '150px', resizable: true, sortable: true },
        { field: 'task_frequency_name', caption: 'Frequency', size: '80px', searchable: 'text', resizable: true, sortable: true },
        { field: 'target_date', caption: 'Target Date', size: '80px', searchable: 'date', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '100px' : '55px'; ?>', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkEdit" href="<?= url('task/edit'); ?>/id/'+record.recid+'">Edit</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                        +'</div>';
                <? } else { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'</div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this TaskController */
/* @var $model Task */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Tasks</h2>";
    $gmAjaxListGrid->intro = "<p>Administrators and Authors can manage the compliance related tasks and assign them to a <a href=\"'.url('employee/list').'\" style=\"color: blue;\">User</a>.</p>";
    $gmAjaxListGrid->listUrl = $listUrl;
    $gmAjaxListGrid->controller = "task";
    $gmAjaxListGrid->recid = "task_id";
    $gmAjaxListGrid->allowAdd = $isAuthor;
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->allowDelete = $isAuthor;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
