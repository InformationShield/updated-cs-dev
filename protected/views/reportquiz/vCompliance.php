<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>
<?
    $pieHeight = 600;
    $lnkAttr = 'data-recid="\'+record.recid+\'" data-toggle="tooltip" data-placement="left" title="\'+((record.description) ? w2utils.stripTags(record.description) : w2utils.stripTags(record.quiz_title))+\'"';
?>
<h2>Quiz Compliance Report</h2>
<div class="left" style="margin: 0 0 10px 0;">
    <p>
        Click on the <strong>Title</strong> to view the chart for that quiz.<br/>
        Click the <i class="fa fa-table purple" style="font-size: 16px;"></i> see a detailed user report with export and print features.
    </p>
</div>
<div class="right" style="margin: 0 0 10px 0;">
    <a href="" id="lnkExport" class="btn btn-success" >Export to CSV</a>
    <a href="<?= $printUrl; ?>" target="_blank" class="btn btn-success">Print</a>
</div>

<style type="text/css">
    .piechart { margin: 0 0 20px 0; height: <?= $pieHeight; ?>px;}
    .pieloader { margin: 75px 0 0 75px; }
</style>
<script type="text/javascript">
    function quizTracking(recid) {
        var record = w2ui['grid'].get(recid);
        if (record) {
            $('#spnTitle').text(record.quiz_title);
            $('#divDescription').html(record.description);
            var quizTrackingChart = new FusionCharts({
                type: 'pie3D',
                renderAt: 'divQuizTracking',
                width: '100%',
                height: '<?= $pieHeight; ?>',
                dataFormat: 'json',
                dataSource: {
                    "chart": {
                        "caption": "Passed " + record.quiz_passed + " of " + record.quiz_count,
                        "subCaption": "",
                        "numberPrefix": "",
                        "showPercentInTooltip": "1",
                        "showLegend": "1",
                        "legendShadow": '1',
                        "decimals": "0",
                        "useDataPlotColorForLabels": "1",
                        "theme": "fint"
                    },
                    "data": [
                        {"label": "Unopened", "value": record.quiz_unopened, "link": "", "color": "FF0000", "labelFontColor": "000000"},
                        {"label": "Viewed", "value": record.quiz_viewed, "link": "", "color": "FFFF00", "labelFontColor": "000000"},
//                        {"label": "Completed", "value": record.quiz_completed, "link": "", "color": "00BFFF", "labelFontColor": "000000"},
                        {"label": "Passed", "value": record.quiz_passed, "link": "", "color": "00FF00", "labelFontColor": "000000"}
                    ]
                }
            }).render();
            return true;
        } else {
            return false;
        }
    };

    var _showChart = true;
    var _ajaxListGridColumns = [
        { field: 'quiz_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'action', caption: 'Detail', size: '45px',  resizable: false, sortable: false,
            render: function(record, index, column_index) {
                if (_showChart) {
                    _showChart = false;
                    $('#divShowChart').show();
                    FusionCharts.ready(function () {
                        quizTracking(record.recid);
                    });
                }
                return '<div>&nbsp;&nbsp;&nbsp;<a href="<?= url('reportquiz/compliancedetail'); ?>/id/' + record.recid + '"><i class="fa fa-table purple" style="font-size: 16px;"></i></a></div>';
            }
        },
        { field: 'quiz_title', caption: 'Title', size: '100%',  resizable: true, sortable: false,
            render: function (record, index, column_index) {
                return '<div><a class="lnkChart" href="" <?= $lnkAttr; ?>>' + record.quiz_title+ '</a></div>';
            }
        },
        { field: 'publish_date', caption: 'Published', size: '90px',  resizable: true, sortable: false},
        { field: 'quiz_passed_per', caption: 'Passed', size: '60px',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                return '<div>'+record.quiz_passed_per+'%</div>';
            }
        },
        { field: 'quiz_completed_per', caption: 'Completed', size: '75px',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                return '<div>'+record.quiz_completed_per+'%</div>';
            }
        },
        { field: 'quiz_viewed_per', caption: 'Viewed', size: '55px',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                return '<div>'+record.quiz_viewed_per+'%</div>';
            }
        },
        { field: 'quiz_unopened_per', caption: 'Unopened', size: '75px',  resizable: true, sortable: false,
            render: function(record, index, column_index) {
                return '<div>'+record.quiz_unopened_per+'%</div>';
            }
        }
    ];

    var _ajaxListGridAddFunction = false;
</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = url('reportquiz/getcompliance');
    $gmAjaxListGrid->title = "";
    $gmAjaxListGrid->intro = "";
    $gmAjaxListGrid->controller = "reportquiz";
    $gmAjaxListGrid->recid = "quiz_id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = false;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    $gmAjaxListGrid->showToolbar = false;
    $gmAjaxListGrid->showSearch = false;
    $gmAjaxListGrid->showFooter = false;
	$gmAjaxListGrid->fixedBody = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
<div id="divShowChart" style="display:none;">
    <div style="margin: 10px 0 10px 10px;">
        <h2><span id="spnTitle">&nbsp;</span></h2>
        <div id="divDescription">&nbsp;</div>
    </div>
    <div id="divQuizTracking" class="col-md-12 col-sm-12 col-xs-12 piechart">
        <img src="/images/ajax-loader.gif" class="pieloader" />
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(document).on('click', '.lnkChart', function () {
            var recid = $(this).data('recid');
            quizTracking(recid);
            return false;
        });
        $(document).on('click', '#lnkExport', function () {
            new PNotify({
                title: 'Downloading...',
                text: 'Your file is downloading now.',
                delay: 3500,
                type: 'info'
            });
            window.location = '<?= $exportUrl; ?>';
            return false;
        });
    });
</script>
