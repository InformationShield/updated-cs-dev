<h2>Profile Compliance Detail Report for <?= $profile['profile_title']; ?></h2>
<div class="left" style="margin: 0 0 10px 0;">
    <p>
        <?= $profile['profile_description']; ?>
    </p>
</div>
<div class="right" style="margin: 0 0 10px 0;">
    <a href="" id="lnkExport" class="btn btn-success" >Export to CSV</a>
    <a href="<?= url('reportprofile/printcompliancedetail', array('id'=>$id)); ?>" target="_blank" class="btn btn-success">Print</a>
</div>

<div class="clearfix"></div>
<script type="text/javascript">
	var _ajaxListGridColumns = [
		{ field: 'employee_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
		{ field: 'username', caption: 'User Name', size: '100%',  resizable: true, sortable: false,
			render: function (record, index, column_index) {
				var style = (record.department_name == '-') ? ' style="background-color: yellow;"' : '';
				return '<div'+style+'><a class="lnkChart" href="" data-recid="'+record.recid+'">' + record.firstname+' '+record.lastname+ '</a></div>';
			}
		},
		{ field: 'department_name', caption: 'Department', size: '90px',  resizable: true, sortable: false,
			render: function (record, index, column_index) {
				var style = (record.department_name == '-') ? ' style="background-color: yellow;"' : '';
				return '<div'+style+'>'+record.department_name+'</div>';
			}
		},
		{ field: 'lastvisit_at', caption: 'Last Visit', size: '100px', hidden: false,  resizable: true, sortable: false,
			render: function(record, index, column_index) {
				if (record.department_name == '-') {
					return '<div style="background-color: yellow;">' +record.lastvisit_at+ '</div>';
				} else {
					return '<div>'+record.lastvisit_at+'</div>';
				}
			}
		},
		{ field: 'policy_acks_per', caption: 'Policy Acknowledged', size: '131px',  resizable: true, sortable: false,
			render: function(record, index, column_index) {
				if (record.policy_count > 0) {
					return '<div>'+record.policy_acks_per+'%</div>';
				} else {
					return '<div>-</div>';
				}
			}
		},
		{ field: 'training_completed_per', caption: 'Training Completed', size: '126px',  resizable: true, sortable: false,
			render: function(record, index, column_index) {
				if (record.training_count > 0) {
					return '<div>'+record.training_completed_per+'%</div>';
				} else {
					return '<div>-</div>';
				}
			}
		},
		{ field: 'quiz_passed_per', caption: 'Quiz Passed', size: '90px',  resizable: true, sortable: false,
			render: function(record, index, column_index) {
				if (record.quiz_count > 0) {
					return '<div>'+record.quiz_passed_per+'%</div>';
				} else {
					return '<div>-</div>';
				}
			}
		}
	];
	var _ajaxListGridAddFunction = false;
</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = url('reportprofile/getcompliancedetail', array('id'=>$id));
    $gmAjaxListGrid->title = '';
    $gmAjaxListGrid->intro = '';
    $gmAjaxListGrid->controller = "reportprofile";
    $gmAjaxListGrid->recid = "id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = false;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    $gmAjaxListGrid->showToolbar = false;
    $gmAjaxListGrid->showSearch = false;
    $gmAjaxListGrid->showFooter = false;
    $gmAjaxListGrid->fixedBody = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>

<script type="text/javascript">
    $(function(){

        $(document).on('click', '#lnkExport', function () {
            new PNotify({
                title: 'Downloading...',
                text: 'Your file is downloading now.',
                delay: 3500,
                type: 'info'
            });
            window.location = '<?= url('reportprofile/exportcompliancedetail', array('id'=>$id,'export'=>'csv')); ?>';
            return false;
        });

    });
</script>
