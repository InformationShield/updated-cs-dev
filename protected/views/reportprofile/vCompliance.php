<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>
<?
    $pieHeight = 300;
    $lnkAttr = 'data-recid="\'+record.recid+\'" data-toggle="tooltip" data-placement="left" title="\'+((record.description) ? w2utils.stripTags(record.description) : w2utils.stripTags(record.doc_title))+\'"';
?>
<h2>Profile Compliance Report</h2>
<div class="left" style="margin: 0 0 10px 0;">
    <p>
        Click on the <strong>Title</strong> to view the chart for that profile.<br/>
        Click the <i class="fa fa-table purple" style="font-size: 16px;"></i> see a detailed user report with export and print features.
    </p>
</div>
<div class="right" style="margin: 0 0 10px 0;">
    <a href="" id="lnkExport" class="btn btn-success" >Export to CSV</a>
    <a href="<?= url('reportprofile/printcompliance'); ?>" target="_blank" class="btn btn-success">Print</a>
</div>

<style type="text/css">
    .piechart { margin: 0 0 20px 0; height: <?= $pieHeight; ?>px;}
    .pieloader { margin: 75px 0 0 75px; }
</style>
<script type="text/javascript">
	function overviewTracking(recid) {
		var record = w2ui['grid'].get(recid);
		if (record) {
			$('#profile_title').text(record.profile_title);
			var completed = parseInt(record.quiz_passed)+parseInt(record.policy_acks)+parseInt(record.training_completed);
			var unopened = parseInt(record.quiz_unopened)+parseInt(record.policy_unopened)+parseInt(record.training_unopened);
			var viewed = parseInt(record.quiz_viewed)+parseInt(record.policy_views)+parseInt(record.training_views);
			var total = unopened+viewed+completed;
			var overviewTrackingChart = new FusionCharts({
				type: 'pie3D',
				renderAt: 'divOverviewTracking',
				width: '100%',
				height: '<?= $pieHeight; ?>',
				dataFormat: 'json',
				dataSource: {
					"chart": {
						"caption": "Overview of all Documents",
						"subCaption": "Completed " +completed+ " of " +total,
						"numberPrefix": "",
						"showPercentInTooltip": "1",
						"showLegend": "1",
						"legendShadow": '1',
						"decimals": "0",
						"useDataPlotColorForLabels": "1",
						"theme": "fint"
					},
					"data": [
						{"label": "Unopened", "value": unopened, "link": "", "color": "FF0000", "labelFontColor": "000000"},
						{"label": "Viewed", "value": viewed, "link": "", "color": "FFFF00", "labelFontColor": "000000"},
						{"label": "Completed", "value": completed, "link": "", "color": "00FF00", "labelFontColor": "000000"}
					]
				}
			}).render();
			return true;
		} else {
			return false;
		}
	};

	function policyTracking(recid) {
		var record = w2ui['grid'].get(recid);
		if (record) {
			var total = parseInt(record.policy_count);
			var policyTrackingChart = new FusionCharts({
				type: 'pie3D',
				renderAt: 'divPolicyTracking',
				width: '100%',
				height: '<?= $pieHeight; ?>',
				dataFormat: 'json',
				dataSource: {
					"chart": {
						"caption": "Policies",
						"subCaption": "Acknowledged " + record.policy_acks + " of " + total,
						"numberPrefix": "",
						"showPercentInTooltip": "1",
						"showLegend": "1",
						"legendShadow": '1',
						"decimals": "0",
						"useDataPlotColorForLabels": "1",
						"theme": "fint"
					},
					"data": [
						{"label": "Unopened", "value": record.policy_unopened, "link": "", "color": "FF0000", "labelFontColor": "000000"},
						{"label": "Viewed", "value": record.policy_views, "link": "", "color": "FFFF00", "labelFontColor": "000000"},
						{"label": "Acknowledged", "value": record.policy_acks, "link": "", "color": "00FF00", "labelFontColor": "000000"}
					]
				}
			}).render();
			return true;
		} else {
			return false;
		}
	};

	function trainingTracking(recid) {
		var record = w2ui['grid'].get(recid);
		if (record) {
			var total = parseInt(record.training_count);
			var trainingTrackingChart = new FusionCharts({
				type: 'pie3D',
				renderAt: 'divTrainingTracking',
				width: '100%',
				height: '<?= $pieHeight; ?>',
				dataFormat: 'json',
				dataSource: {
					"chart": {
						"caption": "Training",
						"subCaption": "Completed " + record.training_completed + " of " +total,
						"numberPrefix": "",
						"showPercentInTooltip": "1",
						"showLegend": "1",
						"legendShadow": '1',
						"decimals": "0",
						"useDataPlotColorForLabels": "1",
						"theme": "fint"
					},
					"data": [
						{"label": "Unopened", "value": record.training_unopened, "link": "", "color": "FF0000", "labelFontColor": "000000"},
						{"label": "Viewed", "value": record.training_views, "link": "", "color": "FFFF00", "labelFontColor": "000000"},
						{"label": "Completed", "value": record.training_completed, "link": "", "color": "00FF00", "labelFontColor": "000000"}
					]
				}
			}).render();
			return true;
		} else {
			return false;
		}
	};

	function quizTracking(recid) {
		var record = w2ui['grid'].get(recid);
		if (record) {
			var total = parseInt(record.quiz_count);
			var quizTrackingChart = new FusionCharts({
				type: 'pie3D',
				renderAt: 'divQuizTracking',
				width: '100%',
				height: '<?= $pieHeight; ?>',
				dataFormat: 'json',
				dataSource: {
					"chart": {
						"caption": "Quizzes",
						"subCaption": "Passed " + record.quiz_passed + " of " +total,
						"numberPrefix": "",
						"showPercentInTooltip": "1",
						"showLegend": "1",
						"legendShadow": '1',
						"decimals": "0",
						"useDataPlotColorForLabels": "1",
						"theme": "fint"
					},
					"data": [
						{"label": "Unopened", "value": record.quiz_unopened, "link": "", "color": "FF0000", "labelFontColor": "000000"},
						{"label": "Viewed", "value": record.quiz_viewed, "link": "", "color": "FFFF00", "labelFontColor": "000000"},
						{"label": "Passed", "value": record.quiz_passed, "link": "", "color": "00FF00", "labelFontColor": "000000"}
					]
				}
			}).render();
			return true;
		} else {
			return false;
		}
	};


    var _showChart = true;
    var _ajaxListGridColumns = [
        { field: 'user_profile_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'action', caption: 'Detail', size: '45px',  resizable: false, sortable: false,
            render: function(record, index, column_index) {
                if (_showChart) {
                    _showChart = false;
                    $('#divShowChart').show();
                    FusionCharts.ready(function () {
						overviewTracking(record.recid);
						policyTracking(record.recid);
						trainingTracking(record.recid);
						quizTracking(record.recid);
                    });
                }
                return '<div>&nbsp;&nbsp;&nbsp;<a href="<?= url('reportprofile/compliancedetail'); ?>/id/' + record.recid + '"><i class="fa fa-table purple" style="font-size: 16px;"></i></a></div>';
            }
        },
        { field: 'profile_title', caption: 'Title', size: '100%',  resizable: true, sortable: false,
            render: function (record, index, column_index) {
                return '<div><a class="lnkChart" href="" <?= $lnkAttr; ?>>' + record.profile_title+ '</a></div>';
            }
        },
		{ field: 'policy_acks_per', caption: 'Policy Acknowledged', size: '131px',  resizable: true, sortable: false,
			render: function(record, index, column_index) {
				if (record.policy_count > 0) {
					return '<div>'+record.policy_acks_per+'%</div>';
				} else {
					return '<div>-</div>';
				}
			}
		},
		{ field: 'training_completed_per', caption: 'Training Completed', size: '126px',  resizable: true, sortable: false,
			render: function(record, index, column_index) {
				if (record.training_count > 0) {
					return '<div>'+record.training_completed_per+'%</div>';
				} else {
					return '<div>-</div>';
				}
			}
		},
		{ field: 'quiz_passed_per', caption: 'Quiz Passed', size: '90px',  resizable: true, sortable: false,
			render: function(record, index, column_index) {
				if (record.quiz_count > 0) {
					return '<div>'+record.quiz_passed_per+'%</div>';
				} else {
					return '<div>-</div>';
				}
			}
		}
    ];

    var _ajaxListGridAddFunction = false;
</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = url('reportprofile/getcompliance');
    $gmAjaxListGrid->title = "";
    $gmAjaxListGrid->intro = "";
    $gmAjaxListGrid->controller = "reportprofile";
    $gmAjaxListGrid->recid = "user_profile_id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = false;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    $gmAjaxListGrid->showToolbar = false;
    $gmAjaxListGrid->showSearch = false;
    $gmAjaxListGrid->showFooter = false;
    $gmAjaxListGrid->fixedBody = false;
//    $gmAjaxListGrid->fixedBody = true;
//    $gmAjaxListGrid->fixedHeight = $height;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
<div id="divShowChart" style="display:none;">
	<div style="margin: 10px 0 10px 10px;"><h2><span id="profile_title">&nbsp;</span></h2></div>
	<div id="divOverviewTracking" class="col-md-6 col-sm-6 col-xs-12 piechart">
		<img src="/images/ajax-loader.gif" class="pieloader" />
	</div>
	<div id="divPolicyTracking" class="col-md-6 col-sm-6 col-xs-12 piechart">
		<img src="/images/ajax-loader.gif" class="pieloader" />
	</div>
	<div id="divTrainingTracking" class="col-md-6 col-sm-6 col-xs-12 piechart">
		<img src="/images/ajax-loader.gif" class="pieloader" />
	</div>
	<div id="divQuizTracking" class="col-md-6 col-sm-6 col-xs-12 piechart">
		<img src="/images/ajax-loader.gif" class="pieloader" />
	</div>

</div>

<script type="text/javascript">
    $(function(){
        $(document).on('click', '.lnkChart', function () {
            var recid = $(this).data('recid');
			overviewTracking(recid);
			policyTracking(recid);
			trainingTracking(recid);
			quizTracking(recid);
            return false;
        });
        $(document).on('click', '#lnkExport', function () {
            new PNotify({
                title: 'Downloading...',
                text: 'Your file is downloading now.',
                delay: 3500,
                type: 'info'
            });
            window.location = '<?= url('reportprofile/exportcompliance', array('export'=>'csv')); ?>';
            return false;
        });
    });
</script>
