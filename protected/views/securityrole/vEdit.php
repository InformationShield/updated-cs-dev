<?
/* @var $this SecurityroleController */
/* @var $model SecurityRole */
/* @var $form CActiveForm */
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit Security Role</h3>

<div class="form">

<? $form=$this->beginWidget('CActiveForm', array('action'=>url('securityrole/edit',array('id'=>Obscure::encode($model->security_role_id))),
	'id'=>'edit-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
        <?= $form->labelEx($model,'role_title'); ?>
		<?= $form->textField($model,'role_title',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'role_title'); ?>
	</div>

	<div class="row">
		<div style="width: 10em; display: inline-block;">
			<?= $form->labelEx($model,'role_status'); ?>
		</div>
		<div style="display: inline-block;">
			<?= $form->labelEx($model,'employee_id'); ?>
		</div>
		<div class="clearfix"></div>
		<div style="width: 10em; display: inline-block;">
			<select name="SecurityRole[role_status]" id="SecurityRole_role_status">
				<? $rolestatus = SecurityRoleStatus::model()->findAll(array('order' => 'role_status_id ASC')); ?>
				<? foreach ($rolestatus as $status) { ?>
					<option value="<?= $status->role_status_id; ?>" <?= ($model->role_status == $status->role_status_id) ? 'selected="selected"' : ''; ?>><?= $status->role_status_name; ?></option>
				<? } ?>
			</select>
			<?= $form->error($model,'role_status'); ?>
		</div>
		<div style="display: inline-block;">
			<select name="SecurityRole[employee_id]" id="SecurityRole_employee_id" style="width: 100%;">
				<option value="" <?= (empty($model->profile_id)) ? 'selected="selected"' : ''; ?>>no one</option>
				<? $users = Employee::model()->findAllByAttributes(array('company_id' => Utils::contextCompanyId()),array('order' => 'firstname ASC, lastname ASC')); ?>
				<? foreach ($users as $user) { ?>
					<option value="<?= $user->employee_id; ?>" <?= ($model->employee_id == $user->employee_id) ? 'selected="selected"' : ''; ?>><?= $user->firstname.' '.$user->lastname; ?></option>
				<? } ?>
			</select>
			<?= $form->error($model,'employee_id'); ?>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'role_department'); ?>
		<?= $form->textField($model,'role_department',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'role_department'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'role_description'); ?>
		<?= $form->textArea($model,'role_description',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'role_description'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'role_summary'); ?>
		<?= $form->textArea($model,'role_summary',array('rows'=>'10','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'role_summary'); ?>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('securityrole/list'); ?>";
			return false;
		});
	});
</script>