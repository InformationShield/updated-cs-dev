<? $isAdmin = Utils::isRoleAdmin(); ?>
<? $isAuditor = Utils::isRoleAuditor(); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'security_role_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'role_status', caption: 'role_status', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'role_title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'role_department', caption: 'Department', size: '150px', searchable: 'text', resizable: true, sortable: true },
        { field: 'role_status_name', caption: 'Status', size: '70px', searchable: 'text', resizable: true, sortable: true },
        { field: 'firstname', caption: 'Assigned To First Name', hidden: true, sortable: true, searchable: 'text' },
        { field: 'lastname', caption: 'Assigned To Last Name', hidden: true, sortable: true, searchable: 'text' },
        { field: 'employee_name', caption: 'Assigned To', size: '150px', hideable: false, sortable: true, resizable: true }
    <? if ($isAdmin) { ?>
        ,{ field: 'action', caption: 'Actions', size: '135px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkEdit" href="<?= url('securityrole/edit'); ?>/id/'+record.recid+'">Edit</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkCopy" href="" data-recid="'+record.recid+'">Copy</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                    +'</div>';
                return html;
            }
        }
    <? } else if ($isAuditor) { ?>
        ,{ field: 'action', caption: 'Actions', size: '55px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'</div>';
                return html;
            }
        }
    <? } ?>
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this SecurityroleController */
/* @var $model SecurityRole */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Security Roles</h2>";
    $gmAjaxListGrid->intro = '<p>Security Roles are used to document the employee roles of your information security program. '
        .'Administrators can add, view, edit, copy and delete Security Roles. '
        .'Security Roles can be assigned to a <a href="'.url('employee/list').'" style="color: blue;">User</a>. '
        .'</p>';
    $gmAjaxListGrid->listUrl = $listUrl;
    $gmAjaxListGrid->controller = "securityrole";
    $gmAjaxListGrid->recid = "security_role_id";
    $gmAjaxListGrid->allowAdd = $isAdmin;
    $gmAjaxListGrid->allowEdit = $isAdmin;
    $gmAjaxListGrid->allowDelete = $isAdmin;
    $gmAjaxListGrid->allowCopy = $isAdmin;
    $gmAjaxListGrid->reloadOnCopy = true;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
