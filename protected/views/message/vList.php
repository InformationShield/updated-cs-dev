<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'message_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'message', caption: 'Message', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'read', caption: 'Status', size: '50px', resizable: true, sortable: false,
            render: function(record, index, column_index){
                var html = '';
                if(record.read == "0"){
                    html = '<i class="fa fa-envelope-o" style="font-size: 20px; margin-left: 10px;"></i>';
                }else if(record.read == "1"){
                    html = '<i class="fa fa-envelope-open-o" style="font-size: 20px; margin-left: 10px;"></i>';
                }
                return html;
            }
        },
        { field: 'created_on', caption: 'Created On', size: '80px', resizable: true, sortable: false},
        { field: 'action', caption: 'Actions', size: '85px', hideable: false, sortable: false,  resizable: false,
                render: function (record, index, column_index) {
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                        +'</div>';
                    return html;
                }
            },
    ];
    var _ajaxListGridAddFunction = false;
</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Messages</h2>";
    $gmAjaxListGrid->intro = '<p></p>';
    $gmAjaxListGrid->controller = "message";
    $gmAjaxListGrid->recid = "message_id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = true;
    $gmAjaxListGrid->reloadOnView = true;
    $gmAjaxListGrid->showViewDelete = true;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = true;
    $gmAjaxListGrid->postReloadFunction = "_ajaxLoadMessageMenu();";
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
