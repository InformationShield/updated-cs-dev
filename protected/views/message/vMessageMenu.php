<? if (!empty($right_messages)) { ?>
    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-envelope-o"></i>
        <span id="spnMessageCount" class="badge bg-green"><?= $right_messages['count'];  ?></span>
    </a>
    <ul id="menuMessages" class="dropdown-menu list-unstyled msg_list" role="menu">
        <? if ($right_messages['count'] > 0) { ?>
            <? $i = 0; ?>
            <? foreach ($messages as $mes) { $i++; ?>
                <li>
                    <a class="lnkMsgView" href="<?= url('message/view',array('id'=>Obscure::encode($mes->message_id))); ?>">
                                            <span class="time">
                                                <?= DateTimeTools::TimeElapsedString($mes->created_on,false); ?>
                                            </span>
                        <br/>
                        <span>
                                                <strong><?= (strlen($mes->title) <= 50) ? $mes->title : substr($mes->title, 0, 50)."..."; ?></strong>
                                            </span>
                        <br/>
                        <span class="message">
                                                <?= (strlen($mes->message) <= 200) ? $mes->message : substr($mes->message, 0, 200)."..."; ?>
                                            </span>
                    </a>
                </li>
                <? if ($i >= 5) break; ?>
            <? } ?>
        <? } ?>
        <li>
            <a href="<?= $right_messages['url']; ?>">
                <div class="text-center">
                    <strong>All Messages</strong>
                </div>
            </a>
        </li>
    </ul>
    <script type="text/javascript">
        $(function () {
            function _calcPageHeight() {
                var _f = $('#footer').height();
                var _adj = _f + 30;
                var _h = $(window).height() - _adj;
                var _bh = $('body').height();
                _h = (_h < (_bh - _adj)) ? (_bh - _adj) : _h;
                return _h;
            }
            $('#menuMessages').on('click', '.lnkMsgView', function () {
                var href = $(this).attr('href');
                var w = $('body').width() - 60;
                var h = _calcPageHeight();
                h = (h > 800) ? 800 : h;
                var popup = w2popup.load({
                    url: href,
                    showMax: true,
                    width: w,     // width in px
                    height: h,     // height in px
                    title: 'View Message',
                    buttons: '<button class="btn" onclick="w2popup.close();">Close</button> '
                    + '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('message/list'); ?>\';">All Messages</button>'
                });
                setTimeout(_ajaxLoadMessageMenu,100); // reload message unread count
                return false;
            });
        });
    </script>
<? } ?>