<? $isAuthor = Utils::isRoleAuthor(); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'training_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'doc_title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                if (record.training_type == 'Video') {
                    return '<div><a class="lnkWatch" href="<?= url('videotraining/watchseries'); ?>/ret/1/library/0/id/' + record.recid + '">' + record.doc_title + '</a></div>';
                } else {
                    return '<div><a class="lnkDownload" href="<?= url('training/download'); ?>/id/' + record.recid + '">' + record.doc_title + '</a></div>';
                }
            }
        },
        { field: 'author', caption: 'Author', size: '150px', searchable: 'text', resizable: true, sortable: true },
        { field: 'training_type', caption: 'Type', size: '65px', searchable: 'text', resizable: true, sortable: true },
        { field: 'publish_date', caption: 'Published', size: '75px', searchable: 'date', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '100px' : '55px'; ?>', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'<span>&nbsp;</span>';
                if (record.training_type == 2) {
                    html += '<a class="lnkEdit" href="<?= url('training/edit'); ?>/id/'+record.recid+'">Edit</a>';
                } else {
                    html += '<a class="lnkEdit" href="<?= url('training/edit'); ?>/id/'+record.recid+'">Edit</a>';
                }
                html += '<span>&nbsp;</span>'
                    +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                    +'</div>';
                <? } else { ?>
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'</div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = function(event) {
        w2popup.open({
            title     : 'Add New Training',
            body      : '<div class="w2ui-centered">Click on <strong>New Document</strong> to create a training document.'+
            '<br/>Or click on <strong>New Video</strong> to create a training video or video series.'+
            '<br/>Or click on <strong>Copy from Library</strong> to copy existing training materials from the Training Library.'+
            '</div>',
            buttons   : '<button class="btn" onclick="w2popup.close();">Cancel</button> '+
            '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('training/edit/type/1'); ?>\';"><strong>New Document</strong></button>'+
            '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('training/edit/type/2'); ?>\';"><strong>New Video</strong></button>'+
            '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('traininglibrary/list'); ?>\';"><strong>Copy from Library</strong></button>',
            width     : 550,
            height    : 250,
            overflow  : 'hidden',
            opacity   : '0.2',
            modal     : true,
            showClose : true,
            showMax   : false
        });
        return false;
    };

</script>
<?
/* @var $this TrainingController */
/* @var $model Training */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Manage Training</h2>";
    $gmAjaxListGrid->intro = "<p>Click on the <strong>Title</strong> to download the training document or view the video series.</p>";
    $gmAjaxListGrid->controller = "training";
    $gmAjaxListGrid->recid = "training_id";
    $gmAjaxListGrid->allowAdd = $isAuthor;
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->allowDelete = $isAuthor;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
