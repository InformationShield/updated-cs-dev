<?
/* @var $this VideotrainingController */
/* @var $model VideoTraining */
/* @var $form CActiveForm */
$selectOption = '';
?>
<h3>Watch Training Video Series</h3>
<p>
    <? if (!empty($trainings) && count($trainings) > 1) { ?>
        Select a video to play from the current series.
        Or select another video training series to watch:<br/>
        <p>
            Series:&nbsp;
            <select name="TrainingTitle" id="TrainingTitle">
                <? foreach ($trainings as $t) { ?>
                    <? if (!isset($trainingInfo['training_id'])) $trainingInfo['training_id'] = ''; ?>
                    <? if (!isset($trainingInfo['training_library_id'])) $trainingInfo['training_library_id'] = ''; ?>
                    <? if ($t['library']==0) { $tid = $trainingInfo['training_id']; } else { $tid = $trainingInfo['training_library_id']; }  ?>
                    <? $selected = (($tid==$t['id'] && $t['library']==0) || ($tid==$t['id'] && $t['library']==1)) ? 'selected="selected"' :''; ?>
                    <? if ($selected) $selectOption = $t['library'].Obscure::encode($t['id']); ?>
                    <option value="<?= $t['library'].Obscure::encode($t['id']); ?>"  <?= $selected; ?>><?= $t['doc_title']; ?></option>
                <? } ?>
            </select>
        </p>
    <? } else if (empty($trainings) && empty($trainingInfo)) { ?>
        There are no Training Video Series available.
    <? } else { ?>
        Select a video to play from the current series.
    <? } ?>
</p>
<? if (!empty($trainingInfo)) { ?>
    <h2><em><?= $trainingInfo['doc_title']; ?></em> by <?= $trainingInfo['author']; ?></h2>
    <div class="col-md-5 col-sm-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Watch List</h2>
                <div class="clearfix"></div>
            </div>
            <div id="divVideos" class="x_content">
                <div class="">
                    <? if (empty($models)) { ?>
                        <p>There are no videos available for this series.</p>
                    <? } else { ?>
                        <ul class="to_do">
                            <? $i=1; ?>
                            <? foreach($models as $model) { ?>
                                    <li>
                                        <p>
                                            <a href="" class="lnkPlay" id="btnPlay-<?= $i; ?>" style="text-decoration: none;">
                                                <?= $model->video_title; ?>
                                                <input name="VideoTraining[<?= $i; ?>][video_title]" id="VideoTraining_<?= $i; ?>_video_title" type="hidden" value="<?= CHtml::encode($model->video_title); ?>">
                                                <input name="VideoTraining[<?= $i; ?>][video_title]" id="VideoTraining_<?= $i; ?>_video_code" type="hidden" value="<?= CHtml::encode($model->video_code); ?>">
                                            </a>
                                        </p>
                                    </li>
                                <? $i++; ?>
                            <? } ?>
                        </ul>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
    <? if (!empty($models)) { ?>
        <div class="col-md-7 col-sm-7 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 id="hdrVideoTitle"></h2>
                    <div class="clearfix"></div>
                </div>
                <div id="divPlayer" class="x_content">
                </div>
            </div>
        </div>
    <? } ?>
<? } ?>

<script type="text/javascript">
    var __currentVideoNum = '1';
    $(function(){
        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }

        $('#divVideos').on('click','.lnkPlay',function(){
            var id = $(this).attr('id');
            id =  id.replace('btnPlay-','');
            __currentVideoNum = id;
            var title = $('#VideoTraining_'+id+'_video_title').val().trim();
            var html = $('#VideoTraining_'+id+'_video_code').val().trim();
            var pw = $('#divPlayer').width();
            var ph = Math.ceil((pw * 9) / 16);
            if (html.indexOf('height="720"') > 0 && html.indexOf('width="1280"') > 0) {
                ph = Math.ceil(pw*0.5625);
            }
            html = html.replace(/(width=")\d+/, '$1'+pw);
            html = html.replace(/(height=")\d+/, '$1'+ph);
            html = html.replace(/http\:/g, 'https:');
            var start = html.indexOf('<iframe');
            var stop = html.indexOf('</iframe>');
            $('#hdrVideoTitle').html(title);
            if (start >= 0 && stop > start+7) {
                var iframe =  $($.parseHTML(html));
                if (iframe && iframe.length > 0) {
                    $('#hdrVideoTitle').html(title);
                    $('#divPlayer').html(html);
                } else {
                    $('#divPlayer').html('<p>Could not play video because the iframe video code is missing or invalid.<p>');
                }
            } else {
                $('#divPlayer').html('<p>Could not play video because the video code is not a valid iframe.</p>');
            }
            return false;
        });

        $('#btnPlay-'+__currentVideoNum).click();

        $( window ).resize(function() {
            $('#btnPlay-'+__currentVideoNum).click();
        });

        $('#TrainingTitle').change(function(){
            var current = '<?= $selectOption; ?>';
            var val = $(this).find("option:selected").val();
            if (val != current) {
                var library = val.substr(0,1);
                var id = val.substr(1);
                var url = '<?= url('videotraining/watchseries'); ?>/library/'+library+'/id/'+id;
                window.location = url;
            }
            return false;
        });
    });
</script>
