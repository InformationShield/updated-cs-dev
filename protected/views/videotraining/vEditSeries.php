<?
/* @var $this VideotrainingController */
/* @var $model VideoTraining */
/* @var $form CActiveForm */
?>

<style>
    #sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
    #sortable li { margin: 0; padding: 0; font-size: 1.2em; }
    #sortable li { height: auto; }
    .ui-state-highlight { height: 3em; min-height: 3em; margin: 0; }
    #sortable li .x_panel { padding: 0; margin: 0; }
    #sortable li .x_title { border: none; padding: 0; margin: 0; }
    #sortable li .panel_toolbox { margin: 8px 5px 0 5px; }
    #sortable li .x_title h2 { margin: 10px; font-size: 1em; height: 1.5em; line-height: 1.5em; }
</style>

<script type="text/javascript">
    $(function() {
        $( "#sortable" ).sortable({
           //placeholder: "ui-state-highlight"
        });
        $( "#sortable" ).disableSelection();
    });
</script>

<h3>Edit Training Video Series</h3>
<p>
    You can <i class="fa fa-close red"></i> delete, <i class="fa fa-play"></i> play, <i class="fa fa-edit"></i> edit, or <i class="fa fa-plus"></i> add videos in the list.<br/>
    The delete action takes immediate effect, while any other changes are not saved until you click the "Save Changes" button.<br/>
    Use drag and drop on the video title to change the play order of the series.
</p>
<h2 style="margin-top: 20px;"><em><?= $trainingInfo['doc_title']; ?></em> by <?= $trainingInfo['author']; ?></h2>
<div class="form">

    <? $form=$this->beginWidget('CActiveForm', array('action'=>$postUrl,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'id'=>'edit-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <? foreach($models as $model) { ?>
        <div>
            <?= $form->errorSummary($model); ?>
        </div>
    <? } ?>

    <? if (!empty($errorMessages)) { ?>
        <div class="errorSummary">
            <p>The following errors occured while saving changes:</p>
            <ul>
                <? foreach($errorMessages as $errorMessage) { ?>
                    <li><?= $errorMessage; ?></li>
                <? } ?>
            </ul>
        </div>
    <? } ?>

    <ul id="sortable">
        <? $i=1; ?>
        <? foreach($models as $model) { ?>
            <? $this->renderPartial('_EditSeriesEntry',array('model'=>$model,'form'=>$form,'i'=>$i)); ?>
            <? $i++; ?>
        <? } ?>
    </ul>

    <div style="margin: 10px 0 10px 0;">
        <a href="" id="btnAdd" class="btn btn-default"><span class="fa fa-plus"></span> Add a Video</a>
        <?= CHtml::button('Save Changes',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
        <?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
    </div>

    <? $this->endWidget(); ?>

</div><!-- form -->
<? $newModel = new VideoTraining(); ?>
<ul id="blank" style="display: none;">
    <? $this->renderPartial('_EditSeriesEntry',array('model'=>$newModel,'form'=>$form,'i'=>0)); ?>
</ul>
<div id="frmEdit" style="display:none;">
    <div style="width: 100%;">
        <div style="margin: 10px;">
            <label for="_hidden_video_title">Video Title</label>
            <input style="width: 100%;" name="_hidden_video_title" id="_hidden_video_title" type="text" maxlength="256" value="x__title__x">
        </div>
        <div style="margin: 10px;">
            <label for="_hidden_video_code">Video Code - example: &lt;iframe src="..."&gt;&lt;/iframe&gt;</label>
            <textarea style="width: 100%;" rows="7" maxlength="512" name="_hidden_video_code" id="_hidden_video_code">x__code__x</textarea>
        </div>
    </div>
</div>

<script type="text/javascript">

    var __closeVideo = function(id) {
        var title = $('#VideoTraining_'+id+'_video_title').val().trim();
        var code = $('#VideoTraining_'+id+'_video_code').val().trim();
        if (title.length <= 0 && code.length <= 0) {
            $('#lineItem-' + id).remove();
        }
        return false;
    };

    var __cancelVideo = function(id) {
        var title = $('#VideoTraining_'+id+'_video_title').val().trim();
        var code = $('#VideoTraining_'+id+'_video_code').val().trim();
        var start = code.indexOf('<iframe');
        var stop = code.indexOf('</iframe>');
        if (title.length <= 0 && code.length <= 0) {
            w2popup.close();
        } else if (title.length <= 0 || code.length <= 0) {
            w2alert('Must save a video title and video code.');
        } else if (start < 0 || stop <= start+7) {
            if (start < 0) {
                w2alert('Video code must start with a valid &lt;iframe tag.');
            } else {
                w2alert('Video code must start with a valid &lt;iframe tag and end with a valid closing &lt;/iframe&gt; tag.');
            }
        } else {
            w2popup.close();
        }
        return false;
    };

    var __saveVideo = function(id) {
        var title = $('#popup_video_title').val().trim();
        var code = $('#popup_video_code').val().trim();
        var start = code.indexOf('<iframe');
        var stop = code.indexOf('</iframe>');
        if (title.length <= 0) {
            w2alert('Must provide a video title.');
        } else if (code.length <= 0) {
            w2alert('Must provide a video code.');
        } else if (start < 0 || stop <= start+7) {
            if (start < 0) {
                w2alert('Video code must start with a valid &lt;iframe tag.');
            } else {
                w2alert('Video code must start with a valid &lt;iframe tag and end with a valid closing &lt;/iframe&gt; tag.');
            }
        } else {
            $('#title-' + id).html(title);
            $('#VideoTraining_' + id + '_video_title').val(title);
            $('#VideoTraining_' + id + '_video_code').val(code);
            w2popup.close();
        }
        return false;
    };

    $(function(){

        var _next = <?= $i; ?>;

        $('input[type=us-date]').w2field('date');
        $("#btnSave").click(function(){
            var li = $('#sortable').children();
            if (li.length > 0) {
                $('#edit-form').submit();
            } else {
                w2alert('Please "Add a Video" first.');
            }
            return false;
        });

        $("#btnCancel").click(function(){
            window.location = "<?= $returnUrl; ?>";
            return false;
        });

        $("#btnAdd").click(function(){
            var blank = $('#blank').html();
            blank = blank.replace(/\[0\]/g,'['+_next+']');
            blank = blank.replace(/_0_/g,'_'+_next+'_');
            blank = blank.replace(/lineItem\-0/g,'lineItem-'+_next);
            blank = blank.replace(/btnDelete\-0/g,'btnDelete-'+_next);
            blank = blank.replace(/btnPlay\-0/g,'btnPlay-'+_next);
            blank = blank.replace(/btnEdit\-0/g,'btnEdit-'+_next);
            blank = blank.replace(/title\-0/g,'title-'+_next);
            var btnEditId = 'btnEdit-'+_next;
            _next++;
            $('#sortable').append(blank);
            setTimeout(function(){ $('#'+btnEditId).click(); }, 10);
            return false;
        });

        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }

        $('#sortable').on('click','.lnkEdit',function(){
            var id = $(this).attr('id');
            id =  id.replace('btnEdit-','');
            var w = $('body').width();
            w = (w > 600) ? 600 : w-50;
            var title = $('#VideoTraining_'+id+'_video_title').val();
            var code = $('#VideoTraining_'+id+'_video_code').val();
            var html = $('#frmEdit').html();
            html = html.replace(/_hidden_/g,'popup_');
            html = html.replace('x__title__x',htmlEntities(title));
            html = html.replace('x__code__x',htmlEntities(code));
            var popup = $().w2popup({
                width: w,     // width in px
                height: 330,     // height in px
                title: 'Edit Video',
                body: html,
                onClose: function(){__closeVideo(id);},
                buttons : '<button class="btn" onclick="__cancelVideo('+id+');">Cancel</button> '+
                    '<button class="btn" onclick="__saveVideo('+id+');">Save</button>'
            });
            return false;
        });

        $('#sortable').on('click','.lnkPlay',function(){
            var id = $(this).attr('id');
            id =  id.replace('btnPlay-','');
            var title = $('#VideoTraining_'+id+'_video_title').val().trim();
            var html = $('#VideoTraining_'+id+'_video_code').val().trim();
            html = html.replace('height=\"720\"', 'height="400"');
            html = html.replace('width=\"1280\"', 'width="500"');
            html = html.replace(/http\:/g, 'https:');
            var start = html.indexOf('<iframe');
            var stop = html.indexOf('</iframe>');
            if (start >= 0 && stop > start+7) {
                var iframe =  $($.parseHTML(html));
                if (iframe && iframe.length > 0) {
                    var w = iframe.attr('width');
                    w = (w) ? parseInt(w) : 500;
                    w = (w > 0) ? w+16 : 516;
                    var h = iframe.attr('height');
                    h = (h) ? parseInt(h) : 400;
                    h = (h > 0) ? h+44 : 444;
                    var popup = $('#play-'+id).w2popup({
                        width: w,     // width in px
                        height: h,     // height in px
                        title: title,
                        body: html
                    });
                } else {
                    w2alert('Could not play video because the iframe video code is missing or invalid.');
                }
            } else {
                w2alert('Could not play video because the video code is not a valid iframe.');
            }
            return false;
        });

        $('#sortable').on('click','.lnkDelete',function(){
            var recid = $(this).data('recid');
            var idname = $(this).data('idname');
            w2confirm('Are you sure you want to delete the video?<br/>If you answer Yes, the video is deleted immediately, without a need to click on the "Save Changes" button.', function(btn){
                if (btn=='Yes') {
                    $('#'+idname).remove();
                    if (recid != "0") {
                        $.ajax({
                            method: 'POST',
                            dataType: "json",
                            url: '<?= url('videotraining/delete'); ?>',
                            data: {id: recid},
                            success: function (response) {
                                if (response.status != 'success') {
                                    w2alert(response.message);
                                }
                            }
                        });
                    }
                }
            });
            return false;
        });
    });
</script>