<?
/* @var $this TrainingController */
/* @var $model Training */
/* @var $form CActiveForm */
?>

<h3>Edit Training Video</h3>

<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array('action'=>url('videotraining/edit',array('id'=>Obscure::encode($model->video_training_id))),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'id'=>'edit-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<? $trainings = VideoTraining_Ex::getTrainingTitleList(Utils::contextCompanyId(),2,Utils::isSuper()); ?>
		<? if (empty($trainings)) { ?>
			<p style="color:red;">Warning: You can not edit or create a video unless there are existing Training Titles to select from.</p>
		<? } ?>
		<label>Training Title that this video belongs to:</label>
		<select name="TrainingTitle" id="TrainingTitle">
			<? foreach ($trainings as $t) { ?>
				<option value="<?= $t['library'].Obscure::encode($t['id']); ?>"  <?= (($model->training_id==$t['id'] && $t['library']==0)|| ($model->training_library_id==$t['id'] && $t['library']==1)) ? 'selected="selected"' :'';?>><?= $t['doc_title']; ?></option>
			<? } ?>
		</select>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'video_title'); ?>
		<?= $form->textField($model,'video_title',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'video_title'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'sort_order'); ?>
		<?= $form->textField($model,'sort_order',array('style'=>"width: 10em;")); ?>
		<?= $form->error($model,'sort_order'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'video_code'); ?>
		<?= $form->textArea($model,'video_code',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'video_code'); ?>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('videotraining/list'); ?>";
			return false;
		});
	});
</script>