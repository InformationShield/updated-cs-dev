<?
/* @var $this VideotrainingController */
/* @var $model VideoTraining */
/* @var $form CActiveForm */
?>
<li id="lineItem-<?= $i; ?>">
    <div class="x_panel tile">
        <div class="x_title">
            <ul class="nav navbar-left panel_toolbox">
                <li>
                    <a class="lnkDelete" id="btnDelete-<?= $i; ?>" data-recid="<?= Obscure::encode($model->video_training_id); ?>" data-idname="lineItem-<?= $i; ?>" data-toggle="tooltip" data-placement="right" title="Delete"><i class="fa fa-close red"></i></a>
                </li>
                <li>
                    <a class="lnkPlay" id="btnPlay-<?= $i; ?>"  data-toggle="tooltip" data-placement="right" title="Play"><i class="fa fa-play"></i></a>
                </li>
                <li>
                    <a class="lnkEdit" id="btnEdit-<?= $i; ?>" data-toggle="tooltip" data-placement="right" title="Edit"><i class="fa fa-edit"></i></a>
                </li>
            </ul>
            <h2 id="title-<?= $i; ?>"><?= $model->video_title; ?></h2>
            <?= $form->hiddenField($model,'['.$i.']sort_order'); ?>
            <?= $form->hiddenField($model,'['.$i.']video_training_id'); ?>
            <?= $form->hiddenField($model,'['.$i.']video_title'); ?>
            <?= $form->hiddenField($model,'['.$i.']video_code'); ?>
        </div>
    </div>
</li>