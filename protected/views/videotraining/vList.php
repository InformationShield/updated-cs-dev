<? $isAuthor = Utils::isRoleAuthor(); ?>
<? $isSuperOwner = (Utils::isSuper() && Utils::isRoleOwner()); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'video_training_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'sort_order', caption: 'Order', size: '45px',  resizable: false, sortable: true },
        { field: 'video_title', caption: 'Video Title', size: '100%', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                    return '<div><a href="<?= url('videotraining/editseries'); ?>/vtid/'+record.recid+'">'+record.video_title+'</a></div>';
                <? } else { ?>
                    return '<div>'+record.video_title+'</div>';
                <? } ?>
            }
        },
        { field: 'training_title', caption: 'Training Title', size: '250px', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                    return '<div><a href="<?= url('videotraining/editseries'); ?>/vtid/'+record.recid+'">'+record.training_title+'</a></div>';
                <? } else { ?>
                    return '<div>'+record.training_title+'</div>';
                <? } ?>
            }
        },
    <? if ($isSuperOwner) { ?>
        { field: 'company', caption: 'Company', size: '100px', searchable: 'text', resizable: true, sortable: true },
    <? } ?>
        { field: 'action', caption: 'Actions', size: '110px', hideable: false, sortable: false,  resizable: false, hidden: <?= (!$isAuthor) ? 'true':'false'; ?>,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkEdit" href="<?= url('videotraining/edit'); ?>/id/'+record.recid+'">Edit</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                    +'</div>';
                <? } else { ?>
                var html = '<div></div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this VideotrainingController */
/* @var $model VideoTraining */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Manage Videos</h2>";
    if ($isAuthor) {
        $gmAjaxListGrid->intro = "<p>Click on the <strong>Video Title</strong> or <strong>Training Title</strong> to edit the entire video series at once. Or select <strong>Edit</strong> to modify a single video.</p>";
    }
    $gmAjaxListGrid->controller = "videotraining";
    $gmAjaxListGrid->recid = "video_training_id";
    $gmAjaxListGrid->allowAdd = $isAuthor;
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->allowDelete = $isAuthor;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
