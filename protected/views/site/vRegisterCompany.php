<?
/* @var $this CompanyController */
/* @var $company Company */
/* @var $form CActiveForm */
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
    tinymce.init({
		mode : "textareas",
		theme : "modern", menubar: false
	});
</script>

<h1>New Company Information</h1>

<div class="form">

<? $form=$this->beginWidget('CActiveForm', array(
	'action'=>url('site/registercompany',array('id'=>Obscure::encode($company->company_id))),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'id'=>'edit-form',
	'enableAjaxValidation'=>false,
)); ?>

		<p class="note">Fields with <span class="required">*</span> are required.</p>

		<?= $form->errorSummary($company); ?>

		<div class="row">
			<label for="Company_name" class="required">Your Company Name <span class="required">*</span></label>
			<?= $form->textField($company,'name',array('size'=>60,'maxlength'=>100,'style'=>"width: 99%;")); ?>
			<?= $form->error($company,'name'); ?>
		</div>

		<div class="row">
			<label for="Company_website">Company Website</label>
			<?= $form->textField($company,'website',array('size'=>80,'maxlength'=>256,'style'=>"width: 99%;")); ?>
			<?= $form->error($company,'website'); ?>
		</div>

		<div class="row">
			<label for="Company_logo">Company Logo</label>
			<? if (!empty($company->logo)) { ?>
				<img src='/files/logos/<?= $company->logo; ?>' height="58">
			<? } ?>
			<p class="note">
				Please choose an image with a height of 58 pixels, and with a width no greater than 175 pixels.
				Any image taller than 58 pixels will be resized to 58 pixels and the width set accordingly to match existing width/height ratio of the original image file.
				We also suggest that the logo image file have a transparent background. Preferably in PNG format.
			</p>
			<input type="file" name="logo" id="logo" value="<?= $company->logo; ?>" style="width: 99%; border: none; background: transparent;" accept="image/*" />
			<?= $form->error($company,'logo'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($company,'policy_contactname'); ?>
			<?= $form->textField($company,'policy_contactname',array('size'=>60,'maxlength'=>200,'style'=>"width: 99%;")); ?>
			<?= $form->error($company,'policy_contactname'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($company,'policy_email'); ?>
			<?= $form->textField($company,'policy_email',array('size'=>80,'maxlength'=>256,'style'=>"width: 99%;")); ?>
			<?= $form->error($company,'policy_email'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($company,'incident_contactname'); ?>
			<?= $form->textField($company,'incident_contactname',array('size'=>60,'maxlength'=>200,'style'=>"width: 99%;")); ?>
			<?= $form->error($company,'incident_contactname'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($company,'incident_email'); ?>
			<?= $form->textField($company,'incident_email',array('size'=>80,'maxlength'=>256,'style'=>"width: 99%;")); ?>
			<?= $form->error($company,'incident_email'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($company,'address'); ?>
			<?= $form->textField($company,'address',array('size'=>80,'maxlength'=>256,'style'=>"width: 99%;")); ?>
			<?= $form->error($company,'address'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($company,'description'); ?>
			<?= $form->textArea($company,'description',array('rows'=>'5','style'=>"width: 100%;")); ?>
			<?= $form->error($company,'description'); ?>
		</div>

		<div class="row buttons">
			<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
			<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
		</div>

	<? $this->endWidget(); ?>
</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('site/home'); ?>";
			return false;
		});
	});
</script>