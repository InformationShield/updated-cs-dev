<?
/* @var $this SiteController */
/* @var $model Company */
?>

<h1>Company Info</h1>

<div class="form">
    <? if (!empty($model->logo)) { ?>
        <div class="row" style="margin: 20px;">
            <img src='/files/logos/<?= $model->logo; ?>' width="320">
        </div>
    <? } ?>

    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('name')); ?>:</b>
        </div>
        <div class="col-md-9">
            <?= CHtml::encode($model->name); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('website')); ?>:</b>
        </div>
        <div class="col-md-9">
            <a href="<?= CHtml::encode($model->website); ?>"><?= CHtml::encode($model->website); ?></a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('license_level')); ?>:</b>
        </div>
        <div class="col-md-9">
            <?= CHtml::encode(mPermission::$Licenses[$model->license_level]); ?>
        </div>
    </div>
	<? if (getSessionVar('license_level') <= Cons::LIC_TRIAL) { ?>
		<div class="row">
			<div class="col-md-3">
				<b>Trial Expires On:</b>
			</div>
			<div class="col-md-9">
				<b><?= DateHelper::FormatDateTimeString($model->trial_expiry, 'm/d/Y'); ?></b>
			</div>
		</div>
	<? } ?>
    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('created_on')); ?>:</b>
        </div>
        <div class="col-md-9">
            <?= DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('policy_contactname')); ?>:</b>
        </div>
        <div class="col-md-9">
            <?= CHtml::encode($model->policy_contactname); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('policy_email')); ?>:</b>
        </div>
        <div class="col-md-9">
            <?= CHtml::encode($model->policy_email); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('incident_contactname')); ?>:</b>
        </div>
        <div class="col-md-9">
            <?= CHtml::encode($model->incident_contactname); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('incident_email')); ?>:</b>
        </div>
        <div class="col-md-9">
            <?= CHtml::encode($model->incident_email); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('address')); ?>:</b>
        </div>
        <div class="col-md-9">
            <?= CHtml::encode($model->address); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <b><?= CHtml::encode($model->getAttributeLabel('description')); ?>:</b>
        </div>
        <div class="col-md-9">
            <?= $model->description; ?>
        </div>
    </div>

    <? if ($editUrl && Utils::isRoleOwner()) { ?>
        <div class="row buttons" style="margin: 10px 10px">
            <a href="<?= $editUrl; ?>" class="btn btn-success">Edit Company Info</a>
        </div>
    <? } ?>

</div>
