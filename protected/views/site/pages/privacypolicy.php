        <h1>Information Shield Privacy Policy</h1>


        <p>Information Shield is committed to maintaining the trust
            and confidence of its customers and visitors to our web site. In particular,
            we want you to know that Information Shield is not in the business or
            selling, renting or trading email lists with other companies for marketing
            purposes. In this Privacy Policy we provide detailed information on when
            and why we collect your personal information, how we use it, the limited
            conditions under which we may disclose it to others and how we keep it
            secure.</p>
        <strong>Collection and Use of Personal Information</strong>
        <p>Personal information means any information that may be used to identify
            you, such as, your name, title, phone number, email address, or mailing
            address.</p>
        <p>In general, you can browse our web site without giving us any personal
            information. We use Google Analytics to analyze traffic to
            this web site in order to understand our customer's and visitor's needs
            and to continually improve our site for them. WebTrends Reporting Services
            collects only anonymous, aggregate statistics. For example, we do not
            tie a specific visit to a specific IP address. </p>
        <p>If you want to subscribe to one of our newsletters, we ask you to simply
            provide your business email address. In that case, we use your email address
            only in connection with providing you the newsletter and for no other
            purpose.</p>
        <p>Additional activities on our site require you to be registered, for
            example, to read a white paper or make a purchase.
            As part of the registration process, we ask you to create an Information
            Shield account where we collect additional personal information. We use
            that information for several general purposes: to tell you about products,
            seminars and services if you so request, to fulfill your request, to contact
            you if we need to obtain or provide additional information; to verify
            the accuracy of our records, to contact you regarding customer satisfaction
            surveys.</p>
        <p>Some activities require additional personal information within your Information
            Shield account. For example, to fulfill online orders for products we
            require you to enter credit card information, and you have the option
            to provide a separate shipping address. </p>
        <strong>Disclosure of Personal Information</strong>
        <p>Information Shield sometimes hires vendor companies to provide limited
            services on our behalf, including packaging, mailing and delivering items,
            sending postal mail, providing technical support, and processing registrations.
            We provide those companies only the information they need to deliver the
            service, and they are prohibited from using that information for any other
            purpose.</p>
        <p>Because Information Shield is a global company, our goal is to abide
            by the strictest interpretations of the privacy laws governing personal
            information for each region in which we operate. While data protection
            laws vary widely among different regions, Information Shield and its offices
            and subsidiaries are governed by this Privacy Policy and will use your
            personal information only as set forth in this policy. </p>
        <p>Information Shield may also disclose your personal information if required
            to do so by law or in the good faith belief that such action is necessary
            in connection with a sale, merger, transfer, exchange or other disposition
            (whether of assets, stock or otherwise) of all or a portion of a business
            of Information Shield and/or its subsidiaries or to (1) conform to legal
            requirements or comply with legal process served on Information Shield
            or this web site; (2) protect and defend the rights or property of Information
            Shield and this web site; (3) enforce its agreements with you, or (4)
            act in urgent circumstances to protect personal safety or the public.
        </p>
        <strong>Children and Privacy</strong>
        <p>Our web site does not offer information intended to attract children.
            Information Shield does not knowingly solicit personal information from
            children under the age of 13.</p>

        <strong>Security of Personal Information</strong>
        <p>When you submit any form requiring sensitive personal information, we
            use a secure server. The secure server software (SSL) helps protect your
            information as it travels over the Internet by encrypting that information
            before it is sent to us. We have also implemented industry-standard security
            mechanisms and procedures to protect data from loss, misuse and unauthorized
            access, disclosure, alteration and destruction.</p>

        <strong>Use of Cookies</strong>
        <p>A cookie is a small text file containing information that a web site
            transfers to your computer's hard disk for record-keeping purposes. Cookies
            allow us to analyze our site traffic patterns and to provide a user-friendly
            myInformation Shield registration and log-in process. A cookie can not
            give us access to your computer or to information beyond what you provide
            to us. Most web browsers automatically accept cookies; consult your browser's
            manual or online help if you want information on restricting or disabling
            the browser's handling of cookies. If you disable cookies, you can still
            view the publicly available information on our web site, but you will
            not be able to take advantage of offers or activities requiring an Information
            Shield account, which includes online sales and accessing customer-only
            support areas. If are using a shared computer and you have cookies turned
            on, be sure to log off when you finish.</p>

        <strong>Email List Security and Privacy</strong>
        <p>We use a third-party service provider, Constant Contact, to manage our opt-in
            email list.  Electronic mail you receive from us may also have information from
            the third-party service provider, including how to subscribe and un-subscribe from the
            list.  Please note that we use a "Safe-Unsubscribe" feature which prohibits us from
            adding your email address to our list once you have un-subscribed.  If you have
            any questions about our email newsletter practices, please contact us.</p>

        <strong>Links to Other Web Sites</strong>
        <p>Our web site contains links to information on other web sites. We do
            not have any control over these other web sites, and therefore we cannot
            be responsible for the protection and privacy of any information that
            you provide while visiting those sites. Those sites are not governed by
            this Privacy Policy, and if you have questions about how a site uses your
            information, consult that site's privacy statement.</p>

        <strong>Access to Your Personal Information/Updating Your Personal Information</strong>
        <p>You are entitled to access the personal information that we hold. Email
            your request to us at <a class="__cf_email__" href="/cdn-cgi/l/email-protection" data-cfemail="c9a8aaaaa6bca7bdba89a0a7afa6bba4a8bda0a6a7baa1a0aca5ade7aaa6a4e7">[email&#160;protected]</a><script data-cfhash='f9e31' type="text/javascript">
                /* <![CDATA[ */!function(){try{var t="currentScript"in document?document.currentScript:function(){for(var t=document.getElementsByTagName("script"),e=t.length;e--;)if(t[e].getAttribute("data-cfhash"))return t[e]}();if(t&&t.previousSibling){var e,r,n,i,c=t.previousSibling,a=c.getAttribute("data-cfemail");if(a){for(e="",r=parseInt(a.substr(0,2),16),n=2;a.length-n;n+=2)i=parseInt(a.substr(n,2),16)^r,e+=String.fromCharCode(i);e=document.createTextNode(e),c.parentNode.replaceChild(e,c)}t.parentNode.removeChild(t);}}catch(u){}}()/* ]]> */</script> You can also change
            this information on-line by visiting our on-line store.</p>
        <strong>Updating Your Personal Information</strong>
        <p>If you have an Information Shield account, you can update or correct
            your information online by visiting our Information Shield Account Management
            page or by emailing us at <a class="__cf_email__" href="/cdn-cgi/l/email-protection" data-cfemail="3e5f5d5d514b504a4d7e575058514c535f4a5751504d56575b525a105d515310">[email&#160;protected]</a><script data-cfhash='f9e31' type="text/javascript">
                /* <![CDATA[ */!function(){try{var t="currentScript"in document?document.currentScript:function(){for(var t=document.getElementsByTagName("script"),e=t.length;e--;)if(t[e].getAttribute("data-cfhash"))return t[e]}();if(t&&t.previousSibling){var e,r,n,i,c=t.previousSibling,a=c.getAttribute("data-cfemail");if(a){for(e="",r=parseInt(a.substr(0,2),16),n=2;a.length-n;n+=2)i=parseInt(a.substr(n,2),16)^r,e+=String.fromCharCode(i);e=document.createTextNode(e),c.parentNode.replaceChild(e,c)}t.parentNode.removeChild(t);}}catch(u){}}()/* ]]> */</script> </p>
        <strong>Unsubscribing</strong>
        <p>If you have set up an Information Shield account, you may change your
            preferences at any time by following the instructions provided in the
            Information Shield account pages.</p>
        <p>If you do not have an Information Shield account, you may unsubscribe
            at any time by sending an email request to <a class="__cf_email__" href="/cdn-cgi/l/email-protection" data-cfemail="b0d1d3d3dfc5dec4c3f0d9ded6dfc2ddd1c4d9dfdec3d8d9d5dcd49ed3dfdd9e">[email&#160;protected]</a><script data-cfhash='f9e31' type="text/javascript">
                /* <![CDATA[ */!function(){try{var t="currentScript"in document?document.currentScript:function(){for(var t=document.getElementsByTagName("script"),e=t.length;e--;)if(t[e].getAttribute("data-cfhash"))return t[e]}();if(t&&t.previousSibling){var e,r,n,i,c=t.previousSibling,a=c.getAttribute("data-cfemail");if(a){for(e="",r=parseInt(a.substr(0,2),16),n=2;a.length-n;n+=2)i=parseInt(a.substr(n,2),16)^r,e+=String.fromCharCode(i);e=document.createTextNode(e),c.parentNode.replaceChild(e,c)}t.parentNode.removeChild(t);}}catch(u){}}()/* ]]> */</script></p>
        <strong>Your Consent</strong>
        <p>By using our web site, you consent to the collection and use of the information
            you provide to us as outlined in this Privacy Policy. We may change this
            Privacy Policy from time to time and without notice. If we change our
            Privacy Policy, we will publish those changes on this page.</p>
        <p>If you have any questions or concerns about our collection, use or disclosure
            of your personal information, please email us at sales ~ @ ~ informationshield.com. (Please
            ignore the ~ when using this email account.)</p>
        <p>This document was last updated May 1, 2011.</p>
