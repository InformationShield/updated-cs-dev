<?
	$this->pageTitle=Yii::app()->name . ' - Message';
	if (!isset($title)) {
		$title = Yii::app()->name;
	}
	if (!isset($message)) {
		$message = null;
	}
?>
    <div>
        <div class="container-fluid" style="margin: 20px;">
            <div class="row">
                <? if ($messageId == Cons::MSG_CUSTOM) { ?>
                    <h3><?= $title; ?></h3>
                    <p><?= $message; ?></p>
                <? } else if ($messageId == Cons::MSG_ERROR) { ?>
                    <h3><i class="fa fa-warning"></i> Error</h3>
                    <p>
                        We could not successfully complete this request, due to an error.<br>
                        Please <a href="<?= url('site/contact'); ?>" style="color: blue;">contact</a> us to resolve this for you.
                    </p>
                <? } else if ($messageId == Cons::MSG_COMPANY_MAX) { ?>
                    <h3><i class="fa fa-warning"></i> Reached Maximum Company Ownership</h3>
                    <p>
                        You can current only register up to <?= $maxCompanyOwnership; ?> companies with a single account.<br>
                        Please <a href="<?= url('site/contact'); ?>" style="color: blue;">contact</a> us to increase this limit.
                    </p>
                <? } else if ($messageId == Cons::MSG_COMPANY_MISSING) { ?>
                    <h3><i class="fa fa-warning"></i> Missing Company Information</h3>
                    <p>
                        We could not access your company information to complete this request.<br>
                        Please <a href="<?= url('site/contact'); ?>" style="color: blue;">contact</a> us to resolve this for you.
                    </p>
                <? } else if ($messageId == Cons::MSG_ACCESS_DENIED) { ?>
                    <h3><i class="fa fa-ban"></i> Access Denied</h3>
                    <p>
                        Your user account does not appear to have permissions for this feature.<br>
                        Please contact your company administrator for higher role if you need access.
                    </p>
                <? } else if ($messageId == Cons::MSG_LICENSE_DENIED) { ?>
                    <h3><i class="fa fa-warning"></i> License Level Authorization</h3>
                    <p>
                        Your company's license level is not authorized for this feature.<br>
						Please upgrade your license by <a href="<?= url('site/contact'); ?>" style="color: blue;">contacting</a> us.
                    </p>
                <? } else if ($messageId == Cons::MSG_ROLE_DENIED) { ?>
                    <h3><i class="fa fa-warning"></i> Role Authorization</h3>
                    <p>
						Your role is not authorized to perform this action.
                    </p>
				<? } else if (isset($message)) { ?>
                    <?= $title; ?>
                    <?= $message; ?>
                <? } else { ?>
					<?= $messageId; ?>
				<? } ?>
        </div>
    </div>
</div>
