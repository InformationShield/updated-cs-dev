<?php
/* @var $this SiteController */
/* @var $model mScoring */
?>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>

<div>
    <div class="row-fluid">
        <div class="left">
            <h3>Overview of <em><?= CHtml::encode(Utils::contextCompanyName()); ?></em></h3>
            <h4>Your role is <em><?= mPermission::companyRoleName(Utils::contextCompanyRole()); ?></em>.</h4>
        </div>
        <? if (Utils::contextCompanyCount() > 1) { ?>
            <div class="right">
                <div class="left">
                    <? $userCompanies = Company_Ex::getUserCompanies(userId()); ?>
                    <? $form=$this->beginWidget('CActiveForm', array('action'=>url('site/accountoptions'),
                        'id'=>'site-account-options-form',
                        'enableAjaxValidation'=>false,
                    )); ?>
                    <select name="context_company_id" id="context_company_id" style="display: none; width: 100%;">
                        <? foreach ($userCompanies as $company) { ?>
                            <option value="<?= $company['company_id']; ?>" <?= (Utils::contextCompanyId() == $company['company_id']) ? 'selected="selected"' : ''; ?>><?= $company['name']; ?></option>
                        <? } ?>
                    </select>
                    <? $this->endWidget(); ?>
                </div>
                <div class="right">
                    &nbsp;&nbsp;<a class="lnkLogin" href=""><i class="fa fa-building purple" style="font-size: 16px;"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
        <? } ?>
        <div class="clearfix"></div>
    </div>

    <? if (false && isset($messages) && count($messages) > 0) { // turn off feature for now, Dave does not like UI ?>
        <div class="row-fluid">
            <? $this->widget('MessageWidget',array('messages'=> $messages)); ?>
        </div>
        <div class="clearfix"></div>
    <? } ?>

    <div class="row-fluid">
        <div class="col-md-6">
            <div id="divChart1" class="x_panel">
                <div style="width: 100%; height: 300px; text-align: center;">
                    <img src="<?= url("images/ajax-loader.gif"); ?>" style="margin-top: 150px;"/>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div id="divChart2" class="x_panel">
                <div style="width: 100%; height: 300px; text-align: center;">
                    <img src="<?= url("images/ajax-loader.gif"); ?>" style="margin-top: 150px;"/>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="col-md-6">
            <div id="divChart3" class="x_panel">
                <div style="width: 100%; height: 300px; text-align: center;">
                    <img src="<?= url("images/ajax-loader.gif"); ?>" style="margin-top: 150px;"/>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div id="divChart4" class="x_panel">
                <div style="width: 100%; height: 300px; text-align: center;">
                    <img src="<?= url("images/ajax-loader.gif"); ?>" style="margin-top: 150px;"/>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<script type="text/javascript">
    $(function(){
        function getChart(selector,url) {
            $.ajax({
                method: 'POST',
                dataType: "html",
                url: url,
                success: function(response){
                    if (response && response.length > 10) {
                        $(selector).html(response);
                    } else {
                        w2alert('Failed to load chart.');
                    }
                },
                error: function (xhr, status) {
                    alert("Sorry, failed to load chart.");
                }
            });
        }
        FusionCharts.ready(function() {
            getChart("#divChart1",'<?= url('compliance/getcontrolsummary/view/_vHomeControlSummary'); ?>');
            getChart("#divChart2",'<?= url('compliance/getroles/view/_vHomeRoles'); ?>');
            getChart("#divChart3",'<?= url('compliance/getpolicysummary/view/_vHomePolicySummary'); ?>');
            getChart("#divChart4",'<?= url('compliance/getpolicytrackingsummary/view/_vHomePolicyTrackingSummary'); ?>');
        });
        $('.lnkLogin').click(function(){
            $('#context_company_id').toggle();
            return false;
        });
        $('#context_company_id').change(function(){
            var val = $(this).val();
            if (val != '<?= Utils::contextCompanyId(); ?>') {
                $('#site-account-options-form').submit();
            }
            return false;
        });
    });
</script>