    <div>
        <div class="container-fluid" style="margin: 20px 0 20px 0; ">
            <h2>Information Shield - ComplianceShield &trade;</h2>
            <h2>End User License Agreement</h2>
            <p>
                <strong>License:</strong> Information Shield ("Licensor") grants the undersigned subscribing organization ("Subscriber") a
                non-exclusive license to copy, modify and distribute pre- written policies and other resources found within the
                ComplianceShield service ("Content") to Subscriber's employees and contractors in hardcopy or in electronic
                form (e.g., as a .pdf or HTML file) subject to the terms and conditions of this license agreement ("Agreement.")
                This material is for internal use only at licensee organizations, and may not be remarketed or redistributed.
                Consultants, VARs, OEMs, and other third parties using this material on behalf of another organization must
                purchase separate licenses for each organization where this material is used.
                <br/><br/>
                <strong>Term:</strong> Subscriber's subscription commences on the date of the first payment ("Agreement Date") and terminates on
                the last day of the month after the expiration of one year from the Agreement Date, unless terminated earlier as
                provided herein. Notwithstanding any inconsistent or conflicting provision herein, if Subscriber fails to pay a
                subscription fee or fails to comply with any material term or condition herein, Licensor may, at its option,
                without prejudice to any other rights, terminate this Agreement.
                <br/><br/>
                <strong>Delivery of Content Updates:</strong> Subscriber will receive access to content via the ComplianceShield web site.
                Each Subscriber will be granted a unique userid and password to access the site. The userid must not be shared
                outside of the organization. Subsequent content updates will be delivered electronically on a periodic basis.
                Subscribers will receive notifications of relative product updates via email delivered to the email address on
                file for the Subscriber. Subscriber is responsible for configuring electronic mail filtering programs within the
                Subscribers network to permit receipt.
                <br/><br/>
                <strong>Disclaimer:</strong> The information ("Content") in this product is provided as a service to the business community. At the
                time of its preparation, the policies and other materials found in this guide are believed to be current, relevant,
                useful, and appropriate for organizations concerned about information security. Such information is nonetheless
                subject to change without notice. Although every reasonable effort has been made to ensure the accuracy,
                completeness, and relevance of the information contained herein, the author(s) and the publisher
                (Information Shield, Inc.), including their affiliates, cannot be responsible for any errors and omissions, or any
                party's interpretations or applications of the ideas or words contained therein. Readers preparing policies and
                related materials are strongly encouraged to seek the advice and guidance of lawyers trained in information
                technology issues and technical information security specialists. Neither the author nor Information Shield Inc.,
                nor any of their affiliates, make any warranties, guarantees, or representations about the fitness for any purpose
                of the information security policies and related material found in this guide.
                <br/><br/>
                <strong>Infringement Claims:</strong> Licensor will defend, indemnify and hold harmless Subscriber against any action based on a
                claim that ComplianceShield infringes upon any patent or copyright, provided that Subscriber promptly notifies
                Licensor in writing of such claim, Licensor has sole control over the defense of the claim and/or any settlement
                negotiations, and Subscriber cooperates fully in the defense of the claim. At its option, Licensor will obtain
                the right to continue using ComplianceShield, repair it, replace it or terminate this license in exchange for a
                pro-rated refund. <strong>The foregoing states Licensor's entire obligation to you for infringement claims.</strong>
                <br/><br/>
                <strong><u>Limitation of Liability:</u> LICENSOR'S ENTIRE LIABILITY AND SUBSCRIBER'S EXCLUSIVE REMEDY UNDER THIS LIMITED WARRANTY
                SHALL BE, AT LICENSOR'S OPTION, EITHER (1) RETURN OF THE PRICE PAID, OR (2) EXTENSION OF SUBCRIPTION TERM.
                TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, LICENSOR DISCLAIMS ALL OTHER WARRANTIES, EXPRESS OR IMPLIED,
                INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
                LICENSOR WILL NOT BE LIABLE FOR ANY SPECIAL, INCIDENTAL, OR INDIRECT DAMAGES, OR FOR ANY ECONOMIC OR CONSEQUENTIAL
                DAMAGES (INCLUDING WITHOUT LIMITATION LOST PROFITS OR LOST SAVINGS), EVEN IF LICENSOR HAS BEEN ADVISED OF THE
                POSSIBILITY OF SUCH DAMAGES. REGARDLESS OF THE BASIS ON WHICH A CLAIM FOR DAMAGES AGAINST LICENSOR IS MADE,
                LICENSOR'S ENTIRE LIABILITY TO SUBSCRIBER IS LIMITED TO NO MORE THAN THE AMOUNT PAID FOR A ONE-YEAR SUBSCRIPTION
                TO COMPLIANCESHIELD &trade;. LICENSOR WILL NOT BE LIABLE TO SUBSCRIBER FOR ANY THIRD PARTY CLAIMS OR DAMAGE TO OR
                LOSS OF RECORDS OR DATA.</strong>
                <br/><br/>
                <strong>Neither Licensor nor Subscriber shall be liable for any delay or non-performance due to governmental regulations,
                hostile actions, weather, acts of God, or any other cause beyond the affected party's reasonable control.</strong>
                <br/><br/>
                <strong>This document gives you specific legal rights. You may have others which vary by jurisdiction. In some
                jurisdictions exclusion or limitation of consequential or incidental damages, and/or exclusion or limitation of
                liability for personal injury, and/or limitations on duration of an implied warranty may not apply to you.
                This Agreement does not affect any statutory rights of consumers that cannot be waived or limited by contract.</strong>
                <br/><br/>
                <strong>Choice of Law:</strong> This Agreement shall be construed and interpreted according to the law of the State of Texas and
                all claims in connection with this Agreement shall be brought in a court of competent jurisdiction in Houston, Texas.
                <br/><br/>
                <strong>Entire Agreement:</strong> This Agreement constitutes the entire agreement of LICENSOR and Licensee regarding the
                subscription, and there are no other agreements, representations, terms or conditions. <strong>Any modification,
                addition or condition of acceptance of the terms and conditions of this Agreement (e.g. in a purchase order)
                is void, unless signed by Licensor.</strong> This Agreement may not be assigned or transferred, and Content may not be
                sublicensed without Licensor's prior written authorization.
            </p>
        </div>
    </div>