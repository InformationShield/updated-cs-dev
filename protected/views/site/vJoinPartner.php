<?
/* @var $this SiteController */
/* @var $model Company */
?>
<?php $this->pageTitle=Yii::app()->name . ' - Join';
?>
<div class="x_panel">
    <div style="margin: 20px;">
        <h2>In partnership with</h2>
        <div>
            <img src="<?= $partnerLogo; ?>">
        </div>

        <div class="form" style="margin-top: 10px;">

            <? $form=$this->beginWidget('CActiveForm', array(
                'action'=>url('site/join',array('partner'=>$partnerId)),
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                'id'=>'edit-form',
                'enableAjaxValidation'=>false,
            )); ?>

            <p>
                Welcome to ComplianceShield.  Please finish setting up your account by entering your first and last name and your company details.
            </p>
            
            <?php echo $form->errorSummary(array($model,$profile)); ?>

            <div class="row">
                <label for="Profile_first_name">First Name</label>
                <input size="40" maxlength="255" style="width: 100%;" name="Profile[first_name]" id="Profile_first_name" type="text" value="<?= $profile->first_name; ?>" />
            </div>

            <div class="row">
                <label for="Profile_last_name">Last Name</label>
                <input size="40" maxlength="255" style="width: 100%;" name="Profile[last_name]" id="Profile_last_name" type="text" value="<?= $profile->last_name; ?>" />
            </div>

            <div class="row">
                <label for="Company_name" class="required">Your Company Name</label>
                <input size="40" maxlength="100" style="width: 100%;" name="Company[name]" id="Company_name" type="text" value="<?= $model->name; ?>" />
            </div>

            <div class="row">
                <label for="Company_website">Company Website</label>
                <input size="40" maxlength="256" style="width: 100%;" name="Company[website]" id="Company_website" type="text" value="<?= $model->website; ?>" />
            </div>

            <div class="row buttons">
                <?= CHtml::button('Join',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#btnSave").click(function(){
            $('#edit-form').submit();
            return false;
        });
    });
</script>
