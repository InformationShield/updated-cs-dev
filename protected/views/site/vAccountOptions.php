<?
/* @var $this SiteController */
/* @var $model mCompanyContext */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Account Options';
$this->breadcrumbs=array(
    'Account Options',
);
$context_company_count = count(Company_Ex::getUserCompanies(userId()));
?>

<h1><i class="fa fa-wrench"></i> Account Options</h1>

<? if ($context_company_count > 0) { ?>
    <p>
        Your account is associated with an existing Company User account.
        If your account info is associated with multiple companies then you may
        choose which company to use as your current context.
        If you do not see a company listed that you are supposed to have access to,
        then you should contact your Company Administrator
        for this site and ask to have your account email address ( <em><?= userEmail(); ?></em> )
        associated with the Company User list.
    </p>
    <div class="form">

        <? $form=$this->beginWidget('CActiveForm', array('action'=>url('site/accountoptions'),
            'id'=>'site-account-options-form',
            'enableAjaxValidation'=>false,
        )); ?>

        <div class="row">
            <? $userCompanies = Company_Ex::getUserCompanies(userId()); ?>
            <label for="context_company_id">Select a company:</label>
            <select name="context_company_id" id="context_company_id" style="width: 80%;">
                <? foreach ($userCompanies as $company) { ?>
                    <option value="<?= $company['company_id']; ?>" <?= (Utils::contextCompanyId() == $company['company_id']) ? 'selected="selected"' : ''; ?>><?= $company['name']; ?></option>
                <? } ?>
            </select>
        </div>

        <div class="row buttons">
            <?= CHtml::button('Change Company Context',array('id'=>"btnSubmit",'class'=>"btn btn-success")); ?>
        </div>

        <? $this->endWidget(); ?>

    </div><!-- form -->
<? } else { ?>
    <p>
        Your account is not associated with an existing Company User.
        You should contact your Company Administrator for this site and ask to have
        your account email address ( <em><?= userEmail(); ?></em> ) associated with the Company User list.
        You may need to create a new account using an email address that was assigned in the Company User list.
    </p>
<? } ?>
<br/>
<br/>

<? if ($context_company_count == 0 || mPermission::checkLicenseRole(null, Cons::LIC_LEVEL3, Cons::ROLE_ADMIN, 'silent')) { ?>
	<h3>Register New Company</h3>
	<p>
		If you are the company administrator then you can click the button below to register your company.
	</p>
	<p>
		<a href="<?= Yii::app()->createAbsoluteUrl('site/registercompany'); ?>" class="btn btn-success">Register New Company</a>
	</p>
<? } ?>

<script type="text/javascript">
    $(function(){
        $("#btnSubmit").click(function(){
            $('#site-account-options-form').submit();
            return false;
        });
    });
</script>