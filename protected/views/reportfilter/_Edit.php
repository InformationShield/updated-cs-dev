<?
/* @var $this QuizController */
/* @var $form CActiveForm */
?>
<li id="lineItemQuestions-<?= $iQuestions; ?>">
    <div class="x_panel tile">
        <div class="x_title">
            <ul class="nav navbar-left panel_toolbox">
                <li>
                    <a class="lnkDeleteQuestions" id="btnDeleteQuestions-<?= $iQuestions; ?>" data-questionid="<?= $question['quiz_question_id']; ?>" data-idname="lineItemQuestions-<?= $iQuestions; ?>" data-toggle="tooltip" data-placement="right" title="Delete"><i class="fa fa-close red"></i></a>
                </li>
                <li>
                    <a class="lnkEditQuestions" id="btnEditQuestions-<?= $iQuestions; ?>" data-toggle="tooltip" data-placement="right" title="Edit"><i class="fa fa-edit"></i></a>
                </li>
            </ul>
            <div class="handle" style="float: left; width: auto;">
                <h1 id="questiontitle-<?= $iQuestions; ?>"><?= $question['question']; ?></h1>
            </div>
        </div>
        <div class="x_content" id="frmEdit-<?= $iQuestions; ?>" style="display:none; margin: 10px;">
            <input type="hidden" name="QuizQuestion[<?= $iQuestions; ?>][quiz_question_id]" id="QuizQuestion_<?= $iQuestions; ?>_quiz_question_id" value="<?= $question['quiz_question_id']; ?>">
            <div style="margin-right: 20px;">
                <label for="QuizQuestion_<?= $iQuestions; ?>_question">Question:</label>
                <input class="inpQuestion" style="width: 100%;" name="QuizQuestion[<?= $iQuestions; ?>][question]" id="QuizQuestion_<?= $iQuestions; ?>_question" type="text" maxlength="512" value="<?= $question['question']; ?>">
            </div>
            <div style="margin-right: 20px;">
                <label for="QuizQuestion_<?= $iQuestions; ?>_hint">Hint</label>
                <input style="width: 100%;" name="QuizQuestion[<?= $iQuestions; ?>][hint]" id="QuizQuestion_<?= $iQuestions; ?>_hint" type="text" maxlength="512" value="<?= $question['hint']; ?>">
            </div>
            <div style="margin-right: 20px;">
                <label for="QuizQuestion_<?= $iQuestions; ?>_category_name">Category</label>
                <input style="width: 100%;" name="QuizQuestion[<?= $iQuestions; ?>][category_name]" id="QuizQuestion_<?= $iQuestions; ?>_category_name" type="text" maxlength="80" value="<?= $question['category_name']; ?>">
            </div>
            <div style="margin-right: 20px;">
                <label for="QuizQuestion_<?= $iQuestions; ?>_score">Score</label>
                <input size="6" name="QuizQuestion[<?= $iQuestions; ?>][score]" id="QuizQuestion_<?= $iQuestions; ?>_score" type="text" maxlength="6" value="<?= $question['score']; ?>">
            </div>
            <div id="qtype-<?= $iQuestions; ?>" style="margin: 0 20px 10px 0;">
                <label>Type of Question</label>
                <select class="qtypeselect" name="QuizQuestion[<?= $iQuestions; ?>][question_type]" id="QuizQuestion_<?= $iQuestions; ?>_question_type">
                    <option <?= ($question['question_type']<=1) ? 'selected':'';?> value="1">True/False</option>
                    <option <?= ($question['question_type']==2) ? 'selected':'';?> value="2">Yes/No</option>
                    <option <?= ($question['question_type']>=3) ? 'selected':'';?> value="3">Multiple Choice</option>
                </select>
            </div>
            <div id="qtype1-<?= $iQuestions; ?>" class="qtypeanswer" style="display: <?= ($question['question_type']<=1) ? 'block':'none'; ?>">
                <div style="margin: 0 20px 10px 0;">
                    <label>Correct Answer</label>
                    <select name="QuizQuestion[<?= $iQuestions; ?>][correct_answer_type1]" id="QuizQuestion_<?= $iQuestions; ?>_correct_answer_type1">
                        <option <?= ($question['correct_answer']<=1) ? 'selected':'';?> value="1">True</option>
                        <option <?= ($question['correct_answer']==2) ? 'selected':'';?> value="2">False</option>
                    </select>
                </div>
            </div>
            <div id="qtype2-<?= $iQuestions; ?>" class="qtypeanswer" style="display: <?= ($question['question_type']==2) ? 'block':'none'; ?>">
                <div style="margin: 0 20px 10px 0;">
                    <label>Correct Answer</label>
                    <select name="QuizQuestion[<?= $iQuestions; ?>][correct_answer_type2]" id="QuizQuestion_<?= $iQuestions; ?>_correct_answer_type2">
                        <option <?= ($question['correct_answer']<=1) ? 'selected':'';?> value="1">Yes</option>
                        <option <?= ($question['correct_answer']==2) ? 'selected':'';?> value="2">No</option>
                    </select>
                </div>
            </div>
            <div id="qtype3-<?= $iQuestions; ?>" class="qtypeanswer" style="display: <?= ($question['question_type']>=3) ? 'block':'none'; ?>">
                <div style="margin-right: 20px;">
                    <label for="QuizQuestion_<?= $iQuestions; ?>_correct_answer">Correct Answer</label>
                    <select name="QuizQuestion[<?= $iQuestions; ?>][correct_answer]" id="QuizQuestion_<?= $iQuestions; ?>_correct_answer">
                        <? for($ans=1; $ans <= 5; $ans++) { ?>
                            <option <?= ($question['correct_answer']==$ans) ? 'selected':'';?> value="<?= $ans; ?>"><?= chr(64+$ans); ?></option>
                        <? } ?>
                    </select>
                </div>
                <div style="margin-right: 20px;">
                    <label for="QuizQuestion_<?= $iQuestions; ?>_answer1">Answer A</label>
                    <input style="width: 100%;" name="QuizQuestion[<?= $iQuestions; ?>][answer1]" id="QuizQuestion_<?= $iQuestions; ?>_answer1" type="text" maxlength="80" value="<?= $question['answer1']; ?>">
                </div>
                <div style="margin-right: 20px;">
                    <label for="QuizQuestion_<?= $iQuestions; ?>_answer2">Answer B</label>
                    <input style="width: 100%;" name="QuizQuestion[<?= $iQuestions; ?>][answer2]" id="QuizQuestion_<?= $iQuestions; ?>_answer2" type="text" maxlength="80" value="<?= $question['answer2']; ?>">
                </div>
                <div style="margin-right: 20px;">
                    <label for="QuizQuestion_<?= $iQuestions; ?>_answer3">Answer C</label>
                    <input style="width: 100%;" name="QuizQuestion[<?= $iQuestions; ?>][answer3]" id="QuizQuestion_<?= $iQuestions; ?>_answer3" type="text" maxlength="80" value="<?= $question['answer3']; ?>">
                </div>
                <div style="margin-right: 20px;">
                    <label for="QuizQuestion_<?= $iQuestions; ?>_answer4">Answer D</label>
                    <input style="width: 100%;" name="QuizQuestion[<?= $iQuestions; ?>][answer4]" id="QuizQuestion_<?= $iQuestions; ?>_answer4" type="text" maxlength="80" value="<?= $question['answer4']; ?>">
                </div>
                <div style="margin-right: 20px;">
                    <label for="QuizQuestion_<?= $iQuestions; ?>_answer5">Answer E</label>
                    <input style="width: 100%;" name="QuizQuestion[<?= $iQuestions; ?>][answer5]" id="QuizQuestion_<?= $iQuestions; ?>_answer5" type="text" maxlength="80" value="<?= $question['answer5']; ?>">
                </div>
            </div>
        </div>
    </div>
</li>
