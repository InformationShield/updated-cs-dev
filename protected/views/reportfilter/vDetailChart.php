<?php
/* @var $this ComplianceController */
/* @var $mReportFilter mReportFilter */
?>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>
<h2 class="left"><strong><?= $mReportFilter->filter_name; ?></strong> controls with <strong><?= strtolower(ControlStatus_Ex::getStatusName($status_id)); ?></strong> status</h2>
<div class="right">
    Change status:
    <select name="control_status_id" id="control_status_id">
        <? $controlstatuses = ControlStatus_Ex::getAllBySortOrder(); ?>
        <? foreach ($controlstatuses as $controlstatus) { ?>
            <option value="<?= $controlstatus['control_status_id']; ?>" <?= ($status_id==$controlstatus['control_status_id']) ? 'selected="selected"' :'';?>><?= $controlstatus['control_status_name']; ?></option>
        <? } ?>
    </select>
</div>
<div class="clearfix"></div>

<div id="divChart" class="x_panel">
    <div style="width: 100%; height: 665px; text-align: center;">
        <img src="/images/ajax-loader.gif" style="margin-top: 300px;"/>
    </div>
</div>

<script type="text/javascript">
    function getChart() {
        $.ajax({
            method: 'POST',
            dataType: "html",
            url: '<?= url('reportfilter/getdetailchart', array('id'=>$mReportFilter->control_filter_id, 'status'=>$status_id)); ?>',
            success: function(response){
                if (response && response.length > 10) {
                    $("#divChart").html(response);
                } else {
                    w2alert('Failed to load chart.');
                }
            },
            error: function (xhr, status) {
                alert("Sorry, failed to load chart.");
            }
        });
    }
    FusionCharts.ready(function() {
        getChart();
    });
    $(function(){
       $('#control_status_id').change(function(){
           var val = $(this).val();
           if (val != '<?= $status_id; ?>') {
               window.location = "<?= url('reportfilter/detailchart/id/'.$mReportFilter->control_filter_id).'/status/'; ?>" + val;
           }
           return false;
       });
    });
</script>