<?
/* @var $this ControlbaselineController */
/* @var $model mReportFilter */
/* @var $form CActiveForm */
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
		mode : "textareas",
		theme : "modern", menubar: false
	});
</script>

<h3>Edit Report Filter</h3>

<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array('action'=>url('reportfilter/edit',array('id'=>Obscure::encode($model->id))),
		'id'=>'edit-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// See class documentation of CActiveForm for details on this,
		// you need to use the performAjaxValidation()-method described there.
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>
	<? $isNew = $model->isNewRecord; ?>

	<div class="row">
		<div class="col-md-12">
			<label for="mReportFilter[filter_name]">Filter Name <span class="required">*</span></label>
			<input type="text" id="mReportFilter[filter_name]" name="mReportFilter[filter_name]" style="width: 99%;" value="<?= $model->filter_name; ?>" >
			<?= $form->error($model,'filter_name'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<label for="mReportFilter[filter_desc]">Filter Description </label>
			<input type="text" id="mReportFilter[filter_desc]" name="mReportFilter[filter_desc]" style="width: 99%;" value="<?= $model->filter_desc; ?>" >
			<?= $form->error($model,'filter_desc'); ?>
		</div>
	</div>

	<hr>

	<p class="note">
		Please enter filter criteria belowy for the control report.
	</p>

	<div class="row">
		<div class="col-md-12">
			<label for="mReportFilter[reference]">Reference contains: </label>
			<?= $form->textField($model,'reference',array('style'=>"width: 99%;")); ?>
			<?= $form->error($model,'reference'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<label for="mReportFilter[label1]">Label1 contains: </label>
			<?= $form->textField($model,'label1',array('style'=>"width: 99%;")); ?>
			<?= $form->error($model,'label1'); ?>
		</div>
		<div class="col-md-6">
			<label for="mReportFilter[label2]">Label2 contains: </label>
			<?= $form->textField($model,'label2',array('style'=>"width: 98%;")); ?>
			<?= $form->error($model,'label2'); ?>
		</div>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
$(function(){
	$("#btnSave").click( function() {
		$('#edit-form').submit();
		return false;
	});

	$("#btnCancel").click( function() {
		window.location = "<?= url('reportfilter/controlsummary'); ?>";
		return false;
	});
});
</script>