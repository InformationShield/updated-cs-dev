<?php
/* @var $this ComplianceController */
/* @var $model mScoring */
?>
<? if (empty($model->control_status_sums)) { ?>
	<div style="margin: 10px 0 10px 10px;">
		<h4>Report Filter: <span id="spnTitle"><?= $model->filter_name; ?></span></h4>
		<h5>No matching controls have been found.</h5>
	</div>
    <div style="margin: 30px 20px 30px 20px;">
        <p>
			No controls were found matching the selected report filter,
            or you have not yet run the &nbsp;<a href="<?= url('wizard/control'); ?>" class="btn btn-success">Control Wizard</a>
        </p>
    </div>
<? } else { ?>
	<div style="margin: 10px 0 10px 10px; text-align: left;">
		<h3><span id="spnTitle"><?= $model->filter_name; ?></span></h3>
	</div>
	<div style="padding: 1.25em; background-color: #fff;">
		<h4>Baseline2: <?= Baseline_Ex::getCurrentBaselineName(); ?></h4>
	</div>

    <div id="divControlSummaryChart">
        <img src="/images/ajax-loader.gif" style="margin: 250px 0 250px 100px;" />
    </div>

    <script type="text/javascript">
        var controlSummaryChart = new FusionCharts({
            type: 'pie3D',
            renderAt: 'divControlSummaryChart',
            width: '100%',
            height: '600',
            dataFormat: 'json',
            dataSource:  {
                "chart": {
                    "caption": "Completed <?= $model->total_complete; ?> of <?= $model->total_controls; ?> controls",
                    "subCaption": "",
                    "numberPrefix": "",
                    "showPercentInTooltip": "1",
                    "showLegend": "1",
                    "legendShadow": '1',
                    "decimals": "0",
                    "useDataPlotColorForLabels": "1",
                    "theme": "fint"
                },
                "data": <?= $model->control_summary_json; ?>
            }
        }).render();
    </script>
<? } ?>