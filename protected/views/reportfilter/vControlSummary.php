<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>
<?
	$isAdmin = Utils::isRoleAdmin();
	$pieHeight = 600;
	$lnkAttr = 'data-recid="\'+record.recid+\'"';
?>
<h2 style="text-align: center;">Control Summary Reports</h2>
<div class="left" style="margin: 0 0 10px 0;">
	<p>
		Click on the <strong>Report Filter Name</strong> to view the chart for that report.<br/>
	</p>
</div>
<!--<div class="right" style="margin: 0 0 10px 0;">-->
<!--    <a href="" id="lnkExport" class="btn btn-success" >Export to CSV</a>-->
<!--    <a href="--><?//= $printUrl; ?><!--" target="_blank" class="btn btn-success">Print</a>-->
<!--</div>-->

<style type="text/css">
	.piechart { margin: 0 0 20px 0; height: <?= $pieHeight; ?>px;}
	.pieloader { margin: 75px 0 0 75px; }
</style>

<? /* Grid code */ ?>
<script type="text/javascript">
	var _showChart = false;
	var _ajaxListGridColumns = [
		{ field: 'filter_name', caption: 'Report Filter Name', size: '100%',  resizable: true, sortable: false,
			render: function (record, index, column_index) {
				if (record.recid == '<?= Obscure::encode($filter_id); ?>') {
					setTimeout(function(){ w2ui.grid.select(record.recid); }, 100);
				}
				return '<div><a class="lnkChart" href="" <?= $lnkAttr; ?>>' + record.filter_name+ '</a></div>';
			}
		},
		{ field: 'filter_desc', caption: 'Filter Description', size: '100%',  resizable: true, sortable: false,
			render: function (record, index, column_index) {
				return '<div><a class="lnkChart" href="" <?= $lnkAttr; ?>>' + record.filter_desc+ '</a></div>';
			}
		},
		{ field: 'updated_on', caption: 'Last Modified', size: '90px',  resizable: true, sortable: false},
		{ field: 'action', caption: 'Actions', size: '150px', hideable: false, sortable: false,  resizable: false, hidden: <?= (!$isAdmin) ? 'true':'false'; ?>,
			render: function (record, index, column_index) {
				var html = '<div></div>';
				if (index == 0) {
					<? if ($isAdmin) { ?>
						html = '<div>'
							+'<a class="Controls" href="<?= url('controlbaseline/list'); ?>/fid/'+record.recid+'">Grid_Display</a>'
							+'</div>';
                    <? } ?>
                } else if (index > 0) {
					<? if ($isAdmin) { ?>
						html = '<div>'
							+'<a class="Controls" href="<?= url('controlbaseline/list'); ?>/fid/'+record.recid+'">Grid_Display</a>'
							+'<span>&nbsp;</span>'
							+'<a class="lnkEdit" href="<?= url('reportfilter/edit'); ?>/id/'+record.recid+'">Edit</a>'
							+'<span>&nbsp;</span>'
							+'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
							+'</div>';
					<? } ?>
				}
				return html;
			}
		}
	];

	var _ajaxListGridAddFunction = false;
</script>
<?
	$gmAjaxListGrid = new gmAjaxListGrid();
	$gmAjaxListGrid->listUrl = url('reportfilter/list');
	$gmAjaxListGrid->title = "";
	$gmAjaxListGrid->intro = "";
	$gmAjaxListGrid->controller = "reportfilter";
	$gmAjaxListGrid->recid = "recid";
	$gmAjaxListGrid->allowAdd = true;
	$gmAjaxListGrid->allowEdit = false;
	$gmAjaxListGrid->allowDelete = true;
	$gmAjaxListGrid->allowCopy = false;
	$gmAjaxListGrid->reloadOnCopy = false;
	$gmAjaxListGrid->showToolbar = true;
	$gmAjaxListGrid->showSearch = false;
	$gmAjaxListGrid->showFooter = false;
	$gmAjaxListGrid->fixedBody = false;
	AjaxListGridGadget::show($gmAjaxListGrid);
?>

<? /* Chart code */ ?>
<div id="divChart">
	<div style="width: 100%; height: 665px; text-align: center;">
		<img src="/images/ajax-loader.gif" style="margin-top: 300px;"/>
	</div>
</div>
<script type="text/javascript">
	function getChart(filter_id) {
		$.ajax({
			method: 'POST',
			dataType: "html",
			url: '<?= url('reportfilter/getcontrolsummary/id'); ?>/' + filter_id,
			success: function(response){
				if (response && response.length > 10) {
					$("#divChart").html(response);
				} else {
					w2alert('Failed to load chart.');
				}
			},
			error: function (xhr, status) {
				alert("Sorry, failed to load chart.");
			}
		});
	}

	$(document).on('click', '.lnkChart', function () {
		var filter_id = $(this).data('recid');
		getChart(filter_id);
		return false;
	});

	FusionCharts.ready(function() {
			setTimeout(function(){}, 100); // add delay to possibly fix grid render bug
			getChart('<?= $filter_id; ?>');
	});
</script>
