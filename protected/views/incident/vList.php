<?
/* @var $this IncidentController */
/* @var $model Incident */
?>
<? $isAuthor = (Utils::isRoleAuthor() || $this->myUserMode); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'incident_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'incident_type', caption: 'Type', size: '150px', searchable: 'text', resizable: true, sortable: true },
        { field: 'summary', caption: 'Summary', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'incident_status_name', caption: 'Status', size: '70px', searchable: 'text', resizable: true, sortable: true },
        { field: 'firstname', caption: 'Assigned To First Name', hidden: true, sortable: true, searchable: 'text' },
        { field: 'lastname', caption: 'Assigned To Last Name', hidden: true, sortable: true, searchable: 'text' },
        { field: 'employee_name', caption: 'Assigned To', size: '150px', resizable: true, sortable: true },
        { field: 'i_reported_on', caption: 'Reporting Date', hidden: true, searchable: 'date', sortable: true },
        { field: 'created_on', caption: 'Reporting Date', size: '155px', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '100px' : '55px'; ?>', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkEdit" href="<?= url(Yii::app()->controller->id.'/edit'); ?>/id/'+record.recid+'">Edit</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                        +'</div>';
                <? } else { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'</div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this IncidentController */
/* @var $model Incident */

    $gmAjaxListGrid = new gmAjaxListGrid();
    if ($this->myUserMode) {
        $gmAjaxListGrid->title = '<h1><i class="fa fa-bell"></i> My Incidents</h1>';
        $gmAjaxListGrid->intro = "<p>Manage, monitor and create Incidents.</p>";
    } else {
        $gmAjaxListGrid->title = "<h2>Manage Incidents</h2>";
        $gmAjaxListGrid->intro = "<p>Administrators and Authors can manage Incidents and assign them to a <a href=\"'.url('employee/list').'\" style=\"color: blue;\">User</a>.</p>";
    }
    $gmAjaxListGrid->listUrl = $listUrl;
    $gmAjaxListGrid->controller = Yii::app()->controller->id;
    $gmAjaxListGrid->recid = "incident_id";
    $gmAjaxListGrid->allowAdd = $isAuthor;
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->allowDelete = $isAuthor;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
