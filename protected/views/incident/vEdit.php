<?
/* @var $this IncidentController */
/* @var $model Incident */
/* @var $form CActiveForm */
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit Incident</h3>

<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array('action'=>url(Yii::app()->controller->id.'/edit/',array('id'=>Obscure::encode($model->incident_id))),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'id'=>'edit-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->labelEx($model,'incident_type_id'); ?>
		<select name="Incident[incident_type_id]" id="Incident_incident_type_id">
			<? $incident_type = IncidentType::model()->findAllByAttributes(array('company_id'=>Utils::contextCompanyId()),array('order' => 'incident_type ASC')); ?>
			<? foreach($incident_type as $tf) { ?>
				<option value="<?= $tf->incident_type_id; ?>"  <?= ($model->incident_type_id==$tf->incident_type_id) ? 'selected="selected"' :'';?>><?= $tf->incident_type; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'incident_type_id'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'summary'); ?>
		<?= $form->textArea($model,'summary',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'summary'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'description'); ?>
		<?= $form->textArea($model,'description',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'system'); ?>
		<?= $form->textField($model,'system',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'system'); ?>
	</div>

	<? if (Utils::isRoleAuthor()) { ?>
		<div class="row">
			<?= $form->labelEx($model,'incident_status_id'); ?>
			<select name="Incident[incident_status_id]" id="Incident_incident_status_id">
				<? $incident_statuses = IncidentStatus::model()->findAll(array('order' => 'incident_status_id ASC')); ?>
				<? foreach($incident_statuses as $ts) { ?>
					<option value="<?= $ts->incident_status_id; ?>"  <?= ($model->incident_status_id==$ts->incident_status_id) ? 'selected="selected"' :'';?>><?= $ts->incident_status_name; ?></option>
				<? } ?>
			</select>
			<?= $form->error($model,'incident_status_id'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($model,'employee_id'); ?>
			<select name="Incident[employee_id]" id="Incident_employee_id" style="width: 100%;">
				<option value="" <?= (empty($model->profile_id)) ? 'selected="selected"' : ''; ?>>no one</option>
				<? $users = Employee::model()->findAllByAttributes(array('company_id' => Utils::contextCompanyId()),array('order' => 'firstname ASC, lastname ASC')); ?>
				<? foreach ($users as $user) { ?>
					<option value="<?= $user->employee_id; ?>" <?= ($model->employee_id == $user->employee_id) ? 'selected="selected"' : ''; ?>><?= $user->firstname.' '.$user->lastname; ?></option>
				<? } ?>
			</select>
			<?= $form->error($model,'employee_id'); ?>
		</div>
	<? } ?>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		//$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url(Yii::app()->controller->id.'/list'); ?>";
			return false;
		});
	});
</script>