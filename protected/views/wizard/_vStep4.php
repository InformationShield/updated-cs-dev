<?
/* @var $this WizardController */
/* @var $model CompanyInfo */
/* @var $form CActiveForm */
?>
<h2 class="StepTitle">Step 4: User Access to Information</h2>
<p>
    In this section we determine how employees and contractor access your sensitive data.
</p>

<div class="form-group">
    <label for="CompanyInfo_employee_mobile_customer_data" class="<?= $labelClass; ?>">1. How many employees and regular contractors have sensitive customer data on mobile devices (including laptops, tablets, smartphones)?</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'employee_mobile_customer_data',
            array(
                '0' => 'None',
                '1' => 'Limited (< 5% of workforce)',
                '2' => 'Some (5 - 25% of workforce)', 
                '3' => 'Moderate (25 - 50% of workforce)', 
                '4' => 'Most ( > 50% of workforce)'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
    </div>
</div>

<div class="form-group">
    <label for="CompanyInfo_remote_network_access" class="<?= $labelClass; ?>">2. About how many employees have remote access to your networks?</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'remote_network_access',
            array(
                '0' => 'None - No Remote Access',
                '1' => 'Limited (< 5% of workforce)', 
                '2' => 'Some (5 - 25% of workforce)', 
                '3' => 'Moderate (25 - 50% of workforce)', 
                '4' => 'Most ( > 50% of workforce)'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
    </div>
</div>

<div class="form-group">
    <label for="CompanyInfo_third_party_data" class="<?= $labelClass; ?>">3. About how many third parties (suppliers, service providers)? do you use to store or process customer information?</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'third_party_data',
            array(
                '0' => 'None',
                '1' => 'Single Primary ',
                '2' => 'Minimal (2 - 25)', 
                '3' => 'Moderate (26 - 100)', 
                '4' => 'High (> 100)'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
    </div>
</div>