<? $totalScore = $wizardInherentRiskScore['total_score']; ?>
<? $maxScore = $wizardInherentRiskScore['max_score']; ?>
<? $updatedOn = DateHelper::FormatDateTimeString($updatedOn, 'm/d/y'); ?>

<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/fusioncharts.js"); ?>"></script>
<script type="text/javascript" src="<?= url("3rdparty/fusioncharts/themes/fusioncharts.theme.fint.js"); ?>"></script>
<div class="row x_color">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_content text-center">
						<h3 style="color: #73879C;">Cyber Risk Score™ Inherent Risk Report</h3>
						<div style="font-size:15px; font-weight: 400;"><strong>Organization:</strong> <?= Utils::contextCompanyName(); ?></div>
					</div>
                </div>
                
                <div class="gauge-risk-score col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_content">
						<div id="inherentRiskReportChart" style="height:370px;"></div>
						<div class="text-center" style="font-size:16px;">
							<p style="display:inline; margin: 0px 20px;"><strong>Completed</strong>: <?= $updatedOn; ?></p>
							<p>
								<strong>Score: </strong><?= $totalScore; ?>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<strong>
									Inherent Risk:
									<? $percentage = ($totalScore / $maxScore) * 100; ?>
									<? $maturity_level = ''; ?>
									<? if($percentage > 90) { ?>
										<? $maturity_level = 'EXTREMELY_HIGH'; ?>
											<span style="color: #FF0000;">Extremely High</span>
									<? } else if($percentage > 70) { ?>
										<? $maturity_level = 'HIGH'; ?>
											<span style="color: #FFA75B;">High</span>
									<? } else if($percentage > 30) { ?>
										<? $maturity_level = 'MODERATE'; ?>
											<span style="color: #FFE65B;">Moderate</span>
									<? } else if($percentage > 10) { ?>
										<? $maturity_level = 'LOW'; ?>
											<span style="color: #CEF357;">Low</span>
									<? } else { ?>
										<? $maturity_level = 'VERY_LOW'; ?>
											<span style="color: #006600;">Very Low</span>
									<? } ?>
								</strong>
							</p>
						</div>
                    </div>
                </div>
				<div class="comparison-risk-score col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_content">
						<div id="divInherentRiskComparisonChart" style="height:270px;"></div>
					</div>
				</div>

				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">&nbsp;</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					 <div class="x_title">
						 <h4 style="color:#445468;">Inherent Risk Summary</h4>
					 </div>
					 <div class="x_content">
						 <p><strong>Your organization has a score of <?= $totalScore; ?> out of 1000 points.  A score of <?= $totalScore; ?> places you in the <?= $maturity_level; ?> inherent cyber risk category</strong></p>
						<? if ($maturity_level === "EXTREMELY_HIGH") { ?>
							<p>
								Don’t worry this doesn’t mean your actual or net cyber risk is extremely high, it just means that you handle a lot of highly sensitive information – data that is likely covered by a lot of different regulations – and that you are highly integrated with multiple third parties. Typically this would be very large companies or government agencies in Defense, Healthcare, Retail, or Financial Services. The size of your organization and the type of business you are in makes you a high-profile target for cyber-attacks. If you implemented no information security practices you would be nearly certain to experience a severe security breach. Organizations in this category should have a comprehensive security program and implement the most stringent security controls available throughout every aspect of the organization, including assessing the security efforts of third parties with any kind of access to your IT infrastructure.
							</p>
						<? } else if ($maturity_level === "HIGH") { ?>
							<p>
								Organizations in this category handle a large amount of sensitive information such as personal healthcare records, financial data or social security numbers. However, due to factors such as size, limited connectivity to third parties, or limited geography of customers/clients/patients; your inherent risk is not at the highest level. Organizations in this category typically include regional-to-national Accounting and Law Firms, large healthcare providers and payers, mid-sized financial institutions like regional Credit Unions, large Universities, and State-level government agencies. It is important for organizations in this category to implement a comprehensive security program that is monitored continuously and assess third parties’ security practices for potential risks.
							</p>
						<? } else if ($maturity_level === "MODERATE") { ?>
							<p>
								Organizations in this area have relatively little regulated data – for example you may accept credit card payments and have employee’s SSNs on file, but a breach would not be devastating to a large number of individuals. However, loss of customer data or an extended outage could make it extremely difficult for the organization to continue due to losing their customer’s trust. Smaller technology companies such as SaaS-based software companies, consulting firms, local-to-regional accounting and Law firms, small healthcare providers, and Local and smaller State government agencies would likely find themselves in this category. Basic Information Security best practices appropriate for an organization of your size should be implemented and evaluated at least once annually.
							</p>
						<? } else if ($maturity_level === "LOW") { ?>
							<p>
								Organizations in this category are unlikely to experience a breach beyond a simple malware infection and if they do, the breach would not put the organization at risk of going out of business.  Small manufacturing companies, small businesses that have a single location and non-profits that do not collect sensitive data might be in this category. Basic information security measures should be implemented and evaluated annually.
							</p>
						<? } else if ($maturity_level === "VERY_LOW") { ?>
							<p>
								This basically means that your operations are entirely off-line and your security risks are limited to physical theft or destruction (natural or man-made). A security incident would affect only your organization, and only minimally at that. A family-owned farm or cash-only services business might fall into this category. Only minimal information security practices are required at this level, with a focus on physical security.
							</p>
						<? } ?>
					 </div>
                </div>
                
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_title">
						<h4 style="color:#445468;">Understanding the Inherent Risk Scoring</h4>
					</div>
					<div class="x_content">
						<p>
							<strong>Inherent risk</strong> is the fundamental cyber security risk based on the type of business
							you are in, the data you process and where you operate.  The Inherent Risk Score is based upon your
							answers to several Key Indicators of cyber information security risk.  In general, the larger and more
							complex the business, the greater the Inherent Risk.   Much like a credit score, the Inherent Risk Score
							is normalized to a range from 0 to 1000.  The higher the score – the higher the inherent risk.
						</p>
						<p>
							Your Inherent Risk Score should only change when there are major shifts in business strategy such as
							entering entirely new markets, International expansion, mergers &amp; acquisitions or dramatic growth in
							customer data.  Inherent risk can be mitigated by implementing information security processes and tools
							or transferred with a cyber insurance policy.
						</p>
					</div>
                </div>

				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="x_content">
						<table class="table table-hover">
							<thead>
							<tr>
								<th>Inherent Risk Category</th>
								<th>Score</th>
								<th>Max Score</th>
								<th>Percentage</th>
							</tr>
							</thead>
							<tbody>
							<? foreach ($wizardInherentRiskScore['steps'] as $section) { ?>
								<tr>
									<td><?= $section['step_name']; ?></td>
									<td><?= $section['score']; ?></td>
									<td><?= $section['max_score']; ?></td>
									<td><?= floor(($section['score'] / $section['max_score']) * 100); ?> %</td>
								</tr>
							<? } ?>
							</tbody>
							<tfoot>
							<tr>
								<td>TOTAL</td>
								<td class="assessment-total-socre"><?= $totalScore; ?></td>
								<td class="assessment-max-socre"><?= $maxScore; ?></td>
								<td><?= floor(($totalScore / $maxScore) * 100); ?> %</td>
							</tr>
							</tfoot>
						</table>
					</div>
				</div>

			</div>
        </div>
    </div>
</div>

<script src="<?= url("3rdparty/echart/echarts-all.js"); ?>"></script>
<script src="<?= url("3rdparty/echart/green.js"); ?>"></script>
<script>
$(document).ready(function () {
	if ($('#inherentRiskReportChart').length > 0) {
		var maxScore = <?= $maxScore;?>;
		var totalScore = <?= $totalScore; ?>;

		var reportOptions = {
			animation: false,

			tooltip : {
				show: false
			},

			toolbox: {
				show : false
			},

			series : [
				{
					name:'the chart',
					type:'gauge',
					clickable: false,
					center : ['50%', '80%'],
					radius : [0, '100%'],
					startAngle: 180,
					endAngle: 0,
					min: 0,
					max: maxScore,
					splitNumber: maxScore,

					axisLine: {
						show: true,
						lineStyle: {
							color: [
								[0.1, '#7DDD4F'],
								[0.30, '#CEF357'],
								[0.70, '#FFE65B'],
								[0.9, '#FFA75B'],
								[1.00, '#FF5F5B']
							],
							width: 20
						}
					},

					axisTick: {
						show: true,
						splitNumber: 1,
						length :0,
					},

					axisLabel : {
						show: true,
						margin: 0,
						formatter: function(v){
							var low_label_position = Math.floor(maxScore * 0.1);
							var medium_label_position = Math.floor(maxScore * 0.5);
							var high_label_position = Math.floor(maxScore * 0.9);

							if(v == 0){
								return 0;
							}

							if(v == maxScore){
								return maxScore;
							}

							if((v % 100) == 0){
								return v;
							}

							/*switch (v){
								case low_label_position: return 'LOW';
								case medium_label_position: return 'MEDIUM';
								case high_label_position: return 'HIGH';
								default: return '';
							}*/
						},
						 textStyle: {
							color: '#000',
							fontSize: 13,
							fontWeight: 'bold'
						}
					},

					splitLine: {
						show: false
					},

					pointer: {
						length : '80%',
						width : 8,
						color : 'auto'
					},

					title: {
						show : false,
					},

					detail: {
						show: true,
						offsetCenter: ['0%', '10%'],
						textStyle: {
							color: '#000',
							align: 'left',
							fontSize: 17
						},
						formatter: function(value){
							//return value + ' out of ' + maxScore + ' points';
						}
					},

					legendHoverLink: false,

					data:[
						{value: totalScore}
					]
				}
			]
		};

		var reportChart = echarts.init(document.getElementById('inherentRiskReportChart'), null);
		reportChart.setOption(reportOptions,true);
		$(window).on('resize', function(){
			reportChart.resize();
		});
	}

	var inherentRiskComparisonChart = new FusionCharts({
		"type": "bar2D",
		"renderAt": "divInherentRiskComparisonChart",
		"width": "100%",
		"height": "270",
		"dataFormat": "json",
		"dataSource": {
			"chart": {
				"caption": "Inherent Risk Comparison",
				"subCaption": "",
				"yAxisMaxValue": "100",
				"numberPrefix": "",
				"theme": "fint"
			},
			"data": <?= json_encode($wizardInherentRiskScore['inherent_risk_score_comparison']); ?>
		}
	}).render();

});
</script>