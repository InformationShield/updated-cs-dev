<?php
/* @var $this WizardController */
/* @var $model CompanyInfo */
/* @var $form CActiveForm */
$labelClass = "control-label col-md-12 col-sm-12 col-xs-12";
$answerClass = "col-md-12 col-sm-12 col-xs-12";
$inputClass = "form-control col-md-12 col-sm-12 col-xs-12";
/*
  `business_name` varchar(100) DEFAULT NULL,
  `num_employees` varchar(10) DEFAULT NULL COMMENT '[1-10, 11-100, 101-1000, 1000+]',
  `address` varchar(256) DEFAULT NULL COMMENT 'Business Location',
  `city` varchar(60) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zipcode` varchar(16) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `publicly_traded` TINYINT(1) DEFAULT NULL COMMENT 'all fields are default null until answered by real human',
  `primary_business_type` varchar(60) DEFAULT NULL,
  `critical_infrastructure` TINYINT(1) DEFAULT NULL COMMENT 'yes,no,not sure - end of step1 wizard questions',
 */
?>
        <h2 class="StepTitle">Getting Started</h2>
        <p>
            Welcome to the Security Setup Wizard.
            The Wizard will help you determine the elements of your information security program.
            In the followings steps we will ask for information about your business that will help refine the program.
            (Don’t worry, you can always change answers later.)
        </p>
        <h2 class="StepTitle">Step 1: Business Profile</h2>
        <p>To get started, first we need to gather some general information about your business.</p>

        <div class="form-group">
            <label class="<?= $labelClass; ?>" for="CompanyInfo_business_name">Business Name</label>
            <div class="<?= $answerClass; ?>">
                <?= $form->textField($model,'business_name',array('size'=>30,'maxlength'=>100,'class'=>$inputClass)); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="<?= $labelClass; ?>" for="CompanyInfo_num_employees">Approximate Number of Employees</label>
            <div class="<?= $answerClass; ?>">
                <?= $form->radioButtonList($model,'num_employees',
                    array(
                        '1-50' => '1-50',
                        '51-500' => '51-500',
                        '501-5000' => '501-5000',
                        '5000+' => '5000+'
                    ),
                    array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span>')); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="CompanyInfo_address" class="<?= $labelClass; ?>">Address of your primary business location</label>
            <div class="<?= $answerClass; ?>">
                <?= $form->textField($model,'address',array('size'=>30,'maxlength'=>100,'class'=>$inputClass)); ?>
            </div>

            <div style="display: inline-block; float: left">
                <label for="CompanyInfo_city" class="<?= $labelClass; ?>">City</label>
                <div class="<?= $answerClass; ?>">
                    <?= $form->textField($model,'city',array('size'=>32,'maxlength'=>60,'class'=>"form-control form-section col-md-3 col-sm-12 col-xs-12")); ?>
                </div>
            </div>
            <div style="display: inline-block; float: left">
                <label for="CompanyInfo_state" class="<?= $labelClass; ?>">State</label>
                <div class="<?= $answerClass; ?>">
                    <? $states = States::getStates(array('US','US TERRITORIES','CANADA')); ?>
                    <?= $form->dropDownList($model,'state',$states,array('class'=>"form-control form-section col-md-3 col-sm-12 col-xs-12")); ?>
                </div>
            </div>
            <div style="display: inline-block; float: left">
                <label for="CompanyInfo_zipcode" class="<?= $labelClass; ?>">Zipcode</label>
                <div class="<?= $answerClass; ?>">
                    <?= $form->textField($model,'zipcode',array('size'=>11,'maxlength'=>20,'class'=>"form-control form-section col-md-3 col-sm-12 col-xs-12")); ?>
                </div>
            </div>
            <div style="display: inline-block; float: left">
                <label for="CompanyInfo_country" class="<?= $labelClass; ?>">Country</label>
                <div class="<?= $answerClass; ?>">
                    <?= $form->dropDownList($model,'country',Country::getCountries(),array('class'=>"form-control form-section col-md-3 col-sm-12 col-xs-12")); ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
        <div class="form-group">
            <label class="<?= $labelClass; ?>" for="CompanyInfo_num_business_locations">Approximately how many business locations do you have?</label>
            <div class="<?= $answerClass; ?>">
                <?= $form->radioButtonList($model,'num_business_locations',
                    array(
                        '0' => 'None (Virtual)',
                        '1' => 'Single',
                        '2-10' => 'Small (2-10 locations)',
                        '10-100' => 'Medium (10 - 100 locations)', 
                        '100+' => 'Large (100+ locations)'
                    ),
                    array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
            </div>
        </div>
        
        <div class="form-group">
            <label for="CompanyInfo_primary_business_type" class="<?= $labelClass; ?>">Primary Business Type</label>
            <div class="<?= $answerClass; ?>">
                <?= $form->dropDownList($model,'primary_business_type',Industries::getKeyVal(),array('class'=>"form-control form-section col-md-12 col-sm-12 col-xs-12")); ?>
            </div>
        </div>
