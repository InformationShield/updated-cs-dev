<?
/* @var $this WizardController */
/* @var $model CompanyInfo */
/* @var $form CActiveForm */
//`security_maturity` TINYINT(1) DEFAULT NULL COMMENT 'step 2 Low, Med, Hi',
?>
<h2 class="StepTitle">Step 2: Target Security Posture</h2>
<p>
    Overall, what level of security maturity do you wish to adopt for your organization?
</p>
<p>
    A "LOW" or Basic security level might be appropriate for an organization that is does not handle sensitive data of
    customers, such as a service organization or manufacturer. A “Moderate” security level may be appropriate for
    organizations that collect sensitive data and want to establish security best practices.
    A "HIGH" security level might be required highly regulated industries that store sensitive customer information.
    The default for most organizations will be "MODERATE."
<p>
<div class="form-group" style="margin-top: 20px;">
    <label for="CompanyInfo_security_maturity" class="<?= $labelClass; ?>">Target Level of Security (Security Maturity)</label>
    <div class="<?= $answerClass; ?>" style="margin: 20px;">
        <?= $form->radioButtonList($model,'security_maturity',
            array('1'=>'Low - Basic Security','2'=>'Moderate – Moderate Security','3'=>'High – High Security'),
            array('class'=>$inputClass." flat",'separator'=>'<br/><br/>')); ?>
    </div>
</div>
