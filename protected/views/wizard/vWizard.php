<?
/* @var $this WizardController */
/* @var $model CompanyInfo */
/* @var $form CActiveForm */

$labelClass = "control-label col-md-12 col-sm-12 col-xs-12";
$answerClass = "col-md-12 col-sm-12 col-xs-12";
$inputClass = "form-control col-md-12 col-sm-12 col-xs-12";
?>
<style type="text/css">
    .form-horizontal .control-label { text-align: left; padding-bottom: 5px;}
    .sep-radios { margin-right: 20px; }
    .form-section { width: auto; }
</style>
<div class="x_panel">
    <div class="x_title">
        <? $baselineName = Baseline_Ex::getCurrentBaselineName(); ?>
        <? if ($baselineName == 'Default') { ?>
            <h2>Security Setup Wizard</h2>
        <? } else { ?>
            <h2>Security Setup Wizard - <?= $baselineName; ?></h2>
        <? } ?>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <? $form=$this->beginWidget('CActiveForm', array('action'=>url('wizard/control',array('id'=>Obscure::encode($model->company_info_id))),
            'id'=>'company-info-form',
            'htmlOptions' => array('class'=>"form-horizontal form-label-left"),
            'enableAjaxValidation'=>false,
        )); ?>
        <div id="wizard" class="form_wizard wizard_horizontal">
            <ul class="wizard_steps">
                <li>
                    <a href="#step-1">
                        <span class="step_no">1</span>
                        <span class="step_descr">
                            <small style="display:none;"><small>Business Profile</small></small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-2">
                        <span class="step_no">2</span>
                        <span class="step_descr">
                            <small style="display:none;"><small>Target Security Posture</small></small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-3">
                        <span class="step_no">3</span>
                        <span class="step_descr">
                            <small style="display:none;"><small>Business Data Profile</small></small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-4">
                        <span class="step_no">4</span>
                        <span class="step_descr">
                            <small style="display:none;"><small>User Access to Information</small></small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-5">
                        <span class="step_no">5</span>
                        <span class="step_descr">
                            <small style="display:none;"><small>Information Technology Environment</small></small>
                        </span>
                    </a>
                </li>
            </ul>
            <? $params = array('model'=>$model,'form'=>$form,'labelClass'=>$labelClass,'answerClass'=>$answerClass,'inputClass'=>$inputClass,'showAllQuestions'=>$showAllQuestions); ?>
            <div id="step-1">
                <div class="form">
                    <?= $form->errorSummary($model); ?>
                </div>
                <? $this->renderPartial('_vStep1',$params); ?>
            </div>
            <div id="step-2">
                <? $this->renderPartial('_vStep2',$params); ?>
            </div>
            <div id="step-3">
                <? $this->renderPartial('_vStep3',$params); ?>
            </div>
            <div id="step-4">
                <? $this->renderPartial('_vStep4',$params); ?>
            </div>
            <div id="step-5">
                <? $this->renderPartial('_vStep5',$params); ?>
            </div>
        </div>
        <? $this->endWidget(); ?>
    </div>
</div>

<script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/3rdparty/wizard/jquery.smartWizard_v3_4.js"></script>
<script type="text/javascript">
    var __last_step = 0;
    $(function(){
        // Smart Wizard
        function onFinishCallback() {
            $('#company-info-form').submit();
        }
        function leaveAStepCallback(obj){
            __last_step= obj.attr('rel');
            //alert(__last_step);
            return true;
        }
        $('#wizard').smartWizard({
            hideButtonsOnDisabled:true,
            onLeaveStep:leaveAStepCallback,
            onFinish: onFinishCallback });
    });
</script>