<?
	$totalScore = $wizardInherentRiskScore['total_score'];
	$maxScore = $wizardInherentRiskScore['max_score'];
?>
<div class="x_panel">
    <div class="x_title">
        <h3>Security Baseline Complete!</h3>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <h4>Congratulations!</h4>
        <p>You have established the baseline for your information security program.
        We have selected a group of<?= ($count > 1) ? ' '.$count : ''; ?> security functions (“controls”) that define your program.<br />
        Your next step is to evaluate each of these Controls and see how they apply to your business.
        </p>
        
        <div class="row">
			<div class="col-xs-12 col-md-6">
				<h4 style="padding: 16px 0 16px 0;">What would you like to do next?</h4>
				<? /*div class="col-xs-12 col-md-12" style="padding-bottom:10px;">
					<a href="<?= url('cyberriskscore/Newassessment'); ?>" class="btn btn-primary">1.&nbsp;&nbsp;Take Cyber Risk Assessment</a>
				</div */?>

				<div class="col-xs-12 col-md-12" style="padding-bottom:10px;">
					<a href="<?= url('controlbaseline/list'); ?>" class="btn btn-primary">1.&nbsp;&nbsp;Review/Refine Security Baseline</a>
				</div>

				<div class="col-xs-12 col-md-12" style="padding-bottom:10px;">
					<a href="<?= url('reportsituation'); ?>" class="btn btn-primary">2.&nbsp;&nbsp;Review Situation Report</a>
				</div>

				<div class="col-xs-12 col-md-12">
					<a href="<?= url('policy/list'); ?>" class="btn btn-primary">3.&nbsp;&nbsp;Begin developing Security Policies for your program</a>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="x_panel">
					<div class="x_content">
						<h3 class="x_content text-center">Cyber Inherent Risk Score</h3>
						<div id="inherentRiskReportChart" style="height:270px;"></div>
						<div class="text-center">
							<p style="font-size:120%; font-weight: bold;">
								<strong>Inherent Risk:</strong>
								<? $percentage = ($totalScore / $maxScore) * 100; ?>
								<? $maturity_level = ''; ?>
								<? if($percentage > 90) { ?>
									<? $maturity_level = 'EXTREMELY_HIGH'; ?>
									<span style="color: #FF0000;">Extremely High</span>
								<? } else if($percentage > 70) { ?>
									<? $maturity_level = 'HIGH'; ?>
									<span style="color: #FFA75B;">High</span>
								<? } else if($percentage > 30) { ?>
									<? $maturity_level = 'MODERATE'; ?>
									<span style="color: #FFE65B;">Moderate</span>
								<? } else if($percentage > 10) { ?>
									<? $maturity_level = 'LOW'; ?>
									<span style="color: #CEF357;">Low</span>
								<? } else { ?>
									<? $maturity_level = 'VERY_LOW'; ?>
									<span style="color: #006600;">Very Low</span>
								<? } ?>
							</p>
						</div>
						<div class="col-xs-12 col-md-12 text-center">
							<a href="<?= url('wizard/inherentriskscore'); ?>" class="btn btn-primary">View Details</a>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>

<script src="<?= url("3rdparty/echart/echarts-all.js"); ?>"></script>
<script src="<?= url("3rdparty/echart/green.js"); ?>"></script>
<script>
$(document).ready(function () {
	if($('#inherentRiskReportChart').length > 0){
		var maxScore = <?= $maxScore;?>;
		var totalScore = <?= $totalScore; ?>;

		var reportOptions = {
			animation: false,

			tooltip : {
				show: false
			},

			toolbox: {
				show : false
			},

			series : [
				{
					name:'the chart',
					type:'gauge',
					clickable: false,
					center : ['50%', '70%'],
					radius : [0, '100%'],
					startAngle: 180,
					endAngle: 0,
					min: 0,
					max: maxScore,
					splitNumber: maxScore,

					axisLine: {
						show: true,
						lineStyle: {
							color: [
								[0.1, '#7DDD4F'],
								[0.30, '#CEF357'],
								[0.70, '#FFE65B'],
								[0.9, '#FFA75B'],
								[1.00, '#FF5F5B']
							],
							width: 14
						}
					},

					axisTick: {
						show: true,
						splitNumber: 1,
						length :0,
					},

					axisLabel : {
						show: true,
						margin: 0,
						formatter: function(v){
							var low_label_position = Math.floor(maxScore * 0.1);
							var medium_label_position = Math.floor(maxScore * 0.5);
							var high_label_position = Math.floor(maxScore * 0.9);

							if(v == 0){
								return 0;
							}

							if(v == maxScore){
								return maxScore;
							}

							if((v % 200) == 0){
								return v;
							}

							/*switch (v){
								case low_label_position: return 'LOW';
								case medium_label_position: return 'MEDIUM';
								case high_label_position: return 'HIGH';
								default: return '';
							}*/
						},
						textStyle: {
							color: '#000',
							fontSize: 13,
							fontWeight: 'bold'
						}
					},

					splitLine: {
						show: false
					},

					pointer: {
						length : '80%',
						width : 8,
						color : 'auto'
					},

					title: {
						show : false,
					},

					detail: {
						show: true,
						offsetCenter: ['0%', '10%'],
						textStyle: {
							color: '#000',
							align: 'left',
							fontSize: 17
						},
						formatter: function(value){
							//return value + ' out of ' + maxScore + ' points';
						}
					},

					legendHoverLink: false,

					data:[
						{value: totalScore}
					]
				}
			]
		};

		var reportChart = echarts.init(document.getElementById('inherentRiskReportChart'), null);
		reportChart.setOption(reportOptions,true);
		$(window).on('resize', function(){
			reportChart.resize();
		});
	}

});
</script>