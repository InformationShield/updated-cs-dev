<?
/* @var $this WizardController */
/* @var $model CompanyInfo */
/* @var $form CActiveForm */

?>
<h2 class="StepTitle">Step 3: Business Data Profile</h2>
<p>
    Next we need to understand the types of data you collect and process.
    This not only determines your security requirements but your potential regulatory requirements and liability in case of breach.
</p>

<div class="form-group">
    <label for="CompanyInfo_highly_sensitive_data" class="<?= $labelClass; ?>">1. How many different types of data do you collect, store or process from customers that they would consider highly sensitive?  If yes, how many types of data?</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'highly_sensitive_data',
            array(
                '0' => 'None - We do not process sensitive data',
                '1' => 'Single - Single Data Type (No PII – Personally Identifiable Information)',
                '2' => 'Single PII - Single Type of PII  (One primary PII)',
                '3' => 'Some - Multiple Data Types (Still No PII)',
                '4' => 'All - Multiple Types (both PII and non PII)'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
    </div>
</div>

<div class="form-group">
    <label for="CompanyInfo_individual_consumer_data" class="<?= $labelClass; ?>">2. Approximately how many records does your organization handle on individual consumers that would be considered “personally identifiable”?  (For example, social security numbers, Driver’s License numbers, bank account numbers or other financial information that is not credit card data.) </label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'individual_consumer_data',
            array(
                '0' => 'None',
                '1' => 'Less than 500',
                '2' => '500 to 5000', 
                '3' => '5000 to 50,000', 
                '4' => 'Greater than 50,000'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
    </div>
</div>
<div class="form-group">
    <label for="CompanyInfo_credit_card_data" class="<?= $labelClass; ?>">3. Do you accept credit card payments or otherwise process/store credit card data (PCI-DSS)?</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'credit_card_data',
            array(
                '0' => 'No',
                '1' => 'Yes',
                '2' => 'Not sure'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span>')); ?>
    </div>
</div>
<div class="form-group">
    <label for="CompanyInfo_individual_health_data" class="<?= $labelClass; ?>">4. Do you process or store electronic or paper health records (ePHI) about individuals?</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'individual_health_data',
            array(
                '0' => 'No',
                '1' => 'Yes',
                '2' => 'Not sure'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span>')); ?>
    </div>
</div>