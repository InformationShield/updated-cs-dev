<?
/* @var $this WizardController */
/* @var $model CompanyInfo */
/* @var $form CActiveForm */
?>
<h2 class="StepTitle">Step 5: Information Technology Environment</h2>

<div class="form-group">
    <label for="CompanyInfo_local_it_datacenters" class="<?= $labelClass; ?>">1. How many different systems store or process sensitive information in your entire organization?</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'local_it_datacenters',
            array(
                '0' => 'None',
                '1' => 'Single System',
                '2' => 'Minimal (2-10)',
                '3' => 'Moderate (20-100)',
                '4' => 'High (100-200)'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
    </div>
</div>

<div class="form-group">
    <label for="CompanyInfo_internally_developed_applications" class="<?= $labelClass; ?>">2. How many internally developed applications that process sensitive information are within your organization?</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'internally_developed_applications',
            array(
                '0' => 'None',
                '1' => 'Single Primary',
                '2' => 'Minimal (2-15)',
                '3' => 'Moderate (16-50)',
                '4' => 'High (50+)'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
    </div>
</div>

<div class="form-group">
    <label for="CompanyInfo_wireless_networks" class="<?= $labelClass; ?>">3. How many wireless networks are in your organization? (Do not count home networks)</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'wireless_networks',
            array(
                '0' => 'None', 
                '1' => 'Single Network, Limited Access Points', 
                '2' => 'Single Network (More than 2 access Points)',
                '3' => 'Multiple (More than 5 access Points)',
                '4' => 'Multiple (More than 20 access points)'
            ),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
    </div>
</div>

<div class="form-group">
    <label for="CompanyInfo_online_customer_transactions" class="<?= $labelClass; ?>">4. How critical is your public facing web site for processes customer transactions?</label>
    <div class="<?= $answerClass; ?>">
        <?= $form->radioButtonList($model,'online_customer_transactions',
            array(
                '0' => 'Not Critical (Our site is information only)',
                '1' => 'Minimal (We provide only basic services)',
                '2' => 'Moderate (Less than 20% of our sales or leads are from our website)', 
                '3' => 'Important (More than 50% of our sales or leads are generated through our website)',
                '4' => 'Critical (It is the sole delivery channel for our business)'),
            array('class'=>$inputClass." flat",'separator'=>'<span class="sep-radios"></span><br/>')); ?>
    </div>
</div>