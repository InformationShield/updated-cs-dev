<? $isAuthor = Utils::isRoleAuthor(); ?>
<? $isSuperOwner = (Utils::isSuper() && Utils::isRoleOwner()); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'policy_library_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'doc_title', caption: 'Title', size: '100%', searchable: 'custom', resizable: true, sortable: true,
            render: function (record, index, column_index) {
				var html = '';
				<? if (getSessionVar('license_level') > Cons::LIC_TRIAL) { ?>
					html = '<span><a class="lnkDownload" href="<?= url('policylibrary/download'); ?>/id/'+record.recid+'">'+record.doc_title+'</a></span>';
				<? } else { ?>
					if (record.license_level <= <?= getSessionVar('license_level'); ?>) {
						html = '<span style="color: blue; font-weight: bold;"><a class="lnkDownload" href="<?= url('policylibrary/download'); ?>/id/'+record.recid+'">'+record.doc_title+'</a></span>';
					} else {
						html = '<span style="color: lightgrey;">' + record.doc_title + '</span>';
					}
				<? } ?>
				return html;
            }
        },
        { field: 'author', caption: 'Author', size: '150px', searchable: 'custom', resizable: true, sortable: true },
		{ field: 'policy_type', caption: 'Type', size: '70px', searchable: 'custom', resizable: true, sortable: true },
		<? if (getSessionVar('license_level') <= Cons::LIC_TRIAL) { ?>
			{ field: 'license_level_name', caption: 'License', size: '70px', searchable: 'custom', resizable: true, sortable: true,
				render: function (record, index, column_index) {
					var html = '';
					if (record.license_level <= <?= getSessionVar('license_level'); ?>) {
						html = '<span style="color: blue; font-weight: bold;">' + record.license_level_name + '</span>';
					} else {
						html = '<span style="color: lightgrey;">' + record.license_level_name + '</span>';
					}
					return html;
				}
			},
		<? } ?>
        <? if (Utils::isSuper()) { // only admins should see non-published policies ?>
        	{ field: 'policy_status_name', caption: 'Status', size: '60px', searchable: 'text', resizable: true, sortable: true },
        <? } ?>
        { field: 'expiry_date', caption: 'Updated', size: '75px', searchable: 'date', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isSuperOwner) ? '130px' : '70px'; ?>', hideable: false, sortable: false,  resizable: false, hidden: <?= (!$isAuthor) ? 'true':'false'; ?>,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkCopy" href="" data-recid="'+record.recid+'">Copy</a>'
                    <? if ($isSuperOwner) { ?>
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkEdit" href="<?= url('policylibrary/edit'); ?>/id/'+record.recid+'">Edit</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                    <? } ?>
                    +'</div>';
                <? } else { ?>
                var html = '<div></div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this PolicylibraryController */
/* @var $model PolicyLibrary */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Policy Library</h2>";
    if ($isAuthor) {
        $gmAjaxListGrid->intro = "<p>Click on the <strong>Title</strong> to download the policy document.</p>";
    }
    $gmAjaxListGrid->controller = "policylibrary";
    $gmAjaxListGrid->recid = "policy_library_id";
    $gmAjaxListGrid->allowAdd = $isSuperOwner;
    $gmAjaxListGrid->allowEdit = $isSuperOwner;
    $gmAjaxListGrid->allowDelete = $isSuperOwner;
    $gmAjaxListGrid->allowCopy = $isAuthor;
    $gmAjaxListGrid->reloadOnCopy = false;
	$gmAjaxListGrid->customSearch = array(
		// bad gadget design forces for definition of preceding search fields to show the searchable fields in the right order
		array(
			'field' => 'doc_title',
			'caption' => 'Title',
			'type' => 'text'
		),
		array(
			'field' => 'author',
			'caption' => 'Author',
			'type' => 'text'
		),
		array(
			'field' => 'policy_type',
			'caption' => 'Type',
			'type' => 'list',
			'hidden' => (empty(mPolicy::$PolicyTypes)) ? true : false,
			'options' => array(
				'items' => mPolicy::$PolicyTypes
			)
		),
	);
    AjaxListGridGadget::show($gmAjaxListGrid);
?>