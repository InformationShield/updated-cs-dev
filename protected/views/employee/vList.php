<? $isAdmin = Utils::isRoleAdmin(); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'employee_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'company_role_id', caption: 'company_role_id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'profile_id', caption: 'profile_id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'firstname', caption: 'First Name', size: '100px', searchable: 'text', resizable: true, sortable: true },
        { field: 'lastname', caption: 'Last Name', size: '100px', hideable: false, sortable: true, searchable: 'text', resizable: true },
        { field: 'email', caption: 'Email', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'department_name', caption: 'Department', size: '100px', hideable: false, sortable: true, searchable: 'text', resizable: true },
        { field: 'company_role_name', caption: 'Role', size: '100px', hideable: false, sortable: true, searchable: 'text', resizable: true },
        { field: 'profile_title', caption: 'Profile', size: '200px', hideable: false, sortable: true, searchable: 'text', resizable: true },
        { field: 'status_name', caption: 'Status', size: '60px', hideable: false, sortable: true, searchable: 'text', resizable: true },
        { field: 'action', caption: 'Actions', size: '100px', hideable: false, sortable: false,  resizable: false, hidden: <?= (!$isAdmin) ? 'true':'false'; ?>,
            render: function (record, index, column_index) {
                <? if ($isAdmin) { ?>
					var html = '<div>'
						+'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
						+'<span>&nbsp;</span>'
						+'<a class="lnkEdit" href="<?= url('employee/edit'); ?>/id/'+record.recid+'">Edit</a>'
						+'<span>&nbsp;</span>'
						+'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
						+'</div>';
                <? } else { ?>
					var html = '<div></div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this EmployeeController */
/* @var $model Employee */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Users</h2>";
    $gmAjaxListGrid->intro = '<p>Administrators can add, view, edit, and delete Users. '
        .'User can be assigned to a single <a href="'.url('userprofile/list').'" style="color: blue;">User Profile</a> and to any number of '
        .'<a href="'.url('task/list').'" style="color: blue;">Tasks</a>. <a href="'.url('employee/list').'" style="color: blue;">Users</a> '
        .'will see <a href="'.url('policy/list').'" style="color: blue;">Policy Documents</a>, '
        .'<a href="'.url('training/list').'" style="color: blue;">Training Documents and Training Videos</a> '
        .'in their Inbox according to their assigned <a href="'.url('userprofile/list').'" style="color: blue;">User Profile</a>. '
        .'</p>';
    $gmAjaxListGrid->controller = "employee";
    $gmAjaxListGrid->recid = "employee_id";
    $gmAjaxListGrid->allowAdd = $isAdmin;
    $gmAjaxListGrid->allowEdit = $isAdmin;
    $gmAjaxListGrid->allowDelete = $isAdmin;
    $gmAjaxListGrid->allowCopy = $isAdmin;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
