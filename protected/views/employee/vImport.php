<?
/* @var $this EmployeeController */
/* @var $form CActiveForm */
?>

<h3 xmlns="http://www.w3.org/1999/html">Import Company Users From Excel File</h3>
<p>
	You may import Excel files with the .xls extension.
	Import will skip the first row as it assumes that it is your header row. So actual data should start at row 2.
	Rows must have at least the first 3 columns Firstname, Lastname, Email, the other columns of Department and Profile Id's are optional. NOTE: The Profile Id's can be comma separated integer values or single integer value.
</p>
<div class="x_panel" style="margin: 20px; font-weight:bold; width:90%; border: black solid 1px; box-shadow: 4px 4px 4px #666666;">
	<table>
		<tr>
			<th>Firstname:</th>
			<th>Lastname:</th>
			<th>Email:</th>
			<th>Department:</th>
			<th>Profile Id:</th>
		</tr>
                <tr>
			<td>John</td>
			<td>Smith</td>
			<td>j.smith@example.com</td>
			<td>Accounting</td>
			<td>1</td>
		</tr>
                <tr>
			<td>Amanda</td>
			<td>Wilshare</td>
			<td>a.wilshare@example.com</td>
			<td>Accounting</td>
			<td>1,2</td>
		</tr>
                <tr>
			<td>Patricia</td>
			<td>Jones</td>
			<td>p.jones@example.com</td>
			<td>IT</td>
			<td>3,4,7</td>
		</tr>
	</table>
</div>

<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array(
		'action'=>url('employee/import'),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'id'=>'edit-form',
		'enableAjaxValidation'=>false,
	)); ?>

		<? if (!empty($message)) { ?>
			<div class="errorSummary">
				<p><?= $message; ?></p>
			</div>
		<? } ?>

		<? if (!empty($errorList)) { ?>
			<div class="errorSummary">
				<h4>Unable to upload users from the XLS file. Please correct the following errors:</h4>
				<?php foreach ($errorList as $row) { ?>
					<?php foreach ($row as $error) { ?>
						<p><?php echo $error; ?></p>
					<?php } ?>
				<?php } ?>
			</div>
		<? } ?>

		<div class="row">
			<h5>Choose an Excel file (.xls) to import:</h5>
		</div>
		<div class="row">
			<input type="file" name="excel" id="logo" value="" style="width: 99%; border: none; background: transparent;" accept=".xls" />
		</div>
		<div class="row buttons">
			<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
			<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
		</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('employee/list'); ?>";
			return false;
		});
	});
</script>