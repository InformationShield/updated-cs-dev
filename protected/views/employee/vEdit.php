<?
/* @var $this EmployeeController */
/* @var $employee Employee */
/* @var $form CActiveForm */
/* @var $mEmployee mEmployee */
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit User</h3>

<div class="form">

<? $form=$this->beginWidget('CActiveForm', array('action'=>url('employee/edit',array('id'=>Obscure::encode($mEmployee->employee_id))),
	'id'=>'edit-form',
	'enableAjaxValidation'=>false,
)); ?>
        <? if (!empty($errorMessages)) { ?>
        <div class="errorSummary">
            <p>The following errors occured while saving changes:</p>
            <ul>
                <? foreach($errorMessages as $errorMessage) { ?>
                <li><?= $errorMessage; ?></li>
                <? } ?>
            </ul>
        </div>
	<? } ?>
	<input type="hidden" id="invitebyemail" name="invitebyemail" value="0">

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($mEmployee); ?>

	<div class="row">
        <?= $form->labelEx($mEmployee,'firstname'); ?>
		<?= $form->textField($mEmployee,'firstname',array('style'=>"width: 99%;")); ?>
		<?= $form->error($mEmployee,'firstname'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($mEmployee,'lastname'); ?>
		<?= $form->textField($mEmployee,'lastname',array('style'=>"width: 99%;")); ?>
		<?= $form->error($mEmployee,'lastname'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($mEmployee,'email'); ?>
		<?= $form->textField($mEmployee,'email',array('style'=>"width: 99%;")); ?>
		<?= $form->error($mEmployee,'email'); ?>
	</div>

	<div class="row">
		<? $roles = CompanyRole_Ex::getRolesAllowedToAssign(); ?>
		<? if (!empty($roles)) { ?>
			<?= $form->labelEx($mEmployee,'company_role_id'); ?>
			<? $disabled = (($mEmployee->company_role_id == Cons::ROLE_OWNER) && Utils::isRoleOwner() && !Utils::isSuper()) ? 'disabled' : '' ?>
			<select name="mEmployee[company_role_id]" id="Employee_company_role_id" style="width: 320px;" <?= $disabled; ?> >
				<? foreach ($roles as $role) { ?>
					<? if ($role->company_role_id == Cons::ROLE_OWNER) { ?>
						<? if (Utils::isSuper() || (($mEmployee->company_role_id == Cons::ROLE_OWNER) && Utils::isRoleOwner())) { ?>
							<option value="<?= $role->company_role_id; ?>" <?= ($mEmployee->company_role_id == $role->company_role_id) ? 'selected="selected"' : ''; ?>><?= $role->company_role_name; ?></option>
						<? } ?>
					<? } else { ?>
						<option value="<?= $role->company_role_id; ?>" <?= ($mEmployee->company_role_id == $role->company_role_id) ? 'selected="selected"' : ''; ?>><?= $role->company_role_name; ?></option>
					<? } ?>
				<? } ?>
			</select>
			<?= $form->error($mEmployee,'company_role_id'); ?>
		<? } ?>
	</div>

	<div class="row">
		<?= $form->labelEx($mEmployee,'department_name'); ?>
		<?= $form->textField($mEmployee,'department_name',array('style'=>"width: 99%;")); ?>
		<?= $form->error($mEmployee,'department_name'); ?>
	</div>


	<div class="row">
		<?= $form->labelEx($mEmployee,'profile_id'); ?>
		<div class="form-group">
			<?php foreach ($mEmployee->employeeProfiles as $employeeProfile) { ?>
				<div class="checkbox">
					<label style="padding-left: 0;">
						<input name="selectedProfiles[]" value="<?php echo $employeeProfile['user_profile_id']; ?>"
							type="checkbox" class="flat"
							<?= ($employeeProfile['employee2profile_id']) ? 'checked="checked"' : ''; ?>>
							<?= $employeeProfile['profile_title']; ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>

	<div class="row">
		<?= $form->labelEx($mEmployee,'status'); ?>
		<?= $form->checkBox($mEmployee, 'status', array('class'=>'flat')); ?>  <span style="font-weight: bold;">Enabled</span>
		<?= $form->error($mEmployee,'status'); ?>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Save and Invite By Email',array('id'=>"btnInvite",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
            $('input[type=us-date]').w2field('date');
            $("#btnSave").click(function(){
                    $('#edit-form').submit();
                    return false;
            });
            $("#btnInvite").click(function(){
                    $("#invitebyemail").val("1");
                    $('#edit-form').submit();
                    return false;
            });
            $("#btnCancel").click(function(){
                    window.location = "<?= url('employee/list'); ?>";
                    return false;
            });
	});
</script>