<?
	$baseline = $mControlBaseline->baseline;
	$controlBaselines = $mControlBaseline->controlBaselines['records'];
	$answerChoices = $mControlBaseline->answerChoices;
	$isCyberRiskBaseline = $mControlBaseline->isCyberRiskBaseline;
?>
<style>
    .errorSummary {
        border: 2px solid #C00;
        padding: 7px 7px 12px 7px;
        margin: 0 0 20px 0;
        background: #FEE;
        font-size: 0.9em;
        display:none;
    }
    .successSummary {
        border: 2px solid #34a437;
        padding: 7px 7px 12px 7px;
        margin: 0 0 20px 0;
        background: #dcf9dd;
        font-size: 0.9em;
        display:none;
    }
    p.current {font-weight:bold;}
    span.guidance{cursor: pointer;}
    div.guidance-content {margin-top: 10px;}
</style>

<h2><?= $baseline['baseline_name']; ?> Assessment</h2>
<p>In the following screens you can update the current Status of your security Controls by answering each question.  The questions determine the level of progress you have made in each area.  You can change these later in the Control Editor.</p>

<div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="errorSummary">
            <p>The following errors occured while saving changes:</p>
            <ul>
                <li>No all answers are selected.</li>
            </ul>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="successSummary">
            <p>Thank you! Assessment is now completed.</p>
        </div>
    </div>

    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h4>Questions Answered</h4>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="text-align: center;">
                <span class="chart" data-percent="0">
                    <span class="percent" style="margin: 8px 0px 0px 10px;"></span>
                </span>
            </div>
        </div>

        <div class="x_panel">
            <div class="x_title">
                <h4>Categories</h4>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <? $i = 0; foreach ($controlBaselines as $k => $v) { ?>
                <div class="col-md-12 col-sm-12 col-xs-12 assessment-question-categories">
                    <p class="<?= ($i == 0) ? "current" : ""; ?>" cid="<?= $k; ?>"><?= $v['category']; ?></p>
                </div>
                <? $i++; } ?>
            </div>
        </div>
    </div>

    <div class="col-lg-9 col-md-8 col-sm-6 col-xs-12 assessment-question-answers">
        <? foreach ($controlBaselines as $k => $controlBaselines) { ?>
			<? foreach ($controlBaselines['questions'] as $i=>$question) { ?>
				<div class="x_panel question-content hidden" cid="<?= $k; ?>">
					<div class="x_content">
						<div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom:2px;">
							<p><strong>Question <?= $i+1; ?>:</strong> <?= $question['question']; ?></p>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px;">
							<span class="label label-info guidance">View Guidance</span>
							<div class="guidance-content hidden"><?= $question['guidance']; ?></div>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom:15px;">
							<p><strong>Answer:</strong></p>
							<? foreach ($answerChoices as $answerChoice) { ?>
								<?
									$answered = ($question['assessed_on']) ? true : false;
									//$checked = (!$answered && $question['selected_control_status_id'] == 0) ? 1 : $question['selected_control_status_id']; // By default select option "Planned" if default answer choice is 0
									$checked = $question['selected_control_status_id'];
								?>
								<? if ( strlen($answerChoice['status_control_desc']) > 0 ) { ?>
									<div class="answers" style="margin-bottom:10px;">
										<input class="flat" name="answer[<?= $question['id']; ?>]" qid="<?= $question['id']; ?>"
											type="radio" value="<?= $answerChoice['control_status_id']; ?>"
											<?= ($checked == $answerChoice['control_status_id']) ? 'checked': ''; ?>
										>&nbsp;<?= $answerChoice['status_control_desc']; ?>
									</div>
								<? } ?>
							<? } ?>
						</div>
					</div>
				</div>
			<? } ?>
        <? } ?>

        <div class="x_panel" style="background: none; border: none;">
            <div class="x_content">
                <p id="btnPreviousCategory" class="btn btn-primary hidden">Previous Category</p>
                <p id="btnBack" class="btn btn-primary hidden">Previous Questions</p>
                <p id="btnNext" class="btn btn-success hidden">Next Questions</p>
                <p id="btnNextCategory" class="btn btn-success hidden">Save &amp; Next Category</p>
                <p id="btnFinish" class="btn btn-success hidden">Finish Assessment</p>
				<a href="<?= url('controlbaseline/list'); ?>" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </div>
</div>

<!-- easypie -->
<script src="<?= url("3rdparty/easypie/jquery.easypiechart.min.js"); ?>"></script>

<script>
    var Assessment = function()
    {
        var $current_category = null;
        var $loader;
        var current_cid = 0;
        var questions_in_current_category = 0;
        var showing_questions = 20;
        var current_question_block_position = 1;
        var percent_complete = 0;

        // buttons
        var $btn_back = $('#btnBack');
        var $btn_next = $('#btnNext');
        var $btn_previous_category = $('#btnPreviousCategory');
        var $btn_next_category = $('#btnNextCategory');
        var $btn_finish = $('#btnFinish');

        function CountQuestionsInCurrentCategory()
        {
            questions_in_current_category = $('.assessment-question-answers .x_panel[cid=' + current_cid + ']').length;
        }

        function GetCurrentCategory()
        {
            $current_category = $('.assessment-question-categories p.current');
            current_cid = parseInt($current_category.attr('cid'));
            return $current_category;
        }

        function GetNextCategory()
        {
            var $next_category = $current_category.parent().next().find('p');
            if(typeof $next_category === 'undefined' || $next_category.length == 0){
                return null;
            }

            return $next_category;
        }

        function GetPreviousCategory()
        {
            var $previous_category = $current_category.parent().prev().find('p');
            if(typeof $previous_category === 'undefined' || $previous_category.length == 0){
                return null;
            }

            return $previous_category;
        }

        function ButtonsRoutine()
        {
            // reset buttons
            $btn_back.addClass("hidden");
            $btn_next.addClass('hidden');
            $btn_previous_category.addClass('hidden');
            $btn_next_category.addClass('hidden');
            $btn_finish.addClass('hidden');

            // reset guidance
            $('p.guidance-content').addClass('hidden');

            // load next category information
            var $next_category = GetNextCategory();
            if($next_category){
                $btn_next_category.find('span').html($next_category.text());
            }

            // load previous category information
            var $previous_category = GetPreviousCategory();
            if($previous_category){
                $btn_previous_category.find('span').html($previous_category.text());
            }

            // display buttons
            if(current_question_block_position > 1){
                $btn_back.removeClass('hidden');
            }else{
                if($previous_category){
                    $btn_previous_category.removeClass('hidden');
                }
                $btn_back.addClass('hidden');
            }

            if(questions_in_current_category > showing_questions){
                $btn_next.removeClass('hidden');
            }

            if((current_question_block_position * showing_questions) >= questions_in_current_category){
                $btn_next.addClass('hidden');
                $btn_next_category.removeClass('hidden');

                if($('.assessment-question-categories').last().find('p').hasClass('current')){
                    $btn_finish.removeClass('hidden');
                    $btn_next_category.addClass('hidden');
                }
            }
        }

        function ShowQuestions(start, reverse)
        {
            var block_start = 0;
            var block_end = 0;

            if(typeof reverse !== 'undeifined' && reverse){
                block_start = (typeof start !== 'undefined') ? (start - 1) * showing_questions : 0;
                block_end = (typeof start !== 'undefined') ? start  * showing_questions : showing_questions;
            }else{
                block_start = (typeof start !== 'undefined') ? start * showing_questions : 0;
                block_end = (typeof start !== 'undefined') ? (start + 1) * showing_questions : showing_questions;
            }

            // reset question visible
            $('.assessment-question-answers .question-content')
                    .addClass('hidden');


            $('.assessment-question-answers .question-content[cid=' + current_cid + ']')
                    .slice(block_start, block_end)
                    .removeClass('hidden');
        }

        function BasicValidation()
        {
            var valid = true;
            $('.errorSummary').hide();
            var $elements = $('.question-content').not('.hidden');
            var $inputs_selected = [];

            $.each($elements, function(index, element){
                var selected = $(element).find('input:checked');
                if(selected.length > 0){
                    $inputs_selected.push(selected[0]);
                }
            });

            if($elements.length != $inputs_selected.length){
                $('.errorSummary').show();
                valid = false;
            }
            return valid;
        }

        function CollectAnswers(category_id)
        {
            var $selected_answers = $('.question-content input:checked');
            if(typeof category_id !== 'undefined'){
                $selected_answers = $('.question-content[cid=' + category_id + '] input:checked');
            }
            var data = [];

            $.each($selected_answers, function(index, answer){
                var q = parseInt($(answer).attr('qid'));
                var v = $(answer).val();

                data.push({qid: q, value: v})
            });

            return data;
        }

        function Listeners()
        {

            $('span.guidance').on('click', function(event){
                $(this).next('div.guidance-content').toggleClass('hidden');
            });

            $btn_next.on('click', function(event){
                // scroll window top
                ScrollTop();

                // perform basic validation
                if(!BasicValidation()){
                    return false;
                }

                // load questions
                ShowQuestions(current_question_block_position);

                // update question position block
                current_question_block_position++;

                // load buttons
                ButtonsRoutine();
            });

            $btn_back.on('click', function(event){
                // update question position block first
                current_question_block_position--;

                // load questions with reverse marker
                ShowQuestions(current_question_block_position, true);

                // load buttons
                ButtonsRoutine();

                // scroll window top
                ScrollTop();
            });

            $btn_next_category.on('click', function(event){
                // perform basic validation
                if(!BasicValidation()){
                    return false;
                }

                // find next category
                var $next_category = GetNextCategory();
                if(!$next_category){
                    // there is no next category
                    return false;
                }

                // perform partial save
                var answers = CollectAnswers(current_cid);
                var data = {
                	baseline_id: '<?= Obscure::encode($baseline['baseline_id']); ?>',
                	answers: answers
                };
                var timer;
                var loaded = false;

                // disable buttons
                $btn_back.addClass('disabled');
                $btn_next.addClass('disabled');
                $btn_previous_category.addClass('disabled');
                $btn_next_category.addClass('disabled');
                $btn_finish.addClass('disabled');

                // append loader
                timer && clearTimeout(timer);
                timer = setTimeout(function () {
                    if(!loaded){
                        $loader.insertAfter($btn_next_category);
                    }
                },600);

                $.post('<?= url('assessment/update'); ?>', data, function(response){
                    // mark as loaded
                    loaded = true;

                    // remove loader
                    $loader.remove();

                    // enable buttons
                    $btn_back.removeClass('disabled');
                    $btn_next.removeClass('disabled');
                    $btn_previous_category.removeClass('disabled');
                    $btn_next_category.removeClass('disabled');
                    $btn_finish.removeClass('disabled');

                    if(response.status == 'success'){
                         // remove current category marker
                        $current_category.removeClass('current');
                        $next_category.addClass('current');

                        // Set current category
                        GetCurrentCategory();

                        // Load current category questions
                        ShowQuestions();

                        // Count current category questions
                        CountQuestionsInCurrentCategory();

                        // reset question position block
                        current_question_block_position = 1;

                        // load buttons
                        ButtonsRoutine();

                        // scroll window top
                        ScrollTop();

                        // Update progress
                        UpdateProgressChart(response.assessment_complete_percent);
                    }

                    if(response.status == 'error'){
                        // scroll window top
                        ScrollTop();
                    }
                }, 'JSON');
            });

            $btn_previous_category.on('click', function(event){
                // scroll window top
                ScrollTop();

                // find previous category
                var $previous_category = GetPreviousCategory();
                if (!$previous_category) {
                    // there is no previous category
                    return false;
                }

                // remove current category marker
                $current_category.removeClass('current');
                $previous_category.addClass('current');

                // Set current category
                GetCurrentCategory();

                // Load current category questions
                ShowQuestions();

                // Count current category questions
                CountQuestionsInCurrentCategory();

                // reset question position block
                current_question_block_position = 1;

                // load buttons
                ButtonsRoutine();
            });

            $btn_finish.on('click', function(event){
                // scroll window top
                ScrollTop();

                // perform basic validation
                if(!BasicValidation()){
                    return false;
                }

                // perform partial save
                var answers = CollectAnswers(current_cid);
                var data = {
					baseline_id: '<?= Obscure::encode($baseline['baseline_id']); ?>',
                	answers: answers
                };
                var timer;
                var loaded = false;

                // disable buttons
                $btn_back.addClass('disabled');
                $btn_next.addClass('disabled');
                $btn_previous_category.addClass('disabled');
                $btn_next_category.addClass('disabled');
                $btn_finish.addClass('disabled');

                 // append loader
                timer && clearTimeout(timer);
                timer = setTimeout(function () {
                    if(!loaded){
                        $loader.insertAfter($btn_next_category);
                    }
                },600);

                $.post('<?= url('assessment/update'); ?>', data, function(response){
                    // mark as loaded
                    loaded = true;
                    // remove loader
                    $loader.remove();

                    // enable buttons
                    $btn_back.removeClass('disabled');
                    $btn_next.removeClass('disabled');
                    $btn_previous_category.removeClass('disabled');
                    $btn_next_category.removeClass('disabled');
                    $btn_finish.removeClass('disabled');

                    if (response.status == 'success') {
                        // redirect
						<? if ($isCyberRiskBaseline) { ?>
							window.location.href = "<?= url('cyberriskscore/summary'); ?>";
						<? } else { ?>
							window.location.href = "<?= url('compliance/controlsummary/id/'.Obscure::encode($baseline['baseline_id'])); ?>";
						<? } ?>

                        // scroll window top
                        ScrollTop();

                        // Update progress
                        UpdateProgressChart(response.assessment_complete_percent);
                    }

                    if(response.status == 'error'){
                        // scroll window top
                        ScrollTop();
                    }
                }, 'JSON');
            });


        }

        function ScrollTop()
        {
            window.scrollTo(0, 0);
        }

        function InitializeProgressChart()
        {
            $('.chart').easyPieChart({
                lineWidth: '6',
                barColor: '#75BCDD',
                onStep: function (from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            UpdateProgressChart(<?= floor(($baseline['assessment_questions_answered'] / $baseline['control_count']) * 100); ?>);
        }

        function UpdateProgressChart(percent)
        {
            var chart = window.chart = $('.chart').data('easyPieChart');
            chart.update(percent);
        }

        return {
            init: function()
            {
                // read current category
                GetCurrentCategory();

                // load questions
                ShowQuestions();

                // count questions
                CountQuestionsInCurrentCategory();

                // initialize buttons
                ButtonsRoutine();

                // initialize listeners
                Listeners();

                // initialize progress chart
                InitializeProgressChart();

                // initialize loader
                $loader = $('<img>')
                    .prop({
                        'src': '<?= url("images/ajax-loader.gif"); ?>'
                    })
                    .css({
                        'position': 'relative',
                        'width': '30px',
                        'height': '30px',
                        'margin': '1px 5px 0px 5px',
                        'border': '0',
                        'float': 'none'
                    });
            }
        }
    }();




    $(document).ready(function(){
        Assessment.init();
    });
</script>