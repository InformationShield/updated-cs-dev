<? $isAuthor = Utils::isRoleAuthor(); 
$isSuperUser = Utils::isSuper();?>
<style>
    .rowasdasd{color: black;}
</style>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'control_baseline_id', caption: 'ID', size: '50px', hidden: true, hideable: false, sortable: false, resizable: false },
        { field: 'id', caption: 'Id', size: '30px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'int' },
        { field: 'category', caption: 'Category', size: '250px', hidden: true, searchable: 'text', resizable: true, sortable: false },
        { field: 'title', caption: 'Title', style: 'font-size:12px;', size: '100%', hideable: false, sortable: false, searchable: 'text', resizable: true },
        { field: 'priority', caption: 'Priority', size: '55px', hidden: true, sortable: false, searchable: 'int', resizable: true },
        { field: 'status', caption: 'Status', style: 'font-size:12px;', size: '90px', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                var html = '';
                switch (record.control_status_id){
                    case '<?php echo Cons::CONTROL_NOT_ACTIVE; ?>':
                        html = '<span style="color: red; font-weight: bold;">ALERT</span>';
                        break;
                    case '<?php echo Cons::CONTROL_PLANNED; ?>':
                        html = '<span style="color: red; font-weight: bold;">ALERT</span>';
                        break;
                    case '<?php echo Cons::CONTROL_IN_PROGRESS; ?>':
                        html = '<span style="color: orange; font-weight: bold;">WARNING</span>';
                        break;
                }
                return html;
            }
        },
        { field: 'target_date', caption: 'Target', size: '85px', searchable: 'date', hidden:true, resizable: true, sortable: false },
        { field: 'label1', caption: 'Label 1', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'text' },
        { field: 'label2', caption: 'Label 2', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'text' },
        { field: 'reference', caption: 'Regulatory Reference', size: '100px', hidden: true, hideable: false, sortable: true, resizable: true, searchable: 'text' },
        { field: 'cat_id', caption: 'Category', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'int' },
        { field: 'control_id', caption: 'Control Id', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'int' },
        { field: 'detail', caption: 'Detail', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'text' },
        { field: 'guidance', caption: 'Details', style: 'font-size:12px; word-wrap:none;', size: '100%', hidden: false, hideable: true, sortable: false, resizable: true, searchable: 'text',
            render: function (record, index, column_index) {
                var html = '<div>' + record.guidance + '</div>';
                return html;
            } },
        { field: 'jobrole', caption: 'Job Role', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'text' },
        { field: 'evidence', caption: 'Evidence', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'text' },
        { field: 'rel_policy', caption: 'Related Policy', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'text' },
        { field: 'question', caption: 'Question', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'text' },
        { field: 'score', caption: 'Score', size: '50px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'int' },
        { field: 'weighting', caption: 'Weighting', size: '50px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'int' },
        { field: 'role_title', caption: 'Security Role', size: '100px', hidden: true, hideable: true, sortable: false, resizable: true, searchable: 'text' },
        { field: 'action', caption: 'Actions', style: 'font-size:12px;', size: '55px', hideable: false, sortable: false, resizable: false,
            render: function (record, index, column_index) {
                var html = '<div>'
                    +'<a class="lnkEdit" href="<?= url('controlbaseline/edit'); ?>/id/'+record.recid+'">Fix</a>'
                    +'</div>';
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = function(event) {};

</script>
<?
/* @var $this ControlbaselineController */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = $listUrl;
    $gmAjaxListGrid->title = '<h2>Situation Report</h2>';
    $gmAjaxListGrid->intro = '<p>This Situation Report shows the Top 10 security controls that must be implemented based on your business environment and Inherent Risk Profile.</p>';
    $gmAjaxListGrid->controller = "reportsituation";
    $gmAjaxListGrid->recid = "control_baseline_id";
    $gmAjaxListGrid->allowAdd = false;
    $gmAjaxListGrid->allowEdit = false;
    $gmAjaxListGrid->allowDelete = false;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    $gmAjaxListGrid->toolbarColumns = false;
    $gmAjaxListGrid->limit = 3000;
    $gmAjaxListGrid->showExportCSV = false;
    $gmAjaxListGrid->showSearch = false;
    $gmAjaxListGrid->recordHeight = 34;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>