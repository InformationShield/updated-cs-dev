<?
/* @var $this UserprofileController */
/* @var $model UserProfile */
/* @var $form CActiveForm */
?>

<style>
	.sortablelist { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	.sortablelist li { margin: 0; padding: 0; font-size: 1.2em; }
	.sortablelist li { height: auto; }
	.ui-state-highlight { height: 3em; min-height: 3em; margin: 0; }
	.sortablelist li .x_panel { padding: 0; margin: 0; }
	.sortablelist li .x_title { border: none; padding: 0; margin: 0; }
	.sortablelist li .panel_toolbox { margin: 8px 5px 0 5px; }
	.sortablelist li .x_title h2 { margin: 10px; font-size: 1em; height: 1.5em; line-height: 1.5em; }
</style>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
    tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });

	$(function() {
		$( "#sortablePolicy,#sortableTraining,#sortableQuiz" ).sortable({
			//placeholder: "ui-state-highlight"
		});
		$( "#sortablePolicy,#sortableTraining,#sortableQuiz" ).disableSelection();
	});
</script>

<h3>Edit User Profile</h3>

<div class="form">

<? $form=$this->beginWidget('CActiveForm', array('action'=>url('userprofile/edit',array('id'=>Obscure::encode($model->user_profile_id))),
	'id'=>'edit-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

    <? if (!empty($errorMessages)) { ?>
        <div class="errorSummary">
            <p>The following errors occured while saving changes:</p>
            <ul>
                <? foreach($errorMessages as $errorMessage) { ?>
                    <li><?= $errorMessage; ?></li>
                <? } ?>
            </ul>
        </div>
    <? } ?>

	<div class="row">
		<?= $form->labelEx($model,'profile_id'); ?>
		<?= $form->textField($model,'profile_id'); ?>
		<?= $form->error($model,'profile_id'); ?>
	</div>

	<div class="row">
        <?= $form->labelEx($model,'profile_title'); ?>
		<?= $form->textField($model,'profile_title',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'profile_title'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'profile_description'); ?>
		<?= $form->textArea($model,'profile_description',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'profile_description'); ?>
	</div>

    <div style="margin: 20px 0 10px 0;">
        <label>Policy List</label>
        <? if (!empty($policies)) { ?>
            <p>To add to the Policy List, select a Policy from the selection below and then click the <span class="fa fa-plus"></span> button.</p>
            <div class="row">
                <a id="btnAddPolicy" href="" class="btn btn-default"><span class="fa fa-plus"></span></a>
                <select name="PolicyOptions" id="PolicyOptions" style="width: 85%;">
                    <? foreach ($policies as $policy) { ?>
                        <? if (empty($policy->doc_title)) continue; ?>
                        <? $policyTitle = $policy->doc_title; ?>
                        <? if (!empty($policy->author)) $policyTitle .= ' by ' . $policy->author; ?>
                        <? if (!empty($policy->publish_date)) $policyTitle .= ' published ' . $policy->publish_date; ?>
                        <option class="policyoption" value="<?= $policy->policy_id; ?>"><?= $policyTitle; ?></option>
                    <? } ?>
                </select>
            </div>
        <? } else { ?>
            <p>No Policies have been published to choose from. Please publish some Policies to add to this list.</p>
        <? } ?>
        <p class="hasPolicy">
            Changes to the list are not saved until you click the "Save Changes" button.<br/>
            Use drag and drop on the Policy title to change the order they will appear to the user.
        </p>
    </div>
	<ul id="sortablePolicy" class="sortablelist">
		<? $iPolicy=1; ?>
		<? foreach($policyList as $policy) { ?>
			<? $this->renderPartial('_EditPolicyEntry',array('policy'=>$policy,'form'=>$form,'iPolicy'=>$iPolicy)); ?>
			<? $iPolicy++; ?>
		<? } ?>
	</ul>
	<ul class="sortablelist emptyPolicy" <?= ($iPolicy > 1) ? 'style="display: none;"' : ''; ?>>
		<li>
			<div class="x_panel tile">
				<div class="x_title">
					<h2>The Policy List is currently empty.</h2>
				</div>
			</div>
		</li>
	</ul>

    <div style="margin: 20px 0 10px 0;">
        <label>Training List</label>
        <? if (!empty($trainings)) { ?>
            <p>To add to the Training List, select a Training from the selection below and then click the <span class="fa fa-plus"></span> button.</p>
            <div class="row">
                <a id="btnAddTraining" href="" class="btn btn-default"><span class="fa fa-plus"></span></a>
                <select name="TrainingOptions" id="TrainingOptions" style="width: 85%;">
                    <? foreach ($trainings as $training) { ?>
                        <? if (empty($training->doc_title)) continue; ?>
                        <? $trainingTitle = $training->doc_title.(($training['training_type']==2) ? ' (video)' : ' (doc)'); ?>
                        <? if (!empty($training->author)) $trainingTitle .= ' by ' . $training->author; ?>
                        <? if (!empty($training->publish_date)) $trainingTitle .= ' published ' . $training->publish_date; ?>
                        <option class="trainingoption" value="<?= $training->training_id; ?>"><?= $trainingTitle; ?></option>
                    <? } ?>
                </select>
            </div>
        <? } else { ?>
            <p>No Training has been published to choose from. Please publish some Training to add to this list.</p>
        <? } ?>
        <p class="hasTraining">
            Changes to the list are not saved until you click the "Save Changes" button.<br/>
            Use drag and drop on the Training title to change the order they will appear to the user.
        </p>
    </div>
    <ul id="sortableTraining" class="sortablelist">
        <? $iTraining=1; ?>
        <? foreach($trainingList as $training) { ?>
            <? $this->renderPartial('_EditTrainingEntry',array('training'=>$training,'form'=>$form,'iTraining'=>$iTraining)); ?>
            <? $iTraining++; ?>
        <? } ?>
    </ul>
    <ul class="sortablelist emptyTraining" <?= ($iTraining > 1) ? 'style="display: none;"' : ''; ?>>
        <li>
            <div class="x_panel tile">
                <div class="x_title">
                    <h2>The Training List is currently empty.</h2>
                </div>
            </div>
        </li>
    </ul>

    <div style="margin: 20px 0 10px 0;">
        <label>Quiz List</label>
        <? if (!empty($quizzes)) { ?>
            <p>To add to the Quiz List, select a Quiz from the selection below and then click the <span class="fa fa-plus"></span> button.</p>
            <div class="row">
                <a id="btnAddQuiz" href="" class="btn btn-default"><span class="fa fa-plus"></span></a>
                <select name="QuizOptions" id="QuizOptions" style="width: 85%;">
                    <? foreach ($quizzes as $quiz) { ?>
                        <? if (empty($quiz->quiz_title)) continue; ?>
                        <? $quizTitle = $quiz->quiz_title; ?>
                        <? if (!empty($quiz->publish_date)) $quizTitle .= ' published ' . $quiz->publish_date; ?>
                        <option class="quizoption" value="<?= $quiz->quiz_id; ?>"><?= $quizTitle; ?></option>
                    <? } ?>
                </select>
            </div>
        <? } else { ?>
            <p>No Quiz has been published to choose from. Please publish some Quiz to add to this list.</p>
        <? } ?>
        <p class="hasQuiz">
            Changes to the list are not saved until you click the "Save Changes" button.<br/>
            Use drag and drop on the Quiz title to change the order they will appear to the user.
        </p>
    </div>
    <ul id="sortableQuiz" class="sortablelist">
        <? $iQuiz=1; ?>
        <? foreach($quizList as $quiz) { ?>
            <? $this->renderPartial('_EditQuizEntry',array('quiz'=>$quiz,'form'=>$form,'iQuiz'=>$iQuiz)); ?>
            <? $iQuiz++; ?>
        <? } ?>
    </ul>
    <ul class="sortablelist emptyQuiz" <?= ($iQuiz > 1) ? 'style="display: none;"' : ''; ?>>
        <li>
            <div class="x_panel tile">
                <div class="x_title">
                    <h2>The Quiz List is currently empty.</h2>
                </div>
            </div>
        </li>
    </ul>

	<div class="row" style="margin: 30px 0 10px 0;">
        <?= CHtml::button('Save Changes',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
        <?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<? $newUserProfilePolicy = array('doc_title'=>'','author'=>'','publish_date'=>'','sort_order'=>'0','user_profile_policy_id'=>'0','policy_id'=>'__x__policy_id__'); ?>
<ul id="blankProfile" style="display: none;">
	<? $this->renderPartial('_EditPolicyEntry',array('policy'=>$newUserProfilePolicy,'form'=>$form,'iPolicy'=>0)); ?>
</ul>

<? $newUserProfileTraining = array('doc_title'=>'','training_type'=>'','author'=>'','publish_date'=>'','sort_order'=>'0','user_profile_training_id'=>'0','training_id'=>'__x__training_id__'); ?>
<ul id="blankTraining" style="display: none;">
    <? $this->renderPartial('_EditTrainingEntry',array('training'=>$newUserProfileTraining,'form'=>$form,'iTraining'=>0)); ?>
</ul>

<? $newUserProfileQuiz = array('quiz_title'=>'','publish_date'=>'','sort_order'=>'0','user_profile_quiz_id'=>'0','quiz_id'=>'__x__quiz_id__'); ?>
<ul id="blankQuiz" style="display: none;">
    <? $this->renderPartial('_EditQuizEntry',array('quiz'=>$newUserProfileQuiz,'form'=>$form,'iQuiz'=>0)); ?>
</ul>

<script type="text/javascript">
	var __nextPolicy = <?= $iPolicy; ?>;
    var __nextTraining = <?= $iTraining; ?>;
    var __nextQuiz = <?= $iQuiz; ?>;


    function isEmptyQuiz() {
        var li = $('#sortableQuiz').children();
        if (li.length > 0) {
            $('.emptyQuiz').hide();
            $('.hasQuiz').show();
        } else {
            $('.emptyQuiz').show();
            $('.hasQuiz').hide();
        }
    }

	function isEmptyPolicy() {
		var li = $('#sortablePolicy').children();
		if (li.length > 0) {
			$('.emptyPolicy').hide();
            $('.hasPolicy').show();
		} else {
			$('.emptyPolicy').show();
            $('.hasPolicy').hide();
		}
	}

    function isEmptyTraining() {
        var li = $('#sortableTraining').children();
        if (li.length > 0) {
            $('.emptyTraining').hide();
            $('.hasTraining').show();
        } else {
            $('.emptyTraining').show();
            $('.hasTraining').hide();
        }
    }

    function showHidePolicyOptions()
    {
        $('.policyoption').show();
        $('.lnkDeletePolicy').each(function(index){
            var id = $(this).data('policyid');
            $('.policyoption[value="'+id+'"]').hide();
        });
        $('#PolicyOptions').val('').change();
        $('.policyoption').each(function () {
            if ($(this).css('display') != 'none') {
                $(this).prop("selected", true);
                return false;
            }
        });
    }

    function showHideTrainingOptions()
    {
        $('.trainingoption').show();
        $('.lnkDeleteTraining').each(function(index){
            var id = $(this).data('trainingid');
            $('.trainingoption[value="'+id+'"]').hide();
        });
        $('#TrainingOptions').val('').change();
        $('.trainingoption').each(function () {
            if ($(this).css('display') != 'none') {
                $(this).prop("selected", true);
                return false;
            }
        });
    }

    function showHideQuizOptions()
    {
        $('.quizoption').show();
        $('.lnkDeleteQuiz').each(function(index){
            var id = $(this).data('quizid');
            $('.quizoption[value="'+id+'"]').hide();
        });
        $('#QuizOptions').val('').change();
        $('.quizoption').each(function () {
            if ($(this).css('display') != 'none') {
                $(this).prop("selected", true);
                return false;
            }
        });
    }

    function AddPolicy(policyTitle, policyId){
		var blank = $('#blankProfile').html();
		blank = blank.replace(/\[0\]/g,'['+__nextPolicy+']');
		blank = blank.replace(/_0_/g,'_'+__nextPolicy+'_');
		blank = blank.replace(/lineItemPolicy\-0/g,'lineItemPolicy-'+__nextPolicy);
		blank = blank.replace(/btnDeletePolicy\-0/g,'btnDeletePolicy-'+__nextPolicy);
		blank = blank.replace(/policytitle\-0/g,'policytitle-'+__nextPolicy);
		blank = blank.replace(/__x__policy_title__/g,policyTitle);
		blank = blank.replace(/__x__policy_id__/g,policyId);
		__nextPolicy++;
		$('#sortablePolicy').append(blank);
		isEmptyPolicy();
		return false;
	}

    function AddTraining(trainingTitle, trainingId){
        var blank = $('#blankTraining').html();
        blank = blank.replace(/\[0\]/g,'['+__nextTraining+']');
        blank = blank.replace(/_0_/g,'_'+__nextTraining+'_');
        blank = blank.replace(/lineItemTraining\-0/g,'lineItemTraining-'+__nextTraining);
        blank = blank.replace(/btnDeleteTraining\-0/g,'btnDeleteTraining-'+__nextTraining);
        blank = blank.replace(/trainingtitle\-0/g,'trainingtitle-'+__nextTraining);
        blank = blank.replace(/__x__training_title__/g,trainingTitle);
        blank = blank.replace(/__x__training_id__/g,trainingId);
        __nextTraining++;
        $('#sortableTraining').append(blank);
        isEmptyTraining();
        return false;
    }

    function AddQuiz(quizTitle, quizId){
        var blank = $('#blankQuiz').html();
        blank = blank.replace(/\[0\]/g,'['+__nextQuiz+']');
        blank = blank.replace(/_0_/g,'_'+__nextQuiz+'_');
        blank = blank.replace(/lineItemQuiz\-0/g,'lineItemQuiz-'+__nextQuiz);
        blank = blank.replace(/btnDeleteQuiz\-0/g,'btnDeleteQuiz-'+__nextQuiz);
        blank = blank.replace(/quiztitle\-0/g,'quiztitle-'+__nextQuiz);
        blank = blank.replace(/__x__quiz_title__/g,quizTitle);
        blank = blank.replace(/__x__quiz_id__/g,quizId);
        __nextQuiz++;
        $('#sortableQuiz').append(blank);
        isEmptyQuiz();
        return false;
    }

	$(function(){
        isEmptyQuiz();
        isEmptyPolicy();
        isEmptyTraining();
        showHidePolicyOptions();
        showHideTrainingOptions();
        showHideQuizOptions();

        $('input[type=us-date]').w2field('date');

		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});

		$("#btnCancel").click(function(){
			window.location = "<?= url('userprofile/list'); ?>";
			return false;
		});
		
		$("#btnAddPolicy").click(function(){
			var policyId = $('#PolicyOptions').val();
            if (policyId > 0) {
                var policyTitle = $("#PolicyOptions option:selected").text();
                var found = $('.lnkDeletePolicy[data-policyid="' + policyId + '"]');
                if (found.length > 0) {
                    w2alert('This Policy is already in the list.');
                } else {
                    AddPolicy(policyTitle, policyId);
                    showHidePolicyOptions();
                }
            }
			return false;
		});

        $("#btnAddTraining").click(function(){
            var trainingId = $('#TrainingOptions').val();
            if (trainingId > 0) {
                var trainingTitle = $("#TrainingOptions option:selected").text();
                var found = $('.lnkDeleteTraining[data-trainingid="' + trainingId + '"]');
                if (found.length > 0) {
                    w2alert('This Training is already in the list.');
                } else {
                    AddTraining(trainingTitle, trainingId);
                    showHideTrainingOptions();
                }
            }
            return false;
        });

        $("#btnAddQuiz").click(function(){
            var quizId = $('#QuizOptions').val();
            if (quizId > 0) {
                var quizTitle = $("#QuizOptions option:selected").text();
                var found = $('.lnkDeleteQuiz[data-quizid="' + quizId + '"]');
                if (found.length > 0) {
                    w2alert('This Quiz is already in the list.');
                } else {
                    AddQuiz(quizTitle, quizId);
                    showHideQuizOptions();
                }
            }
            return false;
        });

		$('#sortablePolicy').on('click','.lnkDeletePolicy',function() {
            var idname = $(this).data('idname');
            $('#' + idname).remove();
            isEmptyPolicy();
            showHidePolicyOptions();
			return false;
		});

        $('#sortableTraining').on('click','.lnkDeleteTraining',function(){
            var idname = $(this).data('idname');
            $('#'+idname).remove();
            isEmptyTraining();
            showHideTrainingOptions();
            return false;
        });

        $('#sortableQuiz').on('click','.lnkDeleteQuiz',function(){
            var idname = $(this).data('idname');
            $('#'+idname).remove();
            isEmptyQuiz();
            showHideQuizOptions();
            return false;
        });
	});
</script>