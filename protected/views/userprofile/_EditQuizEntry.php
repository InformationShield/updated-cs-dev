<?
/* @var $this UserprofileController */
/* @var $form CActiveForm */

$quizTitle = $quiz['quiz_title'];
if (!empty($quizTitle)) {
    if (!empty($quiz['publish_date'])) {
        $quizTitle .= ' published ' . $quiz['publish_date'];
    }
} else {
    $quizTitle = '__x__quiz_title__';
}
$quizid = (empty($quiz['quiz_id'])) ? '__x__quiz_id__' : $quiz['quiz_id'];
?>
<li id="lineItemQuiz-<?= $iQuiz; ?>">
    <div class="x_panel tile">
        <div class="x_title">
            <ul class="nav navbar-left panel_toolbox">
                <li>
                    <a class="lnkDeleteQuiz" id="btnDeleteQuiz-<?= $iQuiz; ?>" data-quizid="<?= $quizid; ?>" data-idname="lineItemQuiz-<?= $iQuiz; ?>" data-toggle="tooltip" data-placement="right" title="Delete"><i class="fa fa-close red"></i></a>
                </li>
            </ul>
            <h2 id="quiztitle-<?= $iQuiz; ?>"><?= $quizTitle; ?></h2>
            <? $columns = array('sort_order','user_profile_quiz_id','quiz_id'); ?>
            <? foreach($columns as $column) { ?>
                <input name="QuizList[<?= $iQuiz; ?>][<?= $column; ?>]" id="QuizList_<?= $iQuiz; ?>_<?= $column; ?>" type="hidden" value="<?= $quiz[$column]; ?>" />
            <? } ?>
        </div>
    </div>
</li>
