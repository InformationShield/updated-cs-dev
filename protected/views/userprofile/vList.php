<? $isAdmin = Utils::isRoleAdmin(); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'user_profile_id', caption: 'upid', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'profile_id', caption: 'Id', size: '45px', sortable: true, searchable: 'int', resizable: true },
        { field: 'profile_title', caption: 'Title', size: '250px', searchable: 'text', resizable: true, sortable: true },
        { field: 'profile_description', caption: 'Description', size: '100%', hideable: false, sortable: true, searchable: 'text', resizable: true },
        { field: 'action', caption: 'Actions', size: '135px', hideable: false, sortable: false,  resizable: false, hidden: <?= (!$isAdmin) ? 'true':'false'; ?>,
            render: function (record, index, column_index) {
                <? if ($isAdmin) { ?>
                var html = '<div>'
                    +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkEdit" href="<?= url('userprofile/edit'); ?>/id/'+record.recid+'">Edit</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkCopy" href="" data-recid="'+record.recid+'">Copy</a>'
                    +'<span>&nbsp;</span>'
                    +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                    +'</div>';
                <? } else { ?>
                var html = '<div></div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this UserprofileController */
/* @var $model UserProfile */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>User Profiles</h2>";
    $gmAjaxListGrid->intro = '<p>Administrators can add, view, edit, copy, and delete User Profiles. '
        .'User Profiles can be assigned <a href="'.url('policy/list').'" style="color: blue;">Policy Documents</a>, '
        .'<a href="'.url('training/list').'" style="color: blue;">Training Documents and Training Videos</a> that designated <a href="'.url('employee/list').'" style="color: blue;">users</a> will see in their Inbox. '
        .'Administrators can assign a single User Profile to each <a href="'.url('employee/list').'" style="color: blue;">User</a>.</p>';
    $gmAjaxListGrid->controller = "userprofile";
    $gmAjaxListGrid->recid = "user_profile_id";
    $gmAjaxListGrid->allowAdd = $isAdmin;
    $gmAjaxListGrid->allowEdit = $isAdmin;
    $gmAjaxListGrid->allowDelete = $isAdmin;
    $gmAjaxListGrid->allowCopy = $isAdmin;
    $gmAjaxListGrid->reloadOnCopy = true;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
