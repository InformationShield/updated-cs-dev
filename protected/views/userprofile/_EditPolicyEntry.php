<?
/* @var $this UserprofileController */
/* @var $form CActiveForm */

$policyTitle = $policy['doc_title'];
if (!empty($policyTitle)) {
    if (!empty($policy['author'])) {
        $policyTitle .= ' by ' . $policy['author'];
    }
    if (!empty($policy['publish_date'])) {
        $policyTitle .= ' published ' . $policy['publish_date'];
    }
} else {
    $policyTitle = '__x__policy_title__';
}
$policyid = (empty($policy['policy_id'])) ? '__x__policy_id__' : $policy['policy_id'];
?>
<li id="lineItemPolicy-<?= $iPolicy; ?>">
    <div class="x_panel tile">
        <div class="x_title">
            <ul class="nav navbar-left panel_toolbox">
                <li>
                    <a class="lnkDeletePolicy" id="btnDeletePolicy-<?= $iPolicy; ?>" data-policyid="<?= $policyid; ?>" data-idname="lineItemPolicy-<?= $iPolicy; ?>" data-toggle="tooltip" data-placement="right" title="Delete"><i class="fa fa-close red"></i></a>
                </li>
            </ul>
            <h2 id="policytitle-<?= $iPolicy; ?>"><?= $policyTitle; ?></h2>
            <? $columns = array('sort_order','user_profile_policy_id','policy_id'); ?>
            <? foreach($columns as $column) { ?>
                <input name="PolicyList[<?= $iPolicy; ?>][<?= $column; ?>]" id="PolicyList_<?= $iPolicy; ?>_<?= $column; ?>" type="hidden" value="<?= $policy[$column]; ?>" />
            <? } ?>
        </div>
    </div>
</li>