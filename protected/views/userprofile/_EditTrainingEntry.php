<?
/* @var $this UserprofileController */
/* @var $form CActiveForm */

$trainingTitle = $training['doc_title'];
if (!empty($trainingTitle)) {
    $trainingTitle .= ($training['training_type']==2) ? ' (video)' : ' (doc)';
    if (!empty($training['author'])) {
        $trainingTitle .= ' by ' . $training['author'];
    }
    if (!empty($training['publish_date'])) {
        $trainingTitle .= ' published ' . $training['publish_date'];
    }
} else {
    $trainingTitle = '__x__training_title__';
}
$trainingid = (empty($training['training_id'])) ? '__x__training_id__' : $training['training_id'];
?>
<li id="lineItemTraining-<?= $iTraining; ?>">
    <div class="x_panel tile">
        <div class="x_title">
            <ul class="nav navbar-left panel_toolbox">
                <li>
                    <a class="lnkDeleteTraining" id="btnDeleteTraining-<?= $iTraining; ?>" data-trainingid="<?= $trainingid; ?>" data-idname="lineItemTraining-<?= $iTraining; ?>" data-toggle="tooltip" data-placement="right" title="Delete"><i class="fa fa-close red"></i></a>
                </li>
            </ul>
            <h2 id="trainingtitle-<?= $iTraining; ?>"><?= $trainingTitle; ?></h2>
            <? $columns = array('sort_order','user_profile_training_id','training_id'); ?>
            <? foreach($columns as $column) { ?>
                <input name="TrainingList[<?= $iTraining; ?>][<?= $column; ?>]" id="TrainingList_<?= $iTraining; ?>_<?= $column; ?>" type="hidden" value="<?= $training[$column]; ?>" />
            <? } ?>
        </div>
    </div>
</li>
