<?
/* @var $this IncidenttypeController */
/* @var $model IncidentType */
/* @var $form CActiveForm */
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit Incident Type</h3>

<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array('action'=>url(Yii::app()->controller->id.'/edit/',array('id'=>Obscure::encode($model->incident_type_id))),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'id'=>'edit-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->labelEx($model,'incident_type'); ?>
		<?= $form->textField($model,'incident_type',array('size'=>80,'max'=>80,'style'=>"width: 99%;")); ?>
		<?= $form->error($model,'incident_type'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'incident_description'); ?>
		<?= $form->textArea($model,'incident_description',array('maxlenght'=>2000,'rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'incident_description'); ?>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url(Yii::app()->controller->id.'/list'); ?>";
			return false;
		});
	});
</script>