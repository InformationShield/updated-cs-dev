<?
/* @var $this IncidenttypeController */
/* @var $model Incident */
?>
<? $isAuthor = Utils::isRoleAuthor(); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'incident_type_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'incident_type', caption: 'Type', size: '250px', searchable: 'text', resizable: true, sortable: true },
        { field: 'incident_description', caption: 'Summary', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '100px' : '55px'; ?>', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkEdit" href="<?= url(Yii::app()->controller->id.'/edit'); ?>/id/'+record.recid+'">Edit</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                        +'</div>';
                <? } else { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'</div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Manage Incident Types</h2>";
    $gmAjaxListGrid->intro = "<p>Administrators and Authors can manage Incidents Types for the company.</p>";
    $gmAjaxListGrid->listUrl = $listUrl;
    $gmAjaxListGrid->controller = Yii::app()->controller->id;
    $gmAjaxListGrid->recid = "incident_type_id";
    $gmAjaxListGrid->allowAdd = $isAuthor;
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->allowDelete = $isAuthor;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
