<?
/* @var $this LibraryController */
/* @var $model ControlLibrary */
/* @var $form CActiveForm */
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
    tinymce.init({
        mode : "textareas",
        theme : "modern",
		menubar: false,
        force_br_newlines : false,
        force_p_newlines : false,
        forced_root_block : '',
    });
</script>
<div class="form">

    <? $form=$this->beginWidget('CActiveForm', array('action'=>url('library/edit',array('id'=>Obscure::encode($model->control_library_id))),
        'id'=>'edit-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation'=>false,
    )); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

    <?= $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-6">
            <?= $form->labelEx($model,'control_id'); ?>
			<?= $model->control_id; ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<label for="ControlLibrary_cat_id" class="required">Category <span class="required">*</span></label>
			<select name="ControlLibrary[cat_id]" id="ControlLibrary_cat_id" style="width: 400px;">
                <? $cats = ControlCategory_Ex::getCategory(); ?>
                <? foreach ($cats as $cat) { ?>
					<option value="<?= $cat['control_category_id']; ?>" <?= ($model->cat_id == $cat['control_category_id']) ? 'selected="selected"' : ''; ?>><?= $cat['category']; ?></option>
                <? } ?>
			</select>
            <?= $form->error($model,'cat_id'); ?>
		</div>

	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'cat_code'); ?>
			<?= $form->textField($model,'cat_code', array('style'=>"width: 35px")); ?>
			<?= $form->error($model,'cat_code'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'cat_sub_code'); ?>
			<?= $form->textField($model,'cat_sub_code', array('style'=>"width: 50px;")); ?>
			<?= $form->error($model,'cat_sub_code'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
            <?= $form->labelEx($model,'sort_order'); ?>
            <?= $form->textField($model,'sort_order', array('size'=>4)); ?>
            <?= $form->error($model,'sort_order'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
            <?= $form->labelEx($model,'priority'); ?>
			<select name="ControlLibrary[priority]" id="ControlLibrary_priority">
				<option value="1"  <?= ($model->priority=='1') ? 'selected="selected"' :'';?>>1</option>
				<option value="2"  <?= ($model->priority=='2') ? 'selected="selected"' :'';?>>2</option>
				<option value="3"  <?= ($model->priority=='3') ? 'selected="selected"' :'';?>>3</option>
			</select>
            <?= $form->error($model,'priority'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
            <?= $form->labelEx($model,'weighting'); ?>
            <?= $form->textField($model,'weighting',array('size'=>4,'maxlength'=>4)); ?>
            <?= $form->error($model,'weighting'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
            <?= $form->labelEx($model,'status'); ?>
			<select name="ControlLibrary[status]" id="ControlLibrary_status">
				<option value="0"  <?= ($model->status=='0') ? 'selected="selected"' :'';?>>deleted</option>
				<option value="1"  <?= ($model->status=='1') ? 'selected="selected"' :'';?>>active</option>
			</select>
            <?= $form->error($model,'status'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
            <?= $form->labelEx($model,'title'); ?>
            <?= $form->textField($model,'title', array('style'=>"width: 100%;")); ?>
            <?= $form->error($model,'title'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
            <?= $form->labelEx($model,'jobrole'); ?>
            <?= $form->textField($model,'jobrole', array('style'=>"width: 50%;")); ?>
            <?= $form->error($model,'jobrole'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
            <?= $form->labelEx($model,'score'); ?>
            <?= $form->textField($model,'score',array('size'=>11,'maxlength'=>11)); ?>
            <?= $form->error($model,'score'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'detail'); ?>
			<?= $form->textArea($model,'detail',array('rows'=>'5','style'=>"width: 99%;")); ?>
			<?= $form->error($model,'detail'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'rel_policy'); ?>
			<?= $form->textArea($model,'rel_policy',array('rows'=>'5','style'=>"width: 99%;")); ?>
			<?= $form->error($model,'rel_policy'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'guidance'); ?>
			<?= $form->textArea($model,'guidance',array('rows'=>'10','style'=>"width: 99%;")); ?>
			<?= $form->error($model,'guidance'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'evidence'); ?>
			<?= $form->textArea($model,'evidence',array('rows'=>'5','style'=>"width: 99%;")); ?>
			<?= $form->error($model,'evidence'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'reference'); ?>
			<?= $form->textArea($model,'reference',array('rows'=>'10','style'=>"width: 99%;")); ?>
			<?= $form->error($model,'reference'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'question'); ?>
			<?= $form->textArea($model,'question',array('rows'=>'5','style'=>"width: 99%;")); ?>
			<?= $form->error($model,'question'); ?>
		</div>
	</div>

	<div class="row"></div>

	<div class="row buttons">
        <?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
        <?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

    <? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
    $(function(){
        $('input[type=us-date]').w2field('date');

        $("#btnSave").click( function() {
            $('#edit-form').submit();
            return false;
        });


        $("#btnCancel").click( function() {
            window.location = "<?= url('library/list'); ?>";
            return false;
        });

    });
</script>