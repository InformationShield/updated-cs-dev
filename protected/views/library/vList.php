<? $isAuthor = Utils::isRoleAuthor(); ?>
<script type="text/javascript">
	var _ajaxListGridColumns = [
		{ field: 'recid', caption: 'recid', size: '0px', sortable: false,  resizable: false, hidden: true },
		{ field: 'control_id', caption: 'ID', size: '45px', sortable: true, searchable: 'int', resizable: true },
		{ field: 'priority', caption: 'Priority', size: '55px', sortable: true, searchable: 'int', resizable: true },
		{ field: 'title', caption: 'Title', size: '100%', hideable: false, sortable: true, searchable: 'text', resizable: true },
		{ field: 'action', caption: 'Actions', size: '100px', hideable: false, sortable: false, resizable: false,
			render: function (record, index, column_index) {
                <? if (Utils::isSuper()) { ?>
					var html = '<div>'
						+'<a class="lnkView" href="" data-recid="'+record.control_id_enc+'">View</a>'
						+'<span>&nbsp;</span>'
						+'<a class="lnkCopy" href="" data-recid="'+record.control_id_enc+'">Add</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkEdit" href="<?= url('library/edit'); ?>/id/'+record.recid+'" data-recid="'+record.recid+'">Edit</a>'
						+'</div>';
				<? } else if ($isAuthor) { ?>
					var html = '<div>'
						+'<a class="lnkView" href="" data-recid="'+record.control_id_enc+'">View</a>'
						+'<span>&nbsp;</span>'
						+'<a class="lnkCopy" href="" data-recid="'+record.control_id_enc+'">Add</a>'
						+'</div>';
				<? } else { ?>
					var html = '<div>'
						+'<a class="lnkView" href="" data-recid="'+record.control_id_enc+'">View</a>'
						+'</div>';
					<? } ?>
				return html;
			}
		}
	];

	var _ajaxListGridAddFunction = false;
</script>

<?
/* @var $this LibraryController */

	$gmAjaxListGrid = new gmAjaxListGrid();
	$gmAjaxListGrid->doInitialization = false; // using a custom init
	$gmAjaxListGrid->allowView = false; // using a custom view handler
	$gmAjaxListGrid->listUrl = false;
	$gmAjaxListGrid->title = "<h2>Control Library</h2>";
	if ($isAuthor) {
		$gmAjaxListGrid->intro = '<p>View our library of Controls and <strong>Add</strong> to your '
			.'<a href="'.url('controlbaseline/list').'" style="color: blue;">'.Baseline_Ex::getCurrentBaselineName().'</a> Control Baseline as desired. '
			.'You can run the Control Wizard to assist you in selecting Controls.</p>'
			.'<a href="'.url('wizard/control').'" class="btn btn-success" style="margin: 0 0 10px 0;">Run the Control Wizard</a>';
	}
	$gmAjaxListGrid->controller = "library";
	$gmAjaxListGrid->recid = "recid";
	$gmAjaxListGrid->allowAdd = Utils::isSuper();
	$gmAjaxListGrid->allowEdit = false;
	$gmAjaxListGrid->allowDelete = false;
	$gmAjaxListGrid->allowCopy = $isAuthor;
	$gmAjaxListGrid->reloadOnCopy = false;
	AjaxListGridGadget::show($gmAjaxListGrid);
?>

<script type="text/javascript">
	$(function () {
		// custom initialization
		// additional widget configuration
		var config2 = {
			layout: {
				name: 'layout',
				style: 'background-color: 0xFFF',
				padding: 0,
				panels: [
					{ type: 'top', size: 150, resizable: true, minSize: 100, hidden: true },
					{ type: 'left', size: 350, resizable: true, minSize: 200 },
					{ type: 'main', minSize: 200, overflow: 'hidden' }
				]
			},
			sidebar: {
				name: 'sidebar',
				topHTML: '<br/>&nbsp;&nbsp;&nbsp;&nbsp;Select a category to filter list.<br/><br/>',
				nodes: <?= ControlCategory_Ex::getTreeNodesJson(); ?>,
				onClick: function (event) {
					w2ui.grid.url='<?= url('library/get'); ?>/categoryid/'+event.target;
					w2ui.grid.header = event.node.text;
					w2ui.grid.reload();
					return false;
				}
			}
		}

		var _window_width  = $(window).width();
		var _window_height = $(window).height();
		$('#maintreegrid').css('height',(_window_height < 700) ? 700 : _window_height-175);
		$( window ).resize(function() {
			_window_height  = $(window).height();
			$('#maintreegrid').css('height',(_window_height < 700) ? 700 : _window_height-175);
		});

		// check for small screens and go tall vs. wide
		if (_window_width < 980) {
			$('#maintreegrid').w2layout(config2.layout);
			w2ui.layout.hide('left');
			w2ui.layout.show('top');
			w2ui.layout.content('top', $().w2sidebar(config2.sidebar));
			w2ui.layout.content('main', $().w2grid(config.grid));
			w2ui.sidebar.topHTML = '';
		} else {
			$('#maintreegrid').w2layout(config2.layout);
			w2ui.layout.content('left', $().w2sidebar(config2.sidebar));
			w2ui.layout.content('main', $().w2grid(config.grid));
		}
		w2ui.sidebar.select('0');
		// end of custom initialization

		// custom lnkView handler
		$('#maintreegrid').on('click','.lnkView',function(){
			var recid = $(this).data('recid');
			w2ui.grid.select(recid);
			var w = $('body').width()-50;
			var ph  = $('body').height();
			var h = (ph < 800) ? ph : ph - 175;
			h = (h > 800) ? 800 : h;
			var popup = w2popup.load({ url: '<?= url('library/view'); ?>/id/'+recid,
				showMax: true,
				width: w,     // width in px
				height: h,     // height in px
				title: 'View Control',
				buttons : '<button class="btn" onclick="w2popup.close();">Close</button> '+
						  '<button class="btn" onclick="w2popup.close(); ajaxListGridCopy(\''+recid+'\');"><strong>Add to Baseline</strong></button>'
			});
			return false;
		});
	});
</script>
