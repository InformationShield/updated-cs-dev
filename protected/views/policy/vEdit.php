<?
/* @var $this PolicyController */
/* @var $model Policy */
/* @var $form CActiveForm */
$this->pageTitle=Yii::app()->name.' - Edit Policy Document';
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
    tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit Policy Document</h3>

<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array('action'=>url('policy/edit',array('id'=>Obscure::encode($model->policy_id))),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'id'=>'edit-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<label>Title <span class="required">*</span>
		</label>
		<?= $form->textField($model,'doc_title',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'doc_title'); ?>
	</div>

	<? if (!empty($model->publish_date)) { ?>
		<div class="row">
			<?= $form->labelEx($model,'publish_date'); ?>
			<p style="color: black;"><?= $model->publish_date; ?></p>
		</div>
	<? } ?>

	<div class="row">
		<label>Review Date</label>
		<input name="Policy[expiry_date]" id="Policy_expiry_date" type="us-date" value="<?= $model->expiry_date; ?>">
		<?= $form->error($model,'expiry_date'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'author'); ?>
		<?= $form->textField($model,'author',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'author'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'keywords'); ?>
		<?= $form->textField($model,'keywords',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'keywords'); ?>
	</div>

	<? /* dave says category not needed now
	<div class="row">
		<?= $form->labelEx($model,'cat_id'); ?>
		<select name="Policy[cat_id]" id="Policy_cat_id" style="width: 320px;">
			<? $cats = ControlCategory_Ex::getCategory(); ?>
			<? foreach ($cats as $cat) { ?>
				<option value="<?= $cat['control_category_id']; ?>" <?= ($model->cat_id == $cat['control_category_id']) ? 'selected="selected"' : ''; ?>><?= $cat['category']; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'cat_id'); ?>
	</div>
	*/ ?>

	<div class="row">
		<label>Type</label>
		<select name="Policy[policy_type]" id="policy_type" style="width: 120px;">
			<? foreach (mPolicy::$PolicyTypes as $policyTypeId=>$policyType) { ?>
				<option value="<?= $policyTypeId; ?>" <?= ($model->policy_type == $policyTypeId) ? 'selected="selected"' : ''; ?>><?= $policyType; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'policy_type'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'approved'); ?>
		<select name="Policy[approved]" id="Policy_approved">
			<option value="0"  <?= ($model->approved=='0') ? 'selected="selected"' :'';?>>Un-Approved</option>
			<option value="1"  <?= ($model->approved=='1') ? 'selected="selected"' :'';?>>Approved</option>
		</select>
		<?= $form->error($model,'approved'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'policy_status_id'); ?>
		<select name="Policy[policy_status_id]" id="Policy_policy_status_id">
			<? $policystatus = PolicyStatus::model()->findAll(array('order' => 'policy_status_id ASC')); ?>
			<? foreach($policystatus as $ps) { ?>
				<option value="<?= $ps->policy_status_id; ?>"  <?= ($model->policy_status_id==$ps->policy_status_id) ? 'selected="selected"' :'';?>><?= $ps->policy_status_name; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'policy_status_id'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'description'); ?>
		<?= $form->textArea($model,'description',array('rows'=>'5','style'=>"width: 99%;")); ?>
		<?= $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'file_name'); ?>
		<? if (empty($model->file_name)) { ?>
			<p>Choose file to upload a document (maximum file size is 10 MB).</p>
		<? } else { ?>
			<p>Current document: <b><?= $model->file_name; ?></b></p>
			<p>Optionally, you may choose another file to upload to replace the current document (maximum file size is 10 MB).</p>
		<? } ?>
		<input type="file" name="Policy" id="Policy" value="" style="width: 99%; border: none; background: transparent;" />
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('policy/list'); ?>";
			return false;
		});
	});
</script>