<? $isAuthor = Utils::isRoleAuthor(); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'policy_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'doc_title', caption: 'Title', size: '100%', searchable: 'custom', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                return '<div><a class="lnkDownload" href="<?= url('policy/download'); ?>/id/'+record.recid+'">'+record.doc_title+'</a></div>';
            }
        },
        { field: 'author', caption: 'Author', size: '150px', searchable: 'custom', resizable: true, sortable: true },
		{ field: 'policy_type', caption: 'Type', size: '70px', searchable: 'custom', resizable: true, sortable: true },
        { field: 'policy_status_name', caption: 'Status', size: '60px', searchable: 'text', resizable: true, sortable: true },
        { field: 'publish_date', caption: 'Published', size: '75px', searchable: 'date', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '100px' : '55px'; ?>', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'<span> </span>'
                        +'<a class="lnkEdit" href="<?= url('policy/edit'); ?>/id/'+record.recid+'">Edit</a>'
                        +'<span> </span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                        +'</div>';
                <? } else { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'</div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = function(event) {
        w2popup.open({
            title     : 'Add New Policy',
            body      : '<div class="w2ui-centered">Click on <strong>Create New</strong> to create a Policy from scratch.<br/>'+
            '<br/>Or click on <strong>Add from the Policy Library</strong> to select a Policy from our library, '+
            'then you may edit it from your Policies list to make further customizations.</div>',
            buttons   : '<button class="btn" onclick="w2popup.close();">Cancel</button> '+
            '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('policy/edit'); ?>\';"><strong>Create New</strong></button>'+
            '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url('policylibrary/list'); ?>\';"><strong>Add from the Policy Library</strong></button>',
            width     : 550,
            height    : 250,
            overflow  : 'hidden',
            opacity   : '0.2',
            modal     : true,
            showClose : true,
            showMax   : false
        });
        return false;
    };
</script>
<?
/* @var $this PolicyController */
/* @var $model Policy */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = $listUrl;
    $gmAjaxListGrid->title = "<h2>Policy</h2>";
    $gmAjaxListGrid->intro = "<p>Click on the <strong>Title</strong> to download the policy document.</p>";
    $gmAjaxListGrid->controller = "policy";
    $gmAjaxListGrid->recid = "policy_id";
    $gmAjaxListGrid->allowAdd = $isAuthor;
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->allowDelete = $isAuthor;
    $gmAjaxListGrid->allowCopy = false;
	$gmAjaxListGrid->reloadOnCopy = false;
	$gmAjaxListGrid->customSearch = array(
		// bad gadget design forces for definition of preceding search fields to show the searchable fields in the right order
		array(
			'field' => 'doc_title',
			'caption' => 'Title',
			'type' => 'text'
		),
		array(
			'field' => 'author',
			'caption' => 'Author',
			'type' => 'text'
		),
		array(
			'field' => 'policy_type',
			'caption' => 'Type',
			'type' => 'list',
			'hidden' => (empty(mPolicy::$PolicyTypes)) ? true : false,
			'options' => array(
				'items' => mPolicy::$PolicyTypes
			)
		),
	);
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
