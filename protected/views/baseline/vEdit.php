<?
/* @var $this BaselineController */
/* @var $model Baseline */
/* @var $form CActiveForm */
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
    tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>
<div class="form">

<? $form=$this->beginWidget('CActiveForm', array('action'=>url('baseline/edit',array('id'=>Obscure::encode($model->baseline_id))),
	'id'=>'edit-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-12">
			<?= $form->labelEx($model,'baseline_name'); ?>
			<?= $form->textField($model,'baseline_name',array('style'=>"width: 100%;",'maxlength'=>255)); ?>
			<?= $form->error($model,'baseline_name'); ?>
		</div>
	</div>

	<input type="hidden" name="Baseline[special_type]" value="<?= $model->special_type; ?>">
	<? /* if ( Utils::isRoleOwner() && Utils::contextCompanyId() == mCyberRiskScore::CYBER_BASELINE_COMPANY_ID ) { ?>
		<div class="row">
			<div class="col-md-4">
				<?= $form->labelEx($model,'special_type'); ?>
				<select name="Baseline[special_type]" id="selSpecialType" style="width: 100%;">
					<option value="0"
						<?= ($model->special_type == 0) ? 'selected="selected"' : ''; ?>>None</option>
					<option value="1"
						<?= ($model->special_type == 1) ? 'selected="selected"' : ''; ?>>Cyber Risk</option>
				</select>
				<?= $form->error($model,'special_type'); ?>
			</div>
		</div>
	<? } */ ?>

	<div class="row">
		<?= $form->labelEx($model,'baseline_description'); ?>
		<?= $form->textArea($model,'baseline_description',array('rows'=>'5','style'=>"width: 100%;")); ?>
		<?= $form->error($model,'baseline_description'); ?>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
$(function(){
	$('input[type=us-date]').w2field('date');

	$("#btnSave").click( function() {
		var results = true;
		$('#edit-form').submit();
		<? /* if (Utils::contextCompanyId() == mCyberRiskScore::CYBER_BASELINE_COMPANY_ID) { ?>
			if ($('#selSpecialType').val() != <?= $model->special_type; ?>) {
				var results = false;
				w2confirm('Changing \'Special Type\' may cause all the customers\' Cyber Risk assessment to be reset. CONTINUE ANYWAY?',
					function(btn) {
						console.log(btn);
						if (btn=='Yes') {
							$('#edit-form').submit();
						}
				});
			} else {
				$('#edit-form').submit();
			}
		<? } else { ?>
			$('#edit-form').submit();
		<? }  */ ?>
		return false;
	});

	$("#btnCancel").click(function(){
		window.location = "<?= url('baseline/list'); ?>";
		return false;
	});
});
</script>