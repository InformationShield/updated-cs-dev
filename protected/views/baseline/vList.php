<? $isAuthor = Utils::isRoleAuthor(); 
$isSuperUser = Utils::isSuper(); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'baseline_id', caption: 'ID', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'baseline_name', caption: 'Name', size: '100%', hideable: false, sortable: true, searchable: 'text', resizable: true ,
            render: function (record, index, column_index) {
                var style="";
                var lnkclass="lnkBaseline";
                if (record.recid == '<?= Obscure::encode(Baseline_Ex::getCurrentBaselineId()); ?>') {
                    setTimeout(function(){ w2ui.grid.select(record.recid); }, 100);
                    style='style="font-weight:bold;"';
                    lnkclass="";
                }
                return '<div '+style+'><a class="'+lnkclass+'" href="<?= url('controlbaseline/changebaseline'); ?>/id/' + record.recid + '">' + record.baseline_name + '</a></div>';
            }
        },
        { field: 'baseline_description', caption: 'Description', size: '250px', searchable: 'text', resizable: true, sortable: true },
        { field: 'control_count', caption: 'Control Count', size: '100px',  resizable: true, sortable: true },
        { field: 'created_on', caption: 'Created On', size: '85px', searchable: 'date', resizable: true, sortable: true },
        { field: 'assessed_on', caption: 'Assessed On', size: '85px', searchable: 'date', resizable: true, sortable: true,
            render: function(record, index, column_index){
                var html = '<div>';                        
                <?php if($isSuperUser){ ?>
                    if(record.assessment_questions_answered == record.control_count){
                        html += record.assessed_on;
                    }else{
                        html += '';
                    }
                <?php } ?>
                html += '</div>';
                return html;
            }},
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '285px' : '55px'; ?>', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                    var html = '<div>';
                    <? if ($isAuthor) { ?>
                        html += '<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                            +'<span>&nbsp;</span>'
                            +'<a class="lnkEdit" href="<?= url('baseline/edit'); ?>/id/'+record.recid+'">Edit</a>'
                            +'<span>&nbsp;</span>'
                            +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>';
                    <? } else { ?>
                        html += '<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>';
                    <? } ?>
                        
                    <?php if($isSuperUser){ ?>
                        if(record.assessment_questions_answered == record.control_count){
                            html += '<span>&nbsp;</span><a class="lnkEdit" href="<?php echo url('assessment/take'); ?>/id/'+record.recid+'">Re-Take Assessment</a>';
                        }else{
                            html += '<span>&nbsp;</span><a class="lnkEdit" href="<?php echo url('assessment/take'); ?>/id/'+record.recid+'">Take Assessment</a>';
                        }
                    <?php } ?>
                    html += '</div>';
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this BaselineController */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->listUrl = $listUrl;
    $gmAjaxListGrid->title = "<h2>Manage Baselines</h2>";
    $gmAjaxListGrid->intro = '<p>You can manage multiple sets of control baselines and choose which baseline is your current context for other pages. <br/>'
        .'Click on the <strong>Name</strong> to change context to that baseline and view it\'s controls.</p>'
        .'<h6>The current baseline context is <strong>'.Baseline_Ex::getCurrentBaselineName().'</strong></h6>';
    $gmAjaxListGrid->controller = "baseline";
    $gmAjaxListGrid->recid = "baseline_id";
    $gmAjaxListGrid->allowAdd = $isAuthor;
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->allowDelete = $isAuthor;
	$gmAjaxListGrid->allowCopy = false;
	$gmAjaxListGrid->showCopy = true;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
<div class="modal fade" id="dlgBaselines"></div>
<script type="text/javascript">
    $(function () {
        $('#maintreegrid<?= $gmAjaxListGrid->gridNum;?>').on('click','.lnkBaseline',function(){
            var href = $(this).attr('href');
            var name = $(this).text();
            w2confirm({
                msg         : 'Are you sure you want to change the current baseline context to the "'+name+'" baseline?',
                title       : 'Change Baseline Context',
                width       : 350,
                height      : 200,
                yes_text    : 'OK',
                yes_callBack: function(){
                    window.location = href;
                    return false;
                },
                no_text     : 'Cancel',
                no_callBack : null
            });
            return false;
        });
    });

    function doCopy() {
        $.ajax({
            method: 'POST',
            dataType: "json",
            url: '<?= url($gmAjaxListGrid->controller.'/CopyBaseline'); ?>',
            data: {},
            success: function(response){
                $('#dlgBaselines').html(response);
                $('#dlgBaselines').modal();
            },
			error: function(xhr) {
                $('#dlgBaselines').html(xhr.responseText);
                $('#dlgBaselines').modal();
                // var errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ': ' + (xhr.responseText);
                // logError(errorResult)
			}
        });
    }
</script>