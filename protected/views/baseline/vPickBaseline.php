<div class="modal-dialog model-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Create Baseline from Template</h4>
		</div>
		<div class="modal-body">
			<form id="frmBaseline" method="post" action="">
				<div class="container-fluid">
					<div class="col-sm-12">
						<label>New Baseline Name *</label>
						<input id="baseline_name" type="text" name="baseline_name" class="form-control">

						<div class="col-sm-12">&nbsp;</div>

						<label>Baseline Description *</label>
						<input id="baseline_description" type="text" name="baseline_description" class="form-control">

						<div class="col-sm-12">&nbsp;</div>

						<label>Template Baseline Name *</label>
						<select id="copy_baseline_id" name="baseline_id" style="width: 100%; height:35px;">
							<option value=""></option>
							<? foreach ($baselines as $baseline) { ?>
								<option value="<?= $baseline['id']; ?>"><?= $baseline["baseline_name"]; ?></option>
							<? } ?>
						</select>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a id="btnCopyBaseline" class="btn btn-primary" type="button">Copy Baseline</a>
			<a data-dismiss="modal" class="btn btn-success" type="button">Cancel</a>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function () {
	$('.baseline').click(function() {
	    var id = $(this).data('id');
	    var copyBaselineName = $(this).data('name');
        $('#id').val(id);
		$('#copy_baseline_name').val(copyBaselineName);
	});

	$('#btnCopyBaseline').click(function() {
		var name = $('#baseline_name').val();
        var description = $('#baseline_description').val();
        var id = $('#copy_baseline_id').val();

        if (!name) {
            alert("Please specify baseline name.");
        } else if (!description) {
            alert("Please specify baseline description.");
		} else if (!id) {
			alert("You must select a baseline to copy from.");
        } else {
            document.getElementById("frmBaseline").action = '<?= url('baseline/CopyBaseline'); ?>'+'/id/'+id;
            $('#frmBaseline').submit();
		}
    });
});
</script>