<?
/* @var $this EvidenceController */
/* @var $model Evidence */
/* @var $form CActiveForm */
$this->pageTitle=Yii::app()->name.' - Edit Evidence';
?>

<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit Evidence</h3>


<div class="form">

	<? $form=$this->beginWidget('CActiveForm', array('action'=>url('evidence/edit',array('id'=>Obscure::encode($model->evidence_id))),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'id'=>'edit-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->labelEx($model,'doc_title'); ?>
		<?= $form->textField($model,'doc_title',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'doc_title'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'control_baseline_id'); ?>
		<select name="Evidence[control_baseline_id]" id="Evidence_control_baseline_id" style="width: 99%;">
			<option value="" <?= (empty($model->company_id)) ? 'selected="selected"' : ''; ?>>None</option>
			<? $controls = ControlBaseline_Ex::getRelatedControls(); ?>
			<? foreach ($controls as $control) { ?>
				<option value="<?= $control['control_baseline_id']; ?>" <?= ($model->control_baseline_id == $control['control_baseline_id']) ? 'selected="selected"' : ''; ?>><?= $control['category'].' - '.$control['title']; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'control_baseline_id'); ?>
	</div>

	<? if ($model->isNewRecord) { ?>
		<div class="row">
			<?= $form->labelEx($model,'file_name'); ?>
			<input type="file" name="evidence" id="evidence" value="" style="width: 99%; border: none; background: transparent;" />
		</div>
	<? } else { ?>
		<p>Since the file has been uploaded you can only edit the meta data above.</p>
	<? } ?>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('evidence/list'); ?>";
			return false;
		});
	});
</script>