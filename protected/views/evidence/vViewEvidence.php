<?php
/* @var $this EvidenceController */
/* @var $model Evidence */

$icon = 'fa-file-text-o';
if ($model->file_type == 'application/pdf') {
    $icon = 'fa-file-pdf-o';
}
?>
<script language="javascript" type="text/javascript">

    $(function(){

        var w = $('#divFrame').width();
        var t = $('#divFrame').offset();
        var f = $('#footer').height();
        var a = $('#above').height();
        var b = $('#below').height();
        var adj = (t.top + f + a + b + 20);
        var h = $(window).height() - adj;
        var bh = $('body').height();
        h = (h < (bh-adj)) ? (bh-adj) : h;
        var iframe = '<iframe id="frmDoc" name="frmDoc" width="'+w+'px" height="'+h+'px" frameborder="0" scrolling="auto" />';
        $('#divFrame').html(iframe);

        setTimeout(function() {
            $('#frmDoc').attr('src', "<?= $fileUrl; ?>");
        }, 10);

        $( window ).resize(function() {
            var w = $('#divFrame').width();
            $('#frmDoc').css('width',w);
        });
    });
</script>
<div id="above">
    <h4><i class="fa <?= $icon; ?>"></i> Evidence: <?= $model->doc_title; ?></h4>
    <p>
        Filename: <?= $model->file_name; ?><br/>
        Uploaded On: <?= DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y'); ?>
    </p>
</div>
<div id="divFrame" style="width: 100%; height: 100%;"></div>
<div id="below">
</div>
