<? $isAuthor = Utils::isRoleAuthor(); ?>
<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'view', caption: 'View', size: '45px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                var icon = 'fa-cloud-download';
                var lnkClass = 'lnkDownload';
                if (record.file_type == 'application/pdf') {
                    icon = 'fa-file-pdf-o';
                    lnkClass = 'lnkGo';
                } else if (record.file_type.substr(0,5) == 'text/') {
                    icon = 'fa-file-text-o';
                    lnkClass = 'lnkGo';
                }  else if (record.file_type.substr(0,6) == 'image/') {
                    icon = 'fa-file-image-o';
                    lnkClass = 'lnkGo';
                }
                icon = '<i class="fa '+icon+' purple" style="font-size: 20px;"></i>';
                return '<div>&nbsp;&nbsp;<a class="'+lnkClass+'" href="<?= url('evidence/viewevidence'); ?>/id/' + record.recid + '">'+icon+'</a></div>';
            }
        },
        { field: 'evidence_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'doc_title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'title', caption: 'Control', size: '300px', searchable: 'text', resizable: true, sortable: true },
        { field: 'file_name', caption: 'File Name', size: '150px', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                return '<div><a class="lnkDownload" href="<?= url('evidence/download'); ?>/id/'+record.recid+'"><i class="fa fa-cloud-download purple" style="font-size: 20px;"></i> '+record.file_name+'</a></div>';
            }
        },
        { field: 'e_uploaded_on', caption: 'Uploaded On (Date)', hidden: true, searchable: 'date', sortable: true },
        { field: 'created_on', caption: 'Uploaded On', size: '150px', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '<?= ($isAuthor) ? '100px' : '55px'; ?>', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                <? if ($isAuthor) { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkEdit" href="<?= url('evidence/edit'); ?>/id/'+record.recid+'">Edit</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>'
                        +'</div>';
                <? } else { ?>
                    var html = '<div>'
                        +'<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'</div>';
                <? } ?>
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = false;

</script>
<?
/* @var $this EvidenceController */
/* @var $model Evidence */

    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Evidence</h2>";
    $gmAjaxListGrid->intro = '<p>Click on these <i class="fa fa-file-pdf-o purple" style="font-size: 20px;"></i> '
        .'<i class="fa fa-file-text-o purple" style="font-size: 20px;"></i> '
        .'<i class="fa fa-file-image-o purple" style="font-size: 20px;"></i> to view an evidence file. '
        .'Or click on the <i class="fa fa-cloud-download purple" style="font-size: 20px;"></i> to download an evidence file.'
        .'</p>';
    $gmAjaxListGrid->controller = "evidence";
    $gmAjaxListGrid->recid = "evidence_id";
    $gmAjaxListGrid->allowAdd = $isAuthor;
    $gmAjaxListGrid->allowEdit = $isAuthor;
    $gmAjaxListGrid->allowDelete = $isAuthor;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = false;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>
