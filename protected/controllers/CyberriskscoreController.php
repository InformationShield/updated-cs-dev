<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class CyberriskscoreController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + update', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index', 'newassessment', 'summary'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex() {
		mPermission::checkAccess($this, 'Compliance', 'Cyber Maturity Risk');
		$assessment = Baseline_Ex::getBaselineAsessmentV2( Utils::contextCompanyId(), Baseline_Ex::getCyberRiskBaselineId() );
		if ($assessment) {
			if ($assessment['assessment_questions_answered'] != $assessment['control_count']) {
				$this->forward(
					'assessment/take/cid/' . Obscure::encode(Utils::contextCompanyId(true))
					. '/id/' . Baseline_Ex::getCyberRiskBaselineId(true)
				);
			} else {
				$this->forward('cyberriskscore/Summary');
			}
		} else {
			$this->renderMessage(
				array('messageId'=>0,'title'=>'<i class="fa fa-warning"></i> Missing Baseline!',
					'message'=>'The Admin has not set a Cyber Risk Baseline for the system.'
					.'<br>Please <a href='.url('site/contact').' style="color: blue;">contact</a> us to resolve this for you.'
				)
			);
		}
	}

	public function actionNewassessment()
    {
		mPermission::checkAccess($this, 'Compliance', 'Cyber Maturity Risk');
		$forwardUrl ='assessment/take/cid/'.Obscure::encode(Utils::contextCompanyId(true))
			.'/id/'.Baseline_Ex::getCyberRiskBaselineId(true);
		$this->forward($forwardUrl);
    }

	public function actionSummary()
	{
		mPermission::checkAccess($this, 'Compliance', 'Cyber Maturity Risk');
		$mCyberRiskScore = new mCyberRiskScore();
		$cyberRiskScore = $mCyberRiskScore->getCyberRiskScore( Utils::contextCompanyId(), Baseline_Ex::getCyberRiskBaselineId());
		if (!is_null($cyberRiskScore)) {
			$this->pageTitle = Yii::app()->name . ' - Cyber Maturity Risk';
			$this->render('vSummary', array('cyberRiskScore' => $cyberRiskScore));
		} else {
			$this->renderMessage(Cons::MSG_ERROR);
		}
	}

}