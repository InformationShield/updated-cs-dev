<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class LibraryController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + get,copy', // we only allow deletion via POST request
			'ajaxOnly + get,view,copy',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('get','view','edit','copy','list'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	public function actionList()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Library');
		$this->render('vList');
	}
	public function actionGet()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Library', null, 'json');
		$s = $this->getSearchParams();
		$categoryid = getGet('categoryid');
		//Yii::log(print_r($_POST,true),'error','search');
		$json = ControlLibrary_Ex::getControlsJson($s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort'],$categoryid);
		echo $json;
	}

	// overriden from base and called by $this->renderView() to get values and skipEncode for View feature
	protected function getViewValues($id,&$skipEncode)
	{
		$values = ControlLibrary_Ex::getViewValues($id);
		$skipEncode = array('Description', 'Sample Policy', 'Guidance', 'Example Evidence', 'Regulatory Reference', 'Assessment Question');
		return $values;
	}
	public function actionView()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Library');
		$this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUTHOR); // base GridViewController method calls override method getViewValues()
	}

	// ControlLibrary Copy
	// override method called by ajaxCopy()
	// function is to copy and returns params for json staus and message
	// can be overridden for other use cases
	protected function doCopy($id)
	{
		$status = 'error';
		$baseline_id = Baseline_Ex::getCurrentBaselineId();
		$message = ControlLibrary_Ex::copyToControlBaseline($id, $baseline_id, Utils::contextCompanyId(), userId());
		if ($message == 0) {
			$status = 'success';
			$message = 'The control was added to your '.Baseline_Ex::getCurrentBaselineName().' baseline.';
		} else if ($message == 1) {
			$message = 'The control failed to be saved to your '.Baseline_Ex::getCurrentBaselineName().' baseline.';
		} else if ($message == 2) {
			$message = 'The control was not added since it already exists in your '.Baseline_Ex::getCurrentBaselineName().' baseline.';
		} else if ($message == 3) {
			$message = 'The control was not found in the Control Library.';
		}
		return array('status'=>$status,'message'=>$message);
	}
	public function actionCopy()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Library', Cons::ROLE_AUTHOR, 'json');
		echo $this->ajaxCopy(Cons::LIC_TRIAL,Cons::ROLE_AUTHOR);
	}

    public function actionEdit()
    {
        mPermission::checkAccess($this, 'Compliance', 'Control Library', Cons::ROLE_SUPER);

        $id = $this->paramObscureId(false); // will exit if bad param for security reasons

        if (isset($id) && $id > 0) {
            $model = ControlLibrary::model()->findByPk($id);
            $model->isNewRecord = false;
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
        } else {
            $model = new ControlLibrary();
            $model->control_id = ControlLibrary_Ex::getNextControlId();
        }

        if (isset($_POST['ControlLibrary'])) {
            $model->attributes = $_POST['ControlLibrary'];

            if ($model->validate()) {
                if ($model->save()) {
                    $this->redirect(url('library/list'));
                } else {
                    Yii::log(print_r($model->getErrors(), true), 'error', 'LibraryController.actionEdit');
                }
            }
        }
        $this->render('vEdit', array('model'=>$model));
    }
}