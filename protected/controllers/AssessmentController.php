<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class AssessmentController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly +  update',
            'ajaxOnly +  update',
        );
    }
    
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('take', 'update'),
				'users'=>array('@'),
            ),
//			array('allow',
//				'actions'=>array('take', 'update'),
//				'users'=>array('@'),
//				'expression'=>'!user()->isGuest && Utils::is()',
//			),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    public function actionTake()
    {
		mPermission::checkLicenseRole($this, Cons::LIC_TRIAL, Cons::ROLE_AUTHOR);
        $baselineId = $this->paramObscureId(true); // will exit if bad param for security reasons
		if ($companyId = getGet('cid')) {
			$companyId = Obscure::decode($companyId);
		} else {
			$companyId = Utils::contextCompanyId();
		}
		$mControlBaseline = new mControlBaseline($companyId, $baselineId);

        $this->render('vTake', array('mControlBaseline'=>$mControlBaseline));
    }
    
    public function actionUpdate()
    {
		// @var $controlAnswer ControlAnswer
    	$baselineId = Obscure::decode(getPost('baseline_id'));
		$answers = getPost('answers');
        $results = array('status' => 'success');
        
        if(!empty($answers)){
        	if ($baselineId == Baseline_Ex::getCyberRiskBaselineId()) {
				foreach ($answers as $answer) {
					$controlBaseline = ControlBaseline::model()->findByAttributes(array('control_baseline_id'=>$answer['qid']));
					if($controlBaseline) {
						$controlAnswer = ControlAnswer::model()->findByAttributes(
							array('control_baseline_id'=>$answer['qid'], 'company_id'=>Utils::contextCompanyId()));
						if (!$controlAnswer) {
							$controlAnswer = new ControlAnswer();
							$controlAnswer->isNewRecord = true;
							$controlAnswer->control_baseline_id = $answer['qid'];
							$controlAnswer->company_id = Utils::contextCompanyId();
						} else {
							$controlAnswer->isNewRecord = false;
						}
						$controlAnswer->score = $controlBaseline['weighting']  * $answer['value'];

						$controlAnswer->assessed_on = date("Y-m-d H:i:s");
						$controlAnswer->control_status_id = $answer['value'];

						// Mark who copied record
						$controlAnswer->updated_on = date("Y-m-d H:i:s");
						$controlAnswer->updated_user_id = userId();

						if($controlAnswer->validate()){
							if(!$controlAnswer->save()){
								// record was not saved
								$results['status'] = 'error';
								$results['msg'][] = 'RECORD_' . $answer['qid'] . '_NOT_SAVED';
							}
						}else{
							// record does not exists
							$results['status'] = 'error';
							$results['msg'][] = 'RECORD_' . $answer['qid'] . '_ACTIVE_RECORD_VALIDATION_ERROR';
						}
					}else{
						// record does not exists
						$results['status'] = 'error';
						$results['msg'][] = 'RECORD_' . $answer['qid'] . '_NOT_EXISTS';
					}
				}
				if ($results['status'] == 'success') {
					$baseline = Baseline_Ex::getBaselineAsessmentV2(Utils::contextCompanyId(), $baselineId);
					$results['assessment_complete_percent'] =  floor(($baseline['assessment_questions_answered'] / $baseline['control_count']) * 100);
				}
			} else {
				foreach ($answers as $answer) {
					$controlBaseline = ControlBaseline::model()->find("control_baseline_id = :qid", array('qid' => $answer['qid']));
					if($controlBaseline){
						$controlBaseline->assessed_on = date("Y-m-d H:i:s");
						$controlBaseline->control_status_id = $answer['value'];

						// Mark who copied record
						$controlBaseline->updated_on = date("Y-m-d H:i:s");
						$controlBaseline->updated_user_id = userId();

						if($controlBaseline->validate()){
							if(!$controlBaseline->save()){
								// record was not saved
								$results['status'] = 'error';
								$results['msg'][] = 'RECORD_' . $answer['qid'] . '_NOT_SAVED';
							}
						}else{
							// record does not exists
							$results['status'] = 'error';
							$results['msg'][] = 'RECORD_' . $answer['qid'] . '_ACTIVE_RECORD_VALIDATION_ERROR';
						}
					}else{
						// record does not exists
						$results['status'] = 'error';
						$results['msg'][] = 'RECORD_' . $answer['qid'] . '_NOT_EXISTS';
					}
				}
				if ($results['status'] == 'success') {
					$baseline = Baseline_Ex::getBaselineAsessment($count, $controlBaseline->company_id, $controlBaseline->baseline_id);
					$results['assessment_complete_percent'] =  floor(($baseline['assessment_questions_answered'] / $baseline['control_count']) * 100);
				}
			}
        }else{
            // no data sent
            $results = array('status' => 'error', 'msg' => array('NO_DATA_AVAILABLE'));
        }
        
        echo json_encode($results);
    }
}

?>