<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class SecurityroleController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete,copy', // we only allow deletion via POST request
            'ajaxOnly + get,delete,copy,view',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','delete','copy','view','edit'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Company', 'Security Roles');
        $status_id = getGet('status');
        $employeeId = getGet('cuid');
        if ($status_id !== null) {
            if ($employeeId !== null) {
                $listUrl = url('securityrole/get', array('status' => $status_id,'cuid' => $employeeId));
            } else {
                $listUrl = url('securityrole/get', array('status' => $status_id));
            }
        } else if ($employeeId !== null) {
            $listUrl = url('securityrole/get',array('cuid'=>$employeeId));
        } else {
            $listUrl = url('securityrole/get');
        }
        $this->render('vList', array('listUrl'=>$listUrl));
    }

    public function actionGet()
    {
		mPermission::checkAccess($this, 'Company', 'Security Roles', Cons::ROLE_AUTHOR, 'json');
        $s = $this->getSearchParams();
        $status_id = getGet('status');
        $employeeId = getGet('cuid');
        if ($employeeId !== null && Obscure::isObscure($employeeId) !== false) {
            $employeeId = Obscure::decode($employeeId);
        } else {
            $employeeId = null;
        }
        $json = SecurityRole_Ex::getSecurityRolesJson(Utils::contextCompanyId(),$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort'],$status_id,$employeeId);
        echo $json;
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = SecurityRole_Ex::getViewValues(Utils::contextCompanyId(),$id);
        $skipEncode = array('Description','Summary','Assigned to Control Baseline');
        return $values;
    }
    public function actionView()
    {
        $this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUDITOR); // base GridViewController method calls override method getViewValues()
    }

    // SecurityRole Delete
    // can override to do preDeleteCheck to potential stop delete action if return is not true
    protected function preDeleteCheck($id,$model,$markStatusOnly=false)
    {
        $results = true;
        if (SecurityRole_Ex::assignedToControlBaseline($id,$controls)) {
            $results = array();
            $results['status'] = 'error';
            $results['message'] = "Can not delete this role because this role is still assigned to the following Company Control Baselines:<br/><br/>" . implode("<br/>", $controls)
                ."<br/><br/>Please reassign those Control Baselines then delete this role.";
            $results['action'] = "See Control Baselines for this role";
            $results['actionUrl'] = url('controlbaseline/list', array('srid' => Obscure::encode($id)));
        }
        return $results; // return true or results array with status and message etc
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,SecurityRole::model(),array('security_role_id'=>$id,'company_id' => Utils::contextCompanyId()),$markStatusOnly);
    }
    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Company', 'Security Roles', Cons::ROLE_ADMIN, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }

    // SecurityRole Copy
    // override method called by ajaxCopy()
    // function is to copy and returns params for json staus and message
    // can be overridden for other use cases
    protected function doCopy($id)
    {
        $status = 'error';
        $message = 'The security role record was not found.';
        $model1 = SecurityRole::model()->findByAttributes(array('security_role_id' => $id, 'company_id' => Utils::contextCompanyId()));
        if (isset($model1)) {
            $model2 = new SecurityRole;
            $attr = $model1->attributes;
            unset($attr['security_role_id']);
            $model2->attributes = $attr;
            $model2->company_id = Utils::contextCompanyId();
            if ($model2->save()) {
                $status = 'success';
                $message = 'The security role was copied.';
            } else {
                $message = 'Failed to copy the security role.';
                Yii::log(print_r($model2->getErrors(), true), 'error', 'SecurityroleController.actionCopy');
            }
        }
        return array('status'=>$status,'message'=>$message);
    }
    public function actionCopy()
    {
		mPermission::checkAccess($this, 'Company', 'Security Roles', Cons::ROLE_ADMIN, 'json');
        echo $this->ajaxCopy(Cons::LIC_TRIAL,Cons::ROLE_ADMIN);
    }

    public function actionEdit()
    {
		mPermission::checkAccess($this, 'Company', 'Security Roles', Cons::ROLE_ADMIN, 'json');

        $id = $this->paramObscureId(false); // will exit if bad param for security reasons

        if (isset($id) && $id > 0) {
            $model = SecurityRole::model()->findByAttributes(array('security_role_id' => $id, 'company_id' => Utils::contextCompanyId()));
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
        } else {
            $model=new SecurityRole;
            $model->company_id = Utils::contextCompanyId();
        }

        if(isset($_POST['SecurityRole']))
        {
            $model->attributes=$_POST['SecurityRole'];
            $model->company_id = Utils::contextCompanyId();
            if($model->validate())
            {
                if($model->save()) {
                    $this->redirect(url('securityrole/list'));
                } else {
                    Yii::log(print_r($model->getErrors(), true), 'error', 'SecurityroleController.actionEdit');
                }
            }
        }
        $this->render('vEdit',array('model'=>$model));
    }
}