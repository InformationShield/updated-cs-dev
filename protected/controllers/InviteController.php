<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class InviteController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + get', // we only allow deletion via POST request
            'ajaxOnly + get',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('signin',),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list', 'get', 'send','open','accept','reject','ignore'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Company', 'Invites');
		$this->render('vList');
    }
    
    public function actionGet()
    {
		mPermission::checkAccess($this, 'Company', 'Invites', Cons::ROLE_ADMIN, 'json');
        $s = $this->getSearchParams();
        $companyId = Utils::contextCompanyId();
        $json = Invite_Ex::getInvitesJson($companyId, $s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }
    
    public function actionSignin()
    {
        $keyId = getGet('k');
        $secretKeys = mWebToken::SecretKeys();     
        if (!empty($keyId) && isset($secretKeys[$keyId])) {
            $encodedToken = getGet('token');
            $decodedToken = null;
            try {
                $mWebToken = new mWebToken($keyId);
                $decodedToken = $mWebToken->decodeToken($encodedToken);
                if ($decodedToken->iss == $keyId) {
                    setSessionVar('invite_guid',$decodedToken->sub);
                    $this->redirect(array('invite/open'));
                } else {
                    $this->renderMessage(array('messageId'=>Cons::MSG_ERROR));
                }
                return;
            } catch (Exception $e) {
                Yii::log($e->getMessage()."\n", 'error', 'InviteController.actionSignin');
                $this->renderMessage(array('messageId'=>Cons::MSG_ERROR));
            }
        } else {
            $this->renderMessage(array('messageId'=>Cons::MSG_ERROR));
        }
    }

    public function actionOpen()
    {
		mPermission::checkAccess($this, 'Company', 'Invites', Cons::ROLE_ADMIN);
        $invite_guid = getSessionVar('invite_guid');
        if (!empty($invite_guid)) {
            $model = Invite::model()->findByAttributes(array('invite_guid'=>$invite_guid));
            if ($model) {
                $details = Invite_Ex::getInvite($count, NULL, $model->invite_id);
            } else {
                $details = NULL;
            }
            if ($model && $details) {
                $this->render('vOpen', array('model' => $model, 'details'=>$details));
            } else {
                Yii::log('Could not find invite record for invite_guid '.$invite_guid, 'error', 'InviteController.actionOpen');
                setSessionVar('invite_guid'); // delete
                $this->redirect(array('site/home'));
            }
        } else {
            $this->redirect(array('site/home'));
        }
    }

    public function actionAccept()
    {
		mPermission::checkAccess($this, 'Company', 'Invites', Cons::ROLE_ADMIN);
        $invite_guid = getSessionVar('invite_guid');
        if (!empty($invite_guid)) {
            $model = Invite::model()->findByAttributes(array('invite_guid'=>$invite_guid));
            if ($model) {
                $details = Invite_Ex::getInvite($count, NULL, $model->invite_id);
            } else {
                $details = NULL;
            }
            setSessionVar('invite_guid'); // delete
            if ($model && $details) {
                $model->accepted = 1;
                $model->accepted_date = date('Y-m-d');
                $model->adopted_baseline = 1;
                $model->save();
                $count  = ControlBaseline_Ex::copyControlsFromBaseline($model->baseline_id,Utils::contextCompanyId(),userId());
                $this->render('vAccepted',array('model' => $model, 'details'=>$details, 'count'=>$count));
            } else {
                Yii::log('Could not find invite record for invite_guid '.$invite_guid, 'error', 'InviteController.actionAccept');
            }
        } else {
            $this->redirect(array('site/home'));
        }
    }

    public function actionReject()
    {
		mPermission::checkAccess($this, 'Company', 'Invites', Cons::ROLE_ADMIN);
        $invite_guid = getSessionVar('invite_guid');
        if (!empty($invite_guid)) {
            $model = Invite::model()->findByAttributes(array('invite_guid'=>$invite_guid));
            setSessionVar('invite_guid'); // delete
            if ($model) {
                $model->accepted = 2;
                $model->adopted_baseline = 2;
                $model->accepted_date = date('Y-m-d');
                $model->save();
                // if no controls defined take to wizard next
                if (ControlBaseline_Ex::getControlCount(Utils::contextCompanyId()) > 0) {
                    $this->redirect(array('site/home'));
                } else {
                    $this->redirect(array('wizard/control'));
                }
            } else {
                Yii::log('Could not find invite record for invite_guid '.$invite_guid, 'error', 'InviteController.actionReject');
            }
        } else {
            $this->redirect(array('site/home'));
        }
    }

    public function actionIgnore()
    {
        setSessionVar('invite_guid'); // delete
        // if no controls defined take to wizard next
        if (ControlBaseline_Ex::getControlCount(Utils::contextCompanyId()) > 0) {
            $this->redirect(array('site/home'));
        } else {
            $this->redirect(array('wizard/control'));
        }
    }
    
    public function actionSend()
    {
		mPermission::checkAccess($this, 'Company', 'Invites', Cons::ROLE_ADMIN);
        $model = new mInvite();
        if (isset($_POST['mInvite'])) {
            $model->attributes=$_POST['mInvite'];
            $model->createMessage();
            if ($model->validate()) {
                if ($model->sendInvite()) {
                    $this->redirect(url('compliance/companies'));
                } else {
                    Yii::log(print_r($model->getErrors(), true), 'error', 'InviteController.actionInvite');
                }
            }
        } else {
            $model->initialize();
        }
        $this->render('vSend',array('model'=>$model));
    }
}