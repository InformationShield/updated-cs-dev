<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class QuizlibraryController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete,copy', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view,copy',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','view','copy'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'edit' and 'delete' actions
                'actions'=>array('delete','edit'),
                'expression'=>'!user()->isGuest && Utils::isSuper()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quiz Library');
		$this->render('vList');
    }

    public function actionGet()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quiz Library', Cons::ROLE_AUTHOR, 'json');
        $s = $this->getSearchParams();
        $json = QuizLibrary_Ex::getQuizLibraryJson($s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    // QuizLibrary Delete
    // can override to do post delete actions
    protected function postDelete($id,$model,$markStatusOnly=false) {
        Quiz_Ex::deleteQuestions($id, null, null, $markStatusOnly);
        return true;
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,QuizLibrary::model(),array('quiz_library_id'=>$id),$markStatusOnly);
    }
    public function actionDelete()
    {
        if (Utils::isSuper()) {
			$id = $this->paramObscureId(); // will exit if bad param for security reasons
			echo $this->doDelete($id, true);
        } else {
            $status = 'error';
            $message = 'Your role is not authorized to delete the quiz library document.';
            echo json_encode(array('status' => $status, 'message'=>$message));
        }
    }

    public function actionView()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quiz Library', Cons::ROLE_AUTHOR);
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		if (isset($id) && $id > 0) {
			$values = QuizLibrary_Ex::getViewValues($id);
			$gmAjaxListView = new gmAjaxListView($values);
			$criteria = new CDbCriteria(array('order' => 'sort_order ASC'));
			$questions = QuizQuestion::model()->findAllByAttributes(array('quiz_library_id' => $id, 'status' => 1), $criteria);
			$this->renderPartial('//quiz/vView',array('gmAjaxListView'=>$gmAjaxListView,'questions'=>$questions));
		}
    }

    // QuizLibrary Copy
    // override method called by ajaxCopy()
    // function is to copy and returns params for json staus and message
    // can be overridden for other use cases
    protected function doCopy($id)
    {
        $status = 'error';
        $message = 'Failed to copy quiz library document to your quiz.';
        $message = QuizLibrary_Ex::copyQuizLibrary($id, Utils::contextCompanyId(), userId());
        if ($message === false) {
            $status = 'success';
            $message = 'The quiz library document was copied to your quiz.';
        }
        return array('status'=>$status,'message'=>$message);
    }
    public function actionCopy()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quiz Library', Cons::ROLE_AUTHOR, 'json');
        echo $this->ajaxCopy(Cons::LIC_TRIAL,Cons::ROLE_AUTHOR);
    }

    public function actionEdit()
    {
        if (Utils::isSuper()) {
            $id = $this->paramObscureId(false); // will exit if bad param for security reasons
			$saveAndExit = getPost('exit');
            $questionList =  array();
            $errorMessages =array();
            $model = null;
            $isNew = true;
            if (isset($id) && $id > 0) {
                $model = QuizLibrary::model()->findByAttributes(array('quiz_library_id' => $id));
                if (!isset($model)) {
                    badurl('User attempting wrongful access.');
                }
                $model->isNewRecord = false;
                $isNew = false;
            } else {
                $model=new QuizLibrary;
                $model->isNewRecord = true;
            }

            if(isset($_POST['QuizLibrary']))
            {
                $preStatus = $model->quiz_status_id;
                $model->attributes=$_POST['QuizLibrary'];
                // if just set state to published then reset date
                if ($preStatus != $model->quiz_status_id && $model->quiz_status_id == Cons::POLICY_STATUS_PUBLISHED) {
                    $model->publish_date = date('Y-m-d H:i:s');
                }

                if($model->validate())
                {
                    if ($model->save()) {
                        $questionList = getPost('QuizQuestion');
                        $errorMessages = Quiz_Ex::saveQuizQuestion($model->quiz_library_id,NULL,NULL,NULL,$questionList);
                        if (empty($errorMessages)) {
							Yii::app()->getUser()->setFlash('general', 'Information saved successfully.');
							if ($saveAndExit) {
								$this->redirect(url('quizlibrary/list'));
							}
                        }
                    } else {
                        $model->addError('quiz_title', "Failed to save changes. Try again.");
                        Yii::log(print_r($model->getErrors(), true), 'error', 'QuizlibraryController.actionEdit');
                    }
                }
            } else {
                if (!$isNew) {
                    $criteria = new CDbCriteria(array('order' => 'sort_order ASC'));
                    $questionList = QuizQuestion::model()->findAllByAttributes(array('quiz_library_id' => $id,'status'=>1),$criteria);
                }
            }
            $this->pageTitle=Yii::app()->name.' - Edit Quiz Library';
            $editUrl = url('quizlibrary/edit',array('id'=>Obscure::encode($model->quiz_library_id)));
            $this->render('//quiz/vEdit',array('editUrl'=>$editUrl,'model'=>$model,'questionList'=>$questionList,'errorMessages'=>$errorMessages));
        } else {
            badurl('Your role is not authorized to edit the quiz library document.');
        }
    }
}