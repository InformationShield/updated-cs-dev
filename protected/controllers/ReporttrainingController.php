<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class ReporttrainingController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + getcompliance,getcompliancedetail',
            'ajaxOnly + getcompliance,getcompliancedetail',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('compliance','getcompliance','exportcompliance','printcompliance',
                    'compliancedetail','getcompliancedetail','exportcompliancedetail','printcompliancedetail'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionCompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Training Compliance');
        $count = Training_Ex::getTrainingCountsByStatus(Utils::contextCompanyId(),3);
        $height = 24;
        if ($count > 0) {
            $count++;
            $height = ($count > 10) ? 24*10 : 24*$count;
        }
        $height += 5;
        $exportUrl = url('reporttraining/exportcompliance',array('export'=>'csv'));
        $printUrl = url('reporttraining/printcompliance');
        $this->render('vCompliance',array('height'=>$height,'exportUrl'=>$exportUrl,'printUrl'=>$printUrl));
    }

    public function actionGetcompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Training Compliance', null, 'json');
        $mReportsTraining = new mReportsTraining();
        $json = $mReportsTraining->ComplianceReport(Utils::contextCompanyId());
        echo $json;
    }

    public function actionExportcompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Training Compliance');
        $export = getGet('export',false);
        if ($export=='csv') {
            $mReportsTraining = new mReportsTraining();
            $data = $mReportsTraining->ComplianceReport(Utils::contextCompanyId(),false,true);
            if (count($data)) {
                mDownloadFile::outputCSV($data);
                Yii::app()->end();
            }
        }
    }

    public function actionPrintcompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Training Compliance');
        $mReportsTraining = new mReportsTraining();
        $data = $mReportsTraining->ComplianceReport(Utils::contextCompanyId(),false,true);
        $this->layout = false;
        $this->render('vCompliancePrint',array('data'=>$data));
    }

    public function actionCompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Training Compliance');
        $id = $this->paramObscureId();
        $training = Training::model()->findByPk($id);
        $dataUrl = url('reporttraining/getcompliancedetail',array('id'=>Obscure::encode($id)));
        $exportUrl = url('reporttraining/exportcompliancedetail',array('id'=>Obscure::encode($id),'export'=>'csv'));
        $printUrl = url('reporttraining/printcompliancedetail',array('id'=>Obscure::encode($id)));
        $this->render('vComplianceDetail',array('training'=>$training,'dataUrl'=>$dataUrl,'exportUrl'=>$exportUrl,'printUrl'=>$printUrl));
    }

    public function actionGetcompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Training Compliance', null, 'json');
        $id = $this->paramObscureId();
        $mReportsTraining = new mReportsTraining();
        $json = $mReportsTraining->ComplianceReportDetail(Utils::contextCompanyId(),$id,true,false);
        echo $json;
    }

    public function actionExportcompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Training Compliance');
        $id = $this->paramObscureId();
        $training = Training::model()->findByPk($id);
        $export = getGet('export',false);
        if ($export=='csv') {
            $mReportsTraining = new mReportsTraining();
            $data = $mReportsTraining->ComplianceReportDetail(Utils::contextCompanyId(),$id,false,true,$training);
            if (count($data)) {
                mDownloadFile::outputCSV($data);
                Yii::app()->end();
            }
        }
    }

    public function actionPrintcompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Training Compliance');
        $id = $this->paramObscureId();
        $training = Training::model()->findByPk($id);
        $mReportsTraining = new mReportsTraining();
        $data = $mReportsTraining->ComplianceReportDetail(Utils::contextCompanyId(),$id,false,true);
        $this->layout = false;
        $this->render('vComplianceDetailPrint',array('training'=>$training,'data'=>$data));
    }
}