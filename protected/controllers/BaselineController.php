<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class BaselineController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + get,delete', // we only allow deletion via POST request
			'ajaxOnly + get,view,delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','view','edit','delete','get', 'CopyBaseline'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	public function actionList()
	{
		mPermission::checkAccess($this, 'Compliance', 'Manage Baselines');
		$listUrl = url('baseline/get');
		$this->render('vList', array('listUrl' => $listUrl));
	}

	public function actionGet()
	{
		mPermission::checkAccess($this, 'Compliance', 'Manage Baselines', Cons::ROLE_AUTHOR, 'json');
		$companyId = Utils::contextCompanyId();
		$s = $this->getSearchParams();
		$json = Baseline_Ex::getBaselineAsessmentJson($companyId, $s['pageSize'], $s['offsetBy'], $s['search'], $s['searchLogic'], $s['sort']);
		echo $json;
	}

	// overriden from base and called by $this->renderView() to get values and skipEncode for View feature
	protected function getViewValues($id,&$skipEncode)
	{
		mPermission::checkAccess($this, 'Compliance', 'Manage Baselines', Cons::ROLE_AUTHOR, 'silent');
		$companyId = Utils::contextCompanyId();
		$values = Baseline_Ex::getViewValues($companyId, $id);
		$skipEncode = array('Description');
		return $values;
	}
	public function actionView()
	{
		mPermission::checkAccess($this, 'Compliance', 'Manage Baselines', Cons::ROLE_AUTHOR, 'json');
		$this->renderView(Cons::LIC_TRIAL, Cons::ROLE_AUTHOR); // base GridViewController method calls override method getViewValues()
	}

	// Baseline Delete
	// override method called by ajaxDelete()
	protected function doDelete($id,$markStatusOnly=true)
	{
		$companyId = Utils::contextCompanyId();
		return $this->doModelDelete($id,Baseline::model(),array('baseline_id' => $id, 'company_id' => $companyId),$markStatusOnly);
	}
	public function actionDelete()
	{
		mPermission::checkAccess($this, 'Compliance', 'Manage Baselines', Cons::ROLE_AUTHOR, 'json');
		$id = $this->paramObscureId();
        $baseline = Baseline_Ex::GetBaseline($id);
		if ($baseline['baseline_name'] == 'default') {
			$results = array();
			$results['status'] = 'error';
			$results['message'] = 'You can not delete your default control baseline.';
			$json = json_encode($results);
			echo $json;
		} else {
			echo $this->doDelete($id, true);
            $baseline = Baseline_Ex::getCurrentBaselineId(Utils::contextCompanyId());
		}
	}

	public function actionEdit()
	{
		mPermission::checkAccess($this, 'Compliance', 'Manage Baselines', Cons::ROLE_AUTHOR);
		$id = $this->paramObscureId(false); // will exit if bad param for security reasons
		$companyId = Utils::contextCompanyId();
		if (isset($id) && $id > 0) {
			$model = Baseline::model()->findByAttributes(array('baseline_id' => $id, 'company_id' => $companyId));
			if (!isset($model)) {
				badurl('User attempting wrongful access.');
			}
		} else {
			$model=new Baseline;
			$model->created_on = DateHelper::FormatDateTimeString('now','Y/m/d H:i:s');
			$model->user_id = userId();
			$model->company_id = $companyId;
		}
		if (isset($_POST['Baseline'])) {
			$model->attributes=$_POST['Baseline'];
			$model->user_id = userId();
			$model->company_id = $companyId;
			if ($model->validate()) {
				if ($model->save()) {
					Baseline_Ex::setCurrentBaseline($companyId,$model->baseline_id);
					if (isset($_POST['Baseline']['special_type']) && $_POST['Baseline']['special_type'] == 1) {
						Baseline_Ex::setCyberRiskBaseline($model->baseline_id);
					}
					$this->redirect(url('controlbaseline/list'));
				} else {
					Yii::log(print_r($model->getErrors(), true), 'error', 'BaselineController.actionEdit');
				}
			}
		}
		$this->render('vEdit',array('model'=>$model));
	}

	public function actionCopyBaseline()
    {
        mPermission::checkAccess($this, 'Compliance', 'Manage Baselines');
        $id = $this->paramObscureId(false);
        $baselines = mBaseline::getStockBaselines();
        if ($id) {
            $baseline_name = getPost('baseline_name');
            $baseline_description = getPost('baseline_description');
            mBaseline::copyBaseLine($id, $baseline_name, $baseline_description);
            $this->redirect(url('baseline/list'));
            return;
        }
        $this->renderPartial('vPickBaseline', array('baselines'=>$baselines));
    }
}