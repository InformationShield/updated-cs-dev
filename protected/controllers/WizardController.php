<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class WizardController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + ', // we only allow deletion via POST request
            'ajaxOnly + ',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('control','complete', 'inherentriskscore'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionControl()
    {
		mPermission::checkAccess($this, 'Compliance', 'Control Wizard');
        $invite_guid = getSessionVar('invite_guid');
        if (!empty($invite_guid)) {
            $this->redirect(array('invite/open'));
        }

        $showAllQuestions = getGet('all',false);
        $id = $this->paramObscureId(false); // will exit if bad param for security reasons

        $company_id = Utils::contextCompanyId();
        $company = Company::model()->findByPk($company_id);
        if (isset($company)) {
            if (isset($id) && $id > 0) {
                $model = CompanyInfo::model()->findByPk($id);
                if (!isset($model)) {
                    badurl('User attempting wrongful access.');
                }
                $model->isNewRecord = false;
            } else {
                $model = CompanyInfo::model()->findByAttributes(array('company_id' => $company_id));
                if (!isset($model)) {
                    $model = new CompanyInfo;
                    $model->isNewRecord = true;
                    $model->country = "US";
                } else {
                    $model->isNewRecord = false;
                }
            }
            // let's keep the names in sync between the private and public data
            // Company_info is private and Company is public (meaning user can see those fields.)
            $model->business_name = $company->name;

            if (isset($_POST['CompanyInfo'])) {
                $model->attributes = $_POST['CompanyInfo'];
                $model->company_id = $company_id;
                $model->user_id = userId();
                $model->updated_on = date('Y-m-d H:i:s');
                if ($model->isNewRecord) {
                    $model->created_on = date('Y-m-d H:i:s');
                }

                if ($model->validate()) {
                    if ($model->save()) {
                        $company->name = $model->business_name;
                        if (!$company->save()) {
                            Yii::log('Interesting failed to save company.name to db.', 'error', 'WizardController.actionControl');
                        }
                        $mWizardRules = new mWizardRules();

                        // return count of controls add to the baseline, note: renames Company X to the business_name
                        $count = $mWizardRules->copyControlsByRules($model,userId());

                        // calculate total risk score and save
						$wizardInherentRiskScore = Wizard_Ex::calculateWizardInherentRisk($model->attributes);

						$this->redirect(url('wizard/complete',array('count'=>$count)));
                        return;
                    } else {
                        $model->addError('business_name', "Failed to save wizard results. Try again or contact technical support.");
                        Yii::log(print_r($model->getErrors(), true), 'error', 'WizardController.actionControl');
                    }
                }
            }
            $this->pageTitle = Yii::app()->name . ' - Control Baseline';
            $this->render('vWizard', array('model' => $model, 'showAllQuestions' => $showAllQuestions));
        } else {
            Yii::log('Company is missing! Should not be!', 'error', 'WizardController.actionControl');
            $this->redirect(url('site/accountoptions'));
        }
    }

    public function actionComplete()
    {
		mPermission::checkAccess($this, 'Compliance', 'Control Wizard', Cons::ROLE_AUTHOR);
        $count = getGet('count',0);
		$model = CompanyInfo::model()->findByAttributes(array('company_id' => Utils::contextCompanyId()));
		if (isset($model)) {
			$wizardInherentRiskScore = Wizard_Ex::calculateWizardInherentRisk($model->attributes);
		}
        $this->render('vComplete',array('count'=>$count, 'wizardInherentRiskScore' => $wizardInherentRiskScore));
    }
    
    public function actionInherentriskscore()
    {
		mPermission::checkAccess($this, 'Compliance', 'Cyber Inherent Risk');
        $model = CompanyInfo::model()->findByAttributes(array('company_id' => Utils::contextCompanyId()));
        if (isset($model)) {
            $companyName = $model->business_name;
            $wizardInherentRiskScore = Wizard_Ex::calculateWizardInherentRisk($model->attributes);
        } else {
            $companyName = Utils::contextCompanyName();
            $wizardInherentRiskScore = Wizard_Ex::calculateWizardInherentRisk();
        }
        
        $this->render('vInherentRiskScore',
			array('wizardInherentRiskScore' => $wizardInherentRiskScore, 'companyName' => $companyName, 'updatedOn'=>$model->updated_on));
    }
}