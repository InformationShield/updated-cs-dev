<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class ReportfilterController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + get, delete, list, getcontrolsummary, getdetailchart',
            'ajaxOnly + get, delete, list, getcontrolsummary, getdetailchart',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(
					'list','view','copy','edit','delete','get',
					'controlsummary','getcontrolsummary',
					'detailchart','getdetailchart',
				),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	public function actionList()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Summary');
		$mReportFilter = new mReportFilter();
		$reportFilter = $mReportFilter->getList(Utils::contextCompanyId());
		echo mGrid::toJsonGrid($reportFilter);
	}

	protected function doDelete($id,$markStatusOnly=false)
	{
		return $this->doModelDelete($id, ControlFilter::model(), array('control_filter_id' => $id), $markStatusOnly);
	}
	public function actionDelete()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline', Cons::ROLE_AUTHOR, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
	}

	public function actionEdit()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline', Cons::ROLE_AUTHOR);
		$id = $this->paramObscureId(false); // will exit if bad param for security reasons

		$mReportFilter = new mReportFilter();
		if (isset($id) && $id > 0) {
			$filter = $mReportFilter->getFilter($id);
			if (!$filter) {
				badurl('User attempting wrongful access.');
			}
		}

		if ($_POST) {
			// add save code here
			if ($mReportFilter->saveFilter($id, $_POST['mReportFilter'])) {
				$this->redirect(url('reportfilter/controlsummary', array('id'=>Obscure::encode($mReportFilter->id))));
			}
		}

		$this->render('vEdit',array('model'=>$mReportFilter));
	}

	public function actionControlSummary()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Summary');
		$filter_id = getGet('id');
		if ($filter_id) {
			setSessionVar('lastFilterId', $filter_id);
		} else {
			$filter_id = getSessionVar('lastFilterId', 1);
		}
		$this->render('vControlSummary', array('filter_id'=>obscure($filter_id)));
	}

	public function actionGetControlSummary()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Summary', null, 'partial');
		$filter_id = $this->paramObscureId(false); // will exit if bad param for security reasons
		if ($filter_id > 0) {
			setSessionVar('lastFilterId', $filter_id);
		} else {
			$filter_id = getSessionVar('lastFilterId', 1);
		}
		$baseline_id = Baseline_Ex::getCurrentBaselineId();

		$mReportFilter = new mReportFilter($filter_id);
		$mReportFilter->getSummaryData(Utils::contextCompanyId(), $baseline_id, $filter_id);
		$this->renderPartial('_vControlSummary',
			array('model'=>$mReportFilter, 'baseline_name'=>Baseline_Ex::getCurrentBaselineName()));
	}

	public function actionDetailChart()
	{
		mPermission::checkAccess($this, 'Reports', 'Control Compliance');
		$filter_id = $this->paramObscureId(false); // will exit if bad param for security reasons
		$status_id = getGet('status');
		$mReportFilter = new mReportFilter($filter_id);
		if (is_null($status_id)) {
			$status_id = Cons::CONTROL_COMPLETE; // assume complete
		}
		$this->render('vDetailChart', array('mReportFilter'=>$mReportFilter, 'status_id'=>$status_id));
	}

	public function actionGetDetailChart()
	{
		mPermission::checkAccess($this, 'Reports', 'Control Compliance', null, 'partial');
		$filter_id = $this->paramObscureId(false); // will exit if bad param for security reasons
		$status_id = getGet('status');
		if (is_null($status_id)) {
			$status_id = 5; // assume complete
		}
		$baseline_id = Baseline_Ex::getCurrentBaselineId();
		$mReportFilter = new mReportFilter();
		$mReportFilter->getDetailData(Utils::contextCompanyId(), $baseline_id, $status_id, $filter_id);
		$this->renderPartial('_vDetailChart',array('model'=>$mReportFilter,'baseline_name'=>Baseline_Ex::getCurrentBaselineName()));
	}
}