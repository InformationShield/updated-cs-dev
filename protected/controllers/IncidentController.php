<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class IncidentController extends GridViewController
{
    public $myUserMode = false;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','delete','view','edit'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		if ($this->myUserMode) {
            if (getSessionVar('site_type') == 'UserSite') {
                mPermission::checkAccess($this, null, 'My Incidents');
            } else {
                mPermission::checkAccess($this, "Incidents", 'My Incidents');
            }
		} else {
			mPermission::checkAccess($this, 'Incidents', 'Manage Incidents');
		}
		$listUrl = url(Yii::app()->controller->id.'/get');
		$this->render('//incident/vList', array('listUrl'=>$listUrl));
    }

    public function actionGet()
    {
        $userId = ($this->myUserMode) ? userId() : null;
		if ($this->myUserMode) {
            if (getSessionVar('site_type') == 'UserSite') {
                mPermission::checkAccess($this, null, 'My Incidents', null, 'json');
            } else {
                mPermission::checkAccess($this, "Incidents", 'My Incidents', null, 'json');
            }
		} else {
			mPermission::checkAccess($this, 'Incidents', 'Manage Incidents', null, 'json');
		}
        $s = $this->getSearchParams();
        $json = Incident_Ex::getIncidentsJson(Utils::contextCompanyId(),$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort'],null,$userId);
        echo $json;
    }

    // Incident Delete
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,Incident::model(),array('incident_id'=>$id,'company_id' => Utils::contextCompanyId()),$markStatusOnly);
    }
    protected function preDeleteCheck($id, $model, $markStatusOnly = false)
    {
        if ($this->myUserMode) {
            // only allow delete of user owned records
            if ($model->user_id != userId()) {
                return array('status'=>'error','message'=>'Not allowed to delete, because you did not report this incident.');
            }
        }
        return true;
    }
    public function actionDelete()
    {
		if ($this->myUserMode) {
            if (getSessionVar('site_type') == 'UserSite') {
                mPermission::checkAccess($this, null, 'My Incidents', null, 'json');
            } else {
                mPermission::checkAccess($this, "Incidents", 'My Incidents', null, 'json');
            }
		} else {
			mPermission::checkAccess($this, 'Incidents', 'Manage Incidents', Cons::ROLE_AUTHOR, 'json');
		}
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $userId = ($this->myUserMode) ? userId() : null;
        $values = Incident_Ex::getViewValues(Utils::contextCompanyId(), $id, $userId);
        $skipEncode = array('Description','Summary');
        return $values;
    }
    public function actionView()
    {
		if ($this->myUserMode) {
            if (getSessionVar('site_type') == 'UserSite') {
                mPermission::checkAccess($this, null, 'My Incidents');
            } else {
                mPermission::checkAccess($this, "Incidents", 'My Incidents');
            }
		} else {
			mPermission::checkAccess($this, 'Incidents', 'Manage Incidents');
		}
        if ($this->myUserMode) {
            $this->renderView(Cons::LIC_TRIAL, Cons::ROLE_USER); // base GridViewController method calls override method getViewValues()
        } else {
            $this->renderView(Cons::LIC_TRIAL, Cons::ROLE_AUDITOR); // base GridViewController method calls override method getViewValues()
        }
    }

    public function actionEdit()
    {
		if ($this->myUserMode) {
            if (getSessionVar('site_type') == 'UserSite') {
                mPermission::checkAccess($this, null, 'My Incidents');
            } else {
                mPermission::checkAccess($this, "Incidents", 'My Incidents');
            }
			$url = url('myincident/list');
		} else {
			mPermission::checkAccess($this, 'Incidents', 'Manage Incidents', Cons::ROLE_AUTHOR);
			$url = url('incident/list');
		}

        $id = $this->paramObscureId(false); // will exit if bad param for security reasons

        if (isset($id) && $id > 0) {
            $model = Incident::model()->findByAttributes(array('incident_id' => $id, 'company_id' => Utils::contextCompanyId()));
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
            if ($this->myUserMode) { // user must own the record as creator/reported by
                if ($model->user_id != userId()) {
                    badurl('User attempting wrongful access.');
                }
            }
            $model->isNewRecord = false;
        } else {
            $model=new Incident;
            $model->user_id = userId();
            $model->created_on = DateHelper::FormatDateTimeString('now','Y/m/d H:i:s');
            $model->isNewRecord = true;
        }

        if(isset($_POST['Incident']))
        {
            $model->attributes=$_POST['Incident'];
            $model->company_id = Utils::contextCompanyId();
            $model->updated_on = DateHelper::FormatDateTimeString('now','Y/m/d H:i:s');
            if($model->validate())
            {
                if ($model->save()) {
                    $this->redirect($url);
                    return;
                } else {
                    $model->addError('incident_summary', "Failed to save changes. Try again.");
                    Yii::log(print_r($model->getErrors(), true), 'error', 'IncidentController.actionEdit');
                }
            }
        }
        $this->pageTitle=Yii::app()->name.' - Edit Incident';
        $this->render('//incident/vEdit',array('model'=>$model));
    }
}