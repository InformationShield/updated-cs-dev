<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class PolicyController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticatpolicyed user to perform 'create' and 'update' actions
                'actions'=>array('list','get','delete','view','edit','download'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policies');
        $status_id = getGet('status');
        if ($status_id !== null) {
            $listUrl = url('policy/get', array('status' => $status_id));
        } else {
            $listUrl = url('policy/get');
        }
		$this->render('vList', array('listUrl'=>$listUrl));
    }

    public function actionGet()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policies', null, 'json');
        $s = $this->getSearchParams();
        $status_id = getGet('status');
        $json = Policy_Ex::getPolicyJson(Utils::contextCompanyId(),$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort'],$status_id);
        echo $json;
    }

    public function actionDownload()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policies', Cons::ROLE_AUDITOR);
        // will send headers and exit or show a message
        $this->downloadFile(mDownloadFile::Policy,Cons::LIC_TRIAL,Cons::ROLE_AUDITOR);
    }

    // Policy Delete
    // can override to do post delete actions
    protected function postDelete($id,$model,$markStatusOnly=false) {
        return Policy_Ex::removeFile($model);
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,Policy::model(),array('policy_id'=>$id,'company_id' => Utils::contextCompanyId()),$markStatusOnly);
    }
    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policies', Cons::ROLE_AUTHOR, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id, true);
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = Policy_Ex::getViewValues(Utils::contextCompanyId(),$id);
        $skipEncode = array('Description');
        return $values;
    }
    public function actionView()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policies', Cons::ROLE_AUDITOR);
        $this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUDITOR); // base GridViewController method calls override method getViewValues()
    }

    public function actionEdit()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policies', Cons::ROLE_AUTHOR);

        $id = $this->paramObscureId(false); // will exit if bad param for security reasons

        if (isset($id) && $id > 0) {
            $model = Policy::model()->findByAttributes(array('policy_id' => $id, 'company_id' => Utils::contextCompanyId()));
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
            $model->isNewRecord = false;
        } else {
            $model=new Policy;
            $model->isNewRecord = true;
        }

        if(isset($_POST['Policy']))
        {
            $model->attributes=$_POST['Policy'];
            $model->company_id = Utils::contextCompanyId();
            $model->user_id = userId();
            if (!empty($model->publish_date)) {
                $model->publish_date = DateHelper::FormatDateTimeString($model->publish_date,'Y-m-d');
            }
            if (!empty($model->expiry_date)) {
                $model->expiry_date = DateHelper::FormatDateTimeString($model->expiry_date,'Y-m-d');
            }
            if ($model->isNewRecord) {
                $model->doc_guid = Utils::create_guid();
                $model->publish_date = date('Y-m-d');
            }

            $fileUrl = false;
            $tmpFileFullPath = false;
            // handle Policy file upload
            if (isset($_FILES['Policy'])) {
                if (!empty($_FILES['Policy']['name'])) {
                    $tmpFileFullPath = $_FILES['Policy']['tmp_name'];
                    $model->file_size = $_FILES['Policy']['size'];
                    $model->file_type = $_FILES['Policy']['type'];
                    $model->file_name = $_FILES['Policy']['name'];
                    $model->file_ext = pathinfo($_FILES['Policy']['name'], PATHINFO_EXTENSION);
                }
            }

            if($model->validate())
            {
                if ($model->isNewRecord) {
                    if ($tmpFileFullPath !== false) {
                        $fileUrl = Policy_Ex::saveFile($tmpFileFullPath, $model);
                        if ($fileUrl !== false) {
                            if ($model->save()) {
                                $this->redirect(url('policy/list'));
                                return;
                            } else {
                                $model->addError('doc_title', "Failed to save policy document. Try again.");
                            }
                        } else {
                            $model->addError('file_name', "Failed to upload Policy File.");
                        }
                    } else {
                        $model->addError('file_name', "Policy File is required. Choose a file to upload.");
                    }
                } else if ($model->save()) {
                    if ($tmpFileFullPath !== false) {
                        $fileUrl = Policy_Ex::saveFile($tmpFileFullPath, $model);
                        if ($fileUrl !== false) {
                            $this->redirect(url('policy/list'));
                            return;
                        } else {
                            $model->addError('file_name', "Failed to upload Policy File.");
                        }
                    } else {
                        $this->redirect(url('policy/list'));
                        return;
                    }
                } else {
                    $model->addError('doc_title', "Failed to save changes. Try again.");
                    Yii::log(print_r($model->getErrors(), true), 'error', 'PolicyController.actionEdit');
                }
            }
        }
        if (!empty($model->publish_date)) {
            $model->publish_date = DateHelper::FormatDateTimeString($model->publish_date,'m/d/Y');
        }
        if (!empty($model->expiry_date)) {
            $model->expiry_date = DateHelper::FormatDateTimeString($model->expiry_date,'m/d/Y');
        }
        $this->render('vEdit',array('model'=>$model));
    }
}