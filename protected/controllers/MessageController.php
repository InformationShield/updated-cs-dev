<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class MessageController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + get,delete,messagemenu', // we only allow deletion via POST request
            'ajaxOnly + get,view,delete,messagemenu',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('messagemenu'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list', 'get', 'view', 'delete'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		$this->render('vList');
    }
    
    public function actionGet()
    {
		mPermission::checkLicenseRole($this, Cons::LIC_FREE, Cons::ROLE_AUTHOR, 'json');
        $s = $this->getSearchParams();
        $user_id = Yii::app()->user->id;
        $json = Message_Ex::getMessagesJson($user_id,1,$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $user_id = Yii::app()->user->id;
        $values = Message_Ex::getViewValues($user_id,$id);
        $skipEncode = array('Title','Message');
        Message_Ex::setRead($id);
        return $values;
    }

    public function actionView()
    {
        $this->renderView(Cons::LIC_FREE,Cons::ROLE_USER); // base GridViewController method calls override method getViewValues()
    }

    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=true)
    {
        return $this->doModelDelete($id,Message::model(),array('message_id'=>$id),$markStatusOnly);
    }
    public function actionDelete()
    {
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }

    public function actionMessagemenu()
    {
        $messages = array();
        $right_messages = array();
        if (!user()->isGuest) {
            $companyCount = Utils::contextCompanyCount();
            if ($companyCount > 0) {
                if (Utils::isRoleAdmin() || Utils::isRoleAuthor() || Utils::isRoleAuditor()) {
                    $user_id = Yii::app()->user->id;
                    $messages = Message_Ex::getUserMessages($user_id);
                    $right_messages = array(
                        'label' => 'Messages',
                        'url' => Yii::app()->createAbsoluteUrl("/message/list"),
                        'count' => count($messages)
                    );
                }
            }
        }
        $this->renderPartial('vMessageMenu', array('messages' => $messages, 'right_messages' => $right_messages));
    }
}