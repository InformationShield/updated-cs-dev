<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class PolicylibraryController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete,copy', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view,copy',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','view','download','copy'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'edit' and 'delete' actions
                'actions'=>array('delete','edit'),
                'expression'=>'!user()->isGuest && Utils::isSuper()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policy Library');
		$this->render('vList');
    }

    public function actionGet()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policy Library', null, 'json');
        $s = $this->getSearchParams();
        $json = PolicyLibrary_Ex::getPolicyLibraryJson($s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    public function actionDownload()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policy Library', Cons::ROLE_AUTHOR);
        // will send headers and exit or show a message
        $this->downloadFile(mDownloadFile::PolicyLibrary,Cons::LIC_TRIAL,Cons::ROLE_AUTHOR);
    }

    // PolicyLibrary Delete
    // can override to do post delete actions
    protected function postDelete($id,$model,$markStatusOnly=false) {
        return PolicyLibrary_Ex::removeFile($model);
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,PolicyLibrary::model(),array('policy_library_id'=>$id),$markStatusOnly);
    }
    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policy Library', Cons::ROLE_ADMIN, 'json');
        $markStatusOnly = true;
        if (Utils::isSuper()) {
			$id = $this->paramObscureId(); // will exit if bad param for security reasons
			echo $this->doDelete($id);
        } else {
            $status = 'error';
            $message = 'Your role is not authorized to delete the policy library document.';
            echo json_encode(array('status' => $status, 'message'=>$message));
        }
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = PolicyLibrary_Ex::getViewValues($id);
        $skipEncode = array('Description');
        return $values;
    }
    public function actionView()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policy Library', Cons::ROLE_AUTHOR);
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		$policyLibrary = PolicyLibrary::model()->findByAttributes(array('policy_library_id' => $id));
		if ($policyLibrary->license_level <= getSessionVar('license_level')) {
			$this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUTHOR); // base GridViewController method calls override method getViewValues()
		} else {
			echo "<div><strong>Available in the paid version.</strong></div>";
		}

    }

    // PolicyLibrary Copy
    // override method called by ajaxCopy()
    // function is to copy and returns params for json staus and message
    // can be overridden for other use cases
    protected function doCopy($id)
    {
        $status = PolicyLibrary_Ex::copyPolicy($id,Utils::contextCompanyId(),userId(),$message);
        return array('status'=>$status,'message'=>$message);
    }
    public function actionCopy()
    {
		mPermission::checkAccess($this, 'Policy Documents', 'Policy Library', Cons::ROLE_AUTHOR, 'json');
        echo $this->ajaxCopy(Cons::LIC_TRIAL,Cons::ROLE_AUTHOR);
    }

    public function actionEdit()
    {
        if (Utils::isSuper()) {
            $id = $this->paramObscureId(false); // will exit if bad param for security reasons

            if (isset($id) && $id > 0) {
                $model = PolicyLibrary::model()->findByAttributes(array('policy_library_id' => $id));
                if (!isset($model)) {
                    badurl('User attempting wrongful access.');
                }
                $model->isNewRecord = false;
            } else {
                $model=new PolicyLibrary;
                $model->isNewRecord = true;
            }

            if(isset($_POST['PolicyLibrary']))
            {
                $model->attributes=$_POST['PolicyLibrary'];
                if (!empty($model->publish_date)) {
                    $model->publish_date = DateHelper::FormatDateTimeString($model->publish_date,'Y-m-d');
                }
                if (!empty($model->expiry_date)) {
                    $model->expiry_date = DateHelper::FormatDateTimeString($model->expiry_date,'Y-m-d');
                }
                if ($model->isNewRecord) {
                    $model->doc_guid = Utils::create_guid();
                    $model->publish_date = date('Y-m-d');
                }

                $fileUrl = false;
                $tmpFileFullPath = false;
                // handle PolicyLibrary file upload
                if (isset($_FILES['PolicyLibrary'])) {
                    if (!empty($_FILES['PolicyLibrary']['name'])) {
                        $tmpFileFullPath = $_FILES['PolicyLibrary']['tmp_name'];
                        $model->file_size = $_FILES['PolicyLibrary']['size'];
                        $model->file_type = $_FILES['PolicyLibrary']['type'];
                        $model->file_name = $_FILES['PolicyLibrary']['name'];
                        $model->file_ext = pathinfo($_FILES['PolicyLibrary']['name'], PATHINFO_EXTENSION);
                    }
                }

                if($model->validate())
                {
                    if ($model->isNewRecord) {
                        if ($tmpFileFullPath !== false) {
                            $fileUrl = PolicyLibrary_Ex::saveFile($tmpFileFullPath, $model);
                            if ($fileUrl !== false) {
                                if ($model->save()) {
                                    $this->redirect(url('policylibrary/list'));
                                    return;
                                } else {
                                    $model->addError('doc_title', "Failed to save policy library document. Try again.");
                                }
                            } else {
                                $model->addError('file_name', "Failed to upload Policy Library File.");
                            }
                        } else {
                            $model->addError('file_name', "Policy Library File is required. Choose a file to upload.");
                        }
                    } else if ($model->save()) {
                        if ($tmpFileFullPath !== false) {
                            $fileUrl = PolicyLibrary_Ex::saveFile($tmpFileFullPath, $model);
                            if ($fileUrl !== false) {
                                $this->redirect(url('policylibrary/list'));
                                return;
                            } else {
                                $model->addError('file_name', "Failed to upload Policy Library File.");
                            }
                        } else {
                            $this->redirect(url('policylibrary/list'));
                            return;
                        }
                    } else {
                        $model->addError('doc_title', "Failed to save changes. Try again.");
                        Yii::log(print_r($model->getErrors(), true), 'error', 'PolicylibraryController.actionEdit');
                    }
                }
            }
            if (!empty($model->publish_date)) {
                $model->publish_date = DateHelper::FormatDateTimeString($model->publish_date,'m/d/Y');
            }
            if (!empty($model->expiry_date)) {
                $model->expiry_date = DateHelper::FormatDateTimeString($model->expiry_date,'m/d/Y');
            }
            $this->render('vEdit',array('model'=>$model));
        } else {
            badurl('Your role is not authorized to edit the policy library document.');
        }
    }
}