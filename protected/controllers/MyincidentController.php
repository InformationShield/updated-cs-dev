<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');
require_once(Yii::app()->basePath.'/controllers/IncidentController.php');

class MyincidentController extends IncidentController
{
    public $myUserMode = true;
    // simple override to run the IncidentController in user mode where records must have been owned by user
}