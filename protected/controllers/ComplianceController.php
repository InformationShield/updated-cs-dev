<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class ComplianceController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + getriskscoring, getcontrolsummary, getdetailchart, getroles, getpolicysummary, getpolicytrackingsummary, getcompanies', // we only allow deletion via POST request
            'ajaxOnly + getriskscoring, getcontrolsummary, getdetailchart, getroles, getpolicysummary, getpolicytrackingsummary, getcompanies',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('riskscoring','getriskscoring',
                    'controlsummary','getcontrolsummary',
					'detailchart','getdetailchart',
                    'policysummary','getpolicysummary',
                    'policytrackingsummary', 'getpolicytrackingsummary',
                    'roles','getroles','companies','getcompanies'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionCompanies()
    {
		mPermission::checkAccess($this, 'Company', 'Companies Risk Scores');
        $companies = mCompanyContext::getUserCompaniesInRole(Cons::ROLE_AUDITOR);
        $dataUrl = null;
        $count = count($companies);
        if ($count > 0) {
            $height = ($count > 10) ? 24*11 : 24*($count+1);
            $c = array_shift($companies);
            $baseline = Baseline_Ex::GetDefaultBaseline($c['company_id']);
            $baseline_id = $baseline->baseline_id;
            $dataUrl = url('compliance/getriskscoring', array('id' => Obscure::encode($c['company_id']),'baseline'=>Obscure::encode($baseline_id)));
        } else {
            $height = 24*2;
        }
        $height += 5;
        $this->render('vCompanies',array('dataUrl'=>$dataUrl,'height'=>$height));
    }

    public function actionGetcompanies()
    {
		mPermission::checkAccess($this, 'Company', 'Companies Risk Scores', null, 'json');
        $companies = mCompanyContext::getUserCompaniesInRole(Cons::ROLE_AUDITOR);
        $model = new mScoring();
        $json = $model->getCompaniesRiskScoringJson($companies);
        echo $json;
    }

    public function actionRiskScoring()
    {
		mPermission::checkAccess($this, 'Compliance', 'Risk Scoring Report');
        $dataUrl = url('compliance/getriskscoring');
        $id = $this->paramObscureId(false); // will exit if bad param for security reasons
        $baseline_id = $this->paramObscureId(false,'baseline');
        if (isset($id) && $id > 0) {
            if (isset($baseline_id) && $baseline_id > 0) {
                $dataUrl = url('compliance/getriskscoring',array('id'=>Obscure::encode($id),'baseline'=>Obscure::encode($baseline_id)));
            } else {
                $dataUrl = url('compliance/getriskscoring',array('id'=>Obscure::encode($id)));
            }
        }
        $this->render('vRiskScoring',array('dataUrl'=>$dataUrl));
    }

    public function actionGetRiskScoring()
    {
		mPermission::checkAccess($this, 'Compliance', 'Risk Scoring Report', null, 'partial');
        $company_id = Utils::contextCompanyId();
        $company = null;
        $baseline = null;
        $id = $this->paramObscureId(false); // will exit if bad param for security reasons
        $baseline_id = $this->paramObscureId(false,'baseline');
        if (isset($id) && $id > 0) {
            $company_id = $id;
            $company = Company::model()->findByPk($company_id);
            if (isset($baseline_id) && $baseline_id > 0) {
                $baseline = Baseline::model()->findByPk($baseline_id);
            } else {
                $baseline = Baseline_Ex::GetDefaultBaseline($company_id);
                $baseline_id = $baseline->baseline_id;
            }
        } else if (isset($baseline_id) && $baseline_id > 0) {
            $baseline = Baseline::model()->findByPk($baseline_id);
        } else {
            $baseline_id = Baseline_Ex::getCurrentBaselineId();
            $baseline = Baseline::model()->findByPk($baseline_id);
        }
        $model = new mScoring();
        $model->getRiskScoring($company_id,$baseline_id);
        $this->renderPartial('_vRiskScoring',array('model'=>$model,'company'=>$company,'baseline'=>$baseline));
    }

    public function actionControlSummary()
    {
		mPermission::checkAccess($this, 'Compliance', 'Control Summary');
        $baselineId = $this->paramObscureId(false); // will not exit if bad param for security reasons
        $this->render('vControlSummary', array('baselineId' => Obscure::encode($baselineId)));
    }

    public function actionGetControlSummary()
    {
		mPermission::checkAccess($this, 'Compliance', 'Control Summary', null, 'partial');
        $view = getGet('view','_vControlSummary');
        $baseline_id = $this->paramObscureId(false); // will not exit if bad param for security reasons
        if (!$baseline_id) {
            $baseline_id = Baseline_Ex::getCurrentBaselineId();
        }

        $model = new mScoring();
        $model->getControlSummary(Utils::contextCompanyId(),$baseline_id);
        $this->renderPartial($view,array('model'=>$model,'baseline_name'=>Baseline_Ex::getCurrentBaselineName()));
    }

//	public function actionComplianceList()
//	{
//		mPermission::checkAccess($this, 'Reports', 'Quiz Compliance');
//		$id = $this->paramObscureId();
//		$quiz = Quiz::model()->findByPk($id);
//		$dataUrl = url('reportquiz/getcompliancedetail',array('id'=>Obscure::encode($id)));
//		$this->render('vComplianceDetail',array('quiz'=>$quiz,'dataUrl'=>$dataUrl));
//	}
//
	public function actionDetailChart()
    {
		mPermission::checkAccess($this, 'Reports', 'Control Compliance');
        $status_id = getGet('status');
        if (is_null($status_id)) {
            $status_id = 5; // assume complete
        }
        $this->render('vDetailChart',array('status_id'=>$status_id));
    }

    public function actionGetDetailChart()
    {
		mPermission::checkAccess($this, 'Reports', 'Control Compliance', null, 'partial');
        $status_id = getGet('status');
        if (is_null($status_id)) {
            $status_id = 5; // assume complete
        }
        $baseline_id = Baseline_Ex::getCurrentBaselineId();
        $model = new mScoring();
        $model->getRiskScoring(Utils::contextCompanyId(),$baseline_id,$status_id);
        $this->renderPartial('_vDetailChart',array('model'=>$model,'baseline_name'=>Baseline_Ex::getCurrentBaselineName()));
    }

    public function actionRoles()
    {
		mPermission::checkAccess($this, 'Compliance', 'Roles Summary');
        $this->render('vRoles');
    }

    public function actionGetRoles()
    {
		mPermission::checkAccess($this, 'Compliance', 'Roles Summary', null, 'partial');
        $view = getGet('view','_vRoles');
        $model = new mScoring();
        $model->getRoleSummary(Utils::contextCompanyId());
        $this->renderPartial($view,array('model'=>$model));
    }

    public function actionPolicySummary()
    {
		mPermission::checkAccess($this, 'Compliance', 'Policy Summary');
        $this->render('vPolicySummary');
    }

    public function actionGetPolicySummary()
    {
		mPermission::checkAccess($this, 'Compliance', 'Policy Summary', null, 'partial');
        $view = getGet('view','_vPolicySummary');
        $model = new mScoring();
        $model->getPolicySummary(Utils::contextCompanyId());
        $this->renderPartial($view,array('model'=>$model));
    }

    public function actionPolicyTrackingSummary()
    {
		mPermission::checkAccess($this, 'Compliance', 'Policy Tracking');
        $this->render('vPolicyTrackingSummary');
    }

    public function actionGetPolicyTrackingSummary()
    {
		mPermission::checkAccess($this, 'Compliance', 'Policy Tracking', null, 'partial');
        $view = getGet('view','_vPolicyTrackingSummary');
        $model = new mScoring();
        $model->getPolicyTrackingSummary(Utils::contextCompanyId());
        $this->renderPartial($view,array('model'=>$model));
    }
}