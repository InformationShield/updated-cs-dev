<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class ControlbaselineController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + get,delete,copy', // we only allow deletion via POST request
			'ajaxOnly + get,view,delete,copy',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(
                    'list','view','copy','edit','delete','get', 'savestate', 'restorestate',
                    'download', 'downloadword','changebaseline',
                    'filterlist',
                ),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
	public function actionList()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline');
		$status_id = getGet('status');
		$filter_id = getGet('fid');
		$controlFilter = null;
        if ($filter_id) {
            $controlFilter = ControlFilter_Ex::GetFilter(Obscure::decode($filter_id));
        }
		$root_id = getGet('rid');
		$securityRoleId = getGet('srid');
		$listUrl = url('controlbaseline/get');
		if ($status_id !== null)	$listUrl .= '/status/'.$status_id;
		if ($filter_id !== null)	$listUrl .= '/fid/'.$filter_id;
		if ($root_id !== null)	$listUrl .= '/rid/'.$root_id;
		if ($securityRoleId !== null) $listUrl .= '/srid/'.$securityRoleId;
		$baselineId = Baseline_Ex::getCurrentBaselineId();
		$baseline = Baseline_Ex::getBaselineAsessment($count, Utils::contextCompanyId(), $baselineId);

        $savedBaseline = Usersetting_Ex::GetValue(UserId(), 'controlBaselineId');
        $savedSearch = Usersetting_Ex::GetJsonValue(userId(), 'controlBaseline');
        if ($savedBaseline != $baselineId || !$savedSearch) {
            $savedSearch = null;
        }

		if ($filter_id) {
            $this->render('vFilterList',
                array('listUrl'=>$listUrl, 'baseline'=>$baseline, 'controlFilter'=>$controlFilter,
                    'root_id'=>$root_id, 'status_id'=>$status_id));
        } else {
            $this->render('vList', array('listUrl'=>$listUrl, 'baseline'=>$baseline, 'savedSearch'=>$savedSearch));
        }
	}

    public function actionGet()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline', null, 'json');
 		$search = $this->getSearchParams();
		$mergedSearch = $search;
		$status_id = getGet('status');
		$filter_id = $this->paramObscureId(false, 'fid');
		$root_id = $this->paramObscureId(false, 'rid');
		$securityRoleId = getGet('srid');
		if ($filter_id > 1) { // 1 = id of default filter
			$filterSearch = mReportFilter::buildSearchFilter($filter_id);
			if ($search['search']) {
				$mergedSearch['search'] = CMap::mergeArray($search['search'], $filterSearch['search']);
			} else {
				$mergedSearch = $filterSearch;
			}
		}
		if ($root_id) {
			$mergedSearch['search'][] = array(
				'field'=>'root_id',
				'type'=>'int',
				'operator'=>'is',
				'value'=>$root_id,
			);
		}
		if ($securityRoleId !== null && Obscure::isObscure($securityRoleId) !== false) {
			$securityRoleId = Obscure::decode($securityRoleId);
		} else {
			$securityRoleId = null;
		}
		$export = getGet('export');
        $baseline_id = Baseline_Ex::getCurrentBaselineId();

        Usersetting_Ex::SetValue(userId(), 'controlBaselineId', $baseline_id);
        UserSetting_Ex::SaveJsonValue(userId(), 'controlBaseline', $mergedSearch);

		if ($export == 'csv') {
			$json = ControlBaseline_Ex::exportCSV($mergedSearch,$status_id,$securityRoleId);
		} else if ($export == 'word') {
			$json = ControlBaseline_Ex::exportWORD($mergedSearch,$status_id,$securityRoleId);
		} else {
			$json = ControlBaseline_Ex::getBaselineControlsJson(Utils::contextCompanyId(), $baseline_id, null,
				$mergedSearch['pageSize'], $mergedSearch['offsetBy'], $mergedSearch['search'], $mergedSearch['searchLogic'], $mergedSearch['sort'],
				$status_id, $securityRoleId);
		}
		echo $json;
	}

	public function actionDownload()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline');
		// will send headers and exit or show a message
		$this->downloadFile(mDownloadFile::ControlBaselineExport,Cons::LIC_TRIAL,Cons::ROLE_AUDITOR);
	}
        
        public function actionDownloadword()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline');
		// will send headers and exit or show a message
		$this->downloadFile(mDownloadFile::ControlBaselineExportToWORD,Cons::LIC_TRIAL,Cons::ROLE_AUDITOR);
	}

	// overriden from base and called by $this->renderView() to get values and skipEncode for View feature
	protected function getViewValues($id,&$skipEncode)
	{
		$values = ControlBaseline_Ex::getViewValues(Utils::contextCompanyId(), $id);
		$skipEncode = array('Description', 'Sample Policy', 'Guidance', 'Example Evidence', 'Regulatory Reference', 'Assessment Question');
		return $values;
	}
	public function actionView()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline');
		$this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUDITOR); // base GridViewController method calls override method getViewValues()
	}

	// ControlBaseline Copy
	// override method called by ajaxCopy()
	// function is to copy and returns params for json staus and message
	// can be overridden for other use cases
	protected function doCopy($id)
	{
		$status = 'error';
		$message = 'The control was not found in the Control Baseline.';
		$model1 = ControlBaseline::model()->findByAttributes(array('control_baseline_id' => $id, 'company_id' => Utils::contextCompanyId()));
		if (isset($model1)) {
			$baseline_id = Baseline_Ex::getCurrentBaselineId();
			$model2 = new ControlBaseline;
			$attr = $model1->attributes;
			unset($attr['control_baseline_id']);
			$model2->attributes = $attr;
			$model2->baseline_id = $baseline_id;
			$model2->user_id = userId();
                        
                        // Mark who copied record
                        $model2->updated_on = date("Y-m-d H:i:s");
                        $model2->updated_user_id = userId();
			if ($model2->save()) {
				$status = 'success';
				$message = 'The control was copied to your baseline.';
			} else {
				$message = 'The copied control failed to saved to your baseline.';
				Yii::log(print_r($model2->getErrors(), true), 'error', 'ControlbaselineController.doCopy');
			}
		}
		return array('status'=>$status,'message'=>$message);
	}
	public function actionCopy()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline', Cons::ROLE_AUTHOR, 'json');
		echo $this->ajaxCopy(Cons::LIC_TRIAL,Cons::ROLE_AUTHOR);
	}

	// ControlBaseline Delete
	// override method called by ajaxDelete()
	protected function doDelete($id,$markStatusOnly=false)
	{
		return $this->doModelDelete($id,ControlBaseline::model(),array('control_baseline_id' => $id, 'company_id' => Utils::contextCompanyId()),$markStatusOnly);
	}
	public function actionDelete()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline', Cons::ROLE_AUTHOR, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
	}

	public function actionEdit()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline', Cons::ROLE_AUTHOR);

		$id = $this->paramObscureId(false); // will exit if bad param for security reasons
        $filter_id = getGet('fid');
        $root_id = getGet('rid');
        $status_id = getGet('status');

		if (isset($id) && $id > 0) {
			$model = ControlBaseline::model()->with('evidences')->findByAttributes(array('control_baseline_id' => $id, 'company_id' => Utils::contextCompanyId()));
			if (!isset($model)) {
				badurl('User attempting wrongful access.');
			}
		} else {
			$baseline_id = Baseline_Ex::getCurrentBaselineId();
			$model = new ControlBaseline;
			$model->baseline_id = $baseline_id;
			$model->user_id = userId();
			$model->company_id = Utils::contextCompanyId();
		}

                $nextControlBaselineId = ControlBaseline_Ex::getNextControlBaselineId($id, $model->company_id, $model->baseline_id);
                
		// uncomment the following code to enable ajax-based validation
		/*
        if(isset($_POST['ajax']) && $_POST['ajax']==='control-baseline-controlbaseline-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        */

		if(isset($_POST['ControlBaseline'])) {
			$model->attributes = $_POST['ControlBaseline'];
            $filter_id = getPost('filter_id');
            $root_id = getPost('root_id');
            $status_id = getPost('status_id');

			if (empty($model->security_role_id)) {
				$model->security_role_id = null;
			}
			if (!empty($model->target_date)) {
				$model->target_date = DateHelper::FormatDateTimeString($model->target_date, 'Y-m-d');
			}
			$model->user_id = userId();
			$model->company_id = Utils::contextCompanyId();

                        // Mark who made change
                        $model->updated_on = date("Y-m-d H:i:s");
                        $model->updated_user_id = userId();
                        
			/*
			 *  This code attempts to correct records that may be missing a cat_code or cat_sub_code
			 *  due to migration of old data
			 */
			if ((empty($model->cat_code) || $model->cat_code == '?') ||
				(empty($model->cat_sub_code) || $model->cat_sub_code == '?')) {
				$cat_code = '?';
				$cat_sub_code = '?';
				if (!empty($model->control_id)) {
					$controlLibrary = ControlLibrary::model()->findByAttributes(array('control_id' => $model->control_id));
					if (isset($controlLibrary)) {
						if (!empty($controlLibrary->cat_code)) $cat_code = $controlLibrary->cat_code;
						if (!empty($controlLibrary->cat_sub_code)) $cat_sub_code = $controlLibrary->cat_sub_code;
					}
				}
				if (empty($model->cat_code) || $model->cat_code == '?') {
					$model->cat_code = $cat_code;
				}
				if (empty($model->cat_sub_code) || $model->cat_sub_code == '?') {
					$model->cat_sub_code = $cat_sub_code;
				}
			}

			if($model->validate()) {
				if($model->save()) {
                    if (isset($_POST['SaveAndNext']) && $nextControlBaselineId) {
                        $this->redirect(url('controlbaseline/edit/id/' . Obscure::encode($nextControlBaselineId)));
                    } else if ($filter_id) {
                        if ($root_id && $status_id) {
                            $this->redirect(array('controlbaseline/list/fid/' . $filter_id
                                . '/rid/' . $root_id . '/status/' . $status_id));
                        } else {  // opening default baseline controls
                            $this->redirect(array('controlbaseline/list/fid/' . $filter_id));
                        }
                    } else {
                        $this->redirect(url('controlbaseline/list'));
                    }
					
				} else {
					Yii::log(print_r($model->getErrors(), true), 'error', 'ControlbaselineController.actionEdit');
				}
			}
		}
		if (!empty($model->target_date)) {
			$model->target_date = DateHelper::FormatDateTimeString($model->target_date,'m/d/Y');
		}
		$this->render('vEdit',
            array('model'=>$model, 'nextControlBaselineId'=>$nextControlBaselineId,
                'filter_id'=>$filter_id, 'root_id'=>$root_id, 'status_id'=>$status_id));
	}
	
	public function actionChangebaseline()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline');
		$id = $this->paramObscureId(false); // will exit if bad param for security reasons
		if (isset($id) && $id > 0) {
			Baseline_Ex::setCurrentBaseline(Utils::contextCompanyId(),$id);
			$this->redirect(url('controlbaseline/list'));
		} else {
			$this->redirect(url('baseline/edit'));
		}
	}

	public function actionSaveState()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline');
		$state = getPost('state');
		$baseline_id = Baseline_Ex::getCurrentBaselineId();

		Usersetting_Ex::SetValue(userId(), 'controlBaselineId', $baseline_id);
		UserSetting_Ex::SaveJsonValue(userId(), 'controlBaselineState', $state);
	}

	public function actionRestoreState()
	{
		mPermission::checkAccess($this, 'Compliance', 'Control Baseline');
		$baseline_id = Baseline_Ex::getCurrentBaselineId();
		if ($baselineId = Usersetting_Ex::GetValue(userId(), 'controlBaselineId')) {
			if ($baseline_id = $baselineId) {
				$state = UserSetting_Ex::GetJsonValue(userId(), 'controlBaselineState');
				echo json_encode($state);
			}
		}
	}

}