<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class EmployeeController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','delete','view','edit','import'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Company', 'Users');
        $this->render('vList');
    }
    public function actionGet()
    {
		mPermission::checkAccess($this, 'Company', 'Users', null, 'json');
        $s = $this->getSearchParams();
        $json = Employee_Ex::getEmployeesJson(Utils::contextCompanyId(),$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    // Employee Delete
    // can override to do preDeleteCheck to potential stop delete action if return is not true
    protected function preDeleteCheck($id,$model,$markStatusOnly=false)
    {
        return Employee_Ex::preDeleteCheck($id);
    }

    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
		$id = $this->paramObscureId(false); // will exit if bad param for security reasons
		if (mEmployee::deleteEmployee($id)) {
			return mGrid::toGridMessage('success', 'Successfully deleted the user.', true); // success message
		} else {
			return mGrid::toGridMessage('error', 'Failed to delete record.');
		}
    }

    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Company', 'Users', Cons::ROLE_ADMIN, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = Employee_Ex::getViewValues(Utils::contextCompanyId(),$id);
        $skipEncode = array('Security Roles assigned','Tasks assigned');
        return $values;
    }

    public function actionView()
    {
		mPermission::checkAccess($this, 'Company', 'Users', Cons::ROLE_ADMIN);
        $this->renderView(Cons::LIC_TRIAL,Cons::ROLE_ADMIN); // base GridViewController method calls override method getViewValues()
    }

    public function actionEdit()
    {
		mPermission::checkAccess($this, 'Company', 'Users', Cons::ROLE_ADMIN);
		$id = $this->paramObscureId(false); // will exit if bad param for security reasons
		$mEmployee = new mEmployee($id);
		if (isset($_POST['mEmployee'])) {
			$results = $mEmployee->editEmployee($_POST);
			if ($results) {
				$this->redirect(url('employee/list'));
			} else if ($mEmployee->builtinError) {
				$this->renderMessage($mEmployee->builtinError);
			}
		}
		$this->render('vEdit', array('mEmployee' => $mEmployee));
    }

    public function actionImport()
    {
		mPermission::checkAccess($this, 'Company', 'Import Users');
        $message = "";
        $errorList = array();

        if (isset($_FILES['excel'])) {
            if (!empty($_FILES['excel']['tmp_name'])) {
                try {
                    $mImportUsers = new mImportUsers();
                    $mImportUsers->importUsersFromExcel($_FILES['excel']['tmp_name'], Utils::contextCompanyId());
                    $this->redirect(url('employee/list'));
                } catch(Exception $e) {
                    $errorList = json_decode($e->getMessage(), TRUE);
                }
            } else {
                $message = "Must choose a file to import.";
            }
        }
        $this->render('vImport',array('message'=>$message, 'errorList' => $errorList));
    }
}