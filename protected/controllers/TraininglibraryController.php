<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class TraininglibraryController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete,copy', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view,copy',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','view','download','copy'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'edit' and 'delete' actions
                'actions'=>array('delete','edit'),
                'expression'=>'!user()->isGuest && Utils::isSuper()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Training', 'Training Library');
		$this->render('vList');
    }

    public function actionGet()
    {
		mPermission::checkAccess($this, 'Training', 'Training Library', null, 'json');
        $s = $this->getSearchParams();
        $json = TrainingLibrary_Ex::getTrainingLibraryJson($s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    public function actionDownload()
    {
		mPermission::checkAccess($this, 'Training', 'Training Library', Cons::ROLE_AUTHOR, 'json');
        // will send headers and exit or show a message
        $this->downloadFile(mDownloadFile::TrainingLibrary,Cons::LIC_TRIAL,Cons::ROLE_AUTHOR);
    }

    // TrainingLibrary Delete
    // can override to do post delete actions
    protected function postDelete($id,$model,$markStatusOnly=false) {
        if ($model->training_type == 1) {
            if ($markStatusOnly == false) {
                TrainingLibrary_Ex::removeFile($model);
            }
        } else if ($model->training_type == 2) {
            VideoTraining_Ex::deleteVideoTraining($id, null, null, $markStatusOnly);
        }
        return true;
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,TrainingLibrary::model(),array('training_library_id'=>$id),$markStatusOnly);
    }
    public function actionDelete()
    {
        if (Utils::isSuper()) {
			$id = $this->paramObscureId(); // will exit if bad param for security reasons
			echo $this->doDelete($id, true);
        } else {
            $status = 'error';
            $message = 'Your role is not authorized to delete the training library document.';
            echo json_encode(array('status' => $status, 'message'=>$message));
        }
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = TrainingLibrary_Ex::getViewValues($id);
        $skipEncode = array('Description');
        return $values;
    }
    public function actionView()
    {
		mPermission::checkAccess($this, 'Training', 'Training Library', Cons::ROLE_AUTHOR);
        $this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUTHOR); // base GridViewController method calls override method getViewValues()
    }

    // TrainingLibrary Copy
    // override method called by ajaxCopy()
    // function is to copy and returns params for json staus and message
    // can be overridden for other use cases
    protected function doCopy($id)
    {
        $status = 'error';
        $message = 'Failed to copy training library document to your training.';
        $message = TrainingLibrary_Ex::copyTrainingLibrary($id, Utils::contextCompanyId(), userId());
        if ($message === false) {
            $status = 'success';
            $message = 'The training library document was copied to your training.';
        }
        return array('status'=>$status,'message'=>$message);
    }
    public function actionCopy()
    {
		mPermission::checkAccess($this, 'Training', 'Training Library', Cons::ROLE_AUTHOR, 'json');
        echo $this->ajaxCopy(Cons::LIC_TRIAL,Cons::ROLE_AUTHOR);
    }

    public function actionEdit()
    {
        if (Utils::isSuper()) {
            $id = $this->paramObscureId(false); // will exit if bad param for security reasons

            if (isset($id) && $id > 0) {
                $model = TrainingLibrary::model()->findByAttributes(array('training_library_id' => $id));
                if (!isset($model)) {
                    badurl('User attempting wrongful access.');
                }
                $model->isNewRecord = false;
            } else {
                $training_type = getGet('type',1);
                $training_type = ($training_type == 2) ? 2 : 1;
                $model=new TrainingLibrary;
                $model->isNewRecord = true;
                $model->training_type = $training_type;
            }

            if(isset($_POST['TrainingLibrary']))
            {
                $model->attributes=$_POST['TrainingLibrary'];
                if (!empty($model->publish_date)) {
                    $model->publish_date = DateHelper::FormatDateTimeString($model->publish_date,'Y-m-d');
                }
                if ($model->isNewRecord) {
                    $model->doc_guid = Utils::create_guid();
                    $model->publish_date = date('Y-m-d');
                }

                $fileUrl = false;
                $tmpFileFullPath = false;
                // handle TrainingLibrary file upload
                if (isset($_FILES['TrainingLibrary'])) {
                    if (!empty($_FILES['TrainingLibrary']['name'])) {
                        $tmpFileFullPath = $_FILES['TrainingLibrary']['tmp_name'];
                        $model->file_size = $_FILES['TrainingLibrary']['size'];
                        $model->file_type = $_FILES['TrainingLibrary']['type'];
                        $model->file_name = $_FILES['TrainingLibrary']['name'];
                        $model->file_ext = pathinfo($_FILES['TrainingLibrary']['name'], PATHINFO_EXTENSION);
                    }
                }

                if($model->validate())
                {
                    if ($model->isNewRecord) {
                        if ($model->training_type == 1) {
                            if ($tmpFileFullPath !== false) {
                                $fileUrl = TrainingLibrary_Ex::saveFile($tmpFileFullPath, $model);
                                if ($fileUrl !== false) {
                                    if ($model->save()) {
                                        $this->redirect(url('traininglibrary/list'));
                                        return;
                                    } else {
                                        $model->addError('doc_title', "Failed to save training library document. Try again.");
                                    }
                                } else {
                                    $model->addError('file_name', "Failed to upload Training Library File.");
                                }
                            } else {
                                $model->addError('file_name', "Training Library File is required. Choose a file to upload.");
                            }
                        } else {
                            if ($model->save()) {
                                $this->redirect(url('videotraining/editseries',array('ret'=>2,'library'=>1,'id'=>Obscure::encode($model->training_library_id))));
                                return;
                            } else {
                                $model->addError('doc_title', "Failed to save training library document. Try again.");
                            }
                        }
                    } else if ($model->save()) {
                        if ($model->training_type == 1 && $tmpFileFullPath !== false) {
                            $fileUrl = TrainingLibrary_Ex::saveFile($tmpFileFullPath, $model);
                            if ($fileUrl !== false) {
                                $this->redirect(url('traininglibrary/list'));
                                return;
                            } else {
                                $model->addError('file_name', "Failed to upload Training Library File.");
                            }
                        } else {
                            if ($model->training_type == 2) {
                                $this->redirect(url('videotraining/editseries',array('ret'=>2,'library'=>1,'id'=>Obscure::encode($model->training_library_id))));
                            } else {
                                $this->redirect(url('traininglibrary/list'));
                            }
                            return;
                        }
                    } else {
                        $model->addError('doc_title', "Failed to save changes. Try again.");
                        Yii::log(print_r($model->getErrors(), true), 'error', 'TraininglibraryController.actionEdit');
                    }
                }
            }
            if (!empty($model->publish_date)) {
                $model->publish_date = DateHelper::FormatDateTimeString($model->publish_date,'m/d/Y');
            }
            $this->render('vEdit',array('model'=>$model));
        } else {
            badurl('Your role is not authorized to edit the training library document.');
        }
    }
}