<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class MyController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + getinbox,gettraining,getpolicy,getquiz,ack,gettraining,updatetaskstatus', // we only allow deletion via POST request
            'ajaxOnly + getinbox,gettraining,getpolicy,getquiz,ack,gettraining,updatetaskstatus',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('inbox','policies','training','quiz','info','tasks' ,'getinbox','gettraining','getpolicy','getquiz', 'gettasks','viewtraining','viewpolicy','viewquiz','ack','updatetaskstatus'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionInbox()
    {
		mPermission::checkAccess($this, null, 'My Inbox');

        $policyList = array();
        $trainingList =  array();
        $company_id = Utils::contextCompanyId();
        $userProfileIds = UserProfile_Ex::getCurrentUserProfileIds();
        if (!empty($userProfileIds)) {
            $policyList = UserProfile_Ex::getUserProfilePolicy($userProfileIds, $company_id);
            $trainingList = UserProfile_Ex::getUserProfileTraining($userProfileIds, $company_id);
        }
        
        $this->pageTitle = Yii::app()->name . " - Inbox";
        $this->render('vInbox', array('policyList' => $policyList, 'trainingList' => $trainingList));
    }

    public function actionPolicies()
    {
		mPermission::checkAccess($this, null, 'Policies');

        $this->pageTitle=Yii::app()->name." - Policies";
        $this->render('vPolicies');
    }

    public function actionTraining()
    {
		mPermission::checkAccess($this, null, 'Training');

        $this->pageTitle=Yii::app()->name." - Training";
        $this->render('vTraining');
    }

    public function actionQuiz()
    {
		mPermission::checkAccess($this, null, 'Quizzes');

		$this->pageTitle=Yii::app()->name." - Quiz";
        $this->render('vQuiz');
    }

    public function actionTasks()
    {
		mPermission::checkAccess($this, null, 'My Tasks');
        
        $this->pageTitle = Yii::app()->name . " - Tasks";
        $this->render('vTasks');
    }
    
    public function actionGetpolicy()
    {
		mPermission::checkAccess($this, null, 'Policies', null, 'json');

        $cmd = getPost('cmd');
        $pageSize = getPost('limit');
        $offsetBy = getPost('offset');
        $json = UserProfile_Ex::getUserProfilePolicyJson();
        echo $json;
    }
    
    public function actionGettraining()
    {
		mPermission::checkAccess($this, null, 'Training', null, 'json');

        $cmd = getPost('cmd');
        $pageSize = getPost('limit');
        $offsetBy = getPost('offset');
        $json = UserProfile_Ex::getUserProfileTrainingJson();
        echo $json;
    }

    public function actionGetquiz()
    {
		mPermission::checkAccess($this, null, 'Quizzes', null, 'json');

        $cmd = getPost('cmd');
        $pageSize = getPost('limit');
        $offsetBy = getPost('offset');
        $json = UserProfile_Ex::getUserProfileQuizJson();
        echo $json;
    }

    public function actionGettasks()
    {
		mPermission::checkAccess($this, null, 'My Tasks', null, 'json');

        $cmd = getPost('cmd');
        $pageSize = getPost('limit');
        $offsetBy = getPost('offset');
        $companyId = Utils::contextCompanyId();
        $employeeModel = Employee::model()->findByAttributes(array('email' => userEmail(), 'company_id' => $companyId));
        $employeeId = ($employeeModel) ? $employeeModel->employee_id : null;
        $json = Task_Ex::getTasksJson($companyId, $pageSize, $offsetBy, null, null, null, $employeeId, true);
        echo $json;
    }
    
    public function actionViewpolicy()
    {
		mPermission::checkAccess($this, null, 'Policies');
        $id = $this->paramObscureId(); // will exit if bad param for security reasons
        $policy = Policy::model()->findByAttributes(array('policy_id'=>$id,'company_id' => Utils::contextCompanyId()));
        $userPolicyTracking = UserPolicyTracking::model()->findByAttributes(array('user_id'=>userId(), 'policy_id'=>$id));
		$ackDate = isset($userPolicyTracking->ack) && $userPolicyTracking->ack ? $userPolicyTracking->ack_date : null;
        if (empty($policy)) {
            $this->renderMessage(array('messageId'=>0,'title' => '<i class="fa fa-warning"></i> Policy Not Found',
                'message'=>'Sorry, Policy document was not found. If you think this is an error then contact technical support.'));
        } else if ($policy->file_type == 'application/pdf' || stripos($policy->file_type,'text/') === 0) {
            $this->pageTitle=Yii::app()->name." - View Policy";
            $fileUrl =  Policy_Ex::getTmpFileUrl($policy);
            if (empty($fileUrl)) {
                $this->renderMessage(array('messageId'=>0,'title' => '<i class="fa fa-warning"></i> Policy Document Failure',
                    'message' => 'Sorry, was unable to retrieve the Policy Document. If you think this is an error then contact technical support.'));
            }
            $this->render('vViewPolicy',array('model'=>$policy, 'fileUrl'=>$fileUrl, 'ackDate'=>$ackDate));
            return;
        } else {
            Policy_Ex::downloadFile($policy);
        }
    }

    public function actionViewtraining()
    {
		mPermission::checkAccess($this, null, 'Training');
        $id = $this->paramObscureId(); // will exit if bad param for security reasons
        $training = Training::model()->findByAttributes(array('training_id'=>$id,'company_id' => Utils::contextCompanyId()));
        if (empty($training)) {
            $this->renderMessage(array('messageId'=>0,'title' => '<i class="fa fa-warning"></i> Training Not Found',
                'message'=>'Sorry, Training document was not found. If you think this is an error then contact technical support.'));
        } else if ($training->training_type == '2') {
            $criteria = new CDbCriteria(array('order' => 'sort_order asc, video_training_id asc'));
            $models = VideoTraining_Ex::getVideosWithTracking($id, Utils::contextCompanyId(), userId());
            $trainingInfo = Training_Ex::getTraining($count, Utils::contextCompanyId(), $id);
            $this->render('vWatchSeries', array('models' => $models, 'trainingInfo'=>$trainingInfo));
            return;
        } else {
            if ($training->file_type == 'application/pdf' || stripos($training->file_type, 'text/') === 0) {
                $this->pageTitle = Yii::app()->name . " - View Training";
                $fileUrl = Training_Ex::getTmpFileUrl($training);
                if (empty($fileUrl)) {
                    $this->renderMessage(array('messageId'=>0,'title' => '<i class="fa fa-warning"></i> Training Document Failure',
                        'message' => 'Sorry, was unable to retrieve the Training Document. If you think this is an error then contact technical support.'));
                }
                $this->render('vViewTraining', array('model' => $training, 'fileUrl' => $fileUrl));
                return;
            } else {
                Training_Ex::downloadFile($training);
            }
        }
    }

    public function actionViewquiz()
    {
		// comment out for now as it breaks when "testing" quiz.
		// mPermission::checkAccess($this, null, 'Quizzes');
        $isTest = (getGet('test',0) == 1) ? true : false;
        $id = $this->paramObscureId(); // will exit if bad param for security reasons
        $company_id = Utils::contextCompanyId();
        $user_id = userId();
        $quiz = Quiz::model()->findByAttributes(array('quiz_id'=>$id,'company_id' => $company_id));
        if (empty($quiz)) {
            $this->renderMessage(array('messageId'=>0,'title' => '<i class="fa fa-warning"></i> Quiz Not Found',
                'message'=>'Sorry, Quiz was not found. If you think this is an error then contact technical support.'));
        } else {
            if(isset($_POST['Answer']))
            {
                if ($isTest) {
                    $this->redirect(url('quiz/list'));
                } else {
                    $answers = getPost('Answer');
                    $errorMessages = QuizTracking_Ex::track($id, $company_id, $user_id, $answers, $quiz);
                    if (empty($errorMessages)) {
                        $this->redirect(url('my/quiz'));
                    } else {
                        $this->renderMessage(array('messageId' => 0, 'title' => '<i class="fa fa-warning"></i> Errors occurred while saving quiz answers.', 'message' => implode('<br/>', $errorMessages)));
                        return;
                    }
                }
            }
            if ($isTest) {
                $canTakeQuiz = true;
            } else {
                // tracked that the quiz is being viewed possibly for first time
                $quizTracking = QuizTracking_Ex::viewed($id, $company_id, $user_id);
                $canTakeQuiz = ($quizTracking->taken_count <= $quiz->max_retakes) ? true : false;
            }
            if ($canTakeQuiz) {
                $criteria = new CDbCriteria(array('order' => 'sort_order ASC'));
                $questions = QuizQuestion::model()->findAllByAttributes(array('quiz_id' => $id, 'company_id' => $company_id, 'status' => 1), $criteria);
                $correctAnswers = QuizTracking_Ex::correctAnswersByUser($id,$company_id,$user_id);
                $this->pageTitle = Yii::app()->name . " - View Quiz";
                if ($isTest) {
                    $editUrl = url('my/viewquiz', array('test'=>1,'id' => Obscure::encode($id)));
                } else {
                    $editUrl = url('my/viewquiz', array('id' => Obscure::encode($id)));
                }
                $this->render('vViewQuiz', array('model' => $quiz, 'questions' => $questions, 'correctAnswers'=>$correctAnswers, 'editUrl'=>$editUrl, 'isTest'=>$isTest));
            } else {
                $this->renderMessage(array('messageId'=>0,'title'=>'<i class="fa fa-warning"></i> Maximum Re-takes Reached','message'=>'Oops, you have exceeded the maximum retakes for this Quiz, thus could not re-take.'));
            }
        }
    }

    public function actionAck()
    {
		mPermission::checkAccess($this, null, 'Policies', null, 'silent');
        $id = $this->paramObscureId();
        $tid = getPost('tid',false);
        $type = getPost('type',false);
        $ack = getPost('ack',false);
        $file = getPost('file',false);
        $user_id = userId();
        $company_id = Utils::contextCompanyId();

        $status = "success";
        $message = "Success!";

        if ($type == 'policy') {
            if (!UserPolicyTracking_Ex::track($id,$company_id,$user_id,$ack,$file)) {
                $status = "failed";
                $message = "Failed to save policy ".(($ack == 'yes') ? 'acknowledgement' : 'view')." tracking record.";
                Yii::log(print_r($_POST,true), 'error', 'MyController.actionAck');
			} else {
				setFlashMessage('Successfully acknowledged the policy document.');
			}
        } else if ($type == 'training') {
            if (!UserTrainingTracking_Ex::track($id,$company_id,$user_id,$ack,$file)) {
                $status = "failed";
                $message = "Failed to save training ".(($ack == 'yes') ? 'acknowledgement' : 'view')." tracking record.";
                Yii::log(print_r($_POST,true), 'error', 'MyController.actionAck');
            }
        } else if ($type == 'video') {
            if ($tid) {
                if (Obscure::isObscure($tid) !== false) {
                    $tid = Obscure::decode($tid);
                } else {
                    $tid=null;
                }
            }
            if (!VideoTracking_Ex::track($id,$company_id,$user_id,$ack,$tid)) {
                $status = "failed";
                $message = "Failed to save video ".(($ack == 'yes') ? 'acknowledgement' : 'view')." tracking record.";
                Yii::log(print_r($_POST,true), 'error', 'MyController.actionAck');
            }
        }

        // only do this on test servers not production
        // production should use a cron job that cleans up every 10 minutes any file older than 10 minutes.
        if (APP_DEVELOPMENT_SITE && !empty($file) && $file != '.') {
            // will delete temp file immediately
            $mUploadedFiles = new mUploadedFiles();
            sleep(3); // a little time to be sure file read.
            $mUploadedFiles->deleteTmpFileUrl($file);
        }

        $array = array("status"=>$status, "message"=>$message);
        $json = json_encode($array);
        echo $json;
    }

    public function actionInfo()
    {
		mPermission::checkAccess($this, null, 'My Info');

        $this->pageTitle = Yii::app()->name . " - My Info";
        $gmAjaxListView = false;
        $companyId = Utils::contextCompanyId();
        $model = Employee::model()->findByAttributes(array('email' => userEmail(), 'company_id' => $companyId));
        if (isset($model)) {
            $values = Employee_Ex::getViewValues($companyId, $model->employee_id, false);
            $skipEncode = array('Security Roles assigned','Tasks assigned');
            $gmAjaxListView = new gmAjaxListView($values, $skipEncode);
        }
        $this->render('vInfo', array('gmAjaxListView' => $gmAjaxListView));
    }
    
    public function actionUpdatetaskstatus(){
		mPermission::checkAccess($this, null, 'My Tasks', null, 'json');
        $status = '';
        $taskCompleted = false;
        $message = '';
        $taskId = getPost('task_id');
        $taskStatusId = getPost('status_id');
        $companyId = Utils::contextCompanyId();
        
        if($taskId != null && $taskStatusId != null){
            if(Task_Ex::updateTaskStatus($companyId, Obscure::decode($taskId), $taskStatusId)){
                $status = 'success';
                $taskCompleted = ($taskStatusId == Cons::TASK_COMPLETE) ? true : false;
            }else{
                $status = "failed";
                $message = 'Unable to update task status. Database connection error.';
                Yii::log(print_r($_POST,true), 'error', 'MyController.actionUpdatetaskstatus');
            }
        }else{
            $status = "failed";
            $message = 'Unable to update task status. Undefined paramiters.';
            Yii::log(print_r($_POST,true), 'error', 'MyController.actionUpdatetaskstatus');
        }
        
        $array = array("status" => $status, "message" => $message, 'completed' => $taskCompleted);
        $json = json_encode($array);
        echo $json;
    }
}