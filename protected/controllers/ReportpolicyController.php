<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class ReportpolicyController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + getcompliance,getcompliancedetail',
            'ajaxOnly + getcompliance,getcompliancedetail',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('compliance','getcompliance','exportcompliance','printcompliance',
                    'compliancedetail','getcompliancedetail','exportcompliancedetail','printcompliancedetail'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionCompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Policy Compliance');
        $this->render('vCompliance');
    }

    public function actionGetcompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Policy Compliance', null, 'json');
        $mReportsPolicy = new mReportsPolicy();
        $json = $mReportsPolicy->ComplianceReport(Utils::contextCompanyId());
        echo $json;
    }

    public function actionExportcompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Policy Compliance');
        $export = getGet('export',false);
        if ($export=='csv') {
            $mReportsPolicy = new mReportsPolicy();
            $data = $mReportsPolicy->ComplianceReport(Utils::contextCompanyId(),false,true);
            if (count($data)) {
                mDownloadFile::outputCSV($data);
                Yii::app()->end();
            }
        }
    }

    public function actionPrintcompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Policy Compliance');
        $mReportsPolicy = new mReportsPolicy();
        $data = $mReportsPolicy->ComplianceReport(Utils::contextCompanyId(),false,true);
        $this->layout = false;
        $this->render('vCompliancePrint',array('data'=>$data));
    }

    public function actionCompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Policy Compliance');
        $id = $this->paramObscureId();
        $policy = Policy::model()->findByPk($id);
        $dataUrl = url('reportpolicy/getcompliancedetail',array('id'=>Obscure::encode($id)));
        $exportUrl = url('reportpolicy/exportcompliancedetail',array('id'=>Obscure::encode($id),'export'=>'csv'));
        $printUrl = url('reportpolicy/printcompliancedetail',array('id'=>Obscure::encode($id)));
        $this->render('vComplianceDetail',array('policy'=>$policy,'dataUrl'=>$dataUrl,'exportUrl'=>$exportUrl,'printUrl'=>$printUrl));
    }

    public function actionGetcompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Policy Compliance', null, 'json');
        $id = $this->paramObscureId();
        $mReportsPolicy = new mReportsPolicy();
        $json = $mReportsPolicy->ComplianceReportDetail(Utils::contextCompanyId(),$id,true,false);
        echo $json;
    }

    public function actionExportcompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Policy Compliance');
        $id = $this->paramObscureId();
        $policy = Policy::model()->findByPk($id);
        $export = getGet('export',false);
        if ($export=='csv') {
            $mReportsPolicy = new mReportsPolicy();
            $data = $mReportsPolicy->ComplianceReportDetail(Utils::contextCompanyId(),$id,false,true,$policy);
            if (count($data)) {
                mDownloadFile::outputCSV($data);
                Yii::app()->end();
            }
        }
    }

    public function actionPrintcompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Policy Compliance');
        $id = $this->paramObscureId();
        $policy = Policy::model()->findByPk($id);
        $mReportsPolicy = new mReportsPolicy();
        $data = $mReportsPolicy->ComplianceReportDetail(Utils::contextCompanyId(),$id,false,true);
        $this->layout = false;
        $this->render('vComplianceDetailPrint',array('policy'=>$policy,'data'=>$data));
    }
}