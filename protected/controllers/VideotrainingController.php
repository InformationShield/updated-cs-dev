<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class VideotrainingController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete,', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','view','delete','edit','editseries','watchseries'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Videos');
		$this->render('vList');
    }

    public function actionGet()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Videos', Cons::ROLE_AUTHOR, 'json');
        $s = $this->getSearchParams();
        $companyId = (Utils::isSuper()) ? null : Utils::contextCompanyId();
        $json = VideoTraining_Ex::getVideoTrainingJson($companyId,$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    // VideoTraining Delete
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        $companyId = (Utils::isSuper()) ? null : Utils::contextCompanyId();
        if (Utils::isSuper()) {
            $model = VideoTraining::model()->findByAttributes(array('video_training_id' => $id));
            $markStatusOnly = (empty($model->training_library_id)) ? false : true;
            return $this->doModelDelete($id,VideoTraining::model(),array('video_training_id' => $id),$markStatusOnly);
        } else {
            $model = VideoTraining::model()->findByAttributes(array('video_training_id' => $id, 'company_id' => $companyId));
            $markStatusOnly = (empty($model->training_library_id)) ? false : true;
            return $this->doModelDelete($id,VideoTraining::model(),array('video_training_id' => $id, 'company_id' => $companyId),$markStatusOnly);
        }
    }
    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Videos', Cons::ROLE_AUTHOR, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode){
        $companyId = (Utils::isSuper()) ? null : Utils::contextCompanyId();
        $values = VideoTraining_Ex::getViewValues($companyId,$id);
        $skipEncode = array('Video');
        return $values;
    }
    public function actionView()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Videos', Cons::ROLE_AUTHOR);
        $this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUTHOR); // base GridViewController method calls override method getViewValues()
    }

    public function actionEdit()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Videos', Cons::ROLE_AUTHOR);
        $id = $this->paramObscureId(false); // will exit if bad param for security reasons
        if (isset($id) && $id > 0) {
            $companyId = (Utils::isSuper()) ? null : Utils::contextCompanyId();
            if (Utils::isSuper()) {
                $model = VideoTraining::model()->findByAttributes(array('video_training_id' => $id));
            } else {
                $model = VideoTraining::model()->findByAttributes(array('video_training_id' => $id, 'company_id' => $companyId));
            }
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
            $model->isNewRecord = false;
        } else {
            $model = new VideoTraining;
            $model->isNewRecord = true;
        }

        if (isset($_POST['TrainingTitle'])) {
            $trainingId = getPost('TrainingTitle');
            if (strlen($trainingId) > 12) {
                $isLibrary = ($trainingId[0] === '1');
                $trainingId = substr($trainingId, 1);
                if (Obscure::isObscure($trainingId) !== false) {
                    $trainingId = Obscure::decode($trainingId);
                    if ($isLibrary) {
                        $model->training_id = null;
                        $model->training_library_id = $trainingId;
                        $model->company_id = null;
                    } else {
                        $model->training_id = $trainingId;
                        $model->training_library_id = null;
                        $model->company_id = Utils::contextCompanyId();
                    }

                    if (isset($_POST['VideoTraining'])) {
                        $model->attributes = $_POST['VideoTraining'];
                        $model->user_id = userId();
                        if ($model->validate()) {
                            if ($model->save()) {
                                $this->redirect(url('videotraining/list'));
                                return;
                            } else {
                                $model->addError('video_title', "Failed to save changes. Try again.");
                                Yii::log(print_r($model->getErrors(), true), 'error', 'VideotrainingController.actionEdit');
                            }
                        }
                    } else {
                        $model->addError('video_title', "Can not save a video because of missing data.");
                    }
                } else {
                    $model->addError('video_title', "Can not save a video unless a Training Title is selected.");
                }
            } else {
                $model->addError('video_title', "Can not save a video unless a Training Title is selected.");
            }
        } else if (isset($_POST['VideoTraining'])) {
            $model->addError('video_title', "Can not save a video unless a Training Title is selected.");
        }
        $this->pageTitle=Yii::app()->name.' - Edit Training Video';
        $this->render('vEdit', array('model' => $model));
    }

    public function getParams(&$isLibrary,&$id,&$companyId,&$returnOption,&$returnUrl)
    {
        $returnOption = getGet('ret', 3);
        if ($returnOption == 1) {
            $returnUrl = url('training/list');
        } else if ($returnOption == 2) {
            $returnUrl = url('traininglibrary/list');
        } else {
            $returnUrl = url('videotraining/list');
        }

        $vtid = getGet('vtid');
        if (!empty($vtid)) {
            if (Obscure::isObscure($vtid) !== false) {
                $vtid = Obscure::decode($vtid);
            } else {
                badurl('Video Training Id is not obscure and invalid.');
            }
            $model = VideoTraining::model()->findByPk($vtid);
            if (!empty($model)) {
                if ($model->training_library_id > 0) {
                    $isLibrary = true;
                    $id = $model->training_library_id;
                } else if ($model->training_id > 0 && $model->company_id == Utils::contextCompanyId()) {
                    $isLibrary = false;
                    $id = $model->training_id;
                    $companyId = $model->company_id;
                } else {
                    badurl('Video Training Id found but invalid.');
                }
            } else {
                badurl('Id is not obscure and invalid.');
            }
        } else {
            $isLibrary = getGet('library', 0);
            $isLibrary = ($isLibrary == 1) ? true : false;
            $companyId = ($isLibrary) ? null : Utils::contextCompanyId();
            $id = $this->paramObscureId(false); // will exit if bad param for security reasons
        }
    }

    public function getVideoSeriesInfo($isLibrary,$id,$companyId,$returnOption,&$models,&$trainingInfo,&$postUrl)
    {
        if (isset($id) && $id > 0) {
            $criteria = new CDbCriteria(array('order' => 'sort_order asc, video_training_id asc'));
            if ($isLibrary) {
                $models = VideoTraining::model()->findAllByAttributes(array('training_library_id' => $id, 'status' => 1), $criteria);
                $postUrl = url('videotraining/editseries', array('ret' => $returnOption, 'library' => 1, 'id' => Obscure::encode($id)));
                $trainingInfo = TrainingLibrary_Ex::getTrainingLibrary($count,$id);
            } else {
                $models = VideoTraining::model()->findAllByAttributes(array('training_id' => $id, 'company_id' => $companyId, 'status' => 1), $criteria);
                $postUrl = url('videotraining/editseries', array('ret' => $returnOption, 'library' => 0, 'id' => Obscure::encode($id)));
                $trainingInfo = Training_Ex::getTraining($count,$companyId, $id);
            }
            if (!isset($models)) {
                badurl('User attempting wrongful access.');
            }
        } else {
            $models = null;
            $postUrl = null;
            $trainingInfo = null;
        }
    }

    public function actionEditseries()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Videos', Cons::ROLE_AUTHOR);

        $this->getParams($isLibrary,$id,$companyId,$returnOption,$returnUrl);

        $errorMessages = array();
        if (isset($_POST['VideoTraining'])) {
            if (count($_POST['VideoTraining']) > 0) {
                $order = 0;
                $videos = $_POST['VideoTraining'];
                foreach($videos as $video) {
                    $video['sort_order'] = ++$order;
                    $video['user_id'] = userId();
                    if (empty($video['video_training_id'])) {
                        $video['training_library_id'] = ($isLibrary) ? $id : null;
                        $video['training_id'] = ($isLibrary) ? null : $id;
                        $video['company_id'] = ($isLibrary) ? null : $companyId;
                        $VideoTraining = new VideoTraining();
                        unset($video['video_training_id']);
                        $VideoTraining->attributes = $video;
                        if (!$VideoTraining->save()) {
                            $errorMessages[] = "Failed to save changes for new video training record with video title '".$video['video_title']."'";
                        }
                    } else {
                        $VideoTraining = VideoTraining::model()->findByPk($video['video_training_id']);
                        if (!empty($VideoTraining)) {
                            unset($video['video_training_id']);
                            $VideoTraining->attributes = $video;
                            if (!$VideoTraining->save()) {
                                $errorMessages[] = "Failed to save changes for existing video training record ID# " . $video['video_training_id'];
                            }
                        } else {
                            $errorMessages[] = "Failed to update changes for existing video training record ID# ".$video['video_training_id'].", because it was not found.";
                        }
                    }
                }
                if (empty($errorMessages)) {
                    $this->redirect($returnUrl);
                    return;
                }
            }
        }

        $this->getVideoSeriesInfo($isLibrary,$id,$companyId,$returnOption,$models,$trainingInfo,$postUrl);

        $this->pageTitle=Yii::app()->name.' - Edit Training Video Series';
        $this->render('vEditSeries', array('models' => $models, 'trainingInfo'=>$trainingInfo, 'postUrl'=>$postUrl, 'returnUrl'=>$returnUrl, 'errorMessages'=>$errorMessages));

    }

    public function actionWatchseries()
    {
		mPermission::checkAccess($this, 'Training', 'Watch Videos');
        $trainings = VideoTraining_Ex::getTrainingTitleList(Utils::contextCompanyId(),2,Utils::isSuper());
        $id = getGet('id');
        $vtid = getGet('vtid');
        if (empty($id) && empty($vtid) && !empty($trainings)) {
            $id = Obscure::encode($trainings[0]['id']);
            $library = $trainings[0]['library'];
            $_GET['id'] = $id;
            $_GET['library'] =  $library;
        }
        $this->getParams($isLibrary,$id,$companyId,$returnOption,$returnUrl);
        $this->getVideoSeriesInfo($isLibrary,$id,$companyId,$returnOption,$models,$trainingInfo,$postUrl);
        $this->pageTitle = Yii::app()->name.' - Watch Training Video Series';
        $this->render('vWatchSeries', array('models' => $models, 'trainingInfo'=>$trainingInfo, 'returnUrl'=>$returnUrl, 'trainings'=>$trainings));
    }
}