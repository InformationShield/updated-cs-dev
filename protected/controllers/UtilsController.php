<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class UtilsController extends GridViewController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + ', // we only allow deletion via POST request
			'ajaxOnly + ',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('deletechi', 'deletecompany', 'deleteuser'),
				//'users'=>array('admin'),
				'expression' => '!user()->isGuest && Utils::isSuper()',
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionDeletecompany()
	{
		$companyId = getGet('id');
		if ($companyId) {
			$this->deleteCompany($companyId);
			return;
		}
		echo "Missing id param.";
	}

	public function actionDeleteuser()
	{
		$userId = getGet('id');
		if ($userId) {
			$this->deleteUser($userId);
			return;
		}
		echo "Missing id param.";
	}

	private function deleteCompany($companyId) {
		if ($companyId) {
//			$sql = "CALL spDeleteUser(:companyId)";
//			$command = Yii::app()->db->createCommand($sql);
//			$command->bindValue(":companyId", $companyId, PDO::PARAM_INT);
//			$command->execute();

			$sql = "CALL spDeleteCompanyAndUsers(:companyId)";
			$command = Yii::app()->db->createCommand($sql);
			$command->bindValue(":companyId", $companyId, PDO::PARAM_INT);
            $result = $command->execute();

			$sql = "
				SELECT *
				FROM company
				WHERE company_id = :companyId
			";
			$command = Yii::app()->db->createCommand($sql);
			$command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
			$results = $command->query()->readAll();
			if (!empty($results)) {
				vardump($results, false);
			} else {
				echo "- Company $companyId and all associated users and data deleted. <br>";
			}
		}
	}

	private function deleteUser($userId) {
		if ($userId) {
			$sql = "CALL spDeleteUser(:userId)";
			$command = Yii::app()->db->createCommand($sql);
			$command->bindValue(":userId", $userId, PDO::PARAM_INT);
			$command->execute();

			$sql = "
				SELECT *
				FROM tbl_users
				WHERE id = :userId
			";
			$command = Yii::app()->db->createCommand($sql);
			$command->bindParam(":userId", $userId, PDO::PARAM_INT);
			$results = $command->query()->readAll();
			if (!empty($results)) {
				vardump($results, false);
			} else {
				echo "********* User $userId deleted. <br>";
			}
		}
	}

	public function actionDeletechi()
	{
		$sql = "
			SELECT *
			FROM company
			WHERE name LIKE 'CHI%2017%'
			-- AND created_on < '2017-08-15 00:00:00'
		";
		$command = Yii::app()->db->createCommand($sql);
		$results = $command->query()->readAll();
		if (!empty($results)) {
            echo "<br>********* Staring delete of CHI companies. <br>";
			foreach ($results as $company) {
                echo "<br>*** Deleting [".$company['company_id']."] ".$company['name'].". <br>";
                $this->deleteCompany($company['company_id']);
			}
            echo "<br>********* Completed deleting CHI companies. <br>";
		} else {
            echo "<br>********* no matching CHI companies found. <br>";
		}
	}

}