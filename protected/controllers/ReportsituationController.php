<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class ReportsituationController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + getsituation',
            'ajaxOnly + getsituation',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index', 'getsituation'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
		mPermission::checkAccess($this, 'Reports', 'Situation Report');
        $listUrl = url('reportsituation/getsituation');
        
        $baselineId = Baseline_Ex::getCurrentBaselineId();
        $baseline = Baseline_Ex::getBaselineAsessment($count, Utils::contextCompanyId(), $baselineId);
		$this->render('vSituation', array('listUrl'=>$listUrl, 'baseline' => $baseline));
    }
    
    public function actionGetsituation()
	{
		mPermission::checkAccess($this, 'Reports', 'Situation Report', null, 'json');
		$baseline_id = Baseline_Ex::getCurrentBaselineId();
		$results = ControlBaseline_Ex::getSituationReport(Utils::contextCompanyId(), $baseline_id);
		echo mGrid::toJsonGrid($results);
	}
}