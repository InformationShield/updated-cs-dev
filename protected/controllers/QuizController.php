<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class QuizController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('list','get','delete','view','edit','test'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quizzes');
		$this->render('vList');
    }

    public function actionGet()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quizzes', Cons::ROLE_AUTHOR, 'json');
        $s = $this->getSearchParams();
        $json = Quiz_Ex::getQuizJson(Utils::contextCompanyId(),$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    // Quiz Delete
    // can override to do post delete actions
    protected function postDelete($id,$model,$markStatusOnly=false) {
        $results = Quiz_Ex::deleteQuestions(null,$id,Utils::contextCompanyId(),false);
        return $results;
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,Quiz::model(),array('quiz_id'=>$id,'company_id' => Utils::contextCompanyId()),$markStatusOnly);
    }
    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quizzes', Cons::ROLE_AUTHOR, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id, true);
    }

    public function actionView()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quizzes', Cons::ROLE_AUTHOR, 'partial');
		$id = $this->paramObscureId(false); // will exit if bad param for security reasons
		if (isset($id) && $id > 0) {
			$company_id = Utils::contextCompanyId();
			$values = Quiz_Ex::getViewValues($company_id,$id);
			$gmAjaxListView = new gmAjaxListView($values);
			$criteria = new CDbCriteria(array('order' => 'sort_order ASC'));
			$questions = QuizQuestion::model()->findAllByAttributes(array('quiz_id' => $id, 'company_id' => $company_id, 'status' => 1), $criteria);
			$answerCounts = QuizTracking_Ex::questionAnswerCounts($id,$company_id);
			$this->renderPartial('//quiz/vView',array('gmAjaxListView'=>$gmAjaxListView,'questions'=>$questions,'answerCounts'=>$answerCounts));
		}
    }

    public function actionEdit()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quizzes', Cons::ROLE_AUTHOR);
        $id = $this->paramObscureId(false); // will exit if bad param for security reasons

		$saveAndExit = getPost('exit');
        $company_id = Utils::contextCompanyId();
        $questionList =  array();
        $errorMessages =array();
        $model = null;
        $isNew = true;
        if (isset($id) && $id > 0) {
            $model = Quiz::model()->findByAttributes(array('quiz_id' => $id, 'company_id' => $company_id));
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
            $model->isNewRecord = false;
            $isNew = false;
        } else {
            $model=new Quiz;
            $model->isNewRecord = true;
        }

        if(isset($_POST['Quiz']))
        {
            $preStatus = $model->quiz_status_id;
            $model->attributes=$_POST['Quiz'];
            // if just set state to published then reset date
            if ($preStatus != $model->quiz_status_id && $model->quiz_status_id == Cons::POLICY_STATUS_PUBLISHED) {
                $model->publish_date = date('Y-m-d H:i:s');
            }
            $model->company_id = Utils::contextCompanyId();
            $model->user_id = userId();

            if($model->validate())
            {
                if ($model->save()) {
                    $questionList = getPost('QuizQuestion');
                    $errorMessages = Quiz_Ex::saveQuizQuestion(NULL,$model->quiz_id,$company_id,userId(),$questionList);
                    if (empty($errorMessages)) {
						Yii::app()->getUser()->setFlash('general', 'Information saved successfully.');
						if ($saveAndExit) {
							$this->redirect(url('quiz/list'));
						}
					}
                } else {
					$model->addError('quiz_title', "Failed to save changes. Try again.");
                    Yii::log(print_r($model->getErrors(), true), 'error', 'QuizController.actionEdit');
                }
            }
        } else {
            if (!$isNew) {
                $criteria = new CDbCriteria(array('order' => 'sort_order ASC'));
                $questionList = QuizQuestion::model()->findAllByAttributes(array('quiz_id' => $id, 'company_id'=>$company_id, 'status'=>1),$criteria);
            }
        }
        $this->pageTitle=Yii::app()->name.' - Edit Quiz';
        $editUrl = url('quiz/edit',array('id'=>Obscure::encode($model->quiz_id)));
        $this->render('vEdit',array('editUrl'=>$editUrl,'model'=>$model,'questionList'=>$questionList,'errorMessages'=>$errorMessages));
    }

    public function actionTest()
    {
		mPermission::checkAccess($this, 'Quizzes', 'Quizzes', Cons::ROLE_AUTHOR);
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		if (isset($id) && $id > 0) {
			//setSessionVar('site_type', 'UserSite');
			$this->forward('my/viewquiz/test/1/id'.Obscure::encode($id));
		}
    }
}