<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class EvidenceController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','delete','view','edit','download','viewevidence'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Compliance', 'Evidence');
		$this->render('vList');
    }
    public function actionGet()
    {
		mPermission::checkAccess($this, 'Compliance', 'Evidence', null, 'json');
        $s = $this->getSearchParams();
        $json = Evidence_Ex::getEvidencesJson(Utils::contextCompanyId(),$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = Evidence_Ex::getViewValues(Utils::contextCompanyId(),$id);
        $gmAjaxListView = new gmAjaxListView($values);
        return $values;
    }
    public function actionView()
    {
		mPermission::checkAccess($this, 'Compliance', 'Evidence');
        $this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUDITOR); // base GridViewController method calls override method getViewValues()
    }

    public function actionDownload()
    {
		mPermission::checkAccess($this, 'Compliance', 'Evidence', Cons::ROLE_AUDITOR, 'json');
        // will send headers and exit or show a message
        $this->downloadFile(mDownloadFile::Evidence,Cons::LIC_TRIAL,Cons::ROLE_AUDITOR);
    }

    public function actionViewEvidence()
    {
		mPermission::checkAccess($this, 'Compliance', 'Evidence', Cons::ROLE_AUDITOR);
        $id = $this->paramObscureId(); // will exit if bad param for security reasons
        $evidence = Evidence::model()->findByAttributes(array('evidence_id' => $id, 'company_id' => Utils::contextCompanyId()));
        if (empty($evidence)) {
            $this->renderMessage(array('messageId'=>0,'title' => '<i class="fa fa-warning"></i> Evidence Not Found',
                'message'=>'Sorry, Evidence file was not found. If you think this is an error then contact technical support.'));
        } else if ($evidence->file_type == 'application/pdf' ||
                    stripos($evidence->file_type,'text/') === 0 ||
                    stripos($evidence->file_type,'image/') === 0) {
            $this->pageTitle=Yii::app()->name." - View Evidence";
            $fileUrl =  Evidence_Ex::getTmpFileUrl($evidence);
            if (empty($fileUrl)) {
                $this->renderMessage(array('messageId'=>0,'title' => '<i class="fa fa-warning"></i> Evidence File Failure',
                    'message' => 'Sorry, was unable to retrieve the Evidence file. If you think this is an error then contact technical support.'));
            }
            $this->render('vViewEvidence',array('model'=>$evidence,'fileUrl'=>$fileUrl));
            return;
        } else {
            Evidence_Ex::downloadFile($evidence);
        }
    }    

    // Evidence Delete
    // can override to do post delete actions
    protected function postDelete($id,$model,$markStatusOnly=false) {
        return Evidence_Ex::removeFile($model);
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,Evidence::model(),array('evidence_id'=>$id,'company_id' => Utils::contextCompanyId()),$markStatusOnly);
    }
    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Compliance', 'Evidence', Cons::ROLE_ADMIN, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }

    public function actionEdit()
    {
		mPermission::checkAccess($this, 'Compliance', 'Evidence', Cons::ROLE_AUTHOR);

        $id = $this->paramObscureId(false); // will exit if bad param for security reasons
        $model = null;
        if (isset($id) && $id > 0) {
            $model = Evidence::model()->findByAttributes(array('evidence_id' => $id, 'company_id' => Utils::contextCompanyId()));
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
            $model->isNewRecord = false;
        } else {
            $model = new Evidence();
            $model->isNewRecord = true;
            $control_baseline_id = $this->paramObscureId(false,'cid');
            if (!empty($control_baseline_id)) {
                $model->setAttribute('control_baseline_id',$control_baseline_id);
            }
        }

        if(isset($_POST['Evidence']))
        {
            $model->attributes=$_POST['Evidence'];
            $model->company_id = Utils::contextCompanyId();
            $model->user_id = userId();

            if ($model->isNewRecord) {
                $model->doc_guid = Utils::create_guid();
            }

            $fileUrl = false;
            $tmpFileFullPath = false;
            // handle evidence upload
            if (isset($_FILES['evidence'])) {
                if (!empty($_FILES['evidence']['name'])) {
                    $tmpFileFullPath = $_FILES['evidence']['tmp_name'];
                    $model->file_size = $_FILES['evidence']['size'];
                    $model->file_type = $_FILES['evidence']['type'];
                    $model->file_name = $_FILES['evidence']['name'];
                    $model->file_ext = pathinfo($_FILES['evidence']['name'], PATHINFO_EXTENSION);
                }
            }

            if($model->validate())
            {
                if ($model->isNewRecord) {
                    if ($tmpFileFullPath !== false) {
                        $fileUrl = Evidence_Ex::saveFile($tmpFileFullPath, $model);
                        if ($fileUrl !== false) {
                            if ($model->save()) {
                                $this->redirect(url('evidence/list'));
                                return;
                            } else {
                                $model->addError('doc_title', "Failed to save evidence. Try again.");
                            }
                        } else {
                            $model->addError('file_name', "Failed to upload Evidence File.");
                        }
                    } else {
                        $model->addError('file_name', "Evidence File is required. Choose a file to upload.");
                    }
                } else if ($model->save()) {
                    $this->redirect(url('evidence/list'));
                    return;
                } else {
                    $model->addError('doc_title', "Failed to save changes. Try again.");
                    Yii::log(print_r($model->getErrors(), true), 'error', 'EvidenceController.actionEdit');
                }
            }
        }
        $this->render('vEdit',array('model'=>$model));
    }
}