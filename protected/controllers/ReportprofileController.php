<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class ReportprofileController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + getcompliance,getcompliancedetail',
            'ajaxOnly + getcompliance,getcompliancedetail',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('compliance','getcompliance','exportcompliance','printcompliance',
                    'compliancedetail','getcompliancedetail','exportcompliancedetail','printcompliancedetail'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionCompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Profile Compliance');
        $this->render('vCompliance');
    }

    public function actionGetcompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Profile Compliance', null, 'json');
        $mReportsProfile = new mReportsProfile();
        $results = $mReportsProfile->ComplianceReport(Utils::contextCompanyId());
        echo mGrid::toJsonGrid($results);
    }

    public function actionExportcompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Profile Compliance');
        $export = getGet('export',false);
        if ($export=='csv') {
            $mReportsProfile = new mReportsProfile();
            $data = $mReportsProfile->ComplianceReport(Utils::contextCompanyId(), true);
            if (count($data)) {
                mDownloadFile::outputCSV($data);
                Yii::app()->end();
            }
        }
    }

    public function actionPrintcompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'Profile Compliance');
        $mReportsProfile = new mReportsProfile();
        $data = $mReportsProfile->ComplianceReport(Utils::contextCompanyId(), true);
        $this->layout = false;
        $this->render('vCompliancePrint',array('data'=>$data));
    }

    public function actionCompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Profile Compliance');
		$id = $this->paramObscureId();
		$profile = UserProfile::model()->findByPk($id);
		$this->render('vComplianceDetail', array('profile'=>$profile, 'id'=>getGet('id')));
    }

    public function actionGetcompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Profile Compliance', null, 'json');
        $id = $this->paramObscureId();
        $mReportsProfile = new mReportsProfile();
		$results = $mReportsProfile->ComplianceReportDetail(Utils::contextCompanyId(), $id, false);
		echo mGrid::toJsonGrid($results);
    }

    public function actionExportcompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Profile Compliance');
        $id = $this->paramObscureId();
        $profile = UserProfile::model()->findByPk($id);
        $export = getGet('export',false);
        if ($export=='csv') {
            $mReportsProfile = new mReportsProfile();
            $data = $mReportsProfile->ComplianceReportDetail(Utils::contextCompanyId(), $id, true);
            if (count($data)) {
                mDownloadFile::outputCSV($data);
                Yii::app()->end();
            }
        }
    }

    public function actionPrintcompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'Profile Compliance');
        $id = $this->paramObscureId();
        $profile = UserProfile::model()->findByPk($id);
        $mReportsProfile = new mReportsProfile();
        $data = $mReportsProfile->ComplianceReportDetail(Utils::contextCompanyId(), $id,true);
        $this->layout = false;
        $this->render('vComplianceDetailPrint',array('profile'=>$profile,'data'=>$data));
    }
}