<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('captcha','index','home','contact','page','error','signin','eula',),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(
                	'home',
                	'companyhome',
                	'userhome',
                	'accountoptions',
					'context',
					'registercompany',
					'company',
					'logout',
					//'toggle',
					'join'
				),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin', 'adminhome', 'token'),
                //'users'=>array('admin'),
                'expression'=>'!user()->isGuest && Utils::isSuper()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /*
     *  This is the auto account creation and signin feature for partner integration. For example eRiskHub.
     *
     *  First time eRiskHub user without an account already.
     *  If the email address does not exist as a user in our system already then this happens:
     *      - We create a new site user account and auto activate it and set the password to a long random string.
     *        They do not really need a password if always arriving to us from the partner site via a auto signin link.
     *        If they they can use the password reset feature to reset password anytime to login without the partner link.
     *      - We auto login the user into the site.
     *      - We then create a company for this user with a Free license and the company name is the email address so it is unique.
     *      - We then show them the Profile edit page so they can enter their First and Last name and change the username if they wish from the default of the email address.
     *      - Once they Save the Profile, then they are shown the Company Profile edit screen to save changes.
     *      - Then finally they see the Control Wizard to add controls.
     *
     *  First time eRishHub user with an account already.
     *  If the email address already exist as a user in our system then this happens:
     *      - We auto login the user into the site.
     *      - We check if the user has registered a company, and if not then create a company for this user with a Free license and the company name is the email address so it is unique.
     *      - If the company has no Controls defined then we take them to the Control Wizard, other wise the home page.
     *
     *  Returning eRiskHub user.
     *      - For the typical use case of a returning eRiskHub user, they would just auto login to the site home page.
     */
    public function actionSignin()
    {
        $partner = null;
        $partnerId = getGet('p');
        if (!empty($partnerId)) {
            $partner = Partner::model()->findByPk($partnerId);
        }
        if (isset($partner)) {
            $encodedToken = getGet('token');
            $decodedToken = null;
            try {
                $easySecLogin = new EasySecLogin($partner->partner_id, $partner->shared_secret);
                $decodedToken = $easySecLogin->decodeToken($encodedToken);
                if ($decodedToken->iss == $partnerId) {
                    $email = $decodedToken->sub;
                    $mSiteUser = new mSiteUser($email,$partnerId);
                    if ($mSiteUser->AutoCreateUser()) {
                        $mSiteUser->AutoLogin();
                        $company_id = $mSiteUser->RegisterCompany(Cons::LIC_TRIAL);
                        $mSiteUser->AddDemoLicenseContent();
                        if ($mSiteUser->IsNewUser()) {
                            // first time user let them edit their profile and company name first
                            $this->redirect(url('site/join',array('partner'=>$partnerId)));
                        } else {
                            // if no controls defined take to wizard next
                            if (!empty($company_id) && ControlBaseline_Ex::getControlCount($company_id) <= 0) {
                                $this->redirect(url('wizard/control'));
                            } else {
                                $this->redirect(url('site/home'));
                            }
                        }
                    } else {
                        Yii::log("Failed to AutoCreateUser for email: ".$decodedToken->sub." and partner_id: ".$partnerId, 'error', 'SiteController.actionSignin');
                    }
                } else {
                    //echo "FAILED TO DECODE!<br/>";
                }
            } catch (Exception $e) {
                //echo "Exception: " .$e->getMessage(). "<br/>";
                Yii::log($e->getMessage()."\n", 'error', 'SiteController.actionSignin');
            }
        } else {
            //echo "Invalid p parameter!<br/>";
        }
        $this->redirect(url('site/home'));
    }

    public function actionJoin()
    {
        $partnerId = getGet('partner');
        if (!isset($partnerId)) {
            $partnerId = getPost('partner');
        }
        if ($partnerId > 0) {
            $partnerLogo = images('partner-logo-'.$partnerId.'.png');
        } else {
            $partnerLogo = images('logo.png');
        }

        $profile = Profile::model()->findByAttributes(array('user_id' => userId()));
        $company = Company::model()->findByAttributes(array('company_id' => Utils::contextCompanyId(), 'user_id' => userId()));
        if (isset($_POST['Company'])) {
            $company->attributes = $_POST['Company'];
            $company->trial_expiry = new CDbExpression('NOW() + INTERVAL 15 DAY');
            $profile->attributes = $_POST['Profile'];
            if ($company->validate() && $profile->validate()) {
                $company->save();
                $profile->save();
                // create new company context
                mCompanyContext::setSessionVars(userId());
                // if no controls defined take to wizard next
                if (ControlBaseline_Ex::getControlCount($company->company_id) > 0) {
                    $this->redirect(array('site/home'));
                } else {
                    $this->redirect(array('wizard/control'));
                }
            }
        } else {
            $company->name = (userEmail() == $company->name) ? '' : $company->name;
        }
        $this->layout = 'nomenu';
        $this->render('vJoinPartner', array('partnerId' => $partnerId, 'partnerLogo' => $partnerLogo, 'model' => $company, 'profile' => $profile));
    }

    /*
     *  This is for super admins to create and test auto login account creation tokens for partner integration.
     *  This will create a test link that you can click on to test the action above actionSignin.
     *  Just type in the url /site/token/email/{some email address for testing}
     */
    public function actionToken()
    {
        if (Utils::isSuper()) { //only super users
            $email = getGet('email');
            $partner = null;
            $partnerId = getGet('p',1);
            if (!empty($partnerId)) {
                $partner = Partner::model()->findByPk($partnerId);
            }
            if (isset($partner)) {
                $easySecLogin = new EasySecLogin($partner->partner_id, $partner->shared_secret);
                $localUrl = $easySecLogin->createUrl($email, null, url('site/signin'));
                echo "EMAIL: " . $email . "<br/><br/>";
                echo 'Test local link: <a href="' . $localUrl . '">' . $localUrl . '</a><br/>';
            } else {
                echo 'Could not find partner record.';
            }
        }
    }

    public function actionIndex()
    {
        $this->redirect(array('site/home'));
    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionHome()
	{
		if (getSessionVar('site_type') == 'UserSite') {
			$this->redirect(url('site/UserHome'));
        } else if (getSessionVar('site_type') == 'CompanySite' || getSessionVar('site_type') == 'InitSite') {
			$this->redirect(url('site/CompanyHome'));
		} else if (getSessionVar('site_type') == 'AdminSite') {
			$this->redirect(url('site/AdminHome'));
		} else {
			$this->redirect(url('user/logout'));
		}
	}

	public function actionUserHome()
	{
		if (Utils::isRoleUser()) {
			setSessionVar('site_type', 'UserSite');
			$this->redirect(url('my/inbox'));
		} else {
			$this->redirect(array('site/accountoptions'));
		}
	}

	public function actionCompanyHome()
	{
		setSessionVar('site_type', 'CompanySite');
		if (Utils::isRoleAuditor()) { // double checking to see if the user really has access to the company site
			$invite_guid = getSessionVar('invite_guid');
			if (!empty($invite_guid)) {
				$this->redirect(array('invite/open'));
			} else {
				$result =  array();
				$user_id = Yii::app()->user->id;
				$messages = Message::model()->findAllByAttributes(array('recipient_id' => $user_id, 'read' => 0), array('order' => 'created_on desc'));
				$this->render('vHome',array('messages'=>$messages));
			}
		} else if (Utils::isRoleUser()) {
			setSessionVar('site_type', 'UserSite');
			$this->redirect(url('my/inbox'));
		} else {
			setSessionVar('site_type', 'InitSite');
			$this->redirect(array('site/accountoptions'));
		}
	}

	public function actionAdminHome()
	{
		if (Utils::isSuper()) { //only super users
			setSessionVar('site_type', 'AdminSite');
			$this->redirect(array('admin/company'));
		}
	}

	public function actionAdmin()
    {
        if (Utils::isSuper()) { //only super users
            $this->redirect(array('admin/controllibrary/admin'));
        }
    }

    public function actionContext()
    {
        $id = $this->paramObscureId(false); // will exit if bad param for security reasons
        if (isset($id) && $id > 0) {
            mCompanyContext::setSessionVars(userId(), $id);
            $this->redirect(array('site/home'));
        }
        $this->redirect(array('site/home'));
    }

	public function actionAccountoptions()
	{
        if(isset($_POST['context_company_id'])) {
            mCompanyContext::setSessionVars(userId(), $_POST['context_company_id']);
            $this->redirect(array('site/home'));
        } else {
            $this->render('vAccountOptions');
        }
	}

    public function actionCompany()
    {
    	mPermission::checkAccess($this, 'Company', 'Company Info');
        $model = Company::model()->findByAttributes(array('company_id' => Utils::contextCompanyId()));
        $editUrl = null;
        if (isset($model)) {
			$editUrl = Yii::app()->createAbsoluteUrl('site/registercompany',array('id'=>Obscure::encode(Utils::contextCompanyId())));
            $this->render('vCompany', array('model' => $model, 'editUrl' => $editUrl));
        } else {
            $this->renderMessage(Cons::MSG_COMPANY_MISSING);
        }
    }

	public function actionRegistercompany()
	{
		//mPermission::checkAccess($this, 'Company', 'Register New Company'); // commented out for now since new users need access as well
        $employeeModel = null;
        $maxCompanyOwnership = Utils::isSuper() ? 99999 : 3;
        $createCompany = false;

        $guid = getGet('guid');
        if (empty($guid)) {
            $id = $this->paramObscureId(false);

            if (isset($id) && $id > 0) {
                // if owner of current company then ok to edit
                if (Utils::isRoleOwner() && $id == Utils::contextCompanyId()) {
                    $company = Company::model()->findByAttributes(array('company_id' => $id));
                } else {
                    // can only edit if the user owns/created it in any role
                    $company = Company::model()->findByAttributes(array('company_id' => $id, 'user_id' => userId()));
                }
                if (!isset($company)) {
                    badurl('User attempting wrongful access.');
                }
                $createCompany = true;
                $company->isNewRecord = false;
            } else {

                if (Utils::contextCompanyCount() >= $maxCompanyOwnership) {
                    $companiesUserOwns = Company_Ex::getCompaniesUserOwns(userId());
                    if (!empty($companiesUserOwns) && count($companiesUserOwns) >= $maxCompanyOwnership) {
                        $this->renderMessage(array('messageId'=>Cons::MSG_COMPANY_MAX,'maxCompanyOwnership'=>$maxCompanyOwnership));
                        return;
                    }
                }

				$createCompany = true;
                $company=new Company;
                $company->isNewRecord = true;

            }
        } else {
            $company = Company::model()->findByAttributes(array('company_guid' => $guid, 'user_id' => userId()));
            if (!isset($company)) {
                badurl('User attempting wrongful access.');
            }
            $company->isNewRecord = false;
        }

		if(isset($_POST['Company']))
		{
			$company->attributes=$_POST['Company'];
            if ($company->isNewRecord) {
                $company->user_id = userId(); // only set on new to mark owner of company record
                $company->license_level = Cons::LIC_TRIAL;
				$company->trial_expiry = new CDbExpression('NOW() + INTERVAL 15 DAY');
                $company->status = 1;
                $company->company_guid = Utils::create_guid();
                $company->password = str_replace('-','',Utils::create_guid());
                // new company user
                $employeeModel = new Employee();
            }

            $notFound = Company::model()->findByAttributes(array('name'=>$company->name));
            if (isset($notFound) && $notFound->company_id != $company->company_id) {
                $notFound = false;
                $company->addError('name','Company name already in use.');
            } else {
                $notFound = true;
            }

            if ($notFound) {
                if ($company->validate()) {
                    // handle logo upload
                    if (isset($_FILES['logo'])) {
                        $tmpFileFullPath = $_FILES['logo']['tmp_name'];
                        if (!empty($_FILES['logo']['name'])) {
                            $imageUpload = new ImageUpload;
                            $imageUpload->desired_height = 58;
                            $filename = Utils::create_guid() . '_' . time() . ".png";
                            $retVal = $imageUpload->saveImage($tmpFileFullPath, $filename, "files/logos/");
                            if (!empty($retVal)) {
                                if (!empty($company->logo)) {
                                    $oldfilepath = __DIR__ . '/../../files/logos/' . $company->logo;
                                    if (file_exists($oldfilepath)) {
                                        unlink($oldfilepath);
                                    }
                                }
                                $company->logo = $filename;
                            }
                        }
                    }

                    if ($company->save()) {
                        if (!is_null($employeeModel)) {
                            $employeeModel->company_role_id = Cons::ROLE_OWNER;
                            $employeeModel->company_id = $company->company_id;
                            $names = Utils::getUserFirstLastName(userId());
                            $employeeModel->firstname = $names['first_name'];
                            $employeeModel->lastname = $names['last_name'];
                            $employeeModel->email = userEmail();
                            if (!$employeeModel->save()) {
                                Yii::log(print_r($employeeModel->getErrors(), true), 'error', 'SiteController.actionRegistercompany');
                            }
                        }
                        mCompanyContext::setSessionVars(userId());

                        // create default security roles if company has none
                        SecurityRole_Ex::copyDefaultRoles($company->company_id, $company->name);
                        // create default incident types if company has none
                        IncidentType_Ex::copyDefault($company->company_id);
                        // create default user profile if company has none
                        UserProfile_Ex::createDefaultUserProfile($company->company_id);

						if ($createCompany) {
							$results = EmailUtils::sendMail(
								param('registrationNotificationEmail'),
								"New Company Registration",
								"A new company [$company->name]  has been registered by $employeeModel->firstname $employeeModel->lastname ($employeeModel->email)."
							);
						}

                        // if no controls defined take to wizard next
                        if (ControlBaseline_Ex::getControlCount($company->company_id) > 0) {
                            $this->redirect(array('site/company'));
                        } else {
							setSessionVar('site_type', 'CompanySite');
                            $this->redirect(array('wizard/control'));
                        }
                    } else {
                        Yii::log(print_r($company->getErrors(), true), 'error', 'SiteController.actionRegistercompany');
                    }
                }
            }
		}
		$this->render('vRegisterCompany',array('company'=>$company));
	}

    /**
     * This is the action to render EULA.
     */
    public function actionEula()
    {
        $this->renderPartial('vEula');
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('vError', $error);
		} else {
            $this->renderMessage(Cons::MSG_ERROR);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
                $model->sendEmailToWebMaster();
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('vContact',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
        Utils::setUserSiteModeOff();
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}