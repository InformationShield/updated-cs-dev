<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class ReportuserController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + getusercompliance,getusercomplianceexport,getusercompliancedetail',
            'ajaxOnly + getusercompliance,getusercomplianceexport,getusercompliancedetail',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('usercompliance','getusercompliance',
                    'usercomplianceexport','getusercomplianceexport','exportusercompliance','printusercompliance',
                    'usercompliancedetail','getusercompliancedetail','exportusercompliancedetail','printusercompliancedetail'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionUsercompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance');
		$count = Employee_Ex::getEmployeeCount(Utils::contextCompanyId());
		$gridHeight = mGrid::calcHeight($count);
		$this->render('vUserCompliance', array('height'=>$gridHeight));
    }

    public function actionGetusercompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance', null, 'json');
        $mReportsUsers = new mReportsUsers();
        $results = $mReportsUsers->UserComplianceReport(Utils::contextCompanyId());
        echo mGrid::toJsonGrid($results);
    }

    public function actionUsercomplianceexport()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance');
		$this->render('vUserComplianceExport');
    }

    public function actionGetusercomplianceexport()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance', null, 'json');
        $mReportsUsers = new mReportsUsers();
		$results = $mReportsUsers->UserComplianceReport(Utils::contextCompanyId());
        echo mGrid::toJsonGrid($results);
    }

    public function actionExportusercompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance');
        $export = getGet('export',false);
        if ($export=='csv') {
            $mReportsUsers = new mReportsUsers();
			$userComplianceData = $mReportsUsers->UserComplianceReport(Utils::contextCompanyId(), true);
            if ($userComplianceData) {
                mDownloadFile::outputCSV($userComplianceData);
                Yii::app()->end();
            }
        }
    }

    public function actionPrintusercompliance()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance');
        $mReportsUsers = new mReportsUsers();
		$userComplianceData = $mReportsUsers->UserComplianceReport(Utils::contextCompanyId(), true);
        $this->layout = false;
        $this->render('vUserCompliancePrint',array('data'=>$userComplianceData));
    }

    public function actionUsercompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance');
        $id = $this->paramObscureId();
        $userInfo = Employee_Ex::getUserDetails($id);
        $this->render('vUserComplianceDetail', array('userInfo'=>$userInfo, 'id'=>getGet('id')));
    }

    public function actionGetusercompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance', null, 'json');
        $id = $this->paramObscureId();
        $mReportsUsers = new mReportsUsers();
		$results = $mReportsUsers->ComplianceReportDetail(Utils::contextCompanyId(), $id);
		echo mGrid::toJsonGrid($results);
    }

    public function actionExportusercompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance');
        $id = $this->paramObscureId();
        $export = getGet('export',false);
        if ($export=='csv') {
            $mReportsUsers = new mReportsUsers();
			$results = $mReportsUsers->ComplianceReportDetail(Utils::contextCompanyId(), $id, true);
            if ($results) {
                mDownloadFile::outputCSV($results);
                Yii::app()->end();
            }
        }
    }

    public function actionPrintusercompliancedetail()
    {
		mPermission::checkAccess($this, 'Reports', 'User Compliance');
		$id = $this->paramObscureId();
		$userInfo = Employee_Ex::getUserDetails($id);
		$mReportsUsers = new mReportsUsers();
		$results = $mReportsUsers->ComplianceReportDetail(Utils::contextCompanyId(), $id, true);
		$this->layout = false;
		$this->render('vUserComplianceDetailPrint',array('data'=>$results,'userInfo'=>$userInfo));
    }
}