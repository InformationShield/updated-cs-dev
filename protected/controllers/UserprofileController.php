<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class UserprofileController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + delete,copy,deleteuserprofiletraining,deleteuserprofilepolicy', // we only allow deletion via POST request
            'ajaxOnly + delete,copy,view,deleteuserprofiletraining,deleteuserprofilepolicy',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','delete','copy','view','edit','deleteuserprofiletraining','deleteuserprofilepolicy'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Company', 'User Profiles');
		$this->render('vList');
    }
    public function actionGet()
    {
		mPermission::checkAccess($this, 'Company', 'User Profiles', null, 'json');
        $s = $this->getSearchParams();
        $json = UserProfile_Ex::getUserProfilesJson(Utils::contextCompanyId(), $s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    // UserProfile Delete
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,UserProfile::model(),array('user_profile_id'=>$id,'company_id' => Utils::contextCompanyId()),$markStatusOnly);
    }
    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Company', 'User Profiles', Cons::ROLE_ADMIN, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = UserProfile_Ex::getViewValues(Utils::contextCompanyId(),$id);
        $skipEncode = array('Description','Policy List','Quiz List','Training List');
        return $values;
    }
    public function actionView()
    {
		mPermission::checkAccess($this, 'Company', 'User Profiles');
        $this->renderView(Cons::LIC_TRIAL,Cons::ROLE_ADMIN); // base GridViewController method calls override method getViewValues()
    }

    // UserProfile Copy
    // override method called by ajaxCopy()
    // function is to copy and returns params for json staus and message
    // can be overridden for other use cases
    protected function doCopy($id)
    {
        $status = 'error';
        $message = 'Failed to copy the user profile.';
        $model1 = UserProfile::model()->findByAttributes(array('user_profile_id' => $id, 'company_id' => Utils::contextCompanyId()));
        if (isset($model1)) {
            $model2 = new UserProfile;
            $attr = $model1->attributes;
            unset($attr['user_profile_id']);
            $model2->attributes = $attr;
            $model2->company_id = Utils::contextCompanyId();
            if ($model2->save()) {
                $status = 'success';
                $message = 'The user was copied to your profile.';
            } else {
                $message = 'Failed to copy the user profile.';
                Yii::log(print_r($model2->getErrors(), true), 'error', 'UserprofileController.actionCopy');
            }
        } else {
            $message = 'The user profile record was not found.';
        }
        return array('status'=>$status,'message'=>$message);
    }
    public function actionCopy()
    {
		mPermission::checkAccess($this, 'Company', 'User Profiles', Cons::ROLE_ADMIN, 'json');
        echo $this->ajaxCopy(Cons::LIC_TRIAL,Cons::ROLE_ADMIN);
    }

    public function actionDeleteuserprofiletraining()
    {
		mPermission::checkAccess($this, 'Company', 'User Profiles', Cons::ROLE_ADMIN, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons

		$status = 'error';
		$model = UserProfileTraining::model()->findByAttributes(array('user_profile_training_id' => $id, 'company_id' => Utils::contextCompanyId()));
		if (isset($model)) {
			if ($model->delete()) {
				$status = 'success';
				$message = 'The user profile training was deleted.';
			} else {
				Yii::log(print_r($model->getErrors(), true), 'error', 'UserprofileController.actionDelete');
				$message = 'Failed to delete the user profile training.';
			}
		} else {
			$message = 'The user profile training record was not found.';
		}
        echo mGrid::toGridMessage($status, $message);
    }

    public function actionDeleteuserprofilepolicy()
    {
		mPermission::checkAccess($this, 'Company', 'User Profiles', Cons::ROLE_ADMIN, 'json');
		$status = 'error';
		$message = 'Failed to delete the user profile policy.';

		$id = $this->paramObscureId(); // will exit if bad param for security reasons

		$model = UserProfilePolicy::model()->findByAttributes(array('user_profile_policy_id' => $id, 'company_id' => Utils::contextCompanyId()));
		if (isset($model)) {
			if ($model->delete()) {
				$status = 'success';
				$message = 'The user profile policy was deleted.';
			} else {
				Yii::log(print_r($model->getErrors(), true), 'error', 'UserprofileController.actionDelete');
				$message = 'Failed to delete the user profile policy.';
			}
		} else {
			$message = 'The user profile policy record was not found.';
		}
        echo json_encode(array('status' => $status, 'message'=>$message));
    }

    public function actionEdit()
    {
		mPermission::checkAccess($this, 'Company', 'User Profiles', Cons::ROLE_ADMIN);

        $id = $this->paramObscureId(false); // will exit if bad param for security reasons

        $company_id = Utils::contextCompanyId();
        $policyList = array();
        $trainingList =  array();
        $quizList =  array();
        $errorMessages =array();
        $model = null;
        $isNew = true;
        if (isset($id) && $id > 0) {
            $model = UserProfile::model()->findByAttributes(array('user_profile_id' => $id, 'company_id' => $company_id));
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
            $isNew = false;
        } else {
            $model=new UserProfile;
            $model->company_id = $company_id;
            $model->profile_id = UserProfile_Ex::getNextProfileId($company_id);
        }

        if(isset($_POST['UserProfile'])) {
            if ($_POST['UserProfile']['profile_id'] <= 0) {
                if ($model->profile_id > 0) {
                    $_POST['UserProfile']['profile_id'] = $model->profile_id;
                } else {
                    $_POST['UserProfile']['profile_id'] = UserProfile_Ex::getNextProfileId($company_id);
                }
            }
            $model->attributes=$_POST['UserProfile'];
            $model->company_id = $company_id;
            if($model->validate())
            {
                if($model->save()) {
                    $savePolicyList = getPost('PolicyList');
                    $saveTrainingList = getPost('TrainingList');
                    $saveQuizList = getPost('QuizList');
                    $errorMessages = UserProfile_Ex::saveUserProfilePolicy($model->primaryKey, $company_id, $savePolicyList);
                    $errorMessages2 = UserProfile_Ex::saveUserProfileTraining($model->primaryKey,$company_id,$saveTrainingList);
                    $errorMessages = array_merge($errorMessages,$errorMessages2);
                    $errorMessages2 = UserProfile_Ex::saveUserProfileQuiz($model->primaryKey,$company_id,$saveQuizList);
                    $errorMessages = array_merge($errorMessages,$errorMessages2);
                    if (empty($errorMessages)) {
                        $this->redirect(url('userprofile/list'));
                    }
                } else {
                    Yii::log(print_r($model->getErrors(), true), 'error', 'UserprofileController.actionEdit');
                }
            }
        } else {
            if (!$isNew) {
                $policyList = UserProfile_Ex::getUserProfilePolicy($id, $company_id);
                $trainingList = UserProfile_Ex::getUserProfileTraining($id, $company_id);
                $quizList = UserProfile_Ex::getUserProfileQuiz($id, $company_id);
            }
        }
        $criteria = new CDbCriteria(array('order' => 'doc_title ASC, author asc, publish_date ASC'));
        $criteriaQ = new CDbCriteria(array('order' => 'quiz_title ASC, publish_date ASC'));
        $policies = Policy::model()->findAllByAttributes(array('company_id'=>Utils::contextCompanyId(),'policy_status_id'=>3,'status'=>1),$criteria);
        $trainings = Training::model()->findAllByAttributes(array('company_id'=>Utils::contextCompanyId(),'training_status_id'=>3,'status'=>1),$criteria);
        $quizzes = Quiz::model()->findAllByAttributes(array('company_id'=>Utils::contextCompanyId(),'quiz_status_id'=>3,'status'=>1),$criteriaQ);
        $this->render('vEdit',array('model'=>$model,'policyList'=>$policyList,'trainingList'=>$trainingList,'quizList'=>$quizList,'policies'=>$policies,'trainings'=>$trainings,'quizzes'=>$quizzes,'errorMessages'=>$errorMessages));
    }
}