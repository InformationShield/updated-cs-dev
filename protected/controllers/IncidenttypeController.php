<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class IncidenttypeController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('list','get','delete','view','edit'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Incidents', 'Manage Incident Types');
        // create default incident types if company has none
        IncidentType_Ex::copyDefault(Utils::contextCompanyId());

        $listUrl = url('incidenttype/get');
		$this->render('vList', array('listUrl'=>$listUrl));
    }

    public function actionGet()
    {
		mPermission::checkAccess($this, 'Incidents', 'Manage Incident Types', null, 'json');
        $s = $this->getSearchParams();
        $employeeId = getGet('cuid');
        if ($employeeId !== null && Obscure::isObscure($employeeId) !== false) {
            $employeeId = Obscure::decode($employeeId);
        } else {
            $employeeId = null;
        }
        $json = IncidentType_Ex::getIncidentTypesJson(Utils::contextCompanyId(),$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    // Incidenttype Delete
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,IncidentType::model(),array('incident_type_id'=>$id,'company_id' => Utils::contextCompanyId()),$markStatusOnly);
    }
    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Incidents', 'Manage Incident Types', Cons::ROLE_AUTHOR, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = IncidentType_Ex::getViewValues(Utils::contextCompanyId(),$id);
        $skipEncode = array('Description');
        return $values;
    }
    public function actionView()
    {
		mPermission::checkAccess($this, 'Incidents', 'Manage Incident Types', Cons::ROLE_AUDITOR, 'json');
        $this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUDITOR); // base GridViewController method calls override method getViewValues()
    }

    public function actionEdit()
    {
		mPermission::checkAccess($this, 'Incidents', 'Manage Incident Types', Cons::ROLE_AUTHOR);

        $id = $this->paramObscureId(false); // will exit if bad param for security reasons

        if (isset($id) && $id > 0) {
            $model = IncidentType::model()->findByAttributes(array('incident_type_id' => $id, 'company_id' => Utils::contextCompanyId()));
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
            $model->isNewRecord = false;
        } else {
            $model=new IncidentType();
            $model->isNewRecord = true;
        }

        if(isset($_POST['IncidentType']))
        {
            $model->attributes=$_POST['IncidentType'];
            $model->company_id = Utils::contextCompanyId();
            if($model->validate())
            {
                if ($model->save()) {
                    $this->redirect(url('incidenttype/list'));
                    return;
                } else {
                    $model->addError('incident_type', "Failed to save changes. Try again.");
                    Yii::log(print_r($model->getErrors(), true), 'error', 'IncidenttypeController.actionEdit');
                }
            }
        }
        $this->pageTitle=Yii::app()->name.' - Edit Incident Type';
        $this->render('vEdit',array('model'=>$model));
    }
}