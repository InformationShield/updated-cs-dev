<?php
require_once(Yii::app()->basePath.'/appinclude/Helper.php');

class TrainingController extends GridViewController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access user for CRUD operations
            'postOnly + get,delete', // we only allow deletion via POST request
            'ajaxOnly + get,delete,view',
        );
    }

    /**
     * Specifies the access user rules.
     * This method is used by the 'accessControl' filter.
     * @return array access user rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users
                'actions'=>array(''),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('list','get','delete','view','edit','download'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Training');
		$this->render('vList');
    }

    public function actionGet()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Training', null, 'json');
        $s = $this->getSearchParams();
        $json = Training_Ex::getTrainingJson(Utils::contextCompanyId(),$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort']);
        echo $json;
    }

    public function actionDownload()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Training', Cons::ROLE_AUDITOR);
        // will send headers and exit or show a message
        $this->downloadFile(mDownloadFile::Training,Cons::LIC_TRIAL,Cons::ROLE_AUDITOR);
    }

    // Training Delete
    // can override to do post delete actions
    protected function postDelete($id,$model,$markStatusOnly=false) {
        $results = true;
        if ($model->training_type == 1) {
            $results = Training_Ex::removeFile($model);
        } else if ($model->training_type == 2) {
            $results = VideoTraining_Ex::deleteVideoTraining(null,$id,Utils::contextCompanyId(),$markStatusOnly);
        }
        return $results;
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
        return $this->doModelDelete($id,Training::model(),array('training_id'=>$id,'company_id' => Utils::contextCompanyId()),$markStatusOnly);
    }
    public function actionDelete()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Training', Cons::ROLE_AUTHOR, 'json');
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id, true);
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = Training_Ex::getViewValues(Utils::contextCompanyId(),$id);
        $skipEncode = array('Description');
        return $values;
    }
    public function actionView()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Training', Cons::ROLE_AUDITOR);
        $this->renderView(Cons::LIC_TRIAL,Cons::ROLE_AUDITOR); // base GridViewController method calls override method getViewValues()
    }

    public function actionEdit()
    {
		mPermission::checkAccess($this, 'Training', 'Manage Training', Cons::ROLE_AUTHOR);
        $id = $this->paramObscureId(false); // will exit if bad param for security reasons

        if (isset($id) && $id > 0) {
            $model = Training::model()->findByAttributes(array('training_id' => $id, 'company_id' => Utils::contextCompanyId()));
            if (!isset($model)) {
                badurl('User attempting wrongful access.');
            }
            $model->isNewRecord = false;
        } else {
            $training_type = getGet('type',1);
            $training_type = ($training_type == 2) ? 2 : 1;
            $model=new Training;
            $model->isNewRecord = true;
            $model->training_type = $training_type;
        }

        if(isset($_POST['Training']))
        {
            $model->attributes=$_POST['Training'];
            $model->company_id = Utils::contextCompanyId();
            $model->user_id = userId();
            if (!empty($model->publish_date)) {
                $model->publish_date = DateHelper::FormatDateTimeString($model->publish_date,'Y-m-d');
            }
            if ($model->isNewRecord) {
                $model->doc_guid = Utils::create_guid();
                $model->publish_date = date('Y-m-d');
            }

            $fileUrl = false;
            $tmpFileFullPath = false;
            // handle Training file upload
            if (isset($_FILES['Training'])) {
                if (!empty($_FILES['Training']['name'])) {
                    $tmpFileFullPath = $_FILES['Training']['tmp_name'];
                    $model->file_size = $_FILES['Training']['size'];
                    $model->file_type = $_FILES['Training']['type'];
                    $model->file_name = $_FILES['Training']['name'];
                    $model->file_ext = pathinfo($_FILES['Training']['name'], PATHINFO_EXTENSION);
                }
            }

            if($model->validate())
            {
                if ($model->isNewRecord) {
                    if ($model->training_type == 1) {
                        if ($tmpFileFullPath !== false) {
                            $fileUrl = Training_Ex::saveFile($tmpFileFullPath, $model);
                            if ($fileUrl !== false) {
                                if ($model->save()) {
                                    $this->redirect(url('training/list'));
                                    return;
                                } else {
                                    $model->addError('doc_title', "Failed to save training document. Try again.");
                                }
                            } else {
                                $model->addError('file_name', "Failed to upload Training File.");
                            }
                        } else {
                            $model->addError('file_name', "Training File is required. Choose a file to upload.");
                        }
                    } else {
                        if ($model->save()) {
                            $this->redirect(url('videotraining/editseries',array('ret'=>1,'library'=>0,'id'=>Obscure::encode($model->training_id))));
                            return;
                        } else {
                            $model->addError('doc_title', "Failed to save training document. Try again.");
                        }
                    }
                } else if ($model->save()) {
                    if ($model->training_type == 1 && $tmpFileFullPath !== false) {
                        $fileUrl = Training_Ex::saveFile($tmpFileFullPath, $model);
                        if ($fileUrl !== false) {
                            $this->redirect(url('training/list'));
                            return;
                        } else {
                            $model->addError('file_name', "Failed to upload Training File.");
                        }
                    } else {
                        if ($model->training_type == 2) {
                            $url = url('videotraining/editseries',array('ret'=>1,'library'=>0,'id'=>Obscure::encode($model->training_id)));
                        } else {
                            $url = url('training/list');
                        }
                        $this->redirect($url);
                        return;
                    }
                } else {
                    $model->addError('doc_title', "Failed to save changes. Try again.");
                    Yii::log(print_r($model->getErrors(), true), 'error', 'TrainingController.actionEdit');
                }
            }
        }
        if (!empty($model->publish_date)) {
            $model->publish_date = DateHelper::FormatDateTimeString($model->publish_date,'m/d/Y');
        }
        $this->render('vEdit',array('model'=>$model));
    }
}