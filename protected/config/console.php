<?php
// Admin login [admin]: admin
// Admin email [webmaster@example.com]: support@easyitcompliance.com
// Admin password [admin]: cyan43Elephant

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=easysecprod',
			'emulatePrepare' => true,
			'username' => 'easysecowner',
			'password' => 'cyan43Elephant',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, trace',
				),
			),
		),
	),
	
    'modules'=>array(
		
		// syp added for yii-user extension
        'user'=>array(
            # encrypting method (php hash function)
            'hash' => 'sha1',

            # send activation email
            'sendActivationMail' => true,

            # allow access for non-activated users
            'loginNotActiv' => false,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),

            # login form path
            'loginUrl' => array('/user/login'),

            # page after login
            'returnUrl' => array('/user/profile'),

            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
        #...
    ),
);