<?php
if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'easysec') { // assume dev env
    define('DB_TIME_ZONE', 'US/Central');
    define('USER_TIME_ZONE', 'US/Central');
} else {    // assume prod env
    define('DB_TIME_ZONE', 'US/Central');
    define('USER_TIME_ZONE', 'US/Central');
//    define('DB_TIME_ZONE', 'GMT');
//    define('USER_TIME_ZONE', 'US/Central');
}

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
define('APP_DEVELOPMENT_SITE', true);
define('GENERAL_ENCRYPTION_KEY','GjwDpkT79Eh2buZ6UqTFR5HXFaEcSWRC');
define('MEMCACHE_ON', false);
define('HEADER_BAR_COLOR', 'darkseagreen'); // local
//define('HEADER_BAR_COLOR', '#f0ad4e'); // dev
//define('HEADER_BAR_COLOR', '#EDEDED'); // prod

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Compliance Shield',
    //'theme'=>'default',

	// preloading 'log' component, syp added bootstrap
    //'preload'=>array('log', 'bootstrap'),
	'preload'=>array('log',),

	// autoloading model and component classes
	'import'=>array(
        'application.appinclude.*',
        'application.components.*',
        'application.gadget.controllers.*',
        'application.gadget.models.*',
        'application.models.*',
        'application.models_ex.*',
        'application.models_prime.*',

        // syp added for yii-user extension
        'application.modules.user.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>false,
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1','192.168.2.*'),
		),
        'admin'=>array(),
		
		// syp added for yii-user extension
        'user'=>array(
            # encrypting method (php hash function)
            'hash' => 'sha1',

            # send activation email
            'sendActivationMail' => true,

            # allow access for non-activated users
            'loginNotActiv' => false,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),

            # login form path
            'loginUrl' => array('/user/login'),

            # page after login
            'returnUrl' => array('/site/home'),

            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),		
	),

	// application components
	'components'=>array(
//		'clientScript' => array(
//			// disable default yii scripts
//			'scriptMap' => array(
//				'jquery.js'     => false,
//				'jquery.min.js' => false,
//				'core.css'      => false,
//				'styles.css'    => false,
//				'pager.css'     => false,
//				'default.css'   => false,
//			),
//		),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			
			// syp added for yii-user extension
            'class' => 'WebUser',
			'loginUrl' => array('/user/login'),
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>false,
            'rules'=>array(
                //''=>'home/index',   // set default controller to home instead of site
                // REST API routing
                array('appapi/test', 'pattern'=>'api/test', 'verb'=>'GET'),
                array('appapi/<apiname>', 'pattern'=>'api/<apiname>', 'verb'=>'POST'),
            ),
// commented the following to force friendly url
//			'rules'=>array(
//				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
//				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
//				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
//			),
		),
		
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=easysecprod',
			'emulatePrepare' => true,
			'username' => 'easysecowner',
			'password' => 'cyan43Elephant',
			'charset' => 'utf8mb4',
			'tablePrefix' => 'tbl_',
		),

        'session' => array(
            'class' => 'system.web.CDbHttpSession',
            'autoCreateSessionTable'=> false,
            'connectionID' => 'db',
            'sessionTableName' => 'yiisession',
            'timeout'=>(21*24*60*60), // 21 days
            'cookieMode' => 'allow',
            'cookieParams' => array(
                'path' => '/',
                // *** Commenting out next line. Bug in php in combo with chrome causes failure
//                'domain' => '.'.APPLICATION_DOMAIN,
                'httpOnly' => true,
            )
        ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                array(
                    'class'=>'CWebLogRoute', // show log messages on web pages
                    'levels'=>'error, warning, info',
                ),
            ),
        ),
		// syp added to include yiibooster/bootstrap
//		'bootstrap' => array(
//			'class' => 'ext.yiibooster20.components.Bootstrap',
//		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'companyName'=>'Information Shield',
        'dbTimezone'=>DB_TIME_ZONE,
        'requestType' => 'Web',
        'dbDeletedPrefix'=>'__DELETED__',
        'appType'=>'web',    // Web or Mobile
        // use PHPMailer to send email with EmailUtils.php EmailUtils.sendMail()
		'adminEmail'=>'support@easyitcompliance.com', // this is used in contact page and login and registration
		'registrationNotificationEmail' => 'test.infoshield@tynylabs.com',
        'sendEmailHost' => 'smtpout.secureserver.net',
        'sendEmailPort' => 465,
		'sendEmailSMTPSecure'=>'ssl', // should be tls for gmail, ssl for godaddy
        'sendEmailName' => 'Compliance Shield',
        'sendEmailId' => 'support@easyitcompliance.com',
        'sendEmailPassword' => 'Gopher#9',
        'sendEmailFromSenderOn' => false, // if false then uses sendEmailId and sendEmailName as sender and then use ReplyTo for sender
    ),
);
