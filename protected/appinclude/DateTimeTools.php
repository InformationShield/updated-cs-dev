<?php
/**
 * DateTimeTools.php - a Yii extension for useful DateTime functions
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * The Software shall be used for Good, not Evil.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * --
 */
class DateTimeTools
{
    const FMT_YMD = 'Y-m-d';
    const FMT_MD = 'M d';
    const FMT_MDY_AMPM = 'M d Y h:i:s A';
    const FMT_YMD_24HR = 'Y-m-d H:i:s';

    const UTC = 'UTC';
    const CENTRAL = 'America/Chicago';

    public static function XTimeFromNowEpoch($xTime=null)
    {
        $newTime = self::NewDateTime('now', 'UTC');
        if ($xTime) {
            $newTime = $newTime->add(DateInterval::createFromDateString($xTime));
        }
        return $newTime->format('U');
    }

    public static function JsonFriendlyDate($dateTimeString)
    {
        $dateTime = $dateTimeString;
        if ($dateTimeString) {
            $dateTime = self::NewDateTime($dateTimeString, self::UTC);
            $dateTime = $dateTime->format(DateTime::ISO8601);
        }
        return $dateTime;
    }

    public static function JsonFriendlify(&$dataToFriendlify, $fieldNames)
    {
        if ($dataToFriendlify) {
            foreach ($dataToFriendlify as $i=>$dataItem) {
                foreach ($fieldNames as $fieldName) {
                    $dataToFriendlify[$i][$fieldName] = self::JsonFriendlyDate($dataToFriendlify[$i][$fieldName]);
                }
            }
        }
//        return $dataToFriendlify;
    }

    public static function NewDateTime($dateTime = 'now', $tz = self::UTC)
    {
        $ntz = new DateTimeZone($tz);
        $dt = new DateTime($dateTime, $ntz);
        return $dt;
    }

    public static function DateTimeFormat($dateTime, $fmt = 'm/d/y h:i:s a')
    {
        $dt = new DateTime($dateTime);
        return $dt->format($fmt);
    }

    public static function SecondsBetween($dt1, $dt2, $abs = false)
    {
        // 0 means exact match
        // +seconds means $dt1 is older than $dt2 by seconds
        // -seconds means $dt1 is younger than $dt2 by seconds
        $interval = $dt1->diff($dt2, $abs);
        $s = ($interval->y * 365 * 24 * 60 * 60) +
            ($interval->m * 30 * 24 * 60 * 60) +
            ($interval->d * 24 * 60 * 60) +
            ($interval->h * 60 * 60) +
            ($interval->i * 60) +
            $interval->s;
        if ($interval->invert == 1) $s = -$s;
        return $s;
    }

    public static function ConvertFormatUtc($utcDateTime, $tz = self::UTC, $fmt = 'M d Y h:i:s a')
    {
        if (empty($utcDateTime)) {
            $ntz = new DateTimeZone($tz);
            $dt = new DateTime('1969-12-31 12:00:00PM', $ntz);
        } else {
            $utc = new DateTimeZone(self::UTC);
            $dt = new DateTime($utcDateTime, $utc);
            $ntz = new DateTimeZone($tz);
            $dt->setTimezone($ntz);
        }
        $t = $dt->format($fmt);
        return $t;
    }

    public static function CstFormatUtc($utcDateTime, $fmt = 'M d Y h:i:s a')
    {
        return self::ConvertFormatUtc($utcDateTime, self::CENTRAL, $fmt);
    }

    public static function Timestamp($yr, $mo, $dy, $h = 0, $m = 0, $s = 0, $tz = self::CENTRAL)
    {
        $utc = new DateTimeZone(self::UTC);
        $d = new DateTime('now', $utc);
        $timezone = new DateTimeZone($tz);
        $d->setTimezone($timezone);
        $d->setDate((int)$yr, (int)$mo, (int)$dy);
        $d->setTime((int)$h, (int)$m, (int)$s);
        $d->setTimezone($utc);
        $tztime = $d->getTimestamp();
        return $tztime;
    }

    public static function MinutesSeconds($seconds)
    {
        $ms = $seconds;
        if ($seconds > 0) {
            $m = floor($seconds / 60);
            $s = $seconds % 60;
            if ($s < 10) {
                $s = '0' . $s;
            }
            $ms = $m . ':' . $s;
        }
        return $ms;
    }

    public static function HourMinutesSeconds($seconds, $labels = false, $leadingZeros = false, $noSeconds = false)
    {
        $hms = $seconds;
        if ($seconds > 0) {
            $m = floor($seconds / 60);
            $s = $seconds % 60;
            if ($s < 10 && !$labels) {
                $s = '0' . $s;
            }
            $h = floor($m / 60);
            $m = $m % 60;
            if ($noSeconds && (int)$s > 0) {
                $m++;
            }
            if ($m < 10 && !$labels) {
                $m = '0' . $m;
            }
            if ($labels) {
                $hours = ($h == 1) ? 'hour' : 'hours';
                $mutes = ($m == 1) ? 'minute' : 'minutes';
                $s = ($s == 1) ? 'second' : 'seconds';
                if ($h > 0) {
                    if ($noSeconds) {
                        if ($m > 0) {
                            $hms = $h . ' ' . $hours . ', ' . $m . ' ' . $mutes;
                        } else {
                            $hms = $h . ' ' . $hours;
                        }
                    } else {
                        if ($m > 0 || $s > 0) {
                            $hms = $h . ' ' . $hours . ', ' . $m . ' ' . $mutes . ', ' . $s . ' ' . $s;
                        } else {
                            $hms = $h . ' ' . $hours;
                        }
                    }
                } else if ($m > 0) {
                    if ($noSeconds) {
                        $hms = $m . ' ' . $mutes;
                    } else {
                        if ($s > 0) {
                            $hms = $m . ' ' . $mutes . ', ' . $s . ' ' . $s;
                        } else {
                            $hms = $m . ' ' . $mutes;
                        }
                    }
                } else {
                    $hms = $s . ' ' . $s;
                }
            } else {
                if ($h > 0) {
                    if ($leadingZeros && $h < 10) {
                        $h = '0' . $h;
                    }
                    $hms = $h . ':' . $m . ':' . $s;
                } else if ($m > 0) {
                    if ($leadingZeros) {
                        $hms = '00:' . $m . ':' . $s;
                    } else {
                        $hms = $m . ':' . $s;
                    }
                } else {
                    if ($leadingZeros) {
                        $hms = '00:00:' . $s;
                    } else {
                        $hms = '0:' . $s;
                    }
                }
            }
        } else {
            if ($labels) {
                if ($noSeconds) {
                    $hms = '0 minutes';
                } else {
                    $hms = '0 seconds';
                }
            } else if ($leadingZeros) {
                $hms = '00:00:00';
            }
        }
        return $hms;
    }

    public static function FormatDateTimeString($strDateTime, $format='m/d/y h:i:s a')
    {
        $d = new DateTime($strDateTime);
        return $d->format($format);
    }
    
    public static function TimeElapsedString($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
