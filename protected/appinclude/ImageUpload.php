<?php

class ImageUpload
{
    public $desired_width=null;
    public $desired_height=null;
    public $cropX=0;
    public $cropY=0;
    public $cropWidth=null;
    public $cropHeight=null;
    public $destinationRoot=null;


    public function saveImage($tmpFileFullPath,$filename,$destinationFolder) {
        $retVal = null;
        if (empty($this->destinationRoot)) $this->destinationRoot = __DIR__.'/../../';
        $destinationFullPath = $this->destinationRoot.$destinationFolder.$filename;
        if ($this->convert_image($tmpFileFullPath, $destinationFullPath, $this->desired_width, $this->desired_height, $this->cropX, $this->cropY, $this->cropWidth, $this->cropHeight)) {
            unlink($tmpFileFullPath);
            $retVal = bu($destinationFolder.$filename);
        }
        return $retVal;
    }

    public function convert_image($src, $dest, $desired_width=null, $desired_height=null, $cropX=0, $cropY=0, $cropWidth=null, $cropHeight=null) {
        $retVal = false;

        /* read the source image */
        set_error_handler(create_function('', "throw new Exception(); return true;"));
        try {
            $exif = null;
            $imageType = exif_imagetype($src);
            switch ($imageType) {
                case 1 :
                    $source_image = imagecreatefromgif($src);
                    break;
                case 2 :
                    $exif = exif_read_data($src);
                    $source_image = imagecreatefromjpeg($src);
                    break;
                case 3 :
                    $source_image = imagecreatefrompng($src);
                    break;
            }
        } catch (Exception $e) { // older android browsers create png even when asked for jpg
            $source_image = imagecreatefrompng($src);
        }
        restore_error_handler();

        if ($source_image) {
            /* set proper rotation */
            if ($exif) {
                $this->image_fix_orientation($source_image, $exif);
            }

            if ($cropWidth) {
                $width = $cropWidth;
                $height = $cropHeight;
            } else {
                $width = imagesx($source_image);
                $height = imagesy($source_image);
            }

            /* find the "desired height or width" of this thumbnail, relative to the desired width  */
            if ($desired_width) {
                $final_width = $desired_width;
                $final_height = floor($height * ($final_width / $width));
            } else if ($desired_height) {
                $final_height = $desired_height;
                $final_width = floor($width * ($final_height / $height));
            } else {
                $final_width = $width;
                $final_height = $height;
            }

            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($final_width, $final_height);

            /* retain transparency */
            imagealphablending( $virtual_image, false );
            imagesavealpha( $virtual_image, true );

            /* copy source image at a resized size */
            imagecopyresampled($virtual_image, $source_image, 0, 0, $cropX, $cropY,
                $final_width, $final_height, $width, $height);

            /* create the physical thumbnail image to its destination */
            if (strripos($dest,".png") !== false) {
                imagepng($virtual_image, $dest, 0);
            } else {
                imagejpeg($virtual_image, $dest, 100);
            }
            $retVal = true;
        }
        return $retVal;
    }

    private function image_fix_orientation(&$image, $exif) {
        if (!empty($exif['Orientation'])) {
            switch ($exif['Orientation']) {
                case 3:
                    $image = imagerotate($image, 180, 0);
                    break;

                case 6:
                    $image = imagerotate($image, -90, 0);
                    break;

                case 8:
                    $image = imagerotate($image, 90, 0);
                    break;
            }
        }
    }
}