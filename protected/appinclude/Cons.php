<?php
// for convenient access to constants in app
class Cons
{
	public static $ActiveDisabled = array(
		1=>'active',
		0=>'disabled',
	);

    // status
    const DELETED = 0;
    const ACTIVE = 1;
    const PENDING = 2;
    const ARCHIVED = 3;

    // visibility add VIS_ since public and private are php keywords
    const VIS_HIDDEN = 0;
    const VIS_PUBLIC = 1;
    const VIS_PRIVATE = 2;

    // sort options
    const SORT_DESC = -1;
    const SORT_NONE = 0;
    const SORT_ASC = 1;

    // policy statuses
    const POLICY_STATUS_DRAFT = 1;
    const POLICY_STATUS_REVIEW = 2;
    const POLICY_STATUS_PUBLISHED = 3;
    const POLICY_STATUS_ARCHIVED = 4;
    
    // task status
    const TASK_NOTACTIVE = 0;
    const TASK_PLANNED = 1;
    const TASK_INPROGRESS = 2;
    const TASK_COMPLETE = 3;
    
    // control status
    const CONTROL_NOT_ACTIVE = 0;
    const CONTROL_PLANNED = 1;
    const CONTROL_IN_PROGRESS = 2;
    const CONTROL_VERIFIED = 3;
    const CONTROL_VALIDATED = 4;
    const CONTROL_COMPLETE = 5;
    
    // user status
    const USER_STATUS_NOT_ACTIVE = 0;
    const USER_STATUS_ACTIVE = 1;
    const USER_STATUS_BANNED = -1;
    const USER_STATUS_DELETED = -2;

    // error messages
	const MSG_CUSTOM = 0;
    const MSG_ERROR = 1;
	const MSG_COMPANY_MAX = 2;
	const MSG_COMPANY_MISSING = 3;
	const MSG_ACCESS_DENIED = 4;
	const MSG_LICENSE_DENIED = 5;
	const MSG_ROLE_DENIED = 6;

	// license types
	const LIC_NONE = 0;
	const LIC_FREE = 1;
	const LIC_TRIAL = 10;
	const LIC_LEVEL1 = 100;
	const LIC_LEVEL2 = 200;
	const LIC_LEVEL3 = 300;
	const LIC_ALL = 1000;

	// company roles
	const ROLE_NONE = 0;
	const ROLE_USER = 100;
	const ROLE_AUDITOR = 200;
	const ROLE_AUTHOR = 300;
	const ROLE_ADMIN = 400;
	const ROLE_OWNER = 500;
	const ROLE_SUPER = 1000;
}
?>
