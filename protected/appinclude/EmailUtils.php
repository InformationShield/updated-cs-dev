<?php
require_once(__DIR__.'/Helper.php');
require_once(__DIR__.'/../vendor/PHPMailer/class.phpmailer.php');
require_once(__DIR__.'/../vendor/PHPMailer/class.smtp.php');

class EmailUtils
{
    public static function sendMail($email, $subject, $txtBody, $htmlBody = "", $fromEmail = "", $fromName = "",$cc=false,$bcc=false)
    {
        $sendEmailHost = param('sendEmailHost');
        if (!empty($sendEmailHost)) {
            $retVal = false;
            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->CharSet = "UTF-8";
            $mail->SMTPSecure = param('sendEmailSMTPSecure'); // should be tls for gmail, ssl for godaddy 'tls';
            $mail->SMTPAuth = true;
            $mail->Host = $sendEmailHost;
            $mail->Port = param('sendEmailPort');
            $mail->Username = param('sendEmailId');
            $mail->Password = param('sendEmailPassword');
            if (!empty($cc)) {
                if (is_array($cc) && count($cc) > 0) {
                    if (count($cc) > 1) {
                        $mail->addCC($cc[0], $cc[1]);
                    } else {
                        $mail->addCC($cc[0]);
                    }
                } else {
                    $mail->addCC($cc);
                }
            }
            if (!empty($bcc)) {
                if (is_array($bcc) && count($bcc) > 0) {
                    if (count($bcc) > 1) {
                        $mail->addBCC($bcc[0], $bcc[1]);
                    } else {
                        $mail->addBCC($bcc[0]);
                    }
                } else {
                    $mail->addBCC($bcc);
                }
            }
            if (empty($fromEmail)) {
                $fromEmail = param('adminEmail');
            }
            if (param('sendEmailFromSenderOn')) {
                $mail->setFrom($fromEmail, $fromName);
            } else {
                $mail->SetFrom(param('sendEmailId'), param('sendEmailName')); // use this to send as static user set in config
                $mail->AddReplyTo($fromEmail, $fromName);
            }
            $mail->Subject = $subject;
            $mail->AltBody = $txtBody;
            if (empty($htmlBody)) {
                $htmlBody = $txtBody;
            }
            $mail->isHTML(true);
            $mail->msgHTML($htmlBody);
            $mail->addAddress($email);
            if ($mail->send()) {
                $retVal = true;
                //Yii::log(print_r($mail,true),'error','EmailUtils.sendMail');
            } else {
                Yii::log("Mailer Error: " . $mail->ErrorInfo, 'error', 'EmailUtils.sendMail');
                Yii::log("Attempted to Send Message From:\n" . $fromName . " <" . $fromEmail . ">\nTo: " . $email . "\nBody:\n" . $htmlBody . "\n", 'error', 'EmailUtils.sendMail');
                Yii::log("Mail Settings:\n" . print_r($mail, true) . "\n", 'error', 'EmailUtils.sendMail');
            }
        } else {
            $nameEncode = '=?UTF-8?B?' . base64_encode($fromName) . '?=';
            $subjectEncode = '=?UTF-8?B?' . base64_encode($subject) . '?=';
            $headers = "From: ".$nameEncode." <".$fromEmail."}>\r\n" .
                "Reply-To: ".$fromEmail."\r\n" .
                "MIME-Version: 1.0\r\n" .
                "Content-Type: text/plain; charset=UTF-8";
            $retVal = mail($email, $subjectEncode, $txtBody, $headers);
        }
        return $retVal;
    }
}
