<?php
/**
 * This is the shortcut to DIRECTORY_SEPARATOR
 */
defined('DS') or define('DS',DIRECTORY_SEPARATOR);

/**
 * This is the shortcut to Yii::app()
 */
function app()
{
    return Yii::app();
}

/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs()
{
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}

/**
 * This is the shortcut to Yii::app()->user.
 */
function user()
{
    return Yii::app()->getUser();
}

/**
 * This is the shortcut to Yii::app()->getModule('user').
 */
function yuser()
{
	return Yii::app()->getModule('user');
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route,$params=array(),$ampersand='&')
{
    if (isset($_SERVER['SERVER_NAME'])) {
        $url = Yii::app()->request->hostInfo.Yii::app()->createUrl($route, $params, $ampersand);
        $url = str_ireplace('/index.php/', '/', $url); //FIXME TODO
    } else {
        foreach ($params as $key=>$value) {
            $route .= '/'.urlencode($key).'/'.urlencode($value);
        }
        $url = APP_URL.$route; // CConsoleApplication does not have a createUrl()
    }
    return $url;    
}

function absoluteUrl($route, $params=null) {
	$absUrl = Yii::app()->createAbsoluteUrl($route);
	if ($params) {
		$absUrl = Yii::app()->createAbsoluteUrl($route, $params);
	}
	return $absUrl;
}

function urlTheme($url=null)
{
    $themeUrl = Yii::app()->theme->baseUrl;
    return $url===null ? $themeUrl : $themeUrl.'/'.ltrim($url,'/');
}

/**
 * This is the shortcut to CHtml::link()
 */
function lnk($text, $url = '#', $htmlOptions = array())
{
    return CHtml::link($text, $url, $htmlOptions);
}

/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($message, $category = 'stay', $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

/**
 * This is the shortcut to UserModule::t() with default dictionary = 'user'
 */
function ut($str='',$params=array(),$dic='user')
{
    return UserModule::t($str, $params, $dic);
}

// Retrieve specified $_GET variable if exists or return default value
function getGet($varname, $default=null)
{
	$retVal = $default;
	if (isset($_GET[$varname])) {
		$retVal = $_GET[$varname];
	}
	return $retVal;
}

// Retrieve specified $_POST variable if exists or return default value
function getPost($varname, $default=null)
{
	$retVal = $default;
	if (isset($_POST[$varname])) {
		$retVal = $_POST[$varname];
	}
	return $retVal;
}

/**
 * Retrieves a session variable
 * sess($varName) retrieves session variable or null if not exists
 * sess($varName, varValue) retrieves session variable or $defaultValue if not exists
 */
function getSessionVar($varName, $defaultValue=null)
{
	//global $gSessionVars;

	$retVal = $defaultValue;
//	if (Yii::app()->params['requestType'] == 'Web') {
		if (isset(user()->$varName)) {
			$retVal = user()->$varName;
		}
//	} else {
//		if (isset($gSessionVars[$varName])) {
//			$retVal = $gSessionVars[$varName];
//		}
//	}
	return $retVal;
}

/**
 * sets or deletes a session variable
 * sess($varName, NULL) deletes session variable
 */
function setSessionVar($varName, $varValue=null)
{
	//global $gSessionVars;
	
//	if (Yii::app()->params['requestType'] == 'Web') {
		user()->setState($varName, $varValue);
//	} else {
//		$gSessionVars[$varName] = $varValue;
//	}
}

function SetACookie($cookieName, $cookieValue, $expiryHrs=2160) // 2160 hours = 90 days
{
    $cookie=new CHttpCookie($cookieName,$cookieValue);
    $cookie->expire = time()+$expiryHrs*3600;
    app()->request->cookies[$cookieName]=$cookie;
}

function GetACookie($cookieName)
{
    $cookie=app()->request->cookies[$cookieName];
    if (isset($cookie)) {
        $value=$cookie->value;
    } else {
        $value = null;
    }
    return $value;
}

function DeleteACookie($cookieName)
{
    $cookie=app()->request->cookies[$cookieName];
    if (isset($cookie)) {
        $cookie->expire = time()-3600; //Set cookie to expire
        app()->request->cookies[$cookieName]=$cookie;
        unset(app()->request->cookies[$cookieName]);
    }
}

/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url=null)
{
    static $baseUrl;
    if ($baseUrl===null)
        $baseUrl=Yii::app()->getRequest()->getBaseUrl();
    return $url===null ? $baseUrl : $baseUrl.'/'.ltrim($url,'/');
}

/*
 *  This returns the client side path to the images directory or to full image if path given.
 */
function images($url=null)
{
    static $imageUrl;
    if ($imageUrl===null)
        $imageUrl=bu('images');
    return ($url===null) ? $imageUrl : $imageUrl.'/'.ltrim($url,'/');
}

/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name, $newValue=NULL)
{
    if (!is_null($newValue)) {
        Yii::app()->params[$name] = $newValue;
    }
    return Yii::app()->params[$name];
}

function userId()
{
    static $userId;
    if ($userId === null)
        $userId = getSessionVar('userId');
    return $userId;
}

function userLogin()
{
    static $userLogin;
    if ($userLogin === null)
        $userLogin = getSessionVar('userLogin');
    return $userLogin;
}

function userEmail()
{
    static $userEmail;
    if ($userEmail === null)
        $userEmail = getSessionVar('email');
    return $userEmail;
}

function setFlashMessage($message, $key='general')
{
	Yii::app()->getUser()->setFlash($key, $message);
}

function sqlTz($field, $convertToDate=false)
{
    if (getSessionVar('clientMode') == 'api')  // mobile api call.  let mobile app convert the date per user timezone.
        $retVal = $field;
    else if ($convertToDate)
        $retVal = "CAST(CONVERT_TZ(".$field.",'".param('dbTimezone')."','US/Central') AS DATE)";
    else
        $retVal = "CONVERT_TZ(".$field.",'".param('dbTimezone')."','US/Central')";
    return $retVal;
}

function getArrayItem($array, $i, $attribute=null)
{
    $retVal = 'zzz';//null;
    $item = array_slice($array, $i, 1);
    if ($item) {
        $item = $item[0];
        if ($attribute) {
            if (is_array($item)) {
                $retVal = $item[$attribute];
            } else if (is_object($item)) {
                $retVal = $item->{$attribute};
            }
        } else {
            $retVal = $item;
        }
    }
    return $retVal;
}

/**
 * devmode returns the value of the global app param devmode
 */
function devmode()
{
    return param('devmode');
}

function blank2null($string)
{
    $retVal = null;
    if (strlen(trim($string)) != 0) {
        $retVal = $string;
    }
    return $retVal;
}

// for simple debugging
function vardump($data, $endApp=true)
{
    var_dump($data);
    if ($endApp) {
        Yii::app()->end(0,true);
    }
}

function yiilog($msg, $title='adhocLog', $errorLevel='error')
{
    if (is_object($msg) || is_array($msg)) {
        $msg = serialize($msg);
    }
    Yii::log($title, $errorLevel, $msg);
}

/**
 *  This works to redirect to another page even if this is done as a partial render.
 *  It would not work if the client has javascript disabled, but then nothing on our site would.
 */
function jsredirect($url){
    echo '<script type="text/javascript">';
    echo 'window.location.href="'.$url.'";';
    echo '</script>';
    Yii::app()->end(); // call this instead of exit; so messages are logged
}

/**
 * This function is for handling the case of bad url params or missing params, by either
 * a bug in the code or a user hacking the url directly in the browser.
 */
function badurl($html='')
{
    Yii::log("BAD URL REQUEST: ".$html,'info','Helper.badurl');
    $url = url(Yii::app()->errorHandler->errorAction); // url('site/error');
//    $url = str_ireplace('https:', 'http:', $url); // make sure not secure
    jsredirect($url);
}

function registerScriptFile($file, $position = null)
{
	// ignore files that are minified or part of highcharts 3rd party code
	if ((stripos($file,".min.") !== false) || 
		(stripos($file,"highcharts/") !== false)) {
			Yii::app()->clientScript->registerScriptFile(bu($file),$position);   
			return;
    }
    $useMinFile = (APP_DEVELOPMENT_SITE) ? false : true; // return non min js code in dev env
    Miniumizer::registerJS($file,$position,$useMinFile);
}

function registerCssFile($file, $position = null)
{
    $useMinFile = (APP_DEVELOPMENT_SITE) ? false : true; // return non min js code in dev env
    Miniumizer::registerCSS($file,$position,$useMinFile);
}

function sendResponse($body = '', $status = Rest::REST_OK, $content_type = 'application/json')
{
    if ($content_type == 'application/json') {
        $body = json_encode($body);
    }
    Rest::sendResponse($status, $body, $content_type);
}

// redirect using Yii path, ex: 'live/mobile'
function redirect($redirectPath)
{
	app()->request->redirect(app()->createUrl($redirectPath));
}

function obscure($id) {
	return Obscure::encode($id);
}

/***************************** Functions below are specific to this project ****************************/
//function userRole()
//{
//    static $userRole;
//    if ($userRole === null)
//        $userRole = getSessionVar('userRole');
//    return $userRole;
//}

function css($url=null)
{
    static $cssUrl;
    if ($cssUrl===null)
        $cssUrl=bu('css');
    return ($url===null) ? $cssUrl : $cssUrl.'/'.ltrim($url,'/');
}

function js($url=null)
{
    static $jsUrl;
    if ($jsUrl===null)
        $jsUrl=bu('js');
    return ($url===null) ? $jsUrl : $jsUrl.'/'.ltrim($url,'/');
}

function thirdparty($url=null)
{
    static $thirdpartyUrl;
    if ($thirdpartyUrl===null)
        $thirdpartyUrl=bu('3rdparty');
    return ($url===null) ? $thirdpartyUrl : $thirdpartyUrl.'/'.ltrim($url,'/');
}

function old($url=null)
{
    static $imageUrl;
    if ($imageUrl===null)
        $imageUrl=bu('old');
    return ($url===null) ? $imageUrl : $imageUrl.'/'.ltrim($url,'/');
}

function bin2Yes($value)
{
    if ($value) {
        $retVal = 'Yes';
    } else {
        $retVal = '';
    }
    return $retVal;
}

function bin2YesNo($value)
{
    if ($value) {
        $retVal = 'Yes';
    } else {
        $retVal = 'No';
    }
    return $retVal;
}

function yesNo2Bin($value)
{
    if (strtolower($value) == 'yes' || strtolower($value) == 'ye' || strtolower($value) == 'y') {
        $retVal = 1;
    } else if (strtolower($value) == 'no' || strtolower($value) == 'n') {
        $retVal = 0;
    } else {
        $retVal = $value;
    }
    return $retVal;
}

function varSet($varToCheck)
{
    $retVal = null;
    if (isset($varToCheck)) {
        $retVal = $varToCheck;
    }
    return $retVal;
}
