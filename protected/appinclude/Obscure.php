<?php

/**
 *  This stuff has been tested well.
 */
class Obscure
{
    // Translated any positive number up to 9,007,199,254,740,992 to a string using these $characters
    // $characters = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    // $numCharacters  = strlen($index);
    //
    // This is the encrypted $characters that does not need to be recomputed on every call
    // and should remain for every unchanged once created for this particular app
    // It was created using the method below CreateIndexSecuredByPasswordAndTest($passKey)
    private static $characters = 'iEARLmsDHT9NQUu0CSXZ8Yrxz16IMhk2PWvFVcfjnBeqJKap5lt7OoGbdy4gw3';
    private static $numCharacters = 62;

    // how long to pad out the string should be 11 or more
    const padOut = 12;
    // ascii markers
    const lenMark = 64; // value between 64-81 or 96-113
    const endMark = 113; // value between 96-113 or 64-81
    const padMark = 32; // value between 0-($numCharacters-padOut)
    const MAXNUM = 9007199254740992;

    // Can obscure numbers > 0 && < MAXNUM to a cryptic and secure hashed string
    // numbers <= 0 are just returned, null for numbers > MAXNUM
    // return false if can not encode
    public static function encode($inVal)
    {
        $out = false;
        if ((int)$inVal > 0) {
            if ((int)$inVal <= self::MAXNUM) {
                // Digital number  -->>  alphabet letter code
                $in = (int)$inVal; //if string passed in
                $out = "";
                for ($t = floor(log($in, self::$numCharacters)); $t >= 0; $t--) {
                    $bcp = bcpow(self::$numCharacters, $t);
                    $a = floor($in / $bcp) % self::$numCharacters;
                    $out = $out . substr(self::$characters, $a, 1);
                    $in = $in - ($a * $bcp);
                }
                $out = strrev($out); // reverse
                self::addPadding($out);
            }
            else { //max number supported
                Yii::log("This number {$inVal} is greater than max number of ".self::MAXNUM." supported.\n", 'error', 'Obscure::encode');
            }
        } else if ((int)$inVal <= 0) {
            $out = ''.$inVal;
        }
        return $out;
    }

    // returns false if it can not decode
    public static function decode($inVal)
    {
        $out = false;
        $code = self::isObscure($inVal);
        if ($code !== false) {
            // Digital number  <<--  alphabet letter code
            $in = strrev($code);
            $len = strlen($in) - 1;
            $out = 0;
            for ($t = 0; $t <= $len; $t++) {
                $bcpow = bcpow(self::$numCharacters, $len - $t);
                $out = $out + strpos(self::$characters, substr($in, $t, 1)) * $bcpow;
            }
            $out = sprintf('%F', $out);
            $out = (int)substr($out, 0, strpos($out, '.'));
        }
        else if (isset($inVal) && strlen($inVal) > 0 && ($inVal[0] === '-' || $inVal === '0')) {
            // if starts with a - or is 0 assume it was a number <= 0 and return it
            $out = (int)$inVal;
        }
        else {
            Yii::log("This value {$inVal} could not be decoded.\n", 'error', 'Obscure::decode');
        }
        return $out;
    }

    // test if it is a coded obscure string and if so returns unwrapped portion to decode, else false
    public static function isObscure(&$inVal) {
        $retVal = false;
        if (strlen($inVal) === self::padOut) {
            // first char tell true length of coded number rest is padding
            $len = ord($inVal[0]) - self::lenMark;
            if ($len > 0 && ($len + 1) < self::padOut) {
                if ((ord($inVal[$len + 1]) - self::endMark) === $len) {
                    $retVal = substr($inVal, 1, $len);
                }
            }
        }
        return $retVal;
    }

    private static function addPadding(&$out) {
        // first char tell true length
        $len = strlen($out);
        $out = chr(self::lenMark+$len).$out.chr(self::endMark+$len);
        $len += 2;
        for ($i = $len; $i < self::padOut; $i++) {
            $out .= self::$characters[self::padMark+$i];
        }
    }

/***************************************************************************************************************/
    // FOR INITIAL SETUP OF APP RUN ONCE FROM site/index Controller then
    // SEE APPLICATION.LOG FOR SETTINGS TO USE FOR THE APPS GENERALENCRYPTIONKEY
    // ONLY NEEDED TO EXECUTE ONCE FOR SETTING self::$characters and self::$numCharacters for encode() above
    // it also does some 2 minutes of testing to make sure new $characters string works solidly
    /*
    public static function CreateIndexSecuredByPasswordAndTest($passKey=null)
    {
        Utils::PageRenderTime();
        $index_orig = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $valid = "The \$characters string has been validated to contain all characters needed only once.";

        // if pass key then we mumble characters up for obscurity not security
        if (!empty($passKey)) {
            self::$characters = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            self::$numCharacters = strlen(self::$characters);
            // Although this function's purpose is to just make the
            // ID short - and not so much secure,
            // with this patch by Simon Franz (http://blog.snaky.org/)
            // you can optionally supply a password to make it harder
            // to calculate the corresponding numeric ID
            for ($n = 0; $n < strlen(self::$characters); $n++) {
                $i[] = substr(self::$characters, $n, 1);
            }

            $passhash = hash('sha256', $passKey);
            $passhash = (strlen($passhash) < strlen(self::$characters))
                ? hash('sha512', $passKey)
                : $passhash;

            for ($n = 0; $n < strlen(self::$characters); $n++) {
                $p[] = substr($passhash, $n, 1);
            }

            array_multisort($p, SORT_DESC, $i);
            self::$characters = implode($i);
        }
        self::$numCharacters = strlen(self::$characters);

        // validate that the mumble string does contain every character once
        // doing this to validate calculations with $passKey
        for ($i = 0; $i < self::$numCharacters; $i++) {
            if (strpos(self::$characters, $index_orig[$i]) === false) {
                $valid = "ERROR!!! WARNING!!! The \$characters string is missing a character " . $index_orig[$i];
                break;
            }
        }

        Yii::log($valid . "\n\n//use these values in the class.\nprivate static \$characters = '" . self::$characters . "';\nprivate static \$numCharacters  = " . self::$numCharacters . ";\n\n\n",
            'error', 'Obscure::CreateIndexSecuredByPasswordAndTest');
        self::test();
        return self::$characters;
    }

    public static function test($numberOfTestsToRun = 200000)
    {
        // 1000000 == 1M == 3min runtime
        $numberOfTestsToRun /= 2; // half low and half high

        Utils::PageRenderTime();
        $tested = true;
        $cnt = 0;
        $out = "";
        // low number tests
        for($i=-10; $i <= ($numberOfTestsToRun - 11); $i++) {
            $cnt++;
            $out = self::encode($i);
            if ($i !== self::decode($out)) {
                $tested = false;
                break;
            }
            if ($i == 1) {
                Yii::log("\n\nMin number test {$i} === {$out}\n\n",'error','Obscure::CreateIndexSecuredByPasswordAndTest');
            }
        }
        if ($tested) {
            // high number tests
            for ($i = self::MAXNUM; $i >= (self::MAXNUM - $numberOfTestsToRun + 1); $i--) {
                $cnt++;
                $out = self::encode($i);
                if ($i !== self::decode($out)) {
                    $tested = false;
                    break;
                }
                if ($i == self::MAXNUM) {
                    Yii::log("\n\nMax number test {$i} === {$out}\n\n",'error','Obscure::CreateIndexSecuredByPasswordAndTest');
                }
            }
        }

        if ($tested) {
            Yii::log("\n\nTested {$cnt} times (in ".Utils::PageRenderTime(true)." milliseconds) encode/decode all good!\n\n",'error','Obscure::CreateIndexSecuredByPasswordAndTest');
        } else {
            Yii::log("\n\nERROR!!! encode testing failed on {$i} == {$out} (in ".Utils::PageRenderTime(true)." milliseconds) encode/decode\n\n",'error','Obscure::CreateIndexSecuredByPasswordAndTest');
        }

        // bad data test
        $i = null;
        $out = self::encode($i);
        Yii::log("\n\nBad data test null {$out} ".(($i !== self::decode($out)) ? "PASS" : "FAILED!")."\n",'error','Obscure::CreateIndexSecuredByPasswordAndTest');
        $i = "";
        $out = self::encode($i);
        Yii::log("\n\nBad data test '' {$out} ".(($i !== self::decode($out)) ? "PASS" : "FAILED!")."\n",'error','Obscure::CreateIndexSecuredByPasswordAndTest');
        $i = "garbage";
        $out = self::encode($i);
        Yii::log("\n\nBad data test {$i} {$out} ".(($i !== self::decode($out)) ? "PASS" : "FAILED!")."\n",'error','Obscure::CreateIndexSecuredByPasswordAndTest');
        $i = "abcdfegherys";
        $out = self::encode($i);
        Yii::log("\n\nBad data test {$i} {$out} ".(($i !== self::decode($out)) ? "PASS" : "FAILED!")."\n",'error','Obscure::CreateIndexSecuredByPasswordAndTest');
        $i = self::MAXNUM + 1;
        $out = self::encode($i);
        Yii::log("\n\nBad data test {$i} {$out} ".(($i !== self::decode($out)) ? "PASS" : "FAILED!")."\n",'error','Obscure::CreateIndexSecuredByPasswordAndTest');
    }
    */
    /***************************************************************************************************************/
}