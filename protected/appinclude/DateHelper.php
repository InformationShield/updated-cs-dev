<?php

class DateHelper
{
    const DEFAULT_TIME_ZONE = 'America/Chicago';

    const PACIFIC = 'America/Los_Angeles';
    const CENTRAL = 'America/Chicago';
    const UTC = 'UTC';
    const FORMAT_DATE_TIME_AMPM = 'M d Y h:i:s A';
    const FORMAT_DATE_24TIME = 'Y-m-d H:i:s';
    const FORMAT_DATEONLY = 'Y-m-d';
    const FORMAT_MONTHDAY = 'M d';

    public static function DateTime($time='now',$tz='UTC')
    {
        $dtz = new DateTimeZone($tz);
        $d = new DateTime($time,$dtz); 
        return $d;
    }
    
    // compare dates and return difference in seconds
    // 0 means exact match
    // +diffsecs means $date1 is older than $date2 by diffsecs
    // -diffsecs means $date1 is younger than $date2 by diffsecs
    public static function DiffSeconds($date1,$date2,$absolute=false)
    {
        $interval = $date1->diff($date2,$absolute);
        $diffsecs= ($interval->y * 365 * 24 * 60 * 60) +
                   ($interval->m * 30 * 24 * 60 * 60) +
                   ($interval->d * 24 * 60 * 60) +
                   ($interval->h * 60 *60) +
                   ($interval->i * 60) +
                   $interval->s;
        if ($interval->invert == 1) $diffsecs = -$diffsecs;
        return $diffsecs;
    }

    public static function DiffDays($dateFrom, $dateTo)
    {
        $dStart = new DateTime($dateFrom);
        $dEnd  = new DateTime($dateTo);
        $dDiff = $dStart->diff($dEnd);
        return $dDiff->days;
    }

    public static function UtcToTz($utcTimeStr,
            $timeZoneStr='UTC',
            $format='M d Y h:i:s a',
            $ifNoTime='1969-12-31 12:00:00PM')
    {
        if (empty($utcTimeStr) || $utcTimeStr == '0000-00-00 00:00:00')
        { 
            $tz = new DateTimeZone($timeZoneStr);
            $d = new DateTime($ifNoTime,$tz);
        } 
        else
        {
            $utc = new DateTimeZone('UTC');
            $d = new DateTime($utcTimeStr,$utc);        
            $tz = new DateTimeZone($timeZoneStr);
            $d->setTimezone($tz);
        }
        $t = $d->format($format);
        return $t;
    }

	public static function CstToUtc($cstTimeStr, $format='M-d-Y h:i:s a')
	{
		$cst = new DateTimeZone('America/Chicago');
		$utc = new DateTimeZone('UTC');
		$d = new DateTime($cstTimeStr, $cst);
		$d->setTimezone($utc);
        $t = $d->format($format);
        return $t;
	}

	public static function UtcToCst($utcTimeStr,$format='M d Y h:i:s a')
    {
        return DateHelper::UtcToTz($utcTimeStr,DateHelper::CENTRAL,$format);
    }
    
    public static function UtcToPst($utcTimeStr,$format='M d Y h:i:s a')
    {
        return DateHelper::UtcToTz($utcTimeStr,DateHelper::PACIFIC,$format);
    }
    
    public static function TzTime($year, $month, $day, $hr=0, $min=0, $sec=0, $tz=DateHelper::PACIFIC)
    {
        $utc = new DateTimeZone('UTC');
        $d = new DateTime('now',$utc);
        $timezone = new DateTimeZone($tz);
        $d->setTimezone($timezone);
        $d->setDate((int)$year, (int)$month, (int)$day);
        $d->setTime((int)$hr, (int)$min, (int)$sec);
        $d->setTimezone($utc);
        $tztime = $d->getTimestamp();
        return $tztime;
    }

    public static function LastUtcMidnight()
    {
        $timezone = new DateTimeZone(DateHelper::UTC);
        $lastMidnight = new DateTime('now',$timezone);
        $lastMidnight->setTime(0, 0, 0);
        return $lastMidnight;
    }

    public static function SecondsSinceLastUtcMidnight()
    {
        $timezone = new DateTimeZone(DateHelper::UTC);
        $now = new DateTime('now',$timezone);
        $lastMidnight = DateHelper::LastUtcMidnight();
        $diffsecs = $now->getTimestamp() - $lastMidnight->getTimestamp();
        return $diffsecs;
    }

    // Provide full weekday name if want to limit to a weekday (ex: Sunday, Monday, Tuesday ...)
    public static function NowIsWithinTime($startTime='00:00:00', $endTime='00:00:00', $weekDay=null, $timeZone=DateHelper::CENTRAL)
    {
        $now = DateHelper::DateTime('now', $timeZone);
        $nowString = $now->format('Y-m-d H:i:s');
        $today = $now->format('l');
        $dateonly = $now->format('Y-m-d');

        $nowInt = strtotime($nowString);
        $startInt = strtotime($dateonly.' '.$startTime);
        $endInt = strtotime($dateonly.' '.$endTime);

        $isWithinTime = false;
        if ($nowInt >= $startInt && $nowInt <= $endInt) {
            if (is_null($weekDay)) {
                $isWithinTime = true;
            } else if ($weekDay == $today) {
                $isWithinTime = true;
            }
        }
        return $isWithinTime;
    }

    public static function IsCurrentWeek($date)
    {
        $retVal = false;

        $monday = strtotime('monday this week');
        $sunday = strtotime("sunday this week");
        $checkDate = strtotime($date);
        if ($checkDate >= $monday && $checkDate <= $sunday) {
            $retVal = true;
        }

        return $retVal;
    }

    // Converts seconds to min:sec format
    public static function SecondsToMinSec($totSeconds)
    {
        $minSec = $totSeconds;
        if ($totSeconds > 0) {
            $min = floor ($totSeconds / 60);
            $sec = $totSeconds % 60;
            if ($sec < 10) {
                $sec = '0'.$sec;
            }
            $minSec = $min.':'.$sec;
        }
        return $minSec;
    }

    // Converts seconds to hr:min:sec format or a long format 3 hours, 4 minutes, 15 seconds
    public static function SecondsToHourMinSec($totSeconds, $longFormat=false, $roundToMinutes=false, $showDetails=false)
    {
        $hrMinSec = $totSeconds;
        if ($totSeconds > 0)
        {
            $min = floor ($totSeconds / 60);
            $sec = $totSeconds % 60;
            if ($sec < 10 && !$longFormat) {
                $sec = '0'.$sec;
            }
            $hr = floor ($min / 60);
            $min = $min % 60;
            if ($roundToMinutes && (int)$sec > 0) {
                $min++;
            }
            if ($min < 10 && !$longFormat) {
                $min = '0'.$min;
            }
            if ($longFormat)
            {
                $hours = ($hr == 1) ? 'hour' : 'hours';
                $minutes = ($min == 1) ? 'minute' : 'minutes';
                $seconds = ($sec == 1) ? 'second' : 'seconds';
                if ($hr > 0) {
                    if ($roundToMinutes) {
                        if ($min > 0) {
                            $hrMinSec = $hr.' '.$hours.', '.$min.' '.$minutes;
                        } else {
                            $hrMinSec = $hr.' '.$hours;
                        }
                    } else {
                        if ($min > 0 || $sec > 0) {
                            $hrMinSec = $hr.' '.$hours.', '.$min.' '.$minutes.', '.$sec.' '.$seconds;
                        } else {
                            $hrMinSec = $hr.' '.$hours;
                        }
                    }
                } else if ($min > 0) {
                    if ($roundToMinutes) {
                        $hrMinSec = $min.' '.$minutes;
                    } else {
                        if ($sec > 0) {
                            $hrMinSec = $min.' '.$minutes.', '.$sec.' '.$seconds;
                        } else {
                            $hrMinSec = $min.' '.$minutes;
                        }
                    }
                } else {
                    $hrMinSec = $sec.' '.$seconds;
                }
            }
            else
            {
                if ($hr > 0) {
                    if ($showDetails && $hr < 10) {
                        $hr = '0'.$hr;
                    }
                    $hrMinSec = $hr.':'.$min.':'.$sec;
                } else if ($min > 0) {
                    if ($showDetails) {
                        $hrMinSec = '00:'.$min.':'.$sec;
                    } else {
                        $hrMinSec = $min.':'.$sec;
                    }
                } else {
                    if ($showDetails) {
                        $hrMinSec = '00:00:'.$sec;
                    } else {
                        $hrMinSec = '0:'.$sec;
                    }
                }
            }
        } 
        else
        {
            if ($longFormat)
            {
                if ($roundToMinutes) {
                    $hrMinSec = '0 minutes';
                } else {
                    $hrMinSec = '0 seconds';
                }
            } else if ($showDetails) {
                $hrMinSec = '00:00:00';
            }
        }
        return $hrMinSec;
    }

    public static function FormatDateTimeString($strDateTime, $format='m/d/y h:i:s a')
    {
        $retVal = null;
        $newStrDate = null;
        if (strtotime($strDateTime)) {  // check for valid date
            $dtz = new DateTimeZone(DateHelper::DEFAULT_TIME_ZONE); // used when 'now' is passed in as strDateTime
            $d = new DateTime($strDateTime, $dtz);
            $newStrDate = $d->format($format);
        }
        if (strtotime($newStrDate)) {  // check after conversion since non realistic date like 00/00/0000 slips through on first check above
            $retVal = $newStrDate;
        }
        return $retVal;
    }

    // add or subtract days.  Ex: DateAdd('yyyy-mm-dd', '-3') subtracts 3 days
    public static function AddDays($strDateTime, $days)
    {
        $date = new DateTime($strDateTime);
        $date->modify($days.' days');
        return $date->format('Y-m-d');
    }

    // add or subtract hours.  Ex: DateHours('yyyy-mm-dd', '-3') subtracts 3 hours
    public static function AddHours($strDateTime, $hours)
    {
        $date = new DateTime($strDateTime);
        $date->modify($hours.' hours');
        return $date->format('Y-m-d');
    }

    public static function isValidDate($date)
    {
        $retVal = false;
        $date = date_parse($date);
        if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"]))
            $retVal = true;
        return $retVal;
    }

}

?>
