<?php
/**
 * Rest.php - Process and send responses for REST requests
 * 
 * @link http://www.yiiframework.com/wiki/175/how-to-create-a-rest-api/
 * @link http://www.gen-x-design.com/archives/create-a-rest-api-with-php/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * The Software shall be used for Good, not Evil.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * --
 */
class Rest
{
	// [Informational 1xx]
	const REST_CONTINUE = 100;
	const REST_SWITCHING_PROTOCOLS = 101;

	// [Successful 2xx]
	const REST_OK = 200;
	const REST_CREATED = 201;
	const REST_ACCEPTED = 202;
	const REST_NONAUTHORITATIVE_INFORMATION = 203;
	const REST_NO_CONTENT = 204;
	const REST_RESET_CONTENT = 205;
	const REST_PARTIAL_CONTENT = 206;

	// [Redirection 3xx]
	const REST_MULTIPLE_CHOICES = 300;
	const REST_MOVED_PERMANENTLY = 301;
	const REST_FOUND = 302;
	const REST_SEE_OTHER = 303;
	const REST_NOT_MODIFIED = 304;
	const REST_USE_PROXY = 305;
	const REST_UNUSED = 306;
	const REST_TEMPORARY_REDIRECT = 307;

	// [Client Error 4xx]
	const REST_BAD_REQUEST = 400;
	const REST_UNAUTHORIZED = 401;
	const REST_PAYMENT_REQUIRED = 402;
	const REST_FORBIDDEN = 403;
	const REST_NOT_FOUND = 404;
	const REST_METHOD_NOT_ALLOWED = 405;
	const REST_NOT_ACCEPTABLE = 406;
	const REST_PROXY_AUTHENTICATION_REQUIRED = 407;
	const REST_REQUEST_TIMEOUT = 408;
	const REST_CONFLICT = 409;
	const REST_GONE = 410;
	const REST_LENGTH_REQUIRED = 411;
	const REST_PRECONDITION_FAILED = 412;
	const REST_REQUEST_ENTITY_TOO_LARGE = 413;
	const REST_REQUEST_URI_TOO_LONG = 414;
	const REST_UNSUPPORTED_MEDIA_TYPE = 415;
	const REST_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
	const REST_EXPECTATION_FAILED = 417;

	// [Server Error 5xx]
	const REST_INTERNAL_SERVER_ERROR = 500;
	const REST_NOT_IMPLEMENTED = 501;
	const REST_BAD_GATEWAY = 502;
	const REST_SERVICE_UNAVAILABLE = 503;
	const REST_GATEWAY_TIMEOUT = 504;
	const REST_VERSION_NOT_SUPPORTED = 505;
	
	public static function sendResponse($status = self::REST_OK, $body = '', $content_type = 'text/html')
	{
		$status_header = 'HTTP/1.1 ' . $status . ' ' . Rest::getStatusCodeMessage($status);
		header($status_header);
		header('Content-type: ' . $content_type);
        echo $body;
        exit;
	}

	public static function getStatusCodeMessage($status)
	{
		// these could be stored in a .ini file and loaded
		// via parse_ini_file()... however, this will suffice
		// for an example
		$codes = Array(
		    100 => 'Continue',
		    101 => 'Switching Protocols',
		    200 => 'OK',
		    201 => 'Created',
		    202 => 'Accepted',
		    203 => 'Non-Authoritative Information',
		    204 => 'No Content',
		    205 => 'Reset Content',
		    206 => 'Partial Content',
		    300 => 'Multiple Choices',
		    301 => 'Moved Permanently',
		    302 => 'Found',
		    303 => 'See Other',
		    304 => 'Not Modified',
		    305 => 'Use Proxy',
		    306 => '(Unused)',
		    307 => 'Temporary Redirect',
		    400 => 'Bad Request',
		    401 => 'Unauthorized',
		    402 => 'Payment Required',
		    403 => 'Forbidden',
		    404 => 'Not Found',
		    405 => 'Method Not Allowed',
		    406 => 'Not Acceptable',
		    407 => 'Proxy Authentication Required',
		    408 => 'Request Timeout',
		    409 => 'Conflict',
		    410 => 'Gone',
		    411 => 'Length Required',
		    412 => 'Precondition Failed',
		    413 => 'Request Entity Too Large',
		    414 => 'Request-URI Too Long',
		    415 => 'Unsupported Media Type',
		    416 => 'Requested Range Not Satisfiable',
		    417 => 'Expectation Failed',
		    500 => 'Internal Server Error',
		    501 => 'Not Implemented',
		    502 => 'Bad Gateway',
		    503 => 'Service Unavailable',
		    504 => 'Gateway Timeout',
		    505 => 'HTTP Version Not Supported'
		);

		return (isset($codes[$status])) ? $codes[$status] : '';
	}
}

?>