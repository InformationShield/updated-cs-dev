<?php
class Utils
{
    public static function FileSizeString($bytes)
    {
        $suffix = 'bytes';
        if ($bytes > 1023) {
            $bytes = round($bytes / 1024 * 100) * .01;
            $suffix = 'KB';
            if ($bytes > 1023) {
                $bytes = round($bytes / 1024 * 100) * .01;
                $suffix = 'MB';
            }
            if ($bytes > 1023) {
                $bytes = round($bytes / 1024 * 100) * .01;
                $suffix = 'GB';
            }
            if ($bytes > 1023) {
                $bytes = round($bytes / 1024 * 100) * .01;
                $suffix = 'TB';
            }
        }
        return $bytes.' '.$suffix;
    }

    public static function createFolder($rootDir, $subDir)
    {
        $newDir = $rootDir.$subDir;
        if (!file_exists($newDir))
        {
            mkdir($newDir);      // best to do in 2 steps due to umask
            chmod($newDir,0777);
        }
        return $newDir;
    }

    public static function getFileContents($file)
    {
		if (file_exists($file)) {
			$size = filesize($file);
			$fh = fopen($file, 'r');
			$contents = fread($fh, $size);
			fclose($fh);
			return $contents;
		} else {
			return null;
		}
    }

    public static function putFileContents($file,$contents)
    {
        $fh = fopen($file, 'w');
        $gotLock = false;
        if (flock($fh, LOCK_EX | LOCK_NB))
        {
            $gotLock = true;
            fwrite($fh, $contents);
        }
        fclose($fh);
        return $gotLock;
    }
	
	public static function unsetAttributes($attributes, $unsetList)
	{
		foreach ($unsetList as $unsetItem) {
			unset($attributes[$unsetItem]);
		}
		return $attributes;
	}
	
	public static function create_guid() 
	{
		$guid = '';
		$uid = uniqid("", true);
		$data = date('mmddyyyyhisu');
		$data .= $_SERVER['REQUEST_TIME'];
		$data .= $_SERVER['HTTP_USER_AGENT'];
		$data .= $_SERVER['SERVER_ADDR'];
		$data .= $_SERVER['SERVER_PORT'];
		$data .= $_SERVER['REMOTE_ADDR'];
		$data .= $_SERVER['REMOTE_PORT'];
		$hash = strtolower(hash('ripemd128', $uid . $guid . md5($data)));
		$guid = substr($hash,  0,  8) . 
				'-' .
				substr($hash,  8,  4) .
				'-' .
				substr($hash, 12,  4) .
				'-' .
				substr($hash, 16,  4) .
				'-' .
				substr($hash, 20, 12);
		return $guid;
	 }

    public static function CreateEncryptedToken()
    {
        $secMgr = new CSecurityManager;
        $secMgr->encryptionKey = GENERAL_ENCRYPTION_KEY;
        $secMgr->cryptAlgorithm = 'blowfish'; //CRYPT_BLOWFISH;
        $value = gmdate('c');
        $encvalue = bin2hex($secMgr->encrypt($value));
        return $encvalue;
    }

    public static function EncryptToken($token)
    {
        $secMgr = new CSecurityManager;
        $secMgr->encryptionKey = GENERAL_ENCRYPTION_KEY;
        $secMgr->cryptAlgorithm = 'blowfish'; //CRYPT_BLOWFISH;
        $encvalue = bin2hex($secMgr->encrypt($token));
        return $encvalue;
    }

    public static function DecryptToken($token)
    {
        // Override Yii's error handling
        set_error_handler(create_function('', "throw new Exception(); return true;"));

        $retVal = null;
        try {
            $secMgr = new CSecurityManager;
            $secMgr->encryptionKey = GENERAL_ENCRYPTION_KEY;
            $secMgr->cryptAlgorithm = 'blowfish'; //CRYPT_BLOWFISH;
            $decvalue = $secMgr->decrypt(pack("H" . strlen($token), $token));
            $retVal = $decvalue;
        } catch (Exception $e) {
            $retVal = null;
        }
        // Put Yii's error handling back
        restore_error_handler();
        return $retVal;
    }

    public static function sqlSafeString($sqlString)
    {
        $newSql = str_ireplace(',', '\,', $sqlString);
        $newSql = str_ireplace('--', '', $newSql);
        $newSql = str_ireplace('"', '\"', $newSql);
        $newSql = str_ireplace("'", "\'", $newSql);
        $newSql = str_ireplace("\t", "", $newSql);
        $newSql = str_ireplace("\r", "", $newSql);
        $newSql = str_ireplace("\n", "", $newSql);
        return $newSql;
    }

    public static function CacheGet($cacheId)
    {
        $retVal = null;
        if (MEMCACHE_ON) {
            $retVal = Yii::app()->cache->get($cacheId);
        }
        return $retVal;
    }

    public static function CacheSet($cacheId, $data, $durationSecs)
    {
        $retVal = false;
        if (MEMCACHE_ON) {
            $data =  Yii::app()->cache->set($cacheId, $data, $durationSecs);
        } else {
            $retVal = true;
        }
        return $retVal;
    }

    public static function CacheExpire($cacheId)
    {
        $retVal = false;
        if (MEMCACHE_ON) {
            $retVal =  Yii::app()->cache->delete($cacheId);
        } else {
            $retVal = true;
        }
        return $retVal;
    }

    public static function PageRenderTime($stop = null)
    {
        static $ourPageStart;
        if ($ourPageStart === null)
            $ourPageStart = microtime(true);
        if ($stop===null)
            return $ourPageStart;
        return (microtime(true) - $ourPageStart);
    }

    public static function getUserFirstLastName($userId = null)
    {
        $data = Utils::getUserProfileFields($userId);
        $first_name = (isset($data['first_name'])) ? $data['first_name']:'';
        $last_name = (isset($data['first_name'])) ? $data['last_name']:'';
        $full_name = trim($first_name.' '.$last_name);
        return array('first_name'=>$first_name,'last_name'=>$last_name,'full_name'=>$full_name);
    }

    public static function getUserProfileFields($userId = null)
    {
        $data = array();
        if (is_null($userId)) $userId = userId();
        $model = User::model()->findByPk($userId);
        $profile = $model->profile;
        $profileFields=ProfileField::model()->forOwner()->sort()->findAll();
        if ($profileFields) {
            foreach($profileFields as $field) {
                $data[$field->varname] = ($field->widgetView($profile)) ? $field->widgetView($profile) : (($field->range) ? Profile::range($field->range, $profile->getAttribute($field->varname)) : $profile->getAttribute($field->varname));
            }
        }
        return $data;
    }

    public static function isSuper() {
        if (getSessionVar('superuser',0) == 1) {
            return true;
        }
        return false;
    }

    public static function setUserSiteModeOff()
    {
        setSessionVar('context_user_site',0);
    }
    public static function isUserSite()
    {
        $context_user_site = getSessionVar('context_user_site',0);
        return ($context_user_site == 1);
    }
    public static function toggleUserSite(){
        $context_user_site = getSessionVar('context_user_site',0);
        $context_user_site = ($context_user_site == 1) ? 0 : 1;
        setSessionVar('context_user_site',$context_user_site);
        return $context_user_site;
    }

    public static function inherentRiskScoreColor($score) {
		if ($score > 90) {
			$color = 'FF5F5B';
		} else if ($score > 70) {
			$color = 'FFA75B';
		} else if ($score > 30) {
			$color = 'FFE65B';
		} else if ($score > 10) {
			$color = 'CEF357';
		} else {
			$color = '7DDD4F';
		}
		return $color;
	}

	public static function maturityScoreColor($score) {
		if ($score > 90) {
			$color = '7DDD4F';
		} else if ($score >= 70) {
			$color = 'CEF357';
		} else if ($score >= 30) {
			$color = 'FFE65B';
		} else {
			$color = 'FF5F5B';
		}
		return $color;
	}

	public static function licenseLevel(){
        return getSessionVar('license_level',0);
    }

    public static function contextCompanyId($encrypted=false){
        $companyId = getSessionVar('context_company_id',null);
        if ($encrypted) {
        	$companyId = Obscure::encode($companyId);
		}
		return $companyId;
    }

    public static function contextCompanyName(){
        return getSessionVar('context_company_name','');
    }

    public static function contextCompanyLogo(){
        return mCompanyContext::GetLogo();
    }

    public static function contextCompanyRole(){
        return getSessionVar('context_company_role',0);
    }

    public static function contextCompanyCount(){
        return getSessionVar('context_company_count',0);
    }

    public static function isRole($role, $exit=false, $orGreater=true)
    {
        $retVal = false;
        if (Utils::isSuper()) {
			$retVal = true;
		} else {
			$currentRole = self::contextCompanyRole();
			if ($orGreater) {
				if ($currentRole >= $role) {
					$retVal = true;
				}
			} else if ($currentRole == $role) {
				$retVal = true;
			}
			if (!$retVal && $exit) {
				badurl('Role not authorized.');
				Yii::app()->end(); // call this instead of exit; so messages are logged
			}
		}
        return $retVal;
    }
    public static function isRoleOwner($exit=false)
    {
        return Utils::isRole(Cons::ROLE_OWNER, $exit);
    }
    public static function isRoleAdmin($exit=false)
    {
        return Utils::isRole(Cons::ROLE_ADMIN, $exit);
    }
    public static function isRoleAuthor($exit=false)
    {
        return Utils::isRole(Cons::ROLE_AUTHOR, $exit);
    }
    public static function isRoleAuditor($exit=false)
    {
        return Utils::isRole(Cons::ROLE_AUDITOR, $exit);
    }
    public static function isRoleUser($exit=false)
    {
        return Utils::isRole(Cons::ROLE_USER, $exit);
    }
}