<?php
/**
 * Miniumizer.php - a Yii extension to minimize and version CSS and JS
 * 
 * Requires JSMin.php
 * @link https://github.com/rgrove/jsmin-php/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * The Software shall be used for Good, not Evil.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * --
 */
class Miniumizer 
{    
    private $companyName;
    
    public function __construct() 
    {
        $this->companyName = Yii::app()->params['companyName'];
    }
    
    public static function registerJS($file, $position = null, $useMinFile=true)
    {
        $miniumizer = new Miniumizer();
        Yii::app()->clientScript->registerScriptFile($miniumizer->VersionMinFile($file,$useMinFile),$position);    
    }

    public static function registerCSS($file, $position = null, $useMinFile=true)
    {
        $miniumizer = new Miniumizer();
        Yii::app()->clientScript->registerCssFile($miniumizer->VersionMinFile($file,$useMinFile),$position);    
    }
    
    public function VersionMinFile($file, $useMinFile=true)
    {
        $rootDir = dirname(__FILE__).'/../..';
        if(strpos($file, '/') !== 0 || !file_exists($rootDir.$file)) {   
            return $file;
        }
        $fileMin = $this->FindMinFile($file);
        if ($useMinFile) {
            $file = $fileMin;
        }

        $srcModTime = filemtime($rootDir.$file);
        $vfile = preg_replace('{\\.([^./]+)$}', ".$srcModTime.\$1", $file);
        return Yii::app()->request->baseUrl . $vfile;
    }
    
    private function FindMinFile($file)
    {        
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if ($ext !== 'js' && $ext !== 'css') {      
            Yii::log('Extension "'.$ext.'" not supported','info','Miniumizer.FindMinFile');
            return $file;            
        }
        
        if(strrpos($file, '.min.js') !== false && strrpos($file, '.min.css') !== false) {
            return $file;
        }

        $refresh = false;
        $rootDir = dirname(__FILE__).'/../..';
        $srcFilePath = $rootDir.$file;
        $fileMin = substr($file, 0, strrpos($file, '.')).'.min.'.$ext;

        if (file_exists($rootDir.$fileMin))
        {
            $srcModTime = filemtime($srcFilePath);
            $minModTime = filemtime($rootDir.$fileMin);
            if ($minModTime >= $srcModTime) {
                return $fileMin;
            } else {
                $refresh = true;
            }
        }

        $newfile = $this->Miniumize($srcFilePath);
        if ($newfile !== false && file_exists($rootDir.$fileMin)) 
        {
            if ($refresh) {
                Yii::log('Miniumizer refresh '.$fileMin,'info','Miniumizer.FindMinFile');
            } else {
                Yii::log('Miniumizer new '.$fileMin,'info','Miniumizer.FindMinFile');
            }
            return $fileMin;
        } else {   
            Yii::log('Failed to create min file'.$fileMin,'error','Miniumizer.FindMinFile');
        }
        return $file;
    }
     
    private function Miniumize($srcFile)
    {        
        $pathinfo = pathinfo($srcFile);
        $name = $pathinfo['filename'];
        $nameWithExt = $pathinfo['basename'];
        $dirname = $pathinfo['dirname']; 
        if (isset($pathinfo['extension'])) {
            $ext = $pathinfo['extension'];
        } else {
            $ext = null;
        }
        if ($ext !== 'js' && $ext !== 'css') {
            Yii::log('not css or js file extension '.$srcFile,'error','Miniumizer.Miniumize');
            return false;
        }
                
        if (empty($nameWithExt)) {
            Yii::log('empty filename','error','Miniumizer.Miniumize');
            return false;
        }
        if ((empty($dirname) || $dirname == '.') && !file_exists($srcFile)) {
            $dirname = dirname(__FILE__).'/../../'.$ext;
            $srcFile = $dirname.'/'.$nameWithExt;
        }
        if (!file_exists($srcFile)) {
            Yii::log('can not find file '.$srcFile,'error','Miniumizer.Miniumize');
            return false;
        }

        $minExtFilePath = $dirname.'/'.$name.'.min.'.$ext;        
        $fileContents = Utils::getFileContents($srcFile);
        if (empty($fileContents)) {
            Yii::log('empty file '.$srcFile,'error','Miniumizer.Miniumize');
            return false;
        }

        try 
        {                        
            if ($ext === 'js') {               
                $minContents = "/* Copyright &copy; ".date('Y')." by ".$this->companyName." All Rights Reserved. */\n";
                $minContents .= JSMin::minify($fileContents);
            } else {                
                $minContents = $this->CSSMin($fileContents);
            }
            $success = Utils::putFileContents($minExtFilePath,$minContents);
            if (!$success) {
                Yii::log('Could not write file '.$minExtFilePath,'error','Miniumizer.Miniumize');
                return false;
            }            
            return $minExtFilePath;            
        } 
        catch (Exception $e) 
        {
            Yii::log("exception occurred on ".$srcFile."\n".$e->__toString()."\n",'error','Miniumizer.Miniumize');
            return false;
        }
        return false;
    }
        
    private function CSSMin($contents)
    {
        $contents = preg_replace( '#/\*.*?\*/#s', '', $contents );
        // remove new lines \\n, tabs and \\r
        $contents = preg_replace('/(\t|\r|\n)/', '', $contents);
        // replace multi spaces with singles
        $contents = preg_replace('/(\s+)/', ' ', $contents);
        //Remove empty rules
        $contents = preg_replace('/[^}{]+{\s?}/', '', $contents);
        // Remove whitespace around selectors and braces
        $contents = preg_replace('/\s*{\s*/', '{', $contents);
        // Remove whitespace at end of rule
        $contents = preg_replace('/\s*}\s*/', '}', $contents);
        // Just for clarity, make every rules 1 line tall
        $contents = preg_replace('/}/', "}\n", $contents);
        $contents = str_replace( ';}', '}', $contents );
        $contents = str_replace( ', ', ',', $contents );
        $contents = str_replace( '; ', ';', $contents );
        $contents = str_replace( ': ', ':', $contents );
        $contents = preg_replace( '#\s+#', ' ', $contents );
        return $contents;
    }    
}

?>
