<?php

// adjust these paths to match you environment
require_once __DIR__ . '/../vendor/php-jwt/src/BeforeValidException.php';
require_once __DIR__ . '/../vendor/php-jwt/src/ExpiredException.php';
require_once __DIR__ . '/../vendor/php-jwt/src/SignatureInvalidException.php';
require_once __DIR__ . '/../vendor/php-jwt/src/JWT.php';

use \Firebase\JWT\JWT;

/**
 * See this website http://jwt.io/ for libraries and info
 * Standards info https://tools.ietf.org/html/rfc7519
 *
 * JSON Web Token php implementation from https://github.com/firebase/php-jwt
 *
 * This class is used to create and decode auto-login urls for the https://app.easyitcompliance.com website.
 *
 * Example usage:
 *
 * require_once EasySecLogin.php
 * $easySecLogin = new EasySecLogin($partner_id, $shared_secret)
 * $url = $easySecLogin->createUrl("testuser@gmail.com");
 *
 */
class EasySecLogin
{
    // Assigned shared secret. Please txt Earl's cell 832-419-7020 for the actual shared_secret
    public $shared_secret = "8324197020";
    // assigned partner id
    public $partner_id = "0";
    // handle clock differences and latency in use (seconds)
    public $timeleeway = 300; // 5 minutes
    // encryption algorithm - do not change value
    public $algorithm = 'HS256';


    public function __construct($partner_id, $shared_secret, $timeleeway = 300, $algorithm = 'HS256')
    {
        $this->partner_id = $partner_id;
        $this->shared_secret = $shared_secret;
        $this->timeleeway = $timeleeway;
        $this->algorithm = $algorithm;
    }

    /**
     * Creates a auto sig-in url for the app.easyitcompliance.com website.
     *
     * @param string    $email  The user's email address.
     * @param string    $key    Optional secret key.
     * @param string    $urlBase    The base url to append the token to.
     *
     * @return string   The url to auto sign-in user by email
     *
     */
    public function createUrl($email, $key = null, $urlBase = "https://app.easyitcompliance.com/easysec/site/signin")
    {
        $encodedToken = $this->encodeToken($email, $key);
        $url = $urlBase."/p/".$this->partner_id."/token/".$encodedToken;
        return $url;
    }

    /**
     * Encodes the token for the url.
     *
     * @param string    $email  The user's email address.
     * @param string    $key    Optional secret key.
     *
     * @return string   Encoded token.
     *
     */
    public function encodeToken($email, $key = null)
    {
        $token = array(
            "iss" => $this->partner_id,
            "nbf" => time() - $this->timeleeway, // not valid before this time
            "exp" => time() + $this->timeleeway, // not valid after this time
            "sub" => $email
        );
        $key = (is_null($key)) ? $this->shared_secret : $key;
        $encodedToken = JWT::encode($token, $key, $this->algorithm);
        return $encodedToken;
    }

    /**
     * Decodes a token.
     *
     * @param string    $encodedToken  The encoded token.
     * @param string    $key    Optional secret key.
     *
     * @return object   Decoded json as php object.
     *
     */
    public function decodeToken($encodedToken, $key = null)
    {
        $key = (is_null($key)) ? $this->shared_secret : $key;
        $tokenObj = JWT::decode($encodedToken, $key, array($this->algorithm));
        return $tokenObj;
    }

}