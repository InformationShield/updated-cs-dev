<?php

class Industries {

	protected static $industries = array("Business Services",
		"Agriculture & Forestry/Wildlife",
		"Business & Information",
		"Construction/Utilities/Contracting",
		"Education",
		"Finance & Insurance",
		"Food & Hospitality",
		"Gaming",
		"Health Services",
		"Motor Vehicle",
		"Natural Resources/Environmental",
		"Other",
		"Personal Services",
		"Real Estate & Housing Home",
		"Warehouse/Storage",
		"Safety/Security & Legal",
		"Transportation Air Transportation"
	);

	public static function getIndustries() {
		return self::$industries;
	}
	public static function getKeyVal() {
		$list = array();
		foreach(self::$industries as $key=>$val) {
			$list[$val] = $val;
		}
		return $list;
	}
}
