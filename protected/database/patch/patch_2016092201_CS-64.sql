ALTER TABLE `company_info`
  DROP `publicly_traded`,
  DROP `critical_infrastructure`;

ALTER TABLE `company_info` CHANGE `remote_network_access` `remote_network_access` TINYINT(1) NULL DEFAULT NULL COMMENT '[0 (None - No Remote Access), 1 (Limited (< 5% of workforce)), 2(Some (5 - 25% of workforce) ), 3 (Moderate (25 - 50% of workforce)), 4 (Most ( > 50% of workforce))] If [QD=6] > 0, then Control ID [222,224] = Planned';

UPDATE `version` SET `Build`= 2016092201, `UpdatedOn` = NOW(), `Description` = "Last patch was patch_2016092201_CS-64.sql" WHERE `Id` = 1;