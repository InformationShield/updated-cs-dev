ALTER TABLE `message` ADD COLUMN `parent_id` int(11) DEFAULT NULL AFTER recipient_id;

UPDATE `version` SET `Build`= 20170510, `UpdatedOn` = NOW(), `Description` = "Last patch was patch_2017051001_CS-89.sql" WHERE `Id` = 1;
