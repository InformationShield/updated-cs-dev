ALTER TABLE `video_training` CHANGE `video_code` `video_code` varchar(2048) NOT NULL;

UPDATE `version` SET `Build`= 20170414, `UpdatedOn` = NOW(), `Description` = "Last patch was patch_2017041401_CS-80.sql" WHERE `Id` = 1;