/*** Manually find & replace the following on the backup db script ***/
-- company_profile -> company
-- company_user -> employee
-- RESTORE DB with the modified script

/*** recreate schema with proper collation ***/
-- drop schema easysecprod;
-- create schema easysecprod CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
-- use easysecprod;

CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL COMMENT 'user who send the message',
  `recipient_id` int(11) NOT NULL COMMENT 'user who recipient the message',
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'title message',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'content message',
  `read` tinyint(1) DEFAULT '0' COMMENT 'Attribute for check was read or not read',
  `updated_on` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  PRIMARY KEY (`message_id`),
  KEY (`sender_id`),
  CONSTRAINT `fk_message_sender_id` FOREIGN KEY (`sender_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  KEY (`recipient_id`),
  CONSTRAINT `fk_message_recipient_id` FOREIGN KEY (`recipient_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `video_training` CHANGE `video_code` `video_code` varchar(4096) NOT NULL;

ALTER TABLE `company_info` ADD COLUMN `inherent_risk_score` INTEGER DEFAULT '0';

ALTER DATABASE [***** ENTER DB NAME HERE *****] CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
SELECT * FROM information_schema.SCHEMATA ;

ALTER TABLE baseline CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE company_info CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE company CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE company_role CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE employee CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE control_baseline CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE control_category CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE control_library CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE control_status CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE cyber_risk_score CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE evidence CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE export_report CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE incident CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE incident_status CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE incident_type CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE invite CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE license_level CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE message CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE partner CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE policy CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE policy_library CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE policy_status CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE quiz CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE quiz_answer CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE quiz_library CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE quiz_question CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE quiz_status CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE quiz_tracking CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE security_role CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE security_role_status CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE task CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE task_frequency CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE task_status CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tbl_migration CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tbl_profiles CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tbl_profiles_fields CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tbl_users CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE training CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE training_library CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE training_status CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE user_policy_tracking CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE user_profile CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE user_profile_policy CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE user_profile_quiz CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE user_profile_training CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE user_training_tracking CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE usersetting CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE version CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE video_tracking CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE video_training CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE yiisession CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `employee2profile`;
CREATE TABLE `employee2profile` (
  `employee2profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `user_profile_id` int(11) NOT NULL,
  PRIMARY KEY (`employee2profile_id`),
  KEY `i_employee_id` (`employee_id`),
  KEY `i_user_profile_id` (`user_profile_id`),
  CONSTRAINT `fk_employee2profile_employee_id` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
  CONSTRAINT `fk_employee2profile_user_profile_id` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profile` (`user_profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*
 Some data massaging to be done manually
 find dupes with same email, company, and profile_id
*/
-- find dupes with same profile (i.e. same email, company, and profile_id)
SELECT e.email
FROM employee e
  INNER JOIN company c ON c.company_id = e.company_id
  LEFT JOIN tbl_users u ON u.email = e.email
GROUP BY e.email, e.company_id, e.profile_id
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC;

-- get the employee_ids of the dupes
SELECT e.*
FROM employee e
  INNER JOIN company c ON c.company_id = e.company_id
  LEFT JOIN tbl_users u ON u.email = e.email
WHERE e.email IN (
  'ashton.brown@emaint.com',
  'Uday.Barya@Contractor.Arthrex.com',
  'jacquie.rivera@dcrworkforce.com'
)
ORDER BY e.email, employee_id;

-- delete the dupes
DELETE FROM employee
WHERE employee_id IN (426, 1386, 1245)
;

-- find dupes with same departments (i.e. same email, and company)
SELECT e.email
FROM employee e
  INNER JOIN company cp ON cp.company_id = e.company_id
  LEFT JOIN tbl_users u ON u.email = e.email
GROUP BY e.email, e.company_id
HAVING COUNT(*) > 1;

-- get the employee_ids of the dupes
SELECT e.employee_id, i.employee_id, sr.employee_id, t.employee_id, tu.id UserId, e.*
FROM employee e
  LEFT JOIN incident i ON i.employee_id = e.employee_id
  LEFT JOIN security_role sr ON sr.employee_id = e.employee_id
  LEFT JOIN task t ON t.employee_id = e.employee_id
  LEFT JOIN tbl_users tu ON tu.email = e.email
WHERE e.email IN (
  'Annmarie.Lyons@dcrworkforce.com',
  'Arunaachalam.Nagarajan@arthrex.com',
  'Bhargav.Pinnam@Arthrex.com',
  'Figurov@suekag.com',
  'Gopavarapu.Rao@Contractor.Arthrex.com'
)
ORDER BY e.email, e.employee_id;

-- ******** Manually merge the departments **********/

-- delete the dupes
DELETE FROM employee
WHERE employee_id IN (1389, 650, 663, 1378, 1247);

-- insert user profiles employees belong to
INSERT INTO employee2profile (employee_id, user_profile_id)
SELECT e.employee_id, up.user_profile_id
FROM employee e
  INNER JOIN user_profile up ON up.company_id = e.company_id AND up.profile_id = e.profile_id
  LEFT JOIN tbl_users u ON u.email = e.email
ORDER BY e.email;

/*** Manually find & replace the following for the code ***/
-- company_user_user_profile -> company2employee (case)
-- CompanyUserUserProfile -> Company2employee (case)
-- cuup -> c2e (case)

-- company_user_user_profile -> employee2profile (case)
-- CompanyUserUserProfile -> Employee2profile (case)
-- cuup -> e2p (case)


-- company_user -> employee (case, word)
-- CompanyUser -> Employee (case, word)
-- Companyuser -> Employee (case, word)
-- companyUser -> employee (case, word)
-- companyuser -> employee (case, word)
-- company_user -> employee (case)
-- CompanyUser -> Employee (case)
-- Companyuser -> Employee (case)
-- companyUser -> employee (case)
-- companyuser -> employee (case)
-- cu -> e (case) - only on models_ex, models_prime, modules/admin
-- companyuser -> employee () ** should find nothing but if finds, manual replace **

-- company_profile -> company (case, word)
-- CompanyProfile -> Company (case, word)
-- Companyprofile -> Company (case, word)
-- companyProfile -> company (case, word)
-- companyprofile -> company (case, word)

-- company_profile_id -> company_id

-- company_profile -> company (case)
-- CompanyProfile -> Company (case)
-- Companyprofile -> Company (case)
-- companyProfile -> company (case)
-- companyprofile -> company (case)
-- cp -> leave alone
-- companyprofile -> company () ** should find nothing but if finds, manual replace **


-- Rename following controllers
-- CompanyuserController -> EmployeeController
-- modules/admin/CompanyprofileController -> CompanyController

-- Rename following models and models_ex
-- CompanyUser -> Employee
-- CompanyUser_Ex -> Employee_Ex

-- rename folder protected/views/companyuser -> employee
-- rename folder modules/admin/views/companyprofile -> company

UPDATE `version` SET `Build`= 20170704, `UpdatedOn` = NOW(), `Description` = "Last patch was patch_2017070401_CS-94.sql" WHERE `Id` = 1;

