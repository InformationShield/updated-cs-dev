CREATE TABLE `company_user_user_profile` (
    `id` INT NOT NULL AUTO_INCREMENT , 
    `company_user_id` INT NOT NULL , 
    `user_profile_id` INT NOT NULL , 
    PRIMARY KEY (`id`),
    KEY (`company_user_id`),
    CONSTRAINT `fk_company_user_user_profile_company_user_id` FOREIGN KEY (`company_user_id`) REFERENCES `company_user` (`company_user_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
    KEY (`user_profile_id`),
    CONSTRAINT `fk_company_user_user_profile_user_profile_id` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profile` (`user_profile_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB;

UPDATE `version` SET `Build`= 20160929, `UpdatedOn` = NOW(), `Description` = "Last patch was patch_2016092901_CS-48.sql" WHERE `Id` = 1;