ALTER TABLE `company_info` ADD COLUMN `inherent_risk_score` INTEGER NOT NULL DEFAULT '0';

UPDATE `version` SET `Build`= 20170504, `UpdatedOn` = NOW(), `Description` = "Last patch was patch_2017050401_CS-83.sql" WHERE `Id` = 1;
