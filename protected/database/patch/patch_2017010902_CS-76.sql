DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL COMMENT 'user who send the message',
  `recipient_id` int(11) NOT NULL COMMENT 'user who recipient the message',
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'title message',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'content message',
  `read` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Attribute for check was read or not read',
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`message_id`),
  KEY (`sender_id`),
  CONSTRAINT `fk_message_sender_id` FOREIGN KEY (`sender_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  KEY (`recipient_id`),
  CONSTRAINT `fk_message_recipient_id` FOREIGN KEY (`recipient_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

UPDATE `version` SET `Build`= 20170109, `UpdatedOn` = NOW(), `Description` = "Last patch was patch_2017010901_CS-76.sql" WHERE `Id` = 1;
