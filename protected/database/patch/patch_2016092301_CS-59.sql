RENAME TABLE `inherent_risk_score` TO `cyber_risk_score`;

UPDATE `version` SET `Build`= 2016092301, `UpdatedOn` = NOW(), `Description` = "Last patch was patch_2016092301_CS-59.sql" WHERE `Id` = 1;
