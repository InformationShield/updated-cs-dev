ALTER TABLE `message` ADD COLUMN `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active';

UPDATE `version` SET `Build`= 20170115, `UpdatedOn` = NOW(), `Description` = "Last patch was patch_2017011501_CS-78.sql" WHERE `Id` = 1;
