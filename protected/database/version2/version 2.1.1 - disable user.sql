ALTER TABLE `employee` ADD COLUMN `status` INTEGER DEFAULT '1';

CREATE TABLE `status` (
  `status_id` tinyint(4) NOT NULL,
  `status_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `status` (`status_id`, `status_name`)
VALUES
  (0, 'Disabled'),
  (1, 'Enabled');
