SET FOREIGN_KEY_CHECKS=0;
DELETE FROM license_level;
INSERT INTO `license_level` (`license_level_id`, `license_level_name`)
VALUES
  (1, 'Free'),
  (10, 'Trial'),
  (100, 'Level 1'),
  (200, 'Level 2'),
  (300, 'Level 3'),
  (1000, 'Ultimate');
SET FOREIGN_KEY_CHECKS=1;

-- CS-95
ALTER TABLE `policy` ADD COLUMN `policy_type` TINYINT DEFAULT '1'
  COMMENT '0=none, 1=Policy, 2=Standard, 3=Procedure, 4=Form, 99=Other' AFTER approved;
ALTER TABLE `policy_library` ADD COLUMN `policy_type` TINYINT DEFAULT '1'
  COMMENT '0=none, 1=Policy, 2=Standard, 3=Procedure, 4=Form, 99=Other' AFTER approved;

-- CS-102
ALTER TABLE `company` ADD COLUMN `trial_expiry` DATETIME DEFAULT '0000-00-00 00:00:00' AFTER created_on;

-- CS-119
ALTER TABLE `policy_library` ADD COLUMN `license_level` INT DEFAULT '100' AFTER policy_type;
UPDATE policy_library
SET license_level = 10
WHERE policy_library_id IN (47, 54, 115, 97);


-- Grandfather existing company licenses
UPDATE company
SET license_level = 200
WHERE license_level >= 100;

-- Reset trial expiry to 15 days from now for all existing trial accounts
UPDATE company
SET trial_expiry = DATE_ADD(NOW(), INTERVAL 15 DAY)
WHERE license_level = 10
      AND status = 1;

-- Create a deleted and disabled user for CHI deletion stored proc.  Updated on production db already
# INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `activkey`, `superuser`, `status`, `partner_id`, `create_at`, `lastvisit_at`, `eula`)
# VALUES
#   (2, '__DELETED__', '$2a$07$HjwEpkS78Dh1buY5VqSGPuNnSnUemBOrFS8hq8X/X9HYhTs6qa1q2', 'deleted.easysec@tynylabs.com', '81f0ba4b3d6233dd43659640845c23cd0124494d', 0, 0, NULL, '2017-08-23 15:20:19', '0000-00-00 00:00:00', 0);
# INSERT INTO `tbl_profiles` (`user_id`, `first_name`, `last_name`)
# VALUES
#   (2, 'Deleted', 'User');

-- CS-129
ALTER TABLE `security_role` CHANGE `role_summary` `role_summary` text COLLATE utf8mb4_unicode_ci;

-- Cyber Risk Score
CREATE TABLE `control_answer` (
  `control_answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `control_baseline_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `control_status_id` int(11) DEFAULT '0',
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT '1',
  `assessed_on` timestamp NULL DEFAULT NULL,
  KEY `fk_answer_control_baseline_id` (`control_baseline_id`),
  CONSTRAINT `fk_answer_control_baseline_id` FOREIGN KEY (`control_baseline_id`) REFERENCES `control_baseline` (`control_baseline_id`),
  KEY `fk_answer_company_id` (`company_id`),
  CONSTRAINT `fk_answer_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  KEY `fk_answer_answer_id` (`control_status_id`),
  CONSTRAINT `fk_answer__answer_id` FOREIGN KEY (`control_status_id`) REFERENCES `control_status` (`control_status_id`),
  KEY `fk_answer_updated_user_id` (`updated_user_id`),
  CONSTRAINT `fk_answer__updated_user_id` FOREIGN KEY (`updated_user_id`) REFERENCES `tbl_users` (`id`),
  PRIMARY KEY (`control_answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `baseline` ADD COLUMN `special_type` INT DEFAULT '0' COMMENT '0=none, 1=CyberBaseline' AFTER user_id;

ALTER TABLE `control_baseline` ADD COLUMN `cat_override` INT DEFAULT NULL AFTER cat_sub_code;

ALTER TABLE `company_info` ADD COLUMN `cyber_risk_score` INTEGER NOT NULL DEFAULT '0';

