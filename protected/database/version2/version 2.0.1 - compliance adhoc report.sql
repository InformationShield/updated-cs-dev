SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `control_filter`;
CREATE TABLE `control_filter` (
  `control_filter_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `filter_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filter_desc` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label1` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label2` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '0=delete, 1=active',
  `updated_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `company_id` (`company_id`),
  CONSTRAINT `fk_control_filter_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  PRIMARY KEY (`control_filter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- delete obsolete user settings
DELETE FROM usersetting
WHERE key_name = 'defaultCompanyProfileId';

SET FOREIGN_KEY_CHECKS=1;
