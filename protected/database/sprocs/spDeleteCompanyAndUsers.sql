DROP PROCEDURE IF EXISTS spDeleteCompanyAndUsers;
DELIMITER $$
CREATE PROCEDURE spDeleteCompanyAndUsers(
  IN CompanyId INT(11)
)
  BEGIN

    -- Delete company users and associated data
    DECLARE done BOOLEAN DEFAULT FALSE;
    DECLARE _userId INT(11);
    DECLARE _employeeId INT(11);
    DECLARE curUser CURSOR FOR
      SELECT u.id, e.employee_id
      FROM tbl_users u
        INNER JOIN employee e ON e.email = u.email
      WHERE e.company_id = CompanyId;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := TRUE;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
      BEGIN
        ROLLBACK;
        SELECT 'FAILED';
      END;

    START TRANSACTION;

    OPEN curUser;
    loopDeleteUser: LOOP
      FETCH curUser INTO _userId, _employeeId;
      IF done THEN
        LEAVE loopDeleteUser;
      END IF;
      CALL spDeleteUser(_userId);
    END LOOP loopDeleteUser;
    CLOSE curUser;

    SET _userId := (SELECT user_id FROM company WHERE company_id = CompanyId);
    CALL spDeleteUser(_userId);

    -- Delete Company and associated data
    DELETE FROM evidence where company_id = CompanyId;
    DELETE FROM control_filter where company_id = CompanyId;
    DELETE FROM control_answer where company_id = CompanyId;
    DELETE FROM control_baseline where company_id = CompanyId;
    DELETE FROM baseline where company_id = CompanyId;

    DELETE FROM incident_type where company_id = CompanyId;
    DELETE FROM incident where company_id = CompanyId;

    DELETE FROM user_policy_tracking where company_id = CompanyId;
    DELETE FROM policy where company_id = CompanyId;

    DELETE FROM quiz_answer where company_id = CompanyId;
    DELETE FROM quiz_tracking where company_id = CompanyId;
    DELETE FROM quiz where company_id = CompanyId;

    DELETE FROM user_training_tracking where company_id = CompanyId;
    DELETE FROM training where company_id = CompanyId;

    DELETE FROM video_tracking where company_id = CompanyId;
    DELETE FROM video_training where company_id = CompanyId;

    DELETE e2p FROM employee2profile e2p
      INNER JOIN employee e ON e.employee_id = e2p.employee_id
      INNER JOIN company c ON c.company_id = e.company_id
      WHERE c.company_id = CompanyId;
    DELETE FROM user_profile_policy where company_id = CompanyId;
    DELETE FROM user_profile_quiz where company_id = CompanyId;
    DELETE FROM user_profile_training where company_id = CompanyId;
    DELETE FROM user_profile where company_id = CompanyId;

    DELETE FROM task where company_id = CompanyId;
    DELETE FROM security_role where company_id = CompanyId;

    DELETE FROM employee where company_id = CompanyId;
    DELETE FROM export_report where company_id = CompanyId;
    DELETE FROM cyber_risk_score where company_id = CompanyId;
    DELETE FROM invite where company_id = CompanyId;
    DELETE FROM company_info where company_id = CompanyId;
    DELETE FROM company where company_id = CompanyId;

    COMMIT;
    SELECT 'SUCCESS';

  END$$
DELIMITER ;
