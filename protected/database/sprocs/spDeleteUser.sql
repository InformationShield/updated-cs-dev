DROP PROCEDURE IF EXISTS spDeleteUser;
DELIMITER $$
CREATE PROCEDURE spDeleteUser(
  IN UserId INT(11)
)
BEGIN
#     SET @EmployeeId = (
#       SELECT e.employee_id
#       FROM employee e
#       INNER JOIN tbl_users u ON u.email = e.email
#       WHERE u.id = UserId
#     );

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      ROLLBACK;
    END;

    START TRANSACTION;

    DELETE FROM evidence WHERE user_id = UserId AND user_id != 49;
    DELETE FROM control_baseline WHERE user_id = UserId AND user_id != 49;
    DELETE FROM baseline WHERE user_id = UserId AND user_id != 49;

    DELETE FROM incident WHERE user_id = UserId AND user_id != 49;

    DELETE FROM message WHERE sender_id = UserId OR recipient_id = UserId AND user_id != 49;

    DELETE FROM user_policy_tracking WHERE user_id = UserId AND user_id != 49;
    DELETE FROM policy WHERE user_id = UserId AND user_id != 49;

    DELETE FROM quiz_answer WHERE user_id = UserId AND user_id != 49;
    DELETE FROM quiz_tracking WHERE user_id = UserId AND user_id != 49;
    DELETE FROM quiz_question WHERE user_id = UserId AND user_id != 49;
    DELETE FROM quiz WHERE user_id = UserId AND user_id != 49;

    DELETE FROM user_training_tracking WHERE user_id = UserId AND user_id != 49;
    DELETE FROM training WHERE user_id = UserId AND user_id != 49;

    DELETE FROM video_tracking WHERE user_id = UserId AND user_id != 49;
    DELETE FROM video_training WHERE user_id = UserId AND user_id != 49;

    DELETE FROM task WHERE user_id = UserId AND user_id != 49;
    DELETE FROM export_report WHERE user_id = UserId AND user_id != 49;
    DELETE FROM cyber_risk_score WHERE user_id = UserId AND user_id != 49;
    DELETE FROM invite WHERE user_id = UserId AND user_id != 49;
    DELETE FROM usersetting WHERE user_id = UserId AND user_id != 49;

    UPDATE company
    SET user_id = 2
    WHERE user_id = UserId AND user_id != 49;

    UPDATE company_info
    SET user_id = 2
    WHERE user_id = UserId AND user_id != 49;

    DELETE FROM tbl_profiles WHERE user_id = UserId AND user_id != 2 AND user_id != 49;
    DELETE FROM tbl_users WHERE id = UserId AND id != 2 AND user_id != 49;

    COMMIT;

END$$
DELIMITER ;
