# Trial Co & User
UPDATE `company` SET
  `user_id` = 4,
  `name` = 'TrialCo',
  `license_level` = 10,
  `trial_expiry` = '2020-01-19 02:57:29'
WHERE company_id = 7;

UPDATE `tbl_users` SET
  username = 'trial',
  password = 'trial',
  email = 'trial.easysec@tynylabs.com'
WHERE id = 4;

UPDATE `tbl_profiles` SET
  first_name = 'Trial',
  last_name = 'User'
WHERE user_id = 4;

# Alone User
UPDATE `tbl_users` SET
  username = 'alone',
  password = 'alone',
  email = 'alone.easysec@tynylabs.com',
  status = 1
WHERE id = 8;

UPDATE `tbl_profiles` SET
  first_name = 'Alone',
  last_name = 'User'
WHERE user_id = 8;

# Free Co & User
UPDATE `company` SET
  `user_id` = 9,
  `name` = 'FreeCo',
  `license_level` = 1
WHERE company_id = 26;

UPDATE `tbl_users` SET
  username = 'free',
  password = 'free',
  email = 'free.easysec@tynylabs.com',
  status = 1
WHERE id = 9;

UPDATE `tbl_profiles` SET
  first_name = 'Free',
  last_name = 'User'
WHERE user_id = 9;

# Author & Auditor User
UPDATE `company` SET
  `user_id` = 3
WHERE company_id IN (26, 27);

UPDATE `tbl_users` SET
  username = 'auditor',
  password = 'auditor',
  email = 'auditor.easysec@tynylabs.com',
  status = 1
WHERE id = 46;

UPDATE `tbl_users` SET
  username = 'author',
  password = 'author',
  email = 'author.easysec@tynylabs.com',
  status = 1
WHERE id = 47;

UPDATE `tbl_profiles` SET
  first_name = 'Auditor',
  last_name = 'User'
WHERE user_id = 46;

UPDATE `tbl_profiles` SET
  first_name = 'Author',
  last_name = 'User'
WHERE user_id = 47;

INSERT INTO `employee` (`employee_id`, `company_id`, `company_role_id`, `email`, `firstname`, `lastname`, `department_name`, `department_id`, `profile_id`, `created_on`)
VALUES
  (5, 46, 200, 'auditor.easysec@tynylabs.com', 'Auditor', 'User', NULL, NULL, 0, '2017-07-14 21:22:44'),
  (6, 47, 300, 'author.easysec@tynylabs.com', 'Author', 'User', NULL, NULL, 0, '2017-07-14 21:22:44');

# Admin Account
UPDATE `tbl_users` SET
  password = 'admin'
WHERE id = 1;

# General Test Account
UPDATE `tbl_users` SET
  password = 'earl'
WHERE id = 3;
