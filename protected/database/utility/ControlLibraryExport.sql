SELECT control_library_id, cat_code, sort_order, cat_code, cat_sub_code, cat_id, control_id, cc.category, title, cl.status, priority,
  REPLACE(REPLACE(detail, '\r\n', ' '), '\n', ' ') detail,
  REPLACE(REPLACE(REPLACE(guidance, '\r\n', ' '), '\r', ' '), '\n', ' ') guidance,
  REPLACE(REPLACE(jobrole, '\r\n', ' '), '\n', ' ') jobrole,
  REPLACE(REPLACE(rel_policy, '\r\n', ' '), '\n', ' ') rel_policy,
  REPLACE(REPLACE(evidence, '\r\n', ' '), '\n', ' ') evidence,
  REPLACE(REPLACE(REPLACE(reference, '\r\n', ' | '), '\r', ' | '), '\n', ' | ') reference,
  REPLACE(REPLACE(question, '\r\n', ' '), '\n', ' ') question,
  score, weighting
FROM control_library cl
  INNER JOIN control_category cc ON cc.control_category_id = cl.cat_id
ORDER BY cat_id, sort_order;