SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `control_category`;
CREATE TABLE `control_category` (
  `control_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `root_id` int(11) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '0=delete, 1=active',
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`control_category_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=558 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `control_category` (`control_category_id`, `parent_id`, `root_id`, `status`, `category`, `description`)
VALUES
  (1, NULL, 1, 1, 'None', NULL),
  (279, 0, 279, 1, '1. IT Risk Management', ''),
  (280, 279, 279, 1, '1.1. Risk Management Process', ''),
  (281, 280, 279, 1, '1.1.1. Risk Management Program', ''),
  (282, 280, 279, 1, '1.1.2. Risk Assessment Implementation', ''),
  (283, 280, 279, 1, '1.1.3. Risk Treatment', ''),
  (284, 0, 284, 1, '2. Security Policies & Procedures', ''),
  (285, 284, 284, 1, '2.1. Security Policy and Procedure Development', ''),
  (286, 285, 284, 1, '2.1.1. Security Policy Development', ''),
  (287, 285, 284, 1, '2.1.2. Security Procedure Development', ''),
  (288, 285, 284, 1, '2.1.3. Security Policy Coverage', ''),
  (289, 284, 284, 1, '2.2. Security Policy Management', ''),
  (290, 289, 284, 1, '2.2.1. Security Policy Distribution ', ''),
  (291, 289, 284, 1, '2.2.2. Security Policy Review', ''),
  (292, 289, 284, 1, '2.2.3. Security Policy Exceptions', ''),
  (293, 289, 284, 1, '2.2.4. Security Policy Sanctions', ''),
  (294, 0, 294, 1, '3. Security Program Management', ''),
  (295, 294, 294, 1, '3.1. Security Program Governance', ''),
  (296, 295, 294, 1, '3.1.1. Information Security Planning', ''),
  (297, 295, 294, 1, '3.1.2. Intellectual Property Protection', ''),
  (298, 294, 294, 1, '3.2. Information Security Organization', ''),
  (299, 298, 294, 1, '3.2.1. Information Security Roles', ''),
  (300, 298, 294, 1, '3.2.2. Responsibility Assignment', ''),
  (301, 298, 294, 1, '3.2.3. Contact with Outside Parties', ''),
  (302, 298, 294, 1, '3.2.4. Information Security Team', ''),
  (303, 294, 294, 1, '3.3. Security Compliance Evaluation', ''),
  (304, 303, 294, 1, '3.3.1. Security Program Compliance', ''),
  (305, 303, 294, 1, '3.3.2. Technical Compliance Evaluation', ''),
  (306, 0, 306, 1, '4. Asset Management', ''),
  (307, 306, 306, 1, '4.1. Asset Procurement', ''),
  (308, 307, 306, 1, '4.1.1. Physical Asset Procurement (Supply Chain)', ''),
  (309, 306, 306, 1, '4.2. Asset Inventory', ''),
  (310, 309, 306, 1, '4.2.1. Physical Asset Inventory', ''),
  (311, 306, 306, 1, '4.3. Asset Accountability', ''),
  (312, 311, 306, 1, '4.3.1. Asset Classification', ''),
  (313, 311, 306, 1, '4.3.2. Asset Ownership Assignment', ''),
  (314, 306, 306, 1, '4.4. Asset Protection', ''),
  (315, 314, 306, 1, '4.4.1. Asset Assignment', ''),
  (316, 314, 306, 1, '4.4.2. Configuration Control', ''),
  (317, 314, 306, 1, '4.4.3  Protection Requirements', ''),
  (318, 306, 306, 1, '4.5. Acceptable Use of Assets', ''),
  (319, 318, 306, 1, '4.5.1. Internal Systems', ''),
  (320, 318, 306, 1, '4.5.2. User IDs and Passwords', ''),
  (321, 318, 306, 1, '4.5.3. Electronic Messaging', ''),
  (322, 318, 306, 1, '4.5.4. Internet and Intranet', ''),
  (323, 318, 306, 1, '4.5.5. Personal Equipment', ''),
  (324, 318, 306, 1, '4.5.6. Telephones and Voice Mail', ''),
  (325, 318, 306, 1, '4.5.7. Secure Work Areas', ''),
  (326, 318, 306, 1, '4.5.8. Teleworking', ''),
  (327, 318, 306, 1, '4.5.9. Travel Controls', ''),
  (328, 306, 306, 1, '4.6. Asset Removal and Transfer', ''),
  (329, 328, 306, 1, '4.6.1. Removable Media', ''),
  (330, 328, 306, 1, '4.6.2. Property Removal', ''),
  (331, 306, 306, 1, '4.7. Asset Disposal', ''),
  (332, 331, 306, 1, '4.7.1. Equipment Disposal', ''),
  (333, 306, 306, 1, '4.8. Mobile Computing', ''),
  (334, 333, 306, 1, '4.8.1. Mobile Device Configuration', ''),
  (335, 333, 306, 1, '4.8.2. Mobile Device Protection', ''),
  (337, 0, 337, 1, '5. Information Management', ''),
  (338, 337, 337, 1, '5.1. Information Collection', ''),
  (339, 338, 337, 1, '5.1.1. Information Asset Inventory', ''),
  (340, 338, 337, 1, '5.1.2. Protection Requirements', ''),
  (341, 338, 337, 1, '5.1.3. Information Collection Practices', ''),
  (342, 337, 337, 1, '5.2. Information Classification and Labeling', ''),
  (343, 342, 337, 1, '5.2.1. Information Classification', ''),
  (344, 342, 337, 1, '5.2.2. Information Labeling', ''),
  (345, 337, 337, 1, '5.3. Information Exchange and Transit', ''),
  (346, 345, 337, 1, '5.3.1. Exchange Agreements', ''),
  (347, 345, 337, 1, '5.3.2. Electronic Transfer', ''),
  (348, 345, 337, 1, '5.3.3. Business Information Systems', ''),
  (349, 345, 337, 1, '5.3.4. Physical Transit', ''),
  (350, 345, 337, 1, '5.3.5. Third Party Disclosure', ''),
  (351, 345, 337, 1, '5.3.6. Verbal and Written Information', ''),
  (352, 337, 337, 1, '5.4. Information Storage and Retention', ''),
  (353, 352, 337, 1, '5.4.1. Storage Requirements', ''),
  (354, 352, 337, 1, '5.4.2. Information Handling ', ''),
  (355, 352, 337, 1, '5.4.3. Information Retention', ''),
  (356, 352, 337, 1, '5.4.4. Records Management', ''),
  (357, 337, 337, 1, '5.5. Information Disposal', ''),
  (358, 357, 337, 1, '5.5.1. Information Disposal', ''),
  (359, 0, 359, 1, '6 Third Party Management', ''),
  (360, 359, 359, 1, '6.1. Third Party Risk Management', ''),
  (361, 360, 359, 1, '6.1.1. Third Party Risk Assessments', ''),
  (362, 359, 359, 1, '6.2. Third Party Contracts', ''),
  (363, 362, 359, 1, '6.2.1. Security Specification', ''),
  (364, 362, 359, 1, '6.2.2. Contract Termination', ''),
  (365, 359, 359, 1, '6.3. Third Party Service Delivery', ''),
  (366, 365, 359, 1, '6.3.1. Third Party Security Requirements', ''),
  (367, 365, 359, 1, '6.3.2. Third Party Access Control', ''),
  (368, 365, 359, 1, '6.3.3. Third Party Monitoring', ''),
  (369, 0, 369, 1, '7. Personnel Security', ''),
  (370, 369, 369, 1, '7.1. Personnel Security Management', ''),
  (371, 370, 369, 1, '7.1.1. Personnel Roles and Responsibilities', ''),
  (372, 370, 369, 1, '7.1.2. Pre-Employment Screening', ''),
  (373, 370, 369, 1, '7.1.3. Terms and Conditions of Employment', ''),
  (374, 370, 369, 1, '7.1.4. Separation of Duties', ''),
  (375, 370, 369, 1, '7.1.5. Personnel Transfers', ''),
  (376, 370, 369, 1, '7.1.6. Personnel Terminations', ''),
  (377, 369, 369, 1, '7.2. Security Awareness and Training ', ''),
  (378, 377, 369, 1, '7.2.1. Information Security Training', ''),
  (379, 377, 369, 1, '7.2.2. Information Security Awareness', ''),
  (380, 0, 380, 1, '8. Access Control', ''),
  (381, 380, 380, 1, '8.1. Access Control Systems', ''),
  (382, 381, 380, 1, '8.1.1. Access Control Requirements', ''),
  (383, 381, 380, 1, '8.1.2. Password Configuration', ''),
  (384, 381, 380, 1, '8.1.3. Strong Authentication Methods', ''),
  (385, 381, 380, 1, '8.1.4. Session Controls', ''),
  (386, 381, 380, 1, '8.1.5. Electronic Signatures', ''),
  (387, 381, 380, 1, '8.1.6. Emergency Access', ''),
  (388, 380, 380, 1, '8.2. User Access Management', ''),
  (389, 388, 380, 1, '8.2.1. Access Authorization and Review', ''),
  (390, 388, 380, 1, '8.2.2. Privilege Assignment', ''),
  (391, 388, 380, 1, '8.2.3. User Password Management', ''),
  (392, 380, 380, 1, '8.3. User Account Management', ''),
  (393, 392, 380, 1, '8.3.1. Account Creation and Distribution ', ''),
  (394, 392, 380, 1, '8.3.2. Privileged Accounts', ''),
  (395, 380, 380, 1, '8.4. Remote Access and Mobile Computing', ''),
  (396, 395, 380, 1, '8.4.1. Servers', ''),
  (397, 395, 380, 1, '8.4.2. Access and Authentication', ''),
  (398, 395, 380, 1, '8.4.3. Remote Access Equipment', ''),
  (399, 0, 399, 1, '9. Network Security', ''),
  (400, 399, 399, 1, '9.1. Intrusion Protection', ''),
  (401, 400, 399, 1, '9.1.1. Intrusion Detection', ''),
  (402, 400, 399, 1, '9.1.2. Firewall Management', ''),
  (405, 400, 399, 1, '9.1.3. Routers and Appliances', ''),
  (406, 399, 399, 1, '9.2. Network Controls', ''),
  (407, 406, 399, 1, '9.2.1. Network Authorization', ''),
  (408, 406, 399, 1, '9.2.2. Network Configuration ', ''),
  (410, 406, 399, 1, '9.2.3. Network Connections', ''),
  (412, 406, 399, 1, '9.2.4. Network Diagrams', ''),
  (414, 406, 399, 1, '9.2.5. Network Access Control', ''),
  (416, 406, 399, 1, '9.2.6. Network Traffic Control', ''),
  (418, 406, 399, 1, '9.2.7. Network Management', ''),
  (419, 418, 399, 1, '9.2.8. Domain Management', ''),
  (420, 399, 399, 1, '9.3. Wireless Networks', ''),
  (421, 420, 399, 1, '9.3.1. Wireless Network Configuration', ''),
  (424, 420, 399, 1, '9.3.2. Wireless Network Management', ''),
  (425, 0, 425, 1, '10 Physical & Environmental Security', ''),
  (426, 425, 425, 1, '10.1. Physical Security Governance', ''),
  (427, 426, 425, 1, '10.1.1. Physical Security Planning', ''),
  (428, 425, 425, 1, '10.2. Site Security', ''),
  (429, 428, 425, 1, '10.2.1. Physical Security Perimeter', ''),
  (430, 428, 425, 1, '10.2.2. Physical Access Control', ''),
  (431, 428, 425, 1, '10.2.3. Employee Identification', ''),
  (432, 428, 425, 1, '10.2.4. Visitor Handling', ''),
  (433, 428, 425, 1, '10.2.5. Physical Access Log and Review', ''),
  (434, 428, 425, 1, '10.2.6. Physical Access Testing', ''),
  (435, 428, 425, 1, '10.2.7. Working in secure areas', ''),
  (436, 425, 425, 1, '10.3. Data Center Security', ''),
  (437, 436, 425, 1, '10.3.1. Data Center Access Control', ''),
  (438, 436, 425, 1, '10.3.2. Data Center Location', ''),
  (439, 436, 425, 1, '10.3.3. Data Center Structure', ''),
  (440, 436, 425, 1, '10.3.4. Environmental Controls', ''),
  (441, 436, 425, 1, '10.3.5. Cabling Security', ''),
  (442, 436, 425, 1, '10.3.6. Data Center Monitoring', ''),
  (444, 425, 425, 1, '10.4. Equipment Security', ''),
  (445, 444, 425, 1, '10.4.1. Physical Access Controls', ''),
  (446, 444, 425, 1, '10.4.2. Equipment Maintenance', ''),
  (447, 444, 425, 1, '10.4.3 Off-premises Security', ''),
  (448, 444, 425, 1, '10.4.4  Delivery and Loading Areas', ''),
  (449, 0, 449, 1, '11. Operations Management', ''),
  (450, 449, 449, 1, '11.1. Security Operations Management', ''),
  (451, 450, 449, 1, '11.1.1.Separation of Duties', ''),
  (452, 450, 449, 1, '11.1.2. Standard Operating Procedures', ''),
  (453, 449, 449, 1, '11.2. System Planning', ''),
  (454, 453, 449, 1, '11.2.1. Information Systems Acquisition', ''),
  (455, 453, 449, 1, '11.2.2. System Acceptance', ''),
  (456, 453, 449, 1, '11.2.3. Trustworthiness', ''),
  (457, 453, 449, 1, '11.2.4. Systems Lifecycle Support', ''),
  (458, 453, 449, 1, '11.2.5. System Documentation', ''),
  (459, 449, 449, 1, '11.3. Systems Management', ''),
  (460, 459, 449, 1, '11.3.1. Secure System Configuration', ''),
  (461, 459, 449, 1, '11.3.2. Patches and Updates', ''),
  (462, 459, 449, 1, '11.3.3. Vulnerability Management', ''),
  (463, 459, 449, 1, '11.3.4. Performance Management', ''),
  (464, 459, 449, 1, '11.3.5. Capacity Management', ''),
  (465, 459, 449, 1, '11.3.6. Software Use Restrictions', ''),
  (466, 459, 449, 1, '11.3.7. Security Status Monitoring', ''),
  (467, 449, 449, 1, '11.4. Change Management', ''),
  (468, 467, 449, 1, '11.4.1. Change Procedures', ''),
  (469, 467, 449, 1, '11.4.2 Change Testing', ''),
  (470, 449, 449, 1, '11.5. Malicious Software', ''),
  (471, 470, 449, 1, '11.5.1. Malicious Software Control', ''),
  (472, 470, 449, 1, '11.5.2. Mobile Code', ''),
  (473, 449, 449, 1, '11.6. Encryption', ''),
  (474, 473, 449, 1, '11.6.1. Encryption Systems and Standards', ''),
  (475, 473, 449, 1, '11.6.2. Encryption Key Management', ''),
  (476, 0, 476, 1, '12. Application Security Management', ''),
  (477, 476, 476, 1, '12.1. Application Development Security ', ''),
  (478, 477, 476, 1, '12.1.1. Proposals and Specifications', ''),
  (479, 477, 476, 1, '12.1.2. Application Procurement', ''),
  (480, 477, 476, 1, '12.1.3. Secure Coding', ''),
  (481, 477, 476, 1, '12.1.4. Application Testing', ''),
  (482, 477, 476, 1, '12.1.5. Application Security Controls', ''),
  (483, 477, 476, 1, '12.1.6. Documentation and Source Code', ''),
  (484, 477, 476, 1, '12.1.7. Outsourced Software Development', ''),
  (485, 477, 476, 1, '12.1.8. Production Migration', ''),
  (486, 476, 476, 1, '12.2. Transaction Controls', ''),
  (487, 486, 476, 1, '12.2.1. Input data', ''),
  (488, 486, 476, 1, '12.2.2. Internal processing', ''),
  (489, 486, 476, 1, '12.2.3. Message integrity', ''),
  (490, 486, 476, 1, '12.2.4. Output Data', ''),
  (491, 476, 476, 1, '12.3. Web Site Security', ''),
  (492, 491, 476, 1, '12.3.1. Web Application Security', ''),
  (493, 491, 476, 1, '12.3.2. Web Site Configuration', ''),
  (494, 491, 476, 1, '12.3.3. Electronic Transactions', ''),
  (495, 491, 476, 1, '12.3.4. Customer Security Requirements', ''),
  (496, 491, 476, 1, '12.3.5. Content Management', ''),
  (497, 0, 497, 1, '13. Incident Detection & Management', ''),
  (498, 497, 497, 1, '13.1. Security Incident Planning', ''),
  (499, 498, 497, 1, '13.1.1. Incident Response Plan', ''),
  (500, 498, 497, 1, '13.1.2. Incident Response Team', ''),
  (501, 497, 497, 1, '13.2. Security Incident Response', ''),
  (502, 501, 497, 1, '13.2.1. Incident Reporting and Management', ''),
  (503, 501, 497, 1, '13.2.2. Incident Response Procedures', ''),
  (504, 501, 497, 1, '13.2.3. Third Party Notification', ''),
  (505, 501, 497, 1, '13.2.4. Collection of Evidence', ''),
  (506, 501, 497, 1, '13.2.5. Investigation and Forensics', ''),
  (507, 501, 497, 1, '13.2.6. Security Incident Review', ''),
  (508, 501, 497, 1, '13.2.7. Contact with Authorities', ''),
  (509, 497, 497, 1, '13.3. Data Breach Management', ''),
  (510, 509, 497, 1, '13.3.1. Breach Response Planning', ''),
  (511, 509, 497, 1, '13.3.2. Data Breach Management', ''),
  (512, 0, 512, 1, '14. IT Business Continuity Planning', ''),
  (513, 512, 512, 1, '14.1. Information Backup', ''),
  (514, 513, 512, 1, '14.1.1. Backup Management', ''),
  (515, 513, 512, 1, '14.1.2. Backup Media', ''),
  (516, 512, 512, 1, '14.2. IT Business Continuity Governance', ''),
  (517, 516, 512, 1, '14.2.1. BCP Organization', ''),
  (518, 516, 512, 1, '14.2.2. Business Recovery Requirements', ''),
  (519, 516, 512, 1, '14.2.3. Business Impact Analysis (BIA)', ''),
  (520, 516, 512, 1, '14.2.4. Third Party BC Considerations', ''),
  (521, 512, 512, 1, '14.3. Business Continuity Planning', ''),
  (522, 521, 512, 1, '14.3.1. BC Plan Development', ''),
  (523, 521, 512, 1, '14.3.2. BC Plan Testing', ''),
  (524, 521, 512, 1, '14.3.3. BC Plan Maintenance', ''),
  (525, 0, 525, 1, '15. Security Monitoring and Audit', ''),
  (526, 525, 525, 1, '15.1. Information System Logs', ''),
  (527, 526, 525, 1, '15.1.1. System Log Composition', ''),
  (528, 526, 525, 1, '15.1.2. Log Event Monitoring', ''),
  (529, 526, 525, 1, '15.1.3. Log Management', ''),
  (530, 526, 525, 1, '15.1.4. System Log Security', ''),
  (531, 526, 525, 1, '15.1.5. Clock Synchronization', ''),
  (532, 525, 525, 1, '15.2. System Audit', ''),
  (533, 532, 525, 1, '15.2.1. Audit Planning and Scheduling', ''),
  (534, 532, 525, 1, '15.2.2. Control Testing', ''),
  (535, 532, 525, 1, '15.2.3. Audit Tools', ''),
  (536, 0, 536, 1, '16. Data Privacy and Personal Information', ''),
  (537, 536, 536, 1, '16.1. Employee Privacy', ''),
  (538, 537, 536, 1, '16.1.1. Employee Privacy Policy', ''),
  (539, 537, 536, 1, '16.1.2. Employee Privacy Notice', ''),
  (540, 536, 536, 1, '16.2. Customer Privacy', ''),
  (541, 540, 536, 1, '16.2.1. Customer Privacy Policies', ''),
  (542, 540, 536, 1, '16.2.2. Collection Limitation', ''),
  (543, 540, 536, 1, '16.2.3. Data Quality', ''),
  (544, 540, 536, 1, '16.2.4. Purpose Specification', ''),
  (545, 540, 536, 1, '16.2.5. Use Limitation', ''),
  (546, 540, 536, 1, '16.2.6. Security Safeguards', ''),
  (547, 540, 536, 1, '16.2.7. Openness', ''),
  (548, 540, 536, 1, '16.2.8. Individual Participation', ''),
  (549, 540, 536, 1, '16.2.9. Privacy Accountability', ''),
  (550, 540, 536, 1, '16.2.10. Free Flow of Information', ''),
  (551, 536, 536, 1, '16.3. Identity Theft Prevention', ''),
  (552, 551, 536, 1, '16.3.1. Identity Theft Prevention Planning', ''),
  (553, 551, 536, 1, '16.3.2. Identity Theft Procedures', ''),
  (554, 536, 536, 1, '16.4 Privacy Governance', ''),
  (555, 554, 536, 1, '16.4.1 Privacy Planning', ''),
  (556, 554, 536, 1, '16.4.2 Privacy Audit', ''),
  (557, 554, 536, 1, '16.4.3 Privacy Organization', '');

SET FOREIGN_KEY_CHECKS=1;
