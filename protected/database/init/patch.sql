

-- PATCH START

ALTER TABLE security_role DROP FOREIGN KEY fk_security_role_cat_id;
ALTER TABLE security_role drop column `cat_id`;
ALTER TABLE control_baseline CHANGE COLUMN cpl_level  `priority` tinyint(4) NOT NULL DEFAULT '1';
ALTER TABLE control_library CHANGE COLUMN cpl_level `priority` tinyint(4) NOT NULL DEFAULT '1';
ALTER TABLE control_baseline CHANGE COLUMN metric  `score` int(11) NOT NULL DEFAULT '1';
ALTER TABLE control_library CHANGE COLUMN metric `score` int(11) NOT NULL DEFAULT '1';
ALTER TABLE control_baseline ADD COLUMN `label1` varchar(255) DEFAULT NULL;
ALTER TABLE control_baseline ADD COLUMN `label2` varchar(255) DEFAULT NULL;
ALTER TABLE evidence add column `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE evidence  DROP FOREIGN KEY  fk_evidence_doc_type_id;
ALTER TABLE evidence drop column `doc_type_id`;
DROP TABLE IF EXISTS `doc_type`;
update evidence set created_on = NOW();
ALTER TABLE control_baseline drop column `jobrole`;
ALTER TABLE control_baseline ADD COLUMN `security_role_id` int(11) DEFAULT NULL;
ALTER TABLE control_baseline ADD CONSTRAINT `fk_control_baseline_security_role_id` FOREIGN KEY (`security_role_id`) REFERENCES `security_role` (`security_role_id`);
-- END PATCH

-- new patch 01-29-2016

ALTER TABLE policy CHANGE COLUMN `cat_id` `cat_id` int(11) DEFAULT NULL;
ALTER TABLE policy_library CHANGE COLUMN `cat_id` `cat_id` int(11) DEFAULT NULL;

-- end and applied

-- new patch 01-31-2016

ALTER TABLE user_profile drop column `doc_list`;
ALTER TABLE user_profile drop column `quiz_list`;
ALTER TABLE user_profile drop column `training_list`;

-- end applied

-- new patch 02-05-2016
ALTER TABLE security_role CHANGE COLUMN `company_profile_id` `company_profile_id` int(11) DEFAULT NULL;

INSERT INTO `security_role` (`security_role_id`, `company_profile_id`, `company_user_id`, `role_status`, `role_title`, `role_description`, `role_department`, `role_summary`, `status`, `created_on`)
VALUES
  (1, NULL, NULL, 0, 'Information Technology Manager', '<pre style=\"font-family: Menlo; font-size: 9pt;\">The Chief Technology Officer is an executive who is focused on the technological and scientific issues of the organization. He or she is concerned with the big-picture, long-term, strategic use of technology and science. The CTO is responsible for developing a vision for the deployment of new technologies, and for adapting existing technologies in new ways, so as to serve Company X&rsquo;s business purposes. He or she is thus responsible for the development of, protection of, extending the useful life of intellectual property such as copyrights, patents, trade secrets, and license contracts. [Note that the CIO is focused on the use of technology for running the organization and thereby achieving strategic advantage, while the CTO is focused on the incorporation of technology into products and services being sold to customers.]</pre>', '', '', 0, '2015-12-20 20:23:51'),
  (2, NULL, NULL, 0, 'Human Resources Manager', '<pre style=\"font-family: Menlo; font-size: 9pt;\">The Human Resources Manager is an executive who is focused on managing relations with employees, including compliance with employment laws and regulations.  The Human Resources Manager must be involved whenever the information security program addresses issues related to employment, including screening, hiring, management and termination of employment.<br /><br />Working in conjunction with other members of the top management team, defines and periodically revises a code of conduct for all workers [This code often addresses a number of information security issues such as use of Company X information for personal purposes, right to know certain information (such as workplace hazards), and freedom of speech. Alternatively, an Internal Control Committee may develop a code of conduct (see Chapter 11 for further details). If this Manager doesn\'t perform this job, it may be done by an Ethics Officer.]  Establishes, documents, and supervises the equitable application of a disciplinary process that has progressively more severe penalties for those who are not in compliance with internal policies, standards, and other requirements [This process can and should be used for information security policies, standards, and other requirements.] Provides top management with observations about and suggestions for the evolution of organizational culture [This area is sometimes called corporate culture rather than organizational culture; this task includes some information security issues such as the openness with which information flows between individuals.]</pre>', '', '', 0, '2015-12-22 00:13:47'),
  (3, NULL, NULL, 0, 'Information Security Manager', '<pre style=\"font-family: Menlo; font-size: 9pt;\">The Information Security Manager is the top security officer within Company X. He or she directs, plans, coordinates, and orchestrates all the security activities within Company X, as well as Company X security-related activities with outside organizations such as business partners, standards setting bodies, and industry associations. These security activities include contingency planning, information security, loss prevention, building security, investigations, fraud prevention, privacy, plus insurance &amp; risk management. This Officer is charged with setting a cooperative tone for, establishing a proactive and responsive organizational culture for, and defining the strategic direction of security-related units throughout the organization.</pre>\r\n<pre style=\"font-family: Menlo; font-size: 9pt;\">The Information Security Manager is responsible for overseeing the work performed by security-related departments including Insurance &amp; Risk Management, Contingency Planning, Information Security, Physical Security, and Safety. He or she is responsible for ensuring that Company X business goals and objectives are integrated with internal and external security related activities. This officer is also responsible for synergistically combining the work of different security-related units so that business activities throughout Company X are supported, accelerated, advanced, at the same time striking and maintaining an appropriate balance of competing objectives such as efficiency, effectiveness, reliability, resilience, flexibility, accountability, and transparency.</pre>', '', '', 0, '2015-12-22 00:14:18');
-- end applied

-- new patch 02-07-2016

INSERT INTO `company_role` (`company_role_id`, `company_role_name`)
VALUES
  (100,'User'),
  (200,'Auditor'),
  (300,'Author'),
  (400,'Administrator'),
  (500,'Owner');

UPDATE company_user SET company_role_id=100 WHERE company_role_id=99;
UPDATE company_user SET company_role_id=200 WHERE company_role_id=4;
UPDATE company_user SET company_role_id=300 WHERE company_role_id=3;
UPDATE company_user SET company_role_id=400 WHERE company_role_id=2;
UPDATE company_user SET company_role_id=500 WHERE company_role_id=1;

DELETE FROM company_role WHERE company_role_id < 100;

ALTER TABLE company_user CHANGE COLUMN `company_role_id` `company_role_id` int(11) NOT NULL DEFAULT 100;

DELETE FROM yiisession;

-- end applied

-- new patch 03-01-2016

ALTER TABLE control_baseline ADD COLUMN `sort_order` int(11) NOT NULL DEFAULT '1' AFTER cat_sub_code;
ALTER TABLE control_library ADD COLUMN `sort_order` int(11) NOT NULL DEFAULT '1' AFTER cat_sub_code;

-- end applied

-- new patch 04-08-2016

DROP TABLE IF EXISTS `invite`;
CREATE TABLE `invite` (
  `invite_id` int(11) NOT NULL AUTO_INCREMENT,
  `invite_guid` varchar(36) DEFAULT NULL,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `invited_company_profile_id` int(11) DEFAULT NULL,
  `invited_user_id` int(11) DEFAULT NULL,
  `baseline_id` int(11) DEFAULT NULL,
  `sent` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 is not sent yet, 1 is sent, 2 is failed sent',
  `accepted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 is not accepted yet, 1 is accepted, 2 is rejected',
  `adopted_baseline` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 is not adopted yet, 1 is adopted, 2 is rejected',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sent_date` DATE DEFAULT NULL,
  `expiry_date` DATE DEFAULT NULL,
  `accepted_date` DATE DEFAULT NULL,
  `invitee_name` varchar(256) NOT NULL,
  `invitee_email` varchar(256) NOT NULL,
  PRIMARY KEY (`invite_id`),
  UNIQUE KEY `invite_invite_guid` (`invite_guid`),
  KEY (`user_id`),
  CONSTRAINT `fk_invite_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_invite_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`invited_user_id`),
  CONSTRAINT `fk_invite_invited_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`invited_company_profile_id`),
  CONSTRAINT `fk_invite_invited_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `partner`;
CREATE TABLE `partner` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` varchar(128) NOT NULL DEFAULT '',
  `shared_secret` varchar(32) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`partner_id`),
  UNIQUE KEY `shared_secret` (`shared_secret`),
  UNIQUE KEY `partner_name` (`partner_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `partner` (`partner_id`, `partner_name`, `shared_secret`, `status`)
VALUES
  (1, 'eRiskHub', '#Qd\'wK]-Hj-7V]wc', 1);

ALTER TABLE tbl_users ADD COLUMN `partner_id` int(11) DEFAULT NULL AFTER status;
ALTER TABLE tbl_users ADD CONSTRAINT `fk_tbl_users_partner_id` FOREIGN KEY (`partner_id`) REFERENCES `partner` (`partner_id`);

DROP TABLE IF EXISTS `baseline`;
CREATE TABLE `baseline` (
  `baseline_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=hidden, 1=active',
  `sort_order` int(11) DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `baseline_name` varchar(255) NOT NULL,
  `baseline_description` text DEFAULT NULL,
  PRIMARY KEY (`baseline_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_baseline_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_baseline_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE control_baseline ADD COLUMN `baseline_id` int(11) DEFAULT NULL AFTER user_id;
ALTER TABLE control_baseline ADD CONSTRAINT `fk_control_baseline_baseline_id` FOREIGN KEY (`baseline_id`) REFERENCES `baseline` (`baseline_id`);

INSERT INTO baseline (baseline_id, company_profile_id, user_id, baseline_name, baseline_description) VALUES (1, NULL, NULL, 'CPL Assessment Baseline', 'CPL Assessment Baseline for the organization.');

ALTER TABLE control_baseline CHANGE COLUMN `company_profile_id` `company_profile_id` int(11) DEFAULT NULL;
ALTER TABLE control_baseline CHANGE COLUMN `user_id` `user_id` int(11) DEFAULT NULL;

INSERT INTO control_baseline (baseline_id,control_id,cat_id,cat_code,cat_sub_code,sort_order,priority,weighting,`status`,title,control_status_id,score,evidence,rel_policy,detail,guidance,reference,question)
SELECT 1 as baseline_id,control_id,cat_id,cat_code,cat_sub_code,sort_order,priority,weighting,`status`,title,control_status_id,score,evidence,rel_policy,detail,guidance,reference,question FROM control_library WHERE priority=1;

UPDATE `version` SET `Build`=2016040901, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied

-- new patch 05/05/2016

INSERT INTO `license_level` (`license_level_id`, `license_level_name`)
VALUES
  (10, 'Demo');
-- end applied


-- new patch 05/06/2016

DROP TABLE IF EXISTS `export_report`;
CREATE TABLE `export_report` (
  `export_report_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_type` varchar(32) NOT NULL,
  `file_ext` varchar(10) DEFAULT NULL,
  `file_type` varchar(127) DEFAULT NULL,
  `file_name` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `data`  LONGTEXT,
  `report` LONGTEXT,
  PRIMARY KEY (`export_report_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_report_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_report_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

UPDATE `version` SET `Build`=2016050701, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied

-- new patch 05/08/2016
-- support multiple company baselines and give all a name including default and be able to switch baselines
INSERT INTO baseline (company_profile_id, user_id, `status`, created_on, baseline_name, baseline_description)
  SELECT company_profile_id, user_id, `status`, created_on, 'Default' as baseline_name, 'Default baseline for the organization.' as baseline_description FROM company_profile;

UPDATE control_baseline c, baseline b SET c.baseline_id = b.baseline_id WHERE c.company_profile_id=b.company_profile_id;

UPDATE `version` SET `Build`=2016050801, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied

-- new patch 06/09/2016
-- update export_report table report field to handle word document content
ALTER TABLE  `export_report` CHANGE  `report`  `report` BLOB NULL DEFAULT NULL ;
-- every new patch should update the Build value so we know when last patch applied to the database
-- Build date format YYYYMMDDxx, where xx starts 01 for first patch of the day, 02 and on if additional patches applied on same day
UPDATE `version` SET `Build`=2016061201, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied


-- new patch 06-13-2016
ALTER TABLE control_status ADD COLUMN `sort_order` int(11) NOT NULL DEFAULT '0' AFTER control_status_name;
UPDATE `control_status` SET `sort_order`=0 WHERE `control_status_id`=0;
UPDATE `control_status` SET `sort_order`=1 WHERE `control_status_id`=1;
UPDATE `control_status` SET `sort_order`=2 WHERE `control_status_id`=2;
UPDATE `control_status` SET `sort_order`=4 WHERE `control_status_id`=3;
UPDATE `control_status` SET `sort_order`=5 WHERE `control_status_id`=4;
UPDATE `control_status` SET `sort_order`=3 WHERE `control_status_id`=5;
UPDATE `version` SET `Build`=2016061301, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied

-- new patch 06-21-2016
ALTER TABLE evidence CHANGE control_baseline_id control_baseline_id int(11) DEFAULT NULL COMMENT 'related control';
UPDATE `version` SET `Build`=2016062101, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied

-- new patch 06-30-2016
INSERT INTO `training_library` (`training_library_id`, `training_type`, `approved`, `training_status_id`, `status`, `file_size`, `file_ext`, `file_type`, `file_name`, `doc_guid`, `doc_title`, `created_on`, `publish_date`, `author`, `description`)
VALUES
  (4, 2, 1, 3, 1, 0, NULL, NULL, NULL, '02dd6db8-653c-8ed3-75ce-4aa4d796db67', 'PISC V2 Enterprise Training - 2016', '2016-06-30 13:28:20', '2016-06-30', 'Information Shield', '<p>Information Security Awareness Training vidoes for all computer users.&nbsp; PISC V2 2016.</p>');

INSERT INTO `video_training` (`training_library_id`, `training_id`, `company_profile_id`, `user_id`, `status`, `created_on`, `sort_order`, `video_title`, `video_code`)
VALUES
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 1, 'Introduction', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/cypw7ljzy1\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 2, '1. Your Role in Information Security', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/9oy8ftlakx\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 3, '2. Information Classification', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/z1b23hv0qx\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 4, '3. Access Control', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/ivm9u3pqfe\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 5, '4. Electronic Mail', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/fmt57gd187\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 6, '5. Internet and Web Use', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/10b5ksjt82\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 7, '6. Personal Computers', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/1viq1h9kar\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 8, '7. Physical Security', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/wyiyw734sn\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 9, '8. Secure Communications', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/4o1qxrfgvm\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 10, '9. Information Security Policies', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/4gbkplkyco\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>'),
  (4, NULL, NULL, 1, 1, '2016-06-30 13:32:23', 11, '10. Reporting Security Problems', '<iframe width=\"500\" height=\"281\" src=\"//fast.wistia.net/embed/iframe/n51rnmvtoc\" allowtransparency=\"true\" frameborder=\"0\" scrolling=\"no\" class=\"wistia_embed\" name=\"wistia_embed\" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen></iframe>');

DROP TABLE IF EXISTS `usersetting`;
CREATE TABLE `usersetting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key_name` varchar(40) NOT NULL,
  `key_value` text DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY (`user_id`),
  CONSTRAINT `fk_usersetting_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- end applied

-- new patch 07-05-2016
ALTER TABLE  `control_status` ADD  `status_control_desc` VARCHAR( 100 ) NOT NULL AFTER  `control_status_name` ;
UPDATE  `control_status` SET  `status_control_desc` =  'No - We haven\’t done that yet' WHERE  `control_status`.`control_status_id` =0;
UPDATE  `control_status` SET  `status_control_desc` =  'Planned - We\’ve got a plan for it' WHERE  `control_status`.`control_status_id` =1;
UPDATE  `control_status` SET  `status_control_desc` =  'In progress - We\’re working on it' WHERE  `control_status`.`control_status_id` =2;
UPDATE  `control_status` SET  `status_control_desc` =  'Tested - We\’ve implemented it and tested' WHERE  `control_status`.`control_status_id` =3;
UPDATE  `control_status` SET  `status_control_desc` =  'Validated - We\’ve had a third party verify this is implemented correctly' WHERE  `control_status`.`control_status_id` =4;
UPDATE  `control_status` SET  `status_control_desc` =  'Complete - Yes, we have that fully implemented' WHERE  `control_status`.`control_status_id` = 5;

ALTER TABLE  `control_baseline` ADD  `assessed_on` TIMESTAMP NULL DEFAULT NULL AFTER  `created_on` ;

UPDATE `version` SET `Build`=2016072001, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied

-- new patch 07-28-2016
UPDATE  `control_status` SET  `status_control_desc` =  'Not Applicable – The control is not yet part of our security program.' WHERE  `control_status`.`control_status_id` =0;
UPDATE  `control_status` SET  `status_control_desc` =  'Planned – The control is planned but not yet started' WHERE  `control_status`.`control_status_id` =1;
UPDATE  `control_status` SET  `status_control_desc` =  'In progress – The control is partially implemented.' WHERE  `control_status`.`control_status_id` =2;
UPDATE  `control_status` SET  `status_control_desc` =  'Complete - The control fully implemented ' WHERE  `control_status`.`control_status_id` =5;
UPDATE  `control_status` SET  `status_control_desc` =  'Tested – The Control is fully implemented and tested ' WHERE  `control_status`.`control_status_id` =3;
UPDATE  `control_status` SET  `status_control_desc` =  'Validated - A third party has verified the control implementation ' WHERE  `control_status`.`control_status_id` =4;

UPDATE `version` SET `Build`=2016081001, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied

-- new patch 08-03-2016
ALTER TABLE `control_baseline` CHANGE `evidence` `evidence` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;
ALTER TABLE `control_baseline` ADD `updated_on` TIMESTAMP NULL;
ALTER TABLE `control_baseline` ADD `updated_user_id` INT NULL;
ALTER TABLE `control_baseline` ADD INDEX(`updated_user_id`);
ALTER TABLE `control_baseline` ADD CONSTRAINT `fix_control_baseline_updated_user_id` FOREIGN KEY (`updated_user_id`) REFERENCES `tbl_users`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

UPDATE `version` SET `Build`=2016080401, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied

-- patch 08-07-2016
ALTER TABLE `tbl_users` ADD  `eula` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 is un-read, 1 is read';

UPDATE `version` SET `Build`=2016080701, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied


-- new patch 08-17-2016
DROP TABLE IF EXISTS `inherent_risk_score`;
CREATE TABLE `inherent_risk_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answers` text NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `assessed_on` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `company_profile_id` (`company_profile_id`),
CONSTRAINT `fk_inherent_risk_score_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile`(`company_profile_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  KEY `user_id` (`user_id`),
CONSTRAINT `fk_inherent_risk_score_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

UPDATE `version` SET `Build`=2016081802, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied

-- new patch 09-05-2016
ALTER TABLE `company_info`
  DROP `international_personal_data`,
  DROP `num_employees_access_sensitive_data`,
  DROP `local_consumer_datastore`,
  DROP `primary_work_location`,
  DROP `sensitive_paper_records`;

ALTER TABLE `company_info`
  ADD `num_business_locations` VARCHAR(10) NULL COMMENT '[0 (None, Virtual), 1 (Single), 2-10 (Small), 11-100 (Medium), 100+ (Large)' AFTER `country`,
  ADD `wireless_networks` TINYINT NULL DEFAULT NULL COMMENT '[0 (None), 1 (Single Network, Limited Access Points, 2 (Single Network (More than 2 access Points)), 3 (Multiple (More than 5 access Points)), 4 (Multiple (More than 20 access points))] If [QD=11] > 0, then Control ID [] = Planned';

ALTER TABLE `company_info` CHANGE `num_employees` `num_employees` VARCHAR(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '[1-50, 51-500, 501-5000, 5000+]',
  CHANGE `highly_sensitive_data` `highly_sensitive_data` TINYINT(1) NULL DEFAULT NULL COMMENT '[0 (None - We do not process sensitive data), 1 (Single - Single Data Type (No PII – Personally Identifiable Information)), 2 (Single PII - Single Type of PII  (One primary PII)), 3 (Some - Multiple Data Types (Still No PII)), 4 (All - Multiple Types (both PII and non PII)] If [QID=1] > 0, then Control ID [] = Planned',
  CHANGE `individual_consumer_data` `individual_consumer_data` TINYINT(1) NULL DEFAULT NULL COMMENT '0 (None), 1 (Less than 500), 2 (500 to 5000), 3 (5000 to 50,000), 4(Greater than 50,000)] If [QID=2] > 0, then Control ID [312, 313, 315, 316, 317] = Planned',
  CHANGE `employee_mobile_customer_data` `employee_mobile_customer_data` TINYINT(1) NULL DEFAULT NULL COMMENT '[0 (None), 1 (Limited (< 5% of workforce)), 2(Some (5 - 25% of workforce)), 3 (Moderate (25 - 50% of workforce)), 4 (Most ( > 50% of workforce))] If [QD=5] > 0, then Control ID [222,224] = Planned',
  CHANGE `remote_network_access` `remote_network_access` TINYINT(1) NULL DEFAULT NULL COMMENT '[0 (None - No Remote Access), 1 (Limited (1 - 5) organizations can access our network), 2(Some (6 - 10) organizations can access our network), 3 (Moderate (11-50) organizations can access our network), 4 (Most ( > 50 third parties))] If [QD=6] > 0, then Control ID [222,224] = Planned',
  CHANGE `third_party_data` `third_party_data` TINYINT(1) NULL DEFAULT NULL COMMENT '[0 (None), 1 (Single Primary), 2(Minimal (2 - 25)), 3 (Moderate (26 - 100)), 4 (High (> 100))] If [QD=7] is yes, then Control ID [179,180, 181] = Planned',
  CHANGE `local_it_datacenters` `local_it_datacenters` TINYINT(1) NULL DEFAULT NULL COMMENT '[0 (None), 1 (Single System), 2 (Minimal (2 - 10)), 3 (Moderate (20 - 100)), 4 (High (100 - 200))] if [QID=8] {}, then Control ID [] = Planned',
  CHANGE `software_development` `internally_developed_applications` TINYINT(1) NULL DEFAULT NULL COMMENT '[0 (None), 1 (Single Primary), 2 (Minimal (2 - 15)), 3 (Moderate (16 - 50)), 4 (High (50+))] If [QD=9] {}, then Control ID [268, 270, 271, 272, 273] = Planned',
  CHANGE `online_customer_transactions` `online_customer_transactions` TINYINT(1) NULL DEFAULT NULL COMMENT '[0 (Not Critical (Our site is information only)), 1 (Minimal (We provide only basic services)), 2 (Moderate (Less than 20% of our sales or leads are from our website)), 3 (Important (More than 50% of our sales or leads are generated through our website)), 4 (Critical (It is the sole delivery channel for our business))] If [QD=10] > 0, then Control ID [274, 275] = Planned';

UPDATE `version` SET `Build`=2016091101, `UpdatedOn`=NOW() WHERE Id=1;
-- end applied