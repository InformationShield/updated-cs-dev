# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.42)
# Database: easysectyny
# Generation Time: 2015-11-21 06:24:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_migration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_migration`;

CREATE TABLE `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;

INSERT INTO `tbl_migration` (`version`, `apply_time`)
VALUES
  ('m000000_000000_base',1447964427),
  ('m110805_153437_installYiiUser',1447964569),
  ('m110810_162301_userTimestampFix',1447964569);

/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_profiles`;

CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tbl_profiles` WRITE;
/*!40000 ALTER TABLE `tbl_profiles` DISABLE KEYS */;

INSERT INTO `tbl_profiles` (`user_id`, `first_name`, `last_name`)
VALUES
  (1,'Administrator','Admin');

/*!40000 ALTER TABLE `tbl_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_profiles_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_profiles_fields`;

CREATE TABLE `tbl_profiles_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` text,
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` text,
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tbl_profiles_fields` WRITE;
/*!40000 ALTER TABLE `tbl_profiles_fields` DISABLE KEYS */;

INSERT INTO `tbl_profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`)
VALUES
  (1,'first_name','First Name','VARCHAR',255,3,2,'','','Incorrect First Name (length between 3 and 50 characters).','','','','',1,3),
  (2,'last_name','Last Name','VARCHAR',255,3,2,'','','Incorrect Last Name (length between 3 and 50 characters).','','','','',2,3);

/*!40000 ALTER TABLE `tbl_profiles_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Partners Table
# ------------------------------------------------------------
DROP TABLE IF EXISTS `partner`;
CREATE TABLE `partner` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` varchar(128) NOT NULL DEFAULT '',
  `shared_secret` varchar(32) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`partner_id`),
  UNIQUE KEY `shared_secret` (`shared_secret`),
  UNIQUE KEY `partner_name` (`partner_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `partner` (`partner_id`, `partner_name`, `shared_secret`, `status`)
VALUES
  (1, 'eRiskHub', '#Qd\'wK]-Hj-7V]wc', 1);

# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `partner_id` int(11) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `eula` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 is un-read, 1 is read',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username` (`username`),
  UNIQUE KEY `user_email` (`email`),
  CONSTRAINT `fk_tbl_users_partner_id` FOREIGN KEY (`partner_id`) REFERENCES `partner` (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `activkey`, `superuser`, `status`, `create_at`, `lastvisit_at`, `eula`)
VALUES
  (1,'admin','$2a$07$$$$$$$$$$$$$$$$$$$$$$.1qlC0.QUv.oMycMPUoyPlr0lvjAZLQu','earl@tynylabs.com','3883af6627a26dc91313825641419316d453b1a6',1,1,'2015-11-19 14:22:49','2015-11-21 00:20:47',1);

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;

#
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usersetting`;

CREATE TABLE `usersetting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key_name` varchar(40) NOT NULL,
  `key_value` text DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY (`user_id`),
  CONSTRAINT `fk_usersetting_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# Dump of table yiisession
# ------------------------------------------------------------

DROP TABLE IF EXISTS `yiisession`;

CREATE TABLE `yiisession` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  KEY `i_expire` (`expire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# License Level table
#---------------------------------------------------------------

DROP TABLE IF EXISTS `license_level`;
CREATE TABLE `license_level` (
  `license_level_id` int(11) NOT NULL,
  `license_level_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`license_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `license_level` (`license_level_id`, `license_level_name`)
VALUES
  (1,'Free'),
  (10,'Demo'),
  (100,'Paid Basic'),
  (200,'Company Level'),
  (1000,'Ultimate Subscription');

# company_role table
#---------------------------------------------------------------

DROP TABLE IF EXISTS `company_role`;
CREATE TABLE `company_role` (
  `company_role_id` int(11) NOT NULL,
  `company_role_name` varchar(32) NOT NULL,
  `company_role_description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`company_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `company_role` (`company_role_id`, `company_role_name`)
VALUES
  (100,'User'),
  (200,'Auditor'),
  (300,'Author'),
  (400,'Administrator'),
  (500,'Owner');

# security_role_status table
#---------------------------------------------------------------

DROP TABLE IF EXISTS `security_role_status`;
CREATE TABLE `security_role_status` (
  `role_status_id` TINYINT(4) NOT NULL,
  `role_status_name` varchar(32) NOT NULL,
  PRIMARY KEY (`role_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `security_role_status` (`role_status_id`, `role_status_name`)
VALUES
  (0,'unassigned'),
  (1,'assigned'),
  (2,'open');

# control_status table
#---------------------------------------------------------------

DROP TABLE IF EXISTS `control_status`;
CREATE TABLE `control_status` (
  `control_status_id` int(11) NOT NULL,
  `control_status_name` varchar(32) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`control_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `control_status` (`control_status_id`, `control_status_name`, `sort_order`)
VALUES
  (0,'Not Active', 0),
  (1,'Planned', 1),
  (2,'In Progress', 2),
  (3,'Verified', 4),
  (4,'Validated', 5),
  (5,'Complete', 3);

# company tables
#---------------------------------------------------------------

DROP TABLE IF EXISTS `company_profile`;
CREATE TABLE `company_profile` (
  `company_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_guid` varchar(36) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `logo` varchar(60) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `license_level` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(32) NOT NULL,
  `policy_contactname` varchar(200) DEFAULT NULL,
  `policy_email` varchar(256) DEFAULT NULL,
  `incident_contactname` varchar(200) DEFAULT NULL,
  `incident_email` varchar(256) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `industry` varchar(60) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_company_profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  CONSTRAINT fk_company_profile_license_level FOREIGN KEY (license_level) REFERENCES license_level(license_level_id),
  CONSTRAINT ui_company_profile_name UNIQUE (name)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `company_user`;
CREATE TABLE `company_user` (
  `company_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) NOT NULL,
  `company_role_id` int(11) NOT NULL DEFAULT 100,
  `email` varchar(256) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `department_name` varchar(100) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `profile_id` int(11) NOT NULL DEFAULT 0,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`company_user_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_company_user_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`company_role_id`),
  CONSTRAINT `fk_company_user_company_role_id` FOREIGN KEY (`company_role_id`) REFERENCES `company_role` (`company_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `security_role`;
CREATE TABLE `security_role` (
  `security_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) DEFAULT NULL,
  `company_user_id` int(11) DEFAULT NULL,
  `role_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=unassigned, 1=assigned, 2=open',
  `role_title` varchar(100) NOT NULL,
  `role_description` text DEFAULT NULL,
  `role_department` varchar(100) DEFAULT NULL,
  `role_summary` varchar(100) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`security_role_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_security_role_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`company_user_id`),
  CONSTRAINT `fk_security_role_company_user_id` FOREIGN KEY (`company_user_id`) REFERENCES `company_user` (`company_user_id`),
  KEY (`role_status`),
  CONSTRAINT `fk_security_role_role_status` FOREIGN KEY (`role_status`) REFERENCES `security_role_status` (`role_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `user_profile`;
CREATE TABLE `user_profile` (
  `user_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL DEFAULT 0,
  `profile_title` varchar(100) NOT NULL,
  `profile_description` text  DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_profile_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_user_profile_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# 11-21-2015
#-------------------------------------------------------------
DROP TABLE IF EXISTS `control_category`;
CREATE TABLE `control_category` (
  `control_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `category` varchar(100) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`control_category_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `control_library`;
CREATE TABLE `control_library` (
  `control_library_id` int(11) NOT NULL AUTO_INCREMENT,
  `control_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `cat_code` char(2) NOT NULL,
  `cat_sub_code` char(5) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  `priority` tinyint(4) NOT NULL DEFAULT '1',
  `weighting` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) NOT NULL,
  `control_status_id` int(11) NOT NULL DEFAULT '0',
  `jobrole` varchar(100) NOT NULL,
  `score` int(11) NOT NULL DEFAULT '1',
  `evidence` text DEFAULT NULL,
  `rel_policy` text DEFAULT NULL,
  `detail` text DEFAULT NULL,
  `guidance` text DEFAULT NULL,
  `reference` text DEFAULT NULL,
  `question` text DEFAULT NULL,
  PRIMARY KEY (`control_library_id`),
  KEY `control_status_id` (`control_status_id`),
  CONSTRAINT `fk_control_library_control_status_id` FOREIGN KEY (`control_status_id`) REFERENCES `control_status` (`control_status_id`),
  KEY `cat_id` (`cat_id`),
  CONSTRAINT `fk_control_library_cat_id` FOREIGN KEY (`cat_id`) REFERENCES `control_category` (`control_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


# baseline table
#-----------------------------------------------------------------
DROP TABLE IF EXISTS `baseline`;
CREATE TABLE `baseline` (
  `baseline_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=hidden, 1=active',
  `sort_order` int(11) DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `baseline_name` varchar(255) NOT NULL,
  `baseline_description` text DEFAULT NULL,
  PRIMARY KEY (`baseline_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_baseline_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_baseline_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `control_baseline`;
CREATE TABLE `control_baseline` (
  `control_baseline_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `baseline_id` int(11) DEFAULT NULL,
  `control_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `cat_id` int(11) NOT NULL,
  `cat_code` char(2) NOT NULL,
  `cat_sub_code` char(5) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  `priority` tinyint(4) NOT NULL DEFAULT '1',
  `weighting` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `target_date` DATE DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `control_status_id` int(11) NOT NULL DEFAULT '0',
  `security_role_id` int(11) DEFAULT NULL,
  `score` int(11) NOT NULL DEFAULT '1',
  `evidence` text DEFAULT NULL,
  `rel_policy` text DEFAULT NULL,
  `detail` text DEFAULT NULL,
  `guidance` text DEFAULT NULL,
  `reference` text DEFAULT NULL,
  `question` text DEFAULT NULL,
  `label1` varchar(255) DEFAULT NULL,
  `label2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`control_baseline_id`),
  KEY `cat_id` (`cat_id`),
  CONSTRAINT `fk_control_baseline_cat_id` FOREIGN KEY (`cat_id`) REFERENCES `control_category` (`control_category_id`),
  KEY `cat_id` (`control_status_id`),
  CONSTRAINT `fk_control_baseline_control_status_id` FOREIGN KEY (`control_status_id`) REFERENCES `control_status` (`control_status_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_control_baseline_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`security_role_id`),
  CONSTRAINT `fk_control_baseline_security_role_id` FOREIGN KEY (`security_role_id`) REFERENCES `security_role` (`security_role_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_control_baseline_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`baseline_id`),
  CONSTRAINT `fk_control_baseline_baseline_id` FOREIGN KEY (`baseline_id`) REFERENCES `baseline` (`baseline_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# evidence table
#-----------------------------------------------------------------
DROP TABLE IF EXISTS `evidence`;
CREATE TABLE `evidence` (
  `evidence_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `control_baseline_id` int(11) NOT NULL,
  `file_size` int(11) NOT NULL DEFAULT '0',
  `file_ext` varchar(10) DEFAULT NULL,
  `file_type` varchar(127) DEFAULT NULL,
  `file_name` varchar(256) NOT NULL,
  `doc_guid` varchar(36) NOT NULL,
  `doc_title` varchar(256) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`evidence_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_evidence_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_evidence_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`control_baseline_id`),
  CONSTRAINT `fk_evidence_control_baseline_id` FOREIGN KEY (`control_baseline_id`) REFERENCES `control_baseline` (`control_baseline_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# policy_status table
#---------------------------------------------------------------
DROP TABLE IF EXISTS `policy_status`;
CREATE TABLE `policy_status` (
  `policy_status_id` TINYINT(4) NOT NULL,
  `policy_status_name` varchar(32) NOT NULL,
  PRIMARY KEY (`policy_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `policy_status` (`policy_status_id`, `policy_status_name`)
VALUES
  (1,'Draft'),
  (2,'Review'),
  (3,'Published'),
  (4,'Archive');

DROP TABLE IF EXISTS `policy_library`;
CREATE TABLE `policy_library` (
  `policy_library_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 is un-approved, 1 is approved',
  `policy_status_id` TINYINT(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `file_size` int(11) NOT NULL DEFAULT '0',
  `file_ext` varchar(10) DEFAULT NULL,
  `file_type` varchar(127) DEFAULT NULL,
  `file_name` varchar(256) NOT NULL,
  `doc_guid` varchar(36) NOT NULL,
  `doc_title` varchar(256) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_date` DATE DEFAULT NULL,
  `expiry_date` DATE DEFAULT NULL,
  `author` varchar(256) DEFAULT NULL,
  `keywords` varchar(2000) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`policy_library_id`),
  KEY (`cat_id`),
  CONSTRAINT `fk_policy_library_cat_id` FOREIGN KEY (`cat_id`) REFERENCES `control_category` (`control_category_id`),
  KEY (`policy_status_id`),
  CONSTRAINT `fk_policy_library_policy_status_id` FOREIGN KEY (`policy_status_id`) REFERENCES `policy_status` (`policy_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `policy`;
CREATE TABLE `policy` (
  `policy_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `policy_library_id` int(11) DEFAULT NULL COMMENT 'original id if copied from policy library',
  `cat_id` int(11) DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 is un-approved, 1 is approved',
  `policy_status_id` TINYINT(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `file_size` int(11) NOT NULL DEFAULT '0',
  `file_ext` varchar(10) DEFAULT NULL,
  `file_type` varchar(127) DEFAULT NULL,
  `file_name` varchar(256) NOT NULL,
  `doc_guid` varchar(36) NOT NULL,
  `doc_title` varchar(256) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_date` DATE DEFAULT NULL,
  `expiry_date` DATE DEFAULT NULL,
  `author` varchar(256) DEFAULT NULL,
  `keywords` varchar(2000) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`policy_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_policy_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_policy_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`cat_id`),
  CONSTRAINT `fk_policy_cat_id` FOREIGN KEY (`cat_id`) REFERENCES `control_category` (`control_category_id`),
  KEY (`policy_library_id`),
  CONSTRAINT `fk_policy_policy_library_id` FOREIGN KEY (`policy_library_id`) REFERENCES `policy_library` (`policy_library_id`),
  KEY (`policy_status_id`),
  CONSTRAINT `fk_policy_policy_status_id` FOREIGN KEY (`policy_status_id`) REFERENCES `policy_status` (`policy_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# training
#------------------------------------------------
DROP TABLE IF EXISTS `training_status`;
CREATE TABLE `training_status` (
  `training_status_id` TINYINT(4) NOT NULL,
  `training_status_name` varchar(32) NOT NULL,
  PRIMARY KEY (`training_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `training_status` (`training_status_id`, `training_status_name`)
VALUES
  (1,'Draft'),
  (2,'Review'),
  (3,'Published'),
  (4,'Archive');

DROP TABLE IF EXISTS `training_library`;
CREATE TABLE `training_library` (
  `training_library_id` int(11) NOT NULL AUTO_INCREMENT,
  `training_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 is document, 2 is video series',
  `approved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 is un-approved, 1 is approved',
  `training_status_id` TINYINT(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `file_size` int(11) NOT NULL DEFAULT '0',
  `file_ext` varchar(10) DEFAULT NULL,
  `file_type` varchar(127) DEFAULT NULL,
  `file_name` varchar(256) DEFAULT NULL,
  `doc_guid` varchar(36) NOT NULL,
  `doc_title` varchar(256) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_date` DATE DEFAULT NULL,
  `author` varchar(256) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`training_library_id`),
  KEY (`training_status_id`),
  CONSTRAINT `fk_training_library_training_status_id` FOREIGN KEY (`training_status_id`) REFERENCES `training_status` (`training_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `training`;
CREATE TABLE `training` (
  `training_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `training_library_id` int(11) DEFAULT NULL COMMENT 'original id if copied from training library',
  `training_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 is document, 2 is video series',
  `approved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 is un-approved, 1 is approved',
  `training_status_id` TINYINT(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `file_size` int(11) NOT NULL DEFAULT '0',
  `file_ext` varchar(10) DEFAULT NULL,
  `file_type` varchar(127) DEFAULT NULL,
  `file_name` varchar(256) DEFAULT NULL,
  `doc_guid` varchar(36) NOT NULL,
  `doc_title` varchar(256) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_date` DATE DEFAULT NULL,
  `author` varchar(256) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`training_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_training_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_training_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`training_library_id`),
  CONSTRAINT `fk_training_training_library_id` FOREIGN KEY (`training_library_id`) REFERENCES `training_library` (`training_library_id`),
  KEY (`training_status_id`),
  CONSTRAINT `fk_training_training_status_id` FOREIGN KEY (`training_status_id`) REFERENCES `training_status` (`training_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `video_training`;
CREATE TABLE `video_training` (
  `video_training_id` int(11) NOT NULL AUTO_INCREMENT,
  `training_library_id` int(11) DEFAULT NULL,
  `training_id` int(11) DEFAULT NULL,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=hidden, 1=active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sort_order` int(11) DEFAULT '0',
  `video_title` varchar(256) NOT NULL,
  `video_code` varchar(512) NOT NULL,
  PRIMARY KEY (`video_training_id`),
  KEY (`training_library_id`),
  CONSTRAINT `fk_video_training_training_library_id` FOREIGN KEY (`training_library_id`) REFERENCES `training_library` (`training_library_id`),
  KEY (`training_id`),
  CONSTRAINT `fk_video_training_training_id` FOREIGN KEY (`training_id`) REFERENCES `training` (`training_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_video_training_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_video_training_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# tasks table Jan. 26, 2016
#------------------------------------------------
DROP TABLE IF EXISTS `task_status`;
CREATE TABLE `task_status` (
  `task_status_id` TINYINT(4) NOT NULL,
  `task_status_name` varchar(32) NOT NULL,
  PRIMARY KEY (`task_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `task_status` (`task_status_id`, `task_status_name`)
VALUES
  (0,'Not Active'),
  (1,'Planned'),
  (2,'In Progress'),
  (3,'Complete');

DROP TABLE IF EXISTS `task_frequency`;
CREATE TABLE `task_frequency` (
  `task_frequency_id` TINYINT(4) NOT NULL,
  `task_frequency_name` varchar(32) NOT NULL,
  PRIMARY KEY (`task_frequency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `task_frequency` (`task_frequency_id`, `task_frequency_name`)
VALUES
  (1,'One Time'),
  (2,'Daily'),
  (3,'Weekly'),
  (4,'Bi-Weekly'),
  (5,'Monthly'),
  (6,'Quarterly'),
  (7,'Twice Yearly'),
  (8,'Yearly');

CREATE TABLE `task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) DEFAULT NULL COMMENT 'company the taks belongs to',
  `company_user_id` int(11) DEFAULT NULL COMMENT 'company user the task is assigned to',
  `user_id` int(11) NOT NULL COMMENT 'user who created the record',
  `task_frequency_id` TINYINT(4) NOT NULL DEFAULT '1',
  `task_status_id` TINYINT(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=hidden, 1=active',
  `control_baseline_id` int(11) DEFAULT NULL COMMENT 'related control',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `target_date` DATE DEFAULT NULL,
  `task_name` varchar(256) NOT NULL,
  `task_description` text DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_task_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`company_user_id`),
  CONSTRAINT `fk_task_company_user_id` FOREIGN KEY (`company_user_id`) REFERENCES `company_user` (`company_user_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_task_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`task_frequency_id`),
  CONSTRAINT `fk_task_task_frequency_id` FOREIGN KEY (`task_frequency_id`) REFERENCES `task_frequency` (`task_frequency_id`),
  KEY (`task_status_id`),
  CONSTRAINT `fk_task_task_status_id` FOREIGN KEY (`task_status_id`) REFERENCES `task_status` (`task_status_id`),
  KEY (`control_baseline_id`),
  CONSTRAINT `fk_task_control_baseline_id` FOREIGN KEY (`control_baseline_id`) REFERENCES `control_baseline` (`control_baseline_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# user_profile policy and training lists Jan. 30, 2016
#-----------------------------------------------
DROP TABLE IF EXISTS `user_profile_policy`;
CREATE TABLE `user_profile_policy` (
  `user_profile_policy_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_profile_id` int(11) NOT NULL,
  `company_profile_id` int(11) NOT NULL,
  `policy_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`user_profile_policy_id`),
  KEY (`user_profile_id`),
  CONSTRAINT `fk_user_profile_policy_user_profile_id` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profile` (`user_profile_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_user_profile_policy_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`policy_id`),
  CONSTRAINT `fk_user_profile_policy_policy_id` FOREIGN KEY (`policy_id`) REFERENCES `policy` (`policy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `user_profile_training`;
CREATE TABLE `user_profile_training` (
  `user_profile_training_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_profile_id` int(11) NOT NULL,
  `company_profile_id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`user_profile_training_id`),
  KEY (`user_profile_id`),
  CONSTRAINT `fk_user_profile_training_user_profile_id` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profile` (`user_profile_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_user_profile_training_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`training_id`),
  CONSTRAINT `fk_user_profile_training_training_id` FOREIGN KEY (`training_id`) REFERENCES `training` (`training_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `user_policy_tracking`;
CREATE TABLE `user_policy_tracking` (
  `user_policy_tracking_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_profile_id` int(11) NOT NULL,
  `policy_id` int(11) NOT NULL,
  `ack` tinyint(1) DEFAULT 0,
  `view` tinyint(1) DEFAULT 0,
  `ack_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `view_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_policy_tracking_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_user_policy_tracking_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_user_policy_tracking_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`policy_id`),
  CONSTRAINT `fk_user_policy_tracking_policy_id` FOREIGN KEY (`policy_id`) REFERENCES `policy` (`policy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `user_training_tracking`;
CREATE TABLE `user_training_tracking` (
  `user_training_tracking_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_profile_id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `ack` tinyint(1) DEFAULT 0,
  `view` tinyint(1) DEFAULT 0,
  `ack_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `view_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_training_tracking_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_user_training_tracking_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_user_training_tracking_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`training_id`),
  CONSTRAINT `fk_user_training_tracking_training_id` FOREIGN KEY (`training_id`) REFERENCES `training` (`training_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `video_tracking`;
CREATE TABLE `video_tracking` (
  `video_tracking_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_profile_id` int(11) NOT NULL,
  `video_training_id` int(11) NOT NULL,
  `ack` tinyint(1) DEFAULT 0,
  `view` tinyint(1) DEFAULT 0,
  `ack_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `view_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`video_tracking_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_video_tracking_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_video_tracking_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`video_training_id`),
  CONSTRAINT `fk_video_tracking_video_training_id` FOREIGN KEY (`video_training_id`) REFERENCES `video_training` (`video_training_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `version`;
CREATE TABLE `version` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Major` int(11) NOT NULL,
  `Minor` int(11) NOT NULL,
  `SP` int(11) NOT NULL,
  `Build` int(11) NOT NULL,
  `Patch` int(11) NOT NULL DEFAULT '0',
  `UpdatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `version` (`Id`, `Major`, `Minor`, `SP`, `Build`, `Patch`, `UpdatedOn`, `Description`)
VALUES
  (1, 1, 0, 0, 2016013101, 0, NOW(), 'First Release');

# company_info holds answers from wizard
#----------------------------------------
DROP TABLE IF EXISTS `company_info`;
CREATE TABLE `company_info` (
  `company_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `business_name` varchar(100) DEFAULT NULL,
  `num_employees` varchar(10) DEFAULT NULL COMMENT '[1-10, 11-100, 101-1000, 1000+]',
  `address` varchar(100) DEFAULT NULL COMMENT 'Business Location',
  `city` varchar(60) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `publicly_traded` TINYINT(1) DEFAULT NULL COMMENT 'all fields are default null until answered by real human',
  `primary_business_type` varchar(60) DEFAULT NULL,
  `critical_infrastructure` TINYINT(1) DEFAULT NULL COMMENT 'yes,no,not sure - end of step1 wizard questions',
  `security_maturity` TINYINT(1) DEFAULT NULL COMMENT 'step 2 Low, Med, Hi',
  `highly_sensitive_data` TINYINT(1) DEFAULT NULL COMMENT '[yes/no/not sure] If [QID=1] yes, then Control ID [] = Planned',
  `individual_consumer_data` TINYINT(1) DEFAULT NULL COMMENT '[yes/no/not sure] If [QID=2] yes, then Control ID [312, 313, 315, 316, 317] = Planned',
  `credit_card_data` TINYINT(1)  DEFAULT NULL COMMENT '[yes/no/not sure] If [QID=3] yes, then Control ID [102, 114, 183, 184, 270, 271, 275] = Planned',
  `individual_health_data` TINYINT(1) DEFAULT NULL COMMENT '[yes/no/not sure] If [QID=4] yes, then Control ID [102, 114, 183, 184] = Planned',
  `international_personal_data` TINYINT(1) DEFAULT NULL COMMENT '[yes/no/not sure] If [QID=5] yes, then Control ID [] = Planned',
  `num_employees_access_sensitive_data` varchar(10) DEFAULT NULL COMMENT '[1-10, 11-100, 101-500, 500+] [QID=6]',
  `employee_mobile_customer_data` TINYINT(1) DEFAULT NULL COMMENT '[yes/no] If [QD=7] yes, then Control ID [222,224] = Planned',
  `remote_network_access` TINYINT(1) DEFAULT NULL COMMENT '[yes/no] If [QD=8] yes, then Control ID [222,224] = Planned',
  `local_consumer_datastore` TINYINT(1) DEFAULT NULL COMMENT '[yes/no] If [QID=9] yes, then Control ID [] = Planned - not in the cloud',
  `third_party_data` TINYINT(1) DEFAULT NULL COMMENT '[yes/no] If [QD=10] is yes, then Control ID [179,180, 181] = Planned',
  `software_development` TINYINT(1) DEFAULT NULL COMMENT '[yes/no] If [QD=11] yes, then Control ID [268, 270, 271, 272, 273] = Planned',
  `online_customer_transactions` TINYINT(1) DEFAULT NULL COMMENT '[yes/no] If [QD=12] yes, then Control ID [274, 275] = Planned',
  `primary_work_location` TINYINT(1) DEFAULT NULL COMMENT '[yes/no] if [QD=13] yes, then Control ID [] = Planned',
  `local_it_datacenters` TINYINT(1) DEFAULT NULL COMMENT '[yes/no,both] if [QID=14] yes, then Control ID [] = Planned',
  `sensitive_paper_records` TINYINT(1) DEFAULT NULL COMMENT '[yes/no] if [QID=15] yes, then Control ID [] = Planned',
  PRIMARY KEY (`company_info_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_company_info_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_company_info_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# incidents
#-----------------------------------------------
DROP TABLE IF EXISTS `incident_status`;
CREATE TABLE `incident_status` (
  `incident_status_id` TINYINT(4) NOT NULL,
  `incident_status_name` varchar(32) NOT NULL,
  PRIMARY KEY (`incident_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `incident_status` (`incident_status_id`, `incident_status_name`)
VALUES
  (1,'Open'),
  (2,'In Progress'),
  (3,'Closed');

DROP TABLE IF EXISTS `incident_type`;
CREATE TABLE `incident_type` (
  `incident_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) DEFAULT NULL,
  `incident_type` varchar(80) NOT NULL,
  `incident_description` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`incident_type_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_incident_type_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `incident_type` (`incident_type`, `incident_description`)
VALUES
  ('Security Failure', 'General Security Failure'),
  ('Disk Failure', 'Disk Failure'),
  ('Malware', 'Malicious Software Infection'),
  ('Unescorted Visitor', 'Visitor without an escort.'),
  ('Door Left Open', 'Observed a door left open that should have been closed.'),
  ('Denial of Service Attack', 'Networks Based External Denial of Service Attack (DDOS)'),
  ('Data Loss', 'Deletion of company data'),
  ('Security Policy Violation', 'Observed Violation of Written Security Policies.'),
  ('Theft', 'Physical theft of company assets, especially electronic devices holding company data.');

DROP TABLE IF EXISTS `incident`;
CREATE TABLE `incident` (
  `incident_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Regular userId that create or reported the incident.',
  `company_user_id` int(11) DEFAULT NULL COMMENT 'company user the task is assigned to',
  `incident_type_id` int(11) DEFAULT NULL,
  `incident_status_id` TINYINT(4) NOT NULL DEFAULT 1,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `system` varchar(256) DEFAULT NULL,
  `summary` text NOT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`incident_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_incident_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_incident_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`incident_type_id`),
  CONSTRAINT `fk_incident_incident_type_id` FOREIGN KEY (`incident_type_id`) REFERENCES `incident_type` (`incident_type_id`),
  KEY (`company_user_id`),
  CONSTRAINT `fk_incident_company_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`incident_status_id`),
  CONSTRAINT `fk_incident_incident_status_id` FOREIGN KEY (`incident_status_id`) REFERENCES `incident_status` (`incident_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

UPDATE `version` SET `Build`=2016021101, `UpdatedOn`=NOW() WHERE Id=1;

# quiz library
#------------------------------------------
DROP TABLE IF EXISTS `quiz_status`;
CREATE TABLE `quiz_status` (
  `quiz_status_id` TINYINT(4) NOT NULL,
  `quiz_status_name` varchar(32) NOT NULL,
  PRIMARY KEY (`quiz_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `quiz_status` (`quiz_status_id`, `quiz_status_name`)
VALUES
  (1,'Draft'),
  (2,'Review'),
  (3,'Published'),
  (4,'Archive');

DROP TABLE IF EXISTS `quiz_library`;
CREATE TABLE `quiz_library` (
  `quiz_library_id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_status_id` TINYINT(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `passing_score` int(11) NOT NULL DEFAULT 0,
  `max_retakes` int(11) NOT NULL DEFAULT 1 COMMENT '0 or less is unlimited, 1 or greater is number of retakes allowed',
  `quiz_title` varchar(256) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_date` DATE DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`quiz_library_id`),
  KEY (`quiz_status_id`),
  CONSTRAINT `fk_quiz_library_quiz_status_id` FOREIGN KEY (`quiz_status_id`) REFERENCES `quiz_status` (`quiz_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `quiz`;
CREATE TABLE `quiz` (
  `quiz_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quiz_library_id` int(11) DEFAULT NULL COMMENT 'original id if copied from quiz library',
  `quiz_status_id` TINYINT(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `passing_score` int(11) NOT NULL DEFAULT 0,
  `max_retakes` int(11) NOT NULL DEFAULT 1 COMMENT '0 or greater is number of retakes allowed',
  `quiz_title` varchar(256) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_date` DATE DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`quiz_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_quiz_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_quiz_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`quiz_library_id`),
  CONSTRAINT `fk_quiz_quiz_library_id` FOREIGN KEY (`quiz_library_id`) REFERENCES `quiz_library` (`quiz_library_id`),
  KEY (`quiz_status_id`),
  CONSTRAINT `fk_quiz_quiz_status_id` FOREIGN KEY (`quiz_status_id`) REFERENCES `quiz_status` (`quiz_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `quiz_question`;
CREATE TABLE `quiz_question` (
  `quiz_question_id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_library_id` int(11) DEFAULT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=hidden, 1=active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `question_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=True or False, 2=Yes or No, 3=multiple choice',
  `correct_answer` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'which answer column is the right answer',
  `weight` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'multiplier of score',
  `score` int(11) NOT NULL DEFAULT '10',
  `category_name` varchar(80) DEFAULT NULL,
  `answer1` varchar(80) DEFAULT NULL,
  `answer2` varchar(80) DEFAULT NULL,
  `answer3` varchar(80) DEFAULT NULL,
  `answer4` varchar(80) DEFAULT NULL,
  `answer5` varchar(80) DEFAULT NULL,
  `question` varchar(512) NOT NULL,
  `hint` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`quiz_question_id`),
  KEY (`quiz_library_id`),
  CONSTRAINT `fk_quiz_question_quiz_library_id` FOREIGN KEY (`quiz_library_id`) REFERENCES `quiz_library` (`quiz_library_id`),
  KEY (`quiz_id`),
  CONSTRAINT `fk_quiz_question_quiz_id` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_quiz_question_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_quiz_question_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `quiz_answer`;
CREATE TABLE `quiz_answer` (
  `quiz_answer_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id of the answer record',
  `quiz_question_id` int(11) NOT NULL COMMENT 'quiz question id of the quiz question being answered',
  `taken_number` int(11) NOT NULL COMMENT 'should match the quiz tracking taken_count to know which attempt number this set of answers was for',
  `quiz_id` int(11) NOT NULL COMMENT 'quiz id of the quiz being taken, that the quiz question id belongs to',
  `user_id` int(11) NOT NULL COMMENT 'user id of the person taken the quiz',
  `company_profile_id` int(11) NOT NULL COMMENT 'company that owns the quiz and the user context',
  `answer` tinyint(4) DEFAULT 0 COMMENT 'which of the 5 possible answers did he pick',
  `correct` tinyint(1) DEFAULT 0 COMMENT '1 is correct, 0 is wrong',
  `score` int(11) DEFAULT '0' COMMENT '0 if wrong, if correct the score is score times the weight assigned to the question',
  `answer_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'time quiz taken',
  PRIMARY KEY (`quiz_answer_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_quiz_answer_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_quiz_answer_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`quiz_id`),
  CONSTRAINT `fk_quiz_answer_quiz_id` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`),
  KEY (`quiz_question_id`),
  CONSTRAINT `fk_quiz_answer_quiz_question_id` FOREIGN KEY (`quiz_question_id`) REFERENCES `quiz_question` (`quiz_question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `quiz_tracking`;
CREATE TABLE `quiz_tracking` (
  `quiz_tracking_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id of the tracking record',
  `quiz_id` int(11) NOT NULL COMMENT 'quiz id of the quiz being taken',
  `user_id` int(11) NOT NULL COMMENT 'user id of the person taken the quiz',
  `company_profile_id` int(11) NOT NULL COMMENT 'company that owns the quiz and the user context',
  `taken_count` int(11) DEFAULT '0' COMMENT 'number of times quiz was taken',
  `possible_score` int(11) DEFAULT '0' COMMENT 'highest possible score of the quiz',
  `low_score` int(11) DEFAULT '0' COMMENT 'total low score ever',
  `high_score` int(11) DEFAULT '0' COMMENT 'total high score ever',
  `viewed` tinyint(1) DEFAULT 0 COMMENT '1 if ever viewed quiz, 0 not viewed ever',
  `completed` tinyint(1) DEFAULT 0 COMMENT '1 if ever completed all answers, 0 not completed ever',
  `passed` tinyint(1) DEFAULT 0 COMMENT '1 if passed once, 0 not passed once',
  `last_score` int(11) DEFAULT '0' COMMENT 'total score, the last time quiz taken',
  `last_correct` int(11) DEFAULT '0' COMMENT 'number of correct answers, the last time quiz taken',
  `last_wrong` int(11) DEFAULT '0' COMMENT 'number of wrong answers, the last time quiz taken',
  `viewed_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'first time quiz was viewed',
  `completed_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'first time quiz was completed',
  `passed_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'first time quiz was passed',
  `answer_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'last time quiz taken',
  PRIMARY KEY (`quiz_tracking_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_quiz_tracking_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_quiz_tracking_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`quiz_id`),
  CONSTRAINT `fk_quiz_tracking_quiz_id` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `user_profile_quiz`;
CREATE TABLE `user_profile_quiz` (
  `user_profile_quiz_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_profile_id` int(11) NOT NULL,
  `company_profile_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`user_profile_quiz_id`),
  KEY (`user_profile_id`),
  CONSTRAINT `fk_user_profile_quiz_user_profile_id` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profile` (`user_profile_id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_user_profile_quiz_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`quiz_id`),
  CONSTRAINT `fk_user_profile_quiz_quiz_id` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `invite`;
CREATE TABLE `invite` (
  `invite_id` int(11) NOT NULL AUTO_INCREMENT,
  `invite_guid` varchar(36) DEFAULT NULL,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `invited_company_profile_id` int(11) DEFAULT NULL,
  `invited_user_id` int(11) DEFAULT NULL,
  `baseline_id` int(11) DEFAULT NULL,
  `sent` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 is not sent yet, 1 is sent, 2 is failed sent',
  `accepted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 is not accepted yet, 1 is accepted, 2 is rejected',
  `adopted_baseline` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 is not adopted yet, 1 is adopted, 2 is rejected',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sent_date` DATE DEFAULT NULL,
  `expiry_date` DATE DEFAULT NULL,
  `accepted_date` DATE DEFAULT NULL,
  `invitee_name` varchar(256) NOT NULL,
  `invitee_email` varchar(256) NOT NULL,
  PRIMARY KEY (`invite_id`),
  UNIQUE KEY `invite_invite_guid` (`invite_guid`),
  KEY (`user_id`),
  CONSTRAINT `fk_invite_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_invite_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`),
  KEY (`invited_user_id`),
  CONSTRAINT `fk_invite_invited_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`invited_company_profile_id`),
  CONSTRAINT `fk_invite_invited_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `export_report`;
CREATE TABLE `export_report` (
  `export_report_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=delete, 1=active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_type` varchar(32) NOT NULL,
  `file_ext` varchar(10) DEFAULT NULL,
  `file_type` varchar(127) DEFAULT NULL,
  `file_name` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `data`  LONGTEXT NULL DEFAULT NULL,
  `report` BLOB NULL DEFAULT NULL,
  PRIMARY KEY (`export_report_id`),
  KEY (`user_id`),
  CONSTRAINT `fk_report_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  KEY (`company_profile_id`),
  CONSTRAINT `fk_report_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile` (`company_profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `inherent_risk_score`;
CREATE TABLE `inherent_risk_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_profile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answers` text NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `assessed_on` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `company_profile_id` (`company_profile_id`),
CONSTRAINT `fk_inherent_risk_score_company_profile_id` FOREIGN KEY (`company_profile_id`) REFERENCES `company_profile`(`company_profile_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  KEY `user_id` (`user_id`),
CONSTRAINT `fk_inherent_risk_score_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

UPDATE `version` SET `Build`=2016081802, `UpdatedOn`=NOW() WHERE Id=1;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
