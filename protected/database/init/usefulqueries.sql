select  company_user_id, lastname, firstname, upp.policy_id, cu.profile_id, upp.user_profile_id, sum(tra.view), sum(tra.ack), tra.user_id, u.id
from company_user cu
left join tbl_users u ON cu.email=u.email
left join user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
left join user_profile_policy upp ON up.user_profile_id=upp.user_profile_id
left join user_policy_tracking tra ON upp.policy_id=tra.policy_id and u.id=tra.user_id and tra.company_profile_id=1
where cu.company_profile_id=1
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;


select  company_user_id, lastname, firstname, upp.policy_id, cu.profile_id, upp.user_profile_id, sum(tra.view), sum(tra.ack), tra.user_id
from user_profile_policy upp
inner join user_profile up on up.user_profile_id=upp.user_profile_id AND up.company_profile_id=1
left join company_user cu ON up.profile_id=cu.profile_id AND up.company_profile_id=1
inner join tbl_users u ON cu.email=u.email
left join user_policy_tracking tra ON upp.policy_id=tra.policy_id and u.id=tra.user_id and tra.company_profile_id=1
where company_user_id is not null and upp.company_profile_id=1
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;


select count(company_user_id), company_user_id, lastname,firstname, upp.policy_id, cu.profile_id, upp.user_profile_id
from user_profile_policy upp
inner join user_profile up on up.user_profile_id=upp.user_profile_id AND up.company_profile_id=1
left join company_user cu ON up.profile_id=cu.profile_id AND up.company_profile_id=1
where company_user_id is not null and upp.company_profile_id=1
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;

select count(company_user_id), company_user_id, lastname,firstname, upt.training_id, cu.profile_id, upt.user_profile_id
from user_profile_training upt
inner join user_profile up on up.user_profile_id=upt.user_profile_id AND up.company_profile_id=1
left join company_user cu ON up.profile_id=cu.profile_id AND up.company_profile_id=1
where company_user_id is not null and upt.company_profile_id=1
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;

SELECT count(company_user_id), company_user_id, lastname,firstname, upq.quiz_id, cu.profile_id, upq.user_profile_id
FROM user_profile_quiz upq
INNER JOIN user_profile up on up.user_profile_id=upq.user_profile_id AND up.company_profile_id=1
LEFT JOIN company_user cu ON up.profile_id=cu.profile_id AND up.company_profile_id=1
WHERE company_user_id is not null and upq.company_profile_id=1
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;


SELECT company_user_id, lastname, firstname, u.id, count(upp.policy_id), sum(tra.view), sum(tra.ack)
FROM company_user cu
  LEFT JOIN tbl_users u ON cu.email=u.email
  LEFT JOIN user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
  LEFT JOIN user_profile_policy upp ON up.user_profile_id=upp.user_profile_id
  LEFT JOIN user_policy_tracking tra ON upp.policy_id=tra.policy_id and u.id=tra.user_id and tra.company_profile_id=1
WHERE cu.company_profile_id=1
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;

SELECT company_user_id, lastname, firstname, u.id, count(upt.training_id), sum(tra.view), sum(tra.ack)
FROM company_user cu
  LEFT JOIN tbl_users u ON cu.email=u.email
  LEFT JOIN user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
  LEFT JOIN user_profile_training upt ON up.user_profile_id=upt.user_profile_id
  LEFT JOIN user_training_tracking tra ON upt.training_id=tra.training_id and u.id=tra.user_id and tra.company_profile_id=1
WHERE cu.company_profile_id=1
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;

SELECT company_user_id, lastname, firstname, u.id, count(upq.quiz_id) items, sum(tra.viewed) viewed, sum(tra.completed) completed, sum(tra.passed) passed
FROM company_user cu
  LEFT JOIN tbl_users u ON cu.email=u.email
  LEFT JOIN user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
  LEFT JOIN user_profile_quiz upq ON up.user_profile_id=upq.user_profile_id
  LEFT JOIN quiz_tracking tra ON upq.quiz_id=tra.quiz_id and u.id=tra.user_id and tra.company_profile_id=1
WHERE cu.company_profile_id=1
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;



SELECT company_profile_id, company_user_id, lastname, firstname, id,
  sum(policy_count) policy_count, sum(policy_unopened) policy_unopened, sum(policy_views) policy_views, sum(policy_acks) policy_acks,
  sum(training_count) training_count, sum(training_unopened) training_unopened, sum(training_views) training_views, sum(training_completed) training_completed,
  sum(quiz_count) quiz_count,  sum(quiz_unopened) quiz_unopened, sum(quiz_viewed) quiz_viewed,  sum(quiz_completed) quiz_completed, sum(quiz_passed) quiz_passed
  FROM
(SELECT cu.company_profile_id, company_user_id, lastname, firstname, u.id,
   count(upp.policy_id) policy_count, count(upp.policy_id)-sum(tra.view) policy_unopened, sum(tra.view) policy_views, sum(tra.ack) policy_acks,
   0 training_count, 0 training_unopened, 0 training_views, 0 training_completed,
   0 quiz_count, 0 quiz_unopened, 0 quiz_viewed, 0 quiz_completed, 0 quiz_passed
FROM company_user cu
  LEFT JOIN tbl_users u ON cu.email=u.email
  LEFT JOIN user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
  LEFT JOIN user_profile_policy upp ON up.user_profile_id=upp.user_profile_id
  LEFT JOIN user_policy_tracking tra ON upp.policy_id=tra.policy_id and u.id=tra.user_id and tra.company_profile_id=1
WHERE cu.company_profile_id=1
GROUP BY company_user_id
UNION ALL
SELECT cu.company_profile_id, company_user_id, lastname, firstname, u.id, 0 policy_count, 0 policy_unopened, 0 policy_views, 0 policy_acks,
   count(upt.training_id) training_count, count(upt.training_id)-sum(tra.view) training_unopened, sum(tra.view) training_views, sum(tra.ack) training_completed,
   0 quiz_count, 0 quiz_unopened, 0 quiz_viewed, 0 quiz_completed, 0 quiz_passed
FROM company_user cu
  LEFT JOIN tbl_users u ON cu.email=u.email
  LEFT JOIN user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
  LEFT JOIN user_profile_training upt ON up.user_profile_id=upt.user_profile_id
  LEFT JOIN user_training_tracking tra ON upt.training_id=tra.training_id and u.id=tra.user_id and tra.company_profile_id=1
WHERE cu.company_profile_id=1
GROUP BY company_user_id
UNION ALL
SELECT cu.company_profile_id, company_user_id, lastname, firstname, u.id,
   0 policy_count, 0 policy_unopened, 0 policy_views, 0 policy_acks,
   0 training_count, 0 training_unopened, 0 training_views, 0 training_completed,
   count(upq.quiz_id) quiz_count, count(upq.quiz_id)-sum(tra.viewed) quiz_unopened, sum(tra.viewed) quiz_viewed, sum(tra.completed) quiz_completed, sum(tra.passed) quiz_passed
FROM company_user cu
  LEFT JOIN tbl_users u ON cu.email=u.email
  LEFT JOIN user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
  LEFT JOIN user_profile_quiz upq ON up.user_profile_id=upq.user_profile_id
  LEFT JOIN quiz_tracking tra ON upq.quiz_id=tra.quiz_id and u.id=tra.user_id and tra.company_profile_id=1
WHERE cu.company_profile_id=1
GROUP BY company_user_id) user_compliance
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;







SELECT company_profile_id, company_user_id, lastname, firstname, id,
  sum(policy_count) policy_count, sum(policy_count)-sum(policy_views) policy_unopened, sum(policy_views) policy_views, sum(policy_acks) policy_acks,
  sum(training_count) training_count,  sum(training_count)-sum(training_views) training_unopened, sum(training_views) training_views, sum(training_completed) training_completed,
  sum(quiz_count) quiz_count,  sum(quiz_count)-sum(quiz_viewed) quiz_unopened, sum(quiz_viewed) quiz_viewed,  sum(quiz_completed) quiz_completed, sum(quiz_passed) quiz_passed
FROM
  (SELECT cu.company_profile_id, company_user_id, lastname, firstname, u.id,
     count(upp.policy_id) policy_count, sum(tra.view) policy_views, sum(tra.ack) policy_acks,
     0 training_count, 0 training_views, 0 training_completed,
     0 quiz_count, 0 quiz_viewed, 0 quiz_completed, 0 quiz_passed
   FROM company_user cu
     LEFT JOIN tbl_users u ON cu.email=u.email
     LEFT JOIN user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
     LEFT JOIN user_profile_policy upp ON up.user_profile_id=upp.user_profile_id
     LEFT JOIN user_policy_tracking tra ON upp.policy_id=tra.policy_id and u.id=tra.user_id and tra.company_profile_id=1
   WHERE cu.company_profile_id=1
   GROUP BY company_user_id
   UNION ALL
   SELECT cu.company_profile_id, company_user_id, lastname, firstname, u.id, 0 policy_count, 0 policy_views, 0 policy_acks,
                                                                             count(upt.training_id) training_count, sum(tra.view) training_views, sum(tra.ack) training_completed,
                                                                             0 quiz_count, 0 quiz_viewed, 0 quiz_completed, 0 quiz_passed
   FROM company_user cu
     LEFT JOIN tbl_users u ON cu.email=u.email
     LEFT JOIN user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
     LEFT JOIN user_profile_training upt ON up.user_profile_id=upt.user_profile_id
     LEFT JOIN user_training_tracking tra ON upt.training_id=tra.training_id and u.id=tra.user_id and tra.company_profile_id=1
   WHERE cu.company_profile_id=1
   GROUP BY company_user_id
   UNION ALL
   SELECT cu.company_profile_id, company_user_id, lastname, firstname, u.id,
     0 policy_count, 0 policy_views, 0 policy_acks,
     0 training_count,  0 training_views, 0 training_completed,
     count(upq.quiz_id) quiz_count,  sum(tra.viewed) quiz_viewed, sum(tra.completed) quiz_completed, sum(tra.passed) quiz_passed
   FROM company_user cu
     LEFT JOIN tbl_users u ON cu.email=u.email
     LEFT JOIN user_profile up on cu.profile_id=up.profile_id AND up.company_profile_id=1
     LEFT JOIN user_profile_quiz upq ON up.user_profile_id=upq.user_profile_id
     LEFT JOIN quiz_tracking tra ON upq.quiz_id=tra.quiz_id and u.id=tra.user_id and tra.company_profile_id=1
   WHERE cu.company_profile_id=1
   GROUP BY company_user_id) user_compliance
GROUP BY company_user_id
ORDER BY lastname ASC, firstname asc;








SELECT 'Policy' dtype,  doc.doc_title title, tra.view v, tra.ack c, 0 p, tra.view_date vt, tra.ack_date ct, NOW() pt
FROM  user_profile_policy up
  INNER JOIN policy doc ON up.policy_id=doc.policy_id
  LEFT JOIN user_policy_tracking tra ON up.policy_id=tra.policy_id and tra.user_id=4 and tra.company_profile_id=1
WHERE up.user_profile_id=7 and up.company_profile_id=1
ORDER BY doc_title ASC;

SELECT 'Training' dtype,  doc.doc_title title, tra.view v, tra.ack c, 0 p, tra.view_date vt, tra.ack_date ct, NOW() pt
FROM  user_profile_training up
  INNER JOIN training doc ON up.training_id=doc.training_id
  LEFT JOIN user_training_tracking tra ON up.training_id=tra.training_id and tra.user_id=4 and tra.company_profile_id=1
WHERE up.user_profile_id=7 and up.company_profile_id=1
ORDER BY doc_title ASC;

SELECT 'Quiz' dtype,  doc.quiz_title title, tra.viewed v, tra.completed c, tra.passed p, tra.viewed_timestamp vt, tra.completed_timestamp ct, tra.passed_timestamp pt
FROM  user_profile_quiz up
  INNER JOIN quiz doc ON up.quiz_id=doc.quiz_id
  LEFT JOIN quiz_tracking tra ON up.quiz_id=tra.quiz_id and tra.user_id=4 and tra.company_profile_id=1
WHERE up.user_profile_id=7 and up.company_profile_id=1
ORDER BY quiz_title ASC;






select  p.policy_id, p.doc_title, sum(upt.ack), sum(upt.view)
from policy p
  left join user_profile_policy upp on p.policy_id = upp.policy_id and upp.company_profile_id=1
  left join user_profile up on upp.user_profile_id=up.user_profile_id and up.company_profile_id=1
  left join user_policy_tracking upt on p.policy_id=upt.policy_id and upt.company_profile_id=1
  left join tbl_users u ON upt.user_id=u.id
where p.company_profile_id=1 and p.policy_status_id=3
group by p.policy_id;

select  p.policy_id, p.doc_title, sum(upt.ack), sum(upt.view)
from policy p
  left join user_profile_policy upp on p.policy_id = upp.policy_id and upp.company_profile_id=1
  left join user_policy_tracking upt on p.policy_id=upt.policy_id and upt.company_profile_id=1
where p.company_profile_id=1 and p.policy_status_id=3
group by p.policy_id;

select  p.policy_id, cu.company_user_id, up.profile_id
from policy p
  left join user_profile_policy upp on p.policy_id = upp.policy_id and upp.company_profile_id=1
  left join user_profile up on upp.user_profile_id=up.user_profile_id and up.company_profile_id=1
  left join company_user cu on up.profile_id=cu.profile_id and cu.company_profile_id=1
where p.company_profile_id=1 and p.policy_status_id=3 and cu.company_user_id is not null;


























