INSERT INTO `quiz_library` (`quiz_library_id`, `quiz_status_id`, `status`, `passing_score`, `quiz_title`, `created_on`, `publish_date`, `description`)
VALUES
  (1, 3, 1, 80, 'PISC Security Awareness', '2016-02-17 15:09:26', '2016-02-17', 'This assessment will test a user’s knowledge of basic information security principles as defined within the PISC Security Awareness Training.');


INSERT INTO quiz_question (quiz_library_id, category_name,	sort_order, question_type,	question,	hint,	answer1, answer2,	answer3, answer4,	answer5, correct_answer,	score, weight)
VALUES
(1,'Organization',1,3,'Who is responsible for Information Security at our organization?', 'Select who has resonsibility for protecting information at your organization.','The Information Security Team','The CEO','Everyone in the organization',NULL,NULL,3,10,1),
(1,'Classification',2,1,'Separating information into different types is called "CLASSIFICATION".', 'Select TRUE if you agree with this statement and FALSE if you do not.','TRUE','FALSE',NULL,NULL,NULL,1,10,1),
(1,'Disposal',3,3,'What is the best way to dispose of sensitive information:', 'Select which is the most secure way to dispose of information based on your training','Throw it away','Return it to IT','Have it destroyed',NULL,NULL,3,10,1),
(1,'Email',4,3,'When should I give out my credentials (username or password) via email?', 'Select which situation','Whenever it is requested','Never - No matter who requests them.','Only when requested by our IT department',NULL,NULL,2,10,1),
(1,'Network',5,1,'My organization may monitor all of my activity on the network:', 'Select TRUE if you agree with this statement and FALSE if you do not.','TRUE','FALSE',NULL,NULL,NULL,1,10,1),
(1,'Policies',6,1,'I am required to understand the information security policies that my organization has published as part of my job.', 'Select TRUE if you agree with this statement and FALSE if you do not.','TRUE','FALSE',NULL,NULL,NULL,1,10,1),
(1,'Incident Response',7,3,'What should I do if I discover a virus on my computer?', 'Select TRUE if you agree with this statement and FALSE if you do not.','Remove it','Shut down the computer','Report it to the Security Team',NULL,NULL,3,10,1),
(1,'Email',8,3,'What should I always do before opening an email attachment:', 'Select the best answer based on your training','Ask Permission from IT','Scan it for Viruses','Delete it',NULL,NULL,2,10,1),
(1,'Access Control',9,3,'Access to our organization\'s informaton is based on the concept of:', NULL, 'Encryption','Need-to-Know','Security',NULL,NULL,2,10,1),
(1,'Passwords',11,3,'Which of the following is the most secure password?', 'Select the password from the list that would be most secure','Pass1234','BobJones','Here$toU',NULL,NULL,3,10,1),
(1,'Physical Security',12,1,'Is it okay to leave sensitive information on my desk as long as it is during working hours?', 'Select TRUE if you agree with this statement and FALSE if you do not.','TRUE','FALSE',NULL,NULL,NULL,2,10,1),
(1,'Access Control',13,3,'Protecting electronic information so it cannot be easily read is called:', 'Select the correct word','Encryption','Classification','Deception',NULL,NULL,1,10,1);
