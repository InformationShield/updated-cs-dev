<?php

class UserPolicyTracking_Ex
{
    public static function getPolicyTrackingStatusCounts($company_id)
    {
        $stats = array(
            array('total'=>0,'status'=>'0','label'=>'Unopened'),
            array('total'=>0,'status'=>'1','label'=>'Viewed'),
            array('total'=>0,'status'=>'2','label'=>'Acknowledged'),
        );
        $sql = "SELECT COUNT(pt.user_policy_tracking_id) total, ((pt.ack*10) + pt.view) status"
                ." FROM user_policy_tracking pt"
                ." INNER JOIN policy p ON pt.policy_id=p.policy_id AND p.policy_status_id=".Cons::POLICY_STATUS_PUBLISHED." AND p.status=".Cons::ACTIVE
                ." WHERE pt.company_id = :company_id"
                ." GROUP BY ((pt.ack*10) + pt.view)"
                ." ORDER BY ((pt.ack*10) + pt.view)";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            foreach($results as $row) {
                $index = intval($row['status']);
                $index = ($index > 1) ? 2 : $index;
                if ($index >= 0 && $index <= 2 &&
                    isset($row['total']) &&
                    $row['total'] > 0) {
                    $stats[$index]['total'] += $row['total'];
                }
            }
        }

        $userpolicycount = 0;
        $sql = "SELECT count(employee_id) userpolicycount"
                ." FROM user_profile_policy upp"
                ." INNER JOIN policy p ON upp.policy_id=p.policy_id AND p.policy_status_id=".Cons::POLICY_STATUS_PUBLISHED." AND p.status=".Cons::ACTIVE
                ." INNER JOIN user_profile up on up.user_profile_id=upp.user_profile_id AND up.company_id=:company_id"
                ." LEFT JOIN employee e ON up.profile_id=e.profile_id AND e.company_id=:company_id"
                ." WHERE employee_id is not null AND upp.company_id=:company_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results) && isset($results['userpolicycount']) && $results['userpolicycount'] > 0) {
            $userpolicycount = intval($results['userpolicycount']);
        }

        if ($userpolicycount > 0) {
            $stats[0]['total'] = $userpolicycount - (intval($stats[1]['total']) + intval($stats[2]['total'])) ;
        }
        return $stats;
    }

    public static function track($policy_id,$company_id,$user_id,$ack,$view)
    {
        $userPolicyTracking = UserPolicyTracking::model()->findByAttributes(array('policy_id' => $policy_id, 'user_id' => $user_id, 'company_id' => $company_id));
        if (!isset($userPolicyTracking)) {
            $userPolicyTracking = new UserPolicyTracking();
            $userPolicyTracking->policy_id = $policy_id;
            $userPolicyTracking->user_id = $user_id;
            $userPolicyTracking->company_id = $company_id;
        }
        if ($ack == 'yes') {
            $userPolicyTracking->ack = 1;
            $userPolicyTracking->ack_date = date("Y-m-d H:i:s");
        }
        if (!empty($view)) {
            $userPolicyTracking->view = 1;
            $userPolicyTracking->view_date = date("Y-m-d H:i:s");
        }
        if ($userPolicyTracking->save()) {
            return true;
        } else {
            Yii::log(print_r($userPolicyTracking->getErrors(), true), 'error', 'UserPolicyTracking_Ex.track');
        }
        return false;
    }
}