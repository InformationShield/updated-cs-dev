<?php

class ControlStatus_Ex
{
    public static function getStatusName($control_status_id)
    {
        $retVal = '';
        $sql = "SELECT control_status_name FROM control_status WHERE control_status_id=:control_status_id LIMIT 1";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":control_status_id", $control_status_id, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results) && !empty($results['control_status_name'])) {
            $retVal = $results['control_status_name'];
        }
        return $retVal;
    }

    public static function getMaxScore()
    {
        $retVal = 1;
        $sql = "SELECT MAX(control_status_id) maxid FROM control_status";
        $command = Yii::app()->db->createCommand($sql);
        $results = $command->query()->read();
        if (!empty($results) && $results['maxid'] > 0) {
            $retVal = $results['maxid'];
        }
        return $retVal;
    }


    public static function getAllBySortOrder()
    {
        $retVal = array();
        $sql = "SELECT * FROM control_status ORDER BY sort_order";
        $command = Yii::app()->db->createCommand($sql);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }
}