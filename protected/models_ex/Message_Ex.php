<?php

class Message_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY created_on desc";

    public static function getUserMessages($user_id)
    {
        $messages = Message::model()->findAllByAttributes(array('recipient_id' => $user_id, 'read' => 0, 'status' => 1), array('order' => 'created_on desc'));
        return $messages;
    }

    public static function setRead($messageId, $read=1)
    {
        $messageModel = Message::model()->findByPk($messageId);
        $messageModel->read = $read;
        $messageModel->save();
    }
    
    public static function getViewValues($userId,$messageId)
    {
        $v = array();
        $row = self::getMessage($count,$messageId,$userId,1);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Created On'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y H:i:s');
            $v['Title'] = $model->title;
            $v['Message'] = $model->message;
            //$v['Updated On'] = DateHelper::FormatDateTimeString($model->updated_on, 'm/d/Y H:i:s');
            //$v['Status'] = ($model->read == 0 ? 'Un-Read' : 'Read');
         }
        return $v;
    }

    // for the Administrator view
    public static function getAdminViewValues($messageId)
    {
        $v = array();
        $row = self::getMessage($count,$messageId,null,null);
        if (!empty($row)) {
            $model = (object) $row;
            if ($model->recipient_name === ' ') {
                $v['Recipient'] = $model->recipient_email;
            } else {
                $v['Recipient'] = $model->recipient_name;
            }
            $v['Created On'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y H:i:s');
            $v['Updated On'] = DateHelper::FormatDateTimeString($model->updated_on, 'm/d/Y H:i:s');
            $v['Status'] = ($model->read == 0 ? 'Un-Read' : 'Read');
            $v['Deleted'] = ($model->status == 0 ? 'Yes' : 'No');
            $v['Title'] = $model->title;
            $v['Message'] = $model->message;
        }
        return $v;
    }

    public static function getMessage(&$count,$messageId=null,$userId=null,$status=1,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null,$groupMessage=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT m.*, CONCAT(up.first_name,' ',up.last_name) as recipient_name, u.email as recipient_email";
        }
        $from = " FROM message m "
               ." INNER JOIN tbl_profiles up ON m.recipient_id=up.user_id"
               ." INNER JOIN tbl_users u ON m.recipient_id=u.id";

        if ($status==null) { // ignore status for admin view
            $sqlWhere = " WHERE m.status is NOT NULL";
        } else {
            $sqlWhere = " WHERE m.status = " . $status;
        }
        if (!empty($userId)) {
            $sqlWhere .= " AND m.recipient_id = ".$userId;
        }
        if (!empty($messageId)) {
            $sqlWhere .= " AND m.message_id = ".$messageId;
        }
        if ($groupMessage) {	// for admin, group messages sent to everyone
			$sqlWhere .= " AND (m.parent_id IS NULL OR m.message_id = m.parent_id)";
		}
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $messageId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();

            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }

        $results = $command->query()->readAll();
        if (!empty($results)) {
            if (!empty($messageId)) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }

        return $retVal;
    }

    public static function getMessagesJson($userId,$status,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null,$groupMessage=null)
    {
        $rows = self::getMessage($count,null,$userId,$status,$pageSize,$offsetBy,null,$search,$searchLogic,$sort,$groupMessage);
        if (!empty($rows)) {
            foreach ($rows as $key=>&$row) {
                if (!empty($row['message_id'])) {
                    $row['created_on'] = DateHelper::FormatDateTimeString($row['created_on'], 'm/d/Y');
                    $row['updated_on'] = DateHelper::FormatDateTimeString($row['updated_on'], 'm/d/Y');
					if ($row['message_id'] == $row['parent_id']) {
						$rows[$key]['recipient_name'] = '* All Company Owners';
					}
                    $row['message_id'] = Obscure::encode($row['message_id']);
                } else {
                    unset($rows[$key]);
                    $count--;
                }
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

    public static function deleteMessages($messageId, $parentId, $markStatusOnly) {

		if ($markStatusOnly) {
			$sql = "UPDATE message SET status = 0";
		} else {
			$sql = "DELETE FROM message";
		}
		if ($parentId) { // delete entire group of messages
			$sql .= " WHERE parent_id = :parentId";
			$command = Yii::app()->db->createCommand($sql);
			$command->bindValue(":parentId", $parentId, PDO::PARAM_INT);
			$command->execute();
		} else { // delete single message
			$sql .= " WHERE message_id = :messageId";
			$command = Yii::app()->db->createCommand($sql);
			$command->bindValue(":messageId", $messageId, PDO::PARAM_INT);
			$command->execute();
		}
		return true;
	}

}