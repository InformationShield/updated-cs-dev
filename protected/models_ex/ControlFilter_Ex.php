<?php

class ControlFilter_Ex extends BaseSearchSort_Ex
{
	public static function GetFilter($filter_id)
	{
		$retVal = null;
		$sql = "
			SELECT cf.*
			FROM control_filter cf
			WHERE cf.control_filter_id = :filter_id
        ";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":filter_id", $filter_id, PDO::PARAM_INT);
		$data = $command->query()->read();
		if ($data) {
			$data['id'] = Obscure::encode($data['control_filter_id']);
			$retVal = $data;
		}
		return $retVal;
	}

	public static function GetFilterSearchFields($filter_id)
	{
		$retVal = null;
		$sql = "
			SELECT cf.label1, cf.label2, cf.reference
			FROM control_filter cf
			WHERE cf.control_filter_id = :filter_id
        ";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":filter_id", $filter_id, PDO::PARAM_INT);
		$data = $command->query()->read();
		if ($data) {
			$retVal = $data;
		}
		return $retVal;
	}

	public static function GetFiltersByCompany($company_id)
	{
		$sql = "
			SELECT 
				cf.*,
				DATE_FORMAT(cf.updated_on,'%m/%d/%Y') updated_on
			FROM control_filter cf
			WHERE cf.company_id = :company_id
			ORDER BY cf.control_filter_id
        ";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
		$data = $command->query()->readAll();
		if (!$data) {
			$data = array();
		}

		foreach ($data as &$item) {
			$item['recid'] = Obscure::encode($item['control_filter_id']);
			unset($item['control_filter_id']);
		}

		return $data;
	}

}