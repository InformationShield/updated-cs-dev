<?php

class CompanyRole_Ex
{
    public static function getAllRoles()
    {
        $roles = CompanyRole::model()->findAll();
        if (empty($roles)) $roles = array();
        return $roles;
    }
    
    public static function getRolesAllowedToAssign()
    {
        $myRole = Utils::contextCompanyRole();
        $criteria = new CDbCriteria();
        if ($myRole == Cons::ROLE_OWNER) {
            // owners can assign other owners
            $criteria->addCondition('company_role_id <= :company_role_id');
        } else {
            // admin can not assign other admins, only an owner can assign admins
            $criteria->addCondition('company_role_id < :company_role_id');
        }
        $criteria->params = array(':company_role_id' => $myRole);
        $criteria->order = 'company_role_id ASC';
        $roles = CompanyRole::model()->findAll($criteria);
        if (empty($roles)) $roles = array();
        return $roles;
    }
}