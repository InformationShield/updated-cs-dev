<?php

class CyberRiskScore_Ex extends BaseSearchSort_Ex
{
    public static function getAnswers($companyId)
    {
        $output = CyberRiskScore::model()->findByAttributes(array('company_id' => $companyId));
        return $output;
    }
    
    public static function saveAnswers($companyId, $userId, $answers)
    {
        if(!$companyId || !$userId || !(is_array($answers))){
            return false;
        }
        
        $cyberRiskScoreModel = CyberRiskScore_Ex::getAnswers($companyId);
        if(!$cyberRiskScoreModel){
            $cyberRiskScoreModel = new CyberRiskScore;
            $cyberRiskScoreModel->created_on = date("Y-m-d H:i:s");
        }
        
        $cyberRiskScoreModel->company_id = $companyId;
        $cyberRiskScoreModel->user_id = $userId;
        $cyberRiskScoreModel->answers = json_encode($answers);
        $cyberRiskScoreModel->assessed_on = date("Y-m-d H:i:s");
        
        if($cyberRiskScoreModel->validate()){
            if($cyberRiskScoreModel->save()){
                return true;
            }
            return false;
        }
        return false;
    }
    
    public static function getReportDate($companyId)
    {
        $output = array();
        $data = self::getAnswers($companyId);
        
        if($data){
            // all data for now is fixed becouse we do not store any questions in database
            // there are 12 questions and max amount of points you can get for each is 4
            // first answers has 0 points
            
            $total_score = 0;
            $max_score = 12 * 4;
            
            $answers = json_decode($data->answers);
            
            // calculate total score
            $total_score = $answers->question1 + $answers->question2 + $answers->question3 + $answers->question4 + $answers->question5 + $answers->question6 + $answers->question7 + $answers->question8 + $answers->question9 + $answers->question10 + $answers->question11 +$answers->question12;
            
            // category 1: Organization Size and Score
            $output['data'][] = array(
                'category' => 'Organization Size and Score',
                'score' => ($answers->question1 + $answers->question2 + $answers->question3),
                'target' => 3 * 4, // 3 questions in this category
            );
            
            // category 2: Data Types and Volume
            $output['data'][] = array(
                'category' => 'Data Types and Volume',
                'score' => ($answers->question4 + $answers->question5 + $answers->question6),
                'target' => 3 * 4, // 3 questions in this category
            );
            
            // category 3: Information Technology Infrastructure
            $output['data'][] = array(
                'category' => 'Information Technology Infrastructure',
                'score' => ($answers->question7 + $answers->question8 + $answers->question9),
                'target' => 3 * 4, // 3 questions in this category
            );
            
            // category 4: Third Party Risk
            $output['data'][] = array(
                'category' => 'Third Party Risk',
                'score' => ($answers->question10 + $answers->question11),
                'target' => 2 * 4, // 2 questions in this category
            );
            
            // category 5: Web Site and External Exposure
            $output['data'][] = array(
                'category' => 'Web Site and External Exposure',
                'score' => ($answers->question12),
                'target' => 1 * 4, // 1 question in this category
            );
            
            $output['max_score'] = $max_score;
            $output['total_score'] = $total_score;
        }
        return $output;
    }

    public static function calcCyberRiskScore($companyId, $baselineId) {
		$sql = "
			UPDATE control_answer ca
			INNER JOIN control_baseline cb ON ca.control_baseline_id = cb.control_baseline_id AND cb.baseline_id = :baseline_id
			SET ca.score = ca.control_status_id * cb.weighting
			WHERE ca.company_id = :company_id 
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(":company_id", $companyId, PDO::PARAM_INT);
		$command->bindValue(":baseline_id", $baselineId, PDO::PARAM_INT);
		$command->execute();
	}

	public static function getCyberRiskScore($companyId, $baselineId) {
		$retVal = array();
		$sql = "
			SELECT cb.cat_id, cb.cat_code, cb.cat_override, 
				IFNULL(ca.score, 0) score, cb.weighting * 5 max_score, ca.assessed_on,
				cc.category, cc2.category cat_override_desc,
				b.baseline_name, b.baseline_description
			FROM control_baseline cb
				INNER JOIN control_category cc ON cc.control_category_id = cb.cat_id
				INNER JOIN baseline b ON b.baseline_id = cb.baseline_id
				LEFT JOIN control_category cc2 ON cb.cat_override = cc2.control_category_id 
				LEFT JOIN control_answer ca ON ca.control_baseline_id = cb.control_baseline_id AND ca.company_id = :company_id
			WHERE cb.baseline_id = :baseline_id
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(":company_id", $companyId, PDO::PARAM_INT);
		$command->bindValue(":baseline_id", $baselineId, PDO::PARAM_INT);
		$results = $command->query()->readAll();
		if (!empty($results)) {
			$scoreInfo = array('total_score'=>0, 'total_max'=>0, 'total_percentage'=>0);
			$scores = array();
			$categories = ControlCategory_Ex::getTreeNodes();
			foreach ($results as $i=>$score) {
				if ($score['cat_override']) {
					$catId = $score['cat_override'];
					$category = $score['cat_override_desc'];
				} else {
					$rootNode = null;
					ControlCategory_Ex::findRootOfNodeInTree($categories, $score['cat_id'], $rootNode,
						function (&$node, &$data) {	}, $c
					);
					$catId = $rootNode['id'];
					$category = $rootNode['text'];
				}
				$category = preg_replace("/^[^\s]*[\s]/i", "", $category); // remove numbers from beginning of the string
				if (isset($scores[$category])) {
					$scores[$category]['score'] += $score['score'];
					$scores[$category]['max_score'] += $score['max_score'];
				} else {
					$scores[$category] = $score;
				}
				$scores[$category]['category'] = $category;
				//$scores[$category]['percentage'] = floor(($scores[$category]['score'] / $scores[$category]['max_score']) * 100);
				$scoreInfo['total_score'] += $score['score'];
				$scoreInfo['total_max'] += $score['max_score'];
			}

			// apply scale of 0 - 1000
			$multiplier = 1000 / $scoreInfo['total_max'];
			foreach($scores as $i=>$score) {
				$score['score'] *= $multiplier ;
				$score['max_score'] *= $multiplier ;
				$score['score'] = round($score['score']);
				$score['max_score'] = round($score['max_score']);
				$score['percentage'] = round($score['score'] / $score['max_score'] * 100) ;
				$scores[$i] = $score;
			}

			$scoreInfo['category_scores'] = $scores;
			$scoreInfo['total_score'] *= $multiplier ;
			$scoreInfo['total_max'] *= $multiplier ;
			$scoreInfo['total_percentage'] = round(($scoreInfo['total_score'] / $scoreInfo['total_max']) * 100);
			$scoreInfo['total_score'] = round($scoreInfo['total_score']);
			$scoreInfo['total_max'] = round($scoreInfo['total_max']);
			$scoreInfo['assessed_on'] = DateHelper::FormatDateTimeString($results[0]['assessed_on'], 'm/d/y');

			// Save company cyber risk score
			$companyInfo = CompanyInfo::model()->findByPk($companyId);
			if ($companyInfo) {
				$companyInfo->isNewRecord = false;
				$companyInfo->cyber_risk_score = $scoreInfo['total_score'];
				$companyInfo->save();
			}

			$retVal = $scoreInfo;
		}
		return $retVal;
	}

}