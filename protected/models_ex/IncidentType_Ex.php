<?php
/* @var $incident IncidentType */

class IncidentType_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY incident_type asc";

    // copy default incidents types if the company has none
    public static function copyDefault($company_id)
    {
        $existing = IncidentType::model()->findAllByAttributes(array('company_id'=>$company_id));
        if (isset($existing) && count($existing) > 0) {
            return false;
        }
        $models = IncidentType::model()->findAllByAttributes(array('company_id'=>null));
        if ($models) {
            foreach($models as $orig) {
                $new = new IncidentType();
                $attributes = $orig->attributes;
                unset($attributes['incident_type_id']);
                $new->attributes = $attributes;
                $new->company_id = $company_id;
                if (!$new->save()) {
                    Yii::log(print_r($new->getErrors(), true), 'error', 'IncidentType.copyDefault');
                }
            }
        }
        return true;
    }

    public static function getViewValues($companyId,$incidentTypeId)
    {
        $v = array();
        $row = self::getIncidentType($count,$companyId,$incidentTypeId,0,0,null,null,null,null);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Type'] = $model->incident_type;
            $v['Description'] = $model->incident_description;
        }
        return $v;
    }

    public static function getIncidentType(&$count,$companyId,$incidentTypeId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT *";
        }
        $from = " FROM incident_type inc";
        $sqlWhere = " WHERE inc.company_id = :companyId";
        if ($incidentTypeId !== null) {
            $sqlWhere .= " AND inc.incident_type_id = :incident_type_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $incidentTypeId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($incidentTypeId !== null) {
                $cmdCount->bindParam(":incident_type_id", $incidentTypeId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($incidentTypeId !== null) {
            $command->bindParam(":incident_type_id", $incidentTypeId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($incidentTypeId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getIncidentTypesJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT inc.incident_type_id, inc.incident_type, inc.incident_description";
        $rows = IncidentType_Ex::getIncidentType($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['incident_type_id'] = Obscure::encode($row['incident_type_id']);
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

}