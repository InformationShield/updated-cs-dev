<?php

class Invite_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY invite_id asc";
    
    public static function getViewValues($companyId,$inviteId)
    {
        $v = array();
        $row = self::getInvite($count,$companyId,$inviteId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Invitee Name'] = $model->invitee_name;
            $v['Invitee Email'] = $model->invitee_email;
            $v['Baseline Name'] = $model->baseline_name;
            $v['Created On'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y');
            $v['Expiry Date'] = DateHelper::FormatDateTimeString($model->expiry_date, 'm/d/Y');
            $v['Sent'] = ($model->sent == 0) ? 'Not sent' : (($model->sent == 1) ? 'Sent' : 'Failed to send.');
            if ($model->sent != 0) {
                $v['Sent Date'] = DateHelper::FormatDateTimeString($model->sent_date, 'm/d/Y');
            }
            $v['Accepted'] = ($model->accepted == 0) ? 'Not yet' : (($model->accepted == 1) ? 'Accepted' : 'Rejected');
            if ($model->accepted != 0) {
                $v['Accepted Date'] = DateHelper::FormatDateTimeString($model->accepted_date, 'm/d/Y');
            }
        }
        return $v;
    }

    public static function getInviteCount($companyId)
    {
        $retVal = 0;
        $sql = "SELECT COUNT(i.invite_id) total"
            ." FROM invite i"
            ." WHERE i.status=".Cons::ACTIVE." AND company_id = :companyId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results)) {
            $retVal = $results['total'];
        }
        return $retVal;
    }

    public static function getInvite(&$count,$companyId=NULL,$inviteId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT i.*, b.baseline_name, cp.name as company_name";
        }
        $from = " FROM invite i"
                ." INNER JOIN company cp ON i.company_id=cp.company_id"
                ." LEFT JOIN baseline b ON i.baseline_id=b.baseline_id";
        $sqlWhere = " WHERE i.status = ".Cons::ACTIVE;
        if (!is_null($companyId)) {
            $sqlWhere .= " AND i.company_id=:companyId";
        }
        if (!empty($inviteId)) {
            $sqlWhere .= " AND i.invite_id = :invite_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $inviteId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            if (!is_null($companyId)) {
                $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            }
            if (!empty($inviteId)) {
                $cmdCount->bindParam(":invite_id", $inviteId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        if (!is_null($companyId)) {
            $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        }
        if (!empty($inviteId)) {
            $command->bindParam(":invite_id", $inviteId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if (!empty($inviteId)) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getInvitesJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT i.invite_id, i.invitee_name, i.invitee_email, i.created_on, i.sent, i.sent_date, i.accepted, i.accepted_date, i.expiry_date, b.baseline_name";
        $rows = self::getInvite($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as $key=>&$row) {
                if (!empty($row['invite_id'])) {
                    $row['created_on'] = DateHelper::FormatDateTimeString($row['created_on'], 'm/d/Y');
                    $row['sent_date'] = DateHelper::FormatDateTimeString($row['sent_date'], 'm/d/Y');
                    $row['accepted_date'] = DateHelper::FormatDateTimeString($row['accepted_date'], 'm/d/Y');
                    $row['expiry_date'] = DateHelper::FormatDateTimeString($row['expiry_date'], 'm/d/Y');
                    $row['invite_id'] = Obscure::encode($row['invite_id']);
                } else {
                    unset($rows[$key]);
                    $count--;
                }
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

}