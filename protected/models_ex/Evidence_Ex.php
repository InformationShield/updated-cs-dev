<?php
/* @var $evidence Evidence */

class Evidence_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY doc_title asc, e.created_on asc";

    public static function getViewValues($companyId,$evidenceId)
    {
        $v = array();
        $row = self::getEvidence($count,$companyId,$evidenceId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Title'] = $model->doc_title;
            $v['Uploaded On'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y g:i:s a');
            $v['Evidence File'] = $model->file_name;
            $v['Related Control'] = $model->related_control;
            $v['Category'] = $model->category;
            $v['MIME Type'] = $model->file_type;
            $v['File Size'] = Utils::FileSizeString($model->file_size);
        }
        return $v;
    }

    public static function saveFile($tmpFileFullPath,$evidence)
    {
        $mUploadedFile = new mUploadedFiles("files/evidence/",$evidence->company_id);
        return $mUploadedFile->saveFile($tmpFileFullPath,$evidence->doc_guid);
    }

    public static function removeFile($evidence)
    {
        $mUploadedFile = new mUploadedFiles("files/evidence/",$evidence->company_id);
        return $mUploadedFile->removeFile($evidence->doc_guid);
    }

    public static function downloadFile($evidence)
    {
        $mUploadedFile = new mUploadedFiles("files/evidence/",$evidence->company_id);
        return $mUploadedFile->downloadFile($evidence->doc_guid,$evidence->file_type,$evidence->file_name);
    }

    public static function getTmpFileUrl($evidence)
    {
        $mUploadedFile = new mUploadedFiles("files/evidence/",$evidence->company_id);
        return $mUploadedFile->getTmpFileUrl($evidence->doc_guid,$evidence->file_name);
    }

    public static function getEvidence(&$count,$companyId,$evidenceId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT e.*, cb.title related_control, cc.category";
        }
        $from = " FROM evidence e"
               ." LEFT JOIN control_baseline cb ON e.control_baseline_id=cb.control_baseline_id"
               ." LEFT JOIN control_category cc ON cb.cat_id=cc.control_category_id";
        $sqlWhere = " WHERE e.company_id = :companyId";
        if ($evidenceId) {
            $sqlWhere .= " AND e.evidence_id = :evidence_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere2 = str_ireplace('created_on','e.created_on',$sqlWhere2);
            $sqlWhere2 = str_ireplace('e_uploaded_on','DATE(e.created_on)',$sqlWhere2);
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $evidenceId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($evidenceId) {
                $cmdCount->bindParam(":evidence_id", $evidenceId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($evidenceId) {
            $command->bindParam(":evidence_id", $evidenceId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($evidenceId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getEvidencesJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT e.evidence_id, e.doc_title, e.created_on, DATE(e.created_on) as e_uploaded_on, e.file_name, 
            e.file_type, cb.title";
        $rows = Evidence_Ex::getEvidence($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['evidence_id'] = Obscure::encode($row['evidence_id']);
                $row['created_on'] = DateHelper::FormatDateTimeString($row['created_on'], 'm/d/Y g:i:s a');
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

}