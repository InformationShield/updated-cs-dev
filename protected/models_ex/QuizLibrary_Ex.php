<?php
/* @var $quizLibrary QuizLibrary */
/* @var $quiz Quiz */

class QuizLibrary_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY quiz_title asc, publish_date asc";

    public static function getViewValues($quizLibraryId)
    {
        $v = array();
        $row = QuizLibrary_Ex::getQuizLibrary($count,$quizLibraryId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Title'] = $model->quiz_title;
            $v['Description'] = $model->description;
            $v['Status'] = $model->quiz_status_name;
            $v['Created Date'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y');
            if ($model->quiz_status_id==Cons::POLICY_STATUS_PUBLISHED) {
                $v['Published Date'] = DateHelper::FormatDateTimeString($model->publish_date, 'm/d/Y');
            }
            $v['Passing Score'] = $model->passing_score;
            $v['Maximum Retakes'] = $model->max_retakes;
        }
        return $v;
    }

    public static function copyQuiz($quiz_library_id,$quiz_id,$company_id,$user_id)
    {
        $copySuccess = true;
        $criteria = new CDbCriteria(array('order' => 'sort_order asc, quiz_question_id asc'));
        $questions = QuizQuestion::model()->findAllByAttributes(array('quiz_library_id' => $quiz_library_id, 'status' => 1), $criteria);
        foreach($questions as $question) {
            $newQuestion = new QuizQuestion();
            $attrs = $question->attributes;
            unset($attrs['quiz_question_id']);
            $newQuestion->attributes = $attrs;
            $newQuestion->company_id = $company_id;
            $newQuestion->user_id = $user_id;
            $newQuestion->quiz_library_id = null;
            $newQuestion->quiz_id = $quiz_id;
            if (!$newQuestion->save()) {
                $copySuccess = false;
            }
        }
        return $copySuccess;
    }

    public static function copyQuizLibrary($quiz_library_id,$company_id,$user_id)
    {
        $errMsg = false;
        $quizLibrary = QuizLibrary::model()->findByAttributes(array('quiz_library_id' => $quiz_library_id));
        if (isset($quizLibrary)) {
            $quiz = Quiz::model()->findByAttributes(array('quiz_library_id' => $quiz_library_id, 'company_id' => $company_id, 'status'=>1));
            if (isset($quiz)) {
                $errMsg = 'The quiz library item was not added since it already exists in your quiz.';
            } else {
                $quiz = new Quiz;
                $quiz->attributes = $quizLibrary->attributes;
                // override values as needed
                $quiz->user_id = $user_id;
                $quiz->quiz_library_id = $quizLibrary->quiz_library_id;
                $quiz->company_id = $company_id;
                $quiz->status = 1; // active
                $quiz->quiz_status_id = 1; // draft
                $quiz->created_on = date('Y-m-d H:i:s');
                $quiz->publish_date = date('Y-m-d H:i:s');

                if ($quiz->save()) {
                    $copySuccess = QuizLibrary_Ex::copyQuiz($quiz_library_id,$quiz->primaryKey,$company_id,$user_id);
                    if (!$copySuccess) {
                        $errMsg = 'The quiz library item failed to copy to your quiz.';
                    }
                } else {
                    Yii::log(print_r($quiz->getErrors(), true), 'error', 'QuizlibraryController.actionCopy');
                    $errMsg = 'The quiz library item failed to save to your quiz.';
                }
            }
        } else {
            $errMsg = 'The quiz library item was not found in the Quiz Library.';
        }
        return $errMsg;
    }

    public static function getQuizLibrary(&$count,$quizLibraryId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT t.*, ts.quiz_status_name";
        }
        $from = " FROM quiz_library t"
               ." INNER JOIN quiz_status ts ON t.quiz_status_id=ts.quiz_status_id";
        $sqlWhere = " WHERE t.status=1";
        if (!Utils::isSuper()) { // only admins should see non-published quizzes
            $sqlWhere .= " AND t.quiz_status_id=3";
        }
        if ($quizLibraryId) {
            $sqlWhere .= " AND t.quiz_library_id = :quiz_library_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $quizLibraryId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            if ($quizLibraryId) {
                $cmdCount->bindParam(":quiz_library_id", $quizLibraryId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        if ($quizLibraryId) {
            $command->bindParam(":quiz_library_id", $quizLibraryId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($quizLibraryId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getQuizLibraryJson($pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT t.quiz_library_id, t.quiz_title, t.publish_date, t.description, t.quiz_status_id, ts.quiz_status_name";
        $rows = QuizLibrary_Ex::getQuizLibrary($count,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['quiz_library_id'] = Obscure::encode($row['quiz_library_id']);
                if ($row['quiz_status_id'] == Cons::POLICY_STATUS_PUBLISHED) {
                    $row['publish_date'] = DateHelper::FormatDateTimeString($row['publish_date'], 'm/d/Y');
                } else {
                    $row['publish_date'] = '';
                }
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
}