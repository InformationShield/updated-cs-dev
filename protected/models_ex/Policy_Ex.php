<?php
/* @var $policy Policy */

class Policy_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY doc_title asc, created_on asc";

    public static function getViewValues($companyId,$policyId)
    {
        $v = array();
        $row = self::getPolicy($count,$companyId,$policyId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Title'] = $model->doc_title;
            $v['Publish Date'] = DateHelper::FormatDateTimeString($model->publish_date, 'm/d/Y');
            $v['Review Date'] = DateHelper::FormatDateTimeString($model->expiry_date, 'm/d/Y');
            $v['Author'] = $model->author;
            $v['Keywords'] = $model->keywords;
            //$v['Category'] = $model->category; //dave says not needed
			$v['Type'] = isset(mPolicy::$PolicyTypes[$model->policy_type]) ? mPolicy::$PolicyTypes[$model->policy_type] : '';
            $v['Approved'] = ($model->approved) ? 'Approved' : 'Un-Approved';
            $v['Policy Status'] = $model->policy_status_name;
            $v['Filename'] = $model->file_name;
            $v['Description'] = $model->description;
            //$v['MIME Type'] = $model->file_type;
            //$v['File Size'] = Utils::FileSizeString($model->file_size);
        }
        return $v;
    }

    public static function saveFile($tmpFileFullPath,$policy)
    {
        $mUploadedFile = new mUploadedFiles("files/policy/",$policy->company_id);
        return $mUploadedFile->saveFile($tmpFileFullPath,$policy->doc_guid);
    }

    public static function removeFile($policy)
    {
        $mUploadedFile = new mUploadedFiles("files/policy/",$policy->company_id);
        return $mUploadedFile->removeFile($policy->doc_guid);
    }

    public static function downloadFile($policy)
    {
        $mUploadedFile = new mUploadedFiles("files/policy/",$policy->company_id);
        return $mUploadedFile->downloadFile($policy->doc_guid,$policy->file_type,$policy->file_name);
    }

    public static function getTmpFileUrl($policy)
    {
        $mUploadedFile = new mUploadedFiles("files/policy/",$policy->company_id);
        return $mUploadedFile->getTmpFileUrl($policy->doc_guid,$policy->file_name);
    }

    public static function getPolicyCountsByStatus($companyId,$policy_status)
    {
        $retVal = 0;
        $sql = "SELECT COUNT(p.policy_id) total"
            ." FROM policy p"
            ." WHERE p.policy_status_id=:policy_status_id AND p.status=".Cons::ACTIVE." AND company_id = :companyId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $command->bindParam(":policy_status_id", $policy_status, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (isset($results) && isset($results['total'])) {
            $retVal = $results['total'];
        }
        return $retVal;
    }

    public static function getPolicyStatusCounts($companyId)
    {
        $retVal = array();
        $sql = "SELECT COUNT(p.policy_status_id) total, ps.policy_status_name, ps.policy_status_id"
            ." FROM policy p"
            ." INNER JOIN policy_status ps ON p.policy_status_id=ps.policy_status_id"
            ." WHERE p.status=".Cons::ACTIVE." AND company_id = :companyId"
            ." GROUP BY p.policy_status_id"
            ." ORDER BY p.policy_status_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

    public static function getPolicy(&$count,$companyId,$policyId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null,$status_id=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT p.*, ps.policy_status_name, cc.category";
        }
        $from = " FROM policy p"
               ." INNER JOIN policy_status ps ON p.policy_status_id=ps.policy_status_id"
               ." LEFT JOIN control_category cc ON p.cat_id=cc.control_category_id";
        $sqlWhere = " WHERE p.company_id = :companyId AND p.status=".Cons::ACTIVE;
        if ($status_id !== null) {
            $sqlWhere .= " AND p.policy_status_id = :policy_status_id";
        }
        if ($policyId) {
            $sqlWhere .= " AND p.policy_id = :policy_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $policyId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($status_id !== null) {
                $cmdCount->bindParam(":policy_status_id", $status_id, PDO::PARAM_INT);
            }
            if ($policyId) {
                $cmdCount->bindParam(":policy_id", $policyId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($status_id !== null) {
            $command->bindParam(":policy_status_id", $status_id, PDO::PARAM_INT);
        }
        if ($policyId) {
            $command->bindParam(":policy_id", $policyId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($policyId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getPolicyJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null,$status_id=null)
    {
        $select = "SELECT p.policy_id, p.doc_title, p.publish_date, p.expiry_date, ps.policy_status_name, p.author, p.policy_type";
        $rows = Policy_Ex::getPolicy($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort,$status_id);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['policy_id'] = Obscure::encode($row['policy_id']);
                $row['publish_date'] = DateHelper::FormatDateTimeString($row['publish_date'], 'm/d/Y');
				$row['policy_type'] = isset(mPolicy::$PolicyTypes[$row['policy_type']]) ? mPolicy::$PolicyTypes[$row['policy_type']] : '';
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
}