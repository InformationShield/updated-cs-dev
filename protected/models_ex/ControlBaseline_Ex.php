<?php

class ControlBaseline_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY cb.cat_id asc, cb.sort_order asc";

    public static function getRelatedControls()
    {
        $baseline_id = Baseline_Ex::getCurrentBaselineId();
        $controls = ControlBaseline_Ex::getBaselineControl($count,Utils::contextCompanyId(),$baseline_id);
        return $controls;
    }

    public static function getViewValues($companyId, $controlBaselineId)
    {
        $v = array();
        $row = ControlBaseline_Ex::getBaselineControl($count, $companyId, null, $controlBaselineId);
        if (!empty($row)) {
            $model = (object)$row;
            $v['Control ID'] = $model->control_id;
            $v['Category'] = $model->category;
            $v['Title'] = $model->title;
            $v['Status'] = $model->control_status_name;
            $v['Target Date'] = DateHelper::FormatDateTimeString($model->target_date, 'm/d/Y');
            $v['Priority'] = $model->priority;
            $v['Assigned'] = (empty($model->role_title)) ? 'unassigned' : $model->role_title;
            $v['Score'] = $model->score;
            $v['Weighting'] = $model->weighting;
            $v['Label 1'] = $model->label1;
            $v['Label 2'] = $model->label2;
            $v['Description'] = $model->detail;
            $v['Sample Policy'] = $model->rel_policy;
            $v['Guidance'] = $model->guidance;
            $v['Example Evidence'] = $model->evidence;
            $v['Regulatory Reference'] = $model->reference;
            $v['Assessment Question'] = $model->question;
        }
        return $v;
    }

    public static function getControlCount($companyId, $baseline_id = null)
    {
        $retVal = 0;
        $sql = "SELECT COUNT(cb.control_baseline_id) total"
            . " FROM control_baseline cb"
            . " WHERE cb.status=" . Cons::ACTIVE . " AND company_id = :companyId";
        if ($baseline_id) {
            $sql .= " AND cb.baseline_id = :baseline_id";
        }
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($baseline_id) {
            $command->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
        }
        $results = $command->query()->read();
        if (!empty($results)) {
            $retVal = $results['total'];
        }
        return $retVal;
    }

    public static function getControlStatusCounts($companyId, $baseline_id = null)
    {
        $retVal = array();
        $select = "SELECT COUNT(cb.control_status_id) total, cs.control_status_name, cs.control_status_id";
        $from = " FROM control_baseline cb"
            . " INNER JOIN control_status cs ON cb.control_status_id=cs.control_status_id";
        $sqlWhere = " WHERE cb.status=" . Cons::ACTIVE . " AND company_id = :companyId";
        if ($baseline_id) {
            $sqlWhere .= " AND cb.baseline_id = :baseline_id";
        }
        $groupBy = " GROUP BY cb.control_status_id";
        $orderBy = " ORDER BY cb.control_status_id";
        $sql = $select . $from . $sqlWhere . $groupBy . $orderBy;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($baseline_id) {
            $command->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

    public static function getBaselineControlScores($companyId, $baseline_id = null)
    {
        $retVal = array();
        $select = "SELECT cb.title, cb.cat_id, cb.weighting, cb.control_status_id,"
            . " (cb.weighting * cb.control_status_id) score, cc.parent_id, cs.control_status_name";
        $from = " FROM control_baseline cb"
            . " INNER JOIN control_status cs ON cb.control_status_id=cs.control_status_id"
            . " INNER JOIN control_category cc ON cb.cat_id=cc.control_category_id";
        $sqlWhere = " WHERE cb.status = " . Cons::ACTIVE . " AND cb.company_id = :companyId";
        if ($baseline_id) {
            $sqlWhere .= " AND cb.baseline_id = :baseline_id";
        }
        $orderBy = " ORDER BY parent_id asc, control_category_id asc";
        $sql = $select . $from . $sqlWhere . $orderBy;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($baseline_id) {
            $command->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

	public static function getControlStatusByFilter($company_id, $baseline_id, $filter=null)
	{
		$retVal = array();
		BaseSearchSort_Ex::filter2Sql($filter, $whereSql, $sortSql, $params, 'cb.');
		$sql = "
			SELECT COUNT(cb.control_status_id) total, cs.control_status_name, cs.control_status_id
			FROM control_baseline cb
				INNER JOIN control_status cs ON cb.control_status_id=cs.control_status_id
			WHERE cb.status=" . Cons::ACTIVE . " 
				AND company_id = :company_id
				AND cb.baseline_id = :baseline_id
				$whereSql
			GROUP BY cb.control_status_id
			ORDER BY cb.control_status_id
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
		$command->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
		if (!empty($params)) {
			foreach ($params as $param) {
				$command->bindValue($param['name'], $param['value'], $param['type']);
			}
		}
		$results = $command->query()->readAll();
		if (!empty($results)) {
			$retVal = $results;
		}
		return $retVal;
	}

	public static function getControlScoresByFilter($company_id, $baseline_id, $filter = null)
	{
		$retVal = array();

		BaseSearchSort_Ex::filter2Sql($filter, $whereSql, $sortSql, $params, 'cb.');
		$sql = "
			SELECT cb.title, cb.cat_id, cb.weighting, cb.control_status_id,
				(cb.weighting * cb.control_status_id) score, cc.parent_id, cs.control_status_name
			FROM control_baseline cb
				INNER JOIN control_status cs ON cb.control_status_id=cs.control_status_id
				INNER JOIN control_category cc ON cb.cat_id=cc.control_category_id
			WHERE cb.status = " . Cons::ACTIVE . " 
				AND cb.company_id = :company_id
				AND cb.baseline_id = :baseline_id
				$whereSql
			ORDER BY parent_id asc, control_category_id asc
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
		$command->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
		if (!empty($params)) {
			foreach ($params as $param) {
				$command->bindValue($param['name'], $param['value'], $param['type']);
			}
		}
		$results = $command->query()->readAll();
		if (!empty($results)) {
			$retVal = $results;
		}
		return $retVal;
	}

	public static function getBaselineControl(&$count, $companyId, $baseline_id = null, $controlBaselineId = null, $pageSize = 0, $offsetBy = 0, $select = null, $search = null, $searchLogic = null, $sort = null, $status_id = null, $securityRoleId = null, $fixedRowLimit = null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select == null) {
            $select = "SELECT cb.*, cc.control_category_id, cc.parent_id, cc.root_id, cc.category, cc.description, cc.status as control_category_status, cs.control_status_name, sr.role_title";
        }
        $from = " FROM control_baseline cb"
            . " LEFT JOIN control_status cs ON cb.control_status_id=cs.control_status_id"
            . " LEFT JOIN control_category cc ON cb.cat_id=cc.control_category_id"
            . " LEFT JOIN control_library cl ON cb.control_id=cl.control_id"
            . " LEFT JOIN security_role sr ON sr.security_role_id=cb.security_role_id"
            . " LEFT JOIN evidence e ON e.control_baseline_id = cb.control_baseline_id";
        $sqlWhere = " WHERE cb.status = " . Cons::ACTIVE . " AND cb.company_id = :companyId";
        if ($baseline_id) {
            $sqlWhere .= " AND cb.baseline_id = :baseline_id";
        }
        if ($controlBaselineId) {
            $sqlWhere .= " AND cb.control_baseline_id = :control_baseline_id";
        }
        if ($status_id !== null) {
            $sqlWhere .= " AND cb.control_status_id = :control_status_id";
        }
        if ($securityRoleId !== null) {
            $sqlWhere .= " AND sr.security_role_id = :security_role_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search, $searchLogic, $params, "cb.");
        if ($sqlWhere2) {
            $sqlWhere2 = str_ireplace('cb.id', 'control_baseline_id', $sqlWhere2);
            $sqlWhere2 = str_ireplace('cb.jobrole', 'jobrole', $sqlWhere2);
            $sqlWhere2 = str_ireplace('cb.role_title', 'role_title', $sqlWhere2);
            $sqlWhere2 = str_ireplace('cb.category', 'category', $sqlWhere2);
            $sqlWhere2 = str_ireplace('cb.control_status_name', 'control_status_name', $sqlWhere2);
			$sqlWhere2 = str_ireplace('cb.root_id', 'root_id', $sqlWhere2);
            $sqlWhere .= ' AND ' . $sqlWhere2;
        }
        $sqlGroup = " GROUP BY cb.control_baseline_id";
        if($fixedRowLimit > 0){ // TODO: review this
            $sqlLimit = " LIMIT " . $offsetBy . ", " . $fixedRowLimit;
            $sqlCount = "SELECT COUNT(*) totRows FROM (" . $select . $from . $sqlWhere . $sqlGroup . $sqlLimit . ") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($baseline_id) {
                $cmdCount->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
            }
            if ($controlBaselineId) {
                $cmdCount->bindParam(":control_baseline_id", $controlBaselineId, PDO::PARAM_INT);
            }
            if ($status_id !== null) {
                $cmdCount->bindParam(":control_status_id", $status_id, PDO::PARAM_INT);
            }
            if ($securityRoleId !== null) {
                $cmdCount->bindParam(":security_role_id", $securityRoleId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        } else if ($pageSize > 0) {
            $sqlLimit = " LIMIT " . $offsetBy . ", " . $pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (" . $select . $from . $sqlWhere . $sqlGroup . ") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($baseline_id) {
                $cmdCount->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
            }
            if ($controlBaselineId) {
                $cmdCount->bindParam(":control_baseline_id", $controlBaselineId, PDO::PARAM_INT);
            }
            if ($status_id !== null) {
                $cmdCount->bindParam(":control_status_id", $status_id, PDO::PARAM_INT);
            }
            if ($securityRoleId !== null) {
                $cmdCount->bindParam(":security_role_id", $securityRoleId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select . $from . $sqlWhere . $sqlGroup . $orderBy . $sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($baseline_id) {
            $command->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
        }
        if ($controlBaselineId) {
            $command->bindParam(":control_baseline_id", $controlBaselineId, PDO::PARAM_INT);
        }
        if ($status_id !== null) {
            $command->bindParam(":control_status_id", $status_id, PDO::PARAM_INT);
        }
        if ($securityRoleId !== null) {
            $command->bindParam(":security_role_id", $securityRoleId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($controlBaselineId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
                if ($pageSize <= 0) {
                    $count = count($results);
                }
            }
        }
        return $retVal;
    }

    public static function getBaselineControlsArray($companyId, $baseline_id = null, $controlBaselineId = null, $pageSize = 0, $offsetBy = 0, $search = null, $searchLogic = null, $sort = null, $status_id = null, $securityRoleId = null, $fixedRowLimit = null)
    {
        $select = "
            SELECT cb.control_baseline_id, cb.control_baseline_id as id, cb.priority, cb.title, category, cb.cat_id, 
            cb.control_status_id, control_status_name, cb.target_date, cb.label1, cb.label2, cb.control_id, cb.detail, 
            cb.guidance, cl.jobrole, cb.evidence, cb.rel_policy, cb.reference, cb.question, cb.assessed_on, cb.score, 
            cb.weighting, sr.role_title, e.evidence_id, e.file_name, e.file_type, e.doc_guid, e.doc_title";
        $rows = ControlBaseline_Ex::getBaselineControl($count, $companyId, $baseline_id, $controlBaselineId, $pageSize, $offsetBy, $select, $search, $searchLogic, $sort, $status_id, $securityRoleId, $fixedRowLimit);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['target_date'] = DateHelper::FormatDateTimeString($row['target_date'], 'm/d/Y');
                $row['control_baseline_id'] = Obscure::encode($row['control_baseline_id']);
                $row['evidence_id'] = Obscure::encode($row['evidence_id']);
            }
        }
        $array = array("status" => "success", "total" => $count, "records" => ($count == 0) ? array() : $rows);
        return $array;
    }

    public static function getBaselineControlsJson($companyId, $baseline_id = null, $controlBaselineId = null, $pageSize = 0, $offsetBy = 0, $search = null, $searchLogic = null, $sort = null, $status_id = null, $securityRoleId = null, $fixedRowLimit = null)
    {
        $array = ControlBaseline_Ex::getBaselineControlsArray($companyId, $baseline_id, $controlBaselineId, $pageSize, $offsetBy, $search, $searchLogic, $sort, $status_id, $securityRoleId, $fixedRowLimit);
        $json = json_encode($array);
        return $json;
    }

    public static function getSituationReport($companyId, $baselineId) {
		$retVal = array();
		$sql = "
			SELECT *
			FROM (
				SELECT
					cb.control_baseline_id,
					cb.control_baseline_id AS id,
					cb.priority,
					cb.title,
					cc.category,
					cb.cat_id,
					cb.control_status_id,
					cs.control_status_name,
					cb.target_date,
					cb.label1,
					cb.label2,
					cb.control_id,
					cb.detail,
					cb.guidance,
					cl.jobrole,
					cb.evidence,
					cb.rel_policy,
					cb.reference,
					cb.question,
					cb.assessed_on,
					cb.score,
					cb.weighting,
					sr.role_title
				FROM control_baseline cb 
				    INNER JOIN control_status cs ON cb.control_status_id = cs.control_status_id
					INNER JOIN control_category cc ON cb.cat_id = cc.control_category_id
					INNER JOIN control_library cl ON cb.control_id = cl.control_id
					LEFT JOIN security_role sr ON sr.security_role_id = cb.security_role_id
				WHERE cb.status = 1 
					AND cb.company_id = :companyId 
					AND cb.baseline_id = :baselineId
					AND cb.control_status_id IN (0, 1, 2)
				ORDER BY cb.priority ASC, cb.weighting DESC
				LIMIT 0, 10
			) AS SituationReport
			ORDER BY control_status_id;
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
		$command->bindParam(":baselineId", $baselineId, PDO::PARAM_INT);
		$results = $command->query()->readAll();
		if (!empty($results)) {
			foreach ($results as &$row) {
				$row['control_baseline_id'] = Obscure::encode($row['control_baseline_id']);
				$row['id'] = Obscure::encode($row['id']);
			}
			$retVal = $results;
		}
		return $retVal;
	}

    public static function getBaselineControlsQuestionsArray($companyId, $baseline_id = null, $controlBaselineId = null, $pageSize = 0, $offsetBy = 0, $search = null, $searchLogic = null, $sort = null, $status_id = null, $securityRoleId = null)
    {
        $output = array();
        $data = self::getBaselineControlsArray($companyId, $baseline_id, $controlBaselineId, $pageSize, $offsetBy, $search, $searchLogic, $sort, $status_id, $securityRoleId);
        
        if ($data['status'] == 'success') {
            if ($data['total'] > 0) {
                $nodes = ControlCategory_Ex::getTreeNodes();
                $output = array('status' => 'success', 'total' => $data['total']);
                
                foreach ($data['records'] as $record) {
                    $rootNode = null;
                    ControlCategory_Ex::findRootOfNodeInTree($nodes, $record['cat_id'], $rootNode, function (&$node, &$data) {
                    }, $c);
                    
                    if ($rootNode) {
                        $output['records'][$rootNode['id']]['category'] = preg_replace("/^[^\s]*[\s]/i", "", $rootNode['text']); // remove numbers from beginning of the string
                        $output['records'][$rootNode['id']]['questions'][] = array(
                            'control_baseline_id' => $record['control_baseline_id'],
                            'id' => $record['id'],
                            'title' => $record['title'],
                            'question' => $record['question'],
                            'guidance' => $record['guidance'],
                            'selected_control_status_id' => $record['control_status_id'],
                            'assessed_on' => $record['assessed_on']
                        );
                    }
                }
            }else{
                $output = array('status' => 'success', 'total' => 0, 'records' => array());
            }
        }else{
            $output = array('status' => 'error', 'total' => 0, 'records' => array());
        }
        
        return $output;
    }

	public static function getControlsQaV2($company_id, $baseline_id)
	{
		$retVal = array();
		$sql = "
			SELECT cb.control_baseline_id, cb.control_baseline_id as id, cb.priority, cb.title, cb.cat_id, 
				cb.target_date, cb.label1, cb.label2, cb.control_id, cb.detail, cb.guidance, cl.jobrole, cb.evidence, 
				cb.rel_policy, cb.reference, cb.question, cb.weighting, cb.cat_override,
				cs.control_status_name, cc.category, cc2.category cat_override_desc,
				IFNULL(ca.control_status_id, 0) control_status_id, ca.score, ca.assessed_on,
				sr.role_title  
			FROM control_baseline cb 
				INNER JOIN control_status cs ON cb.control_status_id = cs.control_status_id 
				INNER JOIN control_category cc ON cb.cat_id = cc.control_category_id 
				INNER JOIN control_library cl ON cb.control_id = cl.control_id 
				LEFT JOIN control_category cc2 ON cb.cat_override = cc2.control_category_id 
				LEFT JOIN control_answer ca ON ca.control_baseline_id = cb.control_baseline_id AND ca.company_id = :company_id
				LEFT JOIN security_role sr ON sr.security_role_id = cb.security_role_id 
			WHERE cb.status = 1 
				AND cb.baseline_id = :baseline_id
			ORDER BY cb.cat_id, cb.sort_order
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
		$command->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
		$results = $command->query()->readAll();
		if (!empty($results)) {
			$output = array();
			$categories = ControlCategory_Ex::getTreeNodes();
			foreach ($results as $control) {
				if ($control['cat_override']) {
					$catId = $control['cat_override'];
					$category = $control['cat_override_desc'];
				} else {
					$rootNode = null;
					ControlCategory_Ex::findRootOfNodeInTree($categories, $control['cat_id'], $rootNode,
						function (&$node, &$data) {	}, $c
					);
					$catId = $rootNode['id'];
					$category = $rootNode['text'];
				}
				$output[$catId]['category'] = preg_replace("/^[^\s]*[\s]/i", "", $category); // remove numbers from beginning of the string
				$output[$catId]['questions'][] = array(
					'control_baseline_id' => Obscure::encode($control['control_baseline_id']),
					'id' => $control['id'],
					'title' => $control['title'],
					'question' => $control['question'],
					'guidance' => $control['guidance'],
					'selected_control_status_id' => $control['control_status_id'],
					'assessed_on' => $control['assessed_on']
				);
			}
			$array = array("status" => "success", "total" => count($results), "records" => $output);
			$retVal = $array;
		}
		return $retVal;
	}

    public static function getBaselineControlsQuestionsJson($companyId, $baseline_id = null, $controlBaselineId = null, $pageSize = 0, $offsetBy = 0, $search = null, $searchLogic = null, $sort = null, $status_id = null, $securityRoleId = null)
    {
        $array = ControlBaseline_Ex::getBaselineControlsQuestionsArray($companyId, $baseline_id, $controlBaselineId, $pageSize, $offsetBy, $search, $searchLogic, $sort, $status_id, $securityRoleId);
        $json = json_encode($array);
        return $json;
    }

    public static function copyControlsFromBaseline($baselineId, $company_id, $user_id, $company_name = null)
    {
        $count = 0;
        $baseline_id = Baseline_Ex::getCurrentBaselineId();
        $controls = ControlBaseline::model()->findAllByAttributes(array('baseline_id' => $baselineId,'status'=>Cons::ACTIVE));
        foreach ($controls as $control) {
            $success = ControlBaseline_Ex::copyControlBaselineToControlBaseline($control->control_baseline_id, $baseline_id, $company_id, $user_id, $company_name);
            if ($success == 0 || $success == 2) {
                $count++; // count add or dups
            }
        }
        return $count;
    }
    
    public static function getNextControlBaselineId($currentControlId, $companyId, $baselineId)
    {
       if(!$currentControlId || !$companyId || !$baselineId){
           return null;
       } 
       
        $sql = "
          SELECT control_baseline_id 
          FROM `control_baseline` 
          WHERE control_baseline_id > :currentControlBaselineId 
            AND company_id = :companyId 
            AND baseline_id = :baselineId 
          ORDER BY control_baseline_id ASC LIMIT 1
        ";
       
       $command = Yii::app()->db->createCommand($sql);
       $command->bindParam(":currentControlBaselineId", $currentControlId, PDO::PARAM_INT);
       $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
       $command->bindParam(":baselineId", $baselineId, PDO::PARAM_INT);
       $results = $command->query()->readAll();
       
       if(!empty($results)){
           return $results[0]['control_baseline_id'];
       }
       return null;
    }

    // returns error message code 1,2,3 or true
    private static function copyControlBaselineToControlBaseline($control_baseline_id, $baseline_id, $company_id, $user_id, $company_name = null)
    {
        if (empty($company_name)) {
            $company = Company::model()->findByPk($company_id);
            if ($company) {
                $company_name = trim($company->name);
            }
        }
        $retVal = 1;//'The control failed to be saved to your baseline.';
        $model1 = ControlBaseline::model()->findByAttributes(array('control_baseline_id' => $control_baseline_id));
        if (isset($model1)) {
            $model2 = ControlBaseline::model()->findByAttributes(array('control_id' => $model1->control_id, 'company_id' => $company_id, 'baseline_id' => $baseline_id));
            if (isset($model2)) {
                $retVal = 2;//'The control was not added since it already exists in your baseline.';
            } else {
                $attributes = $model1->attributes;
                unset($attributes['baseline_id']);
                unset($attributes['control_baseline_id']);
                // replace Company X to $company_name when we copy controls to their baseline. That would be the for rel_policy, detail, and guidance fields.
                if (!empty($company_name)) {
                    $attributes['rel_policy'] = str_replace('Company X', $company_name, $model1->rel_policy);
                    $attributes['detail'] = str_replace('Company X', $company_name, $model1->detail);
                    $attributes['guidance'] = str_replace('Company X', $company_name, $model1->guidance);
                }
                $model2 = new ControlBaseline;
                $model2->attributes = $attributes;
                $model2->baseline_id = $baseline_id;
                $model2->user_id = $user_id;
                $model2->company_id = $company_id;
                $model2->control_status_id = 1; // Planned
                if ($model2->save()) {
                    $retVal = 0;//'The control was added to your baseline.';
                } else {
                    Yii::log(print_r($model2->getErrors(), true), 'error', 'ControlBaseline_Ex.copyToControlBaseline');
                }
            }
        } else {
            $retVal = 3;//'The control was not found.';
        }
        return $retVal;
    }
    
    public static function createCSVReportData($report_id, $company_id, &$filename)
    {
        $data = null;
        $model = ExportReport::model()->findByPk($report_id);
        // we check $company_id for security to ensure only showing a company's data
        if (isset($model) && $model->company_id == $company_id) {
            $filename = $model->file_name;
            $reportData = json_decode($model->data, true);
            $exportColumns = array('id', 'cat_id', 'control_id', 'category', 'title', 'control_status_name', 'target_date', 'priority', 'detail', 'guidance', 'jobrole', 'evidence', 'rel_policy', 'reference', 'question', 'score', 'weighting', 'role_title', 'label1', 'label2');
            $data = array();
            $data[] = array('Id', 'Category Id', 'Control Id', 'Category', 'Title', 'Status', 'Target', 'Priority', 'Detail', 'Guidance', 'Job Role', 'Evidence', 'Related Policy', 'Regulatory Reference', 'Question', 'Score', 'Weighting', 'Security Role', 'Label 1', 'Label 2');
            $results = ControlBaseline_Ex::getBaselineControlsArray($model->company_id, $reportData['baseline_id'], null, $reportData['pageSize'], $reportData['offsetBy'], $reportData['search'], $reportData['searchLogic'], $reportData['sort'], $reportData['status_id'], $reportData['securityRoleId']);
            $rows = $results['records'];
            if (!empty($rows)) {
                foreach ($rows as $row) {
                    $exportData = array();
                    foreach ($exportColumns as $col) {
                        if (!empty($row[$col])) {
                            $exportData[] = $row[$col];
                        } else {
                            $exportData[] = '';
                        }
                    }
                    $data[] = $exportData;
                }
            }
            $model->delete(); // no longer need report record
        } else {
            $filename = 'noreport.csv';
        }
        return $data;
    }

    public static function exportCSV($search,$status_id,$securityRoleId)
    {
        $baseline_id = Baseline_Ex::getCurrentBaselineId();
        $baseline_name = Baseline_Ex::getCurrentBaselineName();
        // save exportReport to database record and send back url with an obscuredId id param
        $data = array(
            'baseline_id' => $baseline_id,
            'pageSize' => 0, //$search['pageSize'], get all data on export
            'offsetBy' => 0, //$search['offsetBy'],
            'search' => $search['search'],
            'searchLogic' => $search['searchLogic'],
            'sort' => $search['sort'],
            'status_id' => $status_id,
            'securityRoleId' => $securityRoleId,
        );
        $dataJson = json_encode($data);
        $exportReport = new ExportReport();
        $exportReport->isNewRecord = true;
        $exportReport->company_id = Utils::contextCompanyId();
        $exportReport->user_id = userId();
        $exportReport->status = 1;
        $exportReport->created_on = DateHelper::FormatDateTimeString('now', 'Y/m/d H:i:s');
        $exportReport->report_type = 'control_export_csv';
        $exportReport->file_ext = 'csv';
        $exportReport->file_type = 'text/csv';
        $exportReport->file_name = str_ireplace(' ', '_', $baseline_name . '.csv');
        $exportReport->title = $baseline_name;
        $exportReport->data = $dataJson;
        $exportReport->report = null;
        if ($exportReport->save()) {
            $results['href'] = url('controlbaseline/download', array('id' => Obscure::encode($exportReport->export_report_id)));
            $results['status'] = 'success';
            $results['message'] = 'Your file is downloading now.';
        } else {
            $results['status'] = 'error';
            $results['message'] = 'Failed to generate export file.';
        }
        $json = json_encode($results);
        return $json;
    }

    public static function exportWORD($search,$status_id,$securityRoleId)
    {
        $baseline_id = Baseline_Ex::getCurrentBaselineId();
        $baseline_name = Baseline_Ex::getCurrentBaselineName();

        // export to word document
        $data = ControlBaseline_Ex::getBaselineControlsArray(Utils::contextCompanyId(), $baseline_id, null,
            0 /*$search['pageSize'] get all data on export */, 0 /*$search['offsetBy']*/,
            $search['search'], $search['searchLogic'], $search['sort'], $status_id, $securityRoleId);

        if ($data['status'] == 'success') {
            if ($data['total'] > 0) {
                $nodes = ControlCategory_Ex::getTreeNodes();
                $references = array();
                $policies = array();

                foreach ($data['records'] as $policy) {
                    $rootNode = null;
                    ControlCategory_Ex::findRootOfNodeInTree($nodes, $policy['cat_id'], $rootNode, function (&$node, &$data) {
                    }, $c);

                    if ($rootNode) {
                        $references[$policy['reference']] = $policy['reference'];
                        $policies[$rootNode['id']]['category'] = $rootNode['text'];
                        $policies[$rootNode['id']]['policies'][] = array(
                            'title' => $policy['title'],
                            'category' => $policy['category'],
                            'rel_policy' => $policy['rel_policy']
                        );
                    }
                }

                $PolicyDocument = new mCreateWordDocument();
                $userName = Utils::getUserFirstLastName();
                $PolicyDocument->SetCompanyData(array(
                    'company_name' => Utils::contextCompanyName(),
                    'company_contact_email' => userEmail(),
                    'company_contact_user_first_last_name' => $userName['full_name']
                ));
                $report = $PolicyDocument->CreatePoliciesDocument($policies, $references);

                // save exportReport to database record and send back url with an obscuredId id param
                $data = array(
                    'baseline_id' => $baseline_id,
                    'pageSize' => 0, //$search['pageSize'], get all data on export
                    'offsetBy' => 0, //$search['offsetBy'],
                    'search' => $search['search'],
                    'searchLogic' => $search['searchLogic'],
                    'sort' => $search['sort'],
                    'status_id' => $status_id,
                    'securityRoleId' => $securityRoleId,
                );
                $dataJson = json_encode($data);
                $exportReport = new ExportReport();
                $exportReport->isNewRecord = true;
                $exportReport->company_id = Utils::contextCompanyId();
                $exportReport->user_id = userId();
                $exportReport->status = 1;
                $exportReport->created_on = DateHelper::FormatDateTimeString('now', 'Y/m/d H:i:s');
                $exportReport->report_type = 'control_export_word';
                $exportReport->file_ext = 'docx';
                $exportReport->file_type = 'application/octet-stream';
                $exportReport->file_name = str_ireplace(' ', '_', $baseline_name . '.docx');
                $exportReport->title = $baseline_name;
                $exportReport->data = $dataJson;
                $exportReport->report = $report;
                if ($exportReport->save()) {
                    $results['href'] = url('controlbaseline/downloadword', array('id' => Obscure::encode($exportReport->export_report_id)));
                    $results['status'] = 'success';
                    $results['message'] = 'Your file is downloading now.';
                } else {
                    $results['status'] = 'error';
                    $results['message'] = 'Failed to generate export file.';
                }
            } else {
                // no records found
                $results['status'] = 'error';
                $results['message'] = 'Failed to generate export file.<br>There are no records to export.';
            }
        } else {
            // data get error
            $results['status'] = 'error';
            $results['message'] = 'Failed to generate export file.<br>Unable to create export data structure.';
        }
        $json = json_encode($results);
        return $json;
    }

    public static function copyFromBaseline ($from_baseline_id, $to_baseline_id, $company_id, $user_id) {
        $sql = "
            INSERT INTO control_baseline
                (company_id, user_id, baseline_id, control_id, version, cat_id, cat_code, 
                cat_sub_code, cat_override, sort_order, priority, weighting, status, created_on, 
                assessed_on, target_date, title, control_status_id, score, evidence, rel_policy, 
                detail, guidance, reference, question, label1, label2, security_role_id, 
                updated_user_id)
            SELECT
                $company_id, $user_id, $to_baseline_id, control_id, version, cat_id, cat_code, 
                cat_sub_code, cat_override, sort_order, priority, weighting, status, created_on, 
                assessed_on, target_date, title, control_status_id, score, evidence, rel_policy, 
                detail, guidance, reference, question, label1, label2, security_role_id, 
                $user_id
            FROM control_baseline
            WHERE baseline_id = $from_baseline_id
              AND status = 1            
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
        return true;
    }
}