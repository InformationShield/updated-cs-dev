<?php

class ControlCategory_Ex
{
    public static function getCategory($categoryId=null,$parentId=null,$select=null) {
        $retVal = NULL;
        $sqlWhere = null;
        if ($categoryId) {
            $sqlWhere = " AND cc.control_category_id = :control_category_id";
        }
        if ($parentId) {
            $sqlWhere = " AND cc.parent_id = :parent_id";
        }
        if ($select==null) {
            $select = "SELECT cc.*";
        }
        $orderBy = " ORDER BY control_category_id asc";
        $sql = $select
            ." FROM control_category cc"
            ." WHERE cc.status = ".Cons::ACTIVE
            .$sqlWhere
            .$orderBy;
        $command = Yii::app()->db->createCommand($sql);
        if ($categoryId) {
            $command->bindParam(":control_category_id", $categoryId, PDO::PARAM_INT);
        }
        if ($parentId) {
            $command->bindParam(":parent_id", $parentId, PDO::PARAM_INT);
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($categoryId) {  // get a particular category
                $retVal = $results[0];
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

	public static function getRootCategories()
	{
		$retVal = null;
		$sql = "
			SELECT *
			FROM control_category
			WHERE parent_id = 0
			ORDER BY category;
		";
		$command = Yii::app()->db->createCommand($sql);
		$results = $command->query()->readAll();
		if (!empty($results)) {
			$output = array();
			foreach($results as $category) {
				$output[$category['control_category_id']] = preg_replace("/^[^\s]*[\s]/i", "", $category['category']); // remove numbers from beginning of the string
			}
			$retVal = $output;
		}
		return $retVal;
	}

    public static function buildTree($items) {

        $nodes = array();

        foreach($items as &$item) {
            $nodes[$item['parent_id']][] = &$item;
        }
        unset($item);

        foreach($items as &$item) {
            if (isset($nodes[$item['id']])) {
                $item['nodes'] = $nodes[$item['id']];
            }
        }

        return $nodes[0];
    }

    public static function traverseTreeCollectIds(&$catIds,&$nodes)
    {
        foreach($nodes as $node) {
            $catIds[] = $node['id'];
            if (array_key_exists('nodes', $node)) {
                self::traverseTreeCollectIds($catIds, $node['nodes']);
            }
        }
    }

    public static function findNodeInTree(&$nodes,$id,&$found,$callback,&$callData)
    {
        foreach($nodes as &$node) {
            if ($id == $node['id']) {
                if (is_callable($callback)) {
                    $callback($node,$callData);
                }
                $found = $node;
                return true;
            }
            if (array_key_exists('nodes', $node)) {
                $foundIt = self::findNodeInTree($node['nodes'], $id, $found, $callback, $callData);
                if ($foundIt !== false) return $foundIt;
            }
        }
        return false;
    }

    public static function findRootOfNodeInTree(&$nodes,$id,&$rootNode,$callback,&$callData)
    {
        $foundIt = self::findNodeInTree($nodes,$id,$rootNode,$callback,$callData);
        if ($foundIt) {
            $id = $rootNode['parent_id'];
            while ($id > 0) {
                $foundIt = self::findNodeInTree($nodes,$id,$rootNode,$callback,$callData);
                if ($foundIt) {
                    $id = $rootNode['parent_id'];
                } else {
                    break;
                }
            }
        }
        return $foundIt;
    }

    public static function getChildNodeCatIds($parent_id)
    {
        $catIds = array();
        if (!empty($parent_id)) {
            $rows = self::getCategory(null, null, "SELECT control_category_id as id, parent_id, category as text");
            $nodes = self::buildTree($rows);

            $foundIt = self::findNodeInTree($nodes,$parent_id,$pnode,null,$callData);
            if ($foundIt) {
                $catIds[] = $parent_id;
                if (array_key_exists('nodes', $pnode)) {
                    self::traverseTreeCollectIds($catIds, $pnode['nodes']);
                }
            }
        }
        return $catIds;
    }

    public static function getTreeNodes($withGroupFlag=false)
    {
        $rows = self::getCategory(null,null,"SELECT control_category_id as id, parent_id, category as text");
        $nodes = self::buildTree($rows);
        array_unshift($nodes,array('id'=>0,'parent_id'=>0,'text'=>'All Categories'));

        if ($withGroupFlag) {
            $cnt = count($nodes);
            for ($i=0; $i < $cnt; $i++) {
                if (array_key_exists('nodes',$nodes[$i])) {
                    $nodes[$i]['group'] = true;
                }
            }
        }
        return $nodes;
    }

    public static function getTreeNodesJson()
    {
        $nodes = self::getTreeNodes();
        $json = json_encode($nodes);
        //Yii::log(print_r($nodes,true)."\n\n".$json,'error','tree');
        return $json;
    }

}