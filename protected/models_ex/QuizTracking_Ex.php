<?php
/* @var $quizQuestion QuizQuestion */
/* @var $quiz Quiz */
/* @var $quizAnswer QuizAnswer */

class QuizTracking_Ex
{
    public static function getQuizTrackingStatusCounts($company_id)
    {
        $stats = array(
            array('total' => 0, 'status' => '0', 'label' => 'Unopened'),
            array('total' => 0, 'status' => '1', 'label' => 'Viewed'),
            array('total' => 0, 'status' => '2', 'label' => 'Completed'),
            array('total' => 0, 'status' => '3', 'label' => 'Passed'),
        );
        $sql = "SELECT COUNT(pt.quiz_tracking_id) total, (pt.viewed + pt.completed + pt.passed) status"
            . " FROM quiz_tracking pt"
            . " INNER JOIN quiz p ON pt.quiz_id=p.quiz_id AND p.quiz_status_id=" . Cons::POLICY_STATUS_PUBLISHED . " AND p.status=" . Cons::ACTIVE
            . " WHERE pt.company_id = :company_id"
            . " GROUP BY (pt.viewed + pt.completed + pt.passed)"
            . " ORDER BY (pt.viewed + pt.completed + pt.passed)";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            foreach ($results as $row) {
                $index = intval($row['status']);
                if ($index >= 0 && $index <= 3 &&
                    isset($row['total']) &&
                    $row['total'] > 0
                ) {
                    $stats[$index]['total'] = $row['total'];
                }
            }
        }

        $userquizcount = 0;
        $sql = "SELECT COUNT(employee_id) userquizcount"
            . " FROM user_profile_quiz upq"
            . " INNER JOIN quiz p ON upq.quiz_id=p.quiz_id and p.quiz_status_id=" . Cons::POLICY_STATUS_PUBLISHED . " AND p.status=" . Cons::ACTIVE
            . " INNER JOIN user_profile up on up.user_profile_id=upq.user_profile_id AND up.company_id=:company_id"
            . " LEFT JOIN employee e ON up.profile_id=e.profile_id AND e.company_id=:company_id"
            . " WHERE employee_id is not null AND upq.company_id=:company_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results) && isset($results['userquizcount']) && $results['userquizcount'] > 0) {
            $userquizcount = intval($results['userquizcount']);
        }

        if ($userquizcount > 0) {
            $stats[0]['total'] = $userquizcount - (intval($stats[1]['total']) + intval($stats[2]['total']));
        }
        return $stats;
    }

    public static function questionAnswerCounts($quiz_id,$company_id)
    {
        $retVal = array();
        $sql = "SELECT qa.quiz_question_id, COUNT(qa.quiz_question_id) as correct, 0 as wrong, qq.question"
            ." FROM quiz_answer qa"
            ." INNER JOIN quiz_question qq ON qa.quiz_question_id=qq.quiz_question_id"
            ." WHERE qa.quiz_id=:quiz_id"
            ." AND qa.company_id=:company_id"
            ." AND qa.taken_number=1"
            ." AND qa.correct=1"
            ." GROUP BY qa.quiz_question_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":quiz_id", $quiz_id, PDO::PARAM_INT);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            foreach($results as $r) {
                $retVal[$r['quiz_question_id']] = $r;
            }
        }
        $sql2 = "SELECT qa.quiz_question_id, 0 as correct, COUNT(qa.quiz_question_id) as wrong, qq.question"
            ." FROM quiz_answer qa"
            ." INNER JOIN quiz_question qq ON qa.quiz_question_id=qq.quiz_question_id"
            ." WHERE qa.quiz_id=:quiz_id"
            ." AND qa.company_id=:company_id"
            ." AND qa.taken_number=1"
            ." AND qa.correct=0"
            ." GROUP BY qa.quiz_question_id";
        $command2 = Yii::app()->db->createCommand($sql2);
        $command2->bindParam(":quiz_id", $quiz_id, PDO::PARAM_INT);
        $command2->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command2->query()->readAll();
        if (!empty($results)) {
            foreach($results as $r) {
                if (isset($retVal[$r['quiz_question_id']])) {
                    $retVal[$r['quiz_question_id']]['wrong'] = $r['wrong'];
                } else {
                    $retVal[$r['quiz_question_id']] = $r;
                }
            }
        }
        return $retVal;
    }

    public static function correctAnswersByUser($quiz_id,$company_id,$user_id)
    {
        $retVal = array();
        $sql = "SELECT quiz_question_id, answer"
            ." FROM quiz_answer"
            ." WHERE quiz_id=:quiz_id"
            ." AND company_id=:company_id"
            ." AND user_id=:user_id"
            ." AND correct=1"
            ." GROUP BY quiz_question_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":quiz_id", $quiz_id, PDO::PARAM_INT);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $command->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            foreach($results as $r) {
                $retVal[$r['quiz_question_id']] = $r['answer'];
            }
        }
        return $retVal;
    }

    public static function viewed($quiz_id,$company_id,$user_id)
    {
        $quizTracking = QuizTracking::model()->findByAttributes(array('quiz_id' => $quiz_id, 'user_id' => $user_id, 'company_id' => $company_id));
        if (!isset($quizTracking)) {
            $quizTracking = new QuizTracking();
            $quizTracking->quiz_id = $quiz_id;
            $quizTracking->user_id = $user_id;
            $quizTracking->company_id = $company_id;
            $quizTracking->taken_count = 0;
            $quizTracking->viewed = 1;
            $quizTracking->viewed_timestamp = date('Y-m-d H:i:s');
            if (!$quizTracking->save()) {
                Yii::log(print_r($quizTracking->getErrors(), true), 'error', 'QuizTracking_Ex.viewed');
            }
        }
        return $quizTracking;
    }

    public static function track($quiz_id,$company_id,$user_id,$answers,$quiz=null)
    {
        $errorMessages = array();
        if (!isset($quiz)) {
            $quiz = Quiz::model()->findByAttributes(array('quiz_id'=>$quiz_id,'company_id' => $company_id));
        }
        if (isset($quiz)) {
            $quizTracking = QuizTracking_Ex::viewed($quiz_id,$company_id,$user_id);
            if ($quizTracking->taken_count <= $quiz->max_retakes) {
                $quizTracking->taken_count = $quizTracking->taken_count + 1;
                $quizTracking->answer_timestamp = date('Y-m-d H:i:s');
                $criteria = new CDbCriteria(array('order' => 'sort_order ASC'));
                $questions = QuizQuestion::model()->findAllByAttributes(array('quiz_id' => $quiz_id, 'company_id' => $company_id, 'status' => 1), $criteria);
                if (!empty($questions)) {
                    $quizQuestions = array();
                    foreach ($questions as $question) {
                        $quizQuestions[$question->quiz_question_id] = $question;
                    }
                    $possible_score = 0;
                    $last_score = 0;
                    $last_correct = 0;
                    $last_wrong = 0;
                    $qnum = 0;
                    foreach ($answers as $quiz_question_id => $answer) {
                        $qnum++;
                        $quizAnswer = new QuizAnswer();
                        $quizAnswer->quiz_question_id = $quiz_question_id;
                        $quizAnswer->taken_number = $quizTracking->taken_count;
                        $quizAnswer->quiz_id = $quiz_id;
                        $quizAnswer->user_id = $user_id;
                        $quizAnswer->company_id = $company_id;
                        $quizAnswer->answer = $answer;
                        $quizAnswer->answer_timestamp = $quizTracking->answer_timestamp;
                        if (isset($quizQuestions[$quiz_question_id])) {
                            $quizQuestion = $quizQuestions[$quiz_question_id];
                            $possible_score += ($quizQuestion->score * $quizQuestion->weight);
                            $quizAnswer->correct = ($answer == $quizQuestion->correct_answer) ? 1 : 0;
                            $quizAnswer->score = $quizAnswer->correct * ($quizQuestion->score * $quizQuestion->weight);
                            $last_score += $quizAnswer->score;
                            if ($quizAnswer->correct) {
                                $last_correct++;
                            } else {
                                $last_wrong++;
                            }
                        } else {
                            $quizAnswer->correct = 0;
                            $quizAnswer->score = 0;
                            $last_wrong++;
                            $errorMessages[] = "Oops, failed to find correct answer for question #".$qnum.", thus no score given for the answer.";
                        }
                        if (!$quizAnswer->save()) {
                            $errorMessages[] = "Oops, failed to save answer for question #".$qnum.", report this to technical support.";
                            Yii::log(print_r($quizAnswer->getErrors(), true), 'error', 'QuizTracking_Ex.track');
                        }
                    }
                    // sum up the stats for this user's quiz
                    $quizTracking->possible_score = $possible_score;
                    $quizTracking->last_score = $last_score;
                    $quizTracking->last_correct = $last_correct;
                    $quizTracking->last_wrong = $last_wrong;
                    if ($quizTracking->completed==0) {
                        $quizTracking->low_score = $last_score;
                        $quizTracking->high_score = $last_score;
                    } else {
                        $quizTracking->low_score = ($last_score >= $quizTracking->low_score) ? $quizTracking->low_score : $last_score;
                        $quizTracking->high_score = ($last_score <= $quizTracking->high_score) ? $quizTracking->high_score : $last_score;
                    }
                    $quizTracking->completed = 1;
                    if (empty($quizTracking->completed_timestamp) || $quizTracking->completed_timestamp == '0000-00-00 00:00:00') {
                        $quizTracking->completed_timestamp = $quizTracking->answer_timestamp;
                    }
                    // if we have not yet passed, did we?
                    if ($quizTracking->passed != 1 && ($last_score >= $quiz->passing_score)) {
                        $quizTracking->passed = 1;
                        $quizTracking->passed_timestamp = $quizTracking->answer_timestamp;
                    }
                    if ($quizTracking->validate()) {
                        if (!$quizTracking->save()) {
                            $errorMessages[] = "Failed to save answers. Try again.";
                            Yii::log(print_r($quizTracking->getErrors(), true), 'error', 'QuizTracking_Ex.track');
                        }
                    } else {
                        $errorMessages[] = "Failed to validate answers. Try again.";
                        Yii::log(print_r($quizTracking->getErrors(), true), 'error', 'QuizTracking_Ex.track');
                    }
                } else {
                    $errorMessages[] = "Oops, failed to find the Quiz questions, thus could not save answers.";
                }
            } else {
                $errorMessages[] = "Oops, you have exceeded the maximum retakes for this Quiz, thus could not save answers.";
            }
        } else {
            $errorMessages[] = "Oops, failed to find Quiz ID# ".$quiz_id.", thus could not save answers.";
        }
        return $errorMessages;
    }
}