<?php
/* @var $profile Profile */

class Profile_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY doc_title asc, created_on asc";

    public static function getViewValues($companyId,$profileId)
    {
        $v = array();
        $row = self::getProfile($count,$companyId,$profileId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Title'] = $model->doc_title;
            $v['Published Date'] = DateHelper::FormatDateTimeString($model->publish_date, 'm/d/Y');
            $v['Updated Date'] = DateHelper::FormatDateTimeString($model->expiry_date, 'm/d/Y');
            $v['Author'] = $model->author;
            $v['keywords'] = $model->keywords;
            //$v['Category'] = $model->category; //dave says not needed
            $v['Approved'] = ($model->approved) ? 'Approved' : 'Un-Approved';
            $v['Status'] = $model->profile_status_name;
            $v['File'] = $model->file_name;
            $v['Description'] = $model->description;
            //$v['MIME Type'] = $model->file_type;
            //$v['File Size'] = Utils::FileSizeString($model->file_size);
        }
        return $v;
    }

    public static function saveFile($tmpFileFullPath,$profile)
    {
        $mUploadedFile = new mUploadedFiles("files/profile/",$profile->company_id);
        return $mUploadedFile->saveFile($tmpFileFullPath,$profile->doc_guid);
    }

    public static function removeFile($profile)
    {
        $mUploadedFile = new mUploadedFiles("files/profile/",$profile->company_id);
        return $mUploadedFile->removeFile($profile->doc_guid);
    }

    public static function downloadFile($profile)
    {
        $mUploadedFile = new mUploadedFiles("files/profile/",$profile->company_id);
        return $mUploadedFile->downloadFile($profile->doc_guid,$profile->file_type,$profile->file_name);
    }

    public static function getTmpFileUrl($profile)
    {
        $mUploadedFile = new mUploadedFiles("files/profile/",$profile->company_id);
        return $mUploadedFile->getTmpFileUrl($profile->doc_guid,$profile->file_name);
    }

    public static function getProfileCountsByStatus($companyId)
    {
        $retVal = 0;
        $sql = "
        	SELECT COUNT(up.profile_id) total
            FROM user_profile up
            WHERE up.status = 1
            	AND up.profile_id = :companyId
		";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (isset($results) && isset($results['total'])) {
            $retVal = $results['total'];
        }
        return $retVal;
    }

    public static function getProfileStatusCounts($companyId)
    {
        $retVal = array();
        $sql = "SELECT COUNT(p.profile_status_id) total, ps.profile_status_name, ps.profile_status_id"
            ." FROM profile p"
            ." INNER JOIN profile_status ps ON p.profile_status_id=ps.profile_status_id"
            ." WHERE p.status=".Cons::ACTIVE." AND company_id = :companyId"
            ." GROUP BY p.profile_status_id"
            ." ORDER BY p.profile_status_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

    public static function getProfile(&$count,$companyId,$profileId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null,$status_id=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT p.*, ps.profile_status_name, cc.category";
        }
        $from = " FROM profile p"
               ." INNER JOIN profile_status ps ON p.profile_status_id=ps.profile_status_id"
               ." LEFT JOIN control_category cc ON p.cat_id=cc.control_category_id";
        $sqlWhere = " WHERE p.company_id = :companyId AND p.status=".Cons::ACTIVE;
        if ($status_id !== null) {
            $sqlWhere .= " AND p.profile_status_id = :profile_status_id";
        }
        if ($profileId) {
            $sqlWhere .= " AND p.profile_id = :profile_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $profileId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($status_id !== null) {
                $cmdCount->bindParam(":profile_status_id", $status_id, PDO::PARAM_INT);
            }
            if ($profileId) {
                $cmdCount->bindParam(":profile_id", $profileId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($status_id !== null) {
            $command->bindParam(":profile_status_id", $status_id, PDO::PARAM_INT);
        }
        if ($profileId) {
            $command->bindParam(":profile_id", $profileId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($profileId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getProfileJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null,$status_id=null)
    {
        $select = "SELECT p.profile_id, p.doc_title, p.publish_date, p.expiry_date, ps.profile_status_name, p.author";
        $rows = Profile_Ex::getProfile($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort,$status_id);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['profile_id'] = Obscure::encode($row['profile_id']);
                $row['publish_date'] = DateHelper::FormatDateTimeString($row['publish_date'], 'm/d/Y');
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
}