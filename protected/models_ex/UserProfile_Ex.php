<?php
/* @var $model UserProfile */

class UserProfile_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY profile_title asc";

    // this function probably needs to be removed in future
    public static function getCurrentUserProfileId()
    {
        $userProfileId = null;
        $company_id = Utils::contextCompanyId();
        $employee = Employee::model()->findByAttributes(array('email' => userEmail(), 'company_id' => $company_id));
        if (isset($employee)) {
            $profile_id = $employee->profile_id;
            $userProfile = UserProfile::model()->findByAttributes(array('profile_id' => $profile_id, 'company_id' => $company_id));
            if (isset($userProfile)) {
                $userProfileId = $userProfile->user_profile_id;
            }
        }
        return $userProfileId;
    }
    
    public static function getCurrentUserProfileIds()
    {
        $userProfileIds = array();
        $company_id = Utils::contextCompanyId();
        $employee = Employee::model()->findByAttributes(array('email' => userEmail(), 'company_id' => $company_id));
        
        if (isset($employee)) {
            $results = Employee2profile::model()->findAllByAttributes(array('employee_id' => $employee->employee_id));
            if (!empty($results)) {
                foreach ($results as $r) {
                    $userProfileIds[] = $r->user_profile_id;
                }
            }
        }
        
        return $userProfileIds;
    }

    public static function saveUserProfile($employee_id, $selected_user_profile_id = array())
    {
        $errorMessages = array();
        if(empty($selected_user_profile_id)){
            UserProfile_Ex::clearUserProfile($employee_id);
            return;
        }
        
        // get current profiles to remove any old records at the end
        $old_profiles = Employee2profile::model()->findAllByAttributes(array('employee_id' => $employee_id));
        $old_profiles_list = array();
        foreach ($old_profiles as $p) {
            $old_profiles_list[$p->user_profile_id] = $p->user_profile_id;
        }
        
        foreach ($selected_user_profile_id as $user_profile_id) {
            // check if record already exists
            $record = Employee2profile::model()->findByAttributes(array('employee_id' => $employee_id, 'user_profile_id' => $user_profile_id));
            if(!isset($record)){
                $record = new Employee2profile();
            }
            
            $record->employee_id = $employee_id;
            $record->user_profile_id = $user_profile_id;

            //remove record from profiles to delete
            if(isset($old_profiles_list[$user_profile_id])){
                unset($old_profiles_list[$user_profile_id]);
            }
            
            if($record->validate()){
                if(!$record->save()){
                    $errorMessages[] = "Failed to save changes for user profile $user_profile_id.";
                }
            }else{
                $errorMessages[] = "Record validation failed for user profile $user_profile_id.";
            }
        }
        
        if(!empty($old_profiles_list) && empty($errorMessages)){
            foreach ($old_profiles_list as $k => $v) {
                if(!Employee2profile::model()->deleteAllByAttributes(array('employee_id' => $employee_id, 'user_profile_id' => $v))){
                    $errorMessages[] = "Failed to delete unwanted user profile $v.";
                }
            }
        }
        
        return $errorMessages;
    }
    
    public static function clearUserProfile($employee_id)
    {
        Employee2profile::model()->deleteAllByAttributes(array('employee_id' => $employee_id));
    }
    
    public static function saveUserProfileQuiz($user_profile_id,$company_id,$quizList)
    {
        $errorMessages = array();
        // will remove any old records left in this list at the end
        $oldQuizList = UserProfile_Ex::getUserProfileQuiz($user_profile_id,$company_id);
        if (!empty($quizList)) {
            $order = 0;
            foreach ($quizList as $quiz) {
                $quiz['sort_order'] = ++$order;
                $quiz['user_profile_id'] = $user_profile_id;
                $quiz['company_id'] = $company_id;
                if (empty($quiz['user_profile_quiz_id'])) {
                    $userProfileQuiz = new UserProfileQuiz();
                    unset($quiz['user_profile_quiz_id']);
                    $userProfileQuiz->attributes = $quiz;
                    if (!$userProfileQuiz->save()) {
                        $errorMessages[] = "Failed to save changes for new user profile quiz record.";
                    }
                } else {
                    $userProfileQuiz = UserProfileQuiz::model()->findByPk($quiz['user_profile_quiz_id']);
                    if (!empty($userProfileQuiz)) {
                        // remove from oldQuizList, since we will remove any old records left in this list at the end
                        foreach($oldQuizList as $key=>$upt) {
                            if ($upt['user_profile_quiz_id'] == $quiz['user_profile_quiz_id']) {
                                unset($oldQuizList[$key]);
                                break;
                            }
                        }
                        // now we can unset value
                        unset($quiz['user_profile_quiz_id']);
                        $userProfileQuiz->attributes = $quiz;
                        if (!$userProfileQuiz->save()) {
                            $errorMessages[] = "Failed to save changes for existing user profile quiz record.";
                        }
                    } else {
                        $errorMessages[] = "Failed to update changes for existing user profile quiz, because it was not found.";
                    }
                }
            }
        }
        if (!empty($oldQuizList)) {
            // delete old Quiz records no longer needed
            foreach ($oldQuizList as $key => $upp) {
                $userProfileQuiz = UserProfileQuiz::model()->findByPk($upp['user_profile_quiz_id']);
                if (!empty($userProfileQuiz)) {
                    if (!$userProfileQuiz->delete()) {
                        $errorMessages[] = "Failed to delete unwanted user profile quiz record.";
                    }
                }
            }
        }
        return $errorMessages;
    }

    public static function saveUserProfilePolicy($user_profile_id,$company_id,$policyList)
    {
        $errorMessages = array();
        // will remove any old records left in this list at the end
        $oldPolicyList = UserProfile_Ex::getUserProfilePolicy($user_profile_id,$company_id);
        if (!empty($policyList)) {
            $order = 0;
            foreach ($policyList as $policy) {
                $policy['sort_order'] = ++$order;
                $policy['user_profile_id'] = $user_profile_id;
                $policy['company_id'] = $company_id;
                if (empty($policy['user_profile_policy_id'])) {
                    $userProfilePolicy = new UserProfilePolicy();
                    unset($policy['user_profile_policy_id']);
                    $userProfilePolicy->attributes = $policy;
                    if (!$userProfilePolicy->save()) {
                        $errorMessages[] = "Failed to save changes for new user profile policy record.";
                    }
                } else {
                    $userProfilePolicy = UserProfilePolicy::model()->findByPk($policy['user_profile_policy_id']);
                    if (!empty($userProfilePolicy)) {
                        // remove from oldPolicyList, since we will remove any old records left in this list at the end
                        foreach($oldPolicyList as $key=>$upp) {
                            if ($upp['user_profile_policy_id'] == $policy['user_profile_policy_id']) {
                                unset($oldPolicyList[$key]);
                                break;
                            }
                        }
                        // now we can unset value
                        unset($policy['user_profile_policy_id']);
                        $userProfilePolicy->attributes = $policy;
                        if (!$userProfilePolicy->save()) {
                            $errorMessages[] = "Failed to save changes for existing user profile policy record.";
                        }
                    } else {
                        $errorMessages[] = "Failed to update changes for existing user profile policy, because it was not found.";
                    }
                }
            }
        }
        if (!empty($oldPolicyList)) {
            // delete old Policy records no longer needed
            foreach ($oldPolicyList as $key => $upp) {
                $userProfilePolicy = UserProfilePolicy::model()->findByPk($upp['user_profile_policy_id']);
                if (!empty($userProfilePolicy)) {
                    if (!$userProfilePolicy->delete()) {
                        $errorMessages[] = "Failed to delete unwanted user profile policy record.";
                    }
                }
            }
        }
        return $errorMessages;
    }
    
    public static function saveUserProfileTraining($user_profile_id,$company_id,$trainingList)
    {
        $errorMessages = array();
        // will remove any old records left in this list at the end
        $oldTrainingList = UserProfile_Ex::getUserProfileTraining($user_profile_id,$company_id);
        if (!empty($trainingList)) {
            $order = 0;
            foreach ($trainingList as $training) {
                $training['sort_order'] = ++$order;
                $training['user_profile_id'] = $user_profile_id;
                $training['company_id'] = $company_id;
                if (empty($training['user_profile_training_id'])) {
                    $userProfileTraining = new UserProfileTraining();
                    unset($training['user_profile_training_id']);
                    $userProfileTraining->attributes = $training;
                    if (!$userProfileTraining->save()) {
                        $errorMessages[] = "Failed to save changes for new user profile training record.";
                    }
                } else {
                    $userProfileTraining = UserProfileTraining::model()->findByPk($training['user_profile_training_id']);
                    if (!empty($userProfileTraining)) {
                        // remove from oldTrainingList, since we will remove any old records left in this list at the end
                        foreach($oldTrainingList as $key=>$upt) {
                            if ($upt['user_profile_training_id'] == $training['user_profile_training_id']) {
                                unset($oldTrainingList[$key]);
                                break;
                            }
                        }
                        // now we can unset value
                        unset($training['user_profile_training_id']);
                        $userProfileTraining->attributes = $training;
                        if (!$userProfileTraining->save()) {
                            $errorMessages[] = "Failed to save changes for existing user profile training record.";
                        }
                    } else {
                        $errorMessages[] = "Failed to update changes for existing user profile training, because it was not found.";
                    }
                }
            }
        }
        if (!empty($oldTrainingList)) {
            // delete old Training records no longer needed
            foreach ($oldTrainingList as $key => $upp) {
                $userProfileTraining = UserProfileTraining::model()->findByPk($upp['user_profile_training_id']);
                if (!empty($userProfileTraining)) {
                    if (!$userProfileTraining->delete()) {
                        $errorMessages[] = "Failed to delete unwanted user profile training record.";
                    }
                }
            }
        }
        return $errorMessages;
    }

    /* @var $user_profile_id can be either single integer or an array */ 
    public static function getUserProfileQuiz($user_profile_id,$company_id,$user_id=null,$select=null)
    {
        $retVal = array();
        $multipleProfiles = false;
        $sql = "";
        
        if(is_array($user_profile_id)){
            $multipleProfiles = true;
        }
        
        if ($select) {
            $sql = $select;
        } else {
            $sql = "SELECT upq.*, q.quiz_title, q.publish_date";
            if ($user_id != null) {
                $sql .= ", tra.viewed, tra.completed, tra.passed, tra.viewed_timestamp, tra.completed_timestamp, tra.passed_timestamp";
            }
        }
        $sql .= " FROM user_profile_quiz upq"
            ." INNER JOIN quiz q ON upq.quiz_id=q.quiz_id";
        if ($user_id != null) {
            $sql .= " LEFT JOIN quiz_tracking tra ON upq.quiz_id=tra.quiz_id"
                ." AND upq.company_id=:company_id"
                ." AND tra.user_id=:user_id";
        }
        
        $userProfileWhereStmt = "upq.user_profile_id  = :user_profile_id";
        if($multipleProfiles){
            $userProfileWhereStmt = "upq.user_profile_id  IN (";
            foreach ($user_profile_id as $k => $id){
                $userProfileWhereStmt .= ":p$k,";
            }
            $userProfileWhereStmt = substr($userProfileWhereStmt, 0, -1) . ")";
        }
        
        $sql .= " WHERE " . $userProfileWhereStmt
            ." AND upq.company_id=:company_id"
            ." AND q.quiz_status_id=".Cons::POLICY_STATUS_PUBLISHED." AND q.status=".Cons::ACTIVE
            ." ORDER BY upq.sort_order ASC";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        
        if($multipleProfiles){
            foreach ($user_profile_id as $k => $id){
                $command->bindValue(":p$k", $id, PDO::PARAM_INT);
            }
        }else{
            $command->bindParam(":user_profile_id", $user_profile_id, PDO::PARAM_INT);
        }
        
        if ($user_id != null) {
            $command->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }
    
    /* @var $user_profile_id can be either single integer or an array */ 
    public static function getUserProfilePolicy($user_profile_id, $company_id,$user_id=null,$select=null)
    {
        $retVal = array();
        $multipleProfiles = false;
        $sql = "";
        $sqlWhere = "";
        
        if(is_array($user_profile_id)){
            $multipleProfiles = true;
        }
        
        if ($select) {
            $sql = $select;
        } else {
            $sql = "SELECT upp.*, p.doc_title, p.author, p.publish_date, p.description, p.file_type, p.file_ext";
            if ($user_id != null) {
                $sql .= ", tra.ack, tra.view, tra.ack_date, tra.view_date";
            }
        }
        $sql .= " FROM user_profile_policy upp"
                ." INNER JOIN policy p ON upp.policy_id=p.policy_id";
        if ($user_id != null) {
            $sql .= " LEFT JOIN user_policy_tracking tra ON upp.policy_id=tra.policy_id"
                    ." AND upp.company_id=:company_id"
                    ." AND tra.user_id=:user_id";
        }
        
        $userProfileWhereStmt = "upp.user_profile_id  = :user_profile_id";
        if($multipleProfiles){
            $userProfileWhereStmt = "upp.user_profile_id  IN (";
            foreach ($user_profile_id as $k => $id){
                $userProfileWhereStmt .= ":p$k,";
            }
            $userProfileWhereStmt = substr($userProfileWhereStmt, 0, -1) . ")";
        }
        
        $sql .= " WHERE " . $userProfileWhereStmt
                . " AND upp.company_id=:company_id"
                . " AND p.policy_status_id=".Cons::POLICY_STATUS_PUBLISHED
                . " AND p.status=".Cons::ACTIVE
                . " ORDER BY upp.sort_order ASC";
        
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        if($multipleProfiles){
            foreach ($user_profile_id as $k => $id){
                $command->bindValue(":p$k", $id, PDO::PARAM_INT);
            }
        }else{
            $command->bindParam(":user_profile_id", $user_profile_id, PDO::PARAM_INT);
        }
        
        if ($user_id != null) {
            $command->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

    /* @var $user_profile_id can be either single integer or an array */ 
    public static function getUserProfileTraining($user_profile_id,$company_id,$user_id=null,$select=null)
    {
        $retVal = array();
        $sql = "";
        $multipleProfiles = false;
        
        if(is_array($user_profile_id)){
            $multipleProfiles = true;
        }
        
        if ($select) {
            $sql = $select;
        } else {
            $sql = "SELECT upt.*, t.doc_title, t.author, t.publish_date, t.training_type, t.file_type, t.file_ext";
            if ($user_id != null) {
                $sql .= ", tra.ack, tra.view, tra.ack_date, tra.view_date";
            }
        }
        $sql .= " FROM user_profile_training upt"
            ." INNER JOIN training t ON upt.training_id=t.training_id";
        if ($user_id != null) {
            $sql .= " LEFT JOIN user_training_tracking tra ON upt.training_id=tra.training_id"
                ." AND upt.company_id=:company_id"
                ." AND tra.user_id=:user_id";
        }
        
        $userProfileWhereStmt = "upt.user_profile_id  = :user_profile_id";
        if($multipleProfiles){
            $userProfileWhereStmt = "upt.user_profile_id  IN (";
            foreach ($user_profile_id as $k => $id){
                $userProfileWhereStmt .= ":p$k,";
            }
            $userProfileWhereStmt = substr($userProfileWhereStmt, 0, -1) . ")";
        }
        
        $sql .= " WHERE " . $userProfileWhereStmt
            ." AND upt.company_id=:company_id"
            ." AND t.training_status_id=".Cons::POLICY_STATUS_PUBLISHED." AND t.status=".Cons::ACTIVE
            ." ORDER BY upt.sort_order ASC";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        if($multipleProfiles){
            foreach ($user_profile_id as $k => $id){
                $command->bindValue(":p$k", $id, PDO::PARAM_INT);
            }
        }else{
            $command->bindParam(":user_profile_id", $user_profile_id, PDO::PARAM_INT);
        }
        if ($user_id != null) {
            $command->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

    public static function getUserProfileQuizJson($user_profile_id=false,$company_id=false,$user_id=false) {
        return self::getUserProfilePolicyOrTrainingOrQuizJson('quiz',$user_profile_id,$company_id,$user_id);
    }
    public static function getUserProfilePolicyJson($user_profile_id=false,$company_id=false,$user_id=false) {
        return self::getUserProfilePolicyOrTrainingOrQuizJson('policy',$user_profile_id,$company_id,$user_id);
    }
    public static function getUserProfileTrainingJson($user_profile_id=false,$company_id=false,$user_id=false) {
        return self::getUserProfilePolicyOrTrainingOrQuizJson('training',$user_profile_id,$company_id,$user_id);
    }
    private static function getUserProfilePolicyOrTrainingOrQuizJson($type,$user_profile_id=false,$company_id=false,$user_id=false)
    {
        $count = 0;
        if ($user_profile_id == false) $user_profile_id = UserProfile_Ex::getCurrentUserProfileIds();
        if ($company_id == false) $company_id = Utils::contextCompanyId();
        if ($user_id == false) $user_id = userId();        
        if (!empty($user_profile_id) && !empty($company_id)) {
            if ($type == 'policy') {
                $select = "SELECT p.policy_id, p.doc_title, p.author, p.publish_date, p.file_type, p.description";
                if ($user_id) {
                    $select .= ", tra.ack, tra.view, tra.ack_date, tra.view_date";
                }
                $rows = UserProfile_Ex::getUserProfilePolicy($user_profile_id, $company_id, $user_id, $select);
            } else if ($type == 'training') {
                $select = "SELECT t.training_id, t.doc_title, t.author, t.publish_date, t.training_type, t.file_type, t.description";
                if ($user_id) {
                    $select .= ", tra.ack, tra.view, tra.ack_date, tra.view_date";
                }
                $rows = UserProfile_Ex::getUserProfileTraining($user_profile_id, $company_id, $user_id, $select);
            } else if ($type == 'quiz') {
                $select = "SELECT q.quiz_id, q.quiz_title, q.publish_date, q.description";
                if ($user_id) {
                    $select .= ", tra.viewed, tra.completed, tra.passed, tra.viewed_timestamp, tra.completed_timestamp, tra.passed_timestamp, tra.high_score, tra.possible_score";
                }
                $rows = UserProfile_Ex::getUserProfileQuiz($user_profile_id, $company_id, $user_id, $select);
            }
            if (!empty($rows)) {
                $count = count($rows);
                foreach ($rows as &$row) {
                    if ($type == 'policy') {
                        $row['policy_id'] = Obscure::encode($row['policy_id']);
                        if ($user_id) {
                            $row['ack_date'] = DateHelper::FormatDateTimeString($row['ack_date'], 'm/d/Y');
                            $row['view_date'] = DateHelper::FormatDateTimeString($row['view_date'], 'm/d/Y');
                        }
                    } else if ($type == 'training') {
                        $row['training_id'] = Obscure::encode($row['training_id']);
                        if ($user_id) {
                            $row['ack_date'] = DateHelper::FormatDateTimeString($row['ack_date'], 'm/d/Y');
                            $row['view_date'] = DateHelper::FormatDateTimeString($row['view_date'], 'm/d/Y');
                        }
                    } else if ($type == 'quiz') {
                        $row['quiz_id'] = Obscure::encode($row['quiz_id']);
                        if ($user_id) {
                            $row['viewed_timestamp'] = DateHelper::FormatDateTimeString($row['viewed_timestamp'], 'm/d/Y');
                            $row['completed_timestamp'] = DateHelper::FormatDateTimeString($row['completed_timestamp'], 'm/d/Y');
                            $row['passed_timestamp'] = DateHelper::FormatDateTimeString($row['passed_timestamp'], 'm/d/Y');
                        }
                    }
                    $row['publish_date'] = DateHelper::FormatDateTimeString($row['publish_date'], 'm/d/Y');
                }
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

    public static function getViewValues($companyId,$userProfileId)
    {
        $v = array();
        $row = self::getUserProfile($count,$companyId,$userProfileId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Profile ID'] = $model->profile_id;
            $v['Title'] = $model->profile_title;
            $v['Description'] = $model->profile_description;
            $rows = UserProfile_Ex::getUserProfilePolicy($userProfileId, $companyId);
            if (!empty($rows)) {
                $titles= array();
                foreach($rows as $r) {
                    $title = $r['doc_title'];
                    if (!empty($r['author'])) {
                        $title .= ' by ' . $r['author'];
                    }
                    if (!empty($r['publish_date'])) {
                        $title .= ' published ' . DateHelper::FormatDateTimeString($r['publish_date'], 'm/d/Y');
                    }
                    $titles[] = $title;
                }
                $v['Policy List'] =  implode("<br/>",$titles);
            }
            $rows = UserProfile_Ex::getUserProfileTraining($userProfileId, $companyId);
            if (!empty($rows)) {
                $titles= array();
                foreach($rows as $r) {
                    $title = $r['doc_title'];
                    $title .= ($r['training_type']==2) ? ' (video)' : ' (doc)';
                    if (!empty($r['author'])) {
                        $title .= ' by ' . $r['author'];
                    }
                    if (!empty($r['publish_date'])) {
                        $title .= ' published ' . DateHelper::FormatDateTimeString($r['publish_date'], 'm/d/Y');
                    }
                    $titles[] = $title;
                }
                $v['Training List'] =  implode("<br/>",$titles);
            }
        }
        return $v;
    }

    public static function getNextProfileId($companyId)
    {
        $retVal = 1;
        $sql = "SELECT MAX(profile_id) maxprofileid FROM user_profile WHERE status = ".Cons::ACTIVE." AND company_id = :companyId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results) && $results['maxprofileid'] > 0) {
            $retVal = $results['maxprofileid'] + 1;
        }
        return $retVal;

    }

	public static function getEmployeeProfile($companyId, $employeeId)
	{
		$retVal = array();
		$sql = "
			SELECT up.*, e2p.employee2profile_id
			FROM user_profile up
			  LEFT JOIN employee2profile e2p ON e2p.user_profile_id = up.user_profile_id
				AND e2p.employee_id = :employee_id
			WHERE up.company_id = :company_id
			ORDER BY up.profile_title
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":company_id", $companyId, PDO::PARAM_INT);
		$command->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
		$results = $command->query()->readAll();
		if (!empty($results)) {
			$retVal = $results;
		}
		return $retVal;
	}

    public static function getUserProfile(&$count,$companyId,$userProfileId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT *";
        }
        $from = " FROM user_profile up";
        $sqlWhere = " WHERE up.status = ".Cons::ACTIVE." AND up.company_id = :companyId";
        if ($userProfileId) {
            $sqlWhere .= " AND up.user_profile_id = :user_profile_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $userProfileId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($userProfileId) {
                $cmdCount->bindParam(":user_profile_id", $userProfileId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($userProfileId) {
            $command->bindParam(":user_profile_id", $userProfileId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($userProfileId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getUserProfilesJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT user_profile_id, profile_title, profile_id, profile_description";
        $rows = UserProfile_Ex::getUserProfile($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['user_profile_id'] = Obscure::encode($row['user_profile_id']);
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

    public static function createDefaultUserProfile($companyId)
    {
        $userProfile = UserProfile::model()->findByAttributes(array('company_id' => $companyId));
        if($userProfile){
            // user profile already exists
            return false;
        }
        
        $defaultUserProfile = new UserProfile;
        if($defaultUserProfile){
            $defaultUserProfile->company_id = $companyId;
            $defaultUserProfile->profile_id = 1;
            $defaultUserProfile->profile_title = "Default User Profile";
            $defaultUserProfile->profile_description = "The default user profile for the company.";
            $defaultUserProfile->status = 1;
            $defaultUserProfile->created_on = date("Y-m-d H:i:s");
            if($defaultUserProfile->validate()){
                if($defaultUserProfile->save()){
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }
}