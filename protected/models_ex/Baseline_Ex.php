<?php

class Baseline_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY baseline_name asc";

    public static function GetBaselines($companyId)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('status=1');
        $criteria->addCondition('company_id=:companyId');
        $criteria->params = array(':companyId'=>$companyId);
        $criteria->order = 'baseline_name ASC, baseline_id ASC';
        $baselines = Baseline::model()->findAll($criteria);
        return $baselines;
    }

    public static function GetDefaultBaseline($companyId)
    {
        $baseline = null;
        if (isset($companyId)) {
            $criteria = new CDbCriteria();
            $criteria->addCondition('status=1');
            $criteria->addCondition('company_id=:companyId');
            $criteria->params = array(':companyId' => $companyId);
            $criteria->order = 'baseline_id ASC';
            $criteria->limit = 1;
            $baseline = Baseline::model()->find($criteria);
            if (empty($baseline)) {
                // create a Default baseline
                $baseline = new Baseline();
                $baseline->company_id = $companyId;
                $baseline->user_id = userId();
                $baseline->status = 1;
                $baseline->sort_order = 0;
                $baseline->created_on = DateHelper::FormatDateTimeString('now', 'Y/m/d H:i:s');
                $baseline->baseline_name = 'Default';
                $baseline->baseline_description = 'Default baseline for the organization.';
                if (!$baseline->save()) {
                    $baseline = null;
                    Yii::log('SEVERE ERROR COULD NOT CREATE DEFAULT BASELINE!', 'error', 'Baseline_Ex.GetDefaultBaseline');
                }
            }
        }
        return $baseline;
    }

	public static function getBaseline($baseline_id) {
		$retVal = null;
		$sql = "
			SELECT b.*
			FROM baseline b
			WHERE b.baseline_id = :baseline_id
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(":baseline_id", $baseline_id, PDO::PARAM_INT);
		$results = $command->query()->read();
		if ($results) {
			$results['baseline_oid'] = Obscure::encode($results['baseline_id']);
			$retVal = $results;
		}
		return $retVal;
	}

	public static function getCyberRiskBaseline() {
		$retVal = null;
		$sql = "
			SELECT b.*
			FROM baseline b
			WHERE company_id = :company_id
			AND b.special_type = 1
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(":company_id", mCyberRiskScore::CYBER_BASELINE_COMPANY_ID, PDO::PARAM_INT);
		$results = $command->query()->read();
		if ($results) {
			$results['baseline_oid'] = Obscure::encode($results['baseline_id']);
			$retVal = $results;
		}
		return $retVal;
	}

	public static function getCyberRiskBaselineId($encrypted=false) {
		$retVal = -1;
		$crBaseline = self::getCyberRiskBaseline();
		if ($crBaseline) {
			if ($encrypted) {
				$retVal= $crBaseline['baseline_oid'];
			} else {
				$retVal= $crBaseline['baseline_id'];
			}
		}
		return $retVal;
	}

	public static function getCurrentBaselineId($reset=false, $userId=null)
    {
        if ($reset) {
            $baseline_id = null;
            $last_baseline_id = Usersetting_Ex::GetValue($userId, 'last_baseline_id', null);
            if ($last_baseline_id) {
                $last_baseline = self::getBaseline($last_baseline_id);
                if ($last_baseline && $last_baseline['company_id'] == Utils::contextCompanyId()) {
                    setSessionVar('context_baseline_id', $last_baseline_id);
                    setSessionVar('context_baseline_name', $last_baseline['baseline_name']);
                    $baseline_id = $last_baseline_id;
                }
            } else {
                setSessionVar('context_baseline_id', null);
                setSessionVar('context_baseline_name', null);
            }
        } else {
            $baseline_id = getSessionVar('context_baseline_id', null);
        }
        if (empty($baseline_id)) {
            // find or create first baseline record
            $baseline = Baseline_Ex::GetDefaultBaseline(Utils::contextCompanyId());
            if (isset($baseline)) {
                $baseline_id = $baseline->baseline_id;
                $baseline_name = $baseline->baseline_name;
                setSessionVar('context_baseline_id', $baseline_id);
                setSessionVar('context_baseline_name', $baseline_name);
            }
        }
        return $baseline_id;
    }

    public static function getCurrentBaselineName()
    {
        // should always check that Id is set or create first
        $baseline_id = Baseline_Ex::getCurrentBaselineId();
        $baseline_name = getSessionVar('context_baseline_name','');
        return $baseline_name;
    }

    public static function setCurrentBaseline($companyId,$baseline_id)
    {
        $retVal = false;
        $baseline = Baseline::model()->findByPk($baseline_id);
        if (isset($baseline) && $baseline->company_id == $companyId) {
            setSessionVar('context_baseline_id', $baseline->baseline_id);
            setSessionVar('context_baseline_name', $baseline->baseline_name);
            Usersetting_Ex::SetValue(userId(), 'last_baseline_id', $baseline->baseline_id);
            $retVal = true;
        }
        return $retVal;
    }

	public static function setCyberRiskBaseline($baseline_id)
	{
		$sql = "
			UPDATE baseline
			SET special_type = 0
			WHERE company_id = :company_id 
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(":company_id", mCyberRiskScore::CYBER_BASELINE_COMPANY_ID, PDO::PARAM_INT);
		$command->execute();

		$sql = "
			UPDATE baseline
			SET special_type = 1
			WHERE baseline_id = :baseline_id 
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(":baseline_id", $baseline_id, PDO::PARAM_INT);
		$command->execute();
	}

	public static function getViewValues($companyId,$baselineId)
    {
        $v = array();
        $row = self::getBaselineAsessment($count,$companyId,$baselineId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Name'] = $model->baseline_name;
            $v['Control Count'] = $model->control_count;
            $v['Created On'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y');
            $v['Description'] = $model->baseline_description;
        }
        return $v;
    }

    public static function getBaselineCount($companyId)
    {
        $retVal = 0;
        $sql = "SELECT COUNT(b.baseline_id) total"
            ." FROM baseline b"
            ." WHERE b.status=".Cons::ACTIVE." AND company_id = :companyId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results)) {
            $retVal = $results['total'];
        }
        return $retVal;
    }

    public static function getBaselineAsessment(&$count,$companyId=NULL,$baselineId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "
            	SELECT b.*, COUNT(cb.control_baseline_id) as control_count, MAX(cb.assessed_on) as assessed_on,
                (	SELECT COUNT(*) 
                	FROM `control_baseline` 
					WHERE baseline_id = b.baseline_id 
						AND assessed_on IS NOT NULL
				) as assessment_questions_answered";
        }
        $from = " FROM baseline b"
                ." LEFT JOIN control_baseline cb ON cb.baseline_id=b.baseline_id";
        $sqlWhere = " WHERE b.status = ".Cons::ACTIVE;
        if (is_null($companyId)) {
            $sqlWhere .= " AND b.company_id IS NULL";
        } else {
            $sqlWhere .= " AND b.company_id=:companyId";
        }
        if (!empty($baselineId)) {
            $sqlWhere .= " AND b.baseline_id = :baseline_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        $sqlWhere .= " GROUP BY b.baseline_id";
        if ($pageSize > 0 && $baselineId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            if (!is_null($companyId)) {
                $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            }
            if (!empty($baselineId)) {
                $cmdCount->bindParam(":baseline_id", $baselineId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        if (!is_null($companyId)) {
            $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        }
        if (!empty($baselineId)) {
            $command->bindParam(":baseline_id", $baselineId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if (!empty($baselineId)) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getBaselineAsessmentJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "
			SELECT b.baseline_id, b.sort_order, b.created_on, b.baseline_name, b.baseline_description, 
				COUNT(cb.control_baseline_id) as control_count, MAX(cb.assessed_on) as assessed_on, 
				(	SELECT COUNT(*) FROM `control_baseline` 
					WHERE baseline_id = b.baseline_id 
						AND assessed_on IS NOT NULL
				) as assessment_questions_answered";
        $rows = self::getBaselineAsessment($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as $key=>&$row) {
                if (!empty($row['baseline_id'])) {
                    $row['created_on'] = DateHelper::FormatDateTimeString($row['created_on'], 'm/d/Y');
                    $row['assessed_on'] = DateHelper::FormatDateTimeString($row['assessed_on'], 'm/d/Y');
                    $row['baseline_id'] = Obscure::encode($row['baseline_id']);
                } else {
                    unset($rows[$key]);
                    $count--;
                }
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

	public static function getBaselineAsessmentV2($company_id, $baseline_id)
	{
		$retVal = null;
		$sql = "
			SELECT b.*, COUNT(cb.control_baseline_id) as control_count, MAX(ca.assessed_on) as assessed_on,
				(
					SELECT COUNT(*) 
					  FROM control_answer ca
						INNER JOIN control_baseline cb ON cb.control_baseline_id = ca.control_baseline_id
					  WHERE cb.baseline_id = :baseline_id
						AND ca.company_id = :company_id
						AND ca.assessed_on IS NOT NULL
				) as assessment_questions_answered 
			FROM baseline b 
				INNER JOIN control_baseline cb ON cb.baseline_id = b.baseline_id
				LEFT JOIN control_answer ca ON ca.control_baseline_id = cb.control_baseline_id AND ca.company_id = :company_id
			WHERE b.status = 1
				AND b.baseline_id = :baseline_id 
			GROUP BY b.baseline_id 
			ORDER BY baseline_name asc
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
		$command->bindParam(":baseline_id", $baseline_id, PDO::PARAM_INT);
		$results = $command->query()->read();
		if ($results) {
			$retVal = $results;
		}
		return $retVal;
	}

	public static function getStockBaseline() {
        $retVal = false;
        $sql = "
            SELECT b.*
            FROM baseline b
            WHERE special_type = 2
              AND status = 1
            ORDER BY baseline_name
        ";
        $command = Yii::app()->db->createCommand($sql);
        $results = $command->query()->readall();
        if ($results) {
            $rows = array();
            foreach ($results as $i=>$result) {
                $rows[$i]['id'] = obscure($result['baseline_id']);
                $rows[$i]['baseline_name'] = $result['baseline_name'];
                $rows[$i]['baseline_description'] = $result['baseline_description'];
            }
            $retVal = $rows;
        }
        return $retVal;
    }

}