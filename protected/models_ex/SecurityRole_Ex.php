<?php

class SecurityRole_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY role_title asc";

    // copy default roles if the company has none
    public static function copyDefaultRoles($company_id,$company_name)
    {
        $company_name = trim($company_name);
        $existing = SecurityRole::model()->findAllByAttributes(array('company_id'=>$company_id));
        if (isset($existing) && count($existing) > 0) {
            return false;
        }
        $models = SecurityRole::model()->findAllByAttributes(array('company_id'=>null));
        if ($models) {
            foreach($models as $orig) {
                $sr = new SecurityRole();
                $attributes = $orig->attributes;
                unset($attributes['security_role_id']);
                // replace Company X to $company_name when we copy
                if (!empty($company_name)) {
                    $attributes['role_description'] = str_replace('Company X',$company_name,$attributes['role_description']);
                    $attributes['role_summary'] = str_replace('Company X',$company_name,$attributes['role_summary']);
                }
                $sr->attributes = $attributes;
                $sr->company_id = $company_id;
                $sr->employee_id = null;
                $sr->status = 1;
                $sr->created_on = date('Y-m-d H:i:s');
                if (!$sr->save()) {
                    Yii::log(print_r($sr->getErrors(), true), 'error', 'SecurityRole_Ex.copyDefaultRoles');
                }
            }
        }
        return true;
    }


    public static function assignedToControlBaseline($securityRoleId,&$controlList)
    {
        $retVal = false;
        $controlList = array();
        $controls = ControlBaseline::model()->findAllByAttributes(array('security_role_id'=>$securityRoleId,'status'=>Cons::ACTIVE));
        if ($controls && count($controls) > 0) {
            $retVal = true;
            foreach($controls as $control) {
                $controlList[] = $control->title;
            }
        }
        return $retVal;
    }

    public static function getViewValues($companyId,$securityRoleId)
    {
        $v = array();
        $row = self::getSecurityRole($count,$companyId,$securityRoleId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Title'] = $model->role_title;
            $v['Status'] = $model->role_status_name;
            $v['Assigned To'] = $model->employee_name;
            $v['Department'] = $model->role_department;
            $v['Description'] = $model->role_description;
            $v['Summary'] = $model->role_summary;
            if (SecurityRole_ex::assignedToControlBaseline($securityRoleId,$controls)) {
                $v['Assigned to Control Baseline'] =  implode("<br/>",$controls);
            }
        }
        return $v;
    }

    public static function getRoleStatusCounts($companyId)
    {
        $retVal = array();
        $sql = "SELECT COUNT(sr.role_status) total, srs.role_status_name, srs.role_status_id"
            ." FROM security_role sr"
            ." INNER JOIN security_role_status srs ON sr.role_status=srs.role_status_id"
            ." WHERE sr.status=".Cons::ACTIVE." AND company_id = :companyId"
            ." GROUP BY srs.role_status_id"
            ." ORDER BY srs.role_status_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

    public static function getSecurityRole(&$count,$companyId,$securityRoleId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null,$status_id=null,$employeeId=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT sr.*, TRIM(CONCAT(e.firstname,' ',e.lastname)) employee_name, rs.role_status_name";
        }
        $from = " FROM security_role sr"
               ." INNER JOIN security_role_status rs ON sr.role_status=rs.role_status_id"
               ." LEFT JOIN employee e ON sr.employee_id=e.employee_id";
        $sqlWhere = " WHERE sr.status = ".Cons::ACTIVE." AND sr.company_id = :companyId";
        if ($securityRoleId) {
            $sqlWhere .= " AND sr.security_role_id = :security_role_id";
        }
        if ($status_id !== null) {
            $sqlWhere .= " AND sr.role_status = :role_status_id";
        }
        if ($employeeId !== null) {
            $sqlWhere .= " AND sr.employee_id = :employee_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $securityRoleId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($securityRoleId) {
                $cmdCount->bindParam(":security_role_id", $securityRoleId, PDO::PARAM_INT);
            }
            if ($status_id !== null) {
                $cmdCount->bindParam(":role_status_id", $status_id, PDO::PARAM_INT);
            }
            if ($employeeId !== null) {
                $cmdCount->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($securityRoleId) {
            $command->bindParam(":security_role_id", $securityRoleId, PDO::PARAM_INT);
        }
        if ($status_id !== null) {
            $command->bindParam(":role_status_id", $status_id, PDO::PARAM_INT);
        }
        if ($employeeId !== null) {
            $command->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($securityRoleId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getSecurityRolesJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null,$status_id=null,$employeeId=null)
    {
        $select = "SELECT sr.security_role_id, sr.role_title, sr.role_department,"
                 ." sr.employee_id, CONCAT(e.firstname,' ',e.lastname) employee_name, e.firstname, e.lastname, "
                 ." sr.role_status, rs.role_status_name";
        $rows = SecurityRole_Ex::getSecurityRole($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort,$status_id,$employeeId);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['security_role_id'] = Obscure::encode($row['security_role_id']);
                $row['employee_id'] = Obscure::encode($row['employee_id']);
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
}