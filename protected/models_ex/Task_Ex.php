<?php
/* @var $task Task */

class Task_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY target_date asc, task_id asc";

    public static function getViewValues($companyId,$taskId)
    {
        $v = array();
        $row = self::getTask($count,$companyId,$taskId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Task Name'] = $model->task_name;
            $v['Assigned To'] = $model->employee_name;
            $v['Description'] = $model->task_description;
            $v['Status'] = $model->task_status_name;
            $v['Target Date'] = DateHelper::FormatDateTimeString($model->target_date, 'm/d/Y');
            $v['Frequency'] = $model->task_frequency_name;
            $v['Related Control'] = $model->related_control;
            $v['Category'] = $model->category;
            $v['Created On'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y g:i:s a');
        }
        return $v;
    }

    public static function getTask(&$count,$companyId,$taskId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null,$employeeId=null,$onlyNotCompleted = false)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT t.*, ts.task_status_name, tf.task_frequency_name, TRIM(CONCAT(e.firstname,' ',e.lastname)) employee_name, cb.title related_control, cc.category";
        }
        $from = " FROM task t"
               ." INNER JOIN task_status ts ON t.task_status_id=ts.task_status_id"
               ." INNER JOIN task_frequency tf ON t.task_frequency_id=tf.task_frequency_id"
               ." LEFT JOIN employee e ON t.employee_id=e.employee_id"
               ." LEFT JOIN control_baseline cb ON t.control_baseline_id=cb.control_baseline_id"
               ." LEFT JOIN control_category cc ON cb.cat_id=cc.control_category_id";
        $sqlWhere = " WHERE t.company_id = :companyId";
        if ($taskId !== null) {
            $sqlWhere .= " AND t.task_id = :task_id";
        }
        if ($employeeId !== null) {
            $sqlWhere .= " AND t.employee_id = :employee_id";
        }
        if ($onlyNotCompleted) {
            $sqlWhere .= " AND t.task_status_id <> " . Cons::TASK_COMPLETE;
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere2 = str_ireplace('target_date','t.target_date',$sqlWhere2);
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $taskId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($taskId !== null) {
                $cmdCount->bindParam(":task_id", $taskId, PDO::PARAM_INT);
            }
            if ($employeeId !== null) {
                $cmdCount->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($taskId !== null) {
            $command->bindParam(":task_id", $taskId, PDO::PARAM_INT);
        }
        if ($employeeId !== null) {
            $command->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($taskId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getTasksJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null,$employeeId=null, $onlyNotCompleted = false)
    {
        $select = "SELECT t.task_id, t.task_name, t.target_date, t.task_description, ts.task_status_id, ts.task_status_name, tf.task_frequency_name, TRIM(CONCAT(e.firstname,' ',e.lastname)) employee_name, e.firstname, e.lastname";
        $rows = Task_Ex::getTask($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort,$employeeId,$onlyNotCompleted);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['task_id'] = Obscure::encode($row['task_id']);
                $row['target_date'] = DateHelper::FormatDateTimeString($row['target_date'], 'm/d/Y');
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
    
    public static function updateTaskStatus($companyId, $taskId, $stausId){
        $update = Yii::app()->db->createCommand()->update(
                'task', 
                array(
                    'task_status_id' => $stausId
                ), 
                "task_id = :task_id AND company_id = :company_id",
                array(
                    'task_id' => $taskId,
                    'company_id' => $companyId
                )
        );
        return $update;
    }

}