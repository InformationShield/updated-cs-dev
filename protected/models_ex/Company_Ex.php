<?php

class Company_Ex extends BaseSearchSort_Ex
{
    public static function getCompanyEmployee($company_id, $user_id)
    {
        $retVal = NULL;
        $sql = "
            SELECT cp.*, e.company_role_id role
            FROM tbl_users tu 
                INNER JOIN employee e ON tu.Email = e.email
                INNER JOIN company cp ON cp.company_id = e.company_id
            WHERE cp.status = ".Cons::ACTIVE."
                AND cp.company_id = :company_id
                AND tu.id = :user_id
        ";
        $sql = "
            SELECT *, MAX(company_role_id) role
            FROM (
                SELECT cp.*, ".Cons::ROLE_OWNER." company_role_id
                FROM company cp
                WHERE cp.status = ".Cons::ACTIVE."
                    AND cp.user_id = :user_id
                    AND cp.company_id = :company_id
                UNION ALL
            
                SELECT cp.*, e.company_role_id role
                FROM tbl_users tu 
                    INNER JOIN employee e ON tu.Email = e.email
                    INNER JOIN company cp ON cp.company_id = e.company_id
                WHERE cp.status = ".Cons::ACTIVE."
                	AND e.status = ".Cons::ACTIVE."
                    AND tu.id = :user_id
                    AND cp.company_id = :company_id
            ) AS companies
            GROUP BY company_id
            ORDER BY role desc, name asc;
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $command->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

    public static function getCompanyLogo($company_id)
    {
        $logo = '';
        $sql = "SELECT logo FROM company WHERE company_id=:company_id LIMIT 1";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results) && !empty($results['logo'])) {
            $logo = $results['logo'];
        }
        return $logo;
    }

    public static function getCompanyGuid($company_id,&$password)
    {
        $retVal = null;
        $password = '';
        $sql = "SELECT company_guid, password FROM company WHERE company_id=:company_id LIMIT 1";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results) && !empty($results['company_guid'])) {
            $retVal = $results['company_guid'];
            $password = $results['password'];
        }
        return $retVal;
    }

    public static function getCompaniesUserOwns($user_id)
    {
        $retVal = NULL;
        $sql = "
            SELECT cp.*
            FROM company cp
            WHERE cp.status = ".Cons::ACTIVE."
            AND cp.user_id = :user_id 
            ORDER BY cp.name asc
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

    public static function getUserCompanies($user_id, $roleGE=null)
    {
        $retVal = array();
        $roleWhere = "";
        if ($roleGE) {
            $roleWhere = " AND e.company_role_id >= :role";
        }
        $sql = "
            SELECT *, MAX(company_role_id) role
            FROM (
                SELECT cp.*, ".Cons::ROLE_OWNER." company_role_id
                FROM company cp
                WHERE cp.status = ".Cons::ACTIVE."
                    AND cp.user_id = :user_id
                UNION ALL
            
                SELECT cp.*, e.company_role_id role
                FROM tbl_users tu 
                    INNER JOIN employee e ON tu.Email = e.email
                    INNER JOIN company cp ON cp.company_id = e.company_id
                WHERE cp.status = ".Cons::ACTIVE."
                	AND e.status = ".Cons::ACTIVE."
                    AND tu.id = :user_id
                    $roleWhere
            ) AS companies
            GROUP BY company_id
            ORDER BY role desc, name asc;
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        if ($roleGE) {
            $command->bindParam(":role", $roleGE, PDO::PARAM_INT);
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

    public static function getAllCompanyOwners()
	{
		$retVal = array();
		$sql = "
			SELECT GROUP_CONCAT(DISTINCT cp.name SEPARATOR ', ') company_name, cp.user_id, e.firstname, e.lastname, e.email
			FROM company cp
				INNER JOIN tbl_users u ON u.id = cp.user_id
				INNER JOIN employee e ON e.email = u.email
			WHERE cp.status = 1
			GROUP BY e.email
			ORDER BY cp.name, e.lastname, e.firstname
		";
		$command = Yii::app()->db->createCommand($sql);
		$results = $command->query()->readAll();
		if (!empty($results)) {
			$retVal = $results;
		}
		return $retVal;
	}

	public static function getAllCompanies($filter)
	{
		$retVal = NULL;
		if (!$filter['sort']) {	// set default sort
			$filter['sort'] = array(
				array('field' => 'c.name', 'direction' => 'asc'),
			);
		}
		self::filter2Sql($filter, $whereSql, $sortSql, $params);
		$sql = "
			SELECT c.*, u.email, up.first_name, up.last_name
			FROM company c
			INNER JOIN tbl_users u ON u.id = c.user_id
			INNER JOIN tbl_profiles up ON up.user_id = c.user_id
			WHERE 1=1
			".$whereSql."
			".$sortSql."
		";
		$command = Yii::app()->db->createCommand($sql);
		if ($params) {
			foreach ($params as $param) {
				$command->bindParam($param['name'], $param['value'], $param['type']);
			}
		}
		$results = $command->query()->readAll();
		if (!empty($results)) {
			foreach($results as &$company) {
				$company['user_id'] = Obscure::encode($company['user_id']);
				$company['license_level'] = isset(mPermission::$Licenses[$company['license_level']]) ? mPermission::$Licenses[$company['license_level']] : '';
				$company['status_desc'] = $company['status'] ? 'active' : 'disabled';
				$company['trial_expiry'] = DateHelper::FormatDateTimeString($company['trial_expiry'], 'm/d/Y');
				$company['created_on'] = DateHelper::FormatDateTimeString($company['created_on'], 'm/d/Y');
			}
			$retVal = $results;
		}
		return $retVal;
	}

	public static function getCompanyLicenseRemaining($companyId)
	{
		$retVal = NULL;
		$sql = "
			SELECT DATEDIFF(trial_expiry, CURDATE()) AS TrialDaysRemaining
			FROM company
			WHERE company_id = :companyId;
		";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
		$results = $command->query()->read();
		if (!empty($results)) {
			$retVal = $results['TrialDaysRemaining'];
		}
		return $retVal;
	}

}
