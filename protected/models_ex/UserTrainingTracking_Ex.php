<?php

class UserTrainingTracking_Ex
{
    public static function getTrainingTrackingStatusCounts($company_id)
    {
        $stats = array(
            array('total'=>0,'status'=>'0','label'=>'Unopened'),
            array('total'=>0,'status'=>'1','label'=>'Viewed'),
            array('total'=>0,'status'=>'2','label'=>'Acknowledged'),
        );
        $sql = "SELECT COUNT(pt.user_training_tracking_id) total, ((pt.ack*10) + pt.view) status"
            ." FROM user_training_tracking pt"
            ." INNER JOIN training p ON pt.training_id=p.training_id AND p.training_status_id=".Cons::POLICY_STATUS_PUBLISHED." AND p.status=".Cons::ACTIVE
            ." WHERE pt.company_id = :company_id"
            ." GROUP BY ((pt.ack*10) + pt.view)"
            ." ORDER BY ((pt.ack*10) + pt.view)";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            foreach($results as $row) {
                $index = intval($row['status']);
                $index = ($index > 1) ? 2 : $index;
                if ($index >= 0 && $index <= 2 &&
                    isset($row['total']) &&
                    $row['total'] > 0) {
                    $stats[$index]['total'] += $row['total'];
                }
            }
        }

        $usertrainingcount = 0;
        $sql = "SELECT COUNT(employee_id) usertrainingcount"
                ." FROM user_profile_training upt"
                ." INNER JOIN training p ON upt.training_id=p.training_id and p.training_status_id=".Cons::POLICY_STATUS_PUBLISHED." AND p.status=".Cons::ACTIVE
                ." INNER JOIN user_profile up on up.user_profile_id=upt.user_profile_id AND up.company_id=:company_id"
                ." LEFT JOIN employee e ON up.profile_id=e.profile_id AND e.company_id=:company_id"
                ." WHERE employee_id is not null AND upt.company_id=:company_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (!empty($results) && isset($results['usertrainingcount']) && $results['usertrainingcount'] > 0) {
            $usertrainingcount = intval($results['usertrainingcount']);
        }

        if ($usertrainingcount > 0) {
            $stats[0]['total'] = $usertrainingcount - (intval($stats[1]['total']) + intval($stats[2]['total'])) ;
        }
        return $stats;
    }
    
    public static function track($training_id,$company_id,$user_id,$ack,$view)
    {
        $userTrainingTracking = UserTrainingTracking::model()->findByAttributes(array('training_id' => $training_id, 'user_id' => $user_id, 'company_id' => $company_id));
        if (!isset($userTrainingTracking)) {
            $userTrainingTracking = new UserTrainingTracking();
            $userTrainingTracking->training_id = $training_id;
            $userTrainingTracking->user_id = $user_id;
            $userTrainingTracking->company_id = $company_id;
        }
        if ($ack == 'yes') {
            $userTrainingTracking->ack = 1;
            $userTrainingTracking->ack_date = date("Y-m-d H:i:s");
        } else if ($ack == 'no') {
            $userTrainingTracking->ack = 0;
            $userTrainingTracking->ack_date = date("Y-m-d H:i:s");
        }
        if (!empty($view)) {
            $userTrainingTracking->view = 1;
            $userTrainingTracking->view_date = date("Y-m-d H:i:s");
        }
        if ($userTrainingTracking->save()) {
            return true;
        } else {
            Yii::log(print_r($userTrainingTracking->getErrors(), true), 'error', 'UserTrainingTracking_Ex.track');
        }
        return false;
    }
}