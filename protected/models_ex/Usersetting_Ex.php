<?php

class Usersetting_Ex extends Usersetting
{
    private static function GetDbRecord($userId, $keyName)
    {
        $sql = "SELECT * FROM usersetting";
        $sql .= " WHERE user_id=:UserId AND key_name=:KeyName";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":UserId", $userId, PDO::PARAM_INT);
        $command->bindParam(":KeyName", $keyName, PDO::PARAM_STR);
        $usersetting = $command->query()->read();
        if (!empty($usersetting))
            return (object)$usersetting;
        else
            return null;
    }

    private static function GetRecord($userId, $keyName)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = "user_id=:UserId AND key_name=:KeyName";
        $criteria->params = array('UserId'=>$userId, 'KeyName'=>$keyName);
        $usersetting = Usersetting::model()->find($criteria);
        if (isset($usersetting))
            return $usersetting;
        else
            return null;
    }

    public static function GetGlobalValue($keyName, $defaultValue=null)
    {
        return Usersetting_Ex::GetValue(0, $keyName, $defaultValue);
    }

    public static function GetValue($userId, $keyName, $defaultValue=null)
    {
        $usersetting = Usersetting_Ex::GetDbRecord($userId, $keyName);
        if (isset($usersetting))
            return $usersetting->key_value;
        else
            return $defaultValue;
    }

    public static function SetGlobalValue($keyName, $keyValue)
    {
        return Usersetting_Ex::SetValue(0, $keyName, $keyValue);
    }

    public static function SetValue($userId, $keyName, $keyValue)
    {
        $usersetting = Usersetting_Ex::GetRecord($userId, $keyName);
        if (isset($usersetting)) {
            $usersetting->key_value = $keyValue;
            $usersetting->isNewRecord = false;
            return $usersetting->save();
        } else {
            $usersetting = new Usersetting();
            $usersetting->isNewRecord = true;
            $usersetting->user_id = $userId;
            $usersetting->key_name = $keyName;
            $usersetting->key_value = $keyValue;
            return $usersetting->save();
        }
    }

    public static function DeleteGlobalValue($keyName)
    {
        return Usersetting_Ex::DeleteValue(0, $keyName);
    }

    public static function DeleteValue($userId, $keyName)
    {
        $usersetting = Usersetting_Ex::GetRecord($userId, $keyName);
        if (isset($usersetting)) {
            return $usersetting->delete();
        }
        return true;
    }

    // Returns true if successful in saving, false if failed
    public static function SaveJsonValue($userId, $keyName, $keyValue)
    {
        $retVal = FALSE;
        $usersetting = Usersetting_Ex::GetRecord($userId, $keyName);
        $jsonValue = json_encode($keyValue);
        if(json_last_error() === JSON_ERROR_NONE) {
            if (isset($usersetting)) {
                $usersetting->key_value = $jsonValue;
                $usersetting->isNewRecord = false;
                $retVal = $usersetting->save();
            } else {
                $usersetting = new Usersetting();
                $usersetting->isNewRecord = true;
                $usersetting->user_id = $userId;
                $usersetting->key_name = $keyName;
                $usersetting->key_value = $jsonValue;
                $retVal = $usersetting->save();
            }
        }
        return $retVal;
    }

    public static function GetJsonValue($userId, $keyName)
    {
        $retVal = null;
        $usersetting = Usersetting_Ex::GetDbRecord($userId, $keyName);
        if (isset($usersetting)) {
            $keyValue = json_decode($usersetting->key_value);
            if(json_last_error() === JSON_ERROR_NONE) {
                $retVal = $keyValue;
            }
        }
        return $retVal;
    }
}