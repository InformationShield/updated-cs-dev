<?php
/* @var $incident Incident */

class Incident_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY updated_on asc, incident_id asc";

    public static function getViewValues($companyId,$incidentId,$userId=null)
    {
        $v = array();
        $row = self::getIncident($count,$companyId,$incidentId,0,0,null,null,null,null,null,$userId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Type'] = $model->incident_type;
            $v['Summary'] = $model->summary;
            $v['Reported By'] = $model->user_name;
            $v['Reported On'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y g:i:s a');
            $v['Status'] = $model->incident_status_name;
            $v['Assigned To'] = $model->employee_name;
            $v['Updated On'] = DateHelper::FormatDateTimeString($model->updated_on, 'm/d/Y g:i:s a');
            $v['System'] = $model->system;
            $v['Description'] = $model->description;
        }
        return $v;
    }

    public static function getIncident(&$count,$companyId,$incidentId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null,$employeeId=null,$userId=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT inc.*, ist.incident_status_name, it.incident_type, TRIM(CONCAT(e.firstname,' ',e.lastname)) employee_name, TRIM(CONCAT(tp.first_name,' ',tp.last_name)) user_name";
        }
        $from = " FROM incident inc"
               ." LEFT JOIN incident_status ist ON inc.incident_status_id=ist.incident_status_id"
               ." LEFT JOIN incident_type it ON inc.incident_type_id=it.incident_type_id"
               ." LEFT JOIN employee e ON inc.employee_id=e.employee_id"
               ." LEFT JOIN tbl_profiles tp ON inc.user_id=tp.user_id";
        $sqlWhere = " WHERE inc.company_id = :companyId";
        if ($incidentId !== null) {
            $sqlWhere .= " AND inc.incident_id = :incident_id";
        }
        if ($userId !== null) {
            $sqlWhere .= " AND inc.user_id = :user_id";
        }
        if ($employeeId !== null) {
            $sqlWhere .= " AND inc.employee_id = :employee_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere2 = str_ireplace('i_reported_on','DATE(inc.created_on)',$sqlWhere2);
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $incidentId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($incidentId !== null) {
                $cmdCount->bindParam(":incident_id", $incidentId, PDO::PARAM_INT);
            }
            if ($userId !== null) {
                $cmdCount->bindParam(":user_id", $userId, PDO::PARAM_INT);
            }
            if ($employeeId !== null) {
                $cmdCount->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($incidentId !== null) {
            $command->bindParam(":incident_id", $incidentId, PDO::PARAM_INT);
        }
        if ($userId !== null) {
            $command->bindParam(":user_id", $userId, PDO::PARAM_INT);
        }
        if ($employeeId !== null) {
            $command->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($incidentId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getIncidentsJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null,$employeeId=null,$userId=null)
    {
        $select = "SELECT inc.incident_id, inc.summary, inc.description, inc.created_on, DATE(inc.created_on) as i_reported_on, inc.updated_on, ist.incident_status_name, it.incident_type,"
            ." TRIM(CONCAT(e.firstname,' ',e.lastname)) employee_name, e.firstname, e.lastname";
        $rows = Incident_Ex::getIncident($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort,$employeeId,$userId);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['incident_id'] = Obscure::encode($row['incident_id']);
                $row['updated_on'] = DateHelper::FormatDateTimeString($row['updated_on'], 'm/d/Y g:i:s a');
                $row['created_on'] = DateHelper::FormatDateTimeString($row['created_on'], 'm/d/Y g:i:s a');
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

}