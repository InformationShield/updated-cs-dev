<?php

class BaseSearchSort_Ex
{
    protected static $orderBy = "";

    protected static function sort2OrderBy($sort)
    {
        $orderBy = static::$orderBy;
        if(isset($sort) && is_array($sort) && !empty($sort)){
            $orderBy = " ORDER BY " . implode(",", array_map(function($entry){
                return $entry['field'] . " " . $entry['direction'];
            }, $sort));
        }
        return $orderBy;
    }

    protected static function advanceSearch2Where($search,$searchLogic="OR",&$params,$fieldPrefix="")
    {
        $params = array();
        $where = '';
        if (!empty($search)) {
            $searchLogic = ($searchLogic == "AND" || $searchLogic == "OR") ? $searchLogic : "OR";
            foreach ($search as $index => $clause) {
                $whereClause = self::processClause($clause, $index, $params,$fieldPrefix);
                if (empty($where)) {
                    $where = $whereClause;
                } else if (!empty($whereClause)) {
                    $where .= ' ' . $searchLogic . ' ' . $whereClause;
                }
            }
            if (!empty($where)) {
                $where = '(' . $where . ')';
            }
        }
        // debug
        //yiilog($where."\n".print_r($params,true));
        return $where;
    }

	protected static function filter2Sql($filter, &$whereSql, &$sortSql, &$params, $fieldPrefix="")
	{
		$params = array();
		$search = $filter['search'];
		$sort = $filter['sort'];

		$whereSql = '';
		if (!empty($search)) {
			$searchLogic = ($filter['searchLogic'] == "AND" || $filter['searchLogic'] == "OR") ? $filter['searchLogic'] : "AND";
			foreach ($search as $index => $clause) {
				$whereClause = self::processClause($clause, $index, $params, $fieldPrefix);
				if (empty($whereSql)) {
					$whereSql = $whereClause;
				} else if (!empty($whereClause)) {
					$whereSql .= ' ' . $searchLogic . ' ' . $whereClause;
				}
			}
			if (!empty($whereSql)) {
				$whereSql = ' AND (' . $whereSql . ')';
			}
		}

		$sortSql = "";
		if(isset($sort) && is_array($sort) && !empty($sort)){
			$sortSql = " ORDER BY " . implode(",", array_map(function($entry) {
					return $entry['field'] . " " . $entry['direction'];
				}, $sort));
		}
	}

	private static function processClause(&$clause,&$index,&$params,$fieldPrefix)
    {
        switch ($clause['type']) {
            case 'text':
                $whereClause = self::processTextOperator($clause,$index,$params,$fieldPrefix);
                break;
            case 'int':
                $whereClause = self::processOperator('int',$clause,$index,$params,$fieldPrefix);
                break;
            case 'date':
                $whereClause = self::processOperator('date',$clause,$index,$params,$fieldPrefix);
                break;
            case 'list':
                $whereClause = self::processOperator('list',$clause,$index,$params,$fieldPrefix);
                break;
            default:
                $whereClause = '';
                break;
        }
        return $whereClause;
    }

    private static function processTextOperator($clause,&$index,&$params,$fieldPrefix)
    {
        $whereClause = '';
        $pname = ":text".$index;
        if (!empty($clause['value']) && !empty($clause['field'])) {
            $field = $fieldPrefix.$clause['field'];
            switch ($clause['operator']) {
                case 'is':
                    $value = $clause['value'];
                    $whereClause = $value. " = " . $pname;
                    break;
                case 'contains':
                    $value = '%'.$clause['value'].'%';
                    $whereClause = $field . " LIKE " . $pname;
                    $params[] = array('name' => $pname, 'value' => $value, 'type' => PDO::PARAM_STR);
                    break;
                case 'begins':
                    $valueA = $clause['value'].'%';
                    $valueB = "<p>".$clause['value'].'%';
                    $pnameA = $pname."a";
                    $pnameB = $pname."b";
                    $whereClause = "(".$field . " LIKE " . $pnameA." OR ".$field . " LIKE " . $pnameB.")";
                    $params[] = array('name' => $pnameA, 'value' => $valueA, 'type' => PDO::PARAM_STR);
                    $params[] = array('name' => $pnameB, 'value' => $valueB, 'type' => PDO::PARAM_STR);
                    break;
                case 'ends':
                    $valueA = '%'.$clause['value'];
                    $valueB = '%'.$clause['value']."</p>";
                    $pnameA = $pname."a";
                    $pnameB = $pname."b";
                    $whereClause = "(".$field . " LIKE " . $pnameA." OR ".$field . " LIKE " . $pnameB.")";
                    $params[] = array('name' => $pnameA, 'value' => $valueA, 'type' => PDO::PARAM_STR);
                    $params[] = array('name' => $pnameB, 'value' => $valueB, 'type' => PDO::PARAM_STR);
                    break;
                default:
                    $whereClause = '';
                    break;
            }
        }
        return $whereClause;
    }

    private static function processOperator($type,$clause,$index,&$params,$fieldPrefix)
    {
        $whereClause = '';
        $pname = ":".$type.$index;
        $hasValue = false;
        if (is_array($clause['value'])) {
            if (count($clause['value']) > 0) {
                if (!empty($clause['value'][0])) {
                    $hasValue = true;
                }
            }
        } else if (!empty($clause['value'])) {
            $hasValue = true;
        } else if (is_numeric($clause['value'])) { // in case of passing 0 as a value
            $hasValue = true;
        }
        
        if ($hasValue && !empty($clause['field'])) {
            $field = $fieldPrefix.$clause['field'];
            $pdoType = ($type == 'int') ?  PDO::PARAM_INT : PDO::PARAM_STR;
            switch ($clause['operator']) {
                case 'is':
                    $whereClause = $field . " = " . $pname;
                    if ($type == 'date') {
                        $value = DateHelper::FormatDateTimeString($clause['value'],DateHelper::FORMAT_DATEONLY);
                    } else {
                        $value = $clause['value'];
                    }
                    $params[] = array('name' => $pname, 'value' => $value, 'type' => $pdoType);
                    break;
                case 'in':
                    $values = self::processValues($clause['value'],$pname,$type,$params);
                    if (!empty($values)) {
                        $whereClause = $field . " IN (".implode(',',$values).')';
                    }
                    break;
                case 'not in':
                    $values = self::processValues($clause['value'],$pname,$type,$params);
                    if (!empty($values)) {
                        $whereClause = $field . " IN (".implode(',',$values).')';
                    }
                    break;
                case 'between':
                    $values = self::processValues($clause['value'],$pname,$type,$params,2);
                    if (!empty($values) && count($values) == 2) {
                        $whereClause = $field . " BETWEEN ".$values[0].' AND '.$values[1];
                    }
                    break;
                default:
                    $whereClause = '';
                    break;
            }
        }
        return $whereClause;
    }

    private static function processValues($values,$pname,$type,&$params,$maxCount=0)
    {
        $retValues = array();
        $pdoType = ($type == 'int') ?  PDO::PARAM_INT : PDO::PARAM_STR;
        foreach ($values as $key=>$value) {
            if (!empty($value)) {
                if ($type == 'int' && !is_numeric($value)) {
                    // if not both numbers then ignore
                    return array();
                }
                $pname2 = $pname.'v'.$key;
                if ($type == 'date') {
                    $value = DateHelper::FormatDateTimeString($value,DateHelper::FORMAT_DATEONLY);
                }
                $params[] = array('name' => $pname2, 'value'=>$value, 'type'=>$pdoType);
                $retValues[] = $pname2;
                if (count($retValues) >= $maxCount) {
                    break;
                }
            }
        }
        return $retValues;
    }
}