<?php

class VideoTracking_Ex
{
    public static function track($video_training_id,$company_id,$user_id,$ack,$training_id)
    {
        $retVal = false;
        $videoTracking = VideoTracking::model()->findByAttributes(array('video_training_id' => $video_training_id, 'user_id' => $user_id, 'company_id' => $company_id));
        if (!isset($videoTracking)) {
            $videoTracking = new VideoTracking();
            $videoTracking->video_training_id = $video_training_id;
            $videoTracking->user_id = $user_id;
            $videoTracking->company_id = $company_id;
        }
        if ($ack == 'yes') {
            $videoTracking->ack = 1;
            $videoTracking->ack_date = date("Y-m-d H:i:s");
        } else if ($ack == 'no') {
            $videoTracking->ack = 0;
            $videoTracking->ack_date = date("Y-m-d H:i:s");
        }
        if ($videoTracking->view != 1) {
            $videoTracking->view = 1;
            $videoTracking->view_date = date("Y-m-d H:i:s");
        }
        if ($videoTracking->save()) {
            $retVal = true;
        } else {
            Yii::log(print_r($videoTracking->getErrors(), true), 'error', 'MyController.actionAck');
        }

        $stats = VideoTraining_Ex::getViewAckCounts($training_id,$company_id,$user_id);
        $tra_ack = 'no';
        if ($stats && $stats['vid_count'] == $stats['ack_count']) {
            $tra_ack = 'yes'; // all video watched
        }
        // a view of a video marks training as viewed and if all acked the training is acked
        UserTrainingTracking_Ex::track($training_id,$company_id,$user_id,$tra_ack,'yes');

        return $retVal;
    }
}