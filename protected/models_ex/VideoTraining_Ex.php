<?php
/* @var $videotraining VideoTraining */

class VideoTraining_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY training_title asc, sort_order asc";

    public static function getViewAckCounts($training_id,$company_id,$user_id)
    {
        $sql = "SELECT count(vt.video_training_id) vid_count, sum(tra.view) view_count, sum(tra.ack) ack_count"
                ." FROM video_training vt"
                ." LEFT JOIN video_tracking tra ON vt.video_training_id=tra.video_training_id AND vt.company_id=tra.company_id AND tra.user_id=:user_id"
                ." WHERE vt.training_id=:training_id AND vt.company_id=:company_id AND vt.status=1";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":training_id", $training_id, PDO::PARAM_INT);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $command->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $retVal = $command->query()->read();
        if (empty($retVal)) {
            $retVal = array();
        }
        return $retVal;
    }

    public static function getVideosWithTracking($training_id,$company_id,$user_id)
    {
        $sql = "SELECT vt.*, tra.ack, tra.view, tra.ack_date, tra.view_date"
            ." FROM video_training vt"
            ." LEFT JOIN video_tracking tra ON vt.video_training_id=tra.video_training_id AND vt.company_id=tra.company_id AND tra.user_id=:user_id"
            ." WHERE vt.training_id=:training_id AND vt.company_id=:company_id AND vt.status=1";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":training_id", $training_id, PDO::PARAM_INT);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $command->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $retVal = $command->query()->readAll();
        if (empty($retVal)) {
            $retVal = array();
        }
        return $retVal;
    }

    public static function deleteVideoTraining($training_library_id,$training_id,$company_id,$markStatusOnly=false)
    {
        $deleteSuccess = true;
        $criteria = new CDbCriteria(array('order' => 'sort_order asc, video_training_id asc'));
        if (isset($training_library_id)) {
            $videos = VideoTraining::model()->findAllByAttributes(array('training_library_id' => $training_library_id), $criteria);
        } else {
            $videos = VideoTraining::model()->findAllByAttributes(array('training_id' => $training_id, 'company_id' => $company_id), $criteria);
        }
        foreach($videos as $video) {
            $video->status = 0;
            $deleteSuccess = $video->save();
            if ($markStatusOnly === false) {
                $deleteSuccess = $video->delete();
            }
        }
        return $deleteSuccess;
    }

    public static function getTrainingTitleList($companyId,$training_type=2,$withLibrary=false)
    {
        $retVal = array();
        if ($companyId) {
            if ($withLibrary) {
                $sql = "SELECT 1 library, training_library_id id, CONCAT_WS(' - ','Training Library',doc_title) doc_title"
                    . " FROM training_library"
                    . " WHERE status = 1"
                    . " AND training_type = :training_type"
                    . " UNION"
                    . " SELECT 0 library, training_id id, doc_title"
                    . " FROM training where status=1"
                    . " AND company_id = :companyId"
                    . " AND training_type = :training_type"
                    . " ORDER BY library desc, doc_title asc;";
            } else {
                $sql = "SELECT 0 library, training_id id, doc_title"
                    . " FROM training where status=1"
                    . " AND company_id = :companyId"
                    . " AND training_type = :training_type"
                    . " ORDER BY library desc, doc_title asc;";
            }
        } else {
            if ($withLibrary) {
                $sql = "SELECT 1 library, training_library_id id, CONCAT_WS(' - ','Training Library',doc_title) doc_title"
                    . " FROM training_library"
                    . " WHERE status = 1"
                    . " AND training_type = :training_type"
                    . " ORDER BY library desc, doc_title asc;";
            } else {
                $sql = null;
            }
        }
        if ($sql) {
            $command = Yii::app()->db->createCommand($sql);
            if ($companyId) {
                $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            }
            $command->bindParam(":training_type", $training_type, PDO::PARAM_INT);
            $retVal = $command->query()->readAll();
            if (empty($retVal)) {
                $retVal = array();
            }
        }
        return $retVal;
    }

    public static function validateVideoCodeFrame($videoCode)
    {
        $videoCode = str_replace("height=\"720\"", "height=\"400\"", $videoCode);
        $videoCode = str_replace("width=\"1280\"", "width=\"500\"", $videoCode);
        $videoCode = str_replace('http:', 'https:', $videoCode);
        return $videoCode;
    }

    public static function getViewValues($companyId,$videotrainingId)
    {
        $v = array();
        $row = self::getVideoTraining($count,$companyId,$videotrainingId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Video Title'] = $model->video_title;
            $v['Training Title'] = $model->training_title;
            $v['Play Order'] = $model->sort_order;
            $v['Created On'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y h:m:i');
            if (!empty($model->company)) $v['Company'] = $model->company;
            if (!empty($model->username)) $v['User Name'] = $model->username;
            $v['Video Code'] = $model->video_code;
            $v['Video'] = VideoTraining_Ex::validateVideoCodeFrame($model->video_code);
        }
        return $v;
    }

    public static function getVideoTraining(&$count,$companyId=null,$videotrainingId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT vt.*, CONCAT_WS('', tl.doc_title, t.doc_title) training_title, CONCAT_WS(' ',u.first_name,u.last_name) username, cp.name company ";
        }
        $from = " FROM video_training vt"
            ." LEFT JOIN training_library tl ON vt.training_library_id=tl.training_library_id"
            ." LEFT JOIN training t ON vt.training_id=t.training_id"
            ." LEFT JOIN company cp ON vt.company_id=cp.company_id"
            ." LEFT JOIN tbl_profiles u ON vt.user_id=u.user_id";
        $sqlWhere = " WHERE vt.status = 1";
        if ($companyId) {
            $sqlWhere .= " AND vt.company_id = :companyId AND vt.training_library_id IS NULL";
        } else {
            $sqlWhere .= " AND vt.training_library_id IS NOT NULL";
        }
        if ($videotrainingId) {
            $sqlWhere .= " AND vt.video_training_id = :video_training_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $videotrainingId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            if ($companyId) {
                $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            }
            if ($videotrainingId) {
                $cmdCount->bindParam(":video_training_id", $videotrainingId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        if ($companyId) {
            $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        }
        if ($videotrainingId) {
            $command->bindParam(":video_training_id", $videotrainingId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($videotrainingId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getVideoTrainingJson($companyId=null,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT vt.video_training_id, vt.video_title, vt.sort_order, vt.created_on, CONCAT_WS('', tl.doc_title, t.doc_title) training_title, cp.name company";
        $rows = VideoTraining_Ex::getVideoTraining($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['video_training_id'] = Obscure::encode($row['video_training_id']);
                $row['created_on'] = DateHelper::FormatDateTimeString($row['created_on'], 'm/d/Y');
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
}