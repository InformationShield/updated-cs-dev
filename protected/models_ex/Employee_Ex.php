<?php
/* @var $model Employee */

class Employee_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY e.lastname asc, e.firstname asc";

    public static function getUserDetails($employee_id,$company_id=null)
    {
        $retVal = false;
        $sql = "SELECT e.*, cp.name company_name, r.company_role_name, up.user_profile_id, up.profile_title, up.profile_description, up.status user_profile_status, u.id user_id, u.username, u.status user_status, u.create_at, u.lastvisit_at"
            ." FROM employee e"
            ." INNER JOIN company cp ON e.company_id=cp.company_id"
            ." INNER JOIN company_role r ON e.company_role_id=r.company_role_id"
            ." INNER JOIN user_profile up on e.profile_id=up.profile_id and e.company_id=up.company_id"
            ." LEFT JOIN tbl_users u on e.email=u.email"
            ." WHERE e.employee_id=:employee_id";
        if ($company_id) {
            $sql .= " AND e.company_id=:company_id";
        }
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":employee_id", $employee_id, PDO::PARAM_INT);
        if ($company_id) {
            $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        }
        $retVal = $command->query()->read();
        return $retVal;
    }

    public static function assignedSecurityRole($employeeId,&$roleList)
    {
        $retVal = false;
        $roleList = array();
        $securityRoles = SecurityRole::model()->findAllByAttributes(array('employee_id'=>$employeeId));
        if ($securityRoles && count($securityRoles) > 0) {
            $retVal = true;
            foreach($securityRoles as $role) {
                $roleList[] = $role->role_title;
            }
        }
        return $retVal;
    }

    public static function assignedTasks($employeeId,&$taskList)
    {
        $retVal = false;
        $taskList = array();
        $tasks = Task::model()->findAllByAttributes(array('employee_id'=>$employeeId));
        if ($tasks && count($tasks) > 0) {
            $retVal = true;
            foreach($tasks as $task) {
                $taskList[] = $task->task_name;
            }
        }
        return $retVal;
    }
    
    public static function getViewValues($companyId,$employeeId,$showAssigment=true)
    {
        $v = array();
        $row = self::getEmployee($count,$companyId,$employeeId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['First Name'] = $model->firstname;
            $v['Last Name'] = $model->lastname;
            $v['Email'] = $model->email;
            $v['Role'] = $model->company_role_name;
            $v['Department Id'] = $model->department_id;
            $v['Department'] = $model->department_name;
            $v['Profile Id'] = $model->profile_id;
            $v['Profile'] = $model->profiles;
            if ($showAssigment) {
                if (Employee_Ex::assignedSecurityRole($employeeId, $roleList)) {
                    $v['Security Roles assigned'] = implode("<br/>", $roleList);
                }
                if (Employee_Ex::assignedTasks($employeeId, $taskList)) {
                    $v['Tasks assigned'] = implode("<br/>", $taskList);
                }
            }
        }
        return $v;
    }

    public static function getUserEmployedList($email)
    {
        $retVal = NULL;
        $sql = "
            SELECT e.*, cp.name, cp.license_level, cp.logo
            FROM employee e
              INNER JOIN company cp ON e.company_id = cp.company_id
            WHERE cp.status = ".Cons::ACTIVE."
                AND e.email = :email
            ORDER BY cp.name asc
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":email", $email, PDO::PARAM_STR);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }

	public static function getEmployeeCount($companyId)
    {
        $retVal = 0;
        $sql = "SELECT COUNT(employee_id) numberofusers"
            ." FROM employee e"
            ." WHERE e.company_id = :companyId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $countResults = $command->query()->read();
        if (isset($countResults["numberofusers"]) && $countResults["numberofusers"] > 0) {
            $retVal = $countResults["numberofusers"];
        }
        return $retVal;
    }

    public static function getEmployee(&$count,$companyId,$employeeId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT e.*, TRIM(CONCAT(e.firstname,' ',e.lastname)) employee_name, cr.company_role_name,"
                    . " GROUP_CONCAT(up.profile_title ORDER BY up.profile_title ASC SEPARATOR ', ') profiles";
        }

        $from = "
        	FROM employee e
            	INNER JOIN status s ON s.status_id = e.status
            	INNER JOIN company_role cr ON e.company_role_id = cr.company_role_id
				LEFT JOIN employee2profile e2p ON e2p.employee_id = e.employee_id
				LEFT JOIN user_profile up ON up.user_profile_id = e2p.user_profile_id
		";
        $sqlWhere = " WHERE e.company_id = :companyId";
        if ($employeeId) {
            $sqlWhere .= " AND e.employee_id = :employee_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        $groupBy = " GROUP BY e.employee_id";
        if ($pageSize > 0 && $employeeId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.$groupBy.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($employeeId) {
                $cmdCount->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$groupBy.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($employeeId) {
            $command->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($employeeId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getEmployeesJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "
			SELECT e.employee_id, e.email, e.firstname, e.lastname, e.company_role_id, cr.company_role_name, 
				e.department_name, e.department_id, s.status_name,
            	GROUP_CONCAT(up.profile_title ORDER BY up.profile_title ASC SEPARATOR ', ') profile_title
        ";

        
        $rows = Employee_Ex::getEmployee($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['employee_id'] = Obscure::encode($row['employee_id']);
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

    public static function preDeleteCheck($id)
    {
        $results = true;
        if (Employee_Ex::assignedSecurityRole($id,$roleList)) {
            $results = array();
            $results['status'] = 'error';
            $results['message'] = "Can not delete this user because this user is still assigned to the following Security Roles:<br/><br/>".implode("<br/>",$roleList)
                ."<br/><br/>Please reassign those Security Roles then delete this user.";
            $results['action'] = "See Security Roles for this user";
            $results['actionUrl'] = url('securityrole/list',array('cuid'=>Obscure::encode($id)));
        } else if (Employee_Ex::assignedTasks($id,$taskList)) {
            $results = array();
            $results['status'] = 'error';
            $results['message'] = "Can not delete this user because this user is still assigned to the following Tasks:<br/><br/>".implode("<br/>",$taskList)
                ."<br/><br/>Please reassign those Tasks then delete this user.";
            $results['action'] = "See Tasks for this user";
            $results['actionUrl'] = url('task/list',array('cuid'=>Obscure::encode($id)));
        }
        return $results; // return true or results array with status and message etc
    }

}