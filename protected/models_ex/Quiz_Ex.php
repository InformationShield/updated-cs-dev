<?php
/* @var $quiz Quiz */

class Quiz_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY quiz_title asc, publish_date asc";


    public static function deleteQuizQuestion($quiz_question_id)
    {
        $del = false;
        $model = QuizQuestion::model()->findByPk($quiz_question_id);
        if (isset($model)) {
            try {
                $del = $model->delete();
            } catch (Exception $e) {
                $del = false;
            }
        }
        if (!$del) {
            try {
                $model->status = Cons::DELETED; // hidden since it can not be deleted do to references
                $del = $model->save();
            } catch (Exception $e) {
                $del = false;
            }
        }
        return $del;
    }
    
    public static function saveQuizQuestion($quiz_library_id,$quiz_id,$company_id,$user_id,$questionList)
    {
        $errorMessages = array();
        // will remove any old records left in this list at the end
        $oldQuestionList = Quiz_Ex::getQuestions($quiz_id,$company_id);
        if (!empty($questionList)) {
            $order = 0;
            foreach ($questionList as $question) {
                //sanitize the correct answer and question type
                if ($question['question_type'] == 1) {
                    $question['correct_answer'] = $question['correct_answer_type1'];
                    $question['answer1'] = 'True';
                    $question['answer2'] = 'False';
                    $question['answer3'] = NULL;
                    $question['answer4'] = NULL;
                    $question['answer5'] = NULL;
                } else if ($question['question_type'] == 2) {
                    $question['correct_answer'] = $question['correct_answer_type2'];
                    $question['answer1'] = 'Yes';
                    $question['answer2'] = 'No';
                    $question['answer3'] = NULL;
                    $question['answer4'] = NULL;
                    $question['answer5'] = NULL;
                } else {
                    $question['question_type'] = 3; // must be multiple choice
                }
                unset($question['correct_answer_type1']);
                unset($question['correct_answer_type2']);
                $question['sort_order'] = ++$order;
                $question['quiz_library_id'] = $quiz_library_id;
                $question['quiz_id'] = $quiz_id;
                $question['company_id'] = $company_id;
                $question['user_id'] = $user_id;
                $question['created_on'] = date('Y-m-d H:i:s');
                if (empty($question['quiz_question_id'])) {
                    $quizQuestion = new QuizQuestion();
                    unset($question['quiz_question_id']);
                    $quizQuestion->attributes = $question;
                    if (!$quizQuestion->save()) {
                        $errorMessages[] = "Failed to save changes for new quiz question record.";
                    }
                } else {
                    $quizQuestion = QuizQuestion::model()->findByPk($question['quiz_question_id']);
                    if (!empty($quizQuestion)) {
                        // remove from oldQuestionList, since we will remove any old records left in this list at the end
                        foreach($oldQuestionList as $key=>$qq) {
                            if ($qq['quiz_question_id'] == $question['quiz_question_id']) {
                                unset($oldQuestionList[$key]);
                                break;
                            }
                        }
                        // now we can unset value
                        unset($question['quiz_question_id']);
                        $quizQuestion->attributes = $question;
                        if (!$quizQuestion->save()) {
                            $errorMessages[] = "Failed to save changes for existing quiz question record.";
                        }
                    } else {
                        $errorMessages[] = "Failed to update changes for existing quiz question, because it was not found.";
                    }
                }
            }
            // delete old Question records no longer needed
            foreach($oldQuestionList as $key=>$que) {
                if (!self::deleteQuizQuestion($que['quiz_question_id'])) {
                    $errorMessages[] = "Failed to delete unwanted quiz question record.";
                }
            }
        }
        return $errorMessages;
    }

    public static function getQuestions($quiz_id,$company_id)
    {
        $retVal = array();
        $sql = "SELECT qq.*"
                ." FROM quiz_question qq"
                ." WHERE qq.quiz_id=:quiz_id AND qq.status=".Cons::ACTIVE
                ." AND qq.company_id=:company_id"
                ." ORDER BY qq.sort_order ASC";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":company_id", $company_id, PDO::PARAM_INT);
        $command->bindParam(":quiz_id", $quiz_id, PDO::PARAM_INT);
        $results = $command->query()->readAll();
        if (!empty($results)) {
            $retVal = $results;
        }
        return $retVal;
    }
    
    public static function deleteQuestions($quiz_library_id,$quiz_id,$company_id,$markDeleted=false)
    {
        $deleteSuccess = true;
        $criteria = new CDbCriteria(array('order' => 'sort_order asc, quiz_question_id asc'));
        if (isset($quiz_library_id)) {
            $questions = QuizQuestion::model()->findAllByAttributes(array('quiz_library_id' => $quiz_library_id), $criteria);
        } else {
            $questions = QuizQuestion::model()->findAllByAttributes(array('quiz_id' => $quiz_id, 'company_id' => $company_id), $criteria);
        }
        foreach($questions as $question) {
            if ($markDeleted) {
                $question->status = 0;
                if (!$question->save()) {
                    $deleteSuccess = false;
                }
            } else {
                if (!$question->delete()) {
                    $deleteSuccess = false;
                }
            }
        }
        return $deleteSuccess;
    }
    
    public static function getViewValues($companyId,$quizId)
    {
        $v = array();
        $row = self::getQuiz($count,$companyId,$quizId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Title'] = $model->quiz_title;
            $v['Description'] = $model->description;
            $v['Status'] = $model->quiz_status_name;
            $v['Created Date'] = DateHelper::FormatDateTimeString($model->created_on, 'm/d/Y');
            if ($model->quiz_status_id==Cons::POLICY_STATUS_PUBLISHED) {
                $v['Published Date'] = DateHelper::FormatDateTimeString($model->publish_date, 'm/d/Y');
            }
            $v['Passing Score'] = $model->passing_score;
            $v['Maximum Retakes'] = $model->max_retakes;
        }
        return $v;
    }

    public static function getQuizCountsByStatus($companyId,$quiz_status)
    {
        $retVal = 0;
        $sql = "SELECT COUNT(quiz_id) total"
            ." FROM quiz"
            ." WHERE quiz_status_id=:quiz_status_id AND status=".Cons::ACTIVE." AND company_id = :companyId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $command->bindParam(":quiz_status_id", $quiz_status, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (isset($results) && isset($results['total'])) {
            $retVal = $results['total'];
        }
        return $retVal;
    }

    public static function getQuiz(&$count,$companyId,$quizId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT t.*, ts.quiz_status_name";
        }
        $from = " FROM quiz t"
               ." INNER JOIN quiz_status ts ON t.quiz_status_id=ts.quiz_status_id";
        $sqlWhere = " WHERE t.company_id = :companyId AND t.status=".Cons::ACTIVE;
        if ($quizId) {
            $sqlWhere .= " AND t.quiz_id = :quiz_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $quizId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($quizId) {
                $cmdCount->bindParam(":quiz_id", $quizId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($quizId) {
            $command->bindParam(":quiz_id", $quizId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($quizId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getQuizJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT t.quiz_id, t.quiz_title, t.publish_date, t.description, t.quiz_status_id, ts.quiz_status_name";
        $rows = Quiz_Ex::getQuiz($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['quiz_id'] = Obscure::encode($row['quiz_id']);
                if ($row['quiz_status_id'] == Cons::POLICY_STATUS_PUBLISHED) {
                    $row['publish_date'] = DateHelper::FormatDateTimeString($row['publish_date'], 'm/d/Y');
                } else {
                    $row['publish_date'] = '';
                }
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
}