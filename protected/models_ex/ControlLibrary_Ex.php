<?php
class ControlLibrary_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY cat_id asc, sort_order asc";


    public static function getControlIdsByPriority($priorityList, $excludeList)
    {
        $retVal = array();
        if (count($priorityList) > 0) {
            $priorityList = implode(',',$priorityList);
            $sql = "SELECT control_id FROM control_library cl"
                    ." WHERE cl.status=1 "
                    ." AND cl.priority IN (" . $priorityList . ")";
            if (count($excludeList) > 0) {
                $excludeList = implode(',',$excludeList);
                $sql .= " AND control_id NOT IN (".$excludeList.")";
            }
            $sql .= " ORDER BY control_id asc";
            $command = Yii::app()->db->createCommand($sql);
            $results = $command->query()->readAll();
            if (!empty($results)) {
                foreach($results as $row) {
                    $retVal[] = $row['control_id'];
                }
            }
        }
        return $retVal;
    }
    
    // returns error message code 1,2,3 or true
    public static function copyToControlBaseline($control_id,$baseline_id,$company_id,$user_id,$company_name=null)
    {
        if (empty($company_name)) {
            $company = Company::model()->findByPk($company_id);
            if ($company) {
                $company_name = trim($company->name);
            }
        }
        $retVal = 1;//'The control failed to be saved to your baseline.';
        $model1 = ControlLibrary::model()->findByAttributes(array('control_id' => $control_id));
        if (isset($model1)) {
            $model2 = ControlBaseline::model()->findByAttributes(array('control_id' => $control_id, 'company_id' => $company_id, 'baseline_id'=>$baseline_id));
            if (isset($model2)) {
                $retVal = 2;//'The control was not added since it already exists in your baseline.';
            } else {
                $attributes = $model1->attributes;
                unset($attributes['control_library_id']);
                unset($attributes['jobrole']);
                // replace Company X to $company_name when we copy controls from the control library to their baseline. That would be the for rel_policy, detail, and guidance fields.
                if (!empty($company_name)) {
                    $attributes['rel_policy'] = str_replace('Company X',$company_name,$model1->rel_policy);
                    $attributes['detail'] = str_replace('Company X',$company_name,$model1->detail);
                    $attributes['guidance'] = str_replace('Company X',$company_name,$model1->guidance);
                }
                $model2 = new ControlBaseline;
                $model2->attributes = $attributes;
                $model2->baseline_id = $baseline_id;
                $model2->user_id = $user_id;
                $model2->company_id = $company_id;
                $model2->control_status_id = 1; // Planned
                
                // Mark who copied record
                $model2->updated_on = date("Y-m-d H:i:s");
                $model2->updated_user_id = $user_id;
                
                if ($model2->save()) {
                    $retVal = 0;//'The control was added to your baseline.';
                } else {
                    Yii::log(print_r($model2->getErrors(), true), 'error', 'ControlLibrary_Ex.copyToControlBaseline');
                }
            }
        } else {
            $retVal = 3;//'The control was not found in the Control Library.';
        }
        return $retVal;
    }

    public static function getViewValues($controlId)
    {
        $v = array();
        $row = self::getControl($count,$controlId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Control ID'] = $model->control_id;
            $v['Category'] = $model->category;
            $v['Title'] = $model->title;
            $v['Priority'] = $model->priority;
            $v['Score'] = $model->score;
            $v['Weighting'] = $model->weighting;
            $v['Related Job Role'] = $model->jobrole;
            $v['Description'] = $model->detail;
            $v['Sample Policy'] = $model->rel_policy;
            $v['Guidance'] = $model->guidance;
            $v['Example Evidence'] = $model->evidence;
            $v['Regulatory Reference'] = $model->reference;
            $v['Assessment Question'] = $model->question;
        }
        return $v;
    }

    public static function getNextControlId()
    {
        $retVal = null;
        $sql = "
          SELECT MAX(control_library_id) + 1000 next_control_id 
          FROM `control_library`
        ";

        $command = Yii::app()->db->createCommand($sql);
        $results = $command->query()->read();

        if($results){
            $retVal = $results['next_control_id'];
        }
        return $retVal;
    }

    public static function getControl(&$count,$controlId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null,$categoryid=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT cl.*, cc.control_category_id, cc.parent_id, cc.category, cc.description, cc.status as control_category_status";
        }
        $from = " FROM control_library cl"
                ." INNER JOIN control_category cc ON cl.cat_id=cc.control_category_id";
        $sqlWhere = " WHERE cl.status = ".Cons::ACTIVE;
        if ($controlId) {
            $sqlWhere .= " AND cl.control_id = :control_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        $catIds = ControlCategory_Ex::getChildNodeCatIds($categoryid);
        if (!empty($catIds)) {
            $catIds = implode(',',$catIds);
            $sqlWhere .= " AND cat_id IN ({$catIds})";
        }
        if ($pageSize > 0 && $controlId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            if ($controlId) {
                $cmdCount->bindParam(":control_id", $controlId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }

        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        if ($controlId) {
            $command->bindParam(":control_id", $controlId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($controlId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        //Yii::log("[".$count."] ".$sql,'error','sql');
        return $retVal;
    }

    public static function getControlsJson($pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null,$categoryid=null)
    {
        $select = "SELECT control_library_id, control_id, priority, title";
        $rows = ControlLibrary_Ex::getControl($count,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort,$categoryid);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['recid'] = Obscure::encode($row['control_library_id']);
                $row['control_id_enc'] = Obscure::encode($row['control_id']);
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }

}