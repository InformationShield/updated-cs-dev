<?php
/* @var $training Training */

class Training_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY doc_title asc, publish_date asc";

    public static function getViewValues($companyId,$trainingId)
    {
        $v = array();
        $row = self::getTraining($count,$companyId,$trainingId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Title'] = $model->doc_title;
            $v['Published Date'] = DateHelper::FormatDateTimeString($model->publish_date, 'm/d/Y');
            $v['Author'] = $model->author;
            $v['Approved'] = ($model->approved) ? 'Approved' : 'Un-Approved';
            $v['Status'] = $model->training_status_name;
            if ($model->training_type == 2) {
                $v['Training Type'] = 'Video';
            } else {
                $v['Training Type'] = 'Document';
                $v['File'] = $model->file_name;
            }
            $v['Description'] = $model->description;
        }
        return $v;
    }

    public static function saveFile($tmpFileFullPath,$training)
    {
        $mUploadedFile = new mUploadedFiles("files/training/",$training->company_id);
        return $mUploadedFile->saveFile($tmpFileFullPath,$training->doc_guid);
    }

    public static function removeFile($training)
    {
        $mUploadedFile = new mUploadedFiles("files/training/",$training->company_id);
        return $mUploadedFile->removeFile($training->doc_guid);
    }

    public static function downloadFile($training)
    {
        $mUploadedFile = new mUploadedFiles("files/training/",$training->company_id);
        return $mUploadedFile->downloadFile($training->doc_guid,$training->file_type,$training->file_name);
    }

    public static function getTmpFileUrl($training)
    {
        $mUploadedFile = new mUploadedFiles("files/training/",$training->company_id);
        return $mUploadedFile->getTmpFileUrl($training->doc_guid,$training->file_name);
    }

    public static function getTrainingCountsByStatus($companyId,$training_status)
    {
        $retVal = 0;
        $sql = "SELECT COUNT(t.training_id) total"
            ." FROM training t"
            ." WHERE t.training_status_id=:training_status_id AND t.status=".Cons::ACTIVE." AND company_id = :companyId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        $command->bindParam(":training_status_id", $training_status, PDO::PARAM_INT);
        $results = $command->query()->read();
        if (isset($results) && isset($results['total'])) {
            $retVal = $results['total'];
        }
        return $retVal;
    }

    public static function getTraining(&$count,$companyId,$trainingId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT t.*, ts.training_status_name";
        }
        $from = " FROM training t"
               ." INNER JOIN training_status ts ON t.training_status_id=ts.training_status_id";
        $sqlWhere = " WHERE t.company_id = :companyId AND t.status=".Cons::ACTIVE;
        if ($trainingId) {
            $sqlWhere .= " AND t.training_id = :training_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $trainingId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            $cmdCount->bindParam(":companyId", $companyId, PDO::PARAM_INT);
            if ($trainingId) {
                $cmdCount->bindParam(":training_id", $trainingId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        if ($trainingId) {
            $command->bindParam(":training_id", $trainingId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($trainingId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getTrainingJson($companyId,$pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT t.training_id, t.doc_title, t.publish_date, t.author, t.training_type";
        $rows = Training_Ex::getTraining($count,$companyId,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['training_id'] = Obscure::encode($row['training_id']);
                $row['publish_date'] = DateHelper::FormatDateTimeString($row['publish_date'], 'm/d/Y');
                $row['training_type'] = ($row['training_type']==2) ? 'Video' : 'Document';
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
}