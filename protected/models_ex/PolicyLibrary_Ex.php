<?php
/* @var $policyLibrary PolicyLibrary */
/* @var $policy Policy */

class PolicyLibrary_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY doc_title asc, created_on asc";

    public static function getViewValues($policyLibraryId)
    {
        $v = array();
        $row = PolicyLibrary_Ex::getPolicyLibrary($count,$policyLibraryId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Title'] = $model->doc_title;
            $v['Publish Date'] = DateHelper::FormatDateTimeString($model->publish_date, 'm/d/Y');
            $v['Review Date'] = DateHelper::FormatDateTimeString($model->expiry_date, 'm/d/Y');
            $v['Author'] = $model->author;
            $v['keywords'] = $model->keywords;
            //$v['Category'] = $model->category; //dave says not needed
			$v['Type'] = isset(mPolicy::$PolicyTypes[$model->policy_type]) ? mPolicy::$PolicyTypes[$model->policy_type] : '';
            $v['Approved'] = ($model->approved) ? 'Approved' : 'Un-Approved';
            $v['Status'] = $model->policy_status_name;
            $v['Filename'] = $model->file_name;
            $v['Description'] = $model->description;
            //$v['MIME Type'] = $model->file_type;
            //$v['File Size'] = Utils::FileSizeString($model->file_size);
        }
        return $v;
    }

    public static function copyFile($policyLibrary, $policy)
    {
        $mUploadedFileSrc = new mUploadedFiles("files/policylibrary/",null);
        $srcFileFullPath = $mUploadedFileSrc->getFilepath($policyLibrary->doc_guid);
        $mUploadedFileDest = new mUploadedFiles("files/policy/",$policy->company_id);
        return $mUploadedFileDest->copyFile($srcFileFullPath,$policy->doc_guid);
    }
    
    public static function saveFile($tmpFileFullPath,$policyLibrary)
    {
        $mUploadedFile = new mUploadedFiles("files/policylibrary/",null);
        return $mUploadedFile->saveFile($tmpFileFullPath,$policyLibrary->doc_guid);
    }

    public static function removeFile($policyLibrary)
    {
        $mUploadedFile = new mUploadedFiles("files/policylibrary/",null);
        return $mUploadedFile->removeFile($policyLibrary->doc_guid);
    }

    public static function downloadFile($policyLibrary)
    {
        $mUploadedFile = new mUploadedFiles("files/policylibrary/",null);
        return $mUploadedFile->downloadFile($policyLibrary->doc_guid,$policyLibrary->file_type,$policyLibrary->file_name);
    }

    public static function copyPolicy($policy_id,$company_id,$user_id,&$message)
    {
		/* @var $policy Policy */
        $status = 'error';
        $message = 'The policy library document was not found in the Policy Library.';
        $policyLibrary = PolicyLibrary::model()->findByAttributes(array('policy_library_id' => $policy_id));
		if ($policyLibrary->license_level <= getSessionVar('license_level')) {
			if (isset($policyLibrary)) {
				$policy = Policy::model()->findByAttributes(array('policy_library_id' => $policy_id, 'company_id' => $company_id));
				if (isset($policy)) {
					if ($policy->status == 0) { // deleted previously
						$policy->isNewRecord = false;
						$policy->status = 1;
						if ($policy->save()) {
							$status = 'success';
							$message = 'The policy library document was copied to your policies.';
						} else {
							$message = 'The policy library document failed to copy to your policies.';
						}
					} else {
						$message = 'The policy library document was not added since it already exists in your policies.';
					}
				} else {
					$policy = new Policy;
					$policy->attributes = $policyLibrary->attributes;
					// override values as needed
					$policy->doc_guid = Utils::create_guid();
					$policy->user_id = $user_id;
					$policy->policy_library_id = $policyLibrary->policy_library_id;
					$policy->company_id = $company_id;
					$policy->status = 1; // active
					$policy->approved = 0; // un-approved
					$policy->policy_status_id = 1; // draft
					$policy->created_on = date('Y-m-d H:i:s');
					$policy->publish_date = date('Y-m-d H:i:s');
					// copy over associated file to the companies' {root}/files/policy/{company_guid} folder
					if (PolicyLibrary_Ex::copyFile($policyLibrary, $policy)) {
						if ($policy->save()) {
							$status = 'success';
							$message = 'The policy library document was copied to your policies.';
						} else {
							$message = 'The policy library document failed to save to your policies.';
							Yii::log(print_r($policy->getErrors(), true), 'error', 'PolicylibraryController.actionCopy');
						}
					} else {
						$message = 'The policy library document failed to copy to your policies.';
					}
				}
			}
		} else {
			$message = "Available in the paid version.";
		}
        return $status;
    }

    public static function getPolicyLibrary(&$count,$policyLibraryId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT p.*, ps.policy_status_name, cc.category";
        }
        $from = "
			FROM policy_library p
			INNER JOIN policy_status ps ON p.policy_status_id = ps.policy_status_id 
			INNER JOIN license_level ll ON ll.license_level_id = p.license_level
			LEFT JOIN control_category cc ON p.cat_id = cc.control_category_id
		";
        $sqlWhere = " WHERE p.status=1";
		//$sqlWhere .= " AND p.license_level <= ".getSessionVar('license_level');
        if (!Utils::isSuper()) { // only admins should see non-published policies
            $sqlWhere .= " AND p.policy_status_id=3";
        }
        if ($policyLibraryId) {
            $sqlWhere .= " AND p.policy_library_id = :policy_library_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $policyLibraryId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            if ($policyLibraryId) {
                $cmdCount->bindParam(":policy_library_id", $policyLibraryId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        if ($policyLibraryId) {
            $command->bindParam(":policy_library_id", $policyLibraryId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($policyLibraryId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getPolicyLibraryJson($pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT p.policy_library_id, p.doc_title, p.publish_date, p.expiry_date, ps.policy_status_name, p.author, p.policy_type, p.license_level, ll.license_level_name";
        $rows = PolicyLibrary_Ex::getPolicyLibrary($count,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['policy_library_id'] = Obscure::encode($row['policy_library_id']);
                $row['publish_date'] = DateHelper::FormatDateTimeString($row['publish_date'], 'm/d/Y');
				$row['policy_type'] = isset(mPolicy::$PolicyTypes[$row['policy_type']]) ? mPolicy::$PolicyTypes[$row['policy_type']] : '';
			}
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
}