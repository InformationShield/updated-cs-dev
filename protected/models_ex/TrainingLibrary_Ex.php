<?php
/* @var $trainingLibrary TrainingLibrary */
/* @var $training Training */

class TrainingLibrary_Ex extends BaseSearchSort_Ex
{
    protected static $orderBy = " ORDER BY doc_title asc, publish_date asc";

    public static function getViewValues($trainingLibraryId)
    {
        $v = array();
        $row = TrainingLibrary_Ex::getTrainingLibrary($count,$trainingLibraryId);
        if (!empty($row)) {
            $model = (object) $row;
            $v['Title'] = $model->doc_title;
            $v['Published Date'] = DateHelper::FormatDateTimeString($model->publish_date, 'm/d/Y');
            $v['Author'] = $model->author;
            $v['Approved'] = ($model->approved) ? 'Approved' : 'Un-Approved';
            $v['Status'] = $model->training_status_name;
            if ($model->training_type == 2) {
                $v['Training Type'] = 'Video';
            } else {
                $v['Training Type'] = 'Document';
                $v['File'] = $model->file_name;
            }
            $v['Description'] = $model->description;
        }
        return $v;
    }

    public static function copyFile($trainingLibrary, $training)
    {
        $mUploadedFileSrc = new mUploadedFiles("files/traininglibrary/",null);
        $srcFileFullPath = $mUploadedFileSrc->getFilepath($trainingLibrary->doc_guid);
        $mUploadedFileDest = new mUploadedFiles("files/training/",$training->company_id);
        return $mUploadedFileDest->copyFile($srcFileFullPath,$training->doc_guid);
    }
    
    public static function saveFile($tmpFileFullPath,$trainingLibrary)
    {
        $mUploadedFile = new mUploadedFiles("files/traininglibrary/",null);
        return $mUploadedFile->saveFile($tmpFileFullPath,$trainingLibrary->doc_guid);
    }

    public static function removeFile($trainingLibrary)
    {
        $mUploadedFile = new mUploadedFiles("files/traininglibrary/",null);
        return $mUploadedFile->removeFile($trainingLibrary->doc_guid);
    }

    public static function downloadFile($trainingLibrary)
    {
        $mUploadedFile = new mUploadedFiles("files/traininglibrary/",null);
        return $mUploadedFile->downloadFile($trainingLibrary->doc_guid,$trainingLibrary->file_type,$trainingLibrary->file_name);
    }

    public static function copyVideoTraining($training_library_id,$training_id,$company_id,$user_id)
    {
        $copySuccess = true;
        $criteria = new CDbCriteria(array('order' => 'sort_order asc, video_training_id asc'));
        $videos = VideoTraining::model()->findAllByAttributes(array('training_library_id' => $training_library_id, 'status' => 1), $criteria);
        foreach($videos as $video) {
            $newVideo = new VideoTraining();
            $attrs = $video->attributes;
            unset($attrs['video_training_id']);
            $newVideo->attributes = $attrs;
            $newVideo->company_id = $company_id;
            $newVideo->user_id = $user_id;
            $newVideo->training_library_id = null;
            $newVideo->training_id = $training_id;
            if (!$newVideo->save()) {
                $copySuccess = false;
            }
        }
        return $copySuccess;
    }

    public static function copyTrainingLibrary($training_library_id,$company_id,$user_id)
    {
        $errMsg = false;
        $trainingLibrary = TrainingLibrary::model()->findByAttributes(array('training_library_id'=>$training_library_id));
        if (isset($trainingLibrary)) {
            $training = Training::model()->findByAttributes(array('training_library_id' => $training_library_id, 'doc_title' => $trainingLibrary->doc_title, 'company_id' => $company_id, 'status'=>1));
            if (isset($training)) {
                 $errMsg = 'The training library document was not added since it already exists in your training.';
            } else {
                $training = new Training;
                $training->attributes = $trainingLibrary->attributes;
                // override values as needed
                $training->doc_guid = Utils::create_guid();
                $training->user_id = $user_id;
                $training->training_library_id = $trainingLibrary->training_library_id;
                $training->company_id = $company_id;
                $training->status = 1; // active
                $training->approved = 0; // un-approved
                $training->training_status_id = 1; // draft
                $training->created_on = date('Y-m-d H:i:s');
                $training->publish_date = date('Y-m-d H:i:s');

                if ($training->save()) {
                    if ($trainingLibrary->training_type == 1) {
                        // copy over associated file to the companies' {root}/files/training/{company_guid} folder
                        $copySuccess = TrainingLibrary_Ex::copyFile($trainingLibrary, $training);
                    } else if ($trainingLibrary->training_type == 2) {
                        $copySuccess = TrainingLibrary_Ex::copyVideoTraining($training_library_id,$training->primaryKey,$company_id,$user_id);
                    }
                    if (!$copySuccess) {
                        if ($trainingLibrary->training_type == 1) {
                            $errMsg = 'The training library document failed to copy to your training.';
                        } else if ($trainingLibrary->training_type == 2) {
                            $errMsg = 'The training library videos failed to copy to your training.';
                        } else {
                            $errMsg = 'The training library record failed to copy to your training.';
                        }
                    }
                } else {
                    Yii::log(print_r($training->getErrors(), true), 'error', 'TraininglibraryController.actionCopy');
                    $errMsg = 'The training library document failed to save to your training.';
                }
            }
        } else {
            $errMsg = 'The training library document was not found in the Training Library.';
        }
        return $errMsg;
    }

    public static function getTrainingLibrary(&$count,$trainingLibraryId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT t.*, ts.training_status_name";
        }
        $from = " FROM training_library t"
               ." INNER JOIN training_status ts ON t.training_status_id=ts.training_status_id";
        $sqlWhere = " WHERE t.status=1";
        if ($trainingLibraryId) {
            $sqlWhere .= " AND t.training_library_id = :training_library_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0 && $trainingLibraryId==null) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            if ($trainingLibraryId) {
                $cmdCount->bindParam(":training_library_id", $trainingLibraryId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        if ($trainingLibraryId) {
            $command->bindParam(":training_library_id", $trainingLibraryId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($trainingLibraryId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
            }
        }
        return $retVal;
    }

    public static function getTrainingLibraryJson($pageSize,$offsetBy,$search=null,$searchLogic=null,$sort=null)
    {
        $select = "SELECT t.training_library_id, t.doc_title, t.publish_date, t.author, t.training_type";
        $rows = TrainingLibrary_Ex::getTrainingLibrary($count,null,$pageSize,$offsetBy,$select,$search,$searchLogic,$sort);
        if (!empty($rows)) {
            foreach ($rows as &$row) {
                $row['training_library_id'] = Obscure::encode($row['training_library_id']);
                $row['publish_date'] = DateHelper::FormatDateTimeString($row['publish_date'], 'm/d/Y');
                $row['training_type'] = ($row['training_type']==2) ? 'Video' : 'Document';
            }
        }
        $array = array("status"=>"success", "total"=>$count, "records" => ($count==0) ? array() : $rows );
        $json = json_encode($array);
        return $json;
    }
}