<?php

class Wizard_Ex 
{
    private static $questions;
    
    // Prepare data for Inherent Risk Score based on user wizard answers
    // var $attributes is an array based on CompanyInfo $model->attributes
    public static function calculateWizardInherentRisk($attributes = null)
    {
        $total_score = 0;
        $max_score = 0;
        Wizard_Ex::initializeScoring();
        $o = array();
        $s = array();
        
        if(!empty($attributes) && is_array($attributes)){
            foreach ($attributes as $k => $v) {
                if(isset(self::$questions[$k]) && !self::$questions[$k]['disabled']){
                    // Answers
                    $question_max_score = self::calculateQuestionMaxScore($k);
                    $question_score = self::calculateQuestionScore($k, $v);

                    $max_score += $question_max_score;
                    $total_score += $question_score;

                    $o[] = array(
                        'question' => $k, 
                        'score' => $question_score, 
                        'max_score' => $question_max_score,
                        'step' => self::$questions[$k]['step']
                    );
                    
                    // Steps
                    if(!empty($s[self::$questions[$k]['step']])) {
                        $s[self::$questions[$k]['step']] = array(
                            'step_name' => self::$questions[$k]['step_name'],
                            'score' => $s[self::$questions[$k]['step']]['score'] + $question_score,
                            'max_score' => $s[self::$questions[$k]['step']]['max_score'] + $question_max_score,
                        );
                    } else {
                        $s[self::$questions[$k]['step']] = array(
                            'step_name' => self::$questions[$k]['step_name'],
                            'score' => $question_score,
                            'max_score' => $question_max_score,
                        );
                    }
                    
                }
            }

            // Save the calculated Inherent Risk Score
			$companyInfo = CompanyInfo::model()->findByPk(array($attributes['company_info_id']));
			if ($companyInfo) {
				$companyInfo->isNewRecord = false;
				$companyInfo->inherent_risk_score = $total_score;
				$companyInfo->save();
			}
		} else {
            foreach (self::$questions as $k=>$v) {
                if(!$v['disabled']){
                    $question_max_score = self::calculateQuestionMaxScore($k);
                    $max_score += $question_max_score;
                    // Answers
                    $o[] = array(
                        'question' => $k,
                        'score' => 0,
                        'max_score' => $question_max_score,
                        'step' => $v['step']
                    );
                    // Steps
                    if(!empty($s[$v['step']])) {
                        $s[$v['step']] = array(
                            'step_name' => $v['step_name'],
                            'score' => $s[$v['step']]['score'] + 0,
                            'max_score' => $s[$v['step']]['max_score'] + $question_max_score,
                        );
                    } else {
                        $s[$v['step']] = array(
                            'step_name' => $v['step_name'],
                            'score' => 0,
                            'max_score' => $question_max_score,
                        );
                    }
                }
            }
        }

		$inherentRiskScoreComparison = self::getInherentRiskScoreComparison($total_score);
        return array('total_score'=>$total_score, 'max_score'=>$max_score, 'steps'=>$s, 'answers'=>$o,
			'inherent_risk_score_comparison'=>$inherentRiskScoreComparison);
    }
    
    // Calculate maximum possible question score based on answers scoring
    private static function calculateQuestionMaxScore($question = null)
    {
        $max_score = 0;
        
        if(!is_null($question) && isset(self::$questions[$question])){
            $question = self::$questions[$question];
            $answers = $question['answers'];
            $weight = $question['weight'];
            
            // sort highest score first
            foreach ($answers as $k => $v) {
                $score[$k] = $v['score'];
            }
            
            array_multisort($score, SORT_DESC, $answers);
            $max_score = array_values($answers)[0]['score'] * $weight;
        }
        return $max_score;
    }
    
    // Calculate question score based on user answer and qestion weight
    private static function calculateQuestionScore($question = null, $answer = null)
    {
        $score = 0;
        
        if(!is_null($question) && !is_null($answer) && trim($answer)!=='' && isset(self::$questions[$question])){
            $question = self::$questions[$question];
            $answers = $question['answers'];
            $weight = $question['weight'];
            if (isset($answers[$answer])) {
                $score = $answers[$answer]['score'] * $weight;
            }
        }
        return $score;
    }
    
    // Initiate wizard questions, assign question weight, decide wether question is disabled or enabled for Inherent Risk Score, set answers score
    private static function initializeScoring()
    {
        /* STEP 1 */
        // 1. Approximately how many business locations do you have?
        $o['num_business_locations'] = array('weight' => 1, 'disabled' => false, 'step' => 1, 'step_name' => 'Business Profile', 'answers' => array(
            '0' => array('score' => 0), // None (Virtual)
            '1' => array('score' => 25), // Single
            '2-10' => array('score' => 50), // Small (2-10 locations)
            '10-100' => array('score' => 75), // Medium (10 - 100 locations)
            '100+' => array('score' => 100), // Large (100+ locations)
        ));
        
        /* STEP 3 */
        // 1. How many different types of data do you collect, store or process from customers that they would consider highly sensitive?  If yes, how many types of data?
        $o['highly_sensitive_data'] = array('weight' => 1, 'disabled' => false, 'step' => 3, 'step_name' => 'Business Date Profile', 'answers' => array(
            0 => array('score' => 0), // None - We do not process sensitive data
            1 => array('score' => 25), // Single - Single Data Type (No PII – Personally Identifiable Information)
            2 => array('score' => 50), // Single PII - Single Type of PII  (One primary PII)
            3 => array('score' => 75), // Some - Multiple Data Types (Still No PII)
            4 => array('score' => 100), // All - Multiple Types (both PII and non PII)
        ));
        
        // 2. Approximately how many records does your organization handle on individual consumers that would be considered “personally identifiable”?  (For example, social security numbers, Driver’s License numbers, bank account numbers or other financial information that is not credit card data.)
        $o['individual_consumer_data'] = array('weight' => 1, 'disabled' => false, 'step' => 3, 'step_name' => 'Business Date Profile', 'answers' => array(
            0 => array('score' => 0), // None
            1 => array('score' => 25), // Less than 500
            2 => array('score' => 50), // 500 to 5000
            3 => array('score' => 75), // 5000 to 50,000
            4 => array('score' => 100), // Greater than 50,000
        ));
        
        // 3. Do you accept credit card payments or otherwise process/store credit card data (PCI-DSS)?
        $o['credit_card_data'] = array('weight' => 1, 'disabled' => true, 'step' => 3, 'step_name' => 'Business Date Profile', 'answers' => array(
            0 => array('score' => 0), // No
            1 => array('score' => 1), // Tes
            2 => array('score' => 2), // Not Sure
        ));
        
        // 4. Do you process or store electronic or paper health records (ePHI) about individuals?
        $o['individual_health_data'] = array('weight' => 1, 'disabled' => true, 'step' => 3, 'step_name' => 'Business Date Profile', 'answers' => array(
            0 => array('score' => 0), // No
            1 => array('score' => 1), // Tes
            2 => array('score' => 2), // Not Sure
        ));
        
        /* STEP 4 */
        // 1. How many employees and regular contractors have sensitive customer data on mobile devices (including laptops, tablets, smartphones)?
        $o['employee_mobile_customer_data'] = array('weight' => 1, 'disabled' => false, 'step' => 4, 'step_name' => 'User Access to Information', 'answers' => array(
            0 => array('score' => 0), // None
            1 => array('score' => 25), // Limited (< 5% of workforce)
            2 => array('score' => 50), // Some (5 - 25% of workforce)
            3 => array('score' => 75), // Moderate (25 - 50% of workforce)
            4 => array('score' => 100), // Most ( > 50% of workforce)
        ));
        
        // 2. About how many employees have remote access to your networks?
        $o['remote_network_access'] = array('weight' => 1, 'disabled' => false, 'step' => 4, 'step_name' => 'User Access to Information', 'answers' => array(
            0 => array('score' => 0), // None
            1 => array('score' => 25), // Limited (< 5% of workforce)
            2 => array('score' => 50), // Some (5 - 25% of workforce)
            3 => array('score' => 75), // Moderate (25 - 50% of workforce)
            4 => array('score' => 100), // Most ( > 50% of workforce)
        ));
        
        // 3. About how many third parties (suppliers, service providers)? do you use to store or process customer information?
        $o['third_party_data'] = array('weight' => 1, 'disabled' => false, 'step' => 4, 'step_name' => 'User Access to Information', 'answers' => array(
            0 => array('score' => 0), // None
            1 => array('score' => 25), // Single Primary 
            2 => array('score' => 50), // Minimal (2 - 25)
            3 => array('score' => 75), // Moderate (26 - 100)
            4 => array('score' => 100), // High (> 100)
        ));
        
        /* STEP 5 */
        // 1. How many different systems store or process sensitive information in your entire organization?
        $o['local_it_datacenters'] = array('weight' => 1, 'disabled' => false, 'step' => 5, 'step_name' => 'Information Technology Environment', 'answers' => array(
            0 => array('score' => 0), // None
            1 => array('score' => 25), // Single System
            2 => array('score' => 50), // Minimal (2-10)
            3 => array('score' => 75), // Moderate (20-100)
            4 => array('score' => 100), // High (100-200)
        ));
        
        // 2. How many internally developed applications that process sensitive information are within your organization?
        $o['internally_developed_applications'] = array('weight' => 1, 'disabled' => false, 'step' => 5, 'step_name' => 'Information Technology Environment', 'answers' => array(
            0 => array('score' => 0), // None
            1 => array('score' => 25), // Single Primary
            2 => array('score' => 50), // Minimal (2-15)
            3 => array('score' => 75), // Moderate (16-50)
            4 => array('score' => 100), // High (50+)
        ));
        
        // 3. How many wireless networks are in your organization? (Do not count home networks)
        $o['wireless_networks'] = array('weight' => 1, 'disabled' => false, 'step' => 5, 'step_name' => 'Information Technology Environment', 'answers' => array(
            0 => array('score' => 0), // None
            1 => array('score' => 25), // Single Network, Limited Access Points
            2 => array('score' => 50), // Single Network (More than 2 access Points)
            3 => array('score' => 75), // Multiple (More than 5 access Points)
            4 => array('score' => 100), // Multiple (More than 20 access points)
        ));
        
        // 4. How critical is your public facing web site for processes customer transactions?
        $o['online_customer_transactions'] = array('weight' => 1, 'disabled' => false, 'step' => 5, 'step_name' => 'Information Technology Environment', 'answers' => array(
            0 => array('score' => 0), // Not Critical (Our site is information only)
            1 => array('score' => 25), // Minimal (We provide only basic services)
            2 => array('score' => 50), // Moderate (Less than 20% of our sales or leads are from our website)
            3 => array('score' => 75), // Important (More than 50% of our sales or leads are generated through our website)
            4 => array('score' => 100), // Critical (It is the sole delivery channel for our business)
        ));
        
        self::$questions = $o;
    }

	private static function getInherentRiskScoreComparison($companyScore)
	{
		$inherentRiskScoreComparison[] = array(
			'label' => 'Your Score',
			'value' => $companyScore,
			'displayValue' => $companyScore,
			'color' => Utils::inherentRiskScoreColor($companyScore / 10),
			'labelFontColor'=> '000000'
		);

		$inherentRiskScoreAvg = self::getInherentRiskScoreAvg();
		$inherentRiskScoreComparison[] = array(
			'label' => 'Industry Average',
			'value' => $inherentRiskScoreAvg,
			'displayValue' => $inherentRiskScoreAvg,
			'color' => Utils::inherentRiskScoreColor($inherentRiskScoreAvg / 10),
			'labelFontColor'=> '000000'
		);
		return $inherentRiskScoreComparison;
	}

    private static function getInherentRiskScoreAvg() {
		$retVal = 0;
		$sql = "
			SELECT ROUND(SUM(inherent_risk_score) / COUNT(company_info_id)) InherentRiskScoreAvg
			FROM company_info
			WHERE inherent_risk_score > 0

		";
		$command = Yii::app()->db->createCommand($sql);
		$results = $command->query()->read();
		if ($results) {
			$retVal = $results['InherentRiskScoreAvg'];
		}
		return $retVal;
	}
}