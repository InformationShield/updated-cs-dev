<? /* @var $gmAjaxListGrid gmAjaxListGrid */ ?>
<style type="text/css">
    <? if (!$gmAjaxListGrid->showSearch) { ?>
        #tb_grid_toolbar_item_w2ui-search-advanced { display: none; }
        .w2ui-icon.icon-search-down.w2ui-search-down { display: none; }
    <? } ?>
    <? // this hide the Server Response time value ?>
    .w2ui-footer-left { display: none; }
</style>
<? if (!empty($gmAjaxListGrid->title)) { ?>
	<?= $gmAjaxListGrid->title; ?>
<? } ?>
<?= $gmAjaxListGrid->intro; ?>
<div id="maintreegrid<?= $gmAjaxListGrid->gridNum;?>" style="width: 100%; height: <?= ($gmAjaxListGrid->fixedHeight) ? $gmAjaxListGrid->fixedHeight.'px' : '100%'; ?>;"></div>

<script type="text/javascript">
    delete localStorage.w2ui;
    <? if ($gmAjaxListGrid->showExportCSV) { ?>
        function downloadCSV() {
            var grid = w2ui.grid<?= $gmAjaxListGrid->gridNum;?>;
            $.ajax({
                method: 'POST',
                dataType: "json",
                url: '<?= $gmAjaxListGrid->listUrl.'/export/csv'; ?>',
                data: {
                    'search': grid.searchData,
                    'searchLogic': grid.last.logic,
                    'sort': grid.sortData
                },
                success: function(response){
                    if (response.status == 'success') {
                        new PNotify({
                            title: 'Downloading...',
                            text: response.message,
                            delay: 3500,
                            type: 'info'
                        });
                        window.location = response.href;
                    } else {
                        w2alert(response.message);
                    }
                }
            });
        }
    <? } ?>

    <? if ($gmAjaxListGrid->showExportWORD) { ?>
        function downloadWORD() {
            var grid = w2ui.grid<?= $gmAjaxListGrid->gridNum;?>;
            $.ajax({
                method: 'POST',
                dataType: "json",
                url: '<?= $gmAjaxListGrid->listUrl.'/export/word'; ?>',
                data: {
                    'search': grid.searchData,
                    'searchLogic': grid.last.logic,
                    'sort': grid.sortData
                },
                success: function(response){
                    if (response.status == 'success') {
                        new PNotify({
                            title: 'Downloading...',
                            text: response.message,
                            delay: 3500,
                            type: 'info'
                        });
                        window.location = response.href;
                    } else {
                        w2alert(response.message);
                    }
                }
            });
        }
    <? } ?>

    // widget configuration
    var config<?= $gmAjaxListGrid->gridNum;?> = {
        grid: {
            name: 'grid<?= $gmAjaxListGrid->gridNum;?>',
            url: '<?= ($gmAjaxListGrid->listUrl) ? $gmAjaxListGrid->listUrl : url($gmAjaxListGrid->controller.'/get'); ?>',
            recid: '<?= $gmAjaxListGrid->recid; ?>',
            limit: <?= $gmAjaxListGrid->limit; ?>,
            header: '<?= $gmAjaxListGrid->header; ?>',
            fixedBody: <?= ($gmAjaxListGrid->fixedBody) ? 'true' : 'false'; ?>,
            show: {
                header         : <?= (!empty($gmAjaxListGrid->header)) ? 'true' : 'false'; ?>,  // indicates if header is visible
                toolbar        : <?= ($gmAjaxListGrid->showToolbar) ? 'true' : 'false'; ?>,  // indicates if toolbar is visible
                footer         : <?= ($gmAjaxListGrid->showFooter) ? 'true' : 'false'; ?>,  // indicates if footer is visible
                columnHeaders  : <?= ($gmAjaxListGrid->columnHeaders) ? 'true' : 'false'; ?>,   // indicates if columns is visible
                lineNumbers    : false,  // indicates if line numbers column is visible
                expandColumn   : false,  // indicates if expand column is visible
                selectColumn   : false,  // indicates if select column is visible
                emptyRecords   : false,   // indicates if empty records are visible
                toolbarReload  : false,   // indicates if toolbar reload button is visible
                toolbarColumns : <?= ($gmAjaxListGrid->toolbarColumns) ? 'true' : 'false'; ?>,   // indicates if toolbar columns button is visible
                toolbarSearch  : <?= ($gmAjaxListGrid->showSearch) ? 'true' : 'false'; ?>,   // indicates if toolbar search is visible
                toolbarAdd     : <?= ($gmAjaxListGrid->allowAdd) ? 'true' : 'false'; ?>,   // indicates if toolbar add new button is visible
                toolbarEdit    : false,   // indicates if toolbar edit button is visible
                toolbarDelete  : false,   // indicates if toolbar delete button is visible
                toolbarSave    : false,   // indicates if toolbar save button is visible
                selectionBorder: false,   // display border arround selection (for selectType = 'cell')
                recordTitles   : false,   // indicates if to define titles for records
                skipRecords    : false    // indicates if skip records should be visible
            },
            recordHeight: <?= ($gmAjaxListGrid->recordHeight) ? (int) $gmAjaxListGrid->recordHeight : 24; ?>,
            <?php if (!empty($gmAjaxListGrid->customSearch)){ ?>
				searches: <?php echo json_encode($gmAjaxListGrid->customSearch); ?>,
            <?php } ?>

            <? if ($gmAjaxListGrid->showExportCSV) { ?>
                toolbar: {
                    items: [
                        { type: 'button', id: 'btnExportCSV', caption: 'Export to CSV' },
                        <?php if ($gmAjaxListGrid->showExportWORD) { ?>
                        	{ type: 'button', id: 'btnExportWORD', caption: 'Export to WORD' },
                        <?php } ?>
                    ],
                    onClick: function (target, data) {
                        if (target == 'btnExportCSV') {
                            downloadCSV();
                        }else if (target == 'btnExportWORD') {
                            downloadWORD();
                        }
                    }
                },
            <? } ?>
            <? if ($gmAjaxListGrid->showCopy) { ?>
				toolbar: {
					items: [
						{ type: 'button', id: 'btnCopy', caption: '<i class="fa fa-copy"></i> Copy Baseline' },
					],
				onClick: function (target, data) {
						if (target == "btnCopy") {
                            doCopy();
                        }
					}
				},
            <? } ?>
            <? if ($gmAjaxListGrid->allowAdd) { ?>
                onAdd: function(event) {
                    if (_ajaxListGridAddFunction<?= $gmAjaxListGrid->gridNum;?> && _ajaxListGridAddFunction<?= $gmAjaxListGrid->gridNum;?> !== false) {
                        _ajaxListGridAddFunction<?= $gmAjaxListGrid->gridNum;?>(event);
                    } else {
                        window.location = "<?= url($gmAjaxListGrid->controller.'/edit'); ?>";
                    }
                    return false;
                },
            <? } ?>
			<? if ($gmAjaxListGrid->stateSave) { ?>
				onStateSave: function(event) {
					$.ajax({
						method: 'POST',
						dataType: "json",
						url: '<?= url($gmAjaxListGrid->controller.'/SaveState'); ?>',
						data: {
							state: event.state
						},
						// success: function(response){
							// alert('success');
						// },
						error: function(xhr) {
							// var errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ': ' + (xhr.responseText);
							// logError(errorResult);
						}
					});
				},
				onStateRestore: function(event) {
					$.ajax({
						method: 'POST',
						dataType: "json",
						url: '<?= url($gmAjaxListGrid->controller.'/RestoreState'); ?>',
						data: {
						},
						success: function(response) {
							if (response.columns) {
								for(var i=0; i<response.columns.length; i++) {
                                	if (response.columns[i].field == "action" || response.columns[i].field == "actions") {
                                        w2ui['grid'].showColumn(response.columns[i].field);
                                    } else if (response.columns[i].hidden == "false") {
										w2ui['grid'].showColumn(response.columns[i].field);
                                    } else {
									    w2ui['grid'].hideColumn(response.columns[i].field);
									}
								}
								w2ui['grid'].refresh();
							}
						},
						error: function(xhr) {
							// var errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ': ' + (xhr.responseText);
							// logError(errorResult);
						}
					});
					return false;
				},
			<? } ?>
            columns: _ajaxListGridColumns<?= $gmAjaxListGrid->gridNum;?>
        }
    }

    <? if ($gmAjaxListGrid->allowCopy) { ?>
        var ajaxListGridCopy<?= $gmAjaxListGrid->gridNum;?> = function(recid) {
            w2ui.grid<?= $gmAjaxListGrid->gridNum;?>.select(recid);
            $.ajax({
                method: 'POST',
                dataType: "json",
                url: '<?= url($gmAjaxListGrid->controller.'/copy'); ?>',
                data: {id: recid},
                success: function(response){
                    if (response.status == 'success') {
                        <? if ($gmAjaxListGrid->reloadOnCopy) { ?>
                            w2ui['grid<?= $gmAjaxListGrid->gridNum;?>'].reload();
                            <? if ($gmAjaxListGrid->postReloadFunction) { ?>
                                <?= $gmAjaxListGrid->postReloadFunction; ?>
                            <? } ?>
                        <? } else { ?>
                            w2alert(response.message);
                        <? } ?>
                    } else {
                        w2alert(response.message);
                    }
                }
            });
            return false;
        };
    <? } ?>

    <? if ($gmAjaxListGrid->allowDelete) { ?>
        var ajaxListGridDelete<?= $gmAjaxListGrid->gridNum;?> = function(recid) {
            w2confirm('Are you sure you want to DELETE?', function(btn){
                if (btn=='Yes') {
                    $.ajax({
                        method: 'POST',
                        dataType: "json",
                        url: '<?= url($gmAjaxListGrid->controller.'/delete'); ?>',
                        data: {id: recid},
                        success: function (response) {
                            if (response.status == 'success') {
                                w2ui['grid<?= $gmAjaxListGrid->gridNum;?>'].reload();
                                <? if ($gmAjaxListGrid->postReloadFunction) { ?>
                                    <?= $gmAjaxListGrid->postReloadFunction; ?>
                                <? } ?>
                            } else {
                                if ((response.action && response.action.length > 0) &&
                                    (response.actionUrl && response.actionUrl.length > 0)) {
                                    w2confirm({
                                        msg         : response.message,
                                        title       : 'Delete Requires Further Action',
                                        width       : 450,
                                        height      : 250,
                                        yes_text    : 'OK',
                                        yes_callBack: null,
                                        no_text     : response.action,
                                        no_callBack : function(){
                                            window.location = response.actionUrl;
                                            return false;
                                        }
                                    });
                                } else {
                                    w2alert(response.message);
                                }
                            }
                        }
                    });
                }
            });
            return false;
        };
    <? } ?>

    $(function () {
        function _calcHeight() {
            var _t = $('#maintreegrid<?= $gmAjaxListGrid->gridNum;?>').offset();
            var _f = $('#footer').height();
            var _adj = (_t.top + _f + 60);
            var _h = $(window).height() - _adj;
            var _bh = $('body').height();
            _h = (_h < (_bh - _adj)) ? (_bh- _adj) : _h;
            return _h;
        }

        // initialization
        <? if ($gmAjaxListGrid->doInitialization) { ?>
            function _resizeGrid() {
                <? if ($gmAjaxListGrid->fixedHeight > 0) { ?>
                    $('#maintreegrid<?= $gmAjaxListGrid->gridNum;?>').css('height','<?= $gmAjaxListGrid->fixedHeight; ?>px');
                <? } else { ?>
                    $('#maintreegrid<?= $gmAjaxListGrid->gridNum;?>').css('height',_calcHeight());
                <? } ?>
            }
            _resizeGrid();
            $( window ).resize(function() {
                _resizeGrid();
            });
            var mygrid<?= $gmAjaxListGrid->gridNum;?> = $('#maintreegrid<?= $gmAjaxListGrid->gridNum;?>').w2grid(config<?= $gmAjaxListGrid->gridNum;?>.grid);
            <?php if (!empty($gmAjaxListGrid->searchData)){ ?>
            mygrid<?= $gmAjaxListGrid->gridNum;?>.search(<?php echo json_encode($gmAjaxListGrid->searchData); ?>, 'AND');   
            <?php } ?>
        <? } ?>
        // end initialization

        <? if ($gmAjaxListGrid->allowView) { ?>
            $('#maintreegrid<?= $gmAjaxListGrid->gridNum;?>').on('click','.lnkView',function(){
                var recid = $(this).data('recid');
                w2ui.grid<?= $gmAjaxListGrid->gridNum;?>.select(recid);
                var w = $('body').width() - <?= $gmAjaxListGrid->widthAdjustment; ?>;
                var h  = _calcHeight();
                h = (h > 800) ? 800 : h;
                var popup = w2popup.load({ url: '<?= url($gmAjaxListGrid->controller.'/view'); ?>/id/'+recid,
                    showMax: true,
                    width: w,     // width in px
                    height: h,     // height in px
                    title: 'View <?= $gmAjaxListGrid->title; ?>',
                    <? if ($gmAjaxListGrid->reloadOnView) { ?>
                        onOpen: function(){
                            w2ui['grid<?= $gmAjaxListGrid->gridNum;?>'].reload();
                            <? if ($gmAjaxListGrid->postReloadFunction) { ?>
                                <?= $gmAjaxListGrid->postReloadFunction; ?>
                            <? } ?>
                        },
                    <? } ?>
                    buttons : '<button class="btn" onclick="w2popup.close();">Close</button> '
                        <? if ($gmAjaxListGrid->allowEdit) { ?>
                            + '<button class="btn" onclick="w2popup.close(); window.location = \'<?= url($gmAjaxListGrid->controller.'/edit'); ?>/id/'+recid+'\';"><strong>Edit</strong></button>'
                        <? } ?>
                        <? if ($gmAjaxListGrid->allowDelete && $gmAjaxListGrid->showViewDelete) { ?>
                            + '<button class="btn" onclick="w2popup.close(); ajaxListGridDelete<?= $gmAjaxListGrid->gridNum;?>(\''+recid+'\');">Delete</button>'
                        <? } ?>
                });
                return false;
            });
        <? } ?>

        <? if ($gmAjaxListGrid->allowDelete) { ?>
            $('#maintreegrid<?= $gmAjaxListGrid->gridNum;?>').on('click','.lnkDelete',function(){
                var recid = $(this).data('recid');
                ajaxListGridDelete<?= $gmAjaxListGrid->gridNum;?>(recid);
                return false;
            });
        <? } ?>

        <? if ($gmAjaxListGrid->allowCopy) { ?>
            $('#maintreegrid<?= $gmAjaxListGrid->gridNum;?>').on('click','.lnkCopy',function(){
                var recid = $(this).data('recid');
                ajaxListGridCopy<?= $gmAjaxListGrid->gridNum;?>(recid);
                return false;
            });
        <? } ?>

        <? if ($gmAjaxListGrid->allowDownload) { ?>
            $('#maintreegrid<?= $gmAjaxListGrid->gridNum;?>').on('click','.lnkDownload',function(){
                var href = $(this).attr('href');
                var recid = $(this).data('recid');
                w2ui.grid<?= $gmAjaxListGrid->gridNum;?>.select(recid);
                if (_ajaxListGridDownloadHook<?= $gmAjaxListGrid->gridNum;?> && _ajaxListGridDownloadHook<?= $gmAjaxListGrid->gridNum;?> !== false) {
                    _ajaxListGridDownloadHook<?= $gmAjaxListGrid->gridNum;?>(this, recid, href);
                }
                new PNotify({
                    title: 'Downloading...',
                    text: 'Your file is downloading now.',
                    delay: 3500,
                    type: 'info'
                });
                window.location = href;
                return false;
            });
        <? } ?>
    });
</script>
