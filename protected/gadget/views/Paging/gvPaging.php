<? /* @var $gmPaging gmPaging */ ?>
<nav class="<?= $gmPaging->PagingControlClass; ?>">
    <ul class="pagination">
        <? // Render "Previous" button ?>
        <? if ($gmPaging->CurrentPage > 1) { ?>
            <li>
                <a href="#" data-page="<?= $gmPaging->CurrentPage-1; ?>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
        <? } else { ?>
            <li class="disabled">
                <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
        <? } ?>

        <? if ($gmPaging->TotalPages <= 7) { // for 7 pages or less, show page buttons for all the pages ?>
            <? for ($i=1; $i<=$gmPaging->TotalPages && $i<=7; $i++) { ?>
                <? if ($i == $gmPaging->CurrentPage) { ?>
                    <li class="active">
                        <a href="#">
                            <?= $i; ?> <span class="sr-only">(current)</span>
                        </a>
                    </li>
                <? } else { ?>
                    <li>
                        <a href="#" data-page="<?= $i; ?>">
                            <span aria-hidden="true"><?= $i; ?></span>
                        </a>
                    </li>
                <? } ?>
            <? } ?>
        <? } else { ?>
            <? // Render "1"/firstpage button ?>
            <? if ($gmPaging->CurrentPage == 1) { ?>
                <li class="active">
                    <a href="#">
                        1 <span class="sr-only">(current)</span>
                    </a>
                </li>
            <? } else { ?>
                <li>
                    <a href="#" data-page="<?= '1'; ?>">
                        <span aria-hidden="true">1</span>
                    </a>
                </li>
            <? } ?>
            <?
                $startPage = max(2, $gmPaging->CurrentPage - 2);
                $endingPage = $startPage + 4;
            ?>
            <? if ($startPage > 2) { ?>
                <li class="disabled">
                    <a href="#">
                        <span aria-hidden="true">&hellip;</span>
                    </a>
                </li>
            <? } ?>

            <? // Render all other numbered/page buttons ?>
            <? for ($i=$startPage; $i<=$gmPaging->TotalPages && $i<=$endingPage; $i++) { ?>
                <? if ($i == $gmPaging->CurrentPage) { ?>
                    <li class="active">
                        <a href="#">
                            <?= $i; ?> <span class="sr-only">(current)</span>
                        </a>
                    </li>
                <? } else { ?>
                    <li>
                        <a href="#" data-page="<?= $i; ?>">
                            <span aria-hidden="true"><?= $i; ?></span>
                        </a>
                    </li>
                <? } ?>
            <? } ?>

            <? // Render last numbered/page button ?>
            <? if ($gmPaging->TotalPages == $i) {  // just one more extra page ?>
                <li>
                    <a href="#" data-page="<?= $i; ?>">
                        <span aria-hidden="true"><?= $i; ?></span>
                    </a>
                </li>
            <? } else if ($gmPaging->TotalPages > $i) { // at least 2 more pages left ?>
                <li class="disabled">
                    <a href="#">
                        <span aria-hidden="true">&hellip;</span>
                    </a>
                </li>
                <li>
                    <a href="#" data-page="<?= $gmPaging->TotalPages; ?>">
                        <span aria-hidden="true"><?= $gmPaging->TotalPages; ?></span>
                    </a>
                </li>
            <? } ?>
        <? } ?>

        <? // Render "Next" button ?>
        <? if ($gmPaging->CurrentPage < $gmPaging->TotalPages) { ?>
            <li>
                <a href="#" data-page="<?= $gmPaging->CurrentPage+1; ?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        <? } else { ?>
            <li class="disabled">
                <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        <? } ?>
    </ul>
</nav>

<script type="text/javascript">
$(document).ready(function() {
    // No need to show paging controls if there is only one page
    if (<?= $gmPaging->TotalPages; ?> > 1) {
        $('.<?= $gmPaging->PagingControlClass; ?>').show();
    } else {
        $('.<?= $gmPaging->PagingControlClass; ?>').hide();
    }

    // Handle page clicks
    $('.<?= $gmPaging->PagingControlClass; ?> li').click(function() {
        var page = $(this).children('a').data('page');
        var pagingUrl = '<?= $gmPaging->PagingUrl; ?>'+'/p/'+page;
        if (typeof page != "undefined") {
            <? if ($gmPaging->PagingJsCallback) { ?>
                <?= $gmPaging->PagingJsCallback; ?>(pagingUrl);
            <? } else if (!$gmPaging->PagingResultTarget) { ?>
                window.location.href = pagingUrl;
            <? } else { ?>
                showLoader();
                $('.<?= $gmPaging->PagingControlClass; ?> li').unbind();
                $('<?= $gmPaging->PagingResultTarget; ?>').load(pagingUrl, function(){
                    hideLoader();
                });
            <? } ?>

            <? if ($gmPaging->TopOfPageId) { ?>
                if ($("<?= $gmPaging->TopOfPageId; ?>").length) {
                    $('html,body').animate({scrollTop: $("<?= $gmPaging->TopOfPageId; ?>").offset().top},100);
                }
            <? } ?>
        }
        return false;
    });

});
</script>
