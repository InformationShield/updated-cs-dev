<? /* @var $gmAjaxListView gmAjaxListView */ ?>
<div>
    <div class="container-fluid" style="margin: 20px 0 20px 0; ">
        <? foreach($gmAjaxListView->namedPairs as $key=>$value) { ?>
            <div class="row">
                <div class="col-md-3">
                    <strong><?= CHtml::encode($key); ?>:</strong>
                </div>
                <div class="col-md-9">
                    <? if (in_array($key,$gmAjaxListView->skipEncode)) { ?>
                        <?= $value; ?>
                    <? } else { ?>
                        <?= CHtml::encode($value); ?>
                    <? } ?>
                </div>
            </div>
        <? } ?>
    </div>
</div>