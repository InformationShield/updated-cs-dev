<? /* restyle for each project based on its look & feel */ ?>
<style>
    .flashMsg {
        font-size: 16px;
        cursor: pointer;

        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        padding: 12px;
        margin: 0 1px 2px 1px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.35), 0 1px 8px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.35), 0 1px 8px rgba(0, 0, 0, 0.15);
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.35), 0 1px 8px rgba(0, 0, 0, 0.15);
        position: relative;
    }
    .flash-notice.flashMsg {
        background-color: #e8e8e8;
        background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #FFFFE0), color-stop(100%, #FFFFA0));
        background-image: -webkit-linear-gradient(#FFFFE0, #FFFFA0);
        background-image: -moz-linear-gradient(#FFFFE0, #FFFFA0);
        background-image: linear-gradient(#FFFFE0, #FFFFA0);
        border: 1px solid #FFFFA0;
        border-color: #FFFFA0 #FFFFE0;
    }
    .flash-error.flashMsg {
        color: #fff;
        background-color: #c6211a;
        background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #e4524e), color-stop(100%, #c6211a));
        background-image: -webkit-linear-gradient(#e4524e, #c6211a);
        background-image: -moz-linear-gradient(#e4524e, #c6211a);
        background-image: linear-gradient(#e4524e, #c6211a);
        border: 1px solid #880700;
        border-color: #880700 #C6211A;
    }
    .flashMsg ul {
        list-style: none;
    }
</style>

<? if (user()->hasFlash($gmFlashError->flashKey)) { ?>
    <? $msg = user()->getFlash($gmFlashError->flashKey); ?>
    <? if (substr($msg, 0, 1) == "!") { // error flash message?>
        <? $msg = substr($msg, 1); ?>
        <div class="flashMsg flash-error">
            <div class="messageText">
                <?= $msg; ?>
            </div>
        </div>
    <? } else { ?>
        <div class="flashMsg flash-notice">
            <div class="messageText">
                <?= $msg; ?>
            </div>
        </div>
    <? } ?>
<? } else if ($gmFlashError->model && $gmFlashError->model->hasErrors()) { ?>
    <div class="flashMsg flash-error">
        <div class="messageText">
            <?= CHtml::errorSummary($gmFlashError->model, '', null, array()); ?>
        </div>
    </div>
<? } ?>

<script type="text/javascript">
$(document).ready(function()
{
    $(document).on('change', 'form :input', function() {
        $('.flashMsg').fadeOut('slow');
    });

    $(document).on('click', '.flashMsg', function() {
        $('.flashMsg').fadeOut(1000);
    });
});
</script>
