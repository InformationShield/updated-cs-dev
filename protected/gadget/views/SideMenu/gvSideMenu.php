<td class="side-menu-container">
    <div class="side-nav-main list-group">
        <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_HOME); ?>" href="<?= url('home/index'); ?>">Home</a>
        <? if (Utils::isInspector(false) || Utils::isAdmin()) { ?>
            <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_COMPANY); ?>" href="<?= url('company/index'); ?>">Companies</a>
            <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_CONTACT); ?>" href="<?= url('contact/index'); ?>">Contacts</a>
        <? } ?>
        <? if (Utils::isAdmin()) { ?>
            <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_SITES); ?>"
               href="<?= url('site/Site'); ?>">Sites</a>
                <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_SITES_SWP3); ?>"
                   href="<?= url('swp3/Swp3Site'); ?>"
                    >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;SWP3 Sites</a>
                <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_SITES_SPCC); ?>"
                   href="<?= url('spcc/SpccSite'); ?>"
                    >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;SPCC Sites</a>
                <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_SITES_ESA); ?>"
                   href="<?= url('esa/EsaSite'); ?>"
                    >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;ESA Sites</a>
        <? } ?>
        <? if (Utils::isManager()) { ?>
            <div class="list-group-item group <?= $gmSideMenu->menuSel(Cons::MENU_TRACKING); ?>">Tracking</div>
                <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_TRACKING_SWP3); ?>"
                   href="<?= url('swp3/Swp3Track'); ?>"
                    >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;SWP3 Tracking</a>
                <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_TRACKING_SPCC); ?>"
                   href="<?= url('spcc/SpccTrack'); ?>"
                    >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;SPCC Tracking</a>
                <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_TRACKING_ESA); ?>"
                   href="<?= url('esa/EsaTrack'); ?>"
                    >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;ESA Tracking</a>
        <? } ?>
        <? if (Utils::isManager()) { ?>
            <div class="list-group-item group <?= $gmSideMenu->menuSel(Cons::MENU_PROJECT); ?>">Projects</div>
                <? if (Utils::isBackOffice()) { ?>
                    <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_PROJECT_LIST); ?>"
                       href="<?= url('project/Project'); ?>"
                        >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;Project List</a>
                    <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_PROJECT_RATES); ?>"
                       href="<?= url('project/Rate'); ?>"
                        >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;Project Rates</a>
                <? } ?>
                <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_PROJECT_HOURS); ?>"
                   href="<?= url('project/Hours'); ?>"
                    >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;Project Hours</a>
                <? if (Utils::isBackOffice()) { ?>
                    <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_PROJECT_DETAILS); ?>"
                       href="<?= url('project/ProjectDetails'); ?>"
                        >&nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;Project Billing</a>
                <? } ?>
        <? } ?>
        <? if (Utils::isTrainee() && !Utils::isManager(false) && !Utils::isAdmin(false)) { // manager and Admin do not have access ?>
            <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_INSPECT); ?>" href="<?= url('inspect/inspect'); ?>"">Inspections</a>
        <? } ?>
        <? if (Utils::isInspector()) { ?>
            <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_HISTORY); ?>" href="<?= url('inspect/history'); ?>">Inspection History</a>
        <? } ?>
        <? if (Utils::isAdmin()) { ?>
            <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_INVOICEREQUESTS); ?>" href="<?= url('invoice/Invoice'); ?>">Invoice Requests</a>
            <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_BILLING); ?>" href="<?= url('billing/billing'); ?>">Billing</a>
        <? } ?>
        <? if (Utils::isBackOffice()) { ?>
            <a class="list-group-item <?= $gmSideMenu->menuSel(Cons::MENU_ADMINISTRATION); ?>" href="<?= url('usr/index'); ?>">User Administration</a>
        <? } ?>
    </div>
</td>

<script type="text/javascript">
$(document).ready(function() {

});
</script>
