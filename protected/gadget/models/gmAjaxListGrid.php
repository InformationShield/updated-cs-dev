<?php

class gmAjaxListGrid  extends CFormModel
{
    public $gridNum = '';           // to support more than 1 grid on a page you maust supply a short prefix
    public $doInitialization=true;  // perform grid and window resizing init, set to false to override
    public $title = '';             // title of grid
    public $header = '';            // some text if you want to show header
    public $intro = '';             // some text to display under the title
    public $listUrl = false;        // override the default of url($controller.'/get');
    public $controller = 'unknown'; // the controller name of the url path
    public $recid = 'unknown';      // the name of the id column for the grid
    public $allowView = true;       // set to false to disable lnkView handler or to override it with custom code for url($controller.'/view/id'.recid);
    public $showViewDelete = false;    // set to true if showing a Delete button on View is desired, must also have $allowDelete = true;
    public $reloadOnView = false;   // set to true if a success view action should reload grid to update records (for example, read/unread status)
    public $allowAdd = false;       // set to true to show + Add button and allow add features for url($controller.'/edit');
    public $allowEdit = false;      // set to true if lnkEdit support is desired for for url($controller.'/edit/id'.recid);
    public $allowDelete = false;    // set to true if lnkDelete support is desired for url($controller.'/delete/id/'.recid);
    public $allowDownload = false;  // set to true if lnkDownload support is desired
    public $allowCopy = false;      // set to true if lnkCopy support is desired for url($controller.'/copy/id/'.recid);
    public $reloadOnCopy = false;   // set to true if a success copy action should reload grid to get new records from copy
    public $showToolbar = true;     // set to true to show toolbar with search and add buttons
    public $showFooter = true;      // set to true to show footer with item count
    public $showSearch = true;      // set true to show Search input in toolbar
    public $customSearch = array(); // custom search attribute for w2ui plugin (Doc - http://w2ui.com/web/docs/w2grid.searches)
    public $searchData = array();   // search data attribute for w2ui plugin (Doc - http://w2ui.com/web/docs/w2grid.searchData?ver=1.2
    public $widthAdjustment = 60;
    public $limit = 1000;
    public $fixedBody = true;
    public $columnHeaders = true;
    public $fixedHeight = false;
    public $toolbarColumns = false;
    public $showExportCSV = false;
    public $showExportWORD = false;
    public $showCopy = false;
    public $recordHeight = false;
    public $postReloadFunction = false; // optional string of js code to call after a grid reload() function call, example: postReload();
	public $stateSave = false;
}
