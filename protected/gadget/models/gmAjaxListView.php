<?php

class gmAjaxListView  extends CFormModel
{    
    public $namedPairs = array();
    public $skipEncode = array();

    public function __construct($namedPairs,$skipEncode=null)
    {
        parent::__construct();
        if (empty($namedPairs)) {
            $namedPairs = array();
        }
        if (empty($skipEncode)) {
            $skipEncode = array();
        }
        $this->namedPairs = $namedPairs;
        $this->skipEncode = $skipEncode;
    }
}
