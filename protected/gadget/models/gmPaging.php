<?php

class gmPaging  extends CFormModel
{    
    public $PagingControlClass = "pcontrol"; // so you can have more than 1 paging control per page
    public $CurrentPage = 0;    // Current page to display
    public $TotalPages = 0;     // Total number of pages available
    public $PagingUrl = null; // When one of the paging button is pressed, it will call this url with the parameter 'p=<pagenumber>'
    public $PagingResultTarget = null; // div id where the resulting data from paging ajax call will be displayed.  Set to null to replace entire page.
    public $PagingJsCallback = null; // a javascript function to handle custom paging controls
    public $TopOfPageId = null;   // id (ex: #top_of_page) of a div or span to scroll up to after paging
}
