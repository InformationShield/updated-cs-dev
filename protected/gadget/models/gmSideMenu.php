<?php

class gmSideMenu extends CFormModel
{
	public $sideMenuItem = 0;

    function menuSel($menuItem) {
        if ($this->sideMenuItem == $menuItem) {
            return 'selected';
        } else {
            return '';
        }
    }
}