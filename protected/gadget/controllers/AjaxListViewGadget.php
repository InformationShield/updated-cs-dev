<?php

class AjaxListViewGadget extends Gadget
{
    public static function show($model, $viewParam=null)
    {
        Gadget::gadgetRender('AjaxListView/gvAjaxListView', $model, $viewParam, 'gmAjaxListView');
    }
}
