<?php

class SideMenuGadget extends Gadget
{
    public static function show($model, $viewParam=null)
    {
        Gadget::gadgetRender('SideMenu/gvSideMenu', $model, $viewParam, 'gmSideMenu');
    }

    public static function initialize($menuItem) {
        $gmSideMenu = new gmSideMenu();
        $gmSideMenu->sideMenuItem = $menuItem;
        self::show($gmSideMenu);
    }
}
