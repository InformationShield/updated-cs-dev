<?php

class FlashErrorGadget extends Gadget
{
    public static function show($model, $flashKey=null, $viewParam=null)
    {
        // don't even render unless there is a flash msg or error to show
        if (user()->hasFlash($flashKey) || ($model && $model->hasErrors())) {
            $gmFlashError = new gmFlashError();
            $gmFlashError->flashKey = $flashKey;
            $gmFlashError->model = $model;
            Gadget::gadgetRender('FlashError/gvFlashError', $gmFlashError, $viewParam, 'gmFlashError');
        }
    }
}
