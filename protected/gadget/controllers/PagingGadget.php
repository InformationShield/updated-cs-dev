<?php

class PagingGadget extends Gadget
{
    public static function show($model, $viewParam=null)
    {
        Gadget::gadgetRender('Paging/gvPaging', $model, $viewParam, 'gmPaging');
    }

    public static function initialize($totalPages, $currentPage, $pagingControlClass=null, $pagingUrl=null,
                                      $pagingResultTarget=null, $pagingJsCallback=null, $topOfPageId=null)
    {
        $gmPaging = new gmPaging();
        if (is_null($pagingControlClass)) {
            $pagingControlClass = mt_rand(5, 15);
        }
        $gmPaging->PagingControlClass = $pagingControlClass;
        $gmPaging->TotalPages = $totalPages;
        $gmPaging->CurrentPage = $currentPage;
        $gmPaging->PagingUrl = $pagingUrl;
        $gmPaging->PagingResultTarget = $pagingResultTarget;
        $gmPaging->PagingJsCallback = $pagingJsCallback;
        $gmPaging->TopOfPageId = $topOfPageId;
        self::show($gmPaging);
    }
}
