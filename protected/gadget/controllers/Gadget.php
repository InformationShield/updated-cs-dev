<?php

class Gadget extends CController
{
	public static function gadgetRender($view, $model, $viewConfig=null, $modelVariableName=null, $controllerCLI=null)
    {
        $output = self::gadgetOutput($view, $model, $viewConfig, $modelVariableName, $controllerCLI);
		if ($output !== false) {
			echo $output;
		}
    }
	
    public static function gadgetOutput($view, $model, $viewConfig=null, $modelVariableName=null, $controllerCLI=null)
    {
        try {
            if ($modelVariableName === null) {
                $modelVariableName = get_class($model);
            }
            $gadget = new Gadget('GadgetController');
            $gadget->layout = false;
            if (is_null($controllerCLI)) {
                $view = '//../gadget/views/'.$view;
                $output = $gadget->renderPartial($view, array($modelVariableName=>$model, 'viewConfig'=>$viewConfig), true);
            } else {
                $viewFileName = dirname(__FILE__).'/../views/'.$view.".php";
                $output = $controllerCLI->renderFile($viewFileName, array($modelVariableName=>$model, 'viewConfig'=>$viewConfig), true);
            }
        } catch(Exception $e) {
            Yii::log("Exception:".$e->__toString(),'error','Gadget.gadgetOutput');
            if (YII_DEBUG) {
                $output = $e->__toString();
            } else {
                $output = false;
            }
        }
        return $output;
    }	
}