<?php

class AjaxListGridGadget extends Gadget
{
    public static function show($model, $viewParam=null)
    {
        Gadget::gadgetRender('AjaxListGrid/gvAjaxListGrid', $model, $viewParam, 'gmAjaxListGrid');
    }
}
