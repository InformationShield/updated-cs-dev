<?php

class mAdminDownloadFile
{
    const AdminUserExport=1;
    const AdminEmployeeExport=2;
    
    public static function send($downloadType, $search = array())
    {
        // validate search options
        $search['pageSize'] = (isset($search['pageSize'])) ? $search['pageSize'] : 0;
        $search['offsetBy'] = (isset($search['offsetBy'])) ? $search['offsetBy'] : 0;
        $search['search'] = (isset($search['search'])) ? $search['search'] : null;
        $search['searchLogic'] = (isset($search['searchLogic'])) ? $search['searchLogic'] : null;
        $search['sort'] = (isset($search['sort'])) ? $search['sort'] : null;
        
        switch ($downloadType) {
            case mAdminDownloadFile::AdminUserExport:
                $data = AdminUser_Ex::createCSVReportData($search);
                if(!empty($data)){
                    mAdminDownloadFile::outputCSV($data, 'Users');
                    Yii::app()->end();
                }else{
                    $message = 'Unable to export selected users.';
                }
                break;
                
            case mAdminDownloadFile::AdminEmployeeExport:
                $data = Employee_Ex::adminCreateCSVReportData($search);
                if(!empty($data)){
                    mAdminDownloadFile::outputCSV($data, 'Employees');
                    Yii::app()->end();
                }else{
                    $message = 'Unable to export selected users.';
                }
                break;
            
            default:
                $message = "Unknown download file type.";
                break;
        }
        return $message;
    }

    public static function outputCSV($data,$filename='export')
    {
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $outputBuffer = fopen("php://output", 'w');
        foreach($data as $val) {
            fputcsv($outputBuffer, $val);
        }
        fclose($outputBuffer);
    }
    
    public static function outputWORD($data, $filename = 'export')
    {
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $filename . '.docx"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        echo $data;
    }

}