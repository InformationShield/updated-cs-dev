<?php

class User extends CActiveRecord
{
	 /**
	 * The followings are the available columns in table 'users':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $activkey
	 * @var integer $createtime
	 * @var integer $lastvisit
	 * @var integer $superuser
	 * @var integer $status
	 * @var integer $partner_id
         * @var timestamp $create_at
         * @var timestamp $lastvisit_at
         * @var integer $eula
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.CConsoleApplication
		return ((get_class(Yii::app())=='CConsoleApplication' || (get_class(Yii::app())!='CConsoleApplication' && Yii::app()->getModule('user')->isAdmin()))?array(
			array('username', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Username is too short (Enter at least 4 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 8,'message' => UserModule::t("Password is too short (Enter at least 8 characters).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
//			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_@]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('status', 'in', 'range'=>array(Cons::USER_STATUS_NOT_ACTIVE,Cons::USER_STATUS_ACTIVE,Cons::USER_STATUS_BANNED,Cons::USER_STATUS_DELETED)),
			array('superuser', 'in', 'range'=>array(0,1,2)),
            array('create_at', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
            array('lastvisit_at', 'default', 'value' => '0000-00-00 00:00:00', 'setOnEmpty' => true, 'on' => 'insert'),
			array('username, email, superuser, status', 'required'),
			array('superuser, status, partner_id, eula', 'numerical', 'integerOnly'=>true),
			array('id, username, password, email, activkey, create_at, lastvisit_at, superuser, status, partner_id, eula', 'safe', 'on'=>'search'),
		):((Yii::app()->user->id==$this->id)?array(
			array('username, email', 'required'),
			array('username', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Username is too short (Enter at least 4 characters).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
//			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_@]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
		):array()));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
            $relations = Yii::app()->getModule('user')->relations;
            $relations['messages'] = array(self::HAS_MANY, 'Message', 'message_id');
            if (!isset($relations['profile']))
                $relations['profile'] = array(self::HAS_ONE, 'Profile', 'user_id');
            return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => UserModule::t("Id"),
			'username'=>UserModule::t("username"),
			'password'=>UserModule::t("password"),
			'verifyPassword'=>UserModule::t("Retype Password"),
			'email'=>UserModule::t("E-mail"),
			'verifyCode'=>UserModule::t("Verification Code"),
			'activkey' => UserModule::t("activation key"),
			'createtime' => UserModule::t("Registration date"),
			'create_at' => UserModule::t("Registration date"),
			'lastvisit_at' => UserModule::t("Last visit"),
			'superuser' => UserModule::t("Superuser"),
			'status' => UserModule::t("Status"),
			'partner_id' => 'PartnerId',
                        'eula' => 'EULA',
		);
	}
	
	public function scopes()
        {
            return array(
                'active'=>array(
                    'condition'=>'status='.Cons::USER_STATUS_ACTIVE,
                ),
                'notactive'=>array(
                    'condition'=>'status='.Cons::USER_STATUS_NOT_ACTIVE,
                ),
                'banned'=>array(
                    'condition'=>'status='.Cons::USER_STATUS_BANNED,
                ),
                'deleted'=>array(
                        'condition'=>'status='.Cons::USER_STATUS_DELETED,
                ),
                'superuser'=>array(
                    'condition'=>'superuser>=1',
                ),
                'deity'=>array(
                    'condition'=>'superuser=2',
                ),
                'notsafe'=>array(
                    'select' => 'id, username, password, email, activkey, create_at, lastvisit_at, superuser, status, eula',
                ),
            );
        }
	
	public function defaultScope()
        {
            return CMap::mergeArray(Yii::app()->getModule('user')->defaultScope,array(
                'alias'=>'user',
                'select' => 'user.id, user.username, user.email, user.create_at, user.lastvisit_at, user.superuser, user.status, user.eula',
            ));
        }
	
	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				Cons::USER_STATUS_NOT_ACTIVE => UserModule::t('Not active'),
				Cons::USER_STATUS_ACTIVE => UserModule::t('Active'),
				Cons::USER_STATUS_BANNED => UserModule::t('Banned'),
				Cons::USER_STATUS_DELETED => UserModule::t('Deleted'),
			),
			'AdminStatus' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
                                //'2' => UserModule::t('Deity'),
			),
                        'ReadEULA' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
			),  
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
	
/**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        
        $criteria->compare('id',$this->id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('activkey',$this->activkey);
        $criteria->compare('create_at',$this->create_at);
        $criteria->compare('lastvisit_at',$this->lastvisit_at);
        $criteria->compare('superuser',$this->superuser);
        $criteria->compare('status',$this->status);
	$criteria->compare('partner_id',$this->partner_id);
        $criteria->compare('eula',$this->eula);

		// do not show deleted users by default
		if (empty($this->status)) {
			$criteria->addCondition("status!=" . Cons::USER_STATUS_DELETED);
		}

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
        	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('user')->user_page_size,
			),
        ));
    }

    public function getCreatetime() {
        return strtotime($this->create_at);
    }

    public function setCreatetime($value) {
        $this->create_at=date('Y-m-d H:i:s',$value);
    }

    public function getLastvisit() {
        return strtotime($this->lastvisit_at);
    }

    public function setLastvisit($value) {
        $this->lastvisit_at=date('Y-m-d H:i:s',$value);
    }
}