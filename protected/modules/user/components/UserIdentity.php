<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	const ERROR_EMAIL_INVALID	= 3;
	const ERROR_STATUS_NOTACTIV	= 4;
	const ERROR_STATUS_BAN		= 5;
	const ERROR_RESET_PASSWORD	= 6;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate($setupSessionVars = true)
	{
        $this->errorCode = self::ERROR_NONE;
		if (strpos($this->username,"@")) {
			$user=User::model()->notsafe()->findByAttributes(array('email'=>$this->username));
		} else {
			$user=User::model()->notsafe()->findByAttributes(array('username'=>$this->username));
		}

		// check if password has been removed to force a reset
		if (isset($user) && empty($user->password)) {
			$this->errorCode=self::ERROR_RESET_PASSWORD;
			return !$this->errorCode;
		}
		
		if($user===null) {
			if (strpos($this->username,"@")) {
				$this->errorCode=self::ERROR_EMAIL_INVALID;
			} else {
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			}
        } else if($this->password !== $user->password && Yii::app()->getModule('user')->encryptPassword($this->password)!==$user->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else if($user->status==0 && Yii::app()->getModule('user')->loginNotActiv==false)
			$this->errorCode=self::ERROR_STATUS_NOTACTIV;
		else if($user->status==-1)
			$this->errorCode=self::ERROR_STATUS_BAN;

        if ($this->errorCode == self::ERROR_NONE) {
			// if password matches clear text, update password with encrypted password
            if (strlen($this->password) < 16 && $this->password == $user->password) {
                $userUpdate = User::model()->findByPk($user->id);
                $userUpdate->password = Yii::app()->getModule('user')->encryptPassword($this->password);
                $userUpdate->isNewRecord = false;
                $userUpdate->save();
            }
			$this->_id=$user->id;
			$this->username=$user->username;
            if ($setupSessionVars) {
				$this->setSessionVars($user);
			}
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}
    
    /**
    * @return integer the ID of the user record
    */
	public function getId()
	{
		return $this->_id;
	}

	private function setSessionVars($user)
    {
        $this->setState('userId', $user->id);
        $this->setState('userLogin', $user->username);
        $this->setState('email', $user->email);
        if ($user->superuser == 1) {
            $this->setState('superuser', 1);
        } else {
			$this->setState('superuser', 0); // must do this to insure proper impersonation
		}
		$this->setState('site_type', 'CompanySite');	// by default take user to company home page
		// set company context for user
        mCompanyContext::setSessionVars($user->id);
    }

	public static function Impersonate($userId)
	{
		$ui = null;
		$user = User::model()->findByPk($userId);
		if ($user)
		{
			$ui = new UserIdentity($user->email, "");
			$ui->setState('superuser', 0);// must do this to insure proper impersonation
			$ui->_id=$user->id;
			$ui->username=$user->username;
			$ui->errorCode=self::ERROR_NONE;
			$ui->setSessionVars($user);
		}
		return $ui;
	}
}