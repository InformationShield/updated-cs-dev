<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	UserModule::t('Create'),
);
$this->menu=array(
    array('label'=>UserModule::t('Manage Users'), 'url'=>Yii::app()->createAbsoluteUrl('user/admin')),
    array('label'=>'List Users', 'url'=>Yii::app()->createAbsoluteUrl('user')),
);
//if(UserModule::isDeity()) {
//    array_push($this->menu,
//        array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin'))
//    );
//}
?>
<h1><?php echo UserModule::t("Create User"); ?></h1>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>