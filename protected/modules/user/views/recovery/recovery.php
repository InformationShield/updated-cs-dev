<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Restore");
$this->breadcrumbs=array(
    UserModule::t("Login") => array('/user/login'),
    UserModule::t("Restore"),
);
?>

    <h1>Password Reset</h1>

<?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
    <div class="success">
        <?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
    </div>
<?php else: ?>

    <? $reason = getGet('reason'); ?>
    <? if (isset($reason)) { ?>
        <p>Your password has been expired due to the account being inactive for past 30 days, please reset your password.</p>
    <? } ?>
    <div class="form">
        <?php echo CHtml::beginForm(); ?>

        <?php echo CHtml::errorSummary($form); ?>

        <div class="row">
            <?php echo CHtml::activeLabel($form,'login_or_email'); ?>
            <?php echo CHtml::activeTextField($form,'login_or_email') ?>
            <p class="hint">Please enter your username or email address.</p>
        </div>

        <div class="row buttons">
            <?php echo CHtml::button("Reset Password",array('id'=>'btnSubmit','class'=>"btn btn-success")); ?>
        </div>

        <?php echo CHtml::endForm(); ?>
    </div><!-- form -->
    <script type="text/javascript">
        $(function(){
            $("#btnSubmit").click(function(){
                $('form').submit();
                return false;
            });
        });
    </script>
<?php endif; ?>