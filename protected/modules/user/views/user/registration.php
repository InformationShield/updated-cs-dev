<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
    UserModule::t("Registration"),
);
?>

    <h1><?php echo UserModule::t("Registration"); ?></h1>

<?php if(Yii::app()->user->hasFlash('registration')): ?>
    <div class="success">
        <?php echo Yii::app()->user->getFlash('registration'); ?>
    </div>
<?php else: ?>

    <div class="form">
        <?php $form=$this->beginWidget('UActiveForm', array(
            'id'=>'registration-form',
            'enableAjaxValidation'=>true,
            'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
            'htmlOptions' => array('enctype'=>'multipart/form-data'),
        )); ?>

		<input type="hidden" name="RegistrationForm[inviteToken]" value="<?= $model->inviteToken; ?>">

        <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

        <?php echo $form->errorSummary(array($model,$profile)); ?>

        <div class="row">
            <?php echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username'); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->passwordField($model,'password'); ?>
            <?php echo $form->error($model,'password'); ?>
            <p class="hint">
                <?php echo UserModule::t("Minimal password length 8 characters."); ?>
            </p>
        </div>

		<div class="row">
            <?php echo $form->labelEx($model,'verifyPassword'); ?>
            <?php echo $form->passwordField($model,'verifyPassword'); ?>
            <?php echo $form->error($model,'verifyPassword'); ?>
		</div>

        <? if ($model->inviteToken) { ?>
			<div class="row">
				<div class="row">
                    <?php echo $form->labelEx($model,'email'); ?>
                    <input type="text" name="RegistrationForm[email]" value="<?= $model->email; ?>" readonly>
                    <?php echo $form->error($model,'email'); ?>
				</div>
			</div>
        <? } else { ?>
			<div class="row">
                <?php echo $form->labelEx($model,'email'); ?>
                <?php echo $form->textField($model,'email'); ?>
                <?php echo $form->error($model,'email'); ?>
			</div>
        <? } ?>

        <?php
        $profileFields=$profile->getFields();
        if ($profileFields) {
            foreach($profileFields as $field) {
                ?>
                <div class="row">
                    <?php echo $form->labelEx($profile,$field->varname); ?>
                    <?php
                    if ($widgetEdit = $field->widgetEdit($profile)) {
                        echo $widgetEdit;
                    } elseif ($field->range) {
                        echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                    } elseif ($field->field_type=="TEXT") {
                        echo$form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
                    } else {
                        echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                    }
                    ?>
                    <?php echo $form->error($profile,$field->varname); ?>
                </div>
            <?php
            }
        }
        ?>

        <?php if (UserModule::doCaptcha('registration')): ?>
            <div class="row">
                <?php echo $form->labelEx($model,'verifyCode'); ?>

                <?php $this->widget('CCaptcha'); ?>
                <?php echo $form->textField($model,'verifyCode'); ?>
                <?php echo $form->error($model,'verifyCode'); ?>

                <p class="hint"><?php echo UserModule::t("Please enter the letters as they are shown in the image above."); ?>
                    <br/><?php echo UserModule::t("Letters are not case-sensitive."); ?></p>
            </div>
        <?php endif; ?>

        <div class="row">
            &nbsp;
        </div>

        <div class="row">
            <label for="RegistrationForm_eula"><a class="lnkEULA">End User License Agreement</a></label>
            <p><?php echo $form->checkBox($model,'eula'); ?> &nbsp;I accept the terms of the <a class="lnkEULA"><u>End User License Agreement</u></a></p>
            <?php echo $form->error($model,'eula'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::button(UserModule::t("Register"),array('id'=>'btnSubmit','class'=>"btn btn-success")); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div><!-- form -->
    <script type="text/javascript">
        $(function(){
            $("#btnSubmit").click(function(){
                if ($('#RegistrationForm_eula').is(':checked')) {
                    $('#registration-form').submit();
                } else {
                    w2alert('You have not accepted the terms of the End User License Agreement.','End User License Agreement')
                }
                return false;
            });

            $('.lnkEULA').on('click',function(){
                $.ajax({
                    method: 'GET',
                    dataType: "html",
                    url: '<?= url('site/eula'); ?>',
                    success: function (response) {
                        w2popup.open({
                            body: response,
                            showMax: true,
                            width: 600,
                            height: 600,
                            title: 'End User License Agreement',
                            buttons : '<button class="btn" onclick="w2popup.close();">Close</button>'
                        });
                    }
                });
                return false;
            });
        });
    </script>
<?php endif; ?>