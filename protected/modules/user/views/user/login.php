<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
    UserModule::t("Login"),
);
?>

<style type="text/css">
    div.success{
        border: 2px solid #116611;
	padding: 7px 7px 12px 7px;
	margin: 0 0 20px 0;
        background: #BAE0BA;
	font-size: 0.9em;}
</style>

    <h1><?php echo UserModule::t("Login"); ?></h1>

<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

    <div class="success">
        <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
    </div>

<?php endif; ?>
    
<?php if($msg): ?>

    <div class="success">
        <?php echo $msg; ?>
    </div>

<?php endif; ?>

    <p><?php echo UserModule::t("Please fill out the following form with your login credentials:"); ?></p>

    <div class="form">
        <?php echo CHtml::beginForm(url('user/login'), 'post'); ?>

        <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

        <? if ($model->errorCode == UserIdentity::ERROR_STATUS_BAN) { ?>
            <h3 style="color: red;">Your account is blocked.</h3>
        <? } else { ?>
            <?php echo CHtml::errorSummary($model); ?>
        <? } ?>

        <div class="row">
            <?php echo CHtml::activeLabelEx($model,'username'); ?>
            <?php echo CHtml::activeTextField($model,'username') ?>
        </div>

        <div class="row">
            <?php echo CHtml::activeLabelEx($model,'password'); ?>
            <?php echo CHtml::activePasswordField($model,'password') ?>
        </div>

        <div class="row">
            <p class="hint">
                <?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?> | <?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
            </p>
        </div>

        <div class="row rememberMe">
            <?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
            <?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton(UserModule::t("Login"),array('id'=>'btnSubmit','class'=>"btn btn-login")); ?>
        </div>

        <?php echo CHtml::endForm(); ?>
    </div><!-- form -->
    <script type="text/javascript">
        $(function(){
			$('#UserLogin_username').select().focus();

            $("#btnSubmit").click(function(){
                $('form').submit();
                return false;
            });
        });
    </script>