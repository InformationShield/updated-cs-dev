<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'recid', caption: 'recId', size: '30px', hidden: true, hideable: true, sortable: true, resizable: true},
        { field: 'id', caption: 'Id', size: '30px', hidden: false, hideable: true, sortable: true, resizable: true, searchable: 'int',
            render: function(record, index, column_index){
                return '<a href="<?php echo url('user/admin/impersonate/id'); ?>/' + record.id + '">' + record.id + '</a>';
            }},
        { field: 'username', caption: 'Username', size: '100%', searchable: 'text', resizable: true, sortable: true, 
            render: function(record, index, column_index){
                return '<a href="<?php echo url('user/admin/view/id'); ?>/' + record.id + '">' + record.username + '</a>';
            }},
        { field: 'tu.email', caption: 'Email', size: '100%', searchable: 'text', resizable: true, sortable: true,
            render: function(record, index, column_index){
                return '<a href="mailto:' + record.email + '">' + record.email + '</a>';
            }},
        { field: 'name', caption: 'Companies', size: '100%', searchable: 'text', resizable: true, sortable: true,
            render: function(record, index, column_index){
                return '<a href="<?php echo url('user/employee/list/email'); ?>/' + record.email + '">' + record.name + '</a>';
            }},
        { field: 'superuser', caption: 'Is Superuser', size: '90px', searchable: 'int', resizable: true, sortable: true,
            render: function (record, index, column_index) {
            return (record.superuser == 1) ? "Yes" : "No";
            }},
        { field: 'create_at', caption: 'Created', size: '90px', searchable: 'text', resizable: true, sortable: true },
        { field: 'lastvisit_at', caption: 'Last Visit', size: '90px', searchable: 'text', resizable: true, sortable: true },
        { field: 'status', caption: 'Status', size: '90px', searchable: 'text', resizable: true, sortable: true,
            render: function (record, index, column_index) {
                var html = '';
                switch (record.status){
                    case "<?php echo Cons::USER_STATUS_NOT_ACTIVE; ?>":
                        html = 'Not Active';
                        break;
                    case "<?php echo Cons::USER_STATUS_ACTIVE; ?>":
                        html = 'Active';
                        break;
                    case "<?php echo Cons::USER_STATUS_BANNED; ?>":
                        html = 'Banned';
                        break;
                    case "<?php echo Cons::USER_STATUS_DELETED; ?>":
                        html = 'Deleted';
                        break;
                }
                return html;
        }},
        { field: 'eula', caption: 'Read EULA', size: '90px', searchable: 'int', resizable: true, sortable: true,
            render: function (record, index, column_index) {
            return (record.eula == 1) ? "Yes" : "No";
            }},
        { field: 'action', caption: 'Actions', size: '155px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                    var html = '<div>';
                    html += '<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkEdit" href="<?= url('user/user/edit'); ?>/id/'+record.recid+'">Edit</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>';
                    html += '</div>';
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = function(event) {};

</script>

<?php
$gmAjaxListGrid = new gmAjaxListGrid();
$gmAjaxListGrid->listUrl = $listUrl;
$gmAjaxListGrid->title = '<h2>Manage User Accounts</h2>';
$gmAjaxListGrid->intro = '<p>&bull; Click on Id to impersonate that user (login as that user).<br/>
	&bull; Click on username to view user records details.<br/>
	&bull; Click on E-mail to send an email to user.<br/>
	&bull; Click on Companies to display in which companies user appear.</p>';
$gmAjaxListGrid->controller = "user/user";
$gmAjaxListGrid->recid = "recid";
$gmAjaxListGrid->allowAdd = false;
$gmAjaxListGrid->allowEdit = true;
$gmAjaxListGrid->allowDelete = true;
$gmAjaxListGrid->allowCopy = false;
$gmAjaxListGrid->reloadOnCopy = true;
$gmAjaxListGrid->toolbarColumns = true;
$gmAjaxListGrid->limit = 3000;
$gmAjaxListGrid->showExportCSV = true;
$gmAjaxListGrid->customSearch = array(
    array(
        'field' => 'id', 
        'caption' => 'Id', 
        'type' => 'int'
    ),
    array(
        'field' => 'username', 
        'caption' => 'Username', 
        'type' => 'text'
    ),
    array(
        'field' => 'tu.email',
        'caption' => 'Email', 
        'type' => 'text'
    ),
    array(
        'field' => 'superuser', 
        'caption' => 'Is Superuser', 
        'type' => 'list', 
        'options' => array(
            'items' => array(
                array('id' => '1', 'text' => 'Yes'),
                array('id' => '0', 'text' => 'No'),
            )
        )
    ),
    array(
        'field' => 'create_at', 
        'caption' => 'Create At', 
        'type' => 'date'
    ),
    array(
        'field' => 'lastvisit_at', 
        'caption' => 'Last Visit', 
        'type' => 'date'
    ),
    array(
        'field' => 'status', 
        'caption' => 'Status', 
        'type' => 'list', 
        'options' => array(
            'items' => array(
                array('id' => '0', 'text' => 'Not Active'),
                array('id' => '1', 'text' => 'Active'),
                array('id' => '-1', 'text' => 'Banned'),
                array('id' => '-2', 'text' => 'Deleted'),
            )
        )
    ),
    array(
        'field' => 'eula', 
        'caption' => 'Read EULA', 
        'type' => 'list', 
        'options' => array(
            'items' => array(
                array('id' => '1', 'text' => 'Yes'),
                array('id' => '0', 'text' => 'No'),
            )
        )
    ),
);
AjaxListGridGadget::show($gmAjaxListGrid);

?>