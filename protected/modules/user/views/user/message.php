<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login"); ?>

<h1><?php echo $title; ?></h1>

<div class="form">
    <?php echo $content; ?>
</div>
<div style="margin: 20px 0 0 0;">
    <a href="<?= url('user/login'); ?>"  class="btn btn-success">Login</a>
</div>
