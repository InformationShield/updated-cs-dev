<script type="text/javascript">
    var _ajaxListGridColumns = [
		{ field: 'name', caption: 'Company', size: '100%', searchable: 'text', resizable: true, sortable: true},
        { field: 'recid', caption: 'recId', size: '30px', hidden: true, hideable: true, sortable: true, resizable: true},
        { field: 'firstname', caption: 'First Name', size: '100%', searchable: 'text', resizable: true, sortable: true},
        { field: 'lastname', caption: 'Last Name', size: '100%', searchable: 'text', resizable: true, sortable: true},
        { field: 'email', caption: 'Email', size: '100%', searchable: 'text', resizable: true, sortable: true},
        { field: 'department_name', caption: 'Department', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'company_role_name', caption: 'Company Role', size: '110px', searchable: 'text', resizable: true, sortable: true },
        { field: 'cu.company_role_id', caption: '', size: '0', hidden: true, searchable: 'int', resizable: true, sortable: true },
        { field: 'created_on', caption: 'Created On', size: '90px', searchable: 'text', resizable: true, sortable: true },
        { field: 'action', caption: 'Actions', size: '155px', hideable: false, sortable: false,  resizable: false,
            render: function (record, index, column_index) {
                    var html = '<div>';
                    html += '<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkEdit" href="<?= url('user/employee/edit'); ?>/id/'+record.recid+'">Edit</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>';
                    html += '</div>';
                return html;
            }
        }
    ];

    var _ajaxListGridAddFunction = function(event) {};

</script>

<?php
$gmAjaxListGrid = new gmAjaxListGrid();
$gmAjaxListGrid->listUrl = $listUrl;
$gmAjaxListGrid->title = '<h2>Manage Employees</h2>';
$gmAjaxListGrid->intro = '<p>';
if(isset($email) && $email){
    $gmAjaxListGrid->intro .= 'Currently showing records that match email <b>' . $email . '</b>. <a href="'.url('user/employee/list').'" style="color: blue;">Click here to show all users</a>.';
}
$gmAjaxListGrid->intro .= '</p>';
$gmAjaxListGrid->controller = "user/employee";
$gmAjaxListGrid->recid = "recid";
$gmAjaxListGrid->allowAdd = false;
$gmAjaxListGrid->allowEdit = true;
$gmAjaxListGrid->allowDelete = true;
$gmAjaxListGrid->allowCopy = false;
$gmAjaxListGrid->reloadOnCopy = true;
$gmAjaxListGrid->toolbarColumns = true;
$gmAjaxListGrid->limit = 3000;
$gmAjaxListGrid->showExportCSV = true;
$gmAjaxListGrid->customSearch = array(
	array(
		'field' => 'cp.name',
		'caption' => 'Company',
		'type' => 'text'
	),
    array(
        'field' => 'firstname', 
        'caption' => 'First Name', 
        'type' => 'text'
    ),
    array(
        'field' => 'lastname', 
        'caption' => 'Last Name', 
        'type' => 'text'
    ),
    array(
        'field' => 'email',
        'caption' => 'Email', 
        'type' => 'text'
    ),
    array(
        'field' => 'department_name', 
        'caption' => 'Department', 
        'type' => 'text'
    ),
    array(
        'field' => 'company_role_name', 
        'caption' => '', 
        'type' => 'text',
        'hidden' => true
    ),
    array(
        'field' => 'cu.company_role_id', 
        'caption' => 'Company Role', 
        'type' => 'list', 
        'hidden' => (empty($companyRoles)) ? true : false,
        'options' => array(
            'items' => $companyRoles
        )
    ),
    array(
        'field' => 'created_on', 
        'caption' => 'Created On', 
        'type' => 'date'
    )
);
if(isset($email) && $email){
    $gmAjaxListGrid->searchData[] = array(
        'field' => 'email',
        'value' => $email,
        'operator' => 'begins',
        'type' => 'text'
    );
}
AjaxListGridGadget::show($gmAjaxListGrid);

?>