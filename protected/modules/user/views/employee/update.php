<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit User</h3>

<div class="form">

<? $form=$this->beginWidget('CActiveForm', array('action'=>url('user/employee/edit',array('id'=>Obscure::encode($model->employee_id))),
	'id'=>'edit-form',
	'enableAjaxValidation'=>false,
)); ?>
	<input type="hidden" id="invitebyemail" name="invitebyemail" value="0">

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
        <?= $form->labelEx($model,'firstname'); ?>
		<?= $form->textField($model,'firstname',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'firstname'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'lastname'); ?>
		<?= $form->textField($model,'lastname',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'lastname'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'email'); ?>
		<?= $form->textField($model,'email',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<? $roles = CompanyRole_Ex::getAllRoles(); ?>
		<? if (!empty($roles)) { ?>
			<?= $form->labelEx($model,'company_role_id'); ?>
			<select name="Employee[company_role_id]" id="Employee_company_role_id" style="width: 320px;">
				<? foreach ($roles as $role) { ?>
					<option value="<?= $role->company_role_id; ?>" <?= ($model->company_role_id == $role->company_role_id) ? 'selected="selected"' : ''; ?>><?= $role->company_role_name; ?></option>
				<? } ?>
			</select>
			<?= $form->error($model,'company_role_id'); ?>
		<? } ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'department_name'); ?>
		<?= $form->textField($model,'department_name',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'department_name'); ?>
	</div>

	<? /* div class="row">
		<?= $form->labelEx($model,'profile_id'); ?>
		<select name="Employee[profile_id]" id="Employee_profile_id" style="width: 320px;">
			<option value="0" <?= ($model->profile_id == 0) ? 'selected="selected"' : ''; ?>>None</option>
			<? $criteria = new CDbCriteria(array('order' => 'profile_title ASC')); ?>
			<? $profiles = UserProfile::model()->findAllByAttributes(array('company_id' => $model->company_id,'status'=>1),$criteria); ?>
			<? foreach ($profiles as $profile) { ?>
				<option value="<?= $profile->profile_id; ?>" <?= ($model->profile_id == $profile->profile_id) ? 'selected="selected"' : ''; ?>><?= $profile->profile_title; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'profile_id'); ?>
	</div */ ?>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('user/employee'); ?>";
			return false;
		});
	});
</script>