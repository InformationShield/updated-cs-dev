<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
$this->breadcrumbs=array(
    UserModule::t("Profile")=>array('profile'),
    UserModule::t("Edit"),
);
//$this->menu=array(
//    ((UserModule::isAdmin())
//        ?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin'))
//        :array()),
//    array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
//    array('label'=>UserModule::t('Profile'), 'url'=>array('/user/profile')),
//    array('label'=>UserModule::t('Change password'), 'url'=>array('changepassword')),
//    array('label'=>UserModule::t('Logout'), 'url'=>array('/user/logout')),
//);
?>
<h1>Edit Account Info</h1>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
    <div class="success">
        <?php echo Yii::app()->user->getFlash('profileMessage'); ?>
    </div>
<?php endif; ?>
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'profile-form',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype'=>'multipart/form-data'),
    )); ?>

    <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

    <?php echo $form->errorSummary(array($model,$profile)); ?>

    <?php
    $profileFields=$profile->getFields();
    if ($profileFields) {
        foreach($profileFields as $field) {
            ?>
            <div class="row">
                <?php echo $form->labelEx($profile,$field->varname);

                if ($widgetEdit = $field->widgetEdit($profile)) {
                    echo $widgetEdit;
                } elseif ($field->range) {
                    echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                } elseif ($field->field_type=="TEXT") {
                    echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
                } else {
                    echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                }
                echo $form->error($profile,$field->varname); ?>
            </div>
        <?php
        }
    }
    ?>
	<? /* Don't let user change login name
		<div class="row">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>
	*/ ?>

<? /*********************************
        Can not change email address of user it is:
        1. a security risk since email not verified and
        2. company users are linked by email address, so if use could change if could spoof another user
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
***************************************/ ?>

    <div class="row buttons">
        <?= CHtml::button($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'),array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
        <?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    $(function(){
        $("#btnSave").click(function(){
            $('#profile-form').submit();
            return false;
        });
        $("#btnCancel").click(function(){
            window.location = "<?= url('user/profile'); ?>";
            return false;
        });
    });
</script>