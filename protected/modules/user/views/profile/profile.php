<? $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
$this->breadcrumbs=array(
	UserModule::t("Profile"),
);
?>
<h1><i class="fa fa-user"></i> Account Info</h1>

<? if(Yii::app()->user->hasFlash('profileMessage')) { ?>
	<div class="success">
		<?= Yii::app()->user->getFlash('profileMessage'); ?>
	</div>
<? } ?>

<div class="form">
	<div class="row">
		<div class="col-md-3">
			<b>Username:</b>
		</div>
		<div class="col-md-9">
			<?= CHtml::encode($model->username); ?>
		</div>
	</div>

	<? $profileFields=ProfileField::model()->forOwner()->sort()->findAll(); ?>
	<? if ($profileFields) { ?>
		<? foreach($profileFields as $field) {	?>
			<div class="row">
				<div class="col-md-3">
					<b><?= CHtml::encode(UserModule::t($field->title)); ?>:</b>
				</div>
				<div class="col-md-9">
					<?= (($field->widgetView($profile))?$field->widgetView($profile):CHtml::encode((($field->range)?Profile::range($field->range,$profile->getAttribute($field->varname)):$profile->getAttribute($field->varname)))); ?>
				</div>
			</div>
		<? } ?>
	<? } ?>

	<div class="row">
		<div class="col-md-3">
			<b><?= CHtml::encode($model->getAttributeLabel('email')); ?>:</b>
		</div>
		<div class="col-md-9">
			<?= CHtml::encode($model->email); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<b><?= CHtml::encode($model->getAttributeLabel('create_at')); ?>:</b>
		</div>
		<div class="col-md-9">
			<?= $model->create_at; ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<b><?= CHtml::encode($model->getAttributeLabel('lastvisit_at')); ?>:</b>
		</div>
		<div class="col-md-9">
			<?= $model->lastvisit_at; ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<b><?= CHtml::encode($model->getAttributeLabel('status')); ?>:</b>
		</div>
		<div class="col-md-9">
			<?= CHtml::encode(User::itemAlias("UserStatus",$model->status)); ?>
		</div>
	</div>

	<div style="margin: 20px 10px">
		<div class="row buttons">
			<a href="<?= Yii::app()->createAbsoluteUrl('user/profile/edit'); ?>" class="btn btn-success">Edit Account Info</a>
			<a href="<?= Yii::app()->createAbsoluteUrl('user/profile/changepassword'); ?>" class="btn btn-success">Change Password</a>
		</div>
	</div>
</div>
