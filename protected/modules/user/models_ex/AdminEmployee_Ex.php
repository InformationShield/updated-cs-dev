<?php
/* @var $model Employee */

class AdminEmployee_Ex extends BaseSearchSort_Ex
{
    
    public static function getEmployeeViewValues($id)
    {
        $v = array();
        $row = AdminEmployee_Ex::getAllUsersFromCompanies($count, $id);
        if (!empty($row)) {
            $model = (object) $row;
            $v['First Name'] = $model->firstname;
            $v['Last Name'] = $model->lastname;
            $v['Email'] = $model->email;
            $v['Company'] = $model->company_name;
            $v['Department'] = $model->department_name;
            $v['Department Id'] = $model->department_id;
            $v['Role'] = $model->company_role_name;
            $v['Profile Id'] = $model->profile_id;
            $v['Profile'] = $model->profile_title;
            $v['Created On'] = DateHelper::FormatDateTimeString($model->created_on, "m/d/Y");
        }
        return $v;
    }
    
    public static function getAllUsersFromCompanies(&$count, $employeeId=null,$pageSize=0,$offsetBy=0,$select=null,$search=null,$searchLogic=null,$sort=null)
    {
        $retVal = null;
        $sqlLimit = null;
        $count = 0;
        if ($select==null) {
            $select = "SELECT cu.*, TRIM(CONCAT(cu.firstname,' ',cu.lastname)) employee_name, cr.company_role_name, cp.name, cp.name as company_name, up.profile_title";
        }
        $from = " FROM employee cu"
               ." INNER JOIN company_role cr ON cu.company_role_id = cr.company_role_id"
               ." INNER JOIN company cp ON cu.company_id = cp.company_id"
               ." LEFT JOIN user_profile up ON cu.profile_id = up.profile_id AND up.company_id=cu.company_id";
        $sqlWhere = " WHERE 1";
        if ($employeeId) {
            $sqlWhere .= " AND cu.employee_id = :employee_id";
        }
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        if ($pageSize > 0) {
            $sqlLimit = " LIMIT ".$offsetBy.", ".$pageSize;
            $sqlCount = "SELECT COUNT(*) totRows FROM (".$select.$from.$sqlWhere.") allrows";
            $cmdCount = Yii::app()->db->createCommand($sqlCount);
            if ($employeeId) {
                $cmdCount->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
            }
            if (!empty($params)) {
                foreach ($params as $param) {
                    $cmdCount->bindParam($param['name'], $param['value'], $param['type']);
                }
            }
            $countResults = $cmdCount->query()->read();
            if (isset($countResults["totRows"]) && $countResults["totRows"] > 0) {
                $count = $countResults["totRows"];
            }
        }
        if($sort == null){
            $orderBy = " ORDER BY cp.name, cu.employee_id";
        }else{
            $orderBy = self::sort2OrderBy($sort);
        }
        
        $sql = $select.$from.$sqlWhere.$orderBy.$sqlLimit;
        $command = Yii::app()->db->createCommand($sql);
        if ($employeeId) {
            $command->bindParam(":employee_id", $employeeId, PDO::PARAM_INT);
        }
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        $results = $command->query()->readAll();
        if (!empty($results)) {
            if ($employeeId) {  // get a particular control
                $retVal = $results[0];
                $count = 1;
            } else {
                $retVal = $results;
                $count = count($results);
            }
        }
        return $retVal;
    }
    
    public static function getAllUsersFromCompaniesArray($pageSize = 0, $offsetBy = 0, $search = null, $searchLogic = null, $sort = null)
    {
        $select = null;
        $rows = AdminEmployee_Ex::getAllUsersFromCompanies($count, null, $pageSize, $offsetBy, $select, $search, $searchLogic, $sort);
        
        if(!empty($rows)){
            foreach ($rows as &$row) {
                $row['recid'] = Obscure::encode($row['employee_id']);
                $row['created_on'] = DateHelper::FormatDateTimeString($row['created_on'], "m/d/Y");
            }
        }
        
        $array = array("status" => "success", "total" => $count, "records" => ($count==0) ? array() : $rows );
        return $array;
    }
    
    public static function getAllUsersFromCompaniesJson($pageSize = 0, $offsetBy = 0, $search = null, $searchLogic = null, $sort = null)
    {        
        $array = AdminEmployee_Ex::getAllUsersFromCompaniesArray($pageSize, $offsetBy, $search, $searchLogic, $sort);
        $json = json_encode($array);
        return $json;
    }
    
    public static function createCSVReportData($search)
    {
        $data = array();
        $data[] = array('Id', 'First Name', 'Last Name', 'Email', 'Company', 'Department', 'Company Role', 'Created');
        
        $results = AdminEmployee_Ex::getAllUsersFromCompaniesArray($search['pageSize'], $search['offsetBy'], $search['search'], $search['searchLogic'], $search['sort']);
        
        $rows = $results['records'];
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $data[] = array(
                    $row['employee_id'],
                    $row['firstname'],
                    $row['lastname'],
                    $row['email'],
                    $row['company_name'],
                    $row['department_name'],
                    $row['company_role_name'],
                    $row['created_on']
                );
            }
        }
        return $data;
    }
    
    public static function exportCSV($search)
    {
        // Do not save any type of record in database when acting as superadmin
        $data = array(
            'pageSize' => 0, //$search['pageSize'], get all data on export
            'offsetBy' => 0, //$search['offsetBy'],
            'search' => $search['search'],
            'searchLogic' => $search['searchLogic'],
            'sort' => $search['sort'],
        );
        
        // instead set temp session variable to pass search data
        setSessionVar('adminEmployeeExportCsvSearch', json_encode($data));
        
        $results = array(
            'status' => 'success',
            'message' => 'Your file is downloading now.',
            'href' => url('user/employee/download')
        );
        
        $json = json_encode($results);
        return $json;
    }
}