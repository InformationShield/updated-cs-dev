<?php
class AdminUser_Ex extends BaseSearchSort_Ex
{
    public static function getUserViewValues($id)
    {
        $user = User::model()->notsafe()->findbyPk($id);
        $values = array(
            'Id' => $user->id,
            'First Name' => (isset($user->profile->first_name)) ? $user->profile->first_name : '',
            'Last Name' => (isset($user->profile->last_name)) ? $user->profile->last_name : '',
            'Username' => $user->username,
            'Email' => $user->email,
            'Activation Key' => $user->activkey,
            'Registration Date' => $user->create_at,
            'Last Visit' => $user->lastvisit_at,
            'Status' => $user->itemAlias('UserStatus', $user->status),
            'Superuser' => $user->itemAlias('AdminStatus', $user->superuser),
            'Read EULA' => $user->itemAlias('ReadEULA', $user->eula),
        );
        return $values;
    }
    
    public static function getAllUsers(&$count, $pageSize = 0, $offsetBy = 0, $select = null, $search = null, $searchLogic = null, $sort = null)
    {
        $results = null;
        $sqlLimit = null;
        $count = 0;
        
        if ($select == null){
            $select = "SELECT tu.*, tp.first_name, tp.last_name";
        }
        
        $from = "
			FROM `tbl_users` as tu
			INNER JOIN `tbl_profiles` as tp ON tu.id = tp.user_id
			LEFT JOIN employee cu ON cu.email = tu.email
			LEFT JOIN company cp ON cp.user_id = tp.user_id
        ";
        
        $sqlWhere = " WHERE 1 ";
        $sqlWhere2 = self::advanceSearch2Where($search,$searchLogic,$params);
        if ($sqlWhere2) {
            $sqlWhere .=  ' AND '.$sqlWhere2;
        }
        $groupBy = " GROUP BY tu.id";
        $orderBy = self::sort2OrderBy($sort);
        $sql = $select.$from.$sqlWhere.$groupBy.$orderBy.$sqlLimit;
        
        $command = Yii::app()->db->createCommand($sql);
        
        if (!empty($params)) {
            foreach ($params as $param) {
                $command->bindParam($param['name'], $param['value'], $param['type']);
            }
        }
        
        $results = $command->query()->readAll();
        
        $count = count($results);
        return $results;
    }
    
    public static function getAllUsersArray($pageSize = 0, $offsetBy = 0, $search = null, $searchLogic = null, $sort = null)
    {
        $select = "
			SELECT tu.id, tu.username, tu.email, tu.superuser, tu.create_at, tu.lastvisit_at, tu.status, tu.eula, tp.first_name, tp.last_name,
			GROUP_CONCAT(DISTINCT cp.name SEPARATOR ', ') name
        ";
        $rows = AdminUser_Ex::getAllUsers($count, $pageSize, $offsetBy, $select, $search, $searchLogic, $sort);
        
        if(!empty($rows)){
            foreach ($rows as &$row) {
                $row['recid'] = Obscure::encode($row['id']);
                $row['create_at'] = DateHelper::FormatDateTimeString($row['create_at'], "m/d/Y");
                $row['lastvisit_at'] = DateHelper::FormatDateTimeString($row['lastvisit_at'], "m/d/Y");
            }
        }
        
        $array = array("status" => "success", "total" => $count, "records" => ($count==0) ? array() : $rows );
        return $array;
    }
    
    public static function getAllUsersJson($pageSize = 0, $offsetBy = 0, $search = null, $searchLogic = null, $sort = null)
    {        
        $array = AdminUser_Ex::getAllUsersArray($pageSize, $offsetBy, $search, $searchLogic, $sort);
        $json = json_encode($array);
        return $json;
    }
    
    public static function createCSVReportData($search)
    {
        $data = array();
        $data[] = array('Id', 'Username', 'Email', 'First Name', 'Last Name', 'Is Superuser', 'Created', 'Last Visit', 'Status', 'Read EULA');
        
        $results = AdminUser_Ex::getAllUsersArray($search['pageSize'], $search['offsetBy'], $search['search'], $search['searchLogic'], $search['sort']);
        
        $rows = $results['records'];
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $is_superuser = ($row['superuser']) ? 'Yes' : 'No';
                switch ($row['status']){
                    case Cons::USER_STATUS_NOT_ACTIVE:
                        $status = 'Not Active';
                        break;
                    case Cons::USER_STATUS_ACTIVE:
                        $status = 'Active';
                        break;
                    case Cons::USER_STATUS_BANNED:
                        $status = 'Banned';
                        break;
                    case Cons::USER_STATUS_DELETED:
                        $status = 'Deleted';
                        break;
                    default:
                        $status = '';
                        break;
                }
                $eula = ($row['eula']) ? 'Yes' : 'No';
                
                $data[] = array(
                    $row['id'],
                    $row['username'],
                    $row['email'],
                    $row['first_name'],
                    $row['last_name'],
                    $is_superuser,
                    $row['create_at'],
                    $row['lastvisit_at'],
                    $status,
                    $eula
                );
            }
        }
        return $data;
    }
    
    public static function exportCSV($search)
    {
        // Do not save any type of record in database when acting as superadmin
        $data = array(
            'pageSize' => 0, //$search['pageSize'], get all data on export
            'offsetBy' => 0, //$search['offsetBy'],
            'search' => $search['search'],
            'searchLogic' => $search['searchLogic'],
            'sort' => $search['sort'],
        );
        
        // instead set temp session variable to pass search data
        setSessionVar('adminUserExportCsvSearch', json_encode($data));
        
        $results = array(
            'status' => 'success',
            'message' => 'Your file is downloading now.',
            'href' => url('user/user/download')
        );
        
        $json = json_encode($results);
        return $json;
    }
}