<?php

class UserController extends GridViewController
{
    public $layout='//layouts/admin_column1';
    
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + get, delete', // we only allow deletion via POST request
            'ajaxOnly + get, view, delete',
        );
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
            return array(
                array('allow',
                        'actions'=>array('view', 'get', 'edit', 'delete', 'download'),
                        'expression'=>'!user()->isGuest && Utils::isSuper()',
                ),
                array('deny',  // deny all users
                        'users'=>array('*'),
                ),
            );
    }	


    protected function getViewValues($id, &$skipEncode) {
        $values = AdminUser_Ex::getUserViewValues($id);
        $skipEncode = array();
        return $values;
    }


    /**
     * Displays a particular user.
     */
    public function actionView()
    {
        $this->renderView();
    }


    public function actionGet()
    {
            $search = $this->getSearchParams();
            $export = getGet('export');

            if ($export == 'csv') {
                    $json = AdminUser_Ex::exportCSV($search);
            } else {
                    $json = AdminUser_Ex::getAllUsersJson($search['pageSize'], $search['offsetBy'], $search['search'], $search['searchLogic'], $search['sort']);
            }
            echo $json;
    }

    /**
     * Updates a particular user.
     * If update is successful, the browser will be redirected to the 'list' page.
     */
    public function actionEdit()
    {
            $id = $this->paramObscureId();
            $model = User::model()->notsafe()->findbyPk($id);

            if(!$model){ // record not found
                $this->redirect(url('user'));
            }

            $profile = $model->profile;
            if(isset($_POST['User']))
            {
                    $model->attributes=$_POST['User'];
                    $profile->attributes=$_POST['Profile'];

                    if($model->validate() && $profile->validate()) {
                            $old_password = User::model()->notsafe()->findByPk($model->id);
                            $model->password = $old_password->password; // force no admin changes of passwords
                            if ($old_password->password!=$model->password) {
                                    $model->password=Yii::app()->controller->module->encrypting($model->password);
                                    $model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
                            }
                            $model->save();
                            $profile->save();
                            $this->redirect(url('user'));
                    } else $profile->validate();
            }

            $this->render('update',array(
                    'model'=>$model,
                    'profile'=>$profile,
            ));
    }

    // User Delete
    // override method called by ajaxDelete()
    protected function doDelete($id, $markStatusOnly=false)
    {
        $results = array('status'=>'','message'=>'');
        if (Utils::isSuper()) {
                // we only allow deletion via POST request
                $id = $this->paramObscureId();
                $model = User::model()->notsafe()->findbyPk($id);

                // check if record exists
                if($model){
                    //$profile = Profile::model()->findByPk($model->id);
                    //$profile->delete();
                    //$model->delete();
                    // DO NOT PHYSICALLY DELETE JUST RENAME AND DISABLE STATUS
                    $t = time().'_';
                    $model->username = substr(param('dbDeletedPrefix').$t.$model->username,0,128);
                    $model->email = substr(param('dbDeletedPrefix').$t.$model->email,0,128);
                    $model->status = Cons::USER_STATUS_DELETED;
                    $model->save();

                    $results = array('status'=>'success','message'=>'Record marked as deleted.');
                }else{
                    $results = array('status'=>'error','message'=>'Delete failed. Record not found.');
                }
        }else {
            $results = array('status'=>'error','message'=>'You are not authorized to perform this action.');
        }
        return $results;
    }
    
    public function actionDelete()
    {
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }
    
    public function actionDownload()
    {
        // skip GridViewController downloadFile function and go straight to mDownloadFile
        // instead read seassion variable
        $search = json_decode(getSessionVar('adminUserExportCsvSearch'), TRUE);
        
        // remove session variable
        setSessionVar('adminUserExportCsvSearch', NULL);
        
        // will send headers and exit if successful, otherwise returns message of failure
        $message = mAdminDownloadFile::send(mAdminDownloadFile::AdminUserExport, $search);
        if($message){
            $this->renderMessage(array('messageId'=>0,'title'=>'<i class="fa fa-warning"></i> Download Failed!','message'=>$message));
        }
        
    }
}
