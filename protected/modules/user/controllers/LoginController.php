<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$this->lastVisit();
					$this->redirect(url('site/home'));
				}
				if ($model->errorCode == UserIdentity::ERROR_RESET_PASSWORD) {
					$this->redirect(url('user/recovery/recovery/reason/expired'));
				}
			}
                        
            // check if any message is set
            $msg = null;
            if(isset($_GET['msg'])){
                $msg = urldecode($_GET['msg']);
            }
                        
			// display the login form
			$this->render('/user/login',array('model'=>$model, 'msg' => $msg));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
	
	private function lastVisit() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		$lastVisit->save();
	}

}