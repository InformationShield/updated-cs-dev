<?php

class EmployeeController extends GridViewController
{
    public $layout='//layouts/admin_column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + get, delete', // we only allow deletion via POST request
            'ajaxOnly + get, view, delete',
        );
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
            return array(
                array('allow',
                        'actions'=>array('index', 'list', 'get', 'view', 'edit', 'download', 'delete'),
                        'expression'=>'!user()->isGuest && Utils::isSuper()',
                ),
                array('deny',  // deny all users
                        'users'=>array('*'),
                ),
            );
    }

    public function actionIndex()
    {
        $this->actionList();
    }
    
    public function actionList()
    {
        $listUrl = url('user/employee/get');
        $email = getGet('email');
        $companyRoles = array();
        foreach (CompanyRole_Ex::getAllRoles() as $role) {
            $companyRoles[] = array('id' => $role['company_role_id'], 'text' => $role['company_role_name']);
        }
		$this->render('/employee/list',  array('listUrl' => $listUrl, 'companyRoles' => $companyRoles, 'email' => $email));
    }

    protected function getViewValues($id, &$skipEncode) 
    {
        $values = AdminEmployee_Ex::getEmployeeViewValues($id);
        $skipEncode = array();
        return $values;
    }

    public function actionView()
    {
        $this->renderView();
    }

    public function actionGet()
    {
        $search = $this->getSearchParams();
        $export = getGet('export');
        if ($export == 'csv') {
                $json = AdminEmployee_Ex::exportCSV($search);
        } else {
                $json = AdminEmployee_Ex::getAllUsersFromCompaniesJson($search['pageSize'], $search['offsetBy'], $search['search'], $search['searchLogic'], $search['sort']);
        }
        echo $json;
    }

    public function actionEdit()
    {
            $id = $this->paramObscureId();
            $model = Employee::model()->findByPk($id);

            if(!$model){ // record not found
                $this->redirect(url('user/employee'));
            }

            if(isset($_POST['Employee']))
            {
                $model->attributes=$_POST['Employee'];
                $model->company_id = $model->company_id;

                if($model->validate())
                {
                    if($model->save()) {
                        $this->redirect(url('user/employee'));
                    } else {
                        Yii::log(print_r($model->getErrors(), true), 'error', 'EmployeeController.actionEdit');
                    }
                }
            }

            $this->render('update',array(
                    'model'=>$model
            ));
    }
    
    public function actionDownload()
    {
        // skip GridViewController downloadFile function and go straight to mDownloadFile
        // instead read seassion variable
        $search = json_decode(getSessionVar('adminEmployeeExportCsvSearch'), TRUE);
        
        // remove session variable
        setSessionVar('adminEmployeeExportCsvSearch', NULL);
        
        // will send headers and exit if successful, otherwise returns message of failure
        $message = mAdminDownloadFile::send(mAdminDownloadFile::AdminEmployeeExport, $search);
        if($message){
            $this->renderMessage(array('messageId'=>0,'title'=>'<i class="fa fa-warning"></i> Download Failed!','message'=>$message));
        }
        
    }
    
    // Employee Delete
    // can override to do preDeleteCheck to potential stop delete action if return is not true
    protected function preDeleteCheck($id,$model,$markStatusOnly=false)
    {
        return Employee_Ex::preDeleteCheck($id);
    }
    // override method called by ajaxDelete()
    protected function doDelete($id,$markStatusOnly=false)
    {
		$id = $this->paramObscureId(false); // will exit if bad param for security reasons
		if (mEmployee::deleteEmployee($id)) {
			return mGrid::toGridMessage();
		} else {
			return mGrid::toGridMessage('error', 'Failed to delete record.');
		}
    }
    public function actionDelete()
    {
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id);
    }
}
