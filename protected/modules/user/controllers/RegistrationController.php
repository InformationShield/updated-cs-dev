<?php

class RegistrationController extends Controller
{
    public $defaultAction = 'registration';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'transparent' => true,
                //'foreColor'=>0xFF0000,
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * Registration user
     */
    public function actionRegistration()
    {
        $model = new RegistrationForm;
        $profile = new Profile;
        $profile->regMode = true;

        // check if user was invited
        if (!isset($_POST['RegistrationForm']) && isset($_GET['t']) && $_GET['t']) { // invite token detected
            $messageContent = "Unexpected error has occurred.  Please contact support.";
            $inviteToken = $_GET['t'];
            $invite = mInvite::getUserInvite($inviteToken);
            if ($invite->hasErrors()) {
                $messageContent = $invite->getError('error');
                $this->render('/user/message', array('title' => "User Invitation", 'content' => $messageContent));
                return;
            } else {
                $model->inviteToken=$inviteToken;
                $model->email = $invite->invitee_email;
            }
        }

        // hack for david lineman demo only allow user[1-20]@mysecurityiq.com to auto activate
//        $isTestDomainUser = false;
//        if (isset($_POST['RegistrationForm'])) {
//            if (stripos($_POST['RegistrationForm']['email'], '@mysecurityiq.com') !== false) {
//                $email = str_ireplace('@mysecurityiq.com', '', $_POST['RegistrationForm']['email']);
//                $email = str_ireplace('user', '', $email);
//                if ($email >= 1 && $email <= 20) {
//                    $isTestDomainUser = true;
//                }
//            }
//        }

        // ajax validator
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'registration-form') {
            echo UActiveForm::validate(array($model, $profile));
            Yii::app()->end();
        }

        if (Yii::app()->user->id) {
            $this->redirect(Yii::app()->controller->module->profileUrl);
        } else {
            if (isset($_POST['RegistrationForm'])) {
                $model->attributes = $_POST['RegistrationForm'];
                $profile->attributes = ((isset($_POST['Profile']) ? $_POST['Profile'] : array()));

                $invite = null;
                if (isset($_POST['RegistrationForm']['inviteToken'])) {
                    $model->inviteToken = $_POST['RegistrationForm']['inviteToken'];
                    $invite = Invite::model()->findByAttributes(array('invite_guid' => $model->inviteToken));
                    if ($invite) {
                        $model->email = $invite->invitee_email;
                    }
                }

                if ($model->validate() && $profile->validate()) {
                    $sourcePassword = $model->password;
                    $model->activkey = UserModule::encrypting(microtime() . $model->password);
                    $model->password = UserModule::encryptPassword($model->password);
                    $model->verifyPassword = UserModule::encryptPassword($model->verifyPassword);
                    $model->superuser = 0;
                    if ($invite) {  // we know invited user email is already valid
                        $model->status = Cons::USER_STATUS_ACTIVE;
                    } else {
                        $model->status = ((Yii::app()->controller->module->activeAfterRegister) ? Cons::USER_STATUS_ACTIVE : Cons::USER_STATUS_NOT_ACTIVE);
                    }
//                    if ($isTestDomainUser) {
//                        $model->status = 1; // auto activate test users
//                    }

                    if ($model->save()) {
                        $profile->user_id = $model->id;
                        $profile->save();

                        // set invite token to used status
                        if ($invite) {
                            $invite->status = 0;
                            $invite->accepted = 1;
                            $invite->accepted_date = date('Y-m-d');
                            $invite->isNewRecord = false;
                            $invite->save();
                            Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Please login."));
                            $this->redirect(Yii::app()->controller->module->returnUrl);
                            return;
                        }

                        if (Yii::app()->controller->module->sendActivationMail && $model->status == Cons::USER_STATUS_NOT_ACTIVE) {
                            $activation_url = $this->createAbsoluteUrl('/user/activation/activation', array("activkey" => $model->activkey, "email" => $model->email));
                            UserModule::sendMail($model->email, UserModule::t("Confirm your {site_name} Registration", array('{site_name}' => Yii::app()->name)), UserModule::t("To activate your account, go to {activation_url}", array('{activation_url}' => $activation_url)));
                        }

                        if ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) {
                            $identity = new UserIdentity($model->username, $sourcePassword);
                            $identity->authenticate();
                            Yii::app()->user->login($identity, 0);
                            $this->redirect(Yii::app()->controller->module->returnUrl);
                        } else {
                            if (!Yii::app()->controller->module->activeAfterRegister && !Yii::app()->controller->module->sendActivationMail) {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Contact Admin to activate your account."));
                            } elseif (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false) {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Please {{login}}.", array('{{login}}' => CHtml::link(UserModule::t('Login'), Yii::app()->controller->module->loginUrl))));
                            } elseif (Yii::app()->controller->module->loginNotActiv) {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Please check your email or login."));
                                //Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Currently this site is invitation only."));
                            } else {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Please check your email."));
                                //Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Currently this site is invitation only."));
                            }
                            $this->refresh();
                        }
                    }
                } else $profile->validate();
            }
            $this->render('/user/registration', array('model' => $model, 'profile' => $profile));
        }
    }

}