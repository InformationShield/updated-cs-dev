<?php

class ActivationController extends Controller
{
	public $defaultAction = 'activation';

	
	/**
	 * Activation user account
	 */
	public function actionActivation () {
		$email = $_GET['email'];
		$activkey = $_GET['activkey'];
		if ($email&&$activkey) {
			$user = User::model()->notsafe()->findByAttributes(array('email'=>$email));
			if (isset($user)&&$user->status) {
				$str = "Your account is already active.";
				$this->redirect(url('user/login/login/msg/' . urlencode($str)));
			    //$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Your account is already active."), 'mode'=>'login'));
			} elseif(isset($user->activkey) && ($user->activkey==$activkey)) {
				$user->activkey = UserModule::encrypting(microtime());
				$user->status = 1;
				$user->save();
				$str = "Your account has been activated.";
				$this->redirect(url('user/login/login/msg/' . urlencode($str)));
                                
				//$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Your account has been activated."), 'mode'=>'login'));
			} else {
			    $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Invalid activation code.")));
			}
		} else {
			$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Invalid activation code.")));
		}
	}

}