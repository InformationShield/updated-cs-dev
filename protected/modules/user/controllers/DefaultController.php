<?php

class DefaultController extends GridViewController
{
    public $layout='//layouts/admin_column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return CMap::mergeArray(parent::filters(),array(
            'accessControl', // perform access control for CRUD operations
        ));
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index'),
                'expression'=>'!user()->isGuest && Utils::isSuper()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
	 * Lists all users.
	 */
	public function actionIndex()
	{
        $listUrl = url('user/user/get');
		$this->render('/user/index', array('listUrl' => $listUrl));
	}
}