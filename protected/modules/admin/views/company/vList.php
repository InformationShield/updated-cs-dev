<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'id', caption: 'recId', size: '30px', hidden: true, hideable: true, sortable: true, resizable: true},
		{ field: 'name', caption: 'Company', size: '100%', searchable: true, resizable: true, sortable: true,
			render: function (record, index, column_index) {
				return '<div><a class="lnkCompany" href="<?= url('admin/company/edit'); ?>/id/'+record.company_id+'">'+record.name+'</a></div>';
			}
		},
		{ field: 'website', caption: 'Website', size: '100%', searchable: true, resizable: true, sortable: true},
		{ field: 'license_level', caption: 'License', size: '70px', resizable: true, sortable: true},
		{ field: 'first_name', caption: 'First Name', size: '120px', searchable: true, resizable: true, sortable: true,
			render: function (record, index, column_index) {
				var firstName = record.first_name.length == 0 ? "&lt;impersonate&gt;" : record.first_name;
				return '<a href="<?php echo url('user/admin/impersonate/id'); ?>/' + record.user_id + '">' + firstName + '</a>';
			}
		},
		{ field: 'last_name', caption: 'Last Name', size: '120px', searchable: true, resizable: true, sortable: true,
			render: function(record, index, column_index) {
				var lastName = record.last_name.length == 0 ? "&lt;impersonate&gt;" : record.last_name;
				return '<a href="<?php echo url('user/admin/impersonate/id'); ?>/' + record.user_id + '">' + lastName + '</a>';
			}
		},
        { field: 'email', caption: 'Owner Email', size: '200px', searchable: true, resizable: true, sortable: true,
			render: function (record, index, column_index) {
				return '<div><a class="lnkUser" href="<?= url('user/user/edit'); ?>/id/'+record.user_id+'">'+record.email+'</a></div>';
			}
		},
        { field: 'status_desc', caption: 'Status', size: '60px', resizable: true, sortable: true,
			render: function (record, index, column_index) {
        		if (record.status != 1)
					return '<div class="text-danger">'+record.status_desc+'</div>';
        		else
					return '<div>'+record.status_desc+'</div>';
			}
		},
		{ field: 'trial_expiry', caption: 'Trial Expiry', size: '90px', searchable: 'date', resizable: true, sortable: true },
        { field: 'created_on', caption: 'Created On', size: '90px', searchable: 'date', resizable: true, sortable: true },
    ];

    var _ajaxListGridAddFunction = function(event) {};

</script>

<?
	$gmAjaxListGrid = new gmAjaxListGrid();
	$gmAjaxListGrid->listUrl = $listUrl;
	$gmAjaxListGrid->title = '<h2>Companies</h2>';
	$gmAjaxListGrid->intro = '<p>&bull; Click on the company to edit the company.<br>&bull; Click on the owner email to edit the owner.<br>&bull; Click on the owner name to impersonate the owner.</p>';
	$gmAjaxListGrid->controller = "admin/company";
	$gmAjaxListGrid->recid = "recid";
	$gmAjaxListGrid->allowAdd = false;
	$gmAjaxListGrid->allowEdit = true;
	$gmAjaxListGrid->allowDelete = true;
	$gmAjaxListGrid->allowCopy = false;
	$gmAjaxListGrid->reloadOnCopy = true;
	$gmAjaxListGrid->toolbarColumns = false;
	$gmAjaxListGrid->limit = 3000;
	$gmAjaxListGrid->showExportCSV = false;
	$gmAjaxListGrid->customSearch = array(
		array(
			'field' => 'name',
			'caption' => 'Company',
			'type' => 'text',
		),
		array(
			'field' => 'license_level',
			'caption' => 'License',
			'type' => 'list',
			'options' => array(
				'items' => mPermission::$Licenses,
			),
		),
		array(
			'field' => 'email',
			'caption' => 'Owner Email',
			'type' => 'text',
		),
		array(
			'field' => 'c.status',
			'caption' => 'Status',
			'type' => 'list',
			'options' => array(
				'items' => Cons::$ActiveDisabled,
//				'items' => array(
//					'0'=>'disabled',
//					'1'=>'active',
//				)
			),
		)
	);
	AjaxListGridGadget::show($gmAjaxListGrid);
?>