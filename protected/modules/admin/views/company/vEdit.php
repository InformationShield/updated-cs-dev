<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
        mode : "textareas",
        theme : "modern", menubar: false
    });
</script>

<h3>Edit Company</h3>

<div class="form">

<? $form=$this->beginWidget('CActiveForm',
	array('action'=>url('admin/company/edit',array('id'=>$model->company_id)),
	'id'=>'edit-form',
	'enableAjaxValidation'=>false,
)); ?>
	<input type="hidden" id="invitebyemail" name="invitebyemail" value="0">

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
        <?= $form->labelEx($model,'Name'); ?>
		<?= $form->textField($model,'name',array('style'=>"width: 500px;")); ?>
		<?= $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'Website'); ?>
		<?= $form->textField($model,'website',array('style'=>"width: 99%;")); ?>
		<?= $form->error($model,'website'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'Status'); ?>
		<select name="Company[status]" id="Company_status" style="width: 200px;">
			<option value="1" <?= ($model->status == 1) ? 'selected="selected"' : ''; ?>>active</option>
			<option value="0" <?= ($model->status == 0) ? 'selected="selected"' : ''; ?>>disabled</option>
		</select>
		<?= $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'License Level'); ?>
		<select name="Company[license_level]" id="Company_license_level" style="width: 200px;">
			<? foreach(mPermission::$Licenses as $licenseLevel=>$licenseDesc) { ?>
				<option value="<?= $licenseLevel; ?>" <?= ($model->license_level == $licenseLevel) ? 'selected="selected"' : ''; ?>><?= $licenseDesc; ?></option>
			<? } ?>
		</select>
		<?= $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'Trial Expiry <small>(only effective if the license is set to Trial license level)</small>'); ?>
		<input type="us-date" name="Company[trial_expiry]" style="width: 100px;"
			value="<?= DateHelper::FormatDateTimeString($model->trial_expiry, 'm/d/Y'); ?>">
		<?= $form->error($model,'trial_expiry'); ?>
	</div>

	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

	<? $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$(function(){
		$('input[type=us-date]').w2field('date');
		$("#btnSave").click(function(){
			$('#edit-form').submit();
			return false;
		});
		$("#btnCancel").click(function(){
			window.location = "<?= url('admin/company'); ?>";
			return false;
		});
	});
</script>