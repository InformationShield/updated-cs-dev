<?php
/* @var $this ControlCategoryController */
/* @var $model ControlCategory */

$this->breadcrumbs=array(
	'Control Categories'=>array('admin'),
	'Create',
);

$this->menu=array(
		array('label'=>'Manage', 'url'=>array('admin')),
);
?>

<h2>Create Control Category</h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>