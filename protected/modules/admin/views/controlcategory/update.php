<?php
/* @var $this ControlCategoryController */
/* @var $model ControlCategory */

$this->breadcrumbs=array(
	'Control Categories'=>array('admin'),
	$model->control_category_id=>array('view','id'=>$model->control_category_id),
	'Update',
);
$this->menu=array(
		array('label'=>'Manage', 'url'=>array('admin')),
		array('label'=>'Create', 'url'=>array('create')),
		array('label'=>'View', 'url'=>array('view', 'id'=>$model->control_category_id)),
);
//$this->menu=array(
//	array('label'=>'List ControlCategory', 'url'=>array('index')),
//	array('label'=>'Create ControlCategory', 'url'=>array('create')),
//	array('label'=>'View ControlCategory', 'url'=>array('view', 'id'=>$model->control_category_id)),
//	array('label'=>'Manage ControlCategory', 'url'=>array('admin')),
//);
?>

<h2>Update Control Category #<?php echo $model->control_category_id; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>