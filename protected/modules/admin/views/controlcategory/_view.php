<?php
/* @var $this ControlCategoryController */
/* @var $data ControlCategory */
?>

<div class="x_panel">

	<b><?php echo CHtml::encode($data->getAttributeLabel('control_category_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->control_category_id), array('view', 'id'=>$data->control_category_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent_id')); ?>:</b>
	<?php echo CHtml::encode($data->parent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category')); ?>:</b>
	<?php echo CHtml::encode($data->category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />


</div>