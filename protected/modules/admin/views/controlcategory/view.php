<?php
/* @var $this ControlCategoryController */
/* @var $model ControlCategory */

$this->breadcrumbs=array(
	'Control Categories'=>array('admin'),
	$model->control_category_id,
);
$this->menu=array(
		array('label'=>'Manage', 'url'=>array('admin')),
		array('label'=>'Create', 'url'=>array('create')),
		array('label'=>'Update', 'url'=>array('update', 'id'=>$model->control_category_id)),
		array('label'=>'Delete', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->control_category_id),'confirm'=>'Are you sure you want to delete this item?')),
);
//$this->menu=array(
//	array('label'=>'List ControlCategory', 'url'=>array('index')),
//	array('label'=>'Create ControlCategory', 'url'=>array('create')),
//	array('label'=>'Update ControlCategory', 'url'=>array('update', 'id'=>$model->control_category_id)),
//	array('label'=>'Delete ControlCategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->control_category_id),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage ControlCategory', 'url'=>array('admin')),
//);
?>

<h2>View Control Category #<?php echo $model->control_category_id; ?></h2>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'control_category_id',
		'parent_id',
		'category',
		'description',
	),
)); ?>
