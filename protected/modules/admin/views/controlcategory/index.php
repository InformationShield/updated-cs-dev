<?php
/* @var $this ControlCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Control Categories',
);

$this->menu=array(
		array('label'=>'Manage', 'url'=>array('admin')),
		array('label'=>'Create', 'url'=>array('create')),
);
?>

<h2>Control Categories</h2>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
