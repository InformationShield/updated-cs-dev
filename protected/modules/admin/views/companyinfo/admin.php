<?php
/* @var $this CompanyinfoController */
/* @var $model CompanyInfo */

$this->breadcrumbs=array(
	'Company Infos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Manage', 'url'=>array('admin')),
	//array('label'=>'Create', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#company-info-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Company Info</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'company-info-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'company_info_id',
		'company_id',
		'user_id',
		'updated_on',
		'created_on',
		'business_name',
		/*
		'num_employees',
		'address',
		'city',
		'state',
		'country',
		'publicly_traded',
		'primary_business_type',
		'critical_infrastructure',
		'security_maturity',
		'highly_sensitive_data',
		'individual_consumer_data',
		'credit_card_data',
		'individual_health_data',
		'international_personal_data',
		'num_employees_access_sensitive_data',
		'employee_mobile_customer_data',
		'remote_network_access',
		'local_consumer_datastore',
		'third_party_data',
		'software_development',
		'online_customer_transactions',
		'primary_work_location',
		'local_it_datacenters',
		'sensitive_paper_records',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
