<?php
/* @var $this CompanyinfoController */
/* @var $data CompanyInfo */
?>

<div class="x_panel">

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_info_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->company_info_id), array('view', 'id'=>$data->company_info_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_id')); ?>:</b>
	<?php echo CHtml::encode($data->company_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_on')); ?>:</b>
	<?php echo CHtml::encode($data->updated_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_on')); ?>:</b>
	<?php echo CHtml::encode($data->created_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_name')); ?>:</b>
	<?php echo CHtml::encode($data->business_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num_employees')); ?>:</b>
	<?php echo CHtml::encode($data->num_employees); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state')); ?>:</b>
	<?php echo CHtml::encode($data->state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
	<?php echo CHtml::encode($data->country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primary_business_type')); ?>:</b>
	<?php echo CHtml::encode($data->primary_business_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('security_maturity')); ?>:</b>
	<?php echo CHtml::encode($data->security_maturity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('highly_sensitive_data')); ?>:</b>
	<?php echo CHtml::encode($data->highly_sensitive_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('individual_consumer_data')); ?>:</b>
	<?php echo CHtml::encode($data->individual_consumer_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('credit_card_data')); ?>:</b>
	<?php echo CHtml::encode($data->credit_card_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('individual_health_data')); ?>:</b>
	<?php echo CHtml::encode($data->individual_health_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('international_personal_data')); ?>:</b>
	<?php echo CHtml::encode($data->international_personal_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num_employees_access_sensitive_data')); ?>:</b>
	<?php echo CHtml::encode($data->num_employees_access_sensitive_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('employee_mobile_customer_data')); ?>:</b>
	<?php echo CHtml::encode($data->employee_mobile_customer_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remote_network_access')); ?>:</b>
	<?php echo CHtml::encode($data->remote_network_access); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('local_consumer_datastore')); ?>:</b>
	<?php echo CHtml::encode($data->local_consumer_datastore); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('third_party_data')); ?>:</b>
	<?php echo CHtml::encode($data->third_party_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('software_development')); ?>:</b>
	<?php echo CHtml::encode($data->software_development); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('online_customer_transactions')); ?>:</b>
	<?php echo CHtml::encode($data->online_customer_transactions); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primary_work_location')); ?>:</b>
	<?php echo CHtml::encode($data->primary_work_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('local_it_datacenters')); ?>:</b>
	<?php echo CHtml::encode($data->local_it_datacenters); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sensitive_paper_records')); ?>:</b>
	<?php echo CHtml::encode($data->sensitive_paper_records); ?>
	<br />

</div>