<?php
/* @var $this CompanyinfoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Company Info',
);

$this->menu=array(
	array('label'=>'Manage', 'url'=>array('admin')),
	//array('label'=>'Create', 'url'=>array('create')),
);
?>

<h1>Company Infos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
