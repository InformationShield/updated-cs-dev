<?php
/* @var $this CompanyinfoController */
/* @var $model CompanyInfo */

$this->breadcrumbs=array(
	'Company Infos'=>array('index'),
	$model->company_info_id=>array('view','id'=>$model->company_info_id),
	'Update',
);

$this->menu=array(
	array('label'=>'Manage', 'url'=>array('admin')),
	//array('label'=>'Create', 'url'=>array('create')),
	array('label'=>'View', 'url'=>array('view', 'id'=>$model->company_info_id)),
);
?>

<h1>Update CompanyInfo <?php echo $model->company_info_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>