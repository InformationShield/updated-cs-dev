<?php
/* @var $this CompanyinfoController */
/* @var $model CompanyInfo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'company-info-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'company_id'); ?>
		<?php echo $form->textField($model,'company_id'); ?>
		<?php echo $form->error($model,'company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_on'); ?>
		<?php echo $form->textField($model,'updated_on'); ?>
		<?php echo $form->error($model,'updated_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_on'); ?>
		<?php echo $form->textField($model,'created_on'); ?>
		<?php echo $form->error($model,'created_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'business_name'); ?>
		<?php echo $form->textField($model,'business_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'business_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'num_employees'); ?>
		<?php echo $form->textField($model,'num_employees',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'num_employees'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php echo $form->textField($model,'country',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'primary_business_type'); ?>
		<?php echo $form->textField($model,'primary_business_type',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'primary_business_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'security_maturity'); ?>
		<?php echo $form->textField($model,'security_maturity'); ?>
		<?php echo $form->error($model,'security_maturity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'highly_sensitive_data'); ?>
		<?php echo $form->textField($model,'highly_sensitive_data'); ?>
		<?php echo $form->error($model,'highly_sensitive_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'individual_consumer_data'); ?>
		<?php echo $form->textField($model,'individual_consumer_data'); ?>
		<?php echo $form->error($model,'individual_consumer_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'credit_card_data'); ?>
		<?php echo $form->textField($model,'credit_card_data'); ?>
		<?php echo $form->error($model,'credit_card_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'individual_health_data'); ?>
		<?php echo $form->textField($model,'individual_health_data'); ?>
		<?php echo $form->error($model,'individual_health_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'employee_mobile_customer_data'); ?>
		<?php echo $form->textField($model,'employee_mobile_customer_data'); ?>
		<?php echo $form->error($model,'employee_mobile_customer_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remote_network_access'); ?>
		<?php echo $form->textField($model,'remote_network_access'); ?>
		<?php echo $form->error($model,'remote_network_access'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'third_party_data'); ?>
		<?php echo $form->textField($model,'third_party_data'); ?>
		<?php echo $form->error($model,'third_party_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'online_customer_transactions'); ?>
		<?php echo $form->textField($model,'online_customer_transactions'); ?>
		<?php echo $form->error($model,'online_customer_transactions'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'local_it_datacenters'); ?>
		<?php echo $form->textField($model,'local_it_datacenters'); ?>
		<?php echo $form->error($model,'local_it_datacenters'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->