<?php
/* @var $this CompanyinfoController */
/* @var $model CompanyInfo */

$this->breadcrumbs=array(
	'Company Infos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage', 'url'=>array('admin')),
	//array('label'=>'Create', 'url'=>array('create')),
);
?>

<h1>Create CompanyInfo</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>