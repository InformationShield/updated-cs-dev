<?php
/* @var $this CompanyinfoController */
/* @var $model CompanyInfo */

$this->breadcrumbs=array(
	'Company Info'=>array('index'),
	$model->company_info_id,
);

$this->menu=array(
	array('label'=>'Manage', 'url'=>array('admin')),
	//array('label'=>'Create', 'url'=>array('create')),
	array('label'=>'Update', 'url'=>array('update', 'id'=>$model->company_info_id)),
	//array('label'=>'Delete', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->company_info_id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View CompanyInfo #<?php echo $model->company_info_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'company_info_id',
		'company_id',
		'user_id',
		'updated_on',
		'created_on',
		'business_name',
		'num_employees',
		'address',
		'city',
		'state',
		'country',
		'primary_business_type',
		'security_maturity',
		'highly_sensitive_data',
		'individual_consumer_data',
		'credit_card_data',
		'individual_health_data',
		'international_personal_data',
		'num_employees_access_sensitive_data',
		'employee_mobile_customer_data',
		'remote_network_access',
		'local_consumer_datastore',
		'third_party_data',
		'software_development',
		'online_customer_transactions',
		'primary_work_location',
		'local_it_datacenters',
		'sensitive_paper_records',
	),
)); ?>
