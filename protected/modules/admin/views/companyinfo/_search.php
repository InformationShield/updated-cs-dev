<?php
/* @var $this CompanyinfoController */
/* @var $model CompanyInfo */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'company_info_id'); ?>
		<?php echo $form->textField($model,'company_info_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'company_id'); ?>
		<?php echo $form->textField($model,'company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_on'); ?>
		<?php echo $form->textField($model,'updated_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_on'); ?>
		<?php echo $form->textField($model,'created_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'business_name'); ?>
		<?php echo $form->textField($model,'business_name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'num_employees'); ?>
		<?php echo $form->textField($model,'num_employees',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'city'); ?>
		<?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'country'); ?>
		<?php echo $form->textField($model,'country',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'primary_business_type'); ?>
		<?php echo $form->textField($model,'primary_business_type',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'security_maturity'); ?>
		<?php echo $form->textField($model,'security_maturity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'highly_sensitive_data'); ?>
		<?php echo $form->textField($model,'highly_sensitive_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'individual_consumer_data'); ?>
		<?php echo $form->textField($model,'individual_consumer_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'credit_card_data'); ?>
		<?php echo $form->textField($model,'credit_card_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'individual_health_data'); ?>
		<?php echo $form->textField($model,'individual_health_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'employee_mobile_customer_data'); ?>
		<?php echo $form->textField($model,'employee_mobile_customer_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'remote_network_access'); ?>
		<?php echo $form->textField($model,'remote_network_access'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'third_party_data'); ?>
		<?php echo $form->textField($model,'third_party_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'online_customer_transactions'); ?>
		<?php echo $form->textField($model,'online_customer_transactions'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'local_it_datacenters'); ?>
		<?php echo $form->textField($model,'local_it_datacenters'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->