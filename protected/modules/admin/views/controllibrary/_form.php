<?php
/* @var $this ControlLibraryController */
/* @var $model ControlLibrary */
/* @var $form CActiveForm */
?>
<script type="text/javascript" src="<?= url("3rdparty/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>
<script type="text/javascript">
	tinymce.init({
		mode : "textareas",
		theme : "modern", menubar: false
	});
</script>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'control-library-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'control_id'); ?>
		<?php echo $form->textField($model,'control_id'); ?>
		<?php echo $form->error($model,'control_id'); ?>
	</div>

    <div class="row">
        <label for="ControlLibrary_cat_id" class="required">Category <span class="required">*</span></label>
        <select name="ControlLibrary[cat_id]" id="ControlLibrary_cat_id" style="width: 320px;">
            <? $cats = ControlCategory_Ex::getCategory(); ?>
            <? foreach ($cats as $cat) { ?>
                <option value="<?php echo $cat['control_category_id']; ?>" <?php echo ($model->cat_id == $cat['control_category_id']) ? 'selected="selected"' : ''; ?>><?php echo $cat['category']; ?></option>
            <? } ?>
        </select>
        <?php echo $form->error($model,'cat_id'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'cat_code'); ?>
		<?php echo $form->textField($model,'cat_code',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'cat_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cat_sub_code'); ?>
		<?php echo $form->textField($model,'cat_sub_code',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'cat_sub_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sort_order'); ?>
		<?php echo $form->textField($model,'sort_order',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'sort_order'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'priority'); ?>
        <select name="ControlLibrary[priority]" id="ControlLibrary_priority">
            <option value="1"  <?php echo ($model->priority=='1') ? 'selected="selected"' :'';?>>1</option>
            <option value="2"  <?php echo ($model->priority=='2') ? 'selected="selected"' :'';?>>2</option>
            <option value="3"  <?php echo ($model->priority=='3') ? 'selected="selected"' :'';?>>3</option>
        </select>
        <?php echo $form->error($model,'priority'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'weighting'); ?>
		<?php echo $form->textField($model,'weighting'); ?>
		<?php echo $form->error($model,'weighting'); ?>
	</div>

<?/*
	<div class="row">
		<?php echo $form->labelEx($model,'created_on'); ?>
		<?php echo $form->textField($model,'created_on'); ?>
		<?php echo $form->error($model,'created_on'); ?>
	</div>
 */ ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'style'=>'width: 99%;')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jobrole'); ?>
		<?php echo $form->textField($model,'jobrole',array('size'=>30,'maxlength'=>30,'style'=>'width: 50%;')); ?>
		<?php echo $form->error($model,'jobrole'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'score'); ?>
		<?php echo $form->textField($model,'score',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'score'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'detail'); ?>
        <?php echo $form->textArea($model,'detail',array('rows'=>'5','style'=>"width: 99%;")); ?>
        <?php echo $form->error($model,'detail'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'rel_policy'); ?>
        <?php echo $form->textArea($model,'rel_policy',array('rows'=>'5','style'=>"width: 99%;")); ?>
        <?php echo $form->error($model,'rel_policy'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'guidance'); ?>
        <?php echo $form->textArea($model,'guidance',array('rows'=>'10','style'=>"width: 99%;")); ?>
        <?php echo $form->error($model,'guidance'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'evidence'); ?>
        <?php echo $form->textArea($model,'evidence',array('rows'=>'5','style'=>"width: 99%;")); ?>
        <?php echo $form->error($model,'evidence'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'reference'); ?>
        <?php echo $form->textArea($model,'reference',array('rows'=>'10','style'=>"width: 99%;")); ?>
        <?php echo $form->error($model,'reference'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'question'); ?>
        <?php echo $form->textArea($model,'question',array('rows'=>'5','style'=>"width: 99%;")); ?>
        <?php echo $form->error($model,'question'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->