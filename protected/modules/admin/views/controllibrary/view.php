<?php
/* @var $this ControlLibraryController */
/* @var $model ControlLibrary */

$this->breadcrumbs=array(
	'Control Libraries'=>array('admin'),
	$model->title,
);
$this->menu=array(
		array('label'=>'Manage', 'url'=>array('admin')),
		array('label'=>'Create', 'url'=>array('create')),
		array('label'=>'Update', 'url'=>array('update', 'id'=>$model->control_library_id)),
		array('label'=>'Delete', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->control_library_id),'confirm'=>'Are you sure you want to delete this item?')),
);
//$this->menu=array(
//	array('label'=>'List ControlLibrary', 'url'=>array('index')),
//	array('label'=>'Create ControlLibrary', 'url'=>array('create')),
//	array('label'=>'Update ControlLibrary', 'url'=>array('update', 'id'=>$model->control_library_id)),
//	array('label'=>'Delete ControlLibrary', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->control_library_id),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage ControlLibrary', 'url'=>array('admin')),
//);
?>

<h2>View Control Library #<?php echo $model->control_library_id; ?></h2>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'control_library_id',
		'control_id',
		'cat_id',
		'cat_code',
		'cat_sub_code',
		'sort_order',
		'priority',
		'weighting',
		'created_on',
		'title',
		'status',
		'jobrole',
		'score',
		'evidence',
		'rel_policy',
		'detail',
		'guidance',
		'reference',
		'question',
	),
)); ?>
