<?php
/* @var $this ControlLibraryController */
/* @var $data ControlLibrary */
?>

<div class="x_panel">

	<b><?php echo CHtml::encode($data->getAttributeLabel('control_library_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->control_library_id), array('view', 'id'=>$data->control_library_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('control_id')); ?>:</b>
	<?php echo CHtml::encode($data->control_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cat_id')); ?>:</b>
	<?php echo CHtml::encode($data->cat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cat_code')); ?>:</b>
	<?php echo CHtml::encode($data->cat_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cat_sub_code')); ?>:</b>
	<?php echo CHtml::encode($data->cat_sub_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sort_order')); ?>:</b>
	<?php echo CHtml::encode($data->sort_order); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priority')); ?>:</b>
	<?php echo CHtml::encode($data->priority); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weighting')); ?>:</b>
	<?php echo CHtml::encode($data->weighting); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_on')); ?>:</b>
	<?php echo CHtml::encode($data->created_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jobrole')); ?>:</b>
	<?php echo CHtml::encode($data->jobrole); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('score')); ?>:</b>
	<?php echo CHtml::encode($data->score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('evidence')); ?>:</b>
	<?php echo CHtml::encode($data->evidence); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rel_policy')); ?>:</b>
	<?php echo CHtml::encode($data->rel_policy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('detail')); ?>:</b>
	<?php echo CHtml::encode($data->detail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guidance')); ?>:</b>
	<?php echo CHtml::encode($data->guidance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reference')); ?>:</b>
	<?php echo CHtml::encode($data->reference); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('question')); ?>:</b>
	<?php echo CHtml::encode($data->question); ?>
	<br />

</div>