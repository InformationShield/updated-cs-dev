<?php
/* @var $this ControlLibraryController */
/* @var $model ControlLibrary */

$this->breadcrumbs=array(
	'Control Libraries'=>array('admin'),
	$model->title=>array('view','id'=>$model->control_library_id),
	'Update',
);
$this->menu=array(
		array('label'=>'Manage', 'url'=>array('admin')),
		array('label'=>'Create', 'url'=>array('create')),
		array('label'=>'View', 'url'=>array('view', 'id'=>$model->control_library_id)),
);
?>

<h2>Update Control Library #<?php echo $model->control_library_id; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>