<?php
/* @var $this ControlLibraryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Control Libraries',
);

$this->menu=array(
	array('label'=>'Manage', 'url'=>array('admin')),
	array('label'=>'Create', 'url'=>array('create')),
);
?>

<h2>Control Libraries</h2>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
