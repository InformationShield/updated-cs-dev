<?php
/* @var $this ControlLibraryController */
/* @var $model ControlLibrary */

$this->breadcrumbs=array(
	'Control Libraries'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage', 'url'=>array('admin')),
	array('label'=>'Create', 'url'=>array('create')),
);
?>

<h2>Create Control Library</h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>