<script type="text/javascript">
    var _ajaxListGridColumns = [
        { field: 'message_id', caption: 'id', size: '0px', hidden: true, sortable: false,  resizable: false },
        { field: 'recipient_name', caption: 'Recipient ', size: '200px', resizable: true, sortable: true,
            render: function(record, index, column_index){
                var html = '<div>';
                if (record.recipient_name == " "){
                    html += record.recipient_email;
                } else {
                    html += record.recipient_name;
                }
                html += '</div>';
                return html;
            }
        },
        { field: 'title', caption: 'Title', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'message', caption: 'Message', size: '100%', searchable: 'text', resizable: true, sortable: true },
        { field: 'read', caption: 'Status', size: '50px', resizable: true, sortable: false,
            render: function(record, index, column_index){
                var html = '';
                if(record.read == "0"){
                    html = '<i class="fa fa-envelope-o" style="font-size: 20px; margin-left: 10px;"></i>';
                }else if(record.read == "1"){
                    html = '<i class="fa fa-envelope-open-o" style="font-size: 20px; margin-left: 10px;"></i>';
                }
                return html;
            }
        },
        { field: 'status', caption: 'Deleted', size: '60px', resizable: true, sortable: false,
            render: function(record, index, column_index){
                var html = '';
                if(record.status == "0"){
                    html = '<i class="fa fa-check" style="font-size: 20px; margin-left: 15px;"></i>';
                }
                return html;
            }
        },
        { field: 'created_on', caption: 'Created On', size: '100px', resizable: true, sortable: false},
        { field: 'action', caption: 'Actions', size: '100px', hideable: false, sortable: false,  resizable: false,
                render: function (record, index, column_index) {
                    console.log(record);
                    var html = '<div>';
                    html += '<a class="lnkView" href="" data-recid="'+record.recid+'">View</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkEdit" href="<?= url('admin/postmessage/edit'); ?>/id/'+record.recid+'">Edit</a>'
                        +'<span>&nbsp;</span>'
                        +'<a class="lnkDelete" href="" data-recid="'+record.recid+'">Delete</a>';
                    html += '</div>';
                    return html;
                }
            },
    ];
    var _ajaxListGridAddFunction = false;

</script>
<?
    $gmAjaxListGrid = new gmAjaxListGrid();
    $gmAjaxListGrid->title = "<h2>Manage Messages</h2>";
    $gmAjaxListGrid->intro = '<p></p>';
    $gmAjaxListGrid->controller = "admin/postmessage";
    $gmAjaxListGrid->recid = "message_id";
    $gmAjaxListGrid->allowAdd = true;
    $gmAjaxListGrid->allowEdit = true;
    $gmAjaxListGrid->allowDelete = true;
    $gmAjaxListGrid->showViewDelete = true;
    $gmAjaxListGrid->allowCopy = false;
    $gmAjaxListGrid->reloadOnCopy = true;
    AjaxListGridGadget::show($gmAjaxListGrid);
?>