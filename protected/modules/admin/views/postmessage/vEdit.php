<?php
/* @var $this ControlCategoryController */
/* @var $model ControlCategory */

$this->breadcrumbs=array(
	'Control Message'=>array('admin'),
	$model->message_id=>array('view','id'=>$model->message_id),
	'Edit',
);
$this->menu=array(
		array('label'=>'Manage', 'url'=>array('admin')),
		array('label'=>'Create', 'url'=>array('create')),
		array('label'=>'View', 'url'=>array('view', 'id'=>$model->message_id)),
);
?>

<h2>Edit Control Message #<?php echo $model->message_id; ?></h2>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'control-message-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<div class="row">
		<label for="Message_recipient_id" class="required">Company Owner - Company Name: <span class="required">*</span></label>
		<? if(isset($list_company_owner) && count($list_company_owner) > 0) { ?>
			<select name="Message[recipient_id]" id="recipient_id">
                <? if ($new_message) { ?>
                    <option value=""></option>
                    <option value="0">All <?= count($list_company_owner); ?> Company Owners</option>
                <? } ?>
				<?php foreach ($list_company_owner as $owner) { ?>
					<option value="<?= $owner['user_id'] ?>"
						<?= $model->recipient_id ==  $owner['user_id'] ? 'selected' :  ''; ?>
						><?= $owner['firstname'].' '.$owner['lastname'].' - '.$owner['company_name']; ?></option>
				<?php } ?>
			</select>
		<? } ?>
		<?=  $form->error($model,'recipient_id'); ?>
	</div>
	<div class="row">
		<?= $form->hiddenField($model,'sender_id',array('value'=> Yii::app()->user->id)); ?>
		<?= $form->hiddenField($model,'parent_id'); ?>
	</div>
	<div class="row">
		<?= $form->labelEx($model,'title'); ?>
		<?= $form->textField($model,'title',array('maxlength'=>256,'style'=>'width: 99%;')); ?>
		<?= $form->error($model,'title'); ?>
	</div>
	<div class="row">
		<?= $form->labelEx($model,'message'); ?>
		<?= $form->textArea($model,'message',array('style'=>'width: 99%;')); ?>
		<?= $form->error($model,'message'); ?>
	</div>
	<div class="row buttons">
		<?= CHtml::button('Save',array('id'=>"btnSave",'class'=>"btn btn-success")); ?>
		<?= CHtml::button('Cancel',array('id'=>"btnCancel",'class'=>"btn btn-primary")); ?>
	</div>

<? $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
	$(function(){
	    $("#btnSave").click(function(){
	            $('#control-message-form').submit();
	            return false;
	    });
	    $("#btnCancel").click(function(){
	            window.location = "<?= url('admin/postmessage'); ?>";
	            return false;
	    });
	});
</script>