<?php

class PostmessageController extends GridViewController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin_column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{

		return array(
			'accessControl', // perform access control for CRUD operations
            'postOnly + get,delete', // we only allow deletion via POST request
            'ajaxOnly + get,view,delete',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(''),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','list','get','view','edit','delete'),
				//'users'=>array('admin'),
				'expression'=>'!user()->isGuest && Utils::isSuper()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionIndex()
    {
        $this->actionList();
    }

    public function actionList()
    {
        $this->render('vList');
    }

    public function actionGet()
    {
        $s = $this->getSearchParams();
        $json = Message_Ex::getMessagesJson(null,null,$s['pageSize'],$s['offsetBy'],$s['search'],$s['searchLogic'],$s['sort'],true);
        echo $json;
    }

    // overriden from base and called by $this->renderView() to get values and skipEncode for View feature
    protected function getViewValues($id,&$skipEncode)
    {
        $values = Message_Ex::getAdminViewValues($id);
        $skipEncode = array('Title','Message');
        return $values;
    }

    public function actionView()
    {
        // since only a superUser admin can get to this action, levels can be ignored
        $this->renderView(Cons::LIC_FREE,Cons::ROLE_AUDITOR);
    }

	public function actionEdit()
	{
        $id = $this->paramObscureId(false);
        $new_message = true;
        if (isset($id) && $id > 0) {
            $new_message = false;
        	$model = Message::model()->findByPk($id);
        }else{
        	$model = new Message;
        }

		if (isset($_POST['Message'])) {
            if ($_POST['Message']['recipient_id'] === '0') { // handle All owner case
//                $companyOwners = Employee::model()->findAllByAttributes(array('company_role_id' => Cons::ROLE_OWNER));
				$companyOwners = Company_Ex::getAllCompanyOwners();
                $parentId = null;
                if (isset($companyOwners) && count($companyOwners) > 0) {
                    foreach ($companyOwners as $i=>$owner) {
                        $model = new Message;
                        $model->attributes = $_POST['Message'];
                        $model->parent_id = $parentId;
                        $model->recipient_id = $owner['user_id'];
                        if (!$model->save()) {
                            Yii::log(print_r($model->getErrors(), true), 'error', 'PostmessageController.actionEdit');
                        }
                        if ($i == 0) {
                        	$parentId = $model->message_id;
                        	$model->isNewRecord = false;
                        	$model->parent_id = $parentId;
							if (!$model->save()) {
								Yii::log(print_r($model->getErrors(), true), 'error', 'PostmessageController.actionEdit');
							}
						}
                    }
                }
                $this->redirect(array('index'));
            } else {
                $model->attributes = $_POST['Message'];
                if ($model->parent_id) {
					$messages = Message::model()->findAllByAttributes(array('parent_id' => $model->parent_id));
					foreach ($messages as $message) {
						$message->isNewRecord = false;
						$message->title = $model->title;
						$message->message = $model->message;
						$message->read = 0;
						$message->attributes = $model->attributes;
						if (!$message->save()) {
							Yii::log(print_r($model->getErrors(), true), 'error', 'PostmessageController.actionEdit');
						}
					}
					$this->redirect(array('index'));
				} else {
					if ($model->save()) {
						$this->redirect(array('index'));
					} else {
						Yii::log(print_r($model->getErrors(), true), 'error', 'PostmessageController.actionEdit');
					}
				}
            }
		}
//		$companyOwners = Employee::model()->findAllByAttributes(array('company_role_id' => Cons::ROLE_OWNER));
		$companyOwners = Company_Ex::getAllCompanyOwners();
		$this->render('vEdit',array(
			'model'=>$model,
            'new_message' => $new_message,
			'list_company_owner' => $companyOwners,
		));
	}

    // override method called by ajaxDelete()
    protected function doDelete($id, $markStatusOnly=false)
    {
		$id = $this->paramObscureId(false);
		$message = Message::model()->findByPk($id);
		if ($message) {
			Message_Ex::deleteMessages($id, $message->parent_id, $markStatusOnly);
			$retVal = mGrid::toGridMessage();
		} else {
			$retVal = mGrid::toGridMessage('error', 'Failed to delete record, due to errors.');
		}
		return $retVal;
    }

    public function actionDelete()
    {
		$id = $this->paramObscureId(); // will exit if bad param for security reasons
		echo $this->doDelete($id, false);
    }
}
