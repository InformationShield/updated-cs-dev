<?php

class CompanyController extends GridViewController
{
    public $layout='//layouts/admin_column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + get', // we only allow deletion via POST request
            'ajaxOnly + get, view',
        );
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
            return array(
                array('allow',
                        'actions'=>array('index', 'list', 'get', 'view', 'edit'),
                        'expression'=>'!user()->isGuest && Utils::isSuper()',
                ),
                array('deny',  // deny all users
                        'users'=>array('*'),
                ),
            );
    }

    public function actionIndex()
    {
        $this->actionList();
    }
    
    public function actionList()
    {
        $listUrl = url('admin/company/get');
		$this->render('/company/vList',  array('listUrl' => $listUrl));
    }

    protected function getViewValues($id, &$skipEncode) 
    {
        $values = AdminEmployee_Ex::getEmployeeViewValues($id);
        $skipEncode = array();
        return $values;
    }

    public function actionView()
    {
        $this->renderView();
    }

    public function actionGet()
    {
        $filter = $this->getSearchParams();
		$companies = Company_Ex::getAllCompanies($filter);
		echo mGrid::toJsonGrid($companies);
		// $search['pageSize'], $search['offsetBy'], $search['search'], $search['searchLogic'], $search['sort']);
        //echo $json;
    }

    public function actionEdit()
    {
		$id = getGet('id');
		$model = Company::model()->findByPk($id);

		if ($model) {
			if (isset($_POST['Company'])) {
				$model->attributes=$_POST['Company'];
				$model->trial_expiry = DateHelper::CstToUtc($_POST['Company']['trial_expiry'], 'Y-m-d');
				if ($model->validate()) {
					if ($model->save()) {
						$this->redirect(url('admin/company'));
					} else {
						Yii::log(print_r($model->getErrors(), true), 'error', 'CompanyController.actionEdit');
					}
				}
			}
		} else {
			setFlashMessage('Company does not exist.');
			$this->redirect(url('admin/company'));
		}


		$this->render('vEdit',array(
                    'model'=>$model
            ));
    }
    
}
