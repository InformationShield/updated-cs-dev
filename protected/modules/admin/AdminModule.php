<?php

require_once(Yii::app()->basePath.'/appinclude/Helper.php');

/** Admin module to hold all the admin functions created with Gii CRUD **/
class AdminModule extends CWebModule
{
	public function init()
	{
        // add init code here down the road if needed
		$this->setImport(array(
			'admin.models_ex.*',
		));

	}
	
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
            // add code here (like setting layout) down the road if needed
			return true;
		else
			return false;
	}
}
