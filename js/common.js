var debugLevel = 3; // 1=production, 2=development, 3=debug
function logError(error) {
    if (debugLevel > 1) {
        console.debug(error);
    }
    if (debugLevel > 2) {
        alert(error);
    }
}

function showLoader() {
    var imageUrl = BASEURL + '/images/ajax-loader.gif';
    $.blockUI({ message: '<img src="' + imageUrl + '" />' });
}

function hideLoader() {
    $.unblockUI();
}

// For debugging.  Pass object to print.  Pass any value into 2nd optional param to show object in a dialog box as well.
function vardump(varToDump, showAlert) {
    if (typeof varToDump === 'object') {
        for (var i in varToDump) {
            if (typeof varToDump[i] === 'object') {
                out += i + ": " + "\n";
                for (var j in varToDump[i]) {
                    if (typeof varToDump[i][j] === 'object') {
                        out += j + ": " + "\n";
                        for (var k in varToDump[i][j]) {
                            out += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + k + ": " + varToDump[i][j][k] + "\n";
                        }
                    } else {
                        out += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + j + ": " + varToDump[i][j] + "\n";
                    }
                }
            } else {
                out += i + ": " + varToDump[i] + "\n";
            }
        }
    } else {
        out = varToDump;
    }

    if (arguments.length == 2)
        alert(out);

    var pre = document.createElement('pre');
    pre.innerHTML = out;
    document.body.appendChild(pre)
}

function isNullOrUndefined(aObject) {
    return (typeof aObject === 'undefined' || aObject === null);
}

function isNullUndefinedOrEmpty(aObject) {
    return (typeof aObject === 'undefined' || aObject === null || aObject == '');
}

function isBlankOrZero(value) {
    return (value === null || $.trim(value) == '' || value == 0);
}

function checkInputRequired(aObject, fieldName) {
    var value = aObject.val();
    var field = aObject.attr('id');

    if (isBlankOrZero(value)) {
        msgBox(fieldName + " is required.", 'Required Input');
        aObject.focus();
        return false;
    } else {
        return true;
    }
}

function url(urlPath) {
    return BASEURL+'/'+urlPath;
}

function setCookie(name,value,days,hours,minutes) {
    /* note: if days=0 it is a session only cookie,
     * thus typically want a day value up to 365, or -1 to expire cookie */
    if (days === undefined || days === null) {
        alert('No expiration days set on cookie "'+name+'". Please provide.');
    }
    hours = (hours) ? hours : 0;
    minutes = (minutes) ? minutes : 0;
    var expires = ""
    if (days != 0 || hours != 0 || minutes != 0) {
        var date = new Date();
        date.setTime(date.getTime()+((days*24*60*60*1000)+(hours*60*60*1000)+(minutes*60*1000)));
        expires = "; expires="+date.toGMTString();
    }
    document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name) {
    var nameEQ;
    nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function deleteCookie(name) {
    saveCookie(name,"",-1,0,0);
}

(function($) { // literal function, created and executed immediately
    $(function($) {	// means on Document Ready

    });
})(jQuery); // jQuery passed in as $

// function that serializes form so that it can be posted via ajax
(function($){
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };

    $.fn.isNullOrUndefined = function() {
        return (typeof this === 'undefined' || this === null);
    };

    $.fn.isNullUndefinedOrEmpty = function() {
        return (typeof this === 'undefined' || this === null || this == '');
    };

})(jQuery);


/*****  Project Specific Javascript Below ******/
(function($){
    $.fn.createDropdown = function(ddName, callbackOnSelect) {
        var dropdown = $(this);
        var ddSelector = '.cbo'+ddName;
        var ddNameLower = ddName.toLowerCase();
        var tmpId = 0;
        var tmpName = '';
        $(document).on('hide.bs.dropdown', ddSelector, function (e) {
            if ($('#hid'+ddName+'Id').val() == 0) { // no selection made.  just closing dropdown.
                setDropDownValue(ddName, tmpId, tmpName);
            }
            $("#txt"+ddName).prop('readonly', true);
        });
        $(document).on('click', '#btn'+ddName, function(e) {
            if ($(".lst-"+ddNameLower).is(':visible')) {   // drop down is open
                tmpId = $('#hid'+ddName+'Id').val();
                tmpName = $('#txt'+ddName).val();
                $("#txt"+ddName).prop('readonly', false);
                setDropDownValue(ddName, 0, '');
                $('#txt'+ddName).focus();
                $(".lst-"+ddNameLower+" > li").show();
            }
        });
        $(document).on('keyup', '#txt'+ddName, function(e) {
            var txtFilter = $(this).val().toLowerCase();
            var listItems = $(".lst-"+ddNameLower+" > li");
            listItems.hide();
            listItems.filter(function() {
                return $(this).text().toLowerCase().indexOf(txtFilter) > -1;
            }).show();
        });
        $(document).on('click', ddSelector+' .lnk-dditem', function(e) {
            setDropDownValue(ddName, $(this).data('ddid'), $(this).data('ddname'));
            $('#txt'+ddName).change(); // this triggers flash msg to close
            if (callbackOnSelect !== undefined) {
                callbackOnSelect($(this).data('ddid'), $(this).data('ddname'));
            }
            dropdown.removeClass('open');
            return false;
        });
    };
})(jQuery);

(function($){
    $.fn.createDropdownOld = function(ddName, callbackOnSelect) {
        var dropdown = $(this);
        var tmpId = 0;
        var tmpName = '';
        var ddNameLower = ddName.toLowerCase();
        $(document).on('hide.bs.dropdown', '.cbo'+ddName, function (e) {
            if ($('#hid'+ddName+'Id').val() == 0) { // no selection made.  just closing dropdown.
                setDropDownValue(ddName, tmpId, tmpName);
            }
            $("#txt"+ddName).prop('readonly', true);
        });
        $(document).on('click', '#btn'+ddName, function(e) {
            if ($(".lst-"+ddNameLower).is(':visible')) {   // drop down is open
                tmpId = $('#hid'+ddName+'Id').val();
                tmpName = $('#txt'+ddName).val();
                $("#txt"+ddName).prop('readonly', false);
                setDropDownValue(ddName, 0, '');
                $('#txt'+ddName).focus();
                $(".lst-"+ddNameLower+" > li").show();
            }
        });
        $(document).on('keyup', '#txt'+ddName, function(e) {
            var txtFilter = $(this).val().toLowerCase();
            var listItems = $(".lst-"+ddNameLower+" > li");
            listItems.hide();
            listItems.filter(function() {
                return $(this).text().toLowerCase().indexOf(txtFilter) > -1;
            }).show();
        });
        $(document).on('click', '.lnk-'+ddNameLower, function(e) {
            setDropDownValue(ddName, $(this).data(ddNameLower+'id'), $(this).data(ddNameLower+'name'));
            $('#txt'+ddName).change(); // this triggers flash msg to close
            if (callbackOnSelect !== undefined) {
                callbackOnSelect($(this).data(ddNameLower+'id'), $(this).data(ddNameLower+'name'));
            }
            return true;
        });
    };
})(jQuery);

//(function($){
//    $.fn.createDropdownWithIds = function(dropdownId, btnId, hidId, txtId, itemName) {
//        var dropdown = $(this);
//        var tmpId = 0;
//        var tmpValue = '';
//        $(document).on('hide.bs.dropdown', dropdownId, function (e) {
//            if ($(hidId).val() == 0) { // no selection made.  just closing dropdown.
//                $(hidId).val(tmpId);
//                $(txtId).val(tmpValue);
//            }
//            $(txtId).prop('readonly', true);
//        });
//        $(document).on('click', btnId, function(e) {
//            tmpId = $(hidId).val();
//            tmpValue = $(txtId).val();
//            $(txtId).prop('readonly', false);
//            $(hidId).val(0);
//            $(txtId).val('');
//            $(txtId).focus();
//            $(".lst-"+itemName+" > li").show();
//        });
//        $(document).on('keyup', txtId, function(e) {
//            var txtFilter = $(this).val().toLowerCase();
//            var lstItems = $(".lst-vendor > li");
//            lstItems.hide();
//            lstItems.filter(function() {
//                return $(this).text().toLowerCase().indexOf(txtFilter) > -1;
//            }).show();
//        });
//        $(document).on('click', '.lnk-vendor', function(e) {
//            $(hidId).val($(this).data(itemName+'id'));
//            $(txtId).val($(this).data(itemName+'name'));
//            $(txtId).change(); // this triggers flash msg to close
//            return true;
//        });
//    };
//})(jQuery);

function setDropDownValue(ddName, id, name) {
    $('#hid'+ddName+'Id').val(id);
    $('#txt'+ddName).val(name);
}

var formChanged = false;
function setFormChange(changeState, initializeWithFormName) {
    if (initializeWithFormName !== undefined) {
        $(initializeWithFormName).on('change', 'input, textarea, select', function() {
            setFormChange(true);
        });
    }

    if (changeState != formChanged) {
        formChanged = changeState;
        if (formChanged) {
            $('.msg-unsaved').show();
        } else {
            $('.msg-unsaved').hide();
        }
    }
}

function showNewCompanyPopup(companyIdSelector, nameSelector, fnCallback) {
    showPopupList('#dlgCompanyNew', companyIdSelector, nameSelector, url('company/CompanyPopupNew'), fnCallback);
}

function showNewContactPopup(contactIdSelector, nameSelector, fnCallback) {
    showPopupList('#dlgContactNew', contactIdSelector, nameSelector, url('contact/ContactPopupNew'), fnCallback);
}

function showPopupList(dlgSelector, idSelector, nameSelector, postUrl, fnCallback) {
    showLoader();
    $(dlgSelector).data('idselector', idSelector);
    $(dlgSelector).data('nameselector', nameSelector);
    if (fnCallback !== undefined) {
        $(dlgSelector).data('fncallback', fnCallback);
    }
    $.ajax({
        type: "post",
        url: postUrl,
        data: {},
        success: function(results) {
            $(dlgSelector).html(results);
            $(dlgSelector).modal();
            hideLoader();
        },
        error: function(xhr) {
            var errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ':</br>' + (xhr.responseText);
            logError(errorResult);
            hideLoader();
        }
    });
}

function highlightFilter(jqSelector) {
    var isHighlighted = jqSelector.hasClass('lightyellow');
    if (jqSelector.val().length > 0) {
        if (!isHighlighted) {
            jqSelector.toggleClass('lightyellow');
        }
    } else {
        if (isHighlighted) {
            jqSelector.toggleClass('lightyellow');
        }
    }
}

function makeReadOnly(roleLevel, curRole) {
    if (curRole < roleLevel) {
        $('input, textarea').prop('readonly', true);
        $('option, button').prop('disabled', true);
    }
}

// Global Utility Javascript functions (jsUtil)
window.jsUtil = {
    // update field only if it doesn't already contain a value
    setNonEmptyField: function (idSelector, value) {
        if ($.trim($(idSelector).val()).length == 0) {
            $(idSelector).val(value);
        }
    }
}

// Capitol Global Javascript functions (jsCap)
window.jsCap = {
    company: {
        get: function(companyId, fnCallback) {
            showLoader();
            var postUrl = url('company/CompanyGet')+'/id/'+companyId;
            $.ajax({
                type: "post",
                url: postUrl,
                data: {},
                success: function(results, textStatus, xhr) {
                    if (xhr.status == 200) {
                        if (fnCallback !== undefined) {
                            fnCallback(results.mCompany);
                        }
                    } else {
                        logError(results);
                    }
                    hideLoader();
                },
                error: function(xhr) {
                    var errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ': ' + (xhr.responseText);
                    logError(errorResult);
                    hideLoader();
                }
            });
        },

        refreshPopupList: function() {
            showLoader();
            var postUrl = url('company/CompanyDropdownGetList');
            $.ajax({
                type: "post",
                url: postUrl,
                data: {},
                success: function(results, textStatus, xhr) {
                    if (xhr.status == 200) {
                        $('.company-dropdown').html(results);
                    } else {
                        logError(results);
                    }
                    hideLoader();
                },
                error: function(xhr) {
                    var errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ': ' + (xhr.responseText);
                    logError(errorResult);
                    hideLoader();
                }
            });
        }

    },

    ms4: {
        get: function(ms4Id, fnCallback) {
            showLoader();
            var postUrl = url('site/Ms4Get')+'/id/'+ms4Id;
            $.ajax({
                type: "post",
                url: postUrl,
                data: {},
                success: function(results, textStatus, xhr) {
                    if (xhr.status == 200) {
                        if (fnCallback !== undefined) {
                            fnCallback(results.ms4);
                        }
                    } else {
                        logError(results);
                    }
                    hideLoader();
                },
                error: function(xhr) {
                    var errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ': ' + (xhr.responseText);
                    logError(errorResult);
                    hideLoader();
                }
            });
        }

    },

    contact: {
        get: function(contactId, fnCallback) {
            showLoader();
            var postUrl = url('contact/ContactGet')+'/id/'+contactId;
            $.ajax({
                type: "post",
                url: postUrl,
                data: {},
                success: function(results, textStatus, xhr) {
                    if (xhr.status == 200) {
                        if (fnCallback !== undefined) {
                            fnCallback(results.mContact);
                        }
                    } else {
                        logError(results);
                    }
                    hideLoader();
                },
                error: function(xhr) {
                    var errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ': ' + (xhr.responseText);
                    logError(errorResult);
                    hideLoader();
                }
            });
        },

        refreshPopupList: function() {
            showLoader();
            var postUrl = url('contact/ContactDropdownGetList');
            $.ajax({
                type: "post",
                url: postUrl,
                data: {},
                success: function(results, textStatus, xhr) {
                    if (xhr.status == 200) {
                        $('.contact-dropdown').html(results);
                    } else {
                        logError(results);
                    }
                    hideLoader();
                },
                error: function(xhr) {
                    var errorResult = 'ERROR ' + xhr.status + ' - '+ xhr.statusText + ': ' + (xhr.responseText);
                    logError(errorResult);
                    hideLoader();
                }
            });
        }

    }

}